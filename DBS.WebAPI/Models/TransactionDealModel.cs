﻿using DBS.Entity;
using DBS.WebAPI.Models;
using DBS.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Transactions;
using System.Web;
using System.Data.Entity;

namespace DBS.WebAPI.Models
{
    #region Property

    public class TransactionDealCheckerDetailModel
    {
        public TransactionDealDetailModel Transaction { get; set; }
        public IList<VerifyDealModel> Verify { get; set; }
        public IList<TransactionTimelineModel> Timelines { get; set; }
        //add by fandi --Trans.History
        public IList<TransactionDetailMaker> TransactionMaker { get; set; }
        // for double task
        public ClientTaskData ClienTaskData { get; set; }
        //end
    }
    public class VerifyDealModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public bool IsVerified { get; set; }
    }
    public class TransactionDealModel
    {
        public long TransactionDealID { get; set; }
        public string ApplicationID { get; set; }
        public string Product { get; set; }
        public bool IsNewCustomer { get; set; }
        public string CIF { get; set; }
        public DateTime? ValueDate { get; set; }
        public DateTime? TradeDate { get; set; }
        public string AccountNumber { get; set; }
        public CustomerModel Customer { get; set; }
        public StatementLetterModel StatementLetter { get; set; }
        public string TZReference { get; set; }
        public RateTypeModel RateType { get; set; }
        public string Rate { get; set; }
        public CurrencyModel Currency { get; set; }
        public decimal Amount { get; set; }
        public decimal AmountUSD { get; set; }
        public string UnutilizedAmount { get; set; }
        public string ShortageAmount { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        public string IsFxTransaction { get; set; }
        public string FxTransactionDesc { get; set; }
        public string TransactionStatus { get; set; }
        public string CustomerName { get; set; }
        public string CurrencyDesc { get; set; }
        public string TzStatus { get; set; }
        public bool? IsTMO { get; set; }
    }

    public class TransactionDealDetailModel
    {
        /// <summary>
        /// CIF
        /// </summary>
        public string CIF { get; set; }

        /// <summary>
        /// Value Date
        /// </summary>
        public DateTime? TradeDate { get; set; }

        /// <summary>
        /// Value Date
        /// </summary>
        public DateTime? ValueDate { get; set; }

        /// <summary>
        /// AccountNumber
        /// </summary>
        public string AccountNumber { get; set; }

        /// <summary>
        /// StatementLetter
        /// </summary>
        public StatementLetterModel StatementLetter { get; set; }

        /// <summary>
        /// TZReference
        /// </summary>
        public string TZReference { get; set; }

        /// <summary>
        /// Product Type
        /// </summary>
        public ProductTypeModel ProductType { get; set; }

        /// <summary>
        /// RateType
        /// </summary>
        public RateTypeModel RateType { get; set; }

        /// <summary>
        /// Rate
        /// </summary>
        public string Rate { get; set; }

        /// <summary>
        /// Workflow Instance ID
        /// </summary>
        public Guid? WorkflowInstanceID { get; set; }

        /// <summary>
        /// Transaction Deal ID.
        /// </summary>
        public long ID { get; set; }

        /// <summary>
        /// Application ID
        /// </summary>
        public string ApplicationID { get; set; }

        /// <summary>
        /// Customer Data Details
        /// </summary>
        public CustomerModel Customer { get; set; }

        /// <summary>
        /// Currency Details
        /// </summary>
        public CurrencyModel Currency { get; set; }

        /// <summary>
        /// Transaction Amount
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Eqv.USD
        /// </summary>
        public decimal AmountUSD { get; set; }

        /// <summary>
        /// Debit Acc Number
        /// </summary>
        public CustomerAccountModel Account { get; set; }

        /// <summary>
        /// Type
        /// </summary>
        public decimal? Type { get; set; }

        /// <summary>
        /// Create Date
        /// </summary>
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }

        /// <summary>
        /// List Transaction Document Details
        /// </summary>
        public IList<TransactionDealDocumentModel> Documents { get; set; }

        /// <summary>
        /// TZ Number
        /// </summary>
        public string TZNumber { get; set; }

        /// <summary>
        /// PaymentDetails
        /// </summary>
        public string PaymentDetails { get; set; }

        /// <summary>
        /// Underlyings
        /// </summary> 
        public IList<TransDetailUnderlyingModel> Underlyings { get; set; }

        /// <summary>
        /// Transaction Create By
        /// </summary> 
        public string CreateBy { get; set; }

        /// <summary>
        /// is book deal not new transaction
        /// </summary> 
        public bool IsBookDeal { get; set; }

        /// <summary>
        /// book underlying amount
        /// </summary>
        public decimal? bookunderlyingamount { get; set; }

        /// <summary>
        /// book underlying currency id
        /// </summary>
        public int? bookunderlyingcurrency { get; set; }

        /// <summary>
        /// book underlying id
        /// </summary>
        public long? bookunderlyingcode { get; set; }
        public string bookunderlyingdoccode { get; set; }
        public string bookunderlyingcurrencydesc { get; set; }
        public string bookunderlyingdesc { get; set; }
        public string AccountCurrencyCode { get; set; }
        public int RateTypeID { get; set; }
        public decimal? utilizationAmount { get; set; }
        public bool? IsFxTransaction { get; set; }
        public string OtherUnderlying { get; set; }
        public UnderlyingDocModel DealUnderlying { get; set; }

        public bool? IsOtherAccountNumber { get; set; }
        public string OtherAccountNumber { get; set; }
        public CurrencyModel DebitCurrency { get; set; }
        public string TransactionStatus { get; set; }
        public string TzStatus { get; set; }
        public bool? IsJointAccount { get; set; }

        public bool? IsResident { get; set; }
        public bool? IsTMO { get; set; }
        public string NB { get; set; }
        public string SwapTZDealNo { get; set; }
        public int? SwapType { get; set; }
        public bool? IsNettingTransaction { get; set; }
        public DateTime? TouchTimeStartDate { get; set; }
        public IList<ReviseDealDocumentModel> ReviseDealDocuments { get; set; }
    }

    public class TransactionTMOModel
    {
        public long ID { get; set; }
        public Guid? WorkflowInstanceID { get; set; }
        public string ApplicationID { get; set; }
        public bool? IsDraft { get; set; }
        public string CIF { get; set; }
        public DateTime? TradeDate { get; set; }
        public DateTime? ValueDate { get; set; }
        public StatementLetterModel StatementLetter { get; set; }
        public string TZReference { get; set; }
        public string Rate { get; set; }
        public CustomerModel Customer { get; set; }
        public CurrencyModel BuyCurrency { get; set; }
        public CurrencyModel SellCurrency { get; set; }
        public decimal? BuyAmount { get; set; }
        public decimal? SellAmount { get; set; }
        public decimal AmountUSD { get; set; }
        public string Remarks { get; set; }
        public DateTime? SubmissionDateSL { get; set; }
        public DateTime? SubmissionDateInstruction { get; set; }
        public DateTime? SubmissionDateUnderlying { get; set; }
        public bool? IsUnderlyingCompleted { get; set; }
        public long? UnderlyingCodeID { get; set; }
        public string OtherUnderlying { get; set; }
        public string TransactionStatus { get; set; }
        public ProductTypeModel ProductType { get; set; }
        public bool? IsResident { get; set; }
        public string AccountCurrencyCode { get; set; }
        public bool? IsJointAccount { get; set; }
        public CustomerAccountModel Account { get; set; }
        public string AccountNumber { get; set; }
        public bool? IsCanceled { get; set; }
        public string NB { get; set; }
        public string SwapTZDealNo { get; set; }
        public bool? IsNettingTransaction { get; set; }
        public DateTime? TouchTimeStartDate { get; set; }
        public int? SwapType { get; set; }
        public long? SwapTransactionID { get; set; }
        public bool? IsOtherAccountNumber { get; set; }
        public string OtherAccountNumber { get; set; }
        public CurrencyModel DebitCurrency { get; set; }
        public IList<TransactionDealDocumentModel> Documents { get; set; }
        public IList<TransDetailUnderlyingModel> Underlyings { get; set; }
        public IList<ReviseDealDocumentModel> ReviseDealDocuments { get; set; }
        public IList<DocumentTBO> DocumentTBO { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
        //Tambah Agung
        public decimal? Rounding { get; set; }
        public bool? IsResubmit { get; set; }
        public bool? IsIPE { get; set; }
    }

    public class DocumentTBO
    {
        public int TBOIndex { get; set; }
        public int TBOTMOID { get; set; }
        public string TBOTMOName { get; set; }
        public DateTime ExpectedDateTBO { get; set; }
        public string RemaksTBO { get; set; }
        public bool? IsChecklistTBO { get; set; }
        public long TBOTMOTransactionID { get; set; }
        public long TBOSLAID { get; set; }
        public long TransactionDealID { get; set; }
        public string CustomerName { get; set; }
        public string CIF { get; set; }
    }


    public class TransactionDealDocumentModel
    {
        /// <summary>
        /// Transaction Document ID
        /// </summary>
        public long ID { get; set; }

        /// <summary>
        /// Document Type details
        /// </summary>
        public DocumentTypeModel Type { get; set; }

        /// <summary>
        /// Document Type details
        /// </summary>
        public DocumentPurposeModel Purpose { get; set; }

        /// <summary>
        /// Document filename
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// DocumentPath
        /// </summary>
        public string DocumentPath { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }

    }

    public class ReviseDealDocumentModel
    {
        /// <summary>
        /// Transaction Document ID
        /// </summary>
        public long ID { get; set; }
        /// <summary>
        /// Underlying ID
        /// </summary>
        public long UnderlyingID { get; set; }
        /// <summary>
        /// Document Type details
        /// </summary>
        public DocumentTypeModel Type { get; set; }

        /// <summary>
        /// Document Type details
        /// </summary>
        public DocumentPurposeModel Purpose { get; set; }

        /// <summary>
        /// Document filename
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// DocumentPath
        /// </summary>
        public string DocumentPath { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }

        public bool? IsDormant { get; set; }
    }
    public class TransactionDealTotalAmountModel
    {
        public string CIF { get; set; }
        public long TransactionID { get; set; }
        public decimal USDAmount { get; set; }
    }

    public class TransDetailUnderlyingModel
    {
        public long ID { get; set; }
        public bool Enable { get; set; }
        public decimal USDAmount { get; set; }
        public decimal? UtilizeAmountDeal { get; set; }
    }

    public class TZTransactionModel
    {
        public long ID { get; set; }
        public string TZRef { get; set; }
        public string CustomerName { get; set; }
        public ProductTypeModel ProductType { get; set; }
        public DateTime? TradeDate { get; set; }
        public CurrencyModel Currency { get; set; }
        public decimal Amount { get; set; }
        public string Rate { get; set; }
        public string AccountNumber { get; set; }
        public bool? IsFxTransaction { get; set; }
        #region basri
        public StatementLetterModel StatementLetter { get; set; }
        public decimal? utilizationAmount { get; set; }
        public bool? IsJointAccount { get; set; }
        #endregion

    }

    public class TransactionDealRow : TransactionDealModel
    {
        public int RowID { get; set; }

    }

    public class TransactionDealGrid : Grid
    {
        public IList<TransactionDealRow> Rows { get; set; }
    }
    #region Tambah Agung
    public class InstructionDocumentGrid : Grid
    {
        public IList<InstructionDocumentModel> Rows { get; set; }
    }
    //public class InstructionDocumentRow : InstructionDocumentModel
    //{
    //    public int RowID { get; set; }

    //}

    public class InstructionDocumentModel
    {
        public long TransactionTMOID { get; set; }
        public string ApplicationID { get; set; }
        public string FileName { get; set; }
        public DocumentPurposeModel Purpose { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public int ID { get; set; }

        public bool IsSelected { get; set; }

        public string DocumentPath { get; set; }

        public bool IsNewDocument { get; set; }

        public DocumentTypeModel Type { get; set; }
    }
    #endregion
    public class TzInformationGrid : Grid
    {
        public IList<TzInformationRow> Rows { get; set; }
    }
    public class TzInformationRow : TZTransactionModel
    {
        public int RowID { get; set; }

    }
    #endregion

    #region Filter
    public class TransactionDealFilter : Filter { }
    public class InstructionDocumentFilter : Filter { }
    #endregion

    #region Interface
    public interface ITransactionDealRepository : IDisposable
    {
        bool GetTransactionDeal(ref IList<TransactionDealModel> TransactionDeal, ref string message);
        bool GetTransactionDeal(int page, int size, IList<TransactionDealFilter> filters, string sortColumn, string sortOrder, ref TransactionDealGrid transaction, ref string message);
        //Tambah Agung
        bool GetTransactionDraftByIDTMOIPE(long id, ref TransactionDraftModel transaction, ref string message);
        bool GetDataInstructionDocument(int page, int size, IList<InstructionDocumentFilter> filters, string sortColumn, string sortOrder, ref InstructionDocumentGrid transaction, string cif, ref string message);
        //End
        bool GetTransactionDealById(long id, ref TransactionDealDetailModel data, ref string message);
        bool GetTransactionTMOById(long id, ref TransactionTMOModel data, ref string message);
        bool AddTransactionDeal(TransactionDealDetailModel data, ref long transactionDealID, ref string message);
        bool UpdateTransactionDealById(long transactionDealID, TransactionDealDetailModel data, ref string message);
        bool UpdateTransactionTMOById(long transactionDealID, TransactionTMOModel data, ref string message);
        bool UpdateTransactionCorrectionTMOById(long transactionDealID, TransactionTMOModel data, ref string message);
        bool UpdateBookDealById(string id, TransactionDealDetailModel data, ref string message);
        bool GetTransactionDealDetails(Guid instanceID, ref TransactionDealDetailModel transaction, ref string message);
        bool GetTransactionReviseDealDetails(Guid instanceID, ref TransactionDealDetailModel transaction, ref string message);
        bool GetAmountTransactionDeal(string Cif, bool isStatementB, ref decimal output, ref string message);
        bool AddBizUnitChecker(Guid workflowInstanceID, long approverID, PaymentCheckerDataModel data, ref string message);
        bool SetApplicationID(string applicationID, long transactionID, ref string message);

        #region IPE
        bool UpdateTransactionDealAfterReviseIPE(Guid workflowInstanceID, long approverID, TransactionDealDetailModel transaction, ref string message);
        bool UpdateTransactionDealReviseIPE(Guid workflowInstanceID, TransactionDealDetailModel TransactionDeal, ref string message);
        #endregion

        bool UpdateTransactionDealAfterRevise(Guid workflowInstanceID, long approverID, TransactionDealDetailModel transaction, ref string message);
        bool UpdateTransactionDealRevise(Guid workflowInstanceID, TransactionDealDetailModel TransactionDeal, ref string message);
        bool UpdateTransactionTMORevise(Guid workflowInstanceID, int TransactionDealID, ref string message);
        bool GetTzInformation(int page, int size, string CIF, ref TzInformationGrid transactionDeal, ref string message);
        bool RoolbackDealCustomerUnderlying(long IdTransactionDeal, ref string message);
        bool RoolbackCustomerUnderlyingTMO(long IdTransaction, ref string message);
        bool UpdateTransactionStatusOTTFXTransaction(string TZNumber, ref string message);
    }
    #endregion

    #region Repository
    public class TransactionDealRepository : ITransactionDealRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool AddTransactionDeal(TransactionDealDetailModel data, ref long transactionDealID, ref string message)
        {
            bool isSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    if (data.IsBookDeal)
                    {
                        DBS.Entity.TransactionDeal datadeal = new DBS.Entity.TransactionDeal()
                        {
                            ApplicationID = data.ApplicationID,
                            CIF = data.Customer.CIF,
                            //AccountNumber = data.Account.AccountNumber,
                            TradeDate = data.TradeDate,
                            ValueDate = data.ValueDate,
                            StatementLetterID = data.StatementLetter.ID,
                            TZRef = data.TZReference,
                            Rate = data.Rate,
                            ProductTypeID = data.ProductType.ID,
                            CurrencyID = data.Currency.ID,
                            Amount = data.Amount,
                            IsBookDeal = data.IsBookDeal,
                            //AmountUSD = data.AmountUSD,
                            UtilizedAmount = 0, // data
                            CreatedDate = DateTime.UtcNow,
                            CreatedBy = currentUser.GetCurrentUser().LoginName,
                            //UnderlyingCurrencyID = data.bookunderlyingcurrency,
                            UnderlyingCodeID = data.bookunderlyingcode,
                            //UnderlyingAmount = data.bookunderlyingamount,
                            IsFxTransaction = data.IsFxTransaction,
                            OtherUnderlying = data.OtherUnderlying,
                            IsJointAccount = data.IsJointAccount,
                            IsResident = data.IsResident.HasValue ? data.IsResident.Value : false,

                            //IsJoinAccountTransaction = data
                        };

                        if (data.DebitCurrency.Code != null)
                        {
                            datadeal.DebitCurrencyID = data.DebitCurrency.ID;
                        }

                        if (data.IsOtherAccountNumber != null)
                        {
                            if (data.IsOtherAccountNumber == true)
                            {
                                datadeal.OtherAccountNumber = data.OtherAccountNumber;
                                datadeal.AccountNumber = null;

                                datadeal.IsOtherAccountNumber = true;
                            }
                            else
                            {
                                if (data.Account.AccountNumber != null)
                                {
                                    datadeal.AccountNumber = data.Account.AccountNumber;
                                    datadeal.OtherAccountNumber = null;
                                }
                                datadeal.IsOtherAccountNumber = false;
                            }

                        }
                        if (data.Currency.Code.ToLower() == "usd")
                        {
                            datadeal.AmountUSD = data.Amount;
                        }
                        else
                        {
                            IHelperRepository helperRepo = new HelperRepository();
                            decimal usdAmount = 0;

                            if (helperRepo.GetAmountUSD(data.Currency.ID, data.Amount, ref usdAmount, ref message))
                            {
                                datadeal.AmountUSD = usdAmount;
                            }
                            else
                            {
                                datadeal.AmountUSD = data.AmountUSD;
                            }
                        }
                        context.TransactionDeals.Add(datadeal);
                        context.SaveChanges();
                    }
                    else
                    {
                        DBS.Entity.TransactionDeal datadeal = new DBS.Entity.TransactionDeal()
                        {
                            ApplicationID = data.ApplicationID,
                            CIF = data.Customer.CIF,
                            AccountNumber = data.Account.AccountNumber,
                            TradeDate = data.TradeDate,
                            ValueDate = data.ValueDate,
                            StatementLetterID = data.StatementLetter.ID,
                            TZRef = data.TZReference,
                            ProductTypeID = data.ProductType.ID,
                            Rate = data.Rate,
                            CurrencyID = data.Currency.ID,
                            Amount = data.Amount,
                            IsBookDeal = data.IsBookDeal,
                            //AmountUSD = data.AmountUSD,
                            UtilizedAmount = data.AmountUSD, // warned
                            CreatedDate = DateTime.UtcNow,
                            CreatedBy = currentUser.GetCurrentUser().LoginName,
                            IsFxTransaction = data.IsFxTransaction,
                            UnderlyingCodeID = data.bookunderlyingcode,
                            OtherUnderlying = data.OtherUnderlying,
                            IsJointAccount = data.IsJointAccount,
                            TransactionStatus = "",// view deal transaction 
                            IsResident = data.IsResident.HasValue ? data.IsResident.Value : false,
                            TouchTimeStartDate = data.TouchTimeStartDate.HasValue ? data.TouchTimeStartDate.Value : (DateTime?)null,
                        };

                        if (data.DebitCurrency.Code != null)
                        {
                            datadeal.DebitCurrencyID = data.DebitCurrency.ID;
                        }

                        if (data.IsOtherAccountNumber != null && data.IsOtherAccountNumber == true)
                        {
                            datadeal.OtherAccountNumber = data.OtherAccountNumber;
                            datadeal.AccountNumber = null;
                            datadeal.IsOtherAccountNumber = true;
                        }
                        else
                        {
                            datadeal.AccountNumber = data.Account.AccountNumber;
                            datadeal.OtherAccountNumber = null;
                            datadeal.IsOtherAccountNumber = false;
                        }
                        if (data.Currency.Code.ToLower() == "usd")
                        {
                            datadeal.AmountUSD = data.Amount;
                        }
                        else
                        {
                            IHelperRepository helperRepo = new HelperRepository();
                            decimal usdAmount = 0;

                            if (helperRepo.GetAmountUSD(data.Currency.ID, data.Amount, ref usdAmount, ref message))
                            {
                                datadeal.AmountUSD = usdAmount;
                            }
                            else
                            {
                                datadeal.AmountUSD = data.AmountUSD;
                            }
                        }
                        context.TransactionDeals.Add(datadeal);
                        context.SaveChanges();

                        transactionDealID = datadeal.TransactionDealID;

                        // Chandra - Update CheckUnderlyingUtilize
                        if (data.Underlyings.Count > 0)
                        {
                            decimal availableAmount = data.AmountUSD;
                            for (int i = 0; data.Underlyings.Count > i; i++)
                            {
                                long UnderlyingID = data.Underlyings[i].ID;
                                Entity.CustomerUnderlying customerUnderlying = context.CustomerUnderlyings.Where(a => a.UnderlyingID.Equals(UnderlyingID)).SingleOrDefault();
                                if (customerUnderlying != null)
                                {
                                    decimal tempAvailableAmount = (decimal)customerUnderlying.AvailableAmountUSD;
                                    availableAmount = (decimal)customerUnderlying.AvailableAmountUSD - availableAmount;
                                    customerUnderlying.IsUtilize = true;
                                    customerUnderlying.UpdateDate = DateTime.UtcNow;
                                    customerUnderlying.AvailableAmountUSD = (decimal?)(availableAmount > 0 ? availableAmount : 0);
                                    customerUnderlying.UpdateBy = currentUser.GetCurrentUser().LoginName;
                                    context.SaveChanges();
                                    // set amount for transaction underlying
                                    data.Underlyings[i].USDAmount = tempAvailableAmount - (availableAmount > 0 ? availableAmount : 0);
                                    availableAmount = (availableAmount > 0 ? 0 : Math.Abs(availableAmount));
                                }
                            }
                        }

                        // Chandra - add transaction underlying 
                        if (data.Underlyings.Count > 0 && !data.IsBookDeal)
                        {
                            List<long> UnderlyingID = new List<long>();
                            foreach (TransDetailUnderlyingModel underlyingItem in data.Underlyings)
                            {
                                Entity.TransactionDealUnderlying Underlying = new Entity.TransactionDealUnderlying()
                                {
                                    TransactionDealID = transactionDealID,
                                    UnderlyingID = underlyingItem.ID,
                                    Amount = underlyingItem.USDAmount,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                };
                                context.TransactionDealUnderlyings.Add(Underlying);
                                UnderlyingID.Add(underlyingItem.ID);
                            }
                            context.SaveChanges();
                            IList<TransactionDealDocumentModel> documents = GetDocumentUnderlying(UnderlyingID);
                            foreach (var item in documents)
                            {
                                data.Documents.Add(item);
                            }
                            IList<TransactionDealDocumentModel> documentsBulk = GetDocumentBulkUnderlying(UnderlyingID);
                            foreach (var item in documentsBulk)
                            {
                                data.Documents.Add(item);
                            }
                        }

                        //update customer undelying?
                        //Add transaction document
                        if (data.Documents.Count > 0)
                        {
                            foreach (var item in data.Documents)
                            {
                                Entity.TransactionDealDocument document = new Entity.TransactionDealDocument()
                                {
                                    TransactionDealID = transactionDealID,
                                    DocTypeID = item.Type.ID,
                                    PurposeID = item.Purpose.ID,
                                    Filename = item.FileName,
                                    DocumentPath = item.DocumentPath,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                };
                                context.TransactionDealDocuments.Add(document);
                            }
                            context.SaveChanges();
                        }
                    }

                    ts.Complete();
                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }

            return isSuccess;
        }

        public IList<TransactionDealDocumentModel> GetDocumentUnderlying(List<long> IDUnderlying)
        {
            try
            {
                IList<TransactionDealDocumentModel> documents = new List<TransactionDealDocumentModel>();

                documents = (from a in context.CustomerUnderlyingMappings.Where(x => x.IsDeleted.Equals(false))
                             join b in context.CustomerUnderlyingFiles on a.UnderlyingFileID equals b.UnderlyingFileID
                             where (from x in IDUnderlying select x).Contains(a.UnderlyingID)
                             select new TransactionDealDocumentModel
                             {
                                 ID = 0,
                                 FileName = b.FileName,
                                 Type = new DocumentTypeModel { ID = b.DocTypeID },
                                 Purpose = new DocumentPurposeModel { ID = b.PurposeID },
                                 DocumentPath = b.DocumentPath,
                             }).ToList();

                return documents;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<TransactionDealDocumentModel> GetDocumentBulkUnderlying(List<long> IDUnderlyings)
        {

            try
            {
                IList<TransactionDealDocumentModel> documents = new List<TransactionDealDocumentModel>();
                List<long> childs = (from a in context.MappingBulkUnderlyings where IDUnderlyings.Contains(a.ParentID) select a.UnderlyingID).ToList();

                if (childs != null)
                {
                    documents = (from a in context.CustomerUnderlyingMappings.Where(x => x.IsDeleted.Equals(false))
                                 join b in context.CustomerUnderlyingFiles on a.UnderlyingFileID equals b.UnderlyingFileID
                                 where (from x in childs select x).Contains(a.UnderlyingID) && a.IsDeleted.Equals(false)
                                 select new TransactionDealDocumentModel
                                 {
                                     ID = 0,
                                     FileName = b.FileName,
                                     Type = new DocumentTypeModel { ID = b.DocTypeID },
                                     Purpose = new DocumentPurposeModel { ID = b.PurposeID },
                                     DocumentPath = b.DocumentPath,
                                 }).ToList();

                }
                return documents;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool GetTransactionReviseDealDetails(Guid instanceID, ref TransactionDealDetailModel transaction, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                context.Transactions.AsNoTracking();

                transaction = (from a in context.TransactionDeals
                               where a.WorkflowInstanceID.Value.Equals(instanceID)
                               select new TransactionDealDetailModel
                               {
                                   ID = a.TransactionDealID,
                                   CIF = a.CIF,
                                   WorkflowInstanceID = a.WorkflowInstanceID.Value,
                                   ApplicationID = a.ApplicationID,
                                   TradeDate = a.TradeDate,
                                   ValueDate = a.ValueDate,
                                   //AccountNumber = a.AccountNumber,
                                   //RateTypeID = a.RateTypeID,
                                   ProductType = new ProductTypeModel()
                                   {
                                       ID = a.ProductType.ProductTypeID,
                                       Code = a.ProductType.ProductTypeCode,
                                       Description = a.ProductType.ProductTypeDesc,
                                       IsFlowValas = a.ProductType.IsFlowValas
                                   },
                                   Rate = a.Rate,
                                   StatementLetter = new StatementLetterModel()
                                   {
                                       ID = a.StatementLetter.StatementLetterID,
                                       Name = a.StatementLetter.StatementLetterName
                                   },
                                   TZNumber = a.TZRef,
                                   Currency = new CurrencyModel()
                                   {
                                       ID = a.Currency.CurrencyID,
                                       Code = a.Currency.CurrencyCode,
                                       Description = a.Currency.CurrencyDescription,
                                       LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                       LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                   },
                                   Amount = a.Amount,
                                   AmountUSD = a.AmountUSD,
                                   IsResident = a.IsResident.HasValue ? a.IsResident.Value : false,
                                   //Account = new CustomerAccountModel()
                                   //{
                                   //    AccountNumber = a.CustomerAccount.AccountNumber,
                                   //    Currency = new CurrencyModel()
                                   //    {
                                   //        ID = a.CustomerAccount.Currency.CurrencyID,
                                   //        Code = a.CustomerAccount.Currency.CurrencyCode,
                                   //        Description = a.CustomerAccount.Currency.CurrencyDescription,
                                   //        LastModifiedBy = a.CustomerAccount.Currency.UpdateDate == null ? a.CustomerAccount.Currency.CreateBy : a.CustomerAccount.Currency.UpdateBy,
                                   //        LastModifiedDate = a.CustomerAccount.Currency.UpdateDate == null ? a.CustomerAccount.Currency.CreateDate : a.CustomerAccount.Currency.UpdateDate.Value
                                   //    },
                                   //    LastModifiedBy = a.CustomerAccount.UpdateDate == null ? a.CustomerAccount.CreateBy : a.CustomerAccount.UpdateBy,
                                   //    LastModifiedDate = a.CustomerAccount.UpdateDate == null ? a.CustomerAccount.CreateDate : a.CustomerAccount.UpdateDate.Value
                                   //},
                                   /* Documents = a.TransactionDealDocuments.Where(x => x.IsDeleted.Equals(false)).Select(x => new TransactionDealDocumentModel()
                                    {
                                        ID = x.TransactionDocumentID,
                                        Type = new DocumentTypeModel()
                                        {
                                            ID = x.DocumentType.DocTypeID,
                                            Name = x.DocumentType.DocTypeName,
                                            Description = x.DocumentType.DocTypeDescription,
                                            LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                            LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                        },
                                        Purpose = new DocumentPurposeModel()
                                        {
                                            ID = x.DocumentPurpose.PurposeID,
                                            Name = x.DocumentPurpose.PurposeName,
                                            Description = x.DocumentPurpose.PurposeDescription,
                                            LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                            LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                        },
                                        FileName = x.Filename,
                                        DocumentPath = x.DocumentPath,
                                        LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                        LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                                    }).ToList(),*/
                                   Documents = (from p in a.TransactionDealDocuments
                                                join c in context.CustomerUnderlyingFiles
                                                on p.DocumentPath equals c.DocumentPath into zi
                                                from c in zi.DefaultIfEmpty()

                                                where p.IsDeleted.Equals(false)
                                                select new TransactionDealDocumentModel()
                                                {
                                                    ID = c.UnderlyingFileID != null ? c.UnderlyingFileID : p.TransactionDocumentID,
                                                    Type = new DocumentTypeModel()
                                                    {
                                                        ID = p.DocumentType.DocTypeID,
                                                        Name = p.DocumentType.DocTypeName,
                                                        Description = p.DocumentType.DocTypeDescription,
                                                        LastModifiedBy = p.DocumentType.UpdateDate == null ? p.DocumentType.CreateBy : p.DocumentType.UpdateBy,
                                                        LastModifiedDate = p.DocumentType.UpdateDate == null ? p.DocumentType.CreateDate : p.DocumentType.UpdateDate.Value
                                                    },
                                                    Purpose = new DocumentPurposeModel()
                                                    {
                                                        ID = p.DocumentPurpose.PurposeID,
                                                        Name = p.DocumentPurpose.PurposeName,
                                                        Description = p.DocumentPurpose.PurposeDescription,
                                                        LastModifiedBy = p.DocumentPurpose.UpdateDate == null ? p.DocumentPurpose.CreateBy : p.DocumentPurpose.UpdateBy,
                                                        LastModifiedDate = p.DocumentPurpose.UpdateDate == null ? p.DocumentPurpose.CreateDate : p.DocumentPurpose.UpdateDate.Value
                                                    },
                                                    FileName = p.Filename,
                                                    DocumentPath = p.DocumentPath,
                                                    LastModifiedBy = p.UpdateDate == null ? p.CreateBy : p.UpdateBy,
                                                    LastModifiedDate = p.UpdateDate == null ? p.CreateDate : p.UpdateDate.Value

                                                }).ToList(),
                                   ReviseDealDocuments = (from cu in a.Customer.CustomerUnderlyings
                                                          join cm in context.CustomerUnderlyingMappings on cu.UnderlyingID equals cm.UnderlyingID
                                                          join cf in context.CustomerUnderlyingFiles on cm.UnderlyingFileID equals cf.UnderlyingFileID
                                                          where cu.IsDeleted.Equals(false) && cm.IsDeleted.Equals(false) && cf.IsDeleted.Equals(false)
                                                          select new ReviseDealDocumentModel()
                                                          {
                                                              UnderlyingID = cu.UnderlyingID,
                                                              ID = cf.UnderlyingFileID,
                                                              Type = new DocumentTypeModel()
                                                              {
                                                                  ID = cf.DocumentType.DocTypeID,
                                                                  Name = cf.DocumentType.DocTypeName,
                                                                  Description = cf.DocumentType.DocTypeDescription,
                                                                  LastModifiedBy = cf.DocumentType.UpdateDate == null ? cf.DocumentType.CreateBy : cf.DocumentType.UpdateBy,
                                                                  LastModifiedDate = cf.DocumentType.UpdateDate == null ? cf.DocumentType.CreateDate : cf.DocumentType.UpdateDate.Value
                                                              },
                                                              Purpose = new DocumentPurposeModel()
                                                              {
                                                                  ID = cf.DocumentPurpose.PurposeID,
                                                                  Name = cf.DocumentPurpose.PurposeName,
                                                                  Description = cf.DocumentPurpose.PurposeDescription,
                                                                  LastModifiedBy = cf.DocumentPurpose.UpdateDate == null ? cf.DocumentPurpose.CreateBy : cf.DocumentPurpose.UpdateBy,
                                                                  LastModifiedDate = cf.DocumentPurpose.UpdateDate == null ? cf.DocumentPurpose.CreateDate : cf.DocumentPurpose.UpdateDate.Value
                                                              },
                                                              FileName = cf.FileName,
                                                              DocumentPath = cf.DocumentPath,
                                                              IsDormant = false,//cf.IsDormant.Value,
                                                              LastModifiedBy = cf.UpdateDate == null ? cf.CreateBy : cf.UpdateBy,
                                                              LastModifiedDate = cf.UpdateDate == null ? cf.CreateDate : cf.UpdateDate.Value
                                                          }).Concat(from p in a.TransactionDealDocuments
                                                                    where p.PurposeID != 2 && p.IsDeleted.Equals(false)
                                                                    select new ReviseDealDocumentModel()
                                                                    {
                                                                        UnderlyingID = 0,
                                                                        ID = p.TransactionDocumentID,
                                                                        Type = new DocumentTypeModel()
                                                                        {
                                                                            ID = p.DocumentType.DocTypeID,
                                                                            Name = p.DocumentType.DocTypeName,
                                                                            Description = p.DocumentType.DocTypeDescription,
                                                                            LastModifiedBy = p.DocumentType.UpdateDate == null ? p.DocumentType.CreateBy : p.DocumentType.UpdateBy,
                                                                            LastModifiedDate = p.DocumentType.UpdateDate == null ? p.DocumentType.CreateDate : p.DocumentType.UpdateDate.Value
                                                                        },
                                                                        Purpose = new DocumentPurposeModel()
                                                                        {
                                                                            ID = p.DocumentPurpose.PurposeID,
                                                                            Name = p.DocumentPurpose.PurposeName,
                                                                            Description = p.DocumentPurpose.PurposeDescription,
                                                                            LastModifiedBy = p.DocumentPurpose.UpdateDate == null ? p.DocumentPurpose.CreateBy : p.DocumentPurpose.UpdateBy,
                                                                            LastModifiedDate = p.DocumentPurpose.UpdateDate == null ? p.DocumentPurpose.CreateDate : p.DocumentPurpose.UpdateDate.Value
                                                                        },
                                                                        FileName = p.Filename,
                                                                        DocumentPath = p.DocumentPath,
                                                                        IsDormant = false,//p.IsDormant.Value,
                                                                        LastModifiedBy = p.UpdateDate == null ? p.CreateBy : p.UpdateBy,
                                                                        LastModifiedDate = p.UpdateDate == null ? p.CreateDate : p.UpdateDate.Value
                                                                    }).ToList(),
                                   CreateDate = a.CreatedDate,
                                   LastModifiedBy = a.UpdateDate == null ? a.CreatedBy : a.UpdateBy,
                                   LastModifiedDate = a.UpdateDate == null ? a.CreatedDate : a.UpdateDate.Value,

                                   DealUnderlying = context.UnderlyingDocuments.Where(y => a.UnderlyingCodeID == (y.UnderlyingDocID)).Select(x => new UnderlyingDocModel()
                                   {
                                       ID = x.UnderlyingDocID,
                                       Name = x.UnderlyingDocName,
                                       Code = x.UnderlyingDocCode,
                                       Description = x.UnderlyingDocCode.Equals("999") ? a.OtherUnderlying : x.UnderlyingDocName
                                   }).FirstOrDefault(),

                                   bookunderlyingamount = a.UnderlyingAmount,
                                   bookunderlyingcode = a.UnderlyingCodeID,
                                   bookunderlyingcurrency = a.UnderlyingCurrencyID,
                                   bookunderlyingcurrencydesc = a.OtherUnderlying,
                                   IsOtherAccountNumber = a.IsOtherAccountNumber != null ? a.IsOtherAccountNumber : false,
                                   IsJointAccount = a.IsJointAccount.HasValue ? a.IsJointAccount.Value : false
                                   //OtherUnderlying = a.UnderlyingDocCode.Equals("999") ?  a.OtherUnderlying : b.UnderlyingDocName

                               }).SingleOrDefault();


                if (transaction != null)
                {
                    long TransactionDealID = transaction.ID;
                    string accountNumber = transaction.AccountNumber;
                    long underlyingcode = Convert.ToInt32(transaction.bookunderlyingcode);

                    string cif = transaction.CIF;
                    transaction.bookunderlyingdoccode = transaction.DealUnderlying != null ? transaction.DealUnderlying.Code : "";
                    transaction.AccountCurrencyCode = transaction.Account != null ? transaction.Account.Currency.Code : "";
                    ICustomerRepository customerRepo = new CustomerRepository();
                    CustomerModel customer = new CustomerModel();
                    bool isUnderlying = context.TransactionDealUnderlyings.Any(x => x.TransactionDealID.Equals(TransactionDealID));

                    var deal = context.TransactionDeals.Where(a => a.TransactionDealID.Equals(TransactionDealID)).Select(a => a).FirstOrDefault();

                    if (isUnderlying)
                    {

                        if (customerRepo.GetCustomerByTransactionReviseDealMaker(TransactionDealID, ref customer, ref message))
                        {
                            transaction.Customer = customer;
                        }
                    }
                    else
                    {
                        if (customerRepo.GetCustomerByCIF(cif, ref customer, ref message))
                        {
                            transaction.Customer = customer;
                        }
                    }

                    if (deal.IsOtherAccountNumber != null && deal.IsOtherAccountNumber == true)
                    {
                        CustomerAccountModel Account = new CustomerAccountModel();
                        transaction.Account = Account;
                        transaction.OtherAccountNumber = deal.OtherAccountNumber;
                    }
                    else
                    {
                        /*CustomerAccountModel Account = new CustomerAccountModel();
                        Account.AccountNumber = deal.CustomerAccount.AccountNumber;
                        Account.LastModifiedBy = deal.CustomerAccount.UpdateDate == null ? deal.CustomerAccount.CreateBy : deal.CustomerAccount.UpdateBy;
                        Account.LastModifiedDate = deal.CustomerAccount.UpdateDate == null ? deal.CustomerAccount.CreateDate : deal.CustomerAccount.UpdateDate.Value;

                        transaction.Account = Account;*/
                        if (deal.AccountNumber != null)
                        {
                            transaction.Account = (from j in context.SP_GETJointAccount(cif)
                                                   where j.AccountNumber.Equals(deal.AccountNumber)
                                                   select new CustomerAccountModel
                                                   {
                                                       AccountNumber = j.AccountNumber,
                                                       Currency = new CurrencyModel
                                                       {
                                                           ID = j.CurrencyID.Value,
                                                           Code = j.CurrencyCode,
                                                           Description = j.CurrencyDescription
                                                       },
                                                       CustomerName = j.CustomerName,
                                                       IsJointAccount = j.IsJointAccount
                                                   }).FirstOrDefault();

                            transaction.AccountNumber = deal.AccountNumber;
                        }
                    }

                    CurrencyModel AccountCurrency = new CurrencyModel();
                    AccountCurrency.ID = deal.DebitCurrency.CurrencyID;
                    AccountCurrency.Code = deal.DebitCurrency.CurrencyCode;
                    AccountCurrency.Description = deal.DebitCurrency.CurrencyDescription;

                    transaction.DebitCurrency = AccountCurrency;
                    transaction.AccountCurrencyCode = AccountCurrency.Code;
                    /*if (transaction.Customer != null)
                    {
                        transaction.Customer.Accounts = customer.Accounts.Where(x => x.Currency.Code.Equals(AccountCurrency.Code)).Select(y => y).ToList();
                    } */

                    customerRepo.Dispose();
                    customer = null;

                    /*     if (customerRepo.GetCustomerByCIF(cif, ref customer, ref message))
                           {
                               transaction.Customer = customer;
                               transaction.AccountCurrencyCode = customer.Accounts.Where(x => x.AccountNumber.Equals(accountNumber)).Select(y => y.Currency.Code).FirstOrDefault();
                               transaction.bookunderlyingdoccode = underlyingdoccode;

                           }
                           customerRepo.Dispose();
                           customer = null;  */
                }


                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }

        public bool GetTransactionDealDetails(Guid instanceID, ref TransactionDealDetailModel transaction, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                context.Transactions.AsNoTracking();

                transaction = (from a in context.TransactionDeals
                               where a.WorkflowInstanceID.Value.Equals(instanceID)
                               select new TransactionDealDetailModel
                               {
                                   ID = a.TransactionDealID,
                                   CIF = a.CIF,
                                   WorkflowInstanceID = a.WorkflowInstanceID.Value,
                                   ApplicationID = a.ApplicationID,
                                   TradeDate = a.TradeDate,
                                   ValueDate = a.ValueDate,
                                   ProductType = new ProductTypeModel()
                                   {
                                       ID = a.ProductType.ProductTypeID,
                                       Code = a.ProductType.ProductTypeCode,
                                       Description = a.ProductType.ProductTypeDesc,
                                       IsFlowValas = a.ProductType.IsFlowValas
                                   },
                                   Rate = a.Rate,
                                   StatementLetter = new StatementLetterModel()
                                   {
                                       ID = a.StatementLetter.StatementLetterID,
                                       Name = a.StatementLetter.StatementLetterName
                                   },
                                   TZNumber = a.TZRef,
                                   Currency = new CurrencyModel()
                                   {
                                       ID = a.Currency.CurrencyID,
                                       Code = a.Currency.CurrencyCode,
                                       Description = a.Currency.CurrencyDescription,
                                       LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                       LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                   },
                                   Amount = a.Amount,
                                   AmountUSD = a.AmountUSD,
                                   AccountNumber = a.AccountNumber,
                                   IsResident = a.IsResident.HasValue ? a.IsResident.Value : false,
                                   IsTMO = a.IsTMO.HasValue ? a.IsTMO.Value : false,
                                   NB = a.NB,
                                   SwapType = a.SwapType,
                                   SwapTZDealNo = a.SwapTZDealNo,
                                   IsNettingTransaction = a.IsNettingTransaction.HasValue ? a.IsNettingTransaction.Value : false,
                                   //Account = new CustomerAccountModel()
                                   //{
                                   //    AccountNumber = a.CustomerAccount.AccountNumber,
                                   //    Currency = new CurrencyModel()
                                   //    {
                                   //        ID = a.CustomerAccount.Currency.CurrencyID,
                                   //        Code = a.CustomerAccount.Currency.CurrencyCode,
                                   //        Description = a.CustomerAccount.Currency.CurrencyDescription,
                                   //        LastModifiedBy = a.CustomerAccount.Currency.UpdateDate == null ? a.CustomerAccount.Currency.CreateBy : a.CustomerAccount.Currency.UpdateBy,
                                   //        LastModifiedDate = a.CustomerAccount.Currency.UpdateDate == null ? a.CustomerAccount.Currency.CreateDate : a.CustomerAccount.Currency.UpdateDate.Value
                                   //    },
                                   //    LastModifiedBy = a.CustomerAccount.UpdateDate == null ? a.CustomerAccount.CreateBy : a.CustomerAccount.UpdateBy,
                                   //    LastModifiedDate = a.CustomerAccount.UpdateDate == null ? a.CustomerAccount.CreateDate : a.CustomerAccount.UpdateDate.Value
                                   //},
                                   Documents = a.TransactionDealDocuments.Where(x => x.IsDeleted.Equals(false)).Select(x => new TransactionDealDocumentModel()
                                   {
                                       ID = x.TransactionDocumentID,
                                       Type = new DocumentTypeModel()
                                       {
                                           ID = x.DocumentType.DocTypeID,
                                           Name = x.DocumentType.DocTypeName,
                                           Description = x.DocumentType.DocTypeDescription,
                                           LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                           LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                       },
                                       Purpose = new DocumentPurposeModel()
                                       {
                                           ID = x.DocumentPurpose.PurposeID,
                                           Name = x.DocumentPurpose.PurposeName,
                                           Description = x.DocumentPurpose.PurposeDescription,
                                           LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                           LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                       },
                                       FileName = x.Filename,
                                       DocumentPath = x.DocumentPath,
                                       LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                       LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                                   }).ToList(),

                                   CreateDate = a.CreatedDate,
                                   LastModifiedBy = a.UpdateDate == null ? a.CreatedBy : a.UpdateBy,
                                   LastModifiedDate = a.UpdateDate == null ? a.CreatedDate : a.UpdateDate.Value,

                                   DealUnderlying = context.UnderlyingDocuments.Where(y => a.UnderlyingCodeID == (y.UnderlyingDocID)).Select(x => new UnderlyingDocModel()
                                   {
                                       ID = x.UnderlyingDocID,
                                       Name = x.UnderlyingDocName,
                                       Code = x.UnderlyingDocCode,
                                       Description = x.UnderlyingDocCode.Equals("999") ? a.OtherUnderlying : x.UnderlyingDocName
                                   }).FirstOrDefault(),

                                   bookunderlyingamount = a.UnderlyingAmount,
                                   bookunderlyingcode = a.UnderlyingCodeID,
                                   bookunderlyingcurrency = a.UnderlyingCurrencyID,
                                   bookunderlyingcurrencydesc = a.OtherUnderlying,
                                   IsOtherAccountNumber = a.IsOtherAccountNumber != null ? a.IsOtherAccountNumber : false
                                   //OtherUnderlying = a.UnderlyingDocCode.Equals("999") ?  a.OtherUnderlying : b.UnderlyingDocName

                               }).SingleOrDefault();


                if (transaction != null)
                {
                    long TransactionDealID = transaction.ID;
                    string accountNumber = transaction.AccountNumber;
                    long underlyingcode = Convert.ToInt32(transaction.bookunderlyingcode);

                    string cif = transaction.CIF;
                    transaction.bookunderlyingdoccode = transaction.DealUnderlying != null ? transaction.DealUnderlying.Code : "";
                    transaction.AccountCurrencyCode = transaction.Account != null ? transaction.Account.Currency.Code : "";
                    ICustomerRepository customerRepo = new CustomerRepository();
                    CustomerModel customer = new CustomerModel();
                    bool isUnderlying = context.TransactionDealUnderlyings.Any(x => x.TransactionDealID.Equals(TransactionDealID));

                    var deal = context.TransactionDeals.Where(a => a.TransactionDealID.Equals(TransactionDealID)).Select(a => a).FirstOrDefault();

                    if (isUnderlying)
                    {
                        if (customerRepo.GetCustomerByTransactionDeal(TransactionDealID, ref customer, ref message))
                        {
                            transaction.Customer = customer;
                        }
                    }
                    else
                    {
                        if (customerRepo.GetCustomerByCIF(cif, ref customer, ref message))
                        {
                            transaction.Customer = customer;
                            transaction.Customer.Underlyings = null;
                        }

                    }

                    if (deal.IsOtherAccountNumber != null && deal.IsOtherAccountNumber == true)
                    {
                        CustomerAccountModel Account = new CustomerAccountModel();
                        transaction.Account = Account;
                        transaction.OtherAccountNumber = deal.OtherAccountNumber;
                    }
                    else
                    {
                        transaction.Account = (from j in context.SP_GETJointAccount(cif)
                                               where j.AccountNumber.Equals(accountNumber)
                                               select new CustomerAccountModel
                                               {
                                                   AccountNumber = j.AccountNumber,
                                                   Currency = new CurrencyModel
                                                   {
                                                       ID = j.CurrencyID.Value,
                                                       Code = j.CurrencyCode,
                                                       Description = j.CurrencyDescription
                                                   },
                                                   CustomerName = j.CustomerName,
                                                   IsJointAccount = j.IsJointAccount
                                               }).FirstOrDefault();
                        transaction.AccountNumber = deal.AccountNumber;
                        /*CustomerAccountModel Account = new CustomerAccountModel();
                        Account.AccountNumber = deal.CustomerAccount.AccountNumber;
                        Account.LastModifiedBy = deal.CustomerAccount.UpdateDate == null ? deal.CustomerAccount.CreateBy : deal.CustomerAccount.UpdateBy;
                        Account.LastModifiedDate = deal.CustomerAccount.UpdateDate == null ? deal.CustomerAccount.CreateDate : deal.CustomerAccount.UpdateDate.Value;

                        transaction.Account = Account;
                        transaction.AccountNumber = deal.AccountNumber; */
                    }

                    CurrencyModel AccountCurrency = new CurrencyModel();
                    AccountCurrency.ID = deal.DebitCurrency.CurrencyID;
                    AccountCurrency.Code = deal.DebitCurrency.CurrencyCode;
                    AccountCurrency.Description = deal.DebitCurrency.CurrencyDescription;

                    transaction.DebitCurrency = AccountCurrency;
                    transaction.AccountCurrencyCode = AccountCurrency.Code;
                    /* if (transaction.Customer != null)
                     {
                         transaction.Customer.Accounts = (from j in context.SP_GETJointAccount(cif).Where(x => x.AccountNumber == accountNumber)
                                                          select new CustomerAccountModel
                                                          {
                                                              AccountNumber = j.AccountNumber,
                                                              Currency = new CurrencyModel
                                                              {
                                                                  ID = j.CurrencyID.Value,
                                                                  Code = j.CurrencyCode,
                                                                  Description = j.CurrencyDescription
                                                              },
                                                              CustomerName = j.CustomerName,
                                                              IsJointAccount = j.IsJointAccount
                                                          }).ToList(); 
                             //customer.Accounts.Where(x => x.Currency.Code.Equals(AccountCurrency.Code)).Select(y => y).ToList();
                     } */

                    customerRepo.Dispose();
                    customer = null;


                }


                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);

            GC.Collect();
            GC.SuppressFinalize(this);
        }

        public bool GetTransactionDeal(ref IList<TransactionDealModel> TransactionDeal, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    TransactionDeal = (from a in context.TransactionDeals
                                       join b in context.Customers on a.CIF equals b.CIF
                                       //join c in context.Products on a.ProductID equals c.ProductID
                                       join d in context.Currencies on a.CurrencyID equals d.CurrencyID
                                       select new TransactionDealModel
                                       {
                                           ApplicationID = a.ApplicationID,
                                           CIF = a.CIF,
                                           CustomerName = a.Customer.CustomerName,
                                           Product = "FX",
                                           CurrencyDesc = a.Currency.CurrencyDescription,
                                           Amount = a.Amount,
                                           AmountUSD = a.AmountUSD,
                                           TransactionStatus = "Submit By " + a.CreatedBy,
                                           UpdateBy = a.UpdateBy,
                                           UpdateDate = a.UpdateDate
                                       }).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetTransactionDealById(long transactionDealID, ref TransactionDealDetailModel data, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                data = (from a in context.TransactionDeals
                        where a.TransactionDealID.Equals(transactionDealID)
                        select new TransactionDealDetailModel
                        {
                            ID = a.TransactionDealID,
                            CreateBy = a.CreatedBy,
                            ApplicationID = a.ApplicationID,
                            CIF = a.CIF,
                            TradeDate = a.TradeDate,
                            ValueDate = a.ValueDate,
                            TZReference = a.TZRef,
                            bookunderlyingamount = a.UnderlyingAmount,
                            bookunderlyingcode = a.UnderlyingCodeID,
                            bookunderlyingcurrency = a.UnderlyingCurrencyID,
                            utilizationAmount = a.UtilizedAmount,
                            OtherUnderlying = a.OtherUnderlying,
                            IsFxTransaction = a.IsFxTransaction,
                            ProductType = new ProductTypeModel()
                            {
                                ID = a.ProductType.ProductTypeID,
                                Code = a.ProductType.ProductTypeCode,
                                Description = a.ProductType.ProductTypeDesc,
                                IsFlowValas = a.ProductType.IsFlowValas
                            },
                            Rate = a.Rate,
                            StatementLetter = new StatementLetterModel()
                            {
                                ID = a.StatementLetter.StatementLetterID,
                                Name = a.StatementLetter.StatementLetterName
                            },
                            TZNumber = a.TZRef,
                            Currency = new CurrencyModel()
                            {
                                ID = a.Currency.CurrencyID,
                                Code = a.Currency.CurrencyCode,
                                Description = a.Currency.CurrencyDescription,
                                LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                            },
                            Amount = a.Amount,
                            AmountUSD = a.AmountUSD,
                            IsBookDeal = a.IsBookDeal.HasValue ? a.IsBookDeal.Value : false,
                            IsJointAccount = a.IsJointAccount.HasValue ? a.IsJointAccount.Value : false,
                            TransactionStatus = a.TransactionStatus,
                            IsResident = a.IsResident.HasValue ? a.IsResident.Value : false,
                            IsTMO = a.IsTMO.HasValue ? a.IsTMO.Value : false,
                            NB = a.NB,
                            SwapType = a.SwapType,
                            SwapTZDealNo = a.SwapTZDealNo,
                            IsNettingTransaction = a.IsNettingTransaction.HasValue ? a.IsNettingTransaction.Value : false,
                            Documents = a.TransactionDealDocuments.Where(x => x.IsDeleted.Equals(false)).Select(x => new TransactionDealDocumentModel()
                            {
                                ID = x.TransactionDocumentID,
                                Type = new DocumentTypeModel()
                                {
                                    ID = x.DocumentType.DocTypeID,
                                    Name = x.DocumentType.DocTypeName,
                                    Description = x.DocumentType.DocTypeDescription,
                                    LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                    LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                },
                                Purpose = new DocumentPurposeModel()
                                {
                                    ID = x.DocumentPurpose.PurposeID,
                                    Name = x.DocumentPurpose.PurposeName,
                                    Description = x.DocumentPurpose.PurposeDescription,
                                    LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                    LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                },
                                FileName = x.Filename,
                                DocumentPath = x.DocumentPath,
                                LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                            }).ToList(),
                            /*ReviseDealDocuments = (from cu in a.Customer.CustomerUnderlyings.Where(x => x.IsDeleted.Equals(false))
                                                   join cm in context.CustomerUnderlyingMappings.Where(x => x.IsDeleted.Equals(false)) on cu.UnderlyingID equals cm.UnderlyingID
                                                   join cf in context.CustomerUnderlyingFiles.Where(x => x.IsDeleted.Equals(false)) on cm.UnderlyingFileID equals cf.UnderlyingFileID
                                                   join trans in context.TransactionDealUnderlyings.Where(x => x.TransactionDealID == transactionDealID && x.IsDeleted == false) on cu.UnderlyingID equals trans.UnderlyingID into grp
                                                   from subTrans in grp.DefaultIfEmpty()
                                                   where (cu.ExpiredDate >= DateTime.Now)
                                                   select new ReviseDealDocumentModel()
                                                   {
                                                       UnderlyingID = cu.UnderlyingID,
                                                       ID = cf.UnderlyingFileID,
                                                       Type = new DocumentTypeModel()
                                                       {
                                                           ID = cf.DocumentType.DocTypeID,
                                                           Name = cf.DocumentType.DocTypeName,
                                                           Description = cf.DocumentType.DocTypeDescription,
                                                           LastModifiedBy = cf.DocumentType.UpdateDate == null ? cf.DocumentType.CreateBy : cf.DocumentType.UpdateBy,
                                                           LastModifiedDate = cf.DocumentType.UpdateDate == null ? cf.DocumentType.CreateDate : cf.DocumentType.UpdateDate.Value
                                                       },
                                                       Purpose = new DocumentPurposeModel()
                                                       {
                                                           ID = cf.DocumentPurpose.PurposeID,
                                                           Name = cf.DocumentPurpose.PurposeName,
                                                           Description = cf.DocumentPurpose.PurposeDescription,
                                                           LastModifiedBy = cf.DocumentPurpose.UpdateDate == null ? cf.DocumentPurpose.CreateBy : cf.DocumentPurpose.UpdateBy,
                                                           LastModifiedDate = cf.DocumentPurpose.UpdateDate == null ? cf.DocumentPurpose.CreateDate : cf.DocumentPurpose.UpdateDate.Value
                                                       },
                                                       FileName = cf.FileName,
                                                       DocumentPath = cf.DocumentPath,
                                                       IsDormant = false,//cf.IsDormant.Value,
                                                       LastModifiedBy = cf.UpdateDate == null ? cf.CreateBy : cf.UpdateBy,
                                                       LastModifiedDate = cf.UpdateDate == null ? cf.CreateDate : cf.UpdateDate.Value
                                                   }).Concat(from p in a.TransactionDealDocuments
                                                             where p.PurposeID != 2 && p.IsDeleted.Equals(false) && a.TransactionDealID == transactionDealID
                                                             select new ReviseDealDocumentModel()
                                                             {
                                                                 UnderlyingID = 0,
                                                                 ID = p.TransactionDocumentID,
                                                                 Type = new DocumentTypeModel()
                                                                 {
                                                                     ID = p.DocumentType.DocTypeID,
                                                                     Name = p.DocumentType.DocTypeName,
                                                                     Description = p.DocumentType.DocTypeDescription,
                                                                     LastModifiedBy = p.DocumentType.UpdateDate == null ? p.DocumentType.CreateBy : p.DocumentType.UpdateBy,
                                                                     LastModifiedDate = p.DocumentType.UpdateDate == null ? p.DocumentType.CreateDate : p.DocumentType.UpdateDate.Value
                                                                 },
                                                                 Purpose = new DocumentPurposeModel()
                                                                 {
                                                                     ID = p.DocumentPurpose.PurposeID,
                                                                     Name = p.DocumentPurpose.PurposeName,
                                                                     Description = p.DocumentPurpose.PurposeDescription,
                                                                     LastModifiedBy = p.DocumentPurpose.UpdateDate == null ? p.DocumentPurpose.CreateBy : p.DocumentPurpose.UpdateBy,
                                                                     LastModifiedDate = p.DocumentPurpose.UpdateDate == null ? p.DocumentPurpose.CreateDate : p.DocumentPurpose.UpdateDate.Value
                                                                 },
                                                                 FileName = p.Filename,
                                                                 DocumentPath = p.DocumentPath,
                                                                 IsDormant = false,//p.IsDormant.Value,
                                                                 LastModifiedBy = p.UpdateDate == null ? p.CreateBy : p.UpdateBy,
                                                                 LastModifiedDate = p.UpdateDate == null ? p.CreateDate : p.UpdateDate.Value
                                                             }).ToList(), */
                            CreateDate = a.CreatedDate,
                            LastModifiedBy = a.UpdateDate == null ? a.CreatedBy : a.UpdateBy,
                            LastModifiedDate = a.UpdateDate == null ? a.CreatedDate : a.UpdateDate.Value,
                            IsOtherAccountNumber = a.IsOtherAccountNumber == null ? false : a.IsOtherAccountNumber


                        }).SingleOrDefault();

                if (data != null)
                {
                    var deal = context.TransactionDeals.Where(a => a.TransactionDealID.Equals(transactionDealID)).Select(a => a).FirstOrDefault();
                    var TransactionDealID = deal.TransactionDealID;
                    var cif = deal.CIF;
                    string accountNumber = deal.AccountNumber;
                    ICustomerRepository customerRepo = new CustomerRepository();
                    CustomerModel customer = new CustomerModel();
                    bool isUnderlying = context.TransactionDealUnderlyings.Any(x => x.TransactionDealID.Equals(TransactionDealID));


                    if (isUnderlying)
                    {
                        if (customerRepo.GetCustomerByTransactionDeal(TransactionDealID, ref customer, ref message))
                        {
                            data.Customer = customer;
                        }
                    }
                    else
                    {
                        if (customerRepo.GetCustomerByCIF(cif, ref customer, ref message))
                        {
                            data.Customer = customer;
                            data.Customer.Underlyings = null;
                        }
                    }


                    var jointAccouts = data.Customer.Accounts.Where(x => x.IsJointAccount.Equals(true)).Select(x => x.AccountNumber).ToList();

                    data.ReviseDealDocuments = (from cu in context.CustomerUnderlyings
                                                join cm in context.CustomerUnderlyingMappings on cu.UnderlyingID equals cm.UnderlyingID
                                                join cf in context.CustomerUnderlyingFiles on cm.UnderlyingFileID equals cf.UnderlyingFileID
                                                where cu.IsDeleted.Equals(false) && cm.IsDeleted.Equals(false) && cf.IsDeleted.Equals(false) &&
                                                (cu.CIF.Equals(cif) || jointAccouts.Contains(cu.AccountNumber))
                                                select new ReviseDealDocumentModel()
                                                {
                                                    UnderlyingID = cu.UnderlyingID,
                                                    ID = cf.UnderlyingFileID,
                                                    Type = new DocumentTypeModel()
                                                    {
                                                        ID = cf.DocumentType.DocTypeID,
                                                        Name = cf.DocumentType.DocTypeName,
                                                        Description = cf.DocumentType.DocTypeDescription,
                                                        LastModifiedBy = cf.DocumentType.UpdateDate == null ? cf.DocumentType.CreateBy : cf.DocumentType.UpdateBy,
                                                        LastModifiedDate = cf.DocumentType.UpdateDate == null ? cf.DocumentType.CreateDate : cf.DocumentType.UpdateDate.Value
                                                    },
                                                    Purpose = new DocumentPurposeModel()
                                                    {
                                                        ID = cf.DocumentPurpose.PurposeID,
                                                        Name = cf.DocumentPurpose.PurposeName,
                                                        Description = cf.DocumentPurpose.PurposeDescription,
                                                        LastModifiedBy = cf.DocumentPurpose.UpdateDate == null ? cf.DocumentPurpose.CreateBy : cf.DocumentPurpose.UpdateBy,
                                                        LastModifiedDate = cf.DocumentPurpose.UpdateDate == null ? cf.DocumentPurpose.CreateDate : cf.DocumentPurpose.UpdateDate.Value
                                                    },
                                                    FileName = cf.FileName,
                                                    DocumentPath = cf.DocumentPath,
                                                    IsDormant = false,//cf.IsDormant.Value,
                                                    LastModifiedBy = cf.UpdateDate == null ? cf.CreateBy : cf.UpdateBy,
                                                    LastModifiedDate = cf.UpdateDate == null ? cf.CreateDate : cf.UpdateDate.Value
                                                }).Concat(from p in context.TransactionDealDocuments.Where(x => x.TransactionDealID.Equals(TransactionDealID))
                                                          where p.PurposeID != 2 && p.IsDeleted.Equals(false)
                                                          select new ReviseDealDocumentModel()
                                                          {
                                                              UnderlyingID = 0,
                                                              ID = p.TransactionDocumentID,
                                                              Type = new DocumentTypeModel()
                                                              {
                                                                  ID = p.DocumentType.DocTypeID,
                                                                  Name = p.DocumentType.DocTypeName,
                                                                  Description = p.DocumentType.DocTypeDescription,
                                                                  LastModifiedBy = p.DocumentType.UpdateDate == null ? p.DocumentType.CreateBy : p.DocumentType.UpdateBy,
                                                                  LastModifiedDate = p.DocumentType.UpdateDate == null ? p.DocumentType.CreateDate : p.DocumentType.UpdateDate.Value
                                                              },
                                                              Purpose = new DocumentPurposeModel()
                                                              {
                                                                  ID = p.DocumentPurpose.PurposeID,
                                                                  Name = p.DocumentPurpose.PurposeName,
                                                                  Description = p.DocumentPurpose.PurposeDescription,
                                                                  LastModifiedBy = p.DocumentPurpose.UpdateDate == null ? p.DocumentPurpose.CreateBy : p.DocumentPurpose.UpdateBy,
                                                                  LastModifiedDate = p.DocumentPurpose.UpdateDate == null ? p.DocumentPurpose.CreateDate : p.DocumentPurpose.UpdateDate.Value
                                                              },
                                                              FileName = p.Filename,
                                                              DocumentPath = p.DocumentPath,
                                                              IsDormant = false,//p.IsDormant.Value,
                                                              LastModifiedBy = p.UpdateDate == null ? p.CreateBy : p.UpdateBy,
                                                              LastModifiedDate = p.UpdateDate == null ? p.CreateDate : p.UpdateDate.Value
                                                          }).ToList();

                    if (deal.IsOtherAccountNumber != null && deal.IsOtherAccountNumber == true)
                    {
                        CustomerAccountModel Account = new CustomerAccountModel();
                        data.Account = Account;
                        data.OtherAccountNumber = deal.OtherAccountNumber;
                    }
                    else
                    {
                        CustomerAccountModel Account = new CustomerAccountModel();
                        Account.AccountNumber = deal.CustomerAccount.AccountNumber;
                        Account.LastModifiedBy = deal.CustomerAccount.UpdateDate == null ? deal.CustomerAccount.CreateBy : deal.CustomerAccount.UpdateBy;
                        Account.LastModifiedDate = deal.CustomerAccount.UpdateDate == null ? deal.CustomerAccount.CreateDate : deal.CustomerAccount.UpdateDate.Value;

                        data.Account = Account;
                        data.AccountNumber = deal.AccountNumber;
                    }

                    CurrencyModel AccountCurrency = new CurrencyModel();
                    AccountCurrency.ID = deal.DebitCurrency.CurrencyID;
                    AccountCurrency.Code = deal.DebitCurrency.CurrencyCode;
                    AccountCurrency.Description = deal.DebitCurrency.CurrencyDescription;

                    data.DebitCurrency = AccountCurrency;
                    data.AccountCurrencyCode = AccountCurrency.Code;

                    customerRepo.Dispose();
                    customer = null;

                }
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }
        public bool GetTransactionTMOById(long transactionDealID, ref TransactionTMOModel data, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                data = (from a in context.TransactionDeals
                        where a.TransactionDealID.Equals(transactionDealID)
                        select new TransactionTMOModel
                        {
                            ID = a.TransactionDealID,
                            ApplicationID = a.ApplicationID,
                            CIF = a.CIF,
                            TradeDate = a.TradeDate,
                            ValueDate = a.ValueDate,
                            TZReference = a.TZRef,
                            OtherUnderlying = a.OtherUnderlying,
                            Rate = a.Rate,
                            StatementLetter = new StatementLetterModel()
                            {
                                ID = a.StatementLetter.StatementLetterID,
                                Name = a.StatementLetter.StatementLetterName
                            },
                            ProductType = new ProductTypeModel()
                            {
                                ID = a.ProductType.ProductTypeID,
                                Code = a.ProductType.ProductTypeCode,
                                Description = a.ProductType.ProductTypeDesc,
                                IsFlowValas = a.ProductType.IsFlowValas
                            },
                            BuyAmount = a.BuyAmount,
                            SellAmount = a.SellAmount,
                            AmountUSD = a.AmountUSD,
                            TransactionStatus = a.TransactionStatus,
                            IsUnderlyingCompleted = a.IsUnderlyingCompleted,
                            SubmissionDateSL = a.ActualSubmissionDateSL,
                            SubmissionDateInstruction = a.ActualSubmissionDateInstruction,
                            SubmissionDateUnderlying = a.ActualSubmissionDateUnderlying,
                            UnderlyingCodeID = a.UnderlyingCodeID,
                            Remarks = a.Remarks,
                            IsResident = a.IsResident.HasValue ? a.IsResident.Value : false,
                            IsJointAccount = a.IsJointAccount.HasValue ? a.IsJointAccount.Value : false,
                            IsCanceled = a.IsCanceled.HasValue ? a.IsCanceled.Value : false,
                            NB = a.NB,
                            SwapType = a.SwapType,
                            SwapTZDealNo = a.SwapTZDealNo,
                            IsIPE = a.IsIPE.HasValue ? a.IsIPE : false,
                            IsNettingTransaction = a.IsNettingTransaction.HasValue ? a.IsNettingTransaction.Value : false,
                            Documents = a.TransactionDealDocuments.Where(x => x.IsDeleted.Equals(false)).Select(x => new TransactionDealDocumentModel()
                            {
                                ID = x.TransactionDocumentID,
                                Type = new DocumentTypeModel()
                                {
                                    ID = x.DocumentType.DocTypeID,
                                    Name = x.DocumentType.DocTypeName,
                                    Description = x.DocumentType.DocTypeDescription,
                                    LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                    LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                },
                                Purpose = new DocumentPurposeModel()
                                {
                                    ID = x.DocumentPurpose.PurposeID,
                                    Name = x.DocumentPurpose.PurposeName,
                                    Description = x.DocumentPurpose.PurposeDescription,
                                    LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                    LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                },
                                FileName = x.Filename,
                                DocumentPath = x.DocumentPath,
                                LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                            }).ToList(),

                            CreateDate = a.CreatedDate,
                            LastModifiedBy = a.UpdateDate == null ? a.CreatedBy : a.UpdateBy,
                            LastModifiedDate = a.UpdateDate == null ? a.CreatedDate : a.UpdateDate.Value,
                            IsOtherAccountNumber = a.IsOtherAccountNumber == null ? false : a.IsOtherAccountNumber

                        }).SingleOrDefault();

                if (data != null)
                {
                    var deal = context.TransactionDeals.Where(a => a.TransactionDealID.Equals(transactionDealID)).Select(a => a).FirstOrDefault();
                    var TransactionDealID = deal.TransactionDealID;
                    var cif = deal.CIF;
                    string accountNumber = deal.AccountNumber;
                    ICustomerRepository customerRepo = new CustomerRepository();
                    CustomerModel customer = new CustomerModel();
                    #region Agung

                    IsNettingModel IsNetting = (from a in context.TMONettings
                                                join b in context.TMONettingDetails on a.TMONettingID equals b.TMONettingID into zi
                                                from c in zi.DefaultIfEmpty()
                                                where a.TZRef.Equals(deal.TZRef) || a.SwapDealNumber.Equals(deal.TZRef) || c.TZRef.Equals(deal.TZRef) || c.SwapDealNumber.Equals(deal.TZRef)
                                                select new IsNettingModel
                                                {
                                                    ApplicationID = a.ApplicationID != null ? a.ApplicationID : a.ApplicationID,
                                                    TransactionDealID = a.TMONettingID != null ? a.TMONettingID : 0,
                                                    DFNumber = a.TZRef
                                                }).FirstOrDefault();
                    if (IsNetting != null)
                    {
                        long DealID = context.TransactionDeals.Where(a => a.TZRef.Equals(IsNetting.DFNumber)).Select(a => a.TransactionDealID).FirstOrDefault();

                        TransactionDealID = DealID;
                    }
                    #region Get Data Underlying
                    bool isUnderlying = context.TransactionDealUnderlyings.Any(x => x.TransactionDealID.Equals(TransactionDealID) && x.IsDeleted.Equals(false));
                    if (isUnderlying)
                    {
                        if (customerRepo.GetCustomerByTransactionDeal(TransactionDealID, ref customer, ref message))
                        {
                            data.Customer = customer;
                        }
                        var jointAccouts = data.Customer.Accounts.Where(x => x.IsJointAccount.Equals(true)).Select(x => x.AccountNumber).ToList();

                        data.ReviseDealDocuments = (from cu in context.CustomerUnderlyings
                                                    join cm in context.CustomerUnderlyingMappings on cu.UnderlyingID equals cm.UnderlyingID
                                                    join cf in context.CustomerUnderlyingFiles on cm.UnderlyingFileID equals cf.UnderlyingFileID
                                                    where cu.IsDeleted.Equals(false) && cm.IsDeleted.Equals(false) && cf.IsDeleted.Equals(false) &&
                                                    (cu.CIF.Equals(cif) || jointAccouts.Contains(cu.AccountNumber))
                                                    select new ReviseDealDocumentModel()
                                                    {
                                                        UnderlyingID = cu.UnderlyingID,
                                                        ID = cf.UnderlyingFileID,
                                                        Type = new DocumentTypeModel()
                                                        {
                                                            ID = cf.DocumentType.DocTypeID,
                                                            Name = cf.DocumentType.DocTypeName,
                                                            Description = cf.DocumentType.DocTypeDescription,
                                                            LastModifiedBy = cf.DocumentType.UpdateDate == null ? cf.DocumentType.CreateBy : cf.DocumentType.UpdateBy,
                                                            LastModifiedDate = cf.DocumentType.UpdateDate == null ? cf.DocumentType.CreateDate : cf.DocumentType.UpdateDate.Value
                                                        },
                                                        Purpose = new DocumentPurposeModel()
                                                        {
                                                            ID = cf.DocumentPurpose.PurposeID,
                                                            Name = cf.DocumentPurpose.PurposeName,
                                                            Description = cf.DocumentPurpose.PurposeDescription,
                                                            LastModifiedBy = cf.DocumentPurpose.UpdateDate == null ? cf.DocumentPurpose.CreateBy : cf.DocumentPurpose.UpdateBy,
                                                            LastModifiedDate = cf.DocumentPurpose.UpdateDate == null ? cf.DocumentPurpose.CreateDate : cf.DocumentPurpose.UpdateDate.Value
                                                        },
                                                        FileName = cf.FileName,
                                                        DocumentPath = cf.DocumentPath,
                                                        IsDormant = false,//cf.IsDormant.Value,
                                                        LastModifiedBy = cf.UpdateDate == null ? cf.CreateBy : cf.UpdateBy,
                                                        LastModifiedDate = cf.UpdateDate == null ? cf.CreateDate : cf.UpdateDate.Value
                                                    }).Concat(from p in context.TransactionDealDocuments.Where(x => x.TransactionDealID.Equals(TransactionDealID))
                                                              where p.PurposeID != 2 && p.IsDeleted.Equals(false)
                                                              select new ReviseDealDocumentModel()
                                                              {
                                                                  UnderlyingID = 0,
                                                                  ID = p.TransactionDocumentID,
                                                                  Type = new DocumentTypeModel()
                                                                  {
                                                                      ID = p.DocumentType.DocTypeID,
                                                                      Name = p.DocumentType.DocTypeName,
                                                                      Description = p.DocumentType.DocTypeDescription,
                                                                      LastModifiedBy = p.DocumentType.UpdateDate == null ? p.DocumentType.CreateBy : p.DocumentType.UpdateBy,
                                                                      LastModifiedDate = p.DocumentType.UpdateDate == null ? p.DocumentType.CreateDate : p.DocumentType.UpdateDate.Value
                                                                  },
                                                                  Purpose = new DocumentPurposeModel()
                                                                  {
                                                                      ID = p.DocumentPurpose.PurposeID,
                                                                      Name = p.DocumentPurpose.PurposeName,
                                                                      Description = p.DocumentPurpose.PurposeDescription,
                                                                      LastModifiedBy = p.DocumentPurpose.UpdateDate == null ? p.DocumentPurpose.CreateBy : p.DocumentPurpose.UpdateBy,
                                                                      LastModifiedDate = p.DocumentPurpose.UpdateDate == null ? p.DocumentPurpose.CreateDate : p.DocumentPurpose.UpdateDate.Value
                                                                  },
                                                                  FileName = p.Filename,
                                                                  DocumentPath = p.DocumentPath,
                                                                  IsDormant = false,//p.IsDormant.Value,
                                                                  LastModifiedBy = p.UpdateDate == null ? p.CreateBy : p.UpdateBy,
                                                                  LastModifiedDate = p.UpdateDate == null ? p.CreateDate : p.UpdateDate.Value
                                                              }).ToList();
                    }
                    else
                    {
                        long isTransUnderlying = context.Transactions.Where(x => x.TZNumber.Equals(deal.TZRef)).Select(x => x.TransactionID).FirstOrDefault();

                        if (isTransUnderlying > 0)
                        {
                            if (customerRepo.GetCustomerByTransactionDealChecker(0, isTransUnderlying, ref customer, ref message))
                            {
                                data.Customer = customer;
                            }
                            var jointAccouts = data.Customer.Accounts.Where(x => x.IsJointAccount.Equals(true)).Select(x => x.AccountNumber).ToList();

                            data.ReviseDealDocuments = (from cu in context.CustomerUnderlyings
                                                        join cm in context.CustomerUnderlyingMappings on cu.UnderlyingID equals cm.UnderlyingID
                                                        join cf in context.CustomerUnderlyingFiles on cm.UnderlyingFileID equals cf.UnderlyingFileID
                                                        where cu.IsDeleted.Equals(false) && cm.IsDeleted.Equals(false) && cf.IsDeleted.Equals(false) &&
                                                        (cu.CIF.Equals(cif) || jointAccouts.Contains(cu.AccountNumber))
                                                        select new ReviseDealDocumentModel()
                                                        {
                                                            UnderlyingID = cu.UnderlyingID,
                                                            ID = cf.UnderlyingFileID,
                                                            Type = new DocumentTypeModel()
                                                            {
                                                                ID = cf.DocumentType.DocTypeID,
                                                                Name = cf.DocumentType.DocTypeName,
                                                                Description = cf.DocumentType.DocTypeDescription,
                                                                LastModifiedBy = cf.DocumentType.UpdateDate == null ? cf.DocumentType.CreateBy : cf.DocumentType.UpdateBy,
                                                                LastModifiedDate = cf.DocumentType.UpdateDate == null ? cf.DocumentType.CreateDate : cf.DocumentType.UpdateDate.Value
                                                            },
                                                            Purpose = new DocumentPurposeModel()
                                                            {
                                                                ID = cf.DocumentPurpose.PurposeID,
                                                                Name = cf.DocumentPurpose.PurposeName,
                                                                Description = cf.DocumentPurpose.PurposeDescription,
                                                                LastModifiedBy = cf.DocumentPurpose.UpdateDate == null ? cf.DocumentPurpose.CreateBy : cf.DocumentPurpose.UpdateBy,
                                                                LastModifiedDate = cf.DocumentPurpose.UpdateDate == null ? cf.DocumentPurpose.CreateDate : cf.DocumentPurpose.UpdateDate.Value
                                                            },
                                                            FileName = cf.FileName,
                                                            DocumentPath = cf.DocumentPath,
                                                            IsDormant = false,//cf.IsDormant.Value,
                                                            LastModifiedBy = cf.UpdateDate == null ? cf.CreateBy : cf.UpdateBy,
                                                            LastModifiedDate = cf.UpdateDate == null ? cf.CreateDate : cf.UpdateDate.Value
                                                        }).Concat(from p in context.TransactionDocuments.Where(x => x.TransactionID.Equals(isTransUnderlying))
                                                                  where p.PurposeID != 2 && p.IsDeleted.Equals(false)
                                                                  select new ReviseDealDocumentModel()
                                                                  {
                                                                      UnderlyingID = 0,
                                                                      ID = p.TransactionDocumentID,
                                                                      Type = new DocumentTypeModel()
                                                                      {
                                                                          ID = p.DocumentType.DocTypeID,
                                                                          Name = p.DocumentType.DocTypeName,
                                                                          Description = p.DocumentType.DocTypeDescription,
                                                                          LastModifiedBy = p.DocumentType.UpdateDate == null ? p.DocumentType.CreateBy : p.DocumentType.UpdateBy,
                                                                          LastModifiedDate = p.DocumentType.UpdateDate == null ? p.DocumentType.CreateDate : p.DocumentType.UpdateDate.Value
                                                                      },
                                                                      Purpose = new DocumentPurposeModel()
                                                                      {
                                                                          ID = p.DocumentPurpose.PurposeID,
                                                                          Name = p.DocumentPurpose.PurposeName,
                                                                          Description = p.DocumentPurpose.PurposeDescription,
                                                                          LastModifiedBy = p.DocumentPurpose.UpdateDate == null ? p.DocumentPurpose.CreateBy : p.DocumentPurpose.UpdateBy,
                                                                          LastModifiedDate = p.DocumentPurpose.UpdateDate == null ? p.DocumentPurpose.CreateDate : p.DocumentPurpose.UpdateDate.Value
                                                                      },
                                                                      FileName = p.Filename,
                                                                      DocumentPath = p.DocumentPath,
                                                                      IsDormant = false,//p.IsDormant.Value,
                                                                      LastModifiedBy = p.UpdateDate == null ? p.CreateBy : p.UpdateBy,
                                                                      LastModifiedDate = p.UpdateDate == null ? p.CreateDate : p.UpdateDate.Value
                                                                  }).ToList();

                            if (data.ReviseDealDocuments != null)
                            {
                                foreach (ReviseDealDocumentModel Itemdoc in data.ReviseDealDocuments)
                                {
                                    if (Itemdoc.Purpose.Name == "Statement Letter" && data.SubmissionDateSL == null)
                                    {
                                        data.SubmissionDateSL = Itemdoc.LastModifiedDate;
                                    }
                                    if (Itemdoc.Purpose.Name == "Instruction" && data.SubmissionDateInstruction == null)
                                    {
                                        data.SubmissionDateInstruction = Itemdoc.LastModifiedDate;
                                    }
                                    if (Itemdoc.Purpose.Name == "Underlying" && data.SubmissionDateUnderlying == null)
                                    {
                                        data.SubmissionDateUnderlying = Itemdoc.LastModifiedDate;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (customerRepo.GetCustomerByCIF(cif, ref customer, ref message))
                            {
                                data.Customer = customer;
                                data.Customer.Underlyings = null;
                            }
                            var jointAccouts = data.Customer.Accounts.Where(x => x.IsJointAccount.Equals(true)).Select(x => x.AccountNumber).ToList();

                            data.ReviseDealDocuments = (from cu in context.CustomerUnderlyings
                                                        join cm in context.CustomerUnderlyingMappings on cu.UnderlyingID equals cm.UnderlyingID
                                                        join cf in context.CustomerUnderlyingFiles on cm.UnderlyingFileID equals cf.UnderlyingFileID
                                                        where cu.IsDeleted.Equals(false) && cm.IsDeleted.Equals(false) && cf.IsDeleted.Equals(false) &&
                                                        (cu.CIF.Equals(cif) || jointAccouts.Contains(cu.AccountNumber))
                                                        select new ReviseDealDocumentModel()
                                                        {
                                                            UnderlyingID = cu.UnderlyingID,
                                                            ID = cf.UnderlyingFileID,
                                                            Type = new DocumentTypeModel()
                                                            {
                                                                ID = cf.DocumentType.DocTypeID,
                                                                Name = cf.DocumentType.DocTypeName,
                                                                Description = cf.DocumentType.DocTypeDescription,
                                                                LastModifiedBy = cf.DocumentType.UpdateDate == null ? cf.DocumentType.CreateBy : cf.DocumentType.UpdateBy,
                                                                LastModifiedDate = cf.DocumentType.UpdateDate == null ? cf.DocumentType.CreateDate : cf.DocumentType.UpdateDate.Value
                                                            },
                                                            Purpose = new DocumentPurposeModel()
                                                            {
                                                                ID = cf.DocumentPurpose.PurposeID,
                                                                Name = cf.DocumentPurpose.PurposeName,
                                                                Description = cf.DocumentPurpose.PurposeDescription,
                                                                LastModifiedBy = cf.DocumentPurpose.UpdateDate == null ? cf.DocumentPurpose.CreateBy : cf.DocumentPurpose.UpdateBy,
                                                                LastModifiedDate = cf.DocumentPurpose.UpdateDate == null ? cf.DocumentPurpose.CreateDate : cf.DocumentPurpose.UpdateDate.Value
                                                            },
                                                            FileName = cf.FileName,
                                                            DocumentPath = cf.DocumentPath,
                                                            IsDormant = false,//cf.IsDormant.Value,
                                                            LastModifiedBy = cf.UpdateDate == null ? cf.CreateBy : cf.UpdateBy,
                                                            LastModifiedDate = cf.UpdateDate == null ? cf.CreateDate : cf.UpdateDate.Value
                                                        }).Concat(from p in context.TransactionDocuments.Where(x => x.TransactionID.Equals(isTransUnderlying))
                                                                  where p.PurposeID != 2 && p.IsDeleted.Equals(false)
                                                                  select new ReviseDealDocumentModel()
                                                                  {
                                                                      UnderlyingID = 0,
                                                                      ID = p.TransactionDocumentID,
                                                                      Type = new DocumentTypeModel()
                                                                      {
                                                                          ID = p.DocumentType.DocTypeID,
                                                                          Name = p.DocumentType.DocTypeName,
                                                                          Description = p.DocumentType.DocTypeDescription,
                                                                          LastModifiedBy = p.DocumentType.UpdateDate == null ? p.DocumentType.CreateBy : p.DocumentType.UpdateBy,
                                                                          LastModifiedDate = p.DocumentType.UpdateDate == null ? p.DocumentType.CreateDate : p.DocumentType.UpdateDate.Value
                                                                      },
                                                                      Purpose = new DocumentPurposeModel()
                                                                      {
                                                                          ID = p.DocumentPurpose.PurposeID,
                                                                          Name = p.DocumentPurpose.PurposeName,
                                                                          Description = p.DocumentPurpose.PurposeDescription,
                                                                          LastModifiedBy = p.DocumentPurpose.UpdateDate == null ? p.DocumentPurpose.CreateBy : p.DocumentPurpose.UpdateBy,
                                                                          LastModifiedDate = p.DocumentPurpose.UpdateDate == null ? p.DocumentPurpose.CreateDate : p.DocumentPurpose.UpdateDate.Value
                                                                      },
                                                                      FileName = p.Filename,
                                                                      DocumentPath = p.DocumentPath,
                                                                      IsDormant = false,//p.IsDormant.Value,
                                                                      LastModifiedBy = p.UpdateDate == null ? p.CreateBy : p.UpdateBy,
                                                                      LastModifiedDate = p.UpdateDate == null ? p.CreateDate : p.UpdateDate.Value
                                                                  }).ToList();
                        }
                    }
                    #endregion
                    #endregion

                    if (deal.IsOtherAccountNumber != null && deal.IsOtherAccountNumber == true)
                    {
                        CustomerAccountModel Account = new CustomerAccountModel();
                        data.Account = Account;
                        data.OtherAccountNumber = deal.OtherAccountNumber;
                    }
                    else
                    {
                        CustomerAccountModel Account = new CustomerAccountModel();
                        Account.AccountNumber = deal.CustomerAccount.AccountNumber;
                        Account.LastModifiedBy = deal.CustomerAccount.UpdateDate == null ? deal.CustomerAccount.CreateBy : deal.CustomerAccount.UpdateBy;
                        Account.LastModifiedDate = deal.CustomerAccount.UpdateDate == null ? deal.CustomerAccount.CreateDate : deal.CustomerAccount.UpdateDate.Value;

                        data.Account = Account;
                        data.AccountNumber = deal.AccountNumber;
                    }

                    CurrencyModel AccountCurrency = new CurrencyModel();
                    AccountCurrency.ID = deal.DebitCurrency.CurrencyID;
                    AccountCurrency.Code = deal.DebitCurrency.CurrencyCode;
                    AccountCurrency.Description = deal.DebitCurrency.CurrencyDescription;

                    data.DebitCurrency = AccountCurrency;
                    data.AccountCurrencyCode = AccountCurrency.Code;


                    if (deal.CurrencySell != null)
                    {
                        data.SellCurrency = new CurrencyModel()
                        {
                            ID = deal.CurrencySell.CurrencyID,
                            Code = deal.CurrencySell.CurrencyCode,
                            CodeDescription = deal.CurrencySell.CurrencyDescription,
                            Description = deal.CurrencySell.CurrencyDescription,
                            LastModifiedBy = deal.CurrencySell.UpdateDate == null ? deal.CurrencySell.CreateBy : deal.CurrencySell.UpdateBy,
                            LastModifiedDate = deal.CurrencySell.UpdateDate == null ? deal.CurrencySell.CreateDate : deal.CurrencySell.UpdateDate.Value
                        };
                    }

                    if (deal.CurrencyBuy != null)
                    {
                        data.BuyCurrency = new CurrencyModel()
                        {
                            ID = deal.CurrencyBuy.CurrencyID,
                            Code = deal.CurrencyBuy.CurrencyCode,
                            CodeDescription = deal.CurrencyBuy.CurrencyDescription,
                            Description = deal.CurrencyBuy.CurrencyDescription,
                            LastModifiedBy = deal.CurrencyBuy.UpdateDate == null ? deal.CurrencyBuy.CreateBy : deal.CurrencyBuy.UpdateBy,
                            LastModifiedDate = deal.CurrencyBuy.UpdateDate == null ? deal.CurrencyBuy.CreateDate : deal.CurrencyBuy.UpdateDate.Value
                        };
                    }

                    customerRepo.Dispose();
                    customer = null;

                }
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }

        public bool UpdateTransactionDealById(long transactionDealID, TransactionDealDetailModel data, ref string message)
        {
            bool isSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    Entity.TransactionDeal deal = context.TransactionDeals.Where(a => a.TransactionDealID.Equals(transactionDealID)).SingleOrDefault();

                    long transactionID = deal.TransactionDealID;

                    if (deal != null)
                    {
                        deal.ApplicationID = data.ApplicationID;
                        //deal.CIF = data.Customer.CIF;
                        //deal.TradeDate = data.TradeDate;
                        //deal.ValueDate = data.ValueDate;
                        deal.StatementLetterID = data.StatementLetter.ID;
                        //deal.TZRef = data.TZReference;
                        //deal.ProductTypeID = data.ProductType.ID;
                        //deal.Rate = data.Rate;
                        //deal.CurrencyID = data.Currency.ID;
                        //deal.Amount = data.Amount;
                        //deal.AmountUSD = data.AmountUSD;
                        deal.UtilizedAmount = data.AmountUSD;
                        deal.UpdateDate = DateTime.UtcNow;
                        deal.UpdateBy = currentUser.GetCurrentUser().LoginName;
                        deal.UnderlyingCurrencyID = data.bookunderlyingcurrency;
                        deal.UnderlyingCodeID = data.bookunderlyingcode;
                        deal.UnderlyingAmount = data.bookunderlyingamount;
                        deal.IsBookDeal = data.IsBookDeal;
                        deal.OtherUnderlying = data.OtherUnderlying;
                        deal.DebitCurrencyID = data.DebitCurrency.ID;
                        deal.IsJointAccount = data.IsJointAccount.HasValue ? data.IsJointAccount.Value : false;

                        // deal.IsResident = data.IsResident.HasValue ? data.IsResident.Value : false;
                        if (!data.IsBookDeal)
                        {
                            deal.TransactionStatus = "";// show view underlying data   
                            deal.TouchTimeStartDate = data.CreateDate;
                        }
                        if (data.IsOtherAccountNumber != null && data.IsOtherAccountNumber == true)
                        {
                            deal.OtherAccountNumber = data.OtherAccountNumber;
                            deal.AccountNumber = null;
                            deal.IsOtherAccountNumber = true;
                        }
                        else
                        {
                            deal.AccountNumber = data.AccountNumber;
                            deal.OtherAccountNumber = null;
                            deal.IsOtherAccountNumber = false;
                        }


                        context.SaveChanges();

                        // Chandra - Update CheckUnderlyingUtilize
                        if (data.Underlyings != null && data.Underlyings.Count > 0)
                        {
                            decimal availableAmount = data.AmountUSD;
                            for (int i = 0; data.Underlyings.Count > i; i++)
                            {
                                long UnderlyingID = data.Underlyings[i].ID;
                                Entity.CustomerUnderlying customerUnderlying = context.CustomerUnderlyings.Where(a => a.UnderlyingID.Equals(UnderlyingID)).SingleOrDefault();
                                if (customerUnderlying != null)
                                {
                                    decimal tempAvailableAmount = (decimal)customerUnderlying.AvailableAmountUSD;
                                    availableAmount = (decimal)customerUnderlying.AvailableAmountUSD - availableAmount;
                                    customerUnderlying.IsUtilize = true;
                                    customerUnderlying.UpdateDate = DateTime.UtcNow;
                                    customerUnderlying.AvailableAmountUSD = (decimal?)(availableAmount > 0 ? availableAmount : 0);
                                    customerUnderlying.UpdateBy = currentUser.GetCurrentUser().LoginName;
                                    context.SaveChanges();
                                    // set amount for transaction underlying
                                    data.Underlyings[i].USDAmount = tempAvailableAmount - (availableAmount > 0 ? availableAmount : 0);
                                    availableAmount = (availableAmount > 0 ? 0 : Math.Abs(availableAmount));
                                }
                            }
                        }


                        if (data.Underlyings != null && data.Underlyings.Count > 0)
                        {
                            List<long> UnderlyingID = new List<long>();
                            foreach (TransDetailUnderlyingModel underlyingItem in data.Underlyings)
                            {
                                Entity.TransactionDealUnderlying Underlying = new Entity.TransactionDealUnderlying()
                                {
                                    TransactionDealID = transactionDealID,
                                    UnderlyingID = underlyingItem.ID,
                                    Amount = underlyingItem.USDAmount,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                };
                                context.TransactionDealUnderlyings.Add(Underlying);
                                UnderlyingID.Add(underlyingItem.ID);
                            }
                            context.SaveChanges();

                            /*IList<TransactionDealDocumentModel> documents = GetDocumentUnderlying(UnderlyingID);
                            foreach (TransactionDealDocumentModel doc in documents)
                            {
                                data.Documents.Add(doc);
                            }*/
                            IList<TransactionDealDocumentModel> documentsBulk = GetDocumentBulkUnderlying(UnderlyingID);
                            foreach (var item in documentsBulk)
                            {
                                data.Documents.Add(item);
                            }
                        }
                        if (data.Documents.Count > 0)
                        {
                            foreach (var item in data.Documents)
                            {
                                Entity.TransactionDealDocument document = new Entity.TransactionDealDocument()
                                {
                                    TransactionDealID = transactionDealID,
                                    DocTypeID = item.Type.ID,
                                    PurposeID = item.Purpose.ID,
                                    Filename = item.FileName,
                                    DocumentPath = item.DocumentPath,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                };

                                context.TransactionDealDocuments.Add(document);
                            }
                            context.SaveChanges();
                        }
                    }

                    ts.Complete();
                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }

            return isSuccess;
        }

        public bool UpdateTransactionTMOById(long transactionDealID, TransactionTMOModel transaction, ref string message)
        {
            bool isSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    Entity.TransactionDeal TMO = context.TransactionDeals.Where(a => a.TransactionDealID.Equals(transactionDealID)).SingleOrDefault();

                    long dealID = TMO.TransactionDealID;

                    if (TMO != null)
                    {
                        TMO.ApplicationID = transaction.ApplicationID;
                        TMO.CIF = transaction.Customer.CIF;
                        //TMO.TradeDate = transaction.TradeDate;
                        //TMO.ValueDate = transaction.ValueDate;
                        //Tambah Agung
                        TMO.Rounding = transaction.Rounding;
                        //End
                        TMO.StatementLetterID = transaction.StatementLetter.ID;
                        TMO.TZRef = transaction.TZReference;
                        TMO.SellCurrencyID = transaction.SellCurrency != null ? transaction.SellCurrency.ID : default(int?);
                        TMO.BuyCurrencyID = transaction.BuyCurrency != null ? transaction.BuyCurrency.ID : default(int?);
                        TMO.SellAmount = transaction.SellAmount;
                        TMO.BuyAmount = transaction.BuyAmount;
                        TMO.ActualSubmissionDateSL = transaction.SubmissionDateSL;
                        TMO.ActualSubmissionDateInstruction = transaction.SubmissionDateInstruction;
                        TMO.ActualSubmissionDateUnderlying = transaction.SubmissionDateUnderlying;
                        TMO.IsUnderlyingCompleted = transaction.IsUnderlyingCompleted;
                        TMO.IsDraft = transaction.IsDraft;
                        TMO.Rate = transaction.Rate;
                        TMO.AmountUSD = transaction.AmountUSD;
                        TMO.UtilizedAmount = transaction.AmountUSD;
                        TMO.OtherUnderlying = transaction.OtherUnderlying;
                        TMO.UnderlyingCodeID = transaction.UnderlyingCodeID;
                        TMO.Remarks = transaction.Remarks;
                        TMO.IsTMO = true;
                        TMO.IsBookDeal = false;
                        TMO.DebitCurrencyID = transaction.DebitCurrency.ID;
                        TMO.IsJointAccount = transaction.IsJointAccount.HasValue ? transaction.IsJointAccount.Value : false;
                        TMO.IsResident = transaction.IsResident.HasValue ? transaction.IsResident.Value : false;
                        TMO.ActualSubmissionDateUnd = transaction.IsUnderlyingCompleted.HasValue ? (transaction.IsUnderlyingCompleted.Value ? DateTime.Now : default(DateTime?)) : default(DateTime?);
                        TMO.UpdateDate = DateTime.UtcNow;
                        TMO.UpdateBy = currentUser.GetCurrentUser().LoginName;
                        if (transaction.IsDraft.HasValue && !transaction.IsDraft.Value)
                        {
                            TMO.TransactionStatus = ""; // only view transaction 
                            TMO.TouchTimeStartDate = transaction.CreateDate;
                        }

                        if (transaction.IsOtherAccountNumber != null && transaction.IsOtherAccountNumber == true)
                        {
                            TMO.OtherAccountNumber = transaction.OtherAccountNumber;
                            TMO.AccountNumber = null;
                            TMO.IsOtherAccountNumber = true;
                        }
                        else
                        {
                            TMO.AccountNumber = transaction.AccountNumber;
                            TMO.OtherAccountNumber = null;
                            TMO.IsOtherAccountNumber = false;
                        }
                        context.SaveChanges();
                        if (transaction.SwapType != null && transaction.NB != null) // update status transaction swap 
                        {
                            if (transaction.IsDraft.HasValue && !transaction.IsDraft.Value)
                            {
                                bool isStatusUpdate = UpdateTransactionStatus(transaction.SwapTransactionID.Value, ref message);
                                if (!isStatusUpdate)
                                {
                                    ts.Dispose();
                                }
                            }
                        }

                        if (transaction.IsDraft.HasValue && !transaction.IsDraft.Value)
                        {
                            if (transaction.Underlyings != null && transaction.Underlyings.Count > 0)
                            {
                                #region Tambah Agung
                                decimal AmountTransaction = TMO.BuyAmount.Value;
                                bool mencukupi = false;
                                foreach (TransDetailUnderlyingModel underlyingItem in transaction.Underlyings)
                                {
                                    if (mencukupi != true)
                                    {
                                        Entity.CustomerUnderlying customerUnderlying = context.CustomerUnderlyings.Where(a => a.UnderlyingID.Equals(underlyingItem.ID)).SingleOrDefault();
                                        if (customerUnderlying != null)
                                        {
                                            decimal tempAvailableAmount = (decimal)customerUnderlying.AvailableAmountUSD;
                                            customerUnderlying.IsUtilize = true;
                                            customerUnderlying.UpdateDate = DateTime.UtcNow;
                                            customerUnderlying.AvailableAmountUSD = tempAvailableAmount - (AmountTransaction > underlyingItem.UtilizeAmountDeal.Value ? (underlyingItem.UtilizeAmountDeal.Value * customerUnderlying.Rate.Value) / Convert.ToDecimal((customerUnderlying.Amount * customerUnderlying.Rate.Value / customerUnderlying.AmountUSD.Value).ToString("#.##")) : (AmountTransaction * customerUnderlying.Rate.Value) / (customerUnderlying.Amount * customerUnderlying.Rate.Value / customerUnderlying.AmountUSD.Value));
                                            customerUnderlying.UpdateBy = currentUser.GetCurrentUser().LoginName;
                                            context.SaveChanges();

                                            Entity.TransactionDealUnderlying Underlying = new Entity.TransactionDealUnderlying()
                                            {
                                                TransactionDealID = transactionDealID,
                                                UnderlyingID = underlyingItem.ID,
                                                Amount = AmountTransaction > underlyingItem.UtilizeAmountDeal.Value ? (underlyingItem.UtilizeAmountDeal.Value * customerUnderlying.Rate.Value) / Convert.ToDecimal((customerUnderlying.Amount * customerUnderlying.Rate.Value / customerUnderlying.AmountUSD.Value).ToString("#.##")) : (AmountTransaction * customerUnderlying.Rate.Value) / Convert.ToDecimal((customerUnderlying.Amount * customerUnderlying.Rate.Value / customerUnderlying.AmountUSD.Value).ToString("#.##")),
                                                AvailableAmount = AmountTransaction > underlyingItem.UtilizeAmountDeal.Value ? underlyingItem.UtilizeAmountDeal.Value : AmountTransaction,
                                                CreateDate = DateTime.UtcNow,
                                                CreateBy = currentUser.GetCurrentUser().LoginName
                                            };
                                            context.TransactionDealUnderlyings.Add(Underlying);
                                            context.SaveChanges();

                                            if (AmountTransaction > underlyingItem.UtilizeAmountDeal.Value)
                                            {
                                                AmountTransaction = AmountTransaction - underlyingItem.UtilizeAmountDeal.Value;
                                            }
                                            else
                                            {
                                                mencukupi = true;
                                            }
                                        }
                                    }
                                }
                                #endregion

                                //IList<TransactionDealDocumentModel> documentsBulk = GetDocumentBulkUnderlying(UnderlyingID);
                                //foreach (var item in documentsBulk)
                                //{
                                //    transaction.Documents.Add(item);
                                //}
                            }
                        }
                        if (transaction.Documents.Count > 0)
                        {
                            foreach (var item in transaction.Documents)
                            {
                                Entity.TransactionDealDocument document = new Entity.TransactionDealDocument()
                                {
                                    TransactionDealID = transactionDealID,
                                    DocTypeID = item.Type.ID,
                                    PurposeID = item.Purpose.ID,
                                    Filename = item.FileName,
                                    DocumentPath = item.DocumentPath,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                };

                                context.TransactionDealDocuments.Add(document);
                                if (transaction.SwapType != null && transaction.NB != null)
                                {
                                    Entity.TransactionDealDocument documentSwap = new Entity.TransactionDealDocument()
                                    {
                                        TransactionDealID = transaction.SwapTransactionID.Value,
                                        DocTypeID = item.Type.ID,
                                        PurposeID = item.Purpose.ID,
                                        Filename = item.FileName,
                                        DocumentPath = item.DocumentPath,
                                        CreateDate = DateTime.UtcNow,
                                        CreateBy = currentUser.GetCurrentUser().LoginName
                                    };
                                    context.TransactionDealDocuments.Add(documentSwap);
                                }
                            }
                            context.SaveChanges();
                        }
                        if (transaction.DocumentTBO != null)
                        {
                            if (transaction.DocumentTBO.Count > 0)
                            {
                                foreach (var item in transaction.DocumentTBO)
                                {
                                    Entity.TBOTMOTransaction documentTBO = new Entity.TBOTMOTransaction()
                                    {
                                        TransactionDealsID = TMO.TransactionDealID,
                                        TBOSLAID = item.TBOTMOID,
                                        TBOTMODocumentID = item.TBOTMOID,
                                        TBOTMODocumentName = item.TBOTMOName,
                                        TBOTMOExpectedReceivedDate = item.ExpectedDateTBO,
                                        RemaksTBO = item.RemaksTBO,
                                        IsChecklistTBO = item.IsChecklistTBO,
                                        CreatedBy = currentUser.GetCurrentUser().DisplayName,
                                        CreatedDate = DateTime.UtcNow,
                                    };
                                    context.TBOTMOTransactions.Add(documentTBO);
                                    context.SaveChanges();
                                }
                            }
                        }

                    }

                    ts.Complete();
                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }

            return isSuccess;
        }

        #region Tambah Agung
        public bool UpdateTransactionCorrectionTMOById(long transactionDealID, TransactionTMOModel transaction, ref string message)
        {
            bool isSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    Entity.TransactionDeal TMO = context.TransactionDeals.Where(a => a.TransactionDealID.Equals(transactionDealID)).SingleOrDefault();
                    if (TMO != null)
                    {
                        TMO.ApplicationID = transaction.ApplicationID;
                        TMO.StatementLetterID = transaction.StatementLetter.ID;
                        TMO.UnderlyingCodeID = transaction.UnderlyingCodeID;
                        TMO.DebitCurrencyID = transaction.DebitCurrency.ID;
                        TMO.ActualSubmissionDateSL = transaction.SubmissionDateSL;
                        TMO.ActualSubmissionDateInstruction = transaction.SubmissionDateInstruction;
                        TMO.ActualSubmissionDateUnderlying = transaction.SubmissionDateUnderlying;
                        TMO.IsResubmit = transaction.IsResubmit;
                        if (transaction.IsOtherAccountNumber != null && transaction.IsOtherAccountNumber == true)
                        {
                            TMO.OtherAccountNumber = transaction.OtherAccountNumber;
                            TMO.AccountNumber = null;
                            TMO.IsOtherAccountNumber = true;
                        }
                        else
                        {
                            TMO.AccountNumber = transaction.AccountNumber;
                            TMO.OtherAccountNumber = null;
                            TMO.IsOtherAccountNumber = false;
                        }
                        context.SaveChanges();
                    }
                    #region Tambah Agung
                    bool isUnderlying = context.TransactionDealUnderlyings.Any(x => x.TransactionDealID.Equals(transactionDealID));

                    if (isUnderlying)
                    {
                        RoolbackDealCustomerUnderlying(transactionDealID, ref message);

                        #region Tambah Agung
                        decimal AmountTransaction = transaction.BuyAmount.Value;
                        bool mencukupi = false;
                        foreach (TransDetailUnderlyingModel underlyingItem in transaction.Underlyings)
                        {
                            if (mencukupi != true)
                            {
                                Entity.CustomerUnderlying customerUnderlying = context.CustomerUnderlyings.Where(a => a.UnderlyingID.Equals(underlyingItem.ID)).SingleOrDefault();
                                if (customerUnderlying != null)
                                {
                                    decimal tempAvailableAmount = (decimal)customerUnderlying.AvailableAmountUSD;
                                    customerUnderlying.IsUtilize = true;
                                    customerUnderlying.UpdateDate = DateTime.UtcNow;
                                    customerUnderlying.AvailableAmountUSD = tempAvailableAmount - (AmountTransaction > underlyingItem.UtilizeAmountDeal.Value ? (underlyingItem.UtilizeAmountDeal.Value * customerUnderlying.Rate.Value) / Convert.ToDecimal((customerUnderlying.Amount * customerUnderlying.Rate.Value / customerUnderlying.AmountUSD.Value).ToString("#.##")) : (AmountTransaction * customerUnderlying.Rate.Value) / (customerUnderlying.Amount * customerUnderlying.Rate.Value / customerUnderlying.AmountUSD.Value));
                                    customerUnderlying.UpdateBy = currentUser.GetCurrentUser().LoginName;
                                    context.SaveChanges();

                                    Entity.TransactionDealUnderlying Underlying = new Entity.TransactionDealUnderlying()
                                    {
                                        TransactionDealID = transactionDealID,
                                        UnderlyingID = underlyingItem.ID,
                                        Amount = AmountTransaction > underlyingItem.UtilizeAmountDeal.Value ? (underlyingItem.UtilizeAmountDeal.Value * customerUnderlying.Rate.Value) / Convert.ToDecimal((customerUnderlying.Amount * customerUnderlying.Rate.Value / customerUnderlying.AmountUSD.Value).ToString("#.##")) : (AmountTransaction * customerUnderlying.Rate.Value) / Convert.ToDecimal((customerUnderlying.Amount * customerUnderlying.Rate.Value / customerUnderlying.AmountUSD.Value).ToString("#.##")),
                                        AvailableAmount = AmountTransaction > underlyingItem.UtilizeAmountDeal.Value ? underlyingItem.UtilizeAmountDeal.Value : AmountTransaction,
                                        CreateDate = DateTime.UtcNow,
                                        CreateBy = currentUser.GetCurrentUser().LoginName
                                    };
                                    context.TransactionDealUnderlyings.Add(Underlying);
                                    context.SaveChanges();


                                    if (AmountTransaction > underlyingItem.UtilizeAmountDeal.Value)
                                    {
                                        AmountTransaction = AmountTransaction - underlyingItem.UtilizeAmountDeal.Value;
                                    }
                                    else
                                    {
                                        mencukupi = true;
                                    }
                                }
                            }
                        }
                        #endregion

                        var existingDocuments = (from x in context.TransactionDealDocuments
                                                 where x.TransactionDealID == transactionDealID && x.IsDeleted == false
                                                 select x);
                        if (existingDocuments != null && existingDocuments.ToList().Count > 0)
                        {
                            foreach (var item in existingDocuments.ToList())
                            {
                                context.TransactionDealDocuments.Remove(item);
                            }
                            context.SaveChanges();
                        }

                        if (transaction.Documents.Count > 0)
                        {
                            foreach (var item in transaction.Documents)
                            {
                                Entity.TransactionDealDocument document = new Entity.TransactionDealDocument()
                                {
                                    TransactionDealID = transactionDealID,
                                    DocTypeID = item.Type.ID,
                                    PurposeID = item.Purpose.ID,
                                    Filename = item.FileName,
                                    DocumentPath = item.DocumentPath,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                };

                                context.TransactionDealDocuments.Add(document);
                            }
                            context.SaveChanges();
                        }
                    }
                    else
                    {
                        long isTransUnderlying = context.Transactions.Where(x => x.TZNumber.Equals(TMO.TZRef)).Select(x => x.TransactionID).FirstOrDefault();

                        RoolbackCustomerUnderlyingTMO(isTransUnderlying, ref message);

                        #region Tambah Agung
                        decimal AmountTransaction = transaction.AmountUSD;
                        bool mencukupi = false;
                        foreach (TransDetailUnderlyingModel underlyingItem in transaction.Underlyings)
                        {
                            if (mencukupi != true)
                            {
                                Entity.CustomerUnderlying customerUnderlying = context.CustomerUnderlyings.Where(a => a.UnderlyingID.Equals(underlyingItem.ID)).SingleOrDefault();
                                if (customerUnderlying != null)
                                {
                                    decimal tempAvailableAmount = (decimal)customerUnderlying.AvailableAmountUSD;

                                    customerUnderlying.IsUtilize = true;
                                    customerUnderlying.UpdateDate = DateTime.UtcNow;
                                    customerUnderlying.AvailableAmountUSD = tempAvailableAmount - (AmountTransaction > underlyingItem.UtilizeAmountDeal.Value ? underlyingItem.UtilizeAmountDeal.Value : AmountTransaction);
                                    customerUnderlying.UpdateBy = currentUser.GetCurrentUser().LoginName;
                                    context.SaveChanges();

                                    Entity.TransactionUnderlying Underlying = new Entity.TransactionUnderlying()
                                    {
                                        TransactionID = isTransUnderlying,
                                        UnderlyingID = underlyingItem.ID,
                                        Amount = AmountTransaction > underlyingItem.UtilizeAmountDeal.Value ? underlyingItem.UtilizeAmountDeal.Value : AmountTransaction,
                                        AvailableAmount = 0,
                                        CreateDate = DateTime.UtcNow,
                                        CreateBy = currentUser.GetCurrentUser().LoginName
                                    };
                                    context.TransactionUnderlyings.Add(Underlying);
                                    context.SaveChanges();

                                    Entity.TransactionUnderlyingHistory vTransactionUnderlyingHistory = new Entity.TransactionUnderlyingHistory()
                                    {
                                        TransactionUnderlyingID = Underlying.TransactionUnderlyingID,
                                        Amount = AmountTransaction > underlyingItem.UtilizeAmountDeal.Value ? underlyingItem.UtilizeAmountDeal.Value : AmountTransaction,
                                        IsDeleted = false,
                                        CreatedDate = DateTime.UtcNow,
                                        CreatedBy = currentUser.GetCurrentUser().DisplayName
                                    };
                                    context.TransactionUnderlyingHistories.Add(vTransactionUnderlyingHistory);
                                    context.SaveChanges();

                                    if (AmountTransaction > underlyingItem.UtilizeAmountDeal.Value)
                                    {
                                        AmountTransaction = AmountTransaction - underlyingItem.UtilizeAmountDeal.Value;
                                    }
                                    else
                                    {
                                        mencukupi = true;
                                    }
                                }
                            }
                        }
                        #endregion

                        var existingDocuments = (from x in context.TransactionDocuments
                                                 where x.TransactionID == isTransUnderlying && x.IsDeleted == false
                                                 select x);
                        if (existingDocuments != null && existingDocuments.ToList().Count > 0)
                        {
                            foreach (var item in existingDocuments.ToList())
                            {
                                context.TransactionDocuments.Remove(item);
                            }
                            context.SaveChanges();
                        }

                        if (transaction.Documents.Count > 0)
                        {
                            foreach (var item in transaction.Documents)
                            {
                                Entity.TransactionDocument document = new Entity.TransactionDocument()
                                {
                                    TransactionID = isTransUnderlying,
                                    DocTypeID = item.Type.ID,
                                    PurposeID = item.Purpose.ID,
                                    Filename = item.FileName,
                                    DocumentPath = item.DocumentPath,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                };

                                context.TransactionDocuments.Add(document);
                            }
                            context.SaveChanges();
                        }
                    }
                    #endregion

                    ts.Complete();
                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }

            return isSuccess;
        }
        #endregion
        public bool GetAmountTransactionDeal(string Cif, bool isStatementB, ref decimal output, ref string message)
        {
            bool isSuccess = false;

            try
            {
                DateTime currenDate = DateTime.UtcNow;

                IList<TransactionDealTotalAmountModel> datas = null;

                if (!isStatementB)
                {
                    datas = (from a in context.TransactionDeals
                             where a.CIF.Equals(Cif) && a.CreatedDate.Month == currenDate.Month && a.CreatedDate.Year == currenDate.Year
                             && a.StatementLetterID == 2 && a.CurrencyID != 13 //<>IDR
                             group a by a.TransactionDealID into grpData
                             select new TransactionDealTotalAmountModel
                             {
                                 CIF = grpData.FirstOrDefault().CIF,
                                 TransactionID = grpData.FirstOrDefault().TransactionDealID,
                                 USDAmount = grpData.FirstOrDefault().AmountUSD
                             }).ToList();

                }
                else
                {
                    datas = (from a in context.TransactionDeals
                             where a.CIF.Equals(Cif) && a.CreatedDate.Month == currenDate.Month && a.CreatedDate.Year == currenDate.Year && a.CurrencyID != 13 //<>IDR

                             group a by a.TransactionDealID into grpData
                             select new TransactionDealTotalAmountModel
                             {
                                 CIF = grpData.FirstOrDefault().CIF,
                                 TransactionID = grpData.FirstOrDefault().TransactionDealID,
                                 USDAmount = grpData.FirstOrDefault().AmountUSD
                             }).ToList();

                }

                if (datas != null)
                {
                    var result = datas.GroupBy(x => x.CIF)
                 .Select(g => new { Id = g.Key, Sum = g.Sum(x => x.USDAmount) }).FirstOrDefault();

                    if (result != null)
                    {
                        output = (decimal)result.Sum;
                    }

                }
                else
                {
                    output = 0;
                }

                isSuccess = true;

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool GetTzInformation(int page, int size, string CIF, ref TzInformationGrid tzInformation, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                int skip = (page - 1) * size;
                var data = (from a in context.TransactionDeals
                            join b in context.Customers on a.CIF equals b.CIF
                            join c in context.ProductTypes on a.ProductTypeID equals c.ProductTypeID
                            join d in context.Currencies on a.CurrencyID equals d.CurrencyID
                            join e in context.StatementLetters on a.StatementLetterID equals e.StatementLetterID
                            where a.CIF == CIF
                            select new TZTransactionModel()
                            {
                                ID = a.TransactionDealID,
                                CustomerName = b.CustomerName,
                                TZRef = a.TZRef,
                                Amount = a.Amount,
                                IsFxTransaction = a.IsFxTransaction,
                                Rate = a.Rate,
                                TradeDate = a.TradeDate,
                                AccountNumber = (a.IsOtherAccountNumber.HasValue && a.IsOtherAccountNumber.Value ? a.OtherAccountNumber : a.AccountNumber),
                                Currency = new CurrencyModel()
                                {
                                    ID = d.CurrencyID,
                                    Code = d.CurrencyCode,
                                    Description = d.CurrencyDescription
                                },
                                ProductType = new ProductTypeModel
                                {
                                    ID = c.ProductTypeID,
                                    Code = c.ProductTypeCode,
                                    Description = c.ProductTypeDesc
                                },
                                StatementLetter = new StatementLetterModel()
                                {
                                    ID = e.StatementLetterID,
                                    Name = e.StatementLetterName,
                                    LastModifiedBy = e.UpdateBy,
                                    LastModifiedDate = e.UpdateDate
                                }
                            });

                tzInformation.Page = page;
                tzInformation.Size = size;
                tzInformation.Total = data.Count();
                tzInformation.Rows = data.AsEnumerable()
                    .Select((a, i) => new TzInformationRow
                    {
                        RowID = i + 1,
                        ID = a.ID,
                        CustomerName = a.CustomerName,
                        TZRef = a.TZRef,
                        Amount = a.Amount,
                        IsFxTransaction = a.IsFxTransaction,
                        Rate = a.Rate,
                        TradeDate = a.TradeDate,
                        AccountNumber = a.AccountNumber,
                        Currency = a.Currency,
                        ProductType = a.ProductType,
                        StatementLetter = a.StatementLetter
                    })
                    .Skip(skip)
                    .Take(size)
                    .ToList();

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return IsSuccess;

        }

        public bool GetTransactionDeal(int page, int size, IList<TransactionDealFilter> filters, string sortColumn, string sortOrder, ref TransactionDealGrid transaction, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                //string orderBy = sortColumn + " " + sortOrder;
                string orderBy = string.Format("TradeDate DESC,{0} {1}", sortColumn, sortOrder);
                DateTime maxDate = new DateTime();
                //DateTime maxTradeDate = new DateTime();
                TransactionDealRow filter = new TransactionDealRow();
                if (filters != null)
                {
                    filter.ApplicationID = (string)filters.Where(a => a.Field.Equals("ApplicationID")).Select(a => a.Value).SingleOrDefault();
                    filter.TZReference = (string)filters.Where(a => a.Field.Equals("TZReference")).Select(a => a.Value).SingleOrDefault();
                    filter.CustomerName = (string)filters.Where(a => a.Field.Equals("Customer")).Select(a => a.Value).SingleOrDefault();
                    filter.Product = (string)filters.Where(a => a.Field.Equals("Product")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("TradeDate")).Any())
                    {
                        filter.TradeDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("TradeDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("ValueDate")).Any())
                    {
                        filter.ValueDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("ValueDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    filter.CurrencyDesc = (string)filters.Where(a => a.Field.Equals("CurrencyDesc")).Select(a => a.Value).SingleOrDefault();
                    filter.Amount = Convert.ToDecimal((string)filters.Where(a => a.Field.Equals("Amount")).Select(a => a.Value == null ? "0" : a.Value).SingleOrDefault());
                    filter.AccountNumber = (string)filters.Where(a => a.Field.Equals("AccountNumber")).Select(a => a.Value).SingleOrDefault();
                    filter.FxTransactionDesc = filters.Where(a => a.Field.Equals("IsFXTransaction")).Select(a => a.Value).SingleOrDefault();
                    filter.TransactionStatus = (string)filters.Where(a => a.Field.Equals("TransactionStatus")).Select(a => a.Value).SingleOrDefault();
                    filter.TzStatus = (string)filters.Where(a => a.Field.Equals("TzStatus")).Select(a => a.Value).SingleOrDefault();
                    filter.UpdateBy = (string)filters.Where(a => a.Field.Equals("UpdateBy")).Select(a => a.Value).SingleOrDefault();
                    filter.IsTMO = Convert.ToBoolean(filters.Where(a => a.Field.Equals("IsTMO")).Select(a => a.Value).SingleOrDefault());
                    if (filters.Where(a => a.Field.Equals("UpdateDate")).Any())
                    {
                        filter.UpdateDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("UpdateDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.UpdateDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    try
                    {
                        var data = (from a in context.SP_GetTransactionDeal(filter.ApplicationID,
                                                     filter.TZReference,
                                                     filter.CustomerName,
                                                     filter.Product,
                                                     filter.TradeDate,
                                                     filter.ValueDate,
                                                     filter.CurrencyDesc,
                                                     filter.Amount,
                                                     filter.AccountNumber,
                                                     filter.FxTransactionDesc,
                                                     filter.TransactionStatus,
                                                     filter.TzStatus,
                                                     filter.UpdateBy,
                                                     filter.UpdateDate,
                                                     filter.IsTMO)
                                    select new TransactionDealRow
                                    {
                                        TransactionDealID = a.TransactionDealID,
                                        TZReference = a.TZRef,
                                        ApplicationID = a.ApplicationID,
                                        CustomerName = a.CustomerName,
                                        Product = a.Product,
                                        TradeDate = a.TradeDate,
                                        ValueDate = a.ValueDate,
                                        CurrencyDesc = a.CurrencyDesc,
                                        Amount = (decimal)a.Amount,
                                        AccountNumber = a.AccountNumber,
                                        TransactionStatus = a.TransactionStatus,//a.IsCanceled == true ? "Un-Reconciled Yet With TZ Report" : "Submit By " + a.CreatedBy,
                                        IsFxTransaction = a.IsFxTransaction,
                                        TzStatus = a.TzStatus,
                                        UpdateBy = a.UpdateBy,
                                        UpdateDate = a.UpdateDate,
                                        CIF = a.CIF
                                    }).ToList();

                        transaction.Page = page;
                        transaction.Size = size;
                        transaction.Total = data.Count();
                        transaction.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                        transaction.Rows = data.AsQueryable().OrderBy(orderBy)
                            .Select((a, i) => new TransactionDealRow
                            {
                                RowID = i + 1,
                                TZReference = a.TZReference,
                                AccountNumber = a.AccountNumber,
                                TransactionDealID = a.TransactionDealID,
                                ApplicationID = a.ApplicationID,
                                CustomerName = a.CustomerName,
                                Product = a.Product,
                                CurrencyDesc = a.CurrencyDesc,
                                TradeDate = a.TradeDate,
                                ValueDate = a.ValueDate,
                                Amount = a.Amount,
                                AmountUSD = a.AmountUSD,
                                IsFxTransaction = a.IsFxTransaction,
                                TransactionStatus = a.TransactionStatus,
                                TzStatus = a.TzStatus,
                                UpdateBy = a.UpdateBy,
                                UpdateDate = a.UpdateDate,
                                CIF = a.CIF
                            })
                            .Skip(skip)
                            .Take(size)
                            .Distinct()
                            .ToList();
                    }
                    catch (Exception ex)
                    {
                        message = ex.ToString();
                        return false;
                    }
                    isSuccess = true;
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateBookDealById(string id, TransactionDealDetailModel data, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool AddBizUnitChecker(Guid workflowInstanceID, long approverID, PaymentCheckerDataModel data, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool SetApplicationID(string applicationID, long transactionID, ref string message)
        {
            bool isSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    Entity.TransactionDeal deal = context.TransactionDeals.Where(a => a.TransactionDealID.Equals(transactionID)).SingleOrDefault();

                    if (deal != null)
                    {
                        deal.ApplicationID = applicationID;
                        context.SaveChanges();
                    }

                    ts.Complete();
                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }

            return isSuccess;
        }

        public bool UpdateTransactionDealAfterRevise(Guid workflowInstanceID, long approverID, TransactionDealDetailModel transaction, ref string message)
        {
            bool isSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    var data = context.TransactionDeals.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID)).SingleOrDefault();
                    if (data != null)
                    {
                        //data.CIF = transaction.Customer.CIF;
                        if (transaction.StatementLetter != null) { data.StatementLetterID = transaction.StatementLetter.ID; }
                        data.UnderlyingCodeID = transaction.bookunderlyingcode;
                        data.UnderlyingAmount = transaction.bookunderlyingamount;
                        data.IsBookDeal = transaction.IsBookDeal;
                        data.OtherUnderlying = transaction.OtherUnderlying;
                        data.DebitCurrencyID = transaction.DebitCurrency.ID;
                        data.IsOtherAccountNumber = transaction.IsOtherAccountNumber;
                        data.IsJointAccount = transaction.IsJointAccount;
                        data.IsResident = transaction.IsResident.HasValue ? transaction.IsResident.Value : false;
                        if (transaction.IsOtherAccountNumber != null && transaction.IsOtherAccountNumber == true)
                        {
                            data.OtherAccountNumber = transaction.OtherAccountNumber;
                            data.AccountNumber = null;
                            data.IsOtherAccountNumber = true;
                        }
                        else
                        {
                            data.AccountNumber = transaction.AccountNumber;
                            data.OtherAccountNumber = null;
                            data.IsOtherAccountNumber = false;
                        }

                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().LoginName;

                        context.SaveChanges();

                        if (transaction.Documents != null)
                        {
                            var existingDocuments = (from x in context.TransactionDealDocuments
                                                     where x.TransactionDealID == data.TransactionDealID && x.IsDeleted == false
                                                     select x);
                            if (existingDocuments != null && existingDocuments.ToList().Count > 0)
                            {
                                foreach (var item in existingDocuments.ToList()) // remove All transaction document
                                {
                                    context.TransactionDealDocuments.Remove(item);
                                }
                                context.SaveChanges();

                                foreach (var item in transaction.Documents) //Add All Transaction document
                                {
                                    //if (item.Purpose.ID != 2)
                                    {
                                        Entity.TransactionDealDocument document = new Entity.TransactionDealDocument()
                                        {
                                            TransactionDealID = data.TransactionDealID,
                                            DocTypeID = item.Type.ID,
                                            PurposeID = item.Purpose.ID,
                                            Filename = item.FileName,
                                            IsDeleted = false,
                                            DocumentPath = item.DocumentPath,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().LoginName
                                        };

                                        context.TransactionDealDocuments.Add(document);
                                    }
                                    context.SaveChanges();
                                }
                            }
                            else
                            {
                                foreach (var item in transaction.Documents)
                                {
                                    // if (item.Purpose.ID != 2) Add All transaction Document
                                    {
                                        Entity.TransactionDealDocument document = new Entity.TransactionDealDocument()
                                        {
                                            TransactionDealID = data.TransactionDealID,
                                            DocTypeID = item.Type.ID,
                                            PurposeID = item.Purpose.ID,
                                            Filename = item.FileName,
                                            IsDeleted = false,
                                            DocumentPath = item.DocumentPath,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().LoginName
                                        };
                                        context.TransactionDealDocuments.Add(document);
                                    }
                                    context.SaveChanges();
                                }
                            }
                        }


                        if (transaction.Underlyings != null && transaction.Underlyings.Count > 0)
                        {
                            long transactionID = transaction.ID;
                            // if (RoolbackDealCustomerUnderlying(transactionID, ref message)) // roolback customer underlying
                            {
                                decimal availableAmount = transaction.AmountUSD; // update customer underlying
                                for (int i = 0; transaction.Underlyings.Count > i; i++)
                                {
                                    long UnderlyingID = transaction.Underlyings[i].ID;
                                    Entity.CustomerUnderlying customerUnderlying = context.CustomerUnderlyings.Where(a => a.UnderlyingID.Equals(UnderlyingID)).SingleOrDefault();
                                    if (customerUnderlying != null)
                                    {
                                        decimal tempAvailableAmount = (decimal)customerUnderlying.AvailableAmountUSD;
                                        availableAmount = (decimal)customerUnderlying.AvailableAmountUSD - availableAmount;
                                        customerUnderlying.UpdateDate = DateTime.UtcNow;
                                        customerUnderlying.AvailableAmountUSD = (decimal?)(availableAmount > 0 ? availableAmount : 0);
                                        customerUnderlying.IsUtilize = true;
                                        customerUnderlying.UpdateBy = currentUser.GetCurrentUser().LoginName;
                                        context.SaveChanges();
                                        // set amount for transaction underlying
                                        transaction.Underlyings[i].USDAmount = tempAvailableAmount - (availableAmount > 0 ? availableAmount : 0);
                                        availableAmount = (availableAmount > 0 ? 0 : Math.Abs(availableAmount));
                                    }
                                }

                                // remove transaction Underlying by TransactionID
                                /* var trnsUnderlyings = context.TransactionDealUnderlyings.Where(a => a.TransactionDealID.Equals(transaction.ID)&&a.IsDeleted.Equals(false)).ToList();
                                 foreach (Entity.TransactionDealUnderlying trnsUnderlying in trnsUnderlyings)
                                 {
                                     context.TransactionDealUnderlyings.Remove(trnsUnderlying);
                                 } */

                                // Update transaction Underlying by TransactionID
                                /* var trnsUnderlyings = context.TransactionDealUnderlyings.Where(a => a.TransactionDealID.Equals(transaction.ID)&&a.IsDeleted.Equals(false)).ToList();
                                 foreach (Entity.TransactionDealUnderlying trnsUnderlying in trnsUnderlyings)
                                 {
                                    trnsUnderlying.IsDeleted = true;
                                    trnsUnderlying.UpdateBy = currentUser.GetCurrentUser().LoginName;
                                    trnsUnderlying.UpdateDate = DateTime.UtcNow;
                                    context.SaveChanges();
                                 } */

                                // add transaction Underlying by TransactionID
                                foreach (TransDetailUnderlyingModel underlyingItem in transaction.Underlyings)
                                {
                                    Entity.TransactionDealUnderlying Underlying = new Entity.TransactionDealUnderlying()
                                    {
                                        TransactionDealID = transactionID,
                                        UnderlyingID = underlyingItem.ID,
                                        Amount = underlyingItem.USDAmount,
                                        IsDeleted = false,
                                        CreateDate = DateTime.UtcNow,
                                        CreateBy = currentUser.GetCurrentUser().LoginName
                                    };
                                    context.TransactionDealUnderlyings.Add(Underlying);
                                }
                                context.SaveChanges();
                            }
                        }
                    }
                    ts.Complete();
                    isSuccess = true;
                }

                catch (Exception ex)
                {
                    ts.Dispose();
                    message = ex.Message;
                }
            }

            return isSuccess;
        }

        public bool UpdateTransactionDealRevise(Guid workflowInstanceID, TransactionDealDetailModel TransactionDeal, ref string message)
        {
            bool isSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    Entity.TransactionDeal data = context.TransactionDeals.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID)).SingleOrDefault();
                    if (data != null)
                    {
                        long transactionID = data.TransactionDealID;
                        isSuccess = RoolbackDealCustomerUnderlying(transactionID, ref message);
                        if (!isSuccess)
                        {
                            ts.Dispose();
                            return isSuccess;
                        }
                        isSuccess = true;
                    }
                    ts.Complete();

                }
                catch (Exception ex)
                {
                    ts.Dispose();
                    message = ex.Message;
                }
            }
            return isSuccess;
        }

        public bool RoolbackDealCustomerUnderlying(long IdTransactionDeal, ref string message)
        {
            bool isSuccess = false;

            try
            {
                List<Entity.TransactionDealUnderlying> items = context.TransactionDealUnderlyings.Where(a => a.TransactionDealID.Equals(IdTransactionDeal) && a.IsDeleted.Equals(false)).ToList();
                if (items != null && items.Count > 0)
                {
                    foreach (Entity.TransactionDealUnderlying data in items)
                    {
                        // set update customer underlying
                        var dataCustomerUnderlying = context.CustomerUnderlyings.Where(a => a.UnderlyingID.Equals(data.UnderlyingID)).SingleOrDefault();
                        dataCustomerUnderlying.AvailableAmountUSD += data.Amount;
                        dataCustomerUnderlying.IsUtilize = dataCustomerUnderlying.AmountUSD == dataCustomerUnderlying.AvailableAmountUSD ? false : true;
                        dataCustomerUnderlying.UpdateDate = DateTime.UtcNow;
                        dataCustomerUnderlying.UpdateBy = currentUser.GetCurrentUser().LoginName;
                        context.SaveChanges();
                        // set update transaction Deal underlying
                        var dataTransactionDealUnderlying = context.TransactionDealUnderlyings.Where(a => a.TransactionDealUnderlyingID.Equals(data.TransactionDealUnderlyingID)).SingleOrDefault();
                        dataTransactionDealUnderlying.IsDeleted = true;
                        dataTransactionDealUnderlying.UpdateDate = DateTime.UtcNow;
                        dataTransactionDealUnderlying.UpdateBy = currentUser.GetCurrentUser().LoginName;
                        context.SaveChanges();
                    }
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {

                message = ex.Message;
            }
            return isSuccess;
        }

        public bool RoolbackCustomerUnderlyingTMO(long IdTransaction, ref string message)
        {
            bool isSuccess = false;

            try
            {
                List<Entity.TransactionUnderlying> items = context.TransactionUnderlyings.Where(a => a.TransactionID.Equals(IdTransaction) && a.IsDeleted.Equals(false)).ToList();
                if (items != null && items.Count > 0)
                {
                    foreach (Entity.TransactionUnderlying data in items)
                    {
                        // set update customer underlying
                        var dataCustomerUnderlying = context.CustomerUnderlyings.Where(a => a.UnderlyingID.Equals(data.UnderlyingID)).SingleOrDefault();
                        dataCustomerUnderlying.AvailableAmountUSD += data.Amount;
                        dataCustomerUnderlying.IsUtilize = dataCustomerUnderlying.AmountUSD == dataCustomerUnderlying.AvailableAmountUSD ? false : true;
                        dataCustomerUnderlying.UpdateDate = DateTime.UtcNow;
                        dataCustomerUnderlying.UpdateBy = currentUser.GetCurrentUser().LoginName;
                        context.SaveChanges();
                        // set update transaction Deal underlying
                        var dataTransactionDealUnderlying = context.TransactionUnderlyings.Where(a => a.TransactionUnderlyingID.Equals(data.TransactionUnderlyingID)).SingleOrDefault();
                        dataTransactionDealUnderlying.IsDeleted = true;
                        dataTransactionDealUnderlying.UpdateDate = DateTime.UtcNow;
                        dataTransactionDealUnderlying.UpdateBy = currentUser.GetCurrentUser().LoginName;

                        Entity.TransactionUnderlyingHistory dataTransactionUnderlyingHistory = context.TransactionUnderlyingHistories.Where(a => a.TransactionUnderlyingID.Equals(data.TransactionUnderlyingID) && a.IsDeleted.Value.Equals(false)).SingleOrDefault();
                        if (dataTransactionUnderlyingHistory != null)
                        {
                            dataTransactionUnderlyingHistory.IsDeleted = true;
                        }

                        context.SaveChanges();
                    }
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {

                message = ex.Message;
            }
            return isSuccess;
        }
        public bool UpdateTransactionStatus(long IdTransactionDeal, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    Entity.TransactionDeal item = context.TransactionDeals.Where(a => a.TransactionDealID.Equals(IdTransactionDeal)).SingleOrDefault();
                    if (item != null)
                    {
                        item.TransactionStatus = ""; // view Transaction
                        item.UpdateDate = DateTime.UtcNow;
                        item.UpdateBy = currentUser.GetCurrentUser().LoginName;
                        context.SaveChanges();
                    }
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {

                message = ex.Message;
            }
            return isSuccess;
        }
        public bool UpdateTransactionTMORevise(Guid workflowInstanceID, int TransactionDealID, ref string message)
        {
            bool isSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    Entity.TransactionDeal data;
                    if (workflowInstanceID != null)
                    {
                        data = context.TransactionDeals.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID)).SingleOrDefault();
                    }
                    else
                    {
                        data = context.TransactionDeals.Where(a => a.TransactionDealID.Equals(TransactionDealID)).SingleOrDefault();
                    }
                    if (data != null)
                    {
                        long transactionID = data.TransactionDealID;
                        isSuccess = RoolbackDealCustomerUnderlying(transactionID, ref message);
                        if (!isSuccess)
                        {
                            ts.Dispose();
                            return isSuccess;
                        }
                        isSuccess = true;
                    }
                    ts.Complete();

                }
                catch (Exception ex)
                {
                    ts.Dispose();
                    message = ex.Message;
                }
            }
            return isSuccess;
        }

        public bool UpdateTransactionStatusOTTFXTransaction(string TZNumber, ref string message)
        {
            bool IsSuccess = false;
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    if (TZNumber != null)
                    {
                        Entity.TransactionDeal data = context.TransactionDeals.Where(x => x.TZRef.Equals(TZNumber)).SingleOrDefault();
                        if (data != null)
                        {
                            data.TransactionStatus = "Completed";
                            context.SaveChanges();
                        }
                        IsSuccess = true;
                    }
                    ts.Complete();
                }
                catch (Exception ex)
                {
                    ts.Dispose();
                    message = ex.Message;
                }
            }
            return IsSuccess;

        }
        #region IPE

        public bool UpdateTransactionDealAfterReviseIPE(Guid workflowInstanceID, long approverID, TransactionDealDetailModel transaction, ref string message)
        {
            bool isSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    var data = context.TransactionDeals.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID)).SingleOrDefault();
                    if (data != null)
                    {
                        //data.CIF = transaction.Customer.CIF;
                        if (transaction.StatementLetter != null) { data.StatementLetterID = transaction.StatementLetter.ID; }
                        data.UnderlyingCodeID = transaction.bookunderlyingcode;
                        data.UnderlyingAmount = transaction.bookunderlyingamount;
                        data.IsBookDeal = transaction.IsBookDeal;
                        data.OtherUnderlying = transaction.OtherUnderlying;
                        data.DebitCurrencyID = transaction.DebitCurrency.ID;
                        data.IsOtherAccountNumber = transaction.IsOtherAccountNumber;
                        data.IsJointAccount = transaction.IsJointAccount;
                        data.IsResident = transaction.IsResident.HasValue ? transaction.IsResident.Value : false;
                        if (transaction.IsOtherAccountNumber != null && transaction.IsOtherAccountNumber == true)
                        {
                            data.OtherAccountNumber = transaction.OtherAccountNumber;
                            data.AccountNumber = null;
                            data.IsOtherAccountNumber = true;
                        }
                        else
                        {
                            data.AccountNumber = transaction.AccountNumber;
                            data.OtherAccountNumber = null;
                            data.IsOtherAccountNumber = false;
                        }

                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        context.SaveChanges();

                        if (transaction.Documents != null)
                        {
                            var existingDocuments = (from x in context.TransactionDealDocuments
                                                     where x.TransactionDealID == data.TransactionDealID && x.IsDeleted == false
                                                     select x);
                            if (existingDocuments != null && existingDocuments.ToList().Count > 0)
                            {
                                foreach (var item in existingDocuments.ToList()) // remove All transaction document
                                {
                                    context.TransactionDealDocuments.Remove(item);
                                }
                                context.SaveChanges();

                                foreach (var item in transaction.Documents) //Add All Transaction document
                                {
                                    //if (item.Purpose.ID != 2)
                                    {
                                        Entity.TransactionDealDocument document = new Entity.TransactionDealDocument()
                                        {
                                            TransactionDealID = data.TransactionDealID,
                                            DocTypeID = item.Type.ID,
                                            PurposeID = item.Purpose.ID,
                                            Filename = item.FileName,
                                            IsDeleted = false,
                                            DocumentPath = item.DocumentPath,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().DisplayName
                                        };

                                        context.TransactionDealDocuments.Add(document);
                                    }
                                    context.SaveChanges();
                                }
                            }
                            else
                            {
                                foreach (var item in transaction.Documents)
                                {
                                    // if (item.Purpose.ID != 2) Add All transaction Document
                                    {
                                        Entity.TransactionDealDocument document = new Entity.TransactionDealDocument()
                                        {
                                            TransactionDealID = data.TransactionDealID,
                                            DocTypeID = item.Type.ID,
                                            PurposeID = item.Purpose.ID,
                                            Filename = item.FileName,
                                            IsDeleted = false,
                                            DocumentPath = item.DocumentPath,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().DisplayName
                                        };
                                        context.TransactionDealDocuments.Add(document);
                                    }
                                    context.SaveChanges();
                                }
                            }
                        }


                        if (transaction.Underlyings != null && transaction.Underlyings.Count > 0)
                        {
                            long transactionID = transaction.ID;
                            // if (RoolbackDealCustomerUnderlying(transactionID, ref message)) // roolback customer underlying
                            {
                                decimal availableAmount = transaction.AmountUSD; // update customer underlying
                                for (int i = 0; transaction.Underlyings.Count > i; i++)
                                {
                                    long UnderlyingID = transaction.Underlyings[i].ID;
                                    Entity.CustomerUnderlying customerUnderlying = context.CustomerUnderlyings.Where(a => a.UnderlyingID.Equals(UnderlyingID)).SingleOrDefault();
                                    if (customerUnderlying != null)
                                    {
                                        decimal tempAvailableAmount = (decimal)customerUnderlying.AvailableAmountUSD;
                                        availableAmount = (decimal)customerUnderlying.AvailableAmountUSD - availableAmount;
                                        customerUnderlying.UpdateDate = DateTime.UtcNow;
                                        customerUnderlying.AvailableAmountUSD = (decimal?)(availableAmount > 0 ? availableAmount : 0);
                                        customerUnderlying.IsUtilize = true;
                                        customerUnderlying.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                                        context.SaveChanges();
                                        // set amount for transaction underlying
                                        transaction.Underlyings[i].USDAmount = tempAvailableAmount - (availableAmount > 0 ? availableAmount : 0);
                                        availableAmount = (availableAmount > 0 ? 0 : Math.Abs(availableAmount));
                                    }
                                }

                                // remove transaction Underlying by TransactionID
                                /* var trnsUnderlyings = context.TransactionDealUnderlyings.Where(a => a.TransactionDealID.Equals(transaction.ID)&&a.IsDeleted.Equals(false)).ToList();
                                 foreach (Entity.TransactionDealUnderlying trnsUnderlying in trnsUnderlyings)
                                 {
                                     context.TransactionDealUnderlyings.Remove(trnsUnderlying);
                                 } */

                                // Update transaction Underlying by TransactionID
                                /* var trnsUnderlyings = context.TransactionDealUnderlyings.Where(a => a.TransactionDealID.Equals(transaction.ID)&&a.IsDeleted.Equals(false)).ToList();
                                 foreach (Entity.TransactionDealUnderlying trnsUnderlying in trnsUnderlyings)
                                 {
                                    trnsUnderlying.IsDeleted = true;
                                    trnsUnderlying.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                                    trnsUnderlying.UpdateDate = DateTime.UtcNow;
                                    context.SaveChanges();
                                 } */

                                // add transaction Underlying by TransactionID
                                foreach (TransDetailUnderlyingModel underlyingItem in transaction.Underlyings)
                                {
                                    Entity.TransactionDealUnderlying Underlying = new Entity.TransactionDealUnderlying()
                                    {
                                        TransactionDealID = transactionID,
                                        UnderlyingID = underlyingItem.ID,
                                        Amount = underlyingItem.USDAmount,
                                        IsDeleted = false,
                                        CreateDate = DateTime.UtcNow,
                                        CreateBy = currentUser.GetCurrentUser().DisplayName
                                    };
                                    context.TransactionDealUnderlyings.Add(Underlying);
                                }
                                context.SaveChanges();
                            }
                        }
                    }
                    ts.Complete();
                    isSuccess = true;
                }

                catch (Exception ex)
                {
                    ts.Dispose();
                    message = ex.Message;
                }
            }

            return isSuccess;
        }


        public bool UpdateTransactionDealReviseIPE(Guid workflowInstanceID, TransactionDealDetailModel TransactionDeal, ref string message)
        {
            bool isSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    Entity.TransactionDeal data = context.TransactionDeals.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID)).SingleOrDefault();
                    if (data != null)
                    {
                        long transactionID = data.TransactionDealID;
                        isSuccess = RoolbackDealCustomerUnderlying(transactionID, ref message);
                        if (!isSuccess)
                        {
                            ts.Dispose();
                            return isSuccess;
                        }
                        isSuccess = true;
                    }
                    ts.Complete();

                }
                catch (Exception ex)
                {
                    ts.Dispose();
                    message = ex.Message;
                }
            }
            return isSuccess;
        }
        #endregion

        #region Tambah Agung

        #region Draft TMO IPE
        public bool GetTransactionDraftByIDTMOIPE(long id, ref TransactionDraftModel transaction, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                context.Transactions.AsNoTracking();

                TransactionDraftModel datatransaction = (from a in context.TransactionDeals
                                                         where a.TransactionDealID.Equals(id)
                                                         select new TransactionDraftModel
                                                         {
                                                             ID = a.TransactionDealID,
                                                             ApplicationID = a.ApplicationID,
                                                             ApplicationDate = DateTime.UtcNow,
                                                             Amount = a.SellAmount,
                                                             IsNewCustomer = false,
                                                             TZNumber = a.TZRef,
                                                             DraftCurrencyID = a.SellCurrencyID,
                                                             CreateDate = a.CreatedDate,
                                                             LastModifiedBy = a.UpdateDate == null ? a.CreatedBy : a.UpdateBy,
                                                             LastModifiedDate = a.UpdateDate == null ? a.CreatedDate : a.UpdateDate.Value,
                                                             IsResident = a.IsResident,
                                                             IsOtherAccountNumber = a.IsOtherAccountNumber,
                                                             AccountNumber = a.AccountNumber,
                                                             OtherAccountNumber = a.OtherAccountNumber,
                                                             CIF = a.CIF,
                                                             UnderlyingDocID = a.UnderlyingCodeID.HasValue ? (int)a.UnderlyingCodeID.Value : 0,
                                                             IsJointAccount = a.IsJointAccount.HasValue ? a.IsJointAccount.Value : false,
                                                             Remarks = a.Remarks,
                                                             Instruction = a.ActualSubmissionDateInstruction.HasValue ? true : false,
                                                             Underlying = a.ActualSubmissionDateUnderlying.HasValue ? true : false,
                                                             StatementLetter = a.ActualSubmissionDateSL.HasValue ? true : false,
                                                             ProductType = context.ProductTypes.Where(x => a.ProductTypeID != null && x.ProductTypeID == a.ProductTypeID).Select(x => new ProductTypeModel
                                                             {
                                                                 ID = x.ProductTypeID,
                                                                 Code = x.ProductTypeCode,
                                                                 IsFlowValas = x.IsFlowValas
                                                             }).FirstOrDefault(),
                                                         }).SingleOrDefault();

                // fill payment data
                if (datatransaction != null)
                {
                    if (datatransaction.IsNewCustomer != null && datatransaction.IsNewCustomer != true)
                    {
                        ICustomerRepository customerRepo = new CustomerRepository();
                        CustomerModel customer = new CustomerModel();
                        bool isUnderlying = context.TransactionDealUnderlyings.Any(x => x.TransactionDealID.Equals(id));
                        if (isUnderlying)
                        {
                            if (customerRepo.GetCustomerByTransactionDeal(id, ref customer, ref message))
                            {
                                datatransaction.Customer = customer;
                            }
                        }
                        else
                        {
                            if (customerRepo.GetCustomerByCIF(datatransaction.CIF, ref customer, ref message))
                            {
                                datatransaction.Customer = customer;
                                datatransaction.Customer.Underlyings = null;
                            }
                        }
                    }
                    else
                    {
                        datatransaction.Customer = new CustomerModel();
                    }

                    if (datatransaction.Customer != null)
                    {
                        datatransaction.BizSegment = datatransaction.Customer.BizSegment;
                        datatransaction.ChargingAccountName = datatransaction.Customer.Name;
                    }

                    if (datatransaction.IsOtherAccountNumber != null && datatransaction.IsOtherAccountNumber == true)
                    {
                        if (datatransaction.OtherAccountNumber != null)
                        {
                            CustomerAccountModel Account = new CustomerAccountModel();
                            datatransaction.Account = Account;
                            datatransaction.OtherAccountNumber = datatransaction.OtherAccountNumber;
                            datatransaction.ChargingACCNumber = datatransaction.OtherAccountNumber;
                            datatransaction.ChargingAccountCurrency = datatransaction.DraftCurrencyID;

                            CurrencyModel AccountCurrency = new CurrencyModel();

                            var data = context.TransactionDeals.Where(x => x.TransactionDealID.Equals(id)).Select(x => x).FirstOrDefault();

                            AccountCurrency.ID = data.DebitCurrency.CurrencyID;
                            AccountCurrency.Code = data.DebitCurrency.CurrencyCode;
                            AccountCurrency.Description = data.DebitCurrency.CurrencyDescription;

                            datatransaction.DebitCurrency = AccountCurrency;

                            if (datatransaction.DebitCurrency.ID == 1)
                            {
                                datatransaction.DraftCurrencyID = data.BuyCurrencyID;
                                datatransaction.Amount = data.BuyAmount;
                            }
                            else
                            {
                                datatransaction.DraftCurrencyID = data.BuyCurrencyID;
                                datatransaction.Amount = data.BuyAmount;
                            }
                        }
                    }
                    else
                    {
                        if (datatransaction.AccountNumber != null)
                        {

                            datatransaction.Account = (from j in context.SP_GETJointAccount(datatransaction.CIF)
                                                       where j.AccountNumber.Equals(datatransaction.AccountNumber)
                                                       select new CustomerAccountModel
                                                       {
                                                           AccountNumber = j.AccountNumber,
                                                           Currency = new CurrencyModel
                                                           {
                                                               ID = j.CurrencyID.Value,
                                                               Code = j.CurrencyCode,
                                                               Description = j.CurrencyDescription
                                                           },
                                                           CustomerName = j.CustomerName,
                                                           IsJointAccount = j.IsJointAccount.HasValue ? j.IsJointAccount.Value : false
                                                       }).FirstOrDefault();
                            datatransaction.AccountNumber = datatransaction.AccountNumber;
                            datatransaction.ChargingACCNumber = datatransaction.AccountNumber;
                            datatransaction.ChargingAccountCurrency = datatransaction.DraftCurrencyID;
                        }
                        var data = context.TransactionDeals.Where(x => x.TransactionDealID.Equals(id)).Select(x => x).FirstOrDefault();
                        if (datatransaction.Account.Currency.ID == 1)
                        {
                            datatransaction.DraftCurrencyID = data.BuyCurrencyID;
                            datatransaction.Amount = data.BuyAmount;
                        }
                        else
                        {
                            datatransaction.DraftCurrencyID = data.BuyCurrencyID;
                            datatransaction.Amount = data.BuyAmount;
                        }
                    }

                    datatransaction.Bank = new BankModel();
                    if (datatransaction.ProductType != null)
                    {

                        ProductTypeModel ptm = new ProductTypeModel();

                        ptm.ID = datatransaction.ProductType.ID;
                        ptm.Code = datatransaction.ProductType.Code;
                        ptm.Description = datatransaction.ProductType.Description;
                        ptm.IsFlowValas = datatransaction.ProductType.IsFlowValas;

                        datatransaction.ProductType = ptm;
                    }

                    UnderlyingDocModel docs = new UnderlyingDocModel();
                    if (datatransaction.UnderlyingDocID != 0)
                    {
                        docs.ID = datatransaction.UnderlyingDocID;
                        datatransaction.UnderlyingDoc = docs;
                    }

                    var docDraft = (from doc in context.TransactionDealDocuments
                                    where doc.TransactionDealID == id && doc.IsDeleted.Equals(false)
                                    select new TransactionDocumentDraftModel
                                    {
                                        ID = doc.TransactionDocumentID,
                                        IsDormant = false,
                                        LastModifiedBy = doc.UpdateBy == null ? doc.CreateBy : doc.UpdateBy,
                                        LastModifiedDate = doc.UpdateDate == null ? doc.CreateDate : doc.CreateDate,
                                        Purpose = new DocumentPurposeModel { ID = doc.DocumentPurpose.PurposeID, Name = doc.DocumentPurpose.PurposeName },
                                        Type = new DocumentTypeModel { ID = doc.DocumentType.DocTypeID, Name = doc.DocumentType.DocTypeName },
                                        FileName = doc.Filename,
                                        DocumentPath = new DocumentPathModel { name = doc.Filename, lastModified = string.Empty, lastModifiedDate = DateTime.Now, DocPath = doc.DocumentPath, type = Util.ExistingDoctype }
                                    }).ToList();
                    if (docDraft != null)
                    {
                        datatransaction.Documents = docDraft;
                    }
                    ///End Andi
                    transaction = datatransaction;
                }



                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }

            return IsSuccess;
        }
        #endregion
        public bool GetDataInstructionDocument(int page, int size, IList<InstructionDocumentFilter> filters, string sortColumn, string sortOrder, ref InstructionDocumentGrid InstructionGrid, string cif, ref string message)
        {
            bool isSuccess = false;
            DateTime currentDate = DateTime.Now;
            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                InstructionDocumentModel filter = new InstructionDocumentModel();
                if (filters != null)
                {
                    filter.ApplicationID = (string)filters.Where(a => a.Field.Equals("ApplicationID")).Select(a => a.Value).SingleOrDefault();
                    filter.FileName = (string)filters.Where(a => a.Field.Equals("FileName")).Select(a => a.Value).SingleOrDefault();
                    filter.Purpose = new DocumentPurposeModel { Name = (string)filters.Where(a => a.Field.Equals("PurposeofDoc")).Select(a => a.Value).SingleOrDefault() };
                    filter.Type = new DocumentTypeModel { Name = (string)filters.Where(a => a.Field.Equals("DocumentType")).Select(a => a.Value).SingleOrDefault() };

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    List<InstructionDocumentModel> data = (from a in context.Transactions
                                                           join b in context.TransactionDocuments on a.TransactionID equals b.TransactionID
                                                           let isApplicationID = !string.IsNullOrEmpty(filter.ApplicationID)
                                                           let isFileName = !string.IsNullOrEmpty(filter.FileName)
                                                           let isPurpose = !string.IsNullOrEmpty(filter.Purpose.Name)
                                                           let isDocumentType = !string.IsNullOrEmpty(filter.Type.Name)
                                                           let isLastModifiedDate = filter.LastModifiedDate.HasValue ? true : false
                                                           where
                                                                           a.CIF.Equals(cif) &&
                                                                           a.ProductID.Equals((int)DBSProductID.TMOProductIDCons) &&
                                                                           a.Transaction_Status.Equals("Completed") &&
                                                                           (isApplicationID ? a.ApplicationID.Contains(filter.ApplicationID) : true) &&
                                                                           (isFileName ? b.Filename.Contains(filter.FileName) : true) &&
                                                                           (isPurpose ? b.DocumentPurpose.PurposeName.Contains(filter.Purpose.Name) : true) &&
                                                                           (isDocumentType ? b.DocumentType.DocTypeName.Contains(filter.Type.Name) : true) &&
                                                                           (isLastModifiedDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                                           select new InstructionDocumentModel
                                                           {
                                                               IsSelected = false,
                                                               IsNewDocument = false,
                                                               TransactionTMOID = a.TransactionID,
                                                               ApplicationID = a.ApplicationID,
                                                               FileName = b.Filename,
                                                               DocumentPath = b.DocumentPath,
                                                               Purpose = new DocumentPurposeModel
                                                               {
                                                                   ID = b.DocumentPurpose.PurposeID,
                                                                   Name = b.DocumentPurpose.PurposeName,
                                                                   Description = b.DocumentPurpose.PurposeDescription,
                                                                   LastModifiedBy = "",
                                                                   LastModifiedDate = a.CreateDate
                                                               },
                                                               Type = new DocumentTypeModel
                                                               {
                                                                   ID = b.DocumentType.DocTypeID,
                                                                   Name = b.DocumentType.DocTypeName,
                                                                   Description = b.DocumentType.DocTypeDescription,
                                                                   LastModifiedBy = "",
                                                                   LastModifiedDate = a.CreateDate
                                                               },
                                                               LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                                           }).ToList();

                    InstructionGrid.Page = page;
                    InstructionGrid.Size = size;
                    InstructionGrid.Total = data.Count();
                    InstructionGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    InstructionGrid.Rows = data.AsQueryable().OrderBy(orderBy).Select((a, i) => new InstructionDocumentModel
                    {
                        ID = i + 1,
                        IsSelected = a.IsSelected,
                        IsNewDocument = a.IsNewDocument,
                        TransactionTMOID = a.TransactionTMOID,
                        ApplicationID = a.ApplicationID,
                        FileName = a.FileName,
                        DocumentPath = a.DocumentPath,
                        Purpose = a.Purpose,
                        Type = a.Type,
                        LastModifiedDate = a.LastModifiedDate
                    })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        #endregion
    }
    #endregion
}