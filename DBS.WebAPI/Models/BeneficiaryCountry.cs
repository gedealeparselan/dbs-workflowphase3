﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Models
{

    #region Property
    [ModelName("BeneficiaryCountry")]
    public class BeneficiaryCountryModel
    {
        public int ID { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        //public Nullable<bool> IsDeleted { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }       
    }

    public class BeneficiaryCountryRow : BeneficiaryCountryModel
    {
        public int RowID { get; set; }

    }

    public class BeneficiaryCountryGrid : Grid
    {
        public IList<BeneficiaryCountryRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class BeneficiaryCountryFilter : Filter { }
    #endregion

    #region Interface
    public interface IBeneficiaryCountryRepository : IDisposable
    {
        bool GetBeneficiaryCountryByID(int id, ref BeneficiaryCountryModel BeneficiaryCountryModel, ref string message);
        bool GetBeneficiaryCountry(ref IList<BeneficiaryCountryModel> BeneficiaryCountryModel, int limit, int index, ref string message);
        bool GetBeneficiaryCountry(BeneficiaryCountryFilter filter, ref IList<BeneficiaryCountryModel> BeneficiaryCountryModel, ref string message);
        bool GetBeneficiaryCountry(ref IList<BeneficiaryCountryModel> BeneficiaryCountryModel, ref string message);
        bool GetBeneficiaryCountry(string key, int limit, ref IList<BeneficiaryCountryModel> BeneficiaryCountryModel, ref string message);
        bool GetBeneficiaryCountry(int page, int size, IList<BeneficiaryCountryFilter> filters, string sortColumn, string sortOrder, ref BeneficiaryCountryGrid BeneficiaryCountryGrid, ref string message);
        bool AddBeneficiaryCountry(BeneficiaryCountryModel BeneficiaryCountryModel, ref string message);
        bool UpdateBeneficiaryCountry(int id, BeneficiaryCountryModel BeneficiaryCountryModel, ref string message);
        bool DeleteBeneficiaryCountry(int id, ref string message);
    }
    #endregion

    #region Repository
    public class BeneficiaryCountryRepository : IBeneficiaryCountryRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetBeneficiaryCountryByID(int id, ref BeneficiaryCountryModel BeneficiaryCountryModel, ref string message)
        {
            bool isSuccess = false;
            try
            {
                BeneficiaryCountryModel = (from a in context.BeneficiaryCountries
                                           where a.IsDeleted == false && a.BeneficiaryCountryID == id
                                           select new BeneficiaryCountryModel
                                           {
                                               ID = a.BeneficiaryCountryID,
                                               Code = a.BeneficiaryCountryCode,
                                               Description = a.Description,
                                               CreateDate = a.CreateDate,
                                               CreateBy = a.CreateBy,
                                               LastModifiedDate = a.UpdateDate,
                                               LastModifiedBy = a.UpdateBy, 
                                           }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool GetBeneficiaryCountry(ref IList<BeneficiaryCountryModel> BeneficiaryCountryModel, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    BeneficiaryCountryModel = (from a in context.BeneficiaryCountries
                                               where a.IsDeleted == false
                                               orderby new { a.BeneficiaryCountryCode }
                                               select new BeneficiaryCountryModel
                                               {
                                                   ID = a.BeneficiaryCountryID,
                                                   Code = a.BeneficiaryCountryCode,
                                                   Description = a.Description,
                                                   CreateDate = a.CreateDate,
                                                   CreateBy = a.CreateBy,
                                                   LastModifiedDate = a.UpdateDate,
                                                   LastModifiedBy = a.UpdateBy, 
                                               }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetBeneficiaryCountry(ref IList<BeneficiaryCountryModel> BeneficiaryCountryModel, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    BeneficiaryCountryModel = (from a in context.BeneficiaryCountries
                                               where a.IsDeleted == false
                                               orderby new { a.BeneficiaryCountryCode }
                                               select new BeneficiaryCountryModel
                                               {
                                                   ID = a.BeneficiaryCountryID,
                                                   Code = a.BeneficiaryCountryCode,
                                                   Description = a.Description,
                                                   CreateDate = a.CreateDate,
                                                   CreateBy = a.CreateBy,
                                                   LastModifiedDate = a.UpdateDate,
                                                   LastModifiedBy = a.UpdateBy,                                                 
                                               }
                                         ).ToList();
                    BeneficiaryCountryModel = BeneficiaryCountryModel.GroupBy(p => p.Code).Select(g => g.First()).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetBeneficiaryCountry(BeneficiaryCountryFilter filter, ref IList<BeneficiaryCountryModel> BeneficiaryCountryModel, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetBeneficiaryCountry(string key, int limit, ref IList<BeneficiaryCountryModel> BeneficiaryCountryModel, ref string message)
        {
            bool isSuccess = false;

            try
            {
                BeneficiaryCountryModel = (from a in context.BeneficiaryCountries
                                           where a.IsDeleted == false && a.BeneficiaryCountryCode.Contains(key)                                                                                           
                                           orderby new { a.BeneficiaryCountryCode }
                                           select new BeneficiaryCountryModel
                                           {
                                               ID = a.BeneficiaryCountryID,
                                               Code = a.BeneficiaryCountryCode,
                                               Description = a.Description,
                                               CreateDate = a.CreateDate,
                                               CreateBy = a.CreateBy,
                                               LastModifiedDate = a.UpdateDate,
                                               LastModifiedBy = a.UpdateBy,   
                                           }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetBeneficiaryCountry(int page, int size, IList<BeneficiaryCountryFilter> filters, string sortColumn, string sortOrder, ref BeneficiaryCountryGrid BeneficiaryCountryGrid, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                BeneficiaryCountryModel filter = new BeneficiaryCountryModel();
                if (filters != null)
                {
                    filter.Code = (string)filters.Where(a => a.Field.Equals("Code")).Select(a => a.Value).SingleOrDefault();
                    filter.Description = (string)filters.Where(a => a.Field.Equals("Description")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.BeneficiaryCountries
                                let isBeneficiaryCountryCode = !string.IsNullOrEmpty(filter.Code)
                                let isBeneficiaryCountryDescription = !string.IsNullOrEmpty(filter.Description)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted == false &&
                                    (isBeneficiaryCountryCode ? a.BeneficiaryCountryCode.Contains(filter.Code) : true) &&
                                    (isBeneficiaryCountryDescription ? a.Description.Contains(filter.Description) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new BeneficiaryCountryModel
                                {
                                    ID = a.BeneficiaryCountryID,
                                    Code = a.BeneficiaryCountryCode,
                                    Description = a.Description,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    BeneficiaryCountryGrid.Page = page;
                    BeneficiaryCountryGrid.Size = size;
                    BeneficiaryCountryGrid.Total = data.Count();
                    BeneficiaryCountryGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    BeneficiaryCountryGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new BeneficiaryCountryRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            Code = a.Code,
                            Description = a.Description,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddBeneficiaryCountry(BeneficiaryCountryModel BeneficiaryCountryModel, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (!context.BeneficiaryCountries.Where(a => a.BeneficiaryCountryCode.Trim() == BeneficiaryCountryModel.Code.Trim() && a.IsDeleted == false).Any())
                {
                    context.BeneficiaryCountries.Add(new Entity.BeneficiaryCountry()
                    {
                        BeneficiaryCountryCode = BeneficiaryCountryModel.Code,
                        Description = BeneficiaryCountryModel.Description,
                        IsDeleted = false,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    });

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Beneficiary Country data for Beneficiary Country {0} is already exist.", BeneficiaryCountryModel.Code);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateBeneficiaryCountry(int id, BeneficiaryCountryModel BeneficiaryCountryModel, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.BeneficiaryCountry data = context.BeneficiaryCountries.Where(a => a.BeneficiaryCountryID == id).SingleOrDefault();

                if (data != null)
                {
                    if (context.BeneficiaryCountries.Where(a => a.BeneficiaryCountryID != BeneficiaryCountryModel.ID && a.BeneficiaryCountryCode == BeneficiaryCountryModel.Code && a.IsDeleted == false).Count() >= 1)
                    {
                        message = string.Format("Beneficiary Country {0} is exist.", BeneficiaryCountryModel.Code);
                    }
                    else
                    {
                        data.BeneficiaryCountryCode = BeneficiaryCountryModel.Code;
                        data.Description = BeneficiaryCountryModel.Description;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        context.SaveChanges();

                        isSuccess = true;
                    }
                }
                else
                {
                    message = string.Format("Beneficiary Country with id {0} does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeleteBeneficiaryCountry(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.BeneficiaryCountry data = context.BeneficiaryCountries.Where(a => a.BeneficiaryCountryID == id).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Beneficiary Country data for Beneficiary Country ID {0} does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion

}