﻿var countResultTo = null;
var countResultCc = null;
var ViewModel = function () {
    //Make the self as 'this' reference
    var self = this;

    self.Readonly = ko.observable(false);
    self.IsWorkflow = ko.observable(false);

    //Declare observable which will be bind with UI

    self.ID = ko.observable("");
    self.TypeOfEmail = ko.observable("");
    self.To = ko.observableArray([]);
    self.Cc = ko.observable("");
    self.MailSubject = ko.observable("");
    self.MailBody = ko.observable("");
    self.LastModifiedBy = ko.observable("");
    self.LastModifiedDate = ko.observable("");

    // filter
    self.FilterTypeOfEmail = ko.observable("");
    self.FilterTo = ko.observable("");
    self.FilterCc = ko.observable("");
    self.FilterModifiedBy = ko.observable("");
    self.FilterModifiedDate = ko.observable("");

    self.IsRoleMaker = ko.observable(false);
    self.IsPermited = ko.observable(false);

    // New Data flag
    self.IsNewData = ko.observable(false);

    self.BarLoad = ko.observable(false);
    // Declare an ObservableArray for Storing the JSON Response
    self.NotificationEmails = ko.observableArray([]);
    self.SelectedTypeOfEmail = ko.observable("");

    self.Modul = ko.observable("");
    self.ModulHeader = ko.observable("");
    self.ModulNewData = ko.observable("");
    self.ModulHeaderNewData = ko.observable("");
    self.ModulHeaderModify = ko.observable("");
    self.Url = ko.observable("");

    self.Modul = function () {
        var url = window.location.href;
        var urlmodul = url.substring(url.lastIndexOf("/") + 1);
        var arrmodul = urlmodul.split("-");
        var modul = null;
        switch (arrmodul[0]) {
            case "cbo":
                modul = arrmodul[0].toUpperCase();
                break;
            case "collateral":
                modul = arrmodul[0].charAt(0).toUpperCase() + arrmodul[0].slice(1);
                break;

        }
        return modul;

    }

    self.ModulHeader(self.Modul() + " Notification Email Parameter");
    self.ModulNewData("New " + self.Modul() + " Notification Email Parameter");
    self.ModulHeaderNewData("Create a new " + self.Modul() + " Notification Email Parameter");
    self.ModulHeaderModify("Modify a " + self.Modul() + " Notification Email Parameter");

    // grid properties
    self.GridProperties = ko.observable();
    self.GridProperties(new GridPropertiesModel(GetData));

    // set default sorting
    self.GridProperties().SortColumn("TypeOfEmail");
    self.GridProperties().SortOrder("ASC");

    self.Url = function () {
        if (ko.toJS(self.Modul()) != null || ko.toJS(self.Modul()) != undefined) {
            switch (ko.toJS(self.Modul()).toLowerCase()) {
                case "cbo":
                    Url = api.server + api.url.cbonotificationemail;
                    break;
                case "collateral":
                    Url = api.server + api.url.collateralnotificationemail;
                    break;

            }
            return Url;
        }
    }

    //dropdown
    self.ddlTypeOfEmail = ko.observableArray([]);

    // bind clear filters
    self.ClearFilters = function () {
        self.FilterTypeOfEmail("");
        self.FilterTo("");
        self.FilterCc("");
        self.FilterModifiedBy("");
        self.FilterModifiedDate("");

        GetData();
    };

    // bind get data function to view
    self.GetData = function () {
        if (self.Modul() != null || self.Modul() != undefined) {
            GetData();
        }
    };

    self.GetParameters = function () {
        GetParameters(ko.toJS(self.Modul()));
    }
    self.SetAutoCompleted = function () {
        function split(val) {
            return val.split(/;\s*/);
        }
        function extractLast(term) {
            return split(term).pop();
        }
        $("#to")
        .bind('keydown', function (event) {
            if (event.keyCode === $.ui.keyCode.TAB && $(this).autocomplete("instance").menu.active) {
                event.preventDefault();
            }
        })
        .autocomplete({

            source: function (request, response) {
                self.BarLoad(true);
                // declare options variable for ajax get request
                var options = {
                    url: api.server + api.url.helper + "/GetUserAD",
                    data: {
                        username: extractLast(request.term),
                        limit: 20
                    },
                    token: accessToken
                };

                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessAutoCompleteTo, TokenOnError);

            },
            minLength: 2,
            select: function (event, ui) {
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': [""]
                    };

                    var terms = split(this.value);
                    terms.pop();
                    terms.push(ui.item.value);
                    terms.push("");
                    this.value = terms.join("; ");
                    self.To(ko.mapping.toJS(this.value, mapping))

                    return false;

                }

            },
            focus: function () {
                // prevent value inserted on focus
                return false;
            },
            response: function (event, ui) {
                if (ui.content.length === 0) {
                    countResultTo = 0;
                } else {
                    countResultTo = 1;
                }
            }

        });
        $("#cc")
        .bind('keydown', function (event) {
            if (event.keyCode === $.ui.keyCode.TAB && $(this).autocomplete("instance").menu.active) {
                event.preventDefault();
            }
        })
        .autocomplete({

            source: function (request, response) {
                self.BarLoad(true);
                // declare options variable for ajax get request
                var options = {
                    url: api.server + api.url.helper + "/GetUserAD",
                    data: {
                        username: extractLast(request.term),
                        limit: 20
                    },
                    token: accessToken
                };

                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessAutoCompleteCc, TokenOnError);

            },
            minLength: 2,
            select: function (event, ui) {
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': [""]
                    };

                    var terms = split(this.value);
                    terms.pop();
                    terms.push(ui.item.value);
                    terms.push("");
                    this.value = terms.join("; ");
                    self.Cc(ko.mapping.toJS(this.value, mapping))

                    return false;

                }

            },
            focus: function () {
                // prevent value inserted on focus
                return false;
            },
            response: function (event, ui) {
                if (ui.content.length === 0) {
                    countResultCc = 0;
                } else {
                    countResultCc = 1;
                }
            }

        });


    }
    function OnSuccessAutoCompleteTo(response, data, textStatus, jqXHR) {
        response($.map(data, function (item) {
            return {
                // default autocomplete object
                label: item.UserName + " - " + item.Email,
                value: item.Email,

                // custom object binding
                data: {
                    ID: item.UserName,
                    Name: item.DisplayName,
                    Email: item.Email,
                    PhysicalDeliveryOfficeName: item.PhysicalDeliveryOfficeName,
                    Department: item.Department,
                    Title: item.Title
                }
            }
        })
        );

        self.BarLoad(false);
    }
    function OnSuccessAutoCompleteCc(response, data, textStatus, jqXHR) {
        response($.map(data, function (item) {
            return {
                // default autocomplete object
                label: item.UserName + " - " + item.Email,
                value: item.Email,

                // custom object binding
                data: {
                    ID: item.UserName,
                    Name: item.DisplayName,
                    Email: item.Email,
                    PhysicalDeliveryOfficeName: item.PhysicalDeliveryOfficeName,
                    Department: item.Department,
                    Title: item.Title
                }
            }
        })
        );

        self.BarLoad(false);
    }

    //The Object which stored data entered in the observables
    var NotificationEmail = {
        ID: self.ID,
        TypeOfEmail: self.SelectedTypeOfEmail,
        To: self.To,
        Cc: self.Cc,
        MailSubject: self.MailSubject,
        MailBody: self.MailBody,
        Modul: self.Modul(),
        LastModifiedBy: self.LastModifiedBy,
        LastModifiedDate: self.LastModifiedDate
    };

    self.IsPermited = function (IsPermitedResult) {
        //Ajax call to delete the Customer
        spUser = $.cookie(api.cookie.spUser);

        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/CheckUserPermited/" + _spFriendlyUrlPageContextInfo.title,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    //compare user role to page permission to get checker validation
                    console.log("spUser Roles : " + ko.toJSON(spUser.Roles));
                    console.log("spUser Permited : " + data);
                    for (i = 0; i < spUser.Roles.length; i++) {
                        for (j = 0; j < data.length; j++) {
                            if (Const_RoleName[spUser.Roles[i].ID] == data[j]) { //if (spUser.Roles[i].Name == data[j]) {
                                if (Const_RoleName[spUser.Roles[i].ID].endsWith('Maker')) { //if (spUser.Roles[i].Name.endsWith('Maker')) {
                                    self.IsRoleMaker(true);
                                    return;
                                }
                                else {
                                    self.IsRoleMaker(false);
                                }
                            }
                        }
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    // Local Date
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return "";
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-18
    };

    self.GetSelectedRow = function (data) {

        self.IsNewData(false);
        if (self.IsWorkflow()) $("#modal-form-workflow").modal('show');
        else $("#modal-form").modal('show');
        if (data.Modul != null || data.Modul != undefined) {
            self.Url = function () {
                if (data.Modul != null || data.Modul != undefined) {
                    switch (data.Modul) {
                        case "CBO":
                            Url = api.server + api.url.cbonotificationemail;
                            break;
                        case "Collateral":
                            Url = api.server + api.url.collateralnotificationemail;
                            break;

                    }
                    return Url;
                }


            }

            GetParameters(data.Modul);

        } else {
            GetParameters(self.Modul());
        }
        var url = window.location.href.toLowerCase();
        if (url.indexOf('parameter-maintenance') > -1) {
            CKEDITOR.instances['editor1'].setData(data.MailBody);
            $('#editorapproval1').hide();
        } else {
            $('#editor1').hide();
            $('#editorapproval1')[0].innerHTML = data.MailBody;

        }

        self.ID(data.ID);
        self.TypeOfEmail(data.TypeOfEmail);
        self.SelectedTypeOfEmail(data.TypeOfEmail);
        self.To(data.To);
        self.Cc(data.Cc);
        self.MailSubject(data.MailSubject);
        self.MailBody(data.MailBody);
        self.LastModifiedBy(data.LastModifiedBy);
        self.LastModifiedDate(data.LastModifiedDate);

    };
    self.NewData = function () {
        // flag as new Data
        self.IsNewData(true);

        self.Readonly(false);
        // bind empty data
        self.ID(0);
        self.TypeOfEmail('');
        self.SelectedTypeOfEmail('');
        self.To('');
        self.Cc('');
        self.MailSubject('');
        self.MailBody('');
        $('#editorapproval1').hide();;
        ClearMailBody();


    };
    self.ClearMailBody = function () {
        ClearMailBody();
    }
    self.save = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {

                    var mailBodyText = CKEDITOR.instances['editor1'].getData();
                    self.MailBody(mailBodyText);

                    if (countResultTo === 0 || countResultCc === 0) {
                        ShowNotification("Form Validation Warning", "Employee Email not found in Active Directory", 'gritter-warning', true);
                        return false;
                    }

                    $.ajax({
                        type: "POST",
                        url: ko.toJS(self.Url()),
                        data: ko.toJSON(NotificationEmail), //Convert the Observable Data into JSON
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {

                            if (jqXHR.status = 200) {
                                // send notification
                                if (data.ID != 0) {

                                    ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                } else {

                                    ShowNotification('Warning', jqXHR.responseJSON.Message, 'gritter-warning', true);

                                }
                                // hide current popup window
                                $("#modal-form").modal('hide');

                                // refresh data
                                GetData();

                            } else {

                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification

                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });

                }
            });
        }
    };
    self.update = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            // hide current popup window
            //$("#modal-form").modal('hide');

            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {
                    var mailBodyText = CKEDITOR.instances['editor1'].getData();
                    self.MailBody(mailBodyText);

                    //Ajax call to insert the MatrixApprovals
                    if (countResultTo === 0 || countResultCc === 0) {
                        ShowNotification("Form Validation Warning", "Employee Email not found in Active Directory", 'gritter-warning', true);
                        return false;
                    }

                    //Ajax call to update the Customer
                    $.ajax({
                        type: "PUT",
                        url: ko.toJS(self.Url()) + "/" + NotificationEmail.ID(),
                        data: ko.toJSON(NotificationEmail),
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {
                                // send notification

                                if (data.ID != 0) {

                                    ShowNotification('Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                } else {

                                    ShowNotification('Warning', jqXHR.responseJSON.Message, 'gritter-warning', true);

                                }

                                // hide current popup window
                                $("#modal-form").modal('hide');

                                // refresh data
                                GetData();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });
                }
            });
        }
    };
    self.delete = function () {
        // hide current popup window
        //$("#modal-form").modal('hide');

        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                $("#modal-form").modal('show');
            } else {
                //Ajax call to delete notification email

                $.ajax({
                    type: "DELETE",
                    url: ko.toJS(self.Url()) + "/" + NotificationEmail.ID(),
                    data: ko.toJSON(NotificationEmail), //Convert the Observable Data into JSON
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if (jqXHR.status = 200) {

                            if (data.ID != 0) {

                                ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                            } else {

                                ShowNotification('Warning', jqXHR.responseJSON.Message, 'gritter-warning', true);

                            }

                            // refresh data
                            GetData();
                        } else
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
            }
        });
    };

    function ClearMailBody() {
        CKEDITOR.instances['editor1'].setData("");
    }

    function GetData() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: ko.toJS(self.Url()),
            params: {
                page: self.GridProperties().Page(),
                size: self.GridProperties().Size(),
                sort_column: self.GridProperties().SortColumn(),
                sort_order: self.GridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetData, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetData, OnError, OnAlways);
        }
    }
    function DecodeParams(params) {
        if (typeof (params) != 'object') {
            alert("The parameter value is not an object!")
            return false;
        }

        var output = "";

        for (var key in params) {
            if (output != "") {
                output += "&";
            } else {
                output += "?";
            }

            output += key + "=" + params[key];
        }

        return output;
    }

    function GetParameters(Modul) {
        // validate & decode params
        var options = {
            url: api.server + 'api/NotificationEmail/Parameters',
            params: {
                modul: Modul
            }
        };

        var Url = options.url;
        if (options.params != null || options.params != undefined) {
            Url += DecodeParams(options.params);
        }

        $.ajax({
            type: "GET",
            async: false,
            url: Url,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.ddlTypeOfEmail(data);
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });


    }
    function GetFilteredColumns() {
        // define filter
        var filters = [];

        if (self.FilterTypeOfEmail() != "") filters.push({ Field: 'TypeOfEmail', Value: self.FilterTypeOfEmail() });
        if (self.FilterTo() != "") filters.push({ Field: 'Description', Value: self.FilterTo() });
        if (self.FilterCc() != "") filters.push({ Field: 'Cc', Value: self.FilterCc() });
        if (self.FilterModifiedBy() != "") filters.push({ Field: 'LastModifiedBy', Value: self.FilterModifiedBy() });
        if (self.FilterModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.FilterModifiedDate() });

        return filters;
    };

    // On success GetData callback
    function OnSuccessGetData(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.NotificationEmails(data.Rows);

            self.GridProperties().Page(data['Page']);
            self.GridProperties().Size(data['Size']);
            self.GridProperties().Total(data['Total']);
            self.GridProperties().TotalPages(Math.ceil(self.GridProperties().Total() / self.GridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }


    // On Error callback
    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    // On Always callback
    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }
    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }

    });


};

$.holdReady(true);
var intervalRoleName = setInterval(function () {
    if (Object.keys(Const_RoleName).length != 0) {
        $.holdReady(false);
        clearInterval(intervalRoleName);
    }
}, 100);

//$(document).on("RoleNameReady", function() {
$(document).ready(function () {

    var url = window.location.href.toLowerCase();

    if (url.indexOf('parameter-maintenance') > -1) {
        var CKEDITORScript = '';
        CKEDITORScript = document.createElement('script');
        CKEDITORScript.type = 'text/javascript';
        CKEDITORScript.src = '/SiteAssets/Scripts/ckeditor/ckeditor.js';
        document.getElementsByTagName('head')[0].appendChild(CKEDITORScript);
    }

});


