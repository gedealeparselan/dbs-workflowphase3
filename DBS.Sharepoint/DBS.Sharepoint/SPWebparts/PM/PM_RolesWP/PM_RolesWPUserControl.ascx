﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PM_RolesWPUserControl.ascx.cs" Inherits="DBS.Sharepoint.SPWebparts.PM.PM_RolesWP.PM_RolesWPUserControl" %>


<h1 class="header smaller lighter dark">Roles</h1>
<!-- widget box start -->

<div id="widget-box" class="widget-box">
	<!-- widget header start -->
	<div class="widget-header widget-hea1der-small header-color-dark">
		<h6>Role Table</h6>

		<div class="widget-toolbar">
			<label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
				<i class="blue icon-filter"></i>
				<input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: Role().GridProperties().AllowFilter" />
				<span class="lbl"></span>
			</label>
			<a href="#" data-bind="click: Role().GetData" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
				<i class="blue icon-refresh"></i>
			</a>
		</div>
	</div>
	<!-- widget header end -->

	<!-- widget body start -->
	<div class="widget-body">
		<!-- widget main start -->
		<div class="widget-main padding-0">
			<!-- widget slim control start -->
			<div class="slim-scroll" data-height="400">
				<!-- widget content start -->
				<div class="content">

					<!-- table responsive start -->
					<div class="table-responsive">
						<div class="dataTables_wrapper" role="grid">
							<table id="Role-table" class="table table-striped table-bordered table-hover dataTable">
                                <thead>
                                    <tr data-bind="with: Role().GridProperties">
                                        <th style="width:50px">No.</th>
                                        <th data-bind="click: function () { Sorting('Name'); }, css: GetSortedColumn('Name')">
										Name</th>
                                        <th data-bind="click: function () { Sorting('Description'); }, css: GetSortedColumn('Description')">
										Description</th>
                                        <th data-bind="click: function () { Sorting('LastModifiedBy'); }, css: GetSortedColumn('LastModifiedBy')">
										Modified By</th>
                                        <th data-bind="click: function () { Sorting('LastModifiedDate'); }, css: GetSortedColumn('LastModifiedDate')">
										Modified Date</th>
                                    </tr>
                                </thead>
                                <thead data-bind="visible: Role().GridProperties().AllowFilter" class="table-filter">
                                    <tr>
                                        <th class="clear-filter">
                                            <a href="#" data-bind="click: Role().ClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                <i class="green icon-trash"></i>
                                            </a>
                                        </th>
                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.Role().FilterName, event: { change: Role().GridProperties().Filter }" /></th>
                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.Role().FilterDescription, event: { change: Role().GridProperties().Filter }" /></th>
                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.Role().FilterModifiedBy, event: { change: Role().GridProperties().Filter }" /></th>
                                        <th><input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.Role().FilterModifiedDate, event: { change: Role().GridProperties().Filter }"/></th>
                                    </tr>
                                </thead>
                                <tbody data-bind="foreach: Role().Roles, visible: Role().Roles().length > 0">
                                    <tr data-bind="click: $root.Role().GetSelectedRow, css: $root.Role().SetColorStatus(ID)" data-toggle="modal"><!--, css: $root.SetColorStatus(ID)-->
                                        <td><span data-bind="text: RowID"></span></td>
                                        <td><span data-bind="text: Name"></span></td>
                                        <td><span data-bind="text: Description"></span></td>
                                        <td><span data-bind="text: LastModifiedBy"></span></td>
                                        <td><span data-bind="text: $root.Role().LocalDate(LastModifiedDate)"></span></td>
                                    </tr>
                                </tbody>
                                <tbody data-bind="visible: Role().Roles().length == 0">
                                    <tr>
                                        <td colspan="6" class="text-center">
                                            No entries available.
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
						</div>
					</div>
					<!-- table responsive end -->
				</div>
				<!-- widget content end -->
			</div>
			<!-- widget slim control end -->

			<!-- widget footer start -->
			<div class="widget-toolbox padding-8 clearfix">
				<div class="row" data-bind="with: Role().GridProperties">
					<!-- pagination size start -->
					<div class="col-sm-6">
						<div class="dataTables_paginate paging_bootstrap pull-left">
							Showing <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
							rows of <span data-bind="text: Total"></span>
							entries
						</div>
					</div>
					<!-- pagination size end -->

					<!-- pagination page jump start -->
					<div class="col-sm-3">
						<div class="dataTables_paginate paging_bootstrap">
							Page <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width:50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page"/>
							of <span data-bind="text: TotalPages"></span>
						</div>
					</div>
					<!-- pagination page jump end -->

					<!-- pagination navigation start -->
					<div class="col-sm-3">
						<div class="dataTables_paginate paging_bootstrap">
							<ul class="pagination">
								<li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
									<a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
										<i class="icon-double-angle-left"></i>
									</a>
								</li>
								<li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
									<a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
										<i class="icon-angle-left"></i>
									</a>
								</li>
								<li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
									<a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
										<i class="icon-angle-right"></i>
									</a>
								</li>
								<li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
									<a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
										<i class="icon-double-angle-right"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
					<!-- pagination navigation end -->

				</div>
			</div>
			<!-- widget footer end -->

		</div>
		<!-- widget main end -->
	</div>
	<!-- widget body end -->
</div>
<!-- widget box end -->

<div data-bind="if: $root.Form().IsRoleMaker()">
	<h4 class="pink">
		<i class="icon-hand-right green"></i>
		<a href="#modal-form" role="button" class="blue" data-toggle="modal" data-bind="click: Form().NewData">
			New Role
		</a>
	</h4>
</div>

<!--Draft-->
<div id="widget-box" class="widget-box">
	<!-- widget header start -->
	<div class="widget-header widget-hea1der-small header-color-dark">
		<h6>Role Draft Table</h6>

		<div class="widget-toolbar">
			<label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
				<i class="blue icon-filter"></i>
				<input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: RoleDraft().GridProperties().AllowFilter" />
				<span class="lbl"></span>
			</label>
			<a href="#" data-bind="click: RoleDraft().GetData" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
				<i class="blue icon-refresh"></i>
			</a>
		</div>
	</div>
	<!-- widget header end -->

	<!-- widget body start -->
	<div class="widget-body">
		<!-- widget main start -->
		<div class="widget-main padding-0">
			<!-- widget slim control start -->
			<div class="slim-scroll" data-height="400">
				<!-- widget content start -->
				<div class="content">

					<!-- table responsive start -->
					<div class="table-responsive">
						<div class="dataTables_wrapper" role="grid">
							<table id="RoleDraft-table" class="table table-striped table-bordered table-hover dataTable">
                                <thead>
                                    <tr data-bind="with: RoleDraft().GridProperties">
                                        <th style="width:50px">No.</th>
                                        <th data-bind="click: function () { Sorting('Name'); }, css: GetSortedColumn('Name')">
										Name</th>
                                        <th data-bind="click: function () { Sorting('Description'); }, css: GetSortedColumn('Description')">
										Description</th>
										<th data-bind="click: function () { Sorting('ActionType'); }, css: GetSortedColumn('ActionType')">
										Action</th>
                                        <th data-bind="click: function () { Sorting('LastModifiedBy'); }, css: GetSortedColumn('LastModifiedBy')">
										Modified By</th>
                                        <th data-bind="click: function () { Sorting('LastModifiedDate'); }, css: GetSortedColumn('LastModifiedDate')">
										Modified Date</th>
                                    </tr>
                                </thead>
                                <thead data-bind="visible: RoleDraft().GridProperties().AllowFilter" class="table-filter">
                                    <tr>
                                        <th class="clear-filter">
                                            <a href="#" data-bind="click: RoleDraft().ClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                <i class="green icon-trash"></i>
                                            </a>
                                        </th>
                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.RoleDraft().FilterName, event: { change: RoleDraft().GridProperties().Filter }" /></th>
                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.RoleDraft().FilterDescription, event: { change: RoleDraft().GridProperties().Filter }" /></th>
										<th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.RoleDraft().FilterAction, event: { change: RoleDraft().GridProperties().Filter }" /></th>
                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.RoleDraft().FilterModifiedBy, event: { change: RoleDraft().GridProperties().Filter }" /></th>
                                        <th><input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.RoleDraft().FilterModifiedDate, event: { change: RoleDraft().GridProperties().Filter }"/></th>
                                    </tr>
                                </thead>
                                <tbody data-bind="foreach: RoleDraft().Roles, visible: RoleDraft().Roles().length > 0">
                                    <tr data-bind="click: $root.RoleDraft().GetSelectedRow" data-toggle="modal">
                                        <td><span data-bind="text: RowID"></span></td>
                                        <td><span data-bind="text: Name"></span></td>
                                        <td><span data-bind="text: Description"></span></td>
										<td><span data-bind="text: ActionType"></span></td>
                                        <td><span data-bind="text: LastModifiedBy"></span></td>
                                        <td><span data-bind="text: $root.RoleDraft().LocalDate(LastModifiedDate)"></span></td>
                                    </tr>
                                </tbody>
                                <tbody data-bind="visible: RoleDraft().Roles().length == 0">
                                    <tr>
                                        <td colspan="6" class="text-center">
                                            No entries available.
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
						</div>
					</div>
					<!-- table responsive end -->
				</div>
				<!-- widget content end -->
			</div>
			<!-- widget slim control end -->

			<!-- widget footer start -->
			<div class="widget-toolbox padding-8 clearfix">
				<div class="row" data-bind="with: RoleDraft().GridProperties">
					<!-- pagination size start -->
					<div class="col-sm-6">
						<div class="dataTables_paginate paging_bootstrap pull-left">
							Showing <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
							rows of <span data-bind="text: Total"></span>
							entries
						</div>
					</div>
					<!-- pagination size end -->

					<!-- pagination page jump start -->
					<div class="col-sm-3">
						<div class="dataTables_paginate paging_bootstrap">
							Page <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width:50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page"/>
							of <span data-bind="text: TotalPages"></span>
						</div>
					</div>
					<!-- pagination page jump end -->

					<!-- pagination navigation start -->
					<div class="col-sm-3">
						<div class="dataTables_paginate paging_bootstrap">
							<ul class="pagination">
								<li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
									<a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
										<i class="icon-double-angle-left"></i>
									</a>
								</li>
								<li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
									<a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
										<i class="icon-angle-left"></i>
									</a>
								</li>
								<li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
									<a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
										<i class="icon-angle-right"></i>
									</a>
								</li>
								<li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
									<a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
										<i class="icon-double-angle-right"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
					<!-- pagination navigation end -->

				</div>
			</div>
			<!-- widget footer end -->

		</div>
		<!-- widget main end -->
	</div>
	<!-- widget body end -->
</div>
<!--End Draft-->

<!-- modal form start -->
<div id="modal-form" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
	<div class="modal-dialog" style="width:49%">
		<div class="modal-content" >
			<!-- modal header start -->
			<div class="modal-header">
                <h4 class="blue bigger">
                    <span data-bind="if: $root.Form().IsNewData()">Create a new 
					Role Parameter</span>
                    <span data-bind="if: !$root.Form().IsNewData()">Modify a 
					Role Parameter</span>
                </h4>
            </div>
			<!-- modal header end -->

			<!-- modal body start -->
			<div class="modal-body overflow-visible">
				<div class="row">
					<div class="col-xs-12">
						<!-- modal body form start -->
						<div id="customer-form" class="form-horizontal" role="form">
							<div class="space-4" data-bind="visible: $root.Form().IsNewData()"></div>
							<div class="form-group" data-bind="visible: $root.Form().IsNewData()">
								<label class="col-sm-4 control-label no-padding-right" data-bind="visible: $root.Form().IsNewData" for="IsNewRole">
								New Role</label>
								<div class="col-sm-8" data-bind="visible: $root.Form().IsNewData()">
									<div class="clearfix">
                        				<input id="IsNewRole" name="IsNewRole" data-bind="checked: $root.Form().IsNewRole, visible: $root.Form().IsNewData" class="ace ace-switch ace-switch-6" type="checkbox">
                        				<span class="lbl"></span>	
                   					</div>
								</div>
							</div>

							
							<div class="space-4" data-bind="visible: $root.Form().IsNewData() && $root.Form().IsNewRole() == false"></div>
							
							<div class="form-group" data-bind="visible: $root.Form().IsNewData() && $root.Form().IsNewRole() == false">
								<label class="col-sm-4 control-label no-padding-right" data-bind="visible: $root.Form().IsNewData() && $root.Form().IsNewRole() == false" for="ExistingRole">
								Copy From Existing Role</label>
								<div class="col-sm-8" data-bind="visible: $root.Form().IsNewData() && $root.Form().IsNewRole() == false">
									<div class="clearfix">
                        				<select id="ExistingRole" name="ExistingRole" data-bind="visible: $root.Form().IsNewData() && $root.Form().IsNewRole() == false, value: $root.Form().ExistingRole().ID, options: $root.Form().ddlExistingRole, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Choose...', event: { change: $root.Role().CopyFromExistingRole }" class="ace ace-switch ace-switch-6" data-rule-required="true"></select>
                                        <label class="control-label bolder text-danger" for="Name">*</label>
                        			</div>
								</div>
							</div>
							
							<div class="space-4"></div>
							<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="Name">
								Name</label>
								<div class="col-sm-8">
									<div class="clearfix">
										<input type="text" id="Name" name="Name" data-bind="value: $root.Form().Name, disable: $root.Form().Readonly()" class="col-xs-10" data-rule-required = "true" data-rule-maxlength="50"/>
                                        <label class="control-label bolder text-danger" for="Name">*</label>
									</div>
								</div>
							</div>

							<div class="space-4"></div>

							<div class="form-group">
								<label class="col-sm-4 control-label no-padding-right" for="description">
								Description</label>

								<div class="col-sm-8">
									<div class="clearfix">
										<input type="text" id="description" name="description" data-bind="value: $root.Form().Description, disable: $root.Form().Readonly()" class="col-xs-12" data-rule-maxlength="255"/>
									</div>
								</div>
							</div>
                        </div>
						<div class="widget-box">
							<div class="widget-header widget-header-flat">
								<h4>Choose Navigations</h4>
							</div>
							<div class="widget-body slim-scroll" data-height="400">
								<div class="dd" id="nestable"></div>
							</div>
						</div>
						<!-- modal body form end -->
					</div>
				</div>
			</div>
			<!-- modal body end -->

			<!-- modal footer start -->
			<div class="modal-footer">
				<button class="btn btn-sm btn-primary" data-bind="click: $root.Form().save, visible: $root.Form().IsNewData() && !$root.Form().Readonly() && $root.Form().IsRoleMaker()">
                    <i id="ADD" class="icon-save"></i>
                    Save
                </button>
                <button class="btn btn-sm btn-primary" data-bind="click: $root.Form().updatedata, visible: !$root.Form().IsNewData() && !$root.Form().Readonly() && $root.Form().IsRoleMaker()">
                    <i id="UPDATE" class="icon-edit"></i>
                    Update
                </button>
                <button class="btn btn-sm btn-warning" data-bind="click: $root.Form().updatedata, visible: !$root.Form().IsNewData() && !$root.Form().Readonly() && $root.Form().IsRoleMaker()">
                    <i id="DELETE" class="icon-trash"></i>
                    Delete
                </button>
                <button class="btn btn-sm" data-dismiss="modal">
                    <i class="icon-remove"></i>
                    Close
                </button>
			</div>
			<!-- modal footer end -->

		</div>
	</div>
</div>
<!-- modal form end -->
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/additional-methods.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/Role.js"></script>