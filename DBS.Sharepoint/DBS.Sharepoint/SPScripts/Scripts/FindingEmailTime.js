﻿var FindingEmailTime = function (id, EmailTime, Status) {
    var self = this;
    self.FindingEmailTimeID = ko.observable(id);
    self.EmailTime = ko.observable(EmailTime);
    self.Status = ko.observable(Status);
};

var ViewModel = function () {
    var self = this;
    self.updateCallback = function () { };
    self.IsRoleMaker = ko.observable(false);
    self.IsWorkflow = ko.observable(false);
    self.SetUpdateCallback = function (callback) {
        self.updateCallback = callback;
    };

    //Declare observable which will be bind with UI 
    self.FindingEmailTimeID = ko.observable();
    self.EmailTime = ko.observable("");
    self.Status = ko.observable(0);
    self.LastModifiedBy = ko.observable("");
    self.LastModifiedDate = ko.observable("");

    //task adi
    self.WorkflowID = ko.observable("");
    self.Workflow = ko.observable("");
    self.ApprovalID = ko.observable("");

    //dropdown
    self.ddlFindingEmailTime = ko.observableArray([]);

    //Filter
    self.FilterEmailTime = ko.observable("");
    self.FilterStatus = ko.observable("");
    self.FilterLastModifiedBy = ko.observable("");
    self.FilterLastModifiedDate = ko.observable("");

    // New Data flag
    self.IsNewData = ko.observable(false);
    self.Readonly = ko.observable(false);

    //Declare an ObservableArray for Storing the JSON Response
    self.FindingEmailTime = ko.observableArray([]);

    // grid properties
    self.GridProperties = ko.observable();
    self.GridProperties(new GridPropertiesModel(GetData));

    //bind clear filter
    self.ClearFilters = function () {
        Self.FilterEmailTime("");
        Self.FilterStatus("");
        Self.FilterLastModifiedBy("");
        Self.FilterLastModifiedDate("");
        GetData();
    }

    // set default sorting
    self.GridProperties().SortColumn("EmailTime");
    self.GridProperties().SortOrder("ASC");

    //bind get data funtion to view
    self.GetData = function () {
        GetData();
    };

   
    //The Object which stored data entered in the observables
    var FindingEmailTime = {
        FindingEmailTimeID: self.FindingEmailTimeID,
        EmailTime: self.EmailTime,
        Status: self.Status,
        LastModifiedBy: self.LastModifiedBy,
        LastModifiedDate: self.LastModifieDate
    }

    // function Add Data
    self.save = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {

                    //Ajax call to insert the Products
                    $.ajax({
                        type: "POST",
                        url: api.server + api.url.FindingEmailTime,
                        data: ko.toJSON(FindingEmailTime), //Convert the Observable Data into JSON
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {
                                // send notification
                                ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // hide current popup window
                                $("#modal-form").modal('hide');

                                // refresh data
                                GetData();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-success', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-success', true);
                        }
                    });
                }
            });
        }
    };

    //Function Update Data
    self.update = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            // hide current popup window
            //$("#modal-form").modal('hide');

            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {
                    //Ajax call to update the Customer
                    $.ajax({
                        type: "PUT",
                        url: api.server + api.url.FindingEmailTime + "/" + self.FindingEmailTimeID(),
                        data: ko.toJSON(FindingEmailTime),
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {
                                // send notification
                                ShowNotification('Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // hide current popup window
                                $("#modal-form").modal('hide');

                                // refresh data
                                GetData();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-success', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-success', true);
                        }
                    });
                }
            });
        }
    };

    //Function Delete Data
    self.delete = function () {
        // hide current popup window
        //$("#modal-form").modal('hide');

        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                $("#modal-form").modal('show');
            } else {
                //Ajax call to delete the Customer
                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.FindingEmailTime + "/" + FindingEmailTime.FindingEmailTimeID(),
                    data: ko.toJSON(FindingEmailTime),
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if (jqXHR.status = 200) {
                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                            // refresh data
                            GetData();
                        } else
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
            }
        });
    };

    //Insert new 
    self.NewData = function () {
        self.IsNewData(true);
        self.Readonly(false);
        self.FindingEmailTimeID(0);
        self.EmailTime('');
        self.Status('0');
        self.LastModifiedBy('');
        self.LastModifiedDate('');
        self.WorkflowID(0);
        self.Workflow('');
    }

    //Get All Data
    function GetData() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.FindingEmailTime,
            params: {
                page: self.GridProperties().Page(),
                size: self.GridProperties().Size(),
                sort_column: self.GridProperties().SortColumn(),
                sort_order: self.GridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);
            Helper.Ajax.Post(options, OnSuccessGetData, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetData, OnError, OnAlways);
        }
    }

    self.IsPermited = function (IsPermitedResult) {
        //Ajax call to delete the Customer
        spUser = $.cookie(api.cookie.spUser);

        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/CheckUserPermited/" + _spFriendlyUrlPageContextInfo.title,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status == 200) {
                    //compare user role to page permission to get checker validation
                    console.log("spUser Roles : " + ko.toJSON(spUser.Roles));
                    console.log("spUser Permited : " + data);
                    for (i = 0; i < spUser.Roles.length; i++) {
                        for (j = 0; j < data.length; j++) {
                            if (Const_RoleName[spUser.Roles[i].ID] == data[j]) { //if (spUser.Roles[i].Name == data[j]) {
                                if (Const_RoleName[spUser.Roles[i].ID].endsWith('Maker')) { //if (spUser.Roles[i].Name.endsWith('Maker')) {
                                    self.IsRoleMaker(true);
                                    return;
                                }
                                else {
                                    self.IsRoleMaker(false);
                                }
                            }
                        }
                    }
                    console.log("IsRoleMaker : " + self.IsRoleMaker());
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return "";
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-17
    };

    //Get Filter Column Value
    function GetFilteredColumns() {
        var filters = [];
        if (self.FilterEmailTime() != "") filters.push({ Field: 'EmailTime', Value: self.FilterEmailTime() });
        // if (self.FilterSatus() != "") filters.push({ Field: 'Status', Value: self.FilterStatus() });

        //role
        if (self.FilterLastModifiedBy() != "") filters.push({ Field: 'LastModifiedBy', Value: self.FilterLastModifiedBy() });
        if (self.FilterLastModifiedDate() != "") filter.push({ Field: 'LastModifiedDate', Value: self.FilterLastModifiedDate() });

        return filters;
    }

    //function to display record to be Update
    self.GetSelectedRow = function (data) {

        self.IsNewData(false);

        if (self.IsWorkflow()) $("#modal-form-workflow").modal('show');
        else $("#modal-form").modal('show');

        self.FindingEmailTimeID(data.FindingEmailTimeID);
        self.EmailTime(data.EmailTime);
        self.Status(data.Status);
        self.LastModifiedBy(data.LastModifiedBy);
        self.LastModifiedDate(data.LastModifiedDate);
    }

    // On success GetData callback
    function OnSuccessGetData(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.FindingEmailTime(data.Rows);
            //console.log(data.Rows);
            self.GridProperties().Page(data['Page']);
            self.GridProperties().Size(data['Size']);
            self.GridProperties().Total(data['Total']);
            self.GridProperties().TotalPages(Math.ceil(self.GridProperties().Total() / self.GridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    // On Error callback
    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    // On Always callback
    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }

    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }

    });
}