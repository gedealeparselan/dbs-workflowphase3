﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Web;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using System.Data.Entity.Validation;
using System.Diagnostics;


namespace DBS.WebAPI.Models
{
    #region property
    [ModelName("ExceptionHandlingParameter")]
    public class ExceptionHandlingParameterModel
    {
        /// <summary>
        /// Exception Handling Parameter ID
        /// </summary>
        public int ID { get; set; }



        /// <summary>
        /// Exception Handling Parameter Code
        /// </summary>
        public string Code { get; set; }


        /// <summary>
        /// Exception Handling Parameter Name
        /// </summary>
        public string Name { get; set; }


        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>

        public string LastModifiedBy { get; set; }
    }

    public class ExceptionHandlingParameterRow: ExceptionHandlingParameterModel
    {
        public int RowID { get; set; }
    }

    public class ExceptionHandlingParameterGrid: Grid
    {
        public IList<ExceptionHandlingParameterRow> Rows { get; set; }
    }
    #endregion

    #region Filter

    public class ExceptionHandlingParameterFilter : Filter { }
    #endregion

    #region Interface
    public interface IExceptionHandlingParameterRepository: IDisposable
    {
        bool GetExceptionHandlingParameter(ref IList<ExceptionHandlingParameterModel> exceptionhandlingparameter, ref string message);
    }
    #endregion

    #region Repository
    public class ExceptionHandlingParameterRepository: IExceptionHandlingParameterRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();
        public bool GetExceptionHandlingParameter(ref IList<ExceptionHandlingParameterModel> exceptionhandlingparameter, ref string message)
        {
            bool isSuccess = false;
            try
            {

                using (DBSEntities context = new DBSEntities())
                {
                    exceptionhandlingparameter = (from a in context.ExceptionHandlingParameters
                                                  orderby new { a.Name }
                                                  select new ExceptionHandlingParameterModel
                                                  {
                                                      ID = a.ExceptionHandlingParameterID,
                                                      Code = a.Code,
                                                      Name = a.Name,                                  
                                                      LastModifiedBy = a.CreateBy,
                                                      LastModifiedDate = a.CreateDate
                                                  }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;

        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        currentUser = null;
                        context.Dispose();
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}