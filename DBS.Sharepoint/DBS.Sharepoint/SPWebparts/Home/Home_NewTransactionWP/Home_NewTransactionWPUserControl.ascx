﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Home_NewTransactionWPUserControl.ascx.cs" Inherits="DBS.Sharepoint.SPWebparts.Home.Home_NewTransactionWP.Home_NewTransactionWPUserControl" %>


<h3 class="header smaller no-margin-top lighter dark">New Transaction</h3>
<div class="modal-body overflow-visible">
    <div class="row" data-bind="with: TransactionModel">
        <div class="col-xs-12">
            <div id="customer-form" class="form-horizontal" role="form">
                <div class="form-group">
                    <label class="col-sm-3 control-label bolder no-padding-right">
                        <span data-bind="text: $root.LocalDate(CreateDate(), true, true)"></span>
                    </label>

                    <label class="col-sm-4 pull-right">
                        <b>Application ID</b> : <span data-bind="text: ApplicationID"></span><br />
                        <b>User Name</b> : <span data-bind="text: $root.SPUser().DisplayName"></span>
                    </label>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label bolder no-padding-right" for="top-urgent">Top Urgent</label>

                    <div class="col-xs-1">
                        <label>
                            <input class="ace ace-switch ace-switch-6" type="checkbox" data-bind="checked: IsTopUrgent">
                            <span class="lbl"></span>
                        </label>
                    </div>

                     <label class="col-sm-2 control-label bolder no-padding-right" for="top-urgent">Top Urgent Chain</label>

                    <div class="col-xs-2">
                        <label>
                            <input class="ace ace-switch ace-switch-6" type="checkbox" data-bind="checked: IsTopUrgentChain">
                            <span class="lbl"></span>
                        </label>
                    </div>

                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label bolder no-padding-right" for="customer-name">Customer Name</label>

                    <div class="col-sm-6">
                        <div class="clearfix">
                            <input type="text" autocomplete="off" class="col-sm-11" name="customer-name" id="customer-name" data-rule-required="true" data-bind="value: Customer().Name, disable: IsDraft" />
                            <label class="control-label bolder text-danger" for="customer-name">*</label>
                        </div>
                    </div>

                    <div class="col-xs-3">
                        <label>
                            <input class="ace ace-switch ace-switch-6" type="checkbox" data-bind="checked: IsNewCustomer, event: { click: $root.IsNewCustomerOnChange }, disable: IsDraft">
                            <span class="lbl"></span>
                        </label>
                        <span>&nbsp; <b>New Customer</b></span>

                    </div>


                    <!--<label class="col-sm-3">
                        <input id="is-new-customer" name="is-new-customer" class="ace ace-switch ace-switch-6" type="checkbox" data-bind="checked: IsNewCustomer, event: { click: $root.IsNewCustomerOnChange }, disable: IsDraft" />
                        <span class="lbl">&nbsp; <b>New Customer</b></span>
                    </label>-->
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label bolder no-padding-right" for="cif">CIF</label>

                    <div class="col-sm-9">
                        <div class="clearfix">
                            <input type="text" autocomplete="off" class="col-sm-3" name="cif" id="cif" data-bind="value: Customer().CIF, disable: !IsNewCustomer()" />
                            <label class="control-label bolder text-danger" for="customer-name" data-bind="visible: IsNewCustomer()">*</label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label bolder no-padding-right" for="product">Product</label>

                    <div class="col-sm-9">
                        <div class="clearfix">
                            <select id="product" name="product" data-rule-required="true" data-rule-value="true" data-bind="options: $root.Parameter().Products, optionsText: function(item) { if(item.Code != null) return item.Code + ' (' + item.Name + ')'}, optionsValue: 'ID', optionsCaption: 'Please Select...', value: $root.Selected().Product, event: { change: $root.OnProductChange }, disable: IsDraft"></select>
                            <label class="control-label bolder text-danger" for="product">*</label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label bolder no-padding-right" for="currency">Transaction Currency</label>

                    <div class="col-sm-9">
                        <div class="clearfix">
                            <select id="currency" name="currency" data-rule-required="true" data-rule-value="true" data-bind="options: $root.Parameter().Currencies, optionsText: function(item) { if(item.Code != null) return item.Code + ' (' + item.Description + ')'}, optionsValue: 'ID', optionsCaption: 'Please Select...', value: $root.Selected().Currency, disable: !$root.IsEditable()"></select>
                            <label class="control-label bolder text-danger" for="currency">*</label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label bolder no-padding-right" for="trxn-amount">Trxn Amount</label>

                    <div class="col-sm-9">
                        <div class="clearfix">
                            <input type="text" autocomplete="off" class="col-sm-4 align-right" name="trxn-amount" id="trxn-amount" data-in="" data-rule-required="true" data-rule-number="true" data-bind="disable: !$root.IsEditable()" />
                            <label class="control-label bolder text-danger" for="trxn-amount">*</label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label bolder no-padding-right" for="rate">Rate</label>

                    <div class="col-sm-9">
                        <div class="clearfix">
                            <input type="text" autocomplete="off" class="col-sm-4 align-right" name="rate" id="rate" data-in="" data-rule-required="true" data-rule-number="true" data-bind="value: $root.Rate, disable: !$root.IsEditable()" />
                            <label class="control-label bolder text-danger" for="rate">*</label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label bolder no-padding-right" for="eqv-usd">Eqv. USD</label>

                    <div class="col-sm-9">
                        <div class="clearfix">
                            <input type="text" autocomplete="off" class="col-sm-4 align-right" disabled="disabled" name="eqv-usd" id="eqv-usd" data-rule-required="true" data-rule-number="true" data-bind="value: formatNumber(AmountUSD())" />
                            <label class="control-label bolder text-danger" for="eqv-usd">*</label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label bolder no-padding-right" for="channel">Channel</label>

                    <div class="col-sm-9">
                        <div class="clearfix">
                            <select id="channel" name="channel" data-rule-required="true" data-rule-value="true" data-bind="options: $root.Parameter().Channels, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Please Select...', value: $root.Selected().Channel, disable: !$root.IsEditable()"></select>
                            <label class="control-label bolder text-danger" for="channel">*</label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label bolder no-padding-right" for="debit-acc-number">Debit Acc Number</label>

                    <div class="col-sm-9">
                        <div class="clearfix">
                            <!-- ko ifnot: IsNewCustomer -->
                            <select id="debit-acc-number" name="debit-acc-number" data-rule-required="true" data-rule-value="true" data-bind="options: Customer().Accounts, optionsText: function(item) { if(item.Currency.Code != null) return item.AccountNumber + ' (' + item.Currency.Code + ')'}, optionsValue: 'AccountNumber', optionsCaption: 'Please Select...', value: $root.Selected().Account,event:{ change:$root.CheckEmptyAccountNumber() }"></select>
                            <!-- /ko -->
                            <!-- ko if: IsNewCustomer -->
                            <input type="text" autocomplete="off" class="col-sm-3" name="debit-acc-number" id="debit-acc-number" data-rule-required="true" data-rule-number="true" data-bind="value: Account().AccountNumber" />
                            <!-- /ko -->
                            <label class="control-label bolder text-danger" for="debit-acc-number">*</label>
                            <input type="text" autocomplete="off" name="debit-acc-number-empty" id="debit-acc-number-empty" data-rule-required="true" data-rule-number="true" data-bind="value: OtherAccountNumber,visible: $root.IsEmptyAccountNumber()" />
                            <label class="control-label bolder text-danger" for="debit-acc-number" data-bind="visible: $root.IsEmptyAccountNumber()">*</label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label bolder no-padding-right" for="debit-acc-ccy">Debit Acc CCY</label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <!-- ko ifnot: IsNewCustomer -->
                            <input type="text" autocomplete="off" class="col-sm-1" name="debit-acc-ccy" id="debit-acc-ccy" disabled="disabled" data-bind="visible: !IsNewCustomer() && !$root.IsEmptyAccountNumber(), value: Account().Currency.Code" />
                            <!-- /ko -->
                            <!-- ko if: IsNewCustomer -->
                            <select id="currency" name="currency" data-rule-required="true" data-rule-value="true" data-bind="options: $root.Parameter().Currencies, optionsText: function(item) { return item.Code + ' (' + item.Description + ')'}, optionsValue: 'ID', optionsCaption: 'Please Select...', value: $root.Selected().NewCustomer().Currency"></select>
                            <!-- /ko -->
                            <select id="currency-empty" name="currency-empty" data-rule-required="true" data-rule-value="true" data-bind="options: $root.Parameter().Currencies, optionsText: function(item) { return item.Code + ' (' + item.Description + ')'}, optionsValue: 'ID', optionsCaption: 'Please Select...', value: $root.Selected().DebitCurrency,visible: $root.IsEmptyAccountNumber(),event:{ change:$root.OnChangeAccountCurrency() }"></select>
                            <label class="control-label bolder text-danger" for="debit-acc-number" data-bind="visible: $root.IsEmptyAccountNumber()">*</label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label bolder no-padding-right" for="application-date">Application Date</label>

                    <div class="col-sm-9">
                        <div class="clearfix input-group col-sm-3 no-padding">
                            <input class="form-control date-picker" type="text" autocomplete="off" data-date-format="dd-M-yyyy" id="application-date" name="application-date" data-rule-required="true" data-rule-value="true" data-bind="value: ApplicationDate, disable: !$root.IsEditable()">
                            <span class="input-group-addon">
                                <i class="icon-calendar bigger-110"></i>
                            </span>
                            <label class="control-label bolder text-danger starremove starremovefirefox" style="position:absolute;" for="application-date">*</label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label bolder no-padding-right" for="biz-segment">Biz Segment</label>

                    <div class="col-sm-9">
                        <div class="clearfix">
                            <select id="biz-segment" name="biz-segment" data-rule-required="true" data-rule-value="true" data-bind="options: $root.Parameter().BizSegments, optionsText: function(item){ if(item.Name != null) return item.Name +' ('+ item.Description +')'; }, optionsValue: 'ID', optionsCaption: 'Please Select...', value: $root.Selected().BizSegment, disable: !IsNewCustomer()"></select>
                            <label class="control-label bolder text-danger" for="biz-segment">*</label>
                        </div>
                    </div>
                </div>

                <div class="form-group" data-bind="if:($root.IsTransactionIDRToFCY()||$root.IsFxTransaction())">
                    <!-- ?FXCONDITION YES -->
                    <label class="col-sm-3 control-label bolder no-padding-right" for="product">Product Type</label>

                    <div class="col-sm-9">
                        <div class="clearfix">
                            <select id="book-product-type" name="book-product-type" data-bind="options:$root.Parameter().ProductType, optionsText:'Code',optionsValue: 'ID',optionsCaption: 'Please Select...',value: $root.Selected().ProductType" class="col-xs-3"></select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label bolder no-padding-right" for="bene-name">Bene Name</label>

                    <div class="col-sm-9">
                        <div class="clearfix">
                            <input type="text" autocomplete="off" class="col-sm-6" name="bene-name" id="bene-name" data-bind="value: BeneName, disable: !$root.IsEditable(), event: { keypress: enforceMaxlength }" />
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label bolder no-padding-right" for="bene-bank">Bene Bank</label>

                    <div class="col-sm-9">
                        <div class="clearfix">
                            <input type="text" autocomplete="off" class="col-sm-6" name="bank-name" id="bank-name" data-bind="value: Bank().Description, disable: IsDraft" />
                            <%--<label class="control-label bolder text-danger" for="biz-segment">*</label>--%>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label bolder no-padding-right" for="bank-code">Swift Code/Bank Code</label>

                    <div class="col-sm-9">
                        <div class="clearfix">
                            <input type="text" autocomplete="off" class="col-sm-2" name="bank-code" id="bank-code" data-bind="value: Bank().SwiftCode, enable: !$root.IsOtherBank()" />
                            <%--<label class="control-label bolder text-danger" for="biz-segment" data-bind="visible: !$root.IsOtherBank()">*</label>--%>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label bolder no-padding-right" for="bene-acc-number">Bene Acc Number</label>

                    <div class="col-sm-9">
                        <div class="clearfix">
                            <!--<input type="text" autocomplete="off" class="col-sm-6" name="bene-acc-number" id="bene-acc-number" disabled="disabled" data-bind="value: Bank().BankAccount" />-->
                            <input type="text" autocomplete="off" class="col-sm-6" name="bene-acc-number" id="bene-acc-number" onkeyup="formatAccount()" data-in="" data-bind="value: $root.BeneAccNumberMask()" />
                            <%--<label class="control-label bolder text-danger" for="biz-segment">*</label>--%>
                        </div>
                    </div>
                </div>
                <div data-bind="if: Product().Name == 'OTT' || Product().Name == 'RTGS' || Product().Name == 'SKN', attr: { class: 'form-group' }">
                    <label class="col-sm-3 control-label bolder no-padding-right" for="bank-charges">Bank Charges</label>

                    <div class="col-sm-9">
                        <div class="clearfix">
                            <select id="bank-charges" name="bank-charges" data-rule-required="true" data-rule-value="true" data-bind="options: $root.Parameter().BankCharges, optionsText: 'Code', optionsValue: 'ID', optionsCaption: 'Please Select...', value: $root.Selected().BankCharges"></select>
                            <label class="control-label bolder text-danger" for="biz-segment">*</label>
                        </div>
                    </div>
                </div>

                <div data-bind="if: Product().Name == 'RTGS', attr: { class: 'form-group' }">
                    <label class="col-sm-3 control-label bolder no-padding-right" for="agent-charges">Agent Charges</label>

                    <div class="col-sm-9">
                        <div class="clearfix">
                            <select id="agent-charges" name="agent-charges" data-rule-required="true" data-rule-value="true" data-bind="options: $root.Parameter().AgentCharges, optionsText: 'Code', optionsValue: 'ID', optionsCaption: 'Please Select...', value: $root.Selected().AgentCharges"></select>
                            <label class="control-label bolder text-danger" for="biz-segment">*</label>
                        </div>
                    </div>
                </div>

                <div data-bind="if: Product().Name == 'SKN', attr: { class: 'form-group' }">
                    <label class="col-sm-3 control-label bolder no-padding-right" for="type">Type</label>

                    <div class="col-sm-9">
                        <div class="clearfix">
                            <input type="text" autocomplete="off" class="col-sm-4" id="type" name="type" data-rule-required="true" data-rule-number="true" data-bind="value: Type" />
                            <label class="control-label bolder text-danger" for="biz-segment">*</label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label bolder no-padding-right" for="citizen">Citizen</label>

                    <div class="col-xs-3">
                        <label>
                            <input class="ace ace-switch ace-switch-6" type="checkbox" data-bind="checked: IsCitizen">
                            <span class="lbl"></span>
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label bolder no-padding-right" for="resident">Resident</label>

                    <div class="col-xs-3">
                        <label>
                            <input class="ace ace-switch ace-switch-6" type="checkbox" data-bind="checked: IsResident">
                            <span class="lbl"></span>
                        </label>
                    </div>
                </div>

                <!--<div class="form-group">
                    <label class="col-sm-3 control-label bolder no-padding-right" for="payment-details">Payment Details</label>

                    <div class="col-sm-9">
                        <div class="clearfix">
                            <textarea class="col-sm-12" name="payment-details" id="payment-details" rows="6" data-rule-required="false" data-bind="value: PaymentDetails, disable: !$root.IsEditable()"></textarea>
                        </div>
                    </div>
                </div>-->

    <div class="form-group">
                    <label class="col-sm-3 control-label bolder no-padding-right" for="lld-code">LLD Code</label>

                    <div class="col-sm-9">
                        <div class="clearfix">
                            <select id="bene-bank" name="lld-code" class="col-sm-12"
                                    data-bind="options: $root.Parameter().LLDs,
    optionsText: function(item) { if(item.Code != null) return item.Code + ' (' + item.Description + ')'},
    optionsValue: 'ID',
    optionsCaption: 'Please Select...',
    value: $root.Selected().LLD,
    disable: !$root.IsEditable()"></select>
                            <!--<input type="text" autocomplete="off" class="col-sm-12 ui-autocomplete-input valid" name="lld-code" id="lld-code" data-bind="value: LLD().Description, disable: !$root.IsEditable()">-->

                        </div>
                    </div>
                </div>

<%--                <div class="form-group">
                    <label class="col-sm-3 control-label bolder no-padding-right" for="lld-desc">LLD Info</label>

                    <div class="col-sm-9">
                        <div class="clearfix">
                            <input type="text" autocomplete="off" class="col-sm-10 ui-autocomplete-input valid" name="lld-code" id="lld-code" data-bind="value: LLD().Description, disable: !$root.IsEditable()">
                        </div>
                    </div>
                </div>--%>

                <!--<div class="form-group" data-bind="if: $root.SPUser().Roles[0].Name != 'DBS PPU HEAD OFFICE Maker'">-->
                <div class="form-group" data-bind="if:false">
                    <label class="col-sm-3 control-label bolder no-padding-right" for="check-signature-verification">Signature Verification</label>

                    <label class="col-sm-9">
                        <input name="check-signature-verification" class="ace ace-switch ace-switch-6" type="checkbox" disabled="disabled" data-bind="checked: IsSignatureVerified" /><!--, disable: !$root.IsEditable()-->
                        <span class="lbl"></span>
                    </label>
                </div>

                <!--<div class="form-group" data-bind="if: $root.SPUser().Roles[0].Name != 'DBS PPU HEAD OFFICE Maker'">-->
                <div class="form-group" data-bind="if:false">
                    <label class="col-sm-3 control-label bolder no-padding-right" for="check-dormant-account">Dormant Account</label>

                    <label class="col-sm-9">
                        <input name="check-dormant-account" class="ace ace-switch ace-switch-6" type="checkbox" disabled="disabled" data-bind="checked: IsDormantAccount" /><!--, disable: !$root.IsEditable()-->
                        <span class="lbl"></span>
                    </label>
                </div>

                <!--<div class="form-group" data-bind="if: $root.SPUser().Roles[0].Name != 'DBS PPU HEAD OFFICE Maker'">-->
                <div class="form-group" data-bind="if:false">
                    <label class="col-sm-3 control-label bolder no-padding-right" for="check-freeze-account">Freeze Account</label>

                    <label class="col-sm-9">
                        <input name="check-freeze-account" class="ace ace-switch ace-switch-6" type="checkbox" disabled="disabled" data-bind="checked: IsFreezeAccount" /><!--, disable: !$root.IsEditable()-->
                        <span class="lbl"></span>
                    </label>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label bolder no-padding-right" for="purpose-detail">Payment Details</label>

                    <div class="col-sm-9">
                        <div class="clearfix">
                            <textarea class="col-sm-12" name="purpose-detail" id="purpose-detail" rows="6" data-rule-required="false" data-bind="value: PaymentDetails, disable: !$root.IsEditable()"></textarea>
                        </div>
                    </div>
                </div>

                <!--<div class="form-group" data-bind="if: $root.SPUser().Roles[0].Name != 'DBS PPU HEAD OFFICE Maker'">-->
                <div class="form-group" data-bind="if:false">
                    <label class="col-sm-3 control-label bolder no-padding-right" for="others">Others</label>

                    <div class="col-sm-9">
                        <div class="clearfix">
                            <textarea class="col-sm-12" name="others" id="others" rows="6" data-rule-required="false" data-bind="value: Others, disable: !$root.IsEditable()"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="space-4"></div>

            <!--- Added Customer Underlying Start //chandra -->
            <!--<div class="padding-4" data-bind="if: (Currency().Code != 'IDR' && (Account().Currency.Code == 'IDR' || DebitCurrency().Code == 'IDR'))">--> <!-- ?FXCONDITION YES -->
            <div class="padding-4" data-bind="visible: $root.IsFxTransaction() == true || $root.IsFxTransactionToIDR() == true">
            <!--<div class="padding-4" data-bind="if: ($root.IsFxTransaction() || $root.IsFxTransactionToIDR())">-->
                <h3 class="header smaller lighter dark">
                    Un-utilize Underlying Documents
                    <button class="btn btn-sm btn-primary pull-right" data-bind="click: $root.NewDataUnderlying, disable: !$root.IsEditable()">
                        <i class="icon-plus"></i>
                        Add Underlying
                    </button>
                </h3>
                <!-- widget box start -->
                <div id="widget-box" class="widget-box">
                    <!-- widget header start -->
                    <div class="widget-header widget-hea1der-small header-color-dark">
                        <h6>Underlying Table</h6>

                        <div class="widget-toolbar">
                            <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                <i class="blue icon-filter"></i>
                                <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: $root.UnderlyingGridProperties().AllowFilter" />
                                
                                <span class="lbl"></span>
                            </label>                           
                                <a href="#" data-bind="click: $root.GetDataUnderlying" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                <i class="blue icon-refresh"></i>
                            </a>
                        </div>
                    </div>
                    <!-- widget header end -->
                    <!-- widget body start -->
                    <div class="widget-body">
                        <!-- widget main start -->
                        <div class="widget-main padding-0">
                            <!-- widget slim control start -->
                            <div class="slim-scroll" data-height="200">
                                <!-- widget content start -->
                                <div class="content">

                                    <!-- table responsive start -->
                                    <div class="table-responsive">
                                        <div class="dataTables_wrapper" role="grid">
                                            <table id="Customer Underlying-table" class="table table-striped table-bordered table-hover dataTable">
                                                <thead>
                                                    <tr data-bind="with: $root.UnderlyingGridProperties">
                                                        <th style="width:50px">No.</th>
                                                        <th style="width:50px">Utilize</th>
                                                        <th data-bind="click: function () { Sorting('StatementLetter'); }, css: GetSortedColumn('StatementLetter')">Statement Letter</th>
                                                        <th data-bind="click: function () { Sorting('UnderlyingDocument'); }, css: GetSortedColumn('UnderlyingDocument')">Underlying Document</th>
                                                        <th data-bind="click: function () { Sorting('DocumentType'); }, css: GetSortedColumn('DocumentType')">Type of Doc</th>
                                                        <th data-bind="click: function () { Sorting('Currency'); }, css: GetSortedColumn('Currency')">Currency</th>
                                                        <th data-bind="click: function () { Sorting('Amount'); }, css: GetSortedColumn('Amount')">Amount</th>
                                                        <th data-bind="click: function () { Sorting('AvailableAmount'); }, css: GetSortedColumn('AvailableAmount')">Available Amount in USD</th>
                                                        <th data-bind="click: function () { Sorting('DateOfUnderlying'); }, css: GetSortedColumn('DateOfUnderlying')">Date</th>
                                                        <th data-bind="click: function () { Sorting('SupplierName'); }, css: GetSortedColumn('SupplierName')">Supplier Name</th>
                                                        <th data-bind="click: function () { Sorting('AttachmentNo'); }, css: GetSortedColumn('AttachmentNo')">Attachment No</th>
                                                        <th data-bind="click: function () { Sorting('ReferenceNumber'); }, css: GetSortedColumn('ReferenceNumber')">Reference Number</th>
                                                        <th style="width:50px">Action</th>
                                                    </tr>
                                                </thead>
                                                <thead data-bind="visible: $root.UnderlyingGridProperties().AllowFilter" class="table-filter">
                                                    <tr>
                                                        <th class="clear-filter">
                                                            <a href="#" data-bind="click: $root.UnderlyingClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                <i class="green icon-trash"></i>
                                                            </a>
                                                        </th>
                                                        <th class="clear-filter"></th>
                                                        <th><input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterStatementLetter, event: { change: $root.UnderlyingGridProperties().Filter }" /></th>
                                                        <th><input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterUnderlyingDocument, event: { change: $root.UnderlyingGridProperties().Filter }" /></th>
                                                        <th><input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterDocumentType, event: { change: $root.UnderlyingGridProperties().Filter }" /></th>
                                                        <th><input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterCurrency, event: { change: $root.UnderlyingGridProperties().Filter }" /></th>
                                                        <th><input type="text" autocomplete="off" class="input-sm col-xs-12 input-numericonly" data-bind="value: $root.UnderlyingFilterAmount, event: { change: $root.UnderlyingGridProperties().Filter }" /></th>
                                                        <th><input type="text" autocomplete="off" class="input-sm col-xs-12 input-numericonly" data-bind="value: $root.UnderlyingFilterAvailableAmount, event: { change: $root.UnderlyingGridProperties().Filter }" /></th>
                                                        <th><input type="text" autocomplete="off" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.UnderlyingFilterDateOfUnderlying, event: { change: $root.UnderlyingGridProperties().Filter }" /></th>
                                                        <th><input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterSupplierName, event: { change: $root.UnderlyingGridProperties().Filter }" /></th>
                                                        <th><input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterAttachmentNo, event: { change: $root.UnderlyingGridProperties().Filter }" /></th>
                                                        <th><input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterReferenceNumber, event: { change: $root.UnderlyingGridProperties().Filter }" /></th>
                                                        <th class="clear-filter"></th>
                                                    </tr>
                                                </thead>
                                                <tbody data-bind="foreach: $root.CustomerUnderlyings, visible: $root.CustomerUnderlyings().length > 0">
                                                    <tr>
                                                        <td><span data-bind="text: RowID"></span></td>
                                                        <td align="center"><input type="checkbox" id="isUtilize" name="isUtilize" data-bind="checked:IsEnable,event: {change:function(data){$root.onSelectionUtilize($index(),$data)}}" /></td>
                                                        <td><span data-bind="text: StatementLetter.Name"></span></td>
                                                        <td><span data-bind="text: UnderlyingDocument.Name"></span></td>
                                                        <td><span data-bind="text: DocumentType.Name"></span></td>
                                                        <td><span data-bind="text: Currency.Code"></span></td>
                                                        <td><span data-bind="text: formatNumber(Amount)"></span></td>
                                                        <td><span data-bind="text: formatNumber(AvailableAmount)"></span></td>
                                                        <td><span data-bind="text: $root.LocalDate(DateOfUnderlying,true,false)"></span></td>
                                                        <td><span data-bind="text: SupplierName"></span></td>
                                                        <td><span data-bind="text: AttachmentNo"></span></td>
                                                        <td><span data-bind="text: ReferenceNumber"></span></td>
                                                        <td><span class="label label-info arrowed-in-right arrowed" data-toggle="modal" data-target="#modal-form-Underlying" data-bind="click: $root.GetUnderlyingSelectedRow,text:IsUtilize?'view':'Update'"></span></td>
                                                    </tr>
                                                </tbody>
                                                <tbody data-bind="visible: $root.CustomerUnderlyings().length == 0">
                                                    <tr>
                                                        <td colspan="13" class="text-center">
                                                            No entries available.
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- table responsive end -->
                                </div>
                                <!-- widget content end -->
                            </div>
                            <!-- widget slim control end -->
                            <!-- widget footer start -->

                            <div class="widget-toolbox padding-8 clearfix">
                                <div class="row" data-bind="with: $root.UnderlyingGridProperties">
                                    <!-- pagination size start -->
                                    <div class="col-sm-6">
                                        <div class="dataTables_paginate paging_bootstrap pull-left">
                                            Showing <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                            rows of <span data-bind="text: Total"></span>
                                            entries
                                        </div>
                                    </div>
                                    <!-- pagination size end -->
                                    <!-- pagination page jump start -->
                                    <div class="col-sm-3">
                                        <div class="dataTables_paginate paging_bootstrap">
                                            Page <input type="text" autocomplete="off" data-bind="value: Page, event: { change: OnPageChange }" style="width:50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                            of <span data-bind="text: TotalPages"></span>
                                        </div>
                                    </div>
                                    <!-- pagination page jump end -->
                                    <!-- pagination navigation start -->
                                    <div class="col-sm-3">
                                        <div class="dataTables_paginate paging_bootstrap">
                                            <ul class="pagination">
                                                <li data-bind="click: FirstPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                        <i class="icon-double-angle-left"></i>
                                                    </a>
                                                </li>
                                                <li data-bind="click: PreviousPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                        <i class="icon-angle-left"></i>
                                                    </a>
                                                </li>
                                                <li data-bind="click: NextPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                        <i class="icon-angle-right"></i>
                                                    </a>
                                                </li>
                                                <li data-bind="click: LastPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                        <i class="icon-double-angle-right"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- pagination navigation end -->

                                </div>
                            </div>

                            <!-- widget footer end -->

                        </div>
                        <!-- widget main end -->
                    </div>
                    <!-- widget body end -->
                </div>
                <!-- widget box end -->


            </div>
            <!--- Added form custome Customer Underlying End -->
            <div class="space-10"></div>
            <!--<div data-bind="if: (Currency().Code != 'IDR' && (Account().Currency.Code == 'IDR' || DebitCurrency().Code == 'IDR'))">--> <!-- ?FXCONDITION YES -->
            <div data-bind="if: ($root.IsFxTransaction() || $root.IsFxTransactionToIDR())">
                <div class="form-horizontal" role="form">

                    <div class="form-group">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="underlying-code">Underlying Code</label>
                        <div class="col-sm-9">
                            <div class="clearfix">
                                <select id="underlying-code" class="col-sm-12" name="underlying-code" data-bind="options: $root.Parameter().UnderlyingDocs, optionsText: function(item) { if(item.ID != null) return item.Code + ' (' + item.Name + ')'} , optionsValue: 'ID', optionsCaption: 'Please Select...', value: $root.Selected().UnderlyingDoc, disable: !$root.IsEditable()"></select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group" data-bind="visible: $root.isNewUnderlying()">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="underlying-desc">Other Underlying</label>

                        <div class="col-xs-9 align-left">
                            <div class="clearfix">
                                <textarea class="col-lg-5" id="underlying-desc" rows="4" data-rule-required="true" data-bind="value: OtherUnderlyingDoc, enable: $root.isNewUnderlying"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="utilization-amount">Total Underlying</label>

                        <div class="col-sm-9">
                            <div class="clearfix">
                                <input type="text" class="col-sm-4 align-right" name="utilization-amount" id="utilization-amount" data-rule-required="true" disabled="disabled" data-rule-number="true" data-bind="value: formatNumber(utilizationAmount())" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="utilization-amount">Total Utilization</label>

                        <div class="col-sm-9">
                            <div class="clearfix">
                                <input type="text" class="col-sm-4 align-right" name="totalutilization" id="totalutilization" data-rule-required="true" disabled="disabled" data-rule-number="true" data-bind="value: formatNumber(TotalUtilization())" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="totalTrx">Total IDR->FCY Transaction in USD</label>
                        <div class="col-sm-9">
                            <div class="clearfix">
                                <input type="text" autocomplete="off" class="col-sm-4 align-right" name="totalTrx" id="totalTrx" disabled="disabled" data-bind="value:TotalTransFX" />
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="tz-number">TZ Number</label>

                        <div class="col-sm-9">
                            <div class="clearfix">
                                <input type="text" autocomplete="off" class="col-sm-6" name="tz-number" id="tz-number" data-bind="value: TZNumber, disable: !$root.IsEditable()" />
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="space-4"></div>
            <div class="padding-4">
                <h3 class="header smaller lighter dark">
                    Instruction Form and Docs
                    <!--<button class="btn btn-sm btn-primary pull-right" data-bind="click: $root.UploadDocumentUnderlying, disable: !$root.IsEditable(), visible: (currency().code !='IDR' && account().currency.code =='IDR')">--> <!-- ?FXCONDITION YES -->
                    <button class="btn btn-sm btn-primary pull-right" data-bind="click: $root.UploadDocumentUnderlying, disable: !$root.IsEditable(), visible: ($root.IsFxTransaction() || $root.IsFxTransactionToIDR())">
                        <i class="icon-plus"></i>
                        Attach
                    </button>
                    <!--<button class="btn btn-sm btn-primary pull-right" data-bind="click: $root.UploadDocument, disable: !$root.IsEditable(), visible: !(currency().code !='IDR' && account().currency.code =='IDR')">--> <!-- ?FXCONDITION NO -->
                    <button class="btn btn-sm btn-primary pull-right" data-bind="click: $root.UploadDocument, disable: !$root.IsEditable(), visible: !($root.IsFxTransaction() || $root.IsFxTransactionToIDR())">
                        <i class="icon-plus"></i>
                        Attach
                    </button>
                </h3>
                <!--data-bind="visible:!$root.IsUnderlyingMode()" -->
                <div class="dataTables_wrapper" role="grid">
                    <table class="table table-striped table-bordered table-hover dataTable">
                        <thead>
                            <tr>
                                <th style="width:50px">No.</th>
                                <th>File Name</th>
                                <th>Purpose of Docs</th>
                                <th>Type of Docs</th>
                                <th>Modified</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody data-bind="foreach: $root.Documents, visible: $root.Documents().length > 0">
                            <tr>
                                <!--data-bind="click: $root.EditDocument"-->
                                <td><span data-bind="text: $index() +1"></span></td>
                                <td><span data-bind="text: DocumentPath.name"></span></td>
                                <td><span data-bind="text: Purpose.Name"></span></td>
                                <td><span data-bind="text: Type.Name"></span></td>
                                <td><span data-bind="text: moment(DocumentPath.lastModifiedDate).format(config.format.dateTime)"></span></td><!--$root.LocalDate(LastModifiedDate)-->
                                <td><a href="#" data-bind="click: $root.RemoveDocument">Remove</a></td>
                            </tr>
                        </tbody>
                        <tbody data-bind="visible: $root.Documents().length == 0">
                            <tr>
                                <td colspan="6" align="center">No entries available.</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <!--- Grid Underlying Proforma Start -->
                <!-- widget header start -->
                <div class="space-10"></div>
                <!-- <div data-bind="if: (Currency().Code != 'IDR' && (Account().Currency.Code == 'IDR' || DebitCurrency().Code == 'IDR'))">--> <!-- ?FXCONDITION YES -->

                <div data-bind="if:($root.IsFxTransaction() || $root.IsFxTransactionToIDR())">
                    <h3 class="header smaller lighter dark">
                        Underlying Docs
                    </h3>
                    <div class="widget-header widget-hea1der-small header-color-dark">
                        <h6>Attach File Table</h6>

                        <div class="widget-toolbar">
                            <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                <i class="blue icon-filter"></i>
                                <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: $root.AttachGridProperties().AllowFilter" />
                                <span class="lbl"></span>
                            </label>
                            <a href="#" data-bind="click: $root.GetDataAttachFile" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                <i class="blue icon-refresh"></i>
                            </a>
                        </div>
                    </div>
                    <!-- widget header end -->
                    <div class="widget-body">
                        <!-- widget main start -->
                        <div class="widget-main padding-0">
                            <!-- widget slim control start -->
                            <div class="slim-scroll" data-height="200">
                                <!-- widget content start -->
                                <div class="content">

                                    <!-- table responsive start -->
                                    <div class="table-responsive">
                                        <div class="dataTables_wrapper" role="grid">
                                            <table id="Customer Underlying-table" class="table table-striped table-bordered table-hover dataTable">
                                                <thead>
                                                    <tr data-bind="with: $root.AttachGridProperties">
                                                        <th style="width:50px">No.</th>
                                                        <th data-bind="click: function () { Sorting('FileName'); }, css: GetSortedColumn('FileName')">File Name</th>
                                                        <th data-bind="click: function () { Sorting('DocumentPurpose'); }, css: GetSortedColumn('DocumentPurpose')">Document Purpose</th>
                                                        <th data-bind="click: function () { Sorting('LastModifiedDate'); }, css: GetSortedColumn('LastModifiedDate')">Update</th>
                                                        <th data-bind="click: function () { Sorting('DocumentRefNumber'); }, css: GetSortedColumn('DocumentRefNumber')">Document Ref Number</th>
                                                        <th style="width:0px;display: none"></th>
                                                        <th style="width:50px">Delete</th>
                                                        <!--
                                                                                                                    <th style="width:50px">Action</th>
                                                        -->
                                                    </tr>
                                                </thead>
                                                <thead data-bind="visible: $root.AttachGridProperties().AllowFilter" class="table-filter">
                                                    <tr>
                                                        <th class="clear-filter">
                                                            <a href="#" data-bind="click: $root.AttachClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                <i class="green icon-trash"></i>
                                                            </a>
                                                        </th>
                                                        <th><input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.AttachFilterFileName, event: { change: $root.AttachGridProperties().Filter }" /></th>
                                                        <th><input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.AttachFilterDocumentPurpose, event: { change: $root.AttachGridProperties().Filter }" /></th>
                                                        <th><input type="text" autocomplete="off" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.AttachFilterLastModifiedDate, event: { change: $root.AttachGridProperties().Filter }" /></th>
                                                        <th><input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.AttachFilterDocumentRefNumber, event: { change: $root.AttachGridProperties().Filter }" /></th>
                                                        <th class="clear-filter" style="display: none"></th>
                                                        <!--
                                                                                                                    <th class="clear-filter"></th>
                                                        -->
                                                    </tr>
                                                </thead>
                                                <tbody data-bind="foreach: $root.CustomerUnderlyingFiles, visible: $root.CustomerUnderlyingFiles().length > 0">
                                                    <tr data-bind="click: $root.GetAttachSelectedRow">
                                                        <td><span data-bind="text: RowID"></span></td>
                                                        <td><a data-bind="attr: { href: DocumentPath, target: '_blank' }, text: FileName"></a></td>
                                                        <td><span data-bind="text: DocumentPurpose.Name"></span></td>
                                                        <td><span data-bind="text: $root.LocalDate(LastModifiedDate,false,false)"></span></td>
                                                        <td><span data-bind="text: DocumentRefNumber"></span></td>
                                                        <td style="width:0px;display: none"><span data-bind="text: DocumentPath"></span></td>
                                                        <td align="center">
                                                            <a href="#" data-bind="click:$root.delete_a" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                <i class="green icon-trash"></i>
                                                            </a>
                                                        </td>
                                                        <!--
                                                                                                                    <td><span class="label label-info arrowed-in-right arrowed" data-bind="click: $root.GetDocument,text:'Update'"></span></td>
                                                        -->
                                                    </tr>
                                                </tbody>
                                                <tbody data-bind="visible: $root.CustomerUnderlyingFiles().length == 0">
                                                    <tr>
                                                        <td colspan="10" class="text-center">
                                                            No entries available.
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- table responsive end -->
                                </div>
                                <!-- widget content end -->
                            </div>
                            <!-- widget slim control end -->
                            <!-- widget footer start -->
                            <div class="widget-toolbox padding-8 clearfix">
                                <div class="row" data-bind="with: $root.AttachGridProperties">
                                    <!-- pagination size start -->
                                    <div class="col-sm-6">
                                        <div class="dataTables_paginate paging_bootstrap pull-left">
                                            Showing <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                            rows of <span data-bind="text: Total"></span>
                                            entries
                                        </div>
                                    </div>
                                    <!-- pagination size end -->
                                    <!-- pagination page jump start -->
                                    <div class="col-sm-3">
                                        <div class="dataTables_paginate paging_bootstrap">
                                            Page <input type="text" autocomplete="off" data-bind="value: Page, event: { change: OnPageChange }" style="width:50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                            of <span data-bind="text: TotalPages"></span>
                                        </div>
                                    </div>
                                    <!-- pagination page jump end -->
                                    <!-- pagination navigation start -->
                                    <div class="col-sm-3">
                                        <div class="dataTables_paginate paging_bootstrap">
                                            <ul class="pagination">
                                                <li data-bind="click: FirstPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                        <i class="icon-double-angle-left"></i>
                                                    </a>
                                                </li>
                                                <li data-bind="click: PreviousPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                        <i class="icon-angle-left"></i>
                                                    </a>
                                                </li>
                                                <li data-bind="click: NextPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                        <i class="icon-angle-right"></i>
                                                    </a>
                                                </li>
                                                <li data-bind="click: LastPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                        <i class="icon-double-angle-right"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- pagination navigation end -->

                                </div>
                            </div>
                            <!-- widget footer end -->

                        </div>
                        <!-- widget main end -->
                    </div>
                </div><!-- widget body end -->
                <!--- Grid Underlying Proforma End -->

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 align-right">
        <button class="btn btn-sm btn-primary" data-bind="click: SaveAsDraft, disable: !IsEditable(), visible: IsRole('DBS Admin')">
            <i class="icon-edit"></i>
            Draft
        </button>
        <button class="btn btn-sm btn-primary" data-bind="click: Submit, disable: !IsEditable()">
            <i class="icon-save"></i>
            Submit
        </button>
    </div>
    
<!-- modal form start Attach Document Underlying Form start -->
<div id="modal-form-Attach" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
    <div class="modal-dialog" style="width:100%">
        <div class="modal-content">
            <!-- modal header start Attach FIle Form -->
            <div class="modal-header">
                <h4 class="blue bigger">
                    <span>Added Attachment File</span>
                </h4>
            </div>
            <!-- modal header end Attach file -->
            <!-- modal body start Attach File -->
            <div class="modal-body overflow-visible">
                <div class="row">
                    <div class="col-xs-12">
                        <!-- modal body form start -->
                        <div id="customer-form" class="form-horizontal" role="form">
                            <div class="space-4"></div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="Name">
                                    Purpose of Doc
                                </label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <select id="documentPurpose" name="documentPurpose" data-bind="value:$root.DocumentPurpose_a().ID, options: ddlDocumentPurpose_a, optionsText: 'Name',optionsValue: 'ID',optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                        <label class="control-label bolder text-danger">*</label>
                                    </div>
                                </div>
                            </div>

                            <div class="space-4"></div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="description">
                                    Type of Doc
                                </label>

                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <select id="documentType" name="documentType" data-bind="value:$root.DocumentType_a().ID, options: ddlDocumentType_a, optionsText: 'Name',optionsValue: 'ID',optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                        <label class="control-label bolder text-danger">*</label>
                                    </div>
                                </div>
                            </div>
                            <div class="space-4"></div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="document-path-upload">Document</label>

                                <div class="col-sm-6">
                                    <div class="clearfix">
                                        <input type="file" id="document-path-upload" name="document-path-upload" data-bind="file: DocumentPath_a" class="col-xs-7"/><!---->                                       
                                        <!--<span data-bind="text: DocumentPath"></span>-->
                                        <!--<input type="text" id="document-path-text" name="document-path-text" data-bind="value: FileName_a, visible: $root.IsEditableDocument" />-->
                                    </div>
                                </div>
                                 <label class="control-label bolder text-danger">*</label>
                            </div>
                        </div>
                        <!-- modal body form Attach File End -->
                        <!-- widget box Underlying Attach start -->
                        <div id="widget-box" class="widget-box">
                            <!-- widget header Underlying Attach start -->
                            <div class="widget-header widget-hea1der-small header-color-dark">
                                <h6>Table</h6>

                                <div class="widget-toolbar">
                                    <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                        <i class="blue icon-filter"></i>
                                        <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: UnderlyingAttachGridProperties().AllowFilter" />
                                        <span class="lbl"></span>
                                    </label>
                                    <a href="#" data-bind="click: GetDataUnderlyingAttach" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                        <i class="blue icon-refresh"></i>
                                    </a>
                                </div>
                            </div>
                            <!-- widget header end -->
                            <!-- widget body underlying Attach start -->
                            <div class="widget-body">
                                <!-- widget main start -->
                                <div class="widget-main padding-0">
                                    <!-- widget slim control start -->
                                    <div class="slim-scroll" data-height="400">
                                        <!-- widget content start -->
                                        <div class="content">
                                            <!-- table responsive start -->
                                            <div class="table-responsive">
                                                <div class="dataTables_wrapper" role="grid">
                                                    <table id="Customer Underlying-table" class="table table-striped table-bordered table-hover dataTable">
                                                        <thead>
                                                            <tr data-bind="with: $root.UnderlyingAttachGridProperties">
                                                                <!-- <th style="width:50px">Select</th> -->
                                                                <th style="width:50px">No.</th>
                                                                <th style="width:50px">Select</th>
                                                                <th data-bind="click: function () { Sorting('StatementLetter'); }, css: GetSortedColumn('StatementLetter')">Statement Letter</th>
                                                                <th data-bind="click: function () { Sorting('UnderlyingDocument'); }, css: GetSortedColumn('UnderlyingDocument')">Underlying Document</th>
                                                                <th data-bind="click: function () { Sorting('DocumentType'); }, css: GetSortedColumn('DocumentType')">Type of Doc</th>
                                                                <th data-bind="click: function () { Sorting('Currency'); }, css: GetSortedColumn('Currency')">Currency</th>
                                                                <th data-bind="click: function () { Sorting('Amount'); }, css: GetSortedColumn('Amount')">Amount</th>
                                                                <th data-bind="click: function () { Sorting('SupplierName'); }, css: GetSortedColumn('SupplierName')">Supplier Name</th>
                                                                <th data-bind="click: function () { Sorting('ReferenceNumber'); }, css: GetSortedColumn('ReferenceNumber')">Reference Number</th>
                                                                <th data-bind="click: function () { Sorting('DateOfUnderlying'); }, css: GetSortedColumn('DateOfUnderlying')">Date</th>
                                                                <th data-bind="click: function () { Sorting('ExpiredDate'); }, css: GetSortedColumn('ExpiredDate')">Expiry Date</th>
                                                            </tr>
                                                        </thead>
                                                        <thead data-bind="visible: $root.UnderlyingAttachGridProperties().AllowFilter" class="table-filter">
                                                            <tr>
                                                                <th class="clear-filter">
                                                                    <a href="#" data-bind="click: $root.UnderlyingAttachClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                        <i class="green icon-trash"></i>
                                                                    </a>
                                                                </th>
                                                                <th class="clear-filter"></th>
                                                                <th><input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterStatementLetter, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                <th><input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterUnderlyingDocument, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                <th><input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterDocumentType, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                <th><input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterCurrency, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                <th><input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterAmount, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                <th><input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterSupplierName, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                <th><input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterReferenceNumber, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                <th><input type="text" autocomplete="off" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.UnderlyingAttachFilterDateOfUnderlying, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                <th><input type="text" autocomplete="off" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.UnderlyingAttachFilterExpiredDate, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody data-bind="foreach: {data: $root.CustomerAttachUnderlyings, as: 'AttachData'}, visible: $root.CustomerAttachUnderlyings().length > 0">
                                                            <tr>
                                                                <td><span data-bind="text: RowID"></span></td>
                                                                <td align="center"><input type="checkbox" id="isSelected2" name="isSelect2" data-bind="checked:IsSelectedAttach,event:{change:function(data){$root.onSelectionAttach($index(),$data)}}, disable: !($root.SelectingUnderlying())" /></td>
                                                                <td><span data-bind="text: StatementLetter.Name"></span></td>
                                                                <td><span data-bind="text: UnderlyingDocument.Name"></span></td>
                                                                <td><span data-bind="text: DocumentType.Name"></span></td>
                                                                <td><span data-bind="text: Currency.Code"></span></td>
                                                                <td><span data-bind="text: Amount"></span></td>
                                                                <td><span data-bind="text: SupplierName"></span></td>
                                                                <td><span data-bind="text: ReferenceNumber"></span></td>
                                                                <td><span data-bind="text: $root.LocalDate(DateOfUnderlying,true,false)"></span></td>
                                                                <td><span data-bind="text: $root.LocalDate(ExpiredDate,true,false)"></span></td>
                                                            </tr>
                                                        </tbody>
                                                        <tbody data-bind="visible: $root.CustomerAttachUnderlyings().length == 0">
                                                            <tr>
                                                                <td colspan="11" class="text-center">
                                                                    No entries available.
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <!-- table responsive end -->
                                        </div>
                                        <!-- widget content end -->
                                    </div>
                                    <!-- widget slim control end -->
                                    <!-- widget footer start -->
                                    <div class="widget-toolbox padding-8 clearfix">
                                        <div class="row" data-bind="with: UnderlyingAttachGridProperties">
                                            <!-- pagination size start -->
                                            <div class="col-sm-6">
                                                <div class="dataTables_paginate paging_bootstrap pull-left">
                                                    Showing <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                                    rows of <span data-bind="text: Total"></span>
                                                    entries
                                                </div>
                                            </div>
                                            <!-- pagination size end -->
                                            <!-- pagination page jump start -->
                                            <div class="col-sm-3">
                                                <div class="dataTables_paginate paging_bootstrap">
                                                    Page <input type="text" autocomplete="off" data-bind="value: Page, event: { change: OnPageChange }" style="width:50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                    of <span data-bind="text: TotalPages"></span>
                                                </div>
                                            </div>
                                            <!-- pagination page jump end -->
                                            <!-- pagination navigation start -->
                                            <div class="col-sm-3">
                                                <div class="dataTables_paginate paging_bootstrap">
                                                    <ul class="pagination">
                                                        <li data-bind="click: FirstPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                            <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                <i class="icon-double-angle-left"></i>
                                                            </a>
                                                        </li>
                                                        <li data-bind="click: PreviousPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                            <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                <i class="icon-angle-left"></i>
                                                            </a>
                                                        </li>
                                                        <li data-bind="click: NextPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                            <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                <i class="icon-angle-right"></i>
                                                            </a>
                                                        </li>
                                                        <li data-bind="click: LastPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                            <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                <i class="icon-double-angle-right"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <!-- pagination navigation end -->

                                        </div>
                                    </div>
                                    <!-- widget footer end -->

                                </div>
                                <!-- widget main end -->
                            </div>
                            <!-- widget body end -->
                        </div>
                        <!-- widget box end -->



                    </div>
                </div>
            </div>
            <!-- modal body end Attach File -->
            <!-- modal footer start Attach File-->
            <div class="modal-footer">
                <button class="btn btn-sm btn-primary" data-bind="click: $root.save_a, disable: $root.IsUploading()">
                    <i class="icon-save"></i>
                    Save
                </button>
                <button class="btn btn-sm" data-dismiss="modal" data-bind="click: $root.cancel_a">
                    <i class="icon-remove"></i>
                    Cancel
                </button>
            </div>
            <!-- modal footer end attach file -->

        </div>
    </div>
</div>
<!-- modal form end Attach Document Underlying Form end -->
<!-- Modal Double Transaction start -->
<div id="modal-double-transaction" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width:950px;">
        <div class="modal-content">
            <!-- modal header start -->
            <div class="alert alert-danger no-margin-bottom">
                <strong>
                    <i class="icon-ban-circle"></i>
                    Double Transaction!
                </strong>
                This transaction has same information with existing transaction. To continue with this transaction, PPU / Branch Ops Checker validation is required.
            </div>
            <!-- modal header end -->
            <!-- modal body start -->
            <div class="modal-body overflow-visible">
                <h3 class="header no-padding-top no-margin-top smaller lighter dark">Existing Transactions</h3>

                <div class="table-responsive">
                    <div class="dataTables_wrapper" role="grid">
                        <table class="table table-striped table-bordered table-hover dataTable">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Application ID</th>
                                    <th>Customer Name</th>
                                    <th>Product</th>
                                    <th>Currency</th>
                                    <th>Trxn Amount</th>
                                    <th>Rate</th>
                                    <th>Eqv. USD</th>
                                    <th>Debit Account CCY</th>
                                    <th>Application Date</th>
                                </tr>
                            </thead>
                            <tbody data-bind="foreach: DoubleTransactions">
                                <tr>
                                    <td><span data-bind="text: $index() +1"></span></td>
                                    <td style="white-space: nowrap"><span data-bind="text: ApplicationID"></span></td>
                                    <td><span data-bind="text: Customer.Name"></span></td>
                                    <td><span data-bind="text: Product.Name"></span></td>
                                    <td><span data-bind="text: Currency.Code"></span></td>
                                    <td><span data-bind="text: $root.FormatNumber(Amount)"></span></td>
                                    <td><span data-bind="text: $root.FormatNumber(Rate)"></span></td>
                                    <td><span data-bind="text: $root.FormatNumber(AmountUSD)"></span></td>
                                    <td><span data-bind="text: Account.Currency.Code"></span></td>
                                    <td><span data-bind="text: $root.LocalDate(ApplicationDate,false,true)"></span></td>
                                    <!-- <td><span data-bind="text: moment(ApplicationDate).format('dd-M-yyyy')"></span></td>-->
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="space-4"></div>

                <!-- form verify login start -->
                <h3 class="header smaller lighter dark">PPU / Branch Ops. Validation</h3>
                <div class="row">
                    <div class="col-xs-12">
                        <div id="checker-login-form" class="form-horizontal" role="form">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="verify-userid">User ID</label>

                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <input id="userid" type="text" autocomplete="off" placeholder="User ID" class="col-xs-10 col-sm-5" data-rule-required="true" data-bind="value: UserValidation().UserID" />
                                        <label class="control-label bolder text-danger">*</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="verify-password">Password</label>

                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <input id="verify-password" type="password" placeholder="Password" class="col-xs-10 col-sm-5" data-rule-required="true" data-bind="value: UserValidation().Password" />
                                        <label class="control-label bolder text-danger">*</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group" data-bind="with: UserValidation">
                                <label class="col-sm-3 control-label no-padding-right"></label>

                                <label class="col-sm-9">
                                    <span class="red" data-bind="text: Error"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- form verify login end -->

            </div>
            <!-- modal body end -->
            <!-- modal footer start -->
            <div class="modal-footer no-margin-top">
                <button class="btn btn-sm btn-primary" data-bind="click: ValidationProcess">
                    <i class="icon-key"></i>
                    Submit
                </button>
                <button class="btn btn-sm btn-success" data-bind="click: ContinueEditing" data-dismiss="modal">
                    <i class="icon-remove"></i>
                    Cancel
                </button>
            </div>
            <!-- modal footer end -->
        </div>
    </div>
</div>
<!-- Modal Double Transaction end -->
<!-- Modal upload form start -->
<div id="modal-form-upload" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body overflow-visible">
                <div class="row">
                    <div class="col-xs-12">
                        <div id="customer-form" class="form-horizontal" role="form">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="document-type">Document Type</label>

                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <select id="document-type" name="document-type" data-rule-required="true" data-rule-value="true" data-bind="options: Parameter().DocumentTypes, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Options..', value: Selected().DocumentType"></select>
                                        <label class="control-label bolder text-danger">*</label>
                                    </div>
                                </div>
                            </div>

                            <div class="space-4"></div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="document-purpose">Purpose of Docs</label>

                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <select id="document-purpose" name="document-purpose" data-rule-required="true" data-rule-value="true" data-bind="options: Parameter().DocumentPurposes, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Options..', value: Selected().DocumentPurpose"></select>
                                        <label class="control-label bolder text-danger">*</label>
                                    </div>
                                </div>
                            </div>

                            <div class="space-4"></div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="document-path">Document</label>

                                <div class="col-sm-8">
                                    <div class="clearfix">
                                        <input type="file" id="document-path" name="document-path" data-bind="file: DocumentPath" />                                         
                                        <!---->
                                        <!--<span data-bind="text: DocumentPath"></span>-->
                                    </div>
                                </div>
                                <label class="control-label bolder text-danger">*</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button class="btn btn-sm btn-primary" data-bind="click: $root.AddDocument">
                    <i class="icon-save"></i>
                    Save
                </button>
                <button class="btn btn-sm" data-dismiss="modal">
                    <i class="icon-remove"></i>
                    Cancel
                </button>
            </div>
        </div>
    </div>
</div>
<!-- Modal upload form end -->
<!-- modal form start Underlying Form -->

<div id="modal-form-Underlying" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
    <div class="modal-dialog" style="width:100%">
        <div class="modal-content">
            <!-- modal header start Underlying Form -->
            <div class="modal-header">
                <h4 class="blue bigger">
                    <span data-bind="if: $root.IsNewDataUnderlying()">Create a new Customer Underlying Parameter</span>
                    <span data-bind="if: !$root.IsNewDataUnderlying()">Modify a Customer Underlying Parameter</span>
                </h4>
            </div>
            <!-- modal header end -->
            <!-- modal body start -->
            <div class="modal-body overflow-visible">
                <div id="modalUnderlyingtest">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- modal body form start -->

                            <div id="customerUnderlyingform" class="form-horizontal" role="form">
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="StatementLetterID">
                                        Statement Letter
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="statementLetter" name="statementLetter" data-bind="value:$root.StatementLetter_u().ID, options: ddlStatementLetter_u, optionsText: 'Name',optionsValue: 'ID',optionsCaption: 'Please Select...',enable:$root.IsStatementA()" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                            <label class="control-label bolder text-danger" for="StatementLetter_u">*</label> 
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="UnderlyingDocument">
                                        Underlying Document
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="underlyingDocument" name="underlyingDocument" data-bind="value:UnderlyingDocument_u().ID, options: ddlUnderlyingDocument_u, optionsText: 'CodeName',optionsValue: 'ID',optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                           <label class="control-label bolder text-danger" for="UnderlyingDocument_u">*</label>  
                                        </div>
                                    </div>
                                </div>
                                
                                <!--    						<span aa-bind="text:UnderlyingDocument_u().Code()"></span>-->
                                <div data-bind="visible:UnderlyingDocument_u().ID()=='50'" class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="otherUnderlying">
                                        Other Underlying
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" autocomplete="off" id="otherUnderlying" name="otherUnderlying" data-bind="value: $root.OtherUnderlying_u" class="col-xs-7" />
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="DocumentType">
                                        Type of Document
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="documentType" name="documentType" data-bind="value:$root.DocumentType_u().ID, options: ddlDocumentType_u, optionsText: 'Name',optionsValue: 'ID',optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                            <label class="control-label bolder text-danger" for="DocumentType_u">*</label> 
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="Currency">
                                        Transaction Currency
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="currency_u" name="currency_u" data-bind="value:$root.Currency_u().ID,disable:$root.StatementLetter_u().ID()==1, options:ddlCurrency_u, optionsText:'Code',optionsValue: 'ID',optionsCaption: 'Please Select...',event: {change:function(data){OnCurrencyChange(Currency_u().ID())}}" data-rule-required="true" data-rule-value="true" class="col-xs-2"></select>
                                            <label class="control-label bolder text-danger" for="Currency_u">*</label> 
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="Amount">
                                        Invoice Amount
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" autocomplete="off" id="Amount_u" name="Amount_u" data-bind="value: $root.Amount_u(),disable:$root.StatementLetter_u().ID()==1,valueUpdate:['afterkeydown','propertychange','input']"  onkeyup="read_u();" data-rule-required="true" data-rule-number="true" class="col-xs-4 align-right" />
                                            <label class="control-label bolder text-danger" for="Amount_u">*</label>
                                        </div>
                                    </div>
                                </div>
                                

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="Rate">Rate</label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" autocomplete="off" class="col-sm-4 align-right" name="rate_u" id="rate_u" disabled="disabled" data-rule-required="true" data-rule-number="true" data-bind="value: formatNumber($root.Rate_u())" />
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="Eqv.USD">Eqv. USD</label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" autocomplete="off" class="col-sm-4 align-right" disabled="disabled" name="eqv-usd_u" id="eqv-usd_u" data-rule-required="true" data-rule-number="true" data-bind="value: formatNumber($root.AmountUSD_u())" />
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="DateOfUnderlying">
                                        Date of Underlying
                                    </label>
                                    <div class="col-sm-2">
                                        <div class="input-group">
                                            <input type="text" autocomplete="off" id="DateOfUnderlying" data-date-format="dd-M-yyyy" name="DateOfUnderlying_u" data-bind="value:DateOfUnderlying_u" class="form-control date-picker col-xs-6" data-rule-required="true" />
                                            <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                            <label class="control-label bolder text-danger starremove" for="DateOfUnderlying_u" style="position:absolute;">*</label>
                                        </div>
                                    </div>
                                    <label class="col-sm-2 control-label no-padding-right" for="ExpiredDate">
                                        Expiry Date
                                    </label>
                                    <div class="col-sm-2">
                                        <div class="input-group">
                                            <input type="text" autocomplete="off" id="ExpiredDate" data-date-format="dd-M-yyyy" name="ExpiredDate" data-bind="value:ExpiredDate_u" class="form-control date-picker col-xs-6" data-rule-required="true" />
                                            <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                            <label class="control-label bolder text-danger starremove" for="ExpiredDate_u" style="position:absolute;">*</label>
                                        </div>
                                    </div>
                                </div>

                                
                                <div class="form-group" data-bind="visible:false">
                                    <label class="col-sm-3 control-label no-padding-right" for="IsDeclarationOfException">
                                        Declaration of Exception
                                    </label>
                                    <div class="col-sm-9">
                                        <label>
                                            <input name="switch-field-1" id="IsDeclarationOfException" data-bind="checked: $root.IsDeclarationOfException_u" class="ace ace-switch ace-switch-6" type="checkbox" />
                                            <span class="lbl"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="space-2" data-bind="visible:false"></div>
                                <div class="form-group" data-bind="visible:false">
                                    <label class="col-sm-3 control-label no-padding-right" for="StartDate">
                                        Start Date
                                    </label>
                                    <div class="col-sm-2">
                                        <div class="clearfix">
                                            <div class="input-group">
                                                <input id="StartDate" name="StartDate" data-date-format="dd-M-yyyy" data-bind="value:StartDate_u" class="form-control date-picker col-xs-6" type="text" autocomplete="off" />
                                                <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <label class="col-sm-2 control-label no-padding-right" for="EndDate">
                                        End Date
                                    </label>
                                    <div class="col-sm-2">
                                        <div class="clearfix">
                                            <div class="input-group">
                                                <input type="text" autocomplete="off" id="EndDate" name="EndDate" data-date-format="dd-M-yyyy" data-bind="value:EndDate_u" class="form-control date-picker col-xs-6" />
                                                <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="ReferenceNumber">
                                        Reference Number
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" autocomplete="off" id="ReferenceNumber" name="ReferenceNumber" data-bind="value: $root.ReferenceNumber_u" disabled="disabled" class="col-xs-9" />
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="SupplierName">
                                        Supplier Name
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" autocomplete="off" id="SupplierName" name="SupplierName" data-bind="value: $root.SupplierName_u" class="col-xs-8" data-rule-required="true" />
                                            <label class="control-label bolder text-danger" for="SupplierName_u">*</label>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="InvoiceNumber">
                                        Invoice Number
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" autocomplete="off" id="invoiceNumber" name="invoiceNumber" data-bind="value: $root.InvoiceNumber_u" class="col-xs-8" data-rule-required="true" />
                                            <label class="control-label bolder text-danger" for="InvoiceNumber_u">*</label>
                                        </div>
                                    </div>
                                </div>

                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="AttachmentNo">
                                        Is This doc Purposed to replace the proforma doc
                                    </label>
                                    <div class="col-sm-9">
                                        <label>
                                            <input name="switch-field-1" id="IsProforma" data-bind="checked:IsProforma_u,disable:DocumentType_u().ID()==2" class="ace ace-switch ace-switch-6" type="checkbox" />
                                            <span class="lbl"></span>
                                        </label>
                                    </div>
                                </div>
                                <!-- grid proforma begin -->
                                <!-- widget box start -->
                                <div id="widget-box" class="widget-box" data-bind="visible:IsProforma_u()">
                                    <!-- widget header start -->
                                    <div class="widget-header widget-hea1der-small header-color-dark">
                                        <h6>Customer Underlying Proforma Table</h6>

                                        <div class="widget-toolbar">
                                            <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                                <i class="blue icon-filter"></i>
                                                <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: UnderlyingProformaGridProperties().AllowFilter" />
                                                <span class="lbl"></span>
                                            </label>
                                            <a href="#" data-bind="click: GetDataUnderlyingProforma" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                                <i class="blue icon-refresh"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <!-- widget header end -->
                                    <!-- widget body start -->
                                    <div class="widget-body">
                                        <!-- widget main start -->
                                        <div class="widget-main padding-0">
                                            <!-- widget slim control start -->
                                            <div class="slim-scroll" data-height="400">
                                                <!-- widget content start -->
                                                <div class="content">

                                                    <!-- table responsive start -->
                                                    <div class="table-responsive">
                                                        <div class="dataTables_wrapper" role="grid">
                                                            <table id="Customer Underlying-table" class="table table-striped table-bordered table-hover dataTable">
                                                                <thead>
                                                                    <tr data-bind="with: UnderlyingProformaGridProperties">
                                                                        <th style="width:50px">No.</th>
                                                                        <th style="width:50px">Select</th>
                                                                        <th data-bind="click: function () { Sorting('StatementLetter'); }, css: GetSortedColumn('StatementLetter')">Statement Letter</th>
                                                                        <th data-bind="click: function () { Sorting('UnderlyingDocument'); }, css: GetSortedColumn('UnderlyingDocument')">Underlying Document</th>
                                                                        <th data-bind="click: function () { Sorting('DocumentType'); }, css: GetSortedColumn('DocumentType')">Type of Doc</th>
                                                                        <th data-bind="click: function () { Sorting('Currency'); }, css: GetSortedColumn('Currency')">Currency</th>
                                                                        <th data-bind="click: function () { Sorting('Amount'); }, css: GetSortedColumn('Amount')">Amount</th>
                                                                        <th data-bind="click: function () { Sorting('SupplierName'); }, css: GetSortedColumn('SupplierName')">Supplier Name</th>
                                                                        <th data-bind="click: function () { Sorting('ReferenceNumber'); }, css: GetSortedColumn('ReferenceNumber')">Reference Number</th>
                                                                        <th data-bind="click: function () { Sorting('DateOfUnderlying'); }, css: GetSortedColumn('DateOfUnderlying')">Date</th>
                                                                        <th data-bind="click: function () { Sorting('ExpiredDate'); }, css: GetSortedColumn('ExpiredDate')">Expiry Date</th>
                                                                    </tr>
                                                                </thead>
                                                                <thead data-bind="visible: UnderlyingProformaGridProperties().AllowFilter" class="table-filter">
                                                                    <tr>
                                                                        <th class="clear-filter">
                                                                            <a href="#" data-bind="click: ProformaClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                                <i class="green icon-trash"></i>
                                                                            </a>
                                                                        </th>
                                                                        <th class="clear-filter"></th>
                                                                        <th><input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterStatementLetter, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                        <th><input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterUnderlyingDocument, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                        <th><input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterDocumentType, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                        <th><input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterCurrency, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                        <th><input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterAmount, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                        <th><input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterSupplierName, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                        <th><input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterReferenceNumber, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                        <th><input type="text" autocomplete="off" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.ProformaFilterDateOfUnderlying, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                        <th><input type="text" autocomplete="off" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.ProformaFilterExpiredDate, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody data-bind="foreach: {data: CustomerUnderlyingProformas, as: 'ProformasData'}, visible: CustomerUnderlyingProformas().length > 0">
                                                                    <tr>
                                                                        <td><span data-bind="text: RowID"></span></td>
                                                                        <td align="center"><input type="checkbox" id="isSelectedProforma" name="isSelect" data-bind="checked:ProformasData.IsSelectedProforma, event: {change:function(data){$root.onSelectionProforma($index(),$data)}}" /></td>
                                                                        <td><span data-bind="text: StatementLetter.Name"></span></td>
                                                                        <td><span data-bind="text: UnderlyingDocument.Name"></span></td>
                                                                        <td><span data-bind="text: DocumentType.Name"></span></td>
                                                                        <td><span data-bind="text: Currency.Code"></span></td>
                                                                        <td><span data-bind="text: Amount"></span></td>
                                                                        <td><span data-bind="text: SupplierName"></span></td>
                                                                        <td><span data-bind="text: ReferenceNumber"></span></td>
                                                                        <td><span data-bind="text: $root.LocalDate(DateOfUnderlying,true,false)"></span></td>
                                                                        <td><span data-bind="text: $root.LocalDate(ExpiredDate,true,false)"></span></td>
                                                                    </tr>
                                                                </tbody>
                                                                <tbody data-bind="visible: CustomerUnderlyingProformas().length == 0">
                                                                    <tr>
                                                                        <td colspan="11" class="text-center">
                                                                            No entries available.
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <!-- table responsive end -->
                                                </div>
                                                <!-- widget content end -->
                                            </div>
                                            <!-- widget slim control end -->
                                            <!-- widget footer start -->
                                            <div class="widget-toolbox padding-8 clearfix">
                                                <div class="row" data-bind="with: UnderlyingProformaGridProperties">
                                                    <!-- pagination size start -->
                                                    <div class="col-sm-6">
                                                        <div class="dataTables_paginate paging_bootstrap pull-left">
                                                            Showing <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                                            rows of <span data-bind="text: Total"></span>
                                                            entries
                                                        </div>
                                                    </div>
                                                    <!-- pagination size end -->
                                                    <!-- pagination page jump start -->
                                                    <div class="col-sm-3">
                                                        <div class="dataTables_paginate paging_bootstrap">
                                                            Page <input type="text" autocomplete="off" data-bind="value: Page, event: { change: OnPageChange }" style="width:50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                            of <span data-bind="text: TotalPages"></span>
                                                        </div>
                                                    </div>
                                                    <!-- pagination page jump end -->
                                                    <!-- pagination navigation start -->
                                                    <div class="col-sm-3">
                                                        <div class="dataTables_paginate paging_bootstrap">
                                                            <ul class="pagination">
                                                                <li data-bind="click: FirstPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                        <i class="icon-double-angle-left"></i>
                                                                    </a>
                                                                </li>
                                                                <li data-bind="click: PreviousPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                        <i class="icon-angle-left"></i>
                                                                    </a>
                                                                </li>
                                                                <li data-bind="click: NextPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                        <i class="icon-angle-right"></i>
                                                                    </a>
                                                                </li>
                                                                <li data-bind="click: LastPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                        <i class="icon-double-angle-right"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <!-- pagination navigation end -->

                                                </div>
                                            </div>
                                            <!-- widget footer end -->

                                        </div>
                                        <!-- widget main end -->
                                    </div>
                                    <!-- widget body end -->

                                </div>
                                <!-- widget box end -->
                                <!-- grid proforma end -->


                                

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="IsBulkUnderlying">
                                        Is This Bulk Underlying
                                    </label>
                                    <div class="col-sm-9">
                                        <label>
                                            <input name="switch-field-1" id="IsBulkUnderlying" data-bind="checked:IsBulkUnderlying_u" class="ace ace-switch ace-switch-6" type="checkbox" />
                                            <span class="lbl"></span>
                                        </label>
                                    </div>
                                </div>
                                <!-- grid Bulk begin -->
                                <!-- widget box start -->
                                <div class="widget-box" data-bind="visible:IsBulkUnderlying_u()">
                                    <!-- widget header start -->
                                    <div class="widget-header widget-hea1der-small header-color-dark">
                                        <h6>Customer Bulk Underlying Table</h6>

                                        <div class="widget-toolbar">
                                            <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                                <i class="blue icon-filter"></i>
                                                <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: UnderlyingBulkGridProperties().AllowFilter" />
                                                <span class="lbl"></span>
                                            </label>
                                            <a href="#" data-bind="click: GetDataBulkUnderlying" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                                <i class="blue icon-refresh"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <!-- widget header end -->
                                    <!-- widget body start -->
                                    <div class="widget-body">
                                        <!-- widget main start -->
                                        <div class="widget-main padding-0">
                                            <!-- widget slim control start -->
                                            <div class="slim-scroll" data-height="400">
                                                <!-- widget content start -->
                                                <div class="content">

                                                    <!-- table responsive start -->
                                                    <div class="table-responsive">
                                                        <div class="dataTables_wrapper" role="grid">
                                                            <table id="Customer-BulkUnderlying-table" class="table table-striped table-bordered table-hover dataTable">
                                                                <thead>
                                                                    <tr data-bind="with: UnderlyingBulkGridProperties">
                                                                        <th style="width:50px">No.</th>
                                                                        <th style="width:50px">Select</th>
                                                                        <th data-bind="click: function () { Sorting('StatementLetter'); }, css: GetSortedColumn('StatementLetter')">Statement Letter</th>
                                                                        <th data-bind="click: function () { Sorting('UnderlyingDocument'); }, css: GetSortedColumn('UnderlyingDocument')">Underlying Document</th>
                                                                        <th data-bind="click: function () { Sorting('DocumentType'); }, css: GetSortedColumn('DocumentType')">Type of Doc</th>
                                                                        <th data-bind="click: function () { Sorting('Currency'); }, css: GetSortedColumn('Currency')">Currency</th>
                                                                        <th data-bind="click: function () { Sorting('Amount'); }, css: GetSortedColumn('Amount')">Amount</th>
                                                                        <th data-bind="click: function () { Sorting('SupplierName'); }, css: GetSortedColumn('SupplierName')">Supplier Name</th>
                                                                        <th data-bind="click: function () { Sorting('InvoiceNumber'); }, css: GetSortedColumn('InvoiceNumber')">Invoice Number</th>
                                                                        <th data-bind="click: function () { Sorting('DateOfUnderlying'); }, css: GetSortedColumn('DateOfUnderlying')">Date</th>
                                                                        <th data-bind="click: function () { Sorting('ExpiredDate'); }, css: GetSortedColumn('ExpiredDate')">Expiry Date</th>
                                                                    </tr>
                                                                </thead>
                                                                <thead data-bind="visible: UnderlyingBulkGridProperties().AllowFilter" class="table-filter">
                                                                    <tr>
                                                                        <th class="clear-filter">
                                                                            <a href="#" data-bind="click: BulkClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                                <i class="green icon-trash"></i>
                                                                            </a>
                                                                        </th>
                                                                        <th class="clear-filter"></th>
                                                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterStatementLetter, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterUnderlyingDocument, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterDocumentType, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterCurrency, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterSupplierName, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterAmount, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterInvoiceNumber, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                        <th><input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.BulkFilterDateOfUnderlying, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                        <th><input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.BulkFilterExpiredDate, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody data-bind="foreach: {data: CustomerBulkUnderlyings, as: 'BulkDatas'}, visible: CustomerBulkUnderlyings().length > 0">
                                                                    <tr>
                                                                        <td><span data-bind="text: RowID"></span></td>
                                                                        <td align="center"><input type="checkbox" id="isSelectedBulk" name="isSelect" data-bind="checked:BulkDatas.IsSelectedBulk, event: {change:function(data){$root.onSelectionBulk($index(),$data)}}" /></td>
                                                                        <td><span data-bind="text: StatementLetter.Name"></span></td>
                                                                        <td><span data-bind="text: UnderlyingDocument.Name"></span></td>
                                                                        <td><span data-bind="text: DocumentType.Name"></span></td>
                                                                        <td><span data-bind="text: Currency.Code"></span></td>
                                                                        <td align="right"><span data-bind="text: formatNumber(Amount)"></span></td>
                                                                        <td><span data-bind="text: SupplierName"></span></td>
                                                                        <td><span data-bind="text: InvoiceNumber"></span></td>
                                                                        <td><span data-bind="text: $root.LocalDate(DateOfUnderlying,true,false)"></span></td>
                                                                        <td><span data-bind="text: $root.LocalDate(ExpiredDate,true,false)"></span></td>
                                                                    </tr>
                                                                </tbody>
                                                                <tbody data-bind="visible: CustomerBulkUnderlyings().length == 0">
                                                                    <tr>
                                                                        <td colspan="11" class="text-center">
                                                                            No entries available.
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <!-- table responsive end -->
                                                </div>
                                                <!-- widget content end -->
                                            </div>
                                            <!-- widget slim control end -->
                                            <!-- widget footer start -->
                                            <div class="widget-toolbox padding-8 clearfix">
                                                <div class="row" data-bind="with: UnderlyingBulkGridProperties">
                                                    <!-- pagination size start -->
                                                    <div class="col-sm-6">
                                                        <div class="dataTables_paginate paging_bootstrap pull-left">
                                                            Showing <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                                            rows of <span data-bind="text: Total"></span>
                                                            entries
                                                        </div>
                                                    </div>
                                                    <!-- pagination size end -->
                                                    <!-- pagination page jump start -->
                                                    <div class="col-sm-3">
                                                        <div class="dataTables_paginate paging_bootstrap">
                                                            Page <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width:50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                            of <span data-bind="text: TotalPages"></span>
                                                        </div>
                                                    </div>
                                                    <!-- pagination page jump end -->
                                                    <!-- pagination navigation start -->
                                                    <div class="col-sm-3">
                                                        <div class="dataTables_paginate paging_bootstrap">
                                                            <ul class="pagination">
                                                                <li data-bind="click: FirstPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                        <i class="icon-double-angle-left"></i>
                                                                    </a>
                                                                </li>
                                                                <li data-bind="click: PreviousPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                        <i class="icon-angle-left"></i>
                                                                    </a>
                                                                </li>
                                                                <li data-bind="click: NextPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                        <i class="icon-angle-right"></i>
                                                                    </a>
                                                                </li>
                                                                <li data-bind="click: LastPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                        <i class="icon-double-angle-right"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <!-- pagination navigation end -->

                                                </div>
                                            </div>
                                            <!-- widget footer end -->

                                        </div>
                                        <!-- widget main end -->
                                    </div>
                                    <!-- widget body end -->

                                </div>
                                <!-- widget box end -->
                                <!-- grid Bulk end -->

                            </div>

                            <!-- modal body form end -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- modal body end -->
            <!-- modal footer start -->
            <div class="modal-footer">
                <button class="btn btn-sm btn-primary" data-bind="click: $root.save_u, visible: $root.IsNewDataUnderlying(), disable:!$root.IsEditTableUnderlying()">
                    <i class="icon-save"></i>
                    Save
                </button>
                <button class="btn btn-sm btn-primary" data-bind="click: $root.update_u, visible: !$root.IsNewDataUnderlying() && !$root.IsUtilize_u()">
                    <i class="icon-edit"></i>
                    Update
                </button>
                <button class="btn btn-sm btn-warning" data-bind="click: $root.delete_u, visible: !$root.IsNewDataUnderlying() && !$root.IsUtilize_u()">
                    <i class="icon-trash"></i>
                    Delete
                </button>
                <button class="btn btn-sm" data-bind="click: $root.cancel_u" data-dismiss="modal">
                    <i class="icon-remove"></i>
                    Cancel
                </button>
            </div>
            <!-- modal footer end -->

        </div>
    </div>
</div>

</div>


<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/Knockout/knockout.mapping-latest.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/Helper.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/CalculatedFX.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/NewTransaction.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.maskedinput.min.js"></script>
