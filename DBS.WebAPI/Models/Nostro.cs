﻿using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace DBS.WebAPI.Models
{
    #region property
    public class CodeModel
    {
        
        public int ID { get; set; }       
        public string Code { get; set; }        
        public string BranchCode { get; set; }        
        public string SwiftCode { get; set; }        
        public string BankAccount { get; set; }        
        public string Description { get; set; }       
        public string CommonName { get; set; }        
        public string Currency { get; set; }      
        public string PGSL { get; set; }  
    }

    [ModelName("Nostro")]
    public class NostroModel
    {
        public int? ID { get; set; }
        public CurrencyModel Currency { get; set; }
        public decimal? Amount { get; set; }
        public BankModel Bank { get; set; }
        public string NostroUsed { get; set; }
        public CodeModel Code { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }       

    }

    public class NostroRow : NostroModel
    {
        public int RowID { get; set; }
    }

    public class NostroGrid : Grid
    {
        public IList<NostroRow> Rows { get; set; }
    }
    #endregion

    #region filter
    public class NostroFilter : Filter
    {

    }
    #endregion

    #region interface
    public interface INostroRepository : IDisposable
    {
        bool GetNostroByID(int Id, ref NostroModel Nostro, ref string Message);
        bool GetNostro(ref IList<NostroModel> Nostros, int Limit, int Index, ref string Message);
        bool GetNostro(ref IList<NostroModel> Nostros, ref string Message);
        bool GetNostro(int Page, int Size, IList<NostroFilter> Filters, string sortColumn, string sortOrder, ref NostroGrid Nostro, ref string Message);
        bool GetNostro(NostroFilter Filter, ref IList<NostroModel> Nostros, ref string Message);
        bool GetNostro(string Key, int Limit, ref IList<NostroModel> Nostros, ref string Message);
        bool AddNostro(NostroModel Nostro, ref string Message);
        bool UpdateNostro(int Id, NostroModel Nostro, ref string Message);
        bool DeleteNostro(int Id, ref string Message);
    }
    #endregion

    #region repository
    public class NostroRepository : INostroRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();
        public bool GetNostroByID(int id, ref NostroModel nostro, ref string message)
        {
            bool isSuccess = false;
            try
            {
                nostro = (from nos in context.Nostroes
                          join bank in context.Banks on nos.BankID equals bank.BankID
                          join currency in context.Currencies on nos.CurrencyID equals currency.CurrencyID
                          where nos.IsDeleted.Equals(false) && bank.IsDeleted.Equals(false) && currency.IsDeleted.Equals(false) && nos.NostroID.Equals(id)
                          select new NostroModel
                          {
                              ID = nos.NostroID,
                              Currency = nos.Currency != null ? new CurrencyModel { ID = nos.Currency.CurrencyID, CodeDescription = nos.Currency.CurrencyCode + " (" + nos.Currency.CurrencyDescription + ")" } : new CurrencyModel { ID = 0, CodeDescription = "" },
                              Amount = nos.Amount,
                              Bank = nos.Bank != null ? new BankModel { ID = nos.Bank.BankID, Description = nos.Bank.BankDescription } : new BankModel { ID = 0, Description = "" },
                              NostroUsed = nos.NostroUsed,
                              Code = new CodeModel { SwiftCode  = string.IsNullOrEmpty(nos.Code) ? "-" : nos.Code },
                              LastModifiedDate = nos.UpdateDate == null ? nos.CreateDate : nos.UpdateDate.Value,
                              LastModifiedBy = nos.UpdateDate == null ? nos.CreateBy : nos.UpdateBy
                          }).SingleOrDefault();
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool GetNostro(ref IList<NostroModel> nostros, int limit, int index, ref string message)
        {
            bool isSuccess = false;
            try
            {
                int skip = limit * index;
                using(DBSEntities context = new DBSEntities()) {
                    nostros = (from nostro in context.Nostroes
                               join bank in context.Banks on nostro.BankID equals bank.BankID
                               join currency in context.Currencies on nostro.CurrencyID equals currency.CurrencyID
                               where nostro.IsDeleted.Equals(false) && bank.IsDeleted.Equals(false) && currency.IsDeleted.Equals(false)
                               orderby new { nostro.Amount, nostro.NostroUsed }
                               select new NostroModel {
                                   ID = nostro.NostroID,
                                   Currency = nostro.Currency != null ? new CurrencyModel { ID = nostro.Currency.CurrencyID, CodeDescription = nostro.Currency.CurrencyCode + " (" + nostro.Currency.CurrencyDescription + ")" } : new CurrencyModel { ID = 0, CodeDescription = "" },
                                   Amount = nostro.Amount,
                                   Bank = nostro.Bank != null ? new BankModel { ID = nostro.Bank.BankID, Description = nostro.Bank.BankDescription } : new BankModel { ID = 0, Description = "" },
                                   NostroUsed = nostro.NostroUsed,
                                   Code = new CodeModel { SwiftCode = string.IsNullOrEmpty(nostro.Code) ? "-" : nostro.Code },
                                   LastModifiedDate = nostro.UpdateDate == null ? nostro.CreateDate : nostro.UpdateDate.Value,
                                   LastModifiedBy = nostro.UpdateDate == null ? nostro.CreateBy : nostro.UpdateBy
                               }).Skip(skip).Take(limit).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool GetNostro(ref IList<NostroModel> nostros, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    nostros = (from nostro in context.Nostroes
                               //join bank in context.Banks on nostro.BankID equals bank.BankID //Fandi 2015-12-03
                               join currency in context.Currencies on nostro.CurrencyID equals currency.CurrencyID
                               where nostro.IsDeleted.Equals(false) 
                               //&& bank.IsDeleted.Equals(false) 
                               && currency.IsDeleted.Equals(false)
                               orderby new { nostro.Amount, nostro.NostroUsed }
                               select new NostroModel {
                                   ID = nostro.NostroID,
                                   Currency = nostro.Currency != null ? new CurrencyModel { ID = nostro.Currency.CurrencyID, CodeDescription = nostro.Currency.CurrencyCode + " (" + nostro.Currency.CurrencyDescription+")" } : new CurrencyModel { ID = 0, CodeDescription = "" },
                                   Amount = nostro.Amount,
                                   Bank = nostro.Bank != null ? new BankModel { ID = nostro.Bank.BankID, Description = nostro.Bank.BankDescription } : new BankModel { ID = 0, Description = "" },
                                   NostroUsed = nostro.NostroUsed,
                                   Code = new CodeModel { SwiftCode = string.IsNullOrEmpty(nostro.Code) ? "-" : nostro.Code },
                                   LastModifiedDate = nostro.UpdateDate == null ? nostro.CreateDate : nostro.UpdateDate.Value,
                                   LastModifiedBy = nostro.UpdateDate == null ? nostro.CreateBy : nostro.UpdateBy
                               }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool GetNostro(int page, int size, IList<NostroFilter> filters, string sortColumn, string sortOrder, ref NostroGrid nostroGrid, ref string message)
        {
            bool isSuccess = false;
            try
            {
                NostroModel filter = new NostroModel();
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();
                filter.Currency = new CurrencyModel { CodeDescription = string.Empty };
                filter.Bank = new BankModel { Description = string.Empty };
                filter.Code = new CodeModel { SwiftCode = string.Empty };
                
                if (filters != null)
                {
                    if (filters.Count > 0)
                    {
                        foreach ( var item in filters)
                        {
                            if (item.Field == "Currency") filter.Currency.CodeDescription = item.Value.ToString();
                            if (item.Field == "Amount") {
                                decimal resultConvertion = 0.0M;
                                bool isSuccessCon = Decimal.TryParse(item.Value.ToString() , out resultConvertion);
                                if (isSuccessCon) filter.Amount = resultConvertion;
                                else filter.Amount = 0;
                            } 
                            if (item.Field == "Bank") filter.Bank.Description = item.Value.ToString();
                            if (item.Field == "NostroUsed") filter.NostroUsed = item.Value.ToString();
                            if (item.Field == "Code") filter.Code.SwiftCode = item.Value.ToString();
                            if (item.Field == "LastModifiedBy") filter.LastModifiedBy = item.Value.ToString();
                            if (item.Field == "LastModifiedDate")
                            {
                                filter.LastModifiedDate = DateTime.Parse(item.Value);
                                maxDate = filter.LastModifiedDate.Value.AddDays(1);
                            }
                        }
                    }
                    /*
                    filter.Currency.CodeDescription = filters.Where(a => a.Field == "Currency").Select(a => a.Value).SingleOrDefault();
                    filter.Amount = Decimal.Parse(filters.Where(a => a.Field == "Amount").Select(a => a.Value).SingleOrDefault());
                    filter.Bank.Description = filters.Where(a => a.Field == "Bank").Select(a => a.Value).SingleOrDefault();
                    filter.NostroBank = (string)filters.Where(a => a.Field == "NostroBank").Select(a => a.Value).SingleOrDefault();
                    filter.Code.SwiftCode = (string)filters.Where(a => a.Field == "Code").Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field == "LastModifiedBy").Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field == "LastModifiedDate").Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }*/
                }
                    using (DBSEntities context = new DBSEntities())
                    {
                        var data = (from nostro in context.Nostroes
                                    join bank in context.Banks on nostro.BankID equals bank.BankID into leftNostro
                                    from ln in leftNostro.Where(a=>a.IsDeleted.Equals(false)).DefaultIfEmpty()
                                    join currency in context.Currencies on nostro.CurrencyID equals currency.CurrencyID into NostroCurrency
                                    from ln2 in NostroCurrency.Where(a=>a.IsDeleted.Equals(false)).DefaultIfEmpty()
                                    let isCurrency = !string.IsNullOrEmpty(filter.Currency.CodeDescription)
                                    let isAmount = (filter.Amount.HasValue ? (filter.Amount.Value > 0 ? true: false) : false)
                                    let isBank = !string.IsNullOrEmpty(filter.Bank.Description)
                                    let isNostroBank = !string.IsNullOrEmpty(filter.NostroUsed)
                                    let isCode = !string.IsNullOrEmpty(filter.Code.SwiftCode)
                                    let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                    let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                    //where nostro.IsDeleted.Equals(false) && ln.IsDeleted.Equals(false) && ln2.IsDeleted.Equals(false)
                                    where nostro.IsDeleted.Equals(false)
                                    && (isNostroBank ? nostro.NostroUsed.Contains(filter.NostroUsed) : true)
                                    && (isCurrency ? ln2.CurrencyCode.Contains(filter.Currency.CodeDescription) : true)
                                    && (isAmount ? nostro.Amount.Value.Equals(filter.Amount.Value) : true)
                                    && (isBank ? ln.BankDescription.Contains(filter.Bank.Description) : true)
                                    && (isCode ? nostro.Code.Contains(filter.Code.SwiftCode) : true)
                                    && (isCreateBy ? (nostro.UpdateDate == null ? nostro.CreateBy.Contains(filter.LastModifiedBy) : nostro.UpdateBy.Contains(filter.LastModifiedBy)) : true) 
                                    && (isCreateDate ? ((nostro.UpdateDate == null ? nostro.CreateDate : nostro.UpdateDate.Value) > filter.LastModifiedDate.Value && ((nostro.UpdateDate == null ? nostro.CreateDate : nostro.UpdateDate.Value) < maxDate)) : true)
                                    select new NostroModel
                                    {
                                        ID = nostro.NostroID,
                                        Currency = new CurrencyModel { ID = ln2.CurrencyID, CodeDescription = ln2.CurrencyCode + " (" + ln2.CurrencyDescription+")" },
                                        Amount = nostro.Amount,
                                        Bank = new BankModel { ID = ln.BankID > 0 ? ln.BankID : 0, Description = string.IsNullOrEmpty(ln.BankDescription) ? "-" : ln.BankDescription },
                                        NostroUsed = nostro.NostroUsed,
                                        Code = new CodeModel { SwiftCode = string.IsNullOrEmpty(nostro.Code) ? "-" : nostro.Code },
                                        LastModifiedBy = nostro.UpdateDate == null ? nostro.CreateBy : nostro.UpdateBy,
                                        LastModifiedDate = nostro.UpdateDate == null ? nostro.CreateDate : nostro.UpdateDate.Value
                                    });

                        nostroGrid.Page = page;
                        nostroGrid.Size = size;
                        nostroGrid.Total = data.Count();
                        nostroGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                        nostroGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new NostroRow {
                            RowID = i +1,
                            ID = a.ID,
                            Currency = a.Currency,
                            Amount = a.Amount,
                            Bank = a.Bank,
                            NostroUsed = a.NostroUsed,
                            Code = a.Code,
                            LastModifiedDate = a.LastModifiedDate,
                            LastModifiedBy = a.LastModifiedBy,                           
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                    }
                 isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool GetNostro(NostroFilter filter, ref IList<NostroModel> nostros, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetNostro(string key, int limit, ref IList<NostroModel> nostros, ref string message)
        {
            bool isSuccess = false;
            try
            {
                nostros = (from nostro in context.Nostroes
                           join currency in context.Currencies on nostro.CurrencyID equals currency.CurrencyID
                           join bank in context.Banks on nostro.BankID equals bank.BankID
                           where nostro.IsDeleted.Equals(false) && currency.IsDeleted.Equals(false) && bank.IsDeleted.Equals(false) && (nostro.NostroUsed.Contains(key) || bank.BankDescription.Contains(key) || currency.CurrencyDescription.Contains(key) || nostro.Code.Contains(key))
                           orderby new { nostro.NostroUsed }
                           select new NostroModel {
                               ID = nostro.NostroID,
                               Currency = nostro.Currency != null ? new CurrencyModel { ID = nostro.Currency.CurrencyID, CodeDescription = nostro.Currency.CurrencyCode + " (" + nostro.Currency.CurrencyDescription +")"} : new CurrencyModel { ID = 0, CodeDescription = "" },
                               Amount = nostro.Amount,
                               Bank = nostro.Bank != null ? new BankModel { ID = nostro.Bank.BankID, Description = nostro.Bank.BankDescription } : new BankModel { ID = 0, Description = "" },
                               NostroUsed = nostro.NostroUsed,
                               Code = new CodeModel { SwiftCode = string.IsNullOrEmpty(nostro.Code) ? "-" : nostro.Code },
                               LastModifiedDate = nostro.UpdateDate == null ? nostro.CreateDate : nostro.UpdateDate.Value,
                               LastModifiedBy = nostro.UpdateDate == null ? nostro.CreateBy : nostro.UpdateBy                              
                           }).Take(limit).ToList();
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool AddNostro(NostroModel nostro, ref string message)
        {
            bool isSuccess = false;
            if (nostro != null)
            {
                try
                {
                   
                        context.Nostroes.Add(new Entity.Nostro()
                        {
                            CurrencyID = nostro.Currency.ID,
                            Amount = nostro.Amount ?? 0,
                            BankID = nostro.Bank == null ? (int?)null : (nostro.Bank.ID > 0 ? nostro.Bank.ID : (int?)null),
                            NostroUsed = nostro.NostroUsed,
                            Code = nostro.Code.SwiftCode,
                            IsDeleted = false,
                            CreateDate = DateTime.UtcNow,
                            CreateBy = currentUser.GetCurrentUser().LoginName
                        });

                        context.SaveChanges();

                        isSuccess = true;
                    
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
            }
            
            return isSuccess;
        }

        public bool UpdateNostro(int id, NostroModel nostro, ref string message)
        {
            if (nostro.Bank.Description == "") nostro.Bank.ID = 0;
            bool isSuccess = false;
            try
            {
                Entity.Nostro data = context.Nostroes.Where(a => a.NostroID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    
                        data.CurrencyID = nostro.Currency.ID;
                        data.Amount = nostro.Amount ?? 0;
                        data.BankID = nostro.Bank == null ? (int?)null : (nostro.Bank.ID > 0 ? nostro.Bank.ID : (int?)null);
                        data.NostroUsed = nostro.NostroUsed;
                        data.Code = nostro.Code.SwiftCode;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().LoginName;
                        context.SaveChanges();
                        isSuccess = true;
                                                     
                }
                else
                {
                    message = string.Format("Nostro data for Nostro ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool DeleteNostro(int id, ref string message)
        {
            bool isSuccess = false;
            try
            {
                Entity.Nostro data = context.Nostroes.Where(a => a.NostroID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().LoginName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Nostro data for Nostro ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion

}