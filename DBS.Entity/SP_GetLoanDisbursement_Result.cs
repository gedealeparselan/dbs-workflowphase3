//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DBS.Entity
{
    using System;
    
    public partial class SP_GetLoanDisbursement_Result
    {
        public long TransactionID { get; set; }
        public string ApplicationID { get; set; }
        public int IsTopUrgent { get; set; }
        public long TransactionFundingmemoID { get; set; }
        public Nullable<System.DateTime> ValueDate { get; set; }
        public Nullable<int> BizSegmentID { get; set; }
        public string BizSegmentDescription { get; set; }
        public string LoanContractNo { get; set; }
        public string CIF { get; set; }
        public string CustomerName { get; set; }
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public int CurrencyID { get; set; }
        public string CurrencyCode { get; set; }
        public Nullable<int> CustomerCategoryID { get; set; }
        public string CustomerCategoryName { get; set; }
        public Nullable<int> LoanTypeID { get; set; }
        public string LoanTypeName { get; set; }
        public string SolID { get; set; }
        public Nullable<int> ProgramTypeID { get; set; }
        public string ProgramTypeName { get; set; }
        public Nullable<int> FinacleSchemeCodeID { get; set; }
        public string FinacleSchemeCodeName { get; set; }
        public Nullable<bool> IsAdhoc { get; set; }
        public Nullable<int> CreditingOperativeID { get; set; }
        public Nullable<int> DebitingOperativeID { get; set; }
        public Nullable<System.DateTime> MaturityDate { get; set; }
        public string InterestCodeID { get; set; }
        public string InterestCodeName { get; set; }
        public Nullable<decimal> BaseRate { get; set; }
        public Nullable<decimal> AccountPreferencial { get; set; }
        public Nullable<decimal> AllInRate { get; set; }
        public Nullable<int> RepricingPlanID { get; set; }
        public string RepricingPlanName { get; set; }
        public Nullable<decimal> ApprovedMarginAsPerCM { get; set; }
        public Nullable<decimal> SpecialFTP { get; set; }
        public Nullable<decimal> Margin { get; set; }
        public Nullable<decimal> AllInSpecialRate { get; set; }
        public Nullable<System.DateTime> PeggingDate { get; set; }
        public Nullable<int> PeggingFrequencyID { get; set; }
        public string PeggingFrequencyName { get; set; }
        public Nullable<int> PrincipalStartDate { get; set; }
        public Nullable<System.DateTime> InterestStartDate { get; set; }
        public string NoOfInstallment { get; set; }
        public Nullable<int> PrincipalFrequencyID { get; set; }
        public string PrincipalFrequencyName { get; set; }
        public Nullable<int> InterestFrequencyID { get; set; }
        public string InterestFrequencyName { get; set; }
        public string LimitIDinFin10 { get; set; }
        public Nullable<System.DateTime> LimitExpiryDate { get; set; }
        public Nullable<decimal> SanctionLimit { get; set; }
        public Nullable<int> SanctionLimitCurrID { get; set; }
        public string SanctionLimitCurrName { get; set; }
        public Nullable<decimal> Utilization { get; set; }
        public Nullable<int> UtilizationCurrID { get; set; }
        public string UtilizationCurrName { get; set; }
        public Nullable<decimal> Outstanding { get; set; }
        public Nullable<int> OutstandingCurrID { get; set; }
        public string OutstandingCurrName { get; set; }
        public string CSOName { get; set; }
        public string Remarks { get; set; }
        public string OtherRemarks { get; set; }
        public System.DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public Nullable<System.DateTime> UpdateDate { get; set; }
        public string UpdateBy { get; set; }
    }
}
