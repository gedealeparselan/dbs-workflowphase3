﻿<%@ Assembly Name="DBS.Sharepoint, Version=1.0.0.0, Culture=neutral, PublicKeyToken=f75c72caf4809f4b" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, 
PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, 
PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, 
PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PM_FundingMemoTLUserControl.ascx.cs"
    Inherits="DBS.Sharepoint.SPWebparts.PM.PM_FundingMemoTL.PM_FundingMemoTLUserControl" %>

<h1 class="header smaller lighter dark">FundingMemo</h1>
<!-- widget box start -->
<div id="widget-box" class="widget-box">
    <!-- widget header start -->
    <div class="widget-header widget-hea1der-small header-color-dark">
        <h6>Holiday Table</h6>
        <div class="widget-toolbar">
            <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                <i class="blue icon-filter"></i>
                <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: GridProperties().AllowFilter" />
                <span class="lbl"></span>
            </label>
            <a href="#" data-bind="click: GetData" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                <i class="blue icon-refresh"></i>
            </a>
        </div>
    </div>
    <!-- widget header end -->
    <!-- widget body start -->
    <div class="widget-body">
        <!-- widget main start -->
        <div class="widget-main padding-0">
            <!-- widget slim control start -->
            <div class="slim-scroll" data-height="400">
                <!-- widget content start -->
                <div class="content">
                    <!-- table responsive start -->
                    <div class="table-responsive">
                        <div class="dataTables_wrapper" role="grid">
                            <table id="BeneBank-table" class="table table-striped table-bordered table-hover dataTable">
                                <thead>
                                    <tr data-bind="with: GridProperties">
                                        <th style="width: 50px">No.</th>
                                        <th data-bind="click: function () { Sorting('CIF'); }, css: GetSortedColumn('CIF')">CIF</th>
                                        <th data-bind="click: function () { Sorting('Customer'); }, css: GetSortedColumn('Customer')">Customer Name</th>
                                        <%--<th data-bind="click: function () { Sorting('BizSegment'); }, css: GetSortedColumn('BizSegment')">Bizsegment 

Name</th>--%>
                                        <th data-bind="click: function () { Sorting('LastModifiedBy'); }, css: GetSortedColumn('LastModifiedBy')">Modified 

By</th>
                                        <th data-bind="click: function () { Sorting('LastModifiedDate'); }, css: GetSortedColumn('LastModifiedDate')">Modified Date</th>
                                    </tr>
                                </thead>
                                <thead data-bind="visible: GridProperties().AllowFilter" class="table-filter">
                                    <tr>
                                        <th class="clear-filter">
                                            <a href="#" data-bind="click: ClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right"
                                                title="Clear Filters">
                                                <i class="green icon-trash"></i>
                                            </a>
                                        </th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCIF, event: {
    change: GridProperties().Filter
}" /></th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCustomer, event: {
    change: GridProperties().Filter
}" /></th>
                                        <%--<th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterBizSegment, 

event: { change: GridProperties().Filter }" /></th>--%>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterModifiedBy, event: {
    change: GridProperties().Filter
}" /></th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value:

$root.FilterModifiedDate, event: { change: GridProperties().Filter }" /></th>
                                    </tr>
                                </thead>
                                <tbody data-bind="foreach: Fundings, visible: Fundings().length > 0">
                                    <tr data-bind="click: $root.GetSelectedRow" data-toggle="modal">
                                        <td><span data-bind="text: RowID"></span></td>
                                        <td><span data-bind="text: CIF"></span></td>
                                        <td><span data-bind="text: Customer.Name"></span></td>
                                        <%--<td><span data-bind="text: BizSegment.Name"></span></td>--%>
                                        <td><span data-bind="text: LastModifiedBy"></span></td>
                                        <td><span data-bind="text: $root.LocalDate(LastModifiedDate)"></span></td>
                                    </tr>
                                </tbody>
                                <tbody data-bind="visible: Fundings().length == 0">
                                    <tr>
                                        <td colspan="11" class="text-center">No entries available.
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- table responsive end -->
                </div>
                <!-- widget content end -->
            </div>
            <!-- widget slim control end -->
            <!-- widget footer start -->
            <div class="widget-toolbox padding-8 clearfix">
                <div class="row" data-bind="with: GridProperties">
                    <!-- pagination size start -->
                    <div class="col-sm-6">
                        <div class="dataTables_paginate paging_bootstrap pull-left">
                            Showing
                            <select data-bind="options: AvailableSizes, selectedOptions: Size, event: {
    change: OnPageSizeChange
}"
                                class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries">
                            </select>
                            rows of <span data-bind="text: Total"></span>
                            entries
                        </div>
                    </div>
                    <!-- pagination size end -->
                    <!-- pagination page jump start -->
                    <div class="col-sm-3">
                        <div class="dataTables_paginate paging_bootstrap">
                            Page
                            <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px"
                                class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                            of <span data-bind="text: TotalPages"></span>
                        </div>
                    </div>
                    <!-- pagination page jump end -->
                    <!-- pagination navigation start -->
                    <div class="col-sm-3">
                        <div class="dataTables_paginate paging_bootstrap">
                            <ul class="pagination">
                                <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                        <i class="icon-double-angle-left"></i>
                                    </a>
                                </li>
                                <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                        <i class="icon-angle-left"></i>
                                    </a>
                                </li>
                                <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                        <i class="icon-angle-right"></i>
                                    </a>
                                </li>
                                <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                        <i class="icon-double-angle-right"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- pagination navigation end -->

                </div>
            </div>
            <!-- widget footer end -->
        </div>
        <!-- widget main end -->
    </div>
    <!-- widget body end -->
</div>
<!-- widget box end -->
<%--<div data-bind="if: !$root.IsRoleMaker()">--%>
<h4 class="pink">
    <i class="icon-hand-right green"></i>
    <a href="#modal-form" role="button" class="blue" data-toggle="modal" data-bind="click: NewData">New Funding Memo</a>
</h4>
<%--</div>--%>

<!-- modal form start -->
<div id="modal-form" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
    <div class="modal-dialog" style="width: 1200px">
        <div class="modal-content">
            <!-- modal header start -->
            <div class="modal-header">
                <h4 class="blue bigger">
                    <span data-bind="if: $root.IsNewData()">Create a new funding memo</span>
                    <span data-bind="if: !$root.IsNewData()">Modify a funding memo</span>
                </h4>
            </div>
            <div class="widget-box ui-sortable-handle">
                <div class="widget-header">
                    <h4 class="widget-title">I.CUSTOMER DETAILS</h4>
                </div>
                <div class="widget-body">
                    <div class="widget-main">
                        <form class="form-inline">
                            <div class="row">
                                <div class="col-md-2">
                                    <label>Customer Name</label>
                                </div>
                                <div class="col-md-3">
                                    <input type="text" id="Customer" name="Customer" data-bind="value: CustomerName, disable: $root.Readonly" class="col-sm-15" data-rule-required="true" data-rule-maxlength="50" />
                                    <label class="control-label bolder text-danger" for="Customer">*</label>
                                </div>
                                <div class="col-md-2">
                                    <label>SOL ID</label>
                                </div>
                                <div class="col-md-3" data-rule-required="true" data-rule-maxlength="255">
                                    <input type="text" id="solidut" name="solidut" data-bind="value: SolID" class="col-sm-15" />
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="widget-main">
                        <form class="form-inline">
                            <div class="row">
                                <div class="col-md-2">
                                    <label>Customer ID</label>
                                </div>
                                <div class="col-md-3" data-rule-required="true" data-rule-maxlength="50">
                                    <input type="text" id="CIF" name="CIF" data-bind="value: CIF"  disabled="disabled" class="col-sm-15" style="padding-left: 5px;" />
                                </div>
                                <div class="col-md-2">
                                    <label>Segment</label>
                                </div>
                                <div class="col-md-3" data-rule-required="true" data-rule-maxlength="255">
                                    <input type="text" id="BizSegmentDesc" name="BizName" disabled="disabled" data-bind="value: BizName" class="col-sm-15" />
                                    <input type="text" id="BizSegmentID" name="BizSegmentID" data-bind="value: BizSegmentID, visible: false " class="col-sm-15" />

                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="widget-main">
                        <form class="form-inline">
                            <div class="row">
                                <div class="col-md-2">
                                    <label>Customer Category</label>
                                </div>
                                <div class="col-md-3">
                                    <input type="text" id="CustomerCategoryID"  disabled="disabled" name="CustomerCategoryID" data-bind="value: CustomerCategoryID" class="col-sm-15" data-rule-required="true" data-rule-maxlength="50" />
                                    <label class="control-label bolder text-danger starremove starremovefirefox" style="position: absolute;" for="InterestStartDate">*</label>
                                </div>
                                <div class="col-md-2">
                                    <label>Program Type</label>
                                </div>
                                <div class="col-md-3">
                                    <select id="ProgramType" name="ProgramType" data-bind="value: ProgramType().ProgramTypeID, options: ddlProgramType, optionsText: 'ProgramTypeName', optionsValue: 'ProgramTypeID', optionsCaption: 'Please Select...', disable: $root.Readonly" class="valid col-sm-15" data-rule-required="true"></select>
                                    
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="widget-main">
                        <form class="form-inline">
                            <div class="row">
                                <div class="col-md-2">
                                    <label>Loan Type</label>
                                </div>
                                <div class="col-md-3">
                                    <select id="LoanID" name="LoanID" data-bind="value: LoanID().LoanTypeID, options: ddlLoanID, optionsText: 'LoanTypeName', optionsValue: 'LoanTypeID', optionsCaption: 'Please Select...', disable: $root.Readonly" class="valid col-xs-10 col-sm-11" data-rule-required="true">
                                    </select>
                                    <label class="control-label bolder text-danger starremove starremovefirefox" style="position: absolute;" for="InterestStartDate">*</label>
                                </div>
                                <div class="col-md-2">
                                    <label>Scheme Code:Finacle</label>
                                </div>
                                <div class="col-md-3">
                                    <select id="FinacleScheme" name="FinacleScheme" data-bind="value: FinacleScheme().FinacleSchemeCodeID, options: ddlFinacleScheme, optionsText: 'FinacleSchemeCodeName', optionsValue: 'FinacleSchemeCodeID', optionsCaption: 'Please Select...', event: { change: function () { $root.Ishidden() } }" class="valid col-xs-10 col-sm-11" data-rule-required="true">
                                    </select>
                                    <label class="control-label bolder text-danger" for="SanctionLimitCurrID" data-bind="disable: $root.Readonly, visible: $root.Readonly() == false">*</label>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="widget-main">
                        <form class="form-inline">
                            <div class="row">
                                <div class="col-md-2">
                                    <label></label>
                                </div>
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-2">
                                    <label>Adhoc</label>
                                </div>
                                <div class="col-md-3">
                                    <input type="checkbox" id="IsAdhoc" name="IsAdhoc" data-bind="value: IsAdhoc, disable: true" class="col-xs-10" data-rule-required="true" data-rule-maxlength="255" />
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="widget-box ui-sortable-handle">
                <div class="widget-header">
                    <h4 class="widget-title">II.LOAN DETAILS</h4>
                </div>
                <div class="widget-body">
                    <div class="widget-main">
                        <form class="form-inline">
                            <div class="row">
                                <div class="col-md-2">
                                    <label>Amount Disburse</label>
                                </div>
                                <div class="col-md-1">
                                    <select id="AmountDisburseID" name="AmountDisburseID" data-bind="value: AmountDisburseID().ID, options: ddlAmountDisburseID, optionsText: 'Code' + 'Description', optionsValue: 'ID', optionsCaption: 'Please Select...', disable: $root.Readonly" class="valid col-xs-11 col-sm-11" style="width: 80px" data-rule-required="true"></select>
                                </div>
                                <div class="col-md-2">
                                    <input type="text" id="AmountDisburse" data-in="" name="AmountDisburse" data-bind="value: AmountDisburse, disable: $root.Readonly" class="col-sm-15" data-rule-maxlength="50" />
                                    <label class="control-label bolder text-danger starremove starremovefirefox" style="position: absolute;" for="InterestStartDate">*</label>
                                </div>
                                <div class="col-md-2">
                                    <label>Crediting Operative/Office A/C ID</label>
                                </div>
                                <div class="col-md-3">
                                    <input type="text" id="CreditingOperative" name="CreditingOperative" data-bind="value: CreditingOperative, disable: $root.Readonly" class="col-sm-15" data-rule-required="true" data-rule-maxlength="255" />
                                    <label class="control-label bolder text-danger starremove starremovefirefox" style="position: absolute;" for="InterestStartDate">*</label>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="widget-main">
                        <form class="form-inline">
                            <div class="row">
                                <div class="col-md-2">
                                    <label></label>
                                </div>
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-2">
                                    <label>Debiting Operative/office A/C(settlement)</label>
                                </div>
                                <div class="col-md-3">
                                    <input type="text" id="DebitingOperative" name="DebitingOperative" data-bind="value: DebitingOperative, disable: $root.Readonly" class="col-sm-15" data-rule-required="true" data-rule-maxlength="255" />
                                    <label class="control-label bolder text-danger starremove starremovefirefox" style="position: absolute;" for="InterestStartDate">*</label>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="widget-main">
                        <form class="form-inline">
                            <div class="row">
                                <div class="col-md-2">
                                    <label>Value Date</label>
                                </div>
                                <div class="col-md-3">
                                    <input class="col-xs-15 date-picker" type="text" autocomplete="off" data-date-format="dd-M-yyyy" id="ValueDate" name="ValueDate" data-rule-required="true" data-rule-value="true" data-bind="value: ValueDate" />
                                    <span><i class="icon-calendar bigger-110"></i></span>
                                    <label class="control-label bolder text-danger starremove starremovefirefox" style="position: absolute;" for="InterestStartDate">*</label>
                                </div>
                                <div class="col-md-2">
                                    <label>Maturity Date</label>
                                </div>
                                <div class="col-md-3">
                                    <input class="col-xs-15 date-picker" type="text" autocomplete="off" data-date-format="dd-M-yyyy" id="MaturityDate" name="MaturityDate" data-rule-required="true" data-rule-value="true" data-bind="value: MaturityDate" />
                                    <span><i class="icon-calendar bigger-110"></i></span>
                                    <label class="control-label bolder text-danger starremove starremovefirefox" style="position: absolute;" for="InterestStartDate">*</label>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="widget-box ui-sortable-handle">
                <div class="widget-header">
                    <h4 class="widget-title">III.INTEREST RATE</h4>
                    
                </div>
                <div class="widget-body">
                    <div class="widget-main">
                        <form class="form-inline">
                            <div class="row">
                                <div class="col-md-2">
                                    <label>Interest rate Code</label>
                                </div>
                                <div class="col-md-3">
                                    <select id="Interest" name="Interest" data-bind="value: Interest().InterestCodeID, options: ddlInterest, optionsText: 'InterestCodeName', optionsValue: 'InterestID', optionsCaption: 'Please Select...', disable: $root.Readonly" class="valid col-xs-10 col-sm-11" data-rule-required="true">
                                    </select>
                                    <label class="control-label bolder text-danger starremove starremovefirefox" style="position: absolute;" for="InterestStartDate">*</label>
                                </div>
                                <div class="col-md-2">
                                    <label>Approve Margin AS Per Credit Memo</label>
                                </div>
                                <div class="col-md-3">
                                    <input type="text" id="ApprovedMarginAsPerCM" name="ApprovedMarginAsPerCM" data-bind="value: ApprovedMarginAsPerCM, disable: $root.Readonly" class="col-sm-15" data-rule-required="true" data-rule-maxlength="255" />
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="widget-main">
                        <form class="form-inline">
                            <div class="row">
                                <div class="col-md-2">
                                    <label>Base Rate</label>
                                </div>
                                <div class="col-md-3">
                                    <input type="text" id="BaseRate" name="BaseRate" data-bind="value: BaseRate" disabled="disabled" class="col-sm-8" data-rule-required="true" data-rule-maxlength="255"  />
                                    <label class="control-label bolder text-danger starremove starremovefirefox" style="position: absolute;" for="InterestStartDate">*</label>
                                    <button class="btn btn-sm btn-primary" style="left:8px"data-bind="click: $root.GetBaseRate">
                                    Calculate
                                    </button>
                                </div>
                                <div class="col-md-2">
                                    <label>Special FTP</label>
                                </div>
                                <div class="col-md-3">
                                    <input type="text" id="SpecialFTP" name="SpecialFTP" data-bind="value: SpecialFTP, disable: $root.Readonly" class="col-sm-15" data-rule-required="true" data-rule-maxlength="255" />
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="widget-main">
                        <form class="form-inline">
                            <div class="row">
                                <div class="col-md-2">
                                    <label>Acc. Prefencial/Margin</label>
                                </div>
                                <div class="col-md-3">
                                    <input type="text" id="AccountPreferencial" name="AccountPreferencial" data-bind="value: AccountPreferencial, disable: $root.Readonly" class="col-sm-15" data-rule-required="true" data-rule-maxlength="255" />
                                    <label class="control-label bolder text-danger starremove starremovefirefox" style="position: absolute;" for="InterestStartDate">*</label>
                                </div>
                                <div class="col-md-2">
                                    <label>Margin</label>
                                </div>
                                <div class="col-md-3">
                                    <input type="text" id="Margin" name="Margin" data-bind="value: Margin, disable: $root.Readonly" class="col-sm-15" data-rule-required="true" data-rule-maxlength="255" />
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="widget-main">
                        <form class="form-inline">
                            <div class="row">
                                <div class="col-md-2">
                                    <label>All In Rate</label>
                                    
                                    
                                </div>
                                <div class="col-md-3">
                                    <input type="text" id="AllInRate" name="AllInRate" disabled="disabled" data-bind="value: AllInRate" class="col-sm-15" data-rule-required="true" data-rule-maxlength="255" />
                                </div>
                                <div class="col-md-2">
                                    <label>All In Special Rate</label>
                                </div>
                                <div class="col-md-2">
                                    <input type="text" id="AllInSpecialRate" name="AllInSpecialRate" data-bind="value: AllInSpecialRate" disabled="disabled" class="col-sm-15" data-rule-required="true" data-rule-maxlength="255" />
                                    
                                </div>
                                <div class="col-md-1">
                                    <button class="btn btn-sm btn-primary" data-bind="click: $root.GetAllInRate">
                                    Calculate
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="widget-main">
                        <form class="form-inline">
                            <div class="row">
                                <div class="col-md-2">
                                    <label>Repricing Plan</label>
                                </div>
                                <div class="col-md-3">
                                    <select id="RepricingPlan" name="RepricingPlan" data-bind="value: RepricingPlan().RepricingPlanID, options: ddlRepricing, optionsText: 'RepricingPlanName', optionsValue: 'RepricingPlanID', optionsCaption: 'Please Select...', disable: $root.Readonly" class="valid col-xs-10 col-sm-11" data-rule-required="true"></select>
                                    <label class="control-label bolder text-danger starremove starremovefirefox" style="position: absolute;" for="InterestStartDate">*</label>
                                </div>
                                <div class="col-md-2">
                                    <label>Pegging Date</label>
                                </div>
                                <div class="col-md-3">
                                    <input class="col-xs-15 date-picker" type="text" autocomplete="off" data-date-format="dd-M-yyyy" id="PeggingDate" name="PeggingDate" data-rule-required="true" data-rule-value="true" data-bind="value: PeggingDate" />
                                    <span><i class="icon-calendar bigger-110"></i></span>
                                    <label class="control-label bolder text-danger starremove starremovefirefox" style="position: absolute;" for="InterestStartDate">*</label>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="widget-main">
                        <form class="form-inline">
                            <div class="row">
                                <div class="col-md-2">
                                    <label></label>
                                </div>
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-2">
                                    <label>Pegging Frequency</label>
                                </div>
                                <div class="col-md-3">
                                    <select id="PeggingFrequency" name="PeggingFrequency" data-bind="value: PeggingFrequency().PeggingFrequencyID, options: ddlFrequency, optionsText: 'PeggingFrequencyName', optionsValue: 'PeggingFrequencyID', optionsCaption: 'Please Select...', disable: $root.Readonly" class="valid col-xs-10 col-sm-11" data-rule-required="true">
                                    </select>
                                    <label class="control-label bolder text-danger starremove starremovefirefox" style="position: absolute;" for="InterestStartDate">*</label>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="widget-box ui-sortable-handle" data-bind="visible: $root.ShortTerm()">
                <div class="widget-header">
                    <h4 class="widget-title">IV.REPAYMENT SCHEDULE</h4>
                </div>
                <div class="widget-body">
                    <div class="widget-main">
                        <form class="form-inline">
                            <div class="row">
                                <div class="col-md-2">
                                    <label>Principal Start Date</label>
                                </div>
                                <div class="col-md-3">
                                    <input class="col-xs-15 date-picker" type="text" autocomplete="off" data-date-format="dd-M-yyyy" id="PrincipalStartDate" name="PrincipalStartDate" data-rule-required="true" data-rule-value="true" data-bind="value: PrincipalStartDate" />
                                    <span><i class="icon-calendar bigger-110"></i></span>
                                    <label class="control-label bolder text-danger starremove starremovefirefox" style="position: absolute;" for="InterestStartDate">*</label>
                                </div>
                                <div class="col-md-2">
                                    <label>Principal Frequency</label>
                                </div>
                                <div class="col-md-3">
                                    <select id="PrincipalFrequency" name="PrincipalFrequency" data-bind="value: PrincipalFrequency().PrincipalFrequencyID, options: ddlFrequency, optionsText: 'PeggingFrequencyName', optionsValue: 'PeggingFrequencyID', optionsCaption: 'Please Select...', disable: $root.Readonly" class="valid col-xs-10 col-sm-11" data-rule-required="true"></select>
                                    <label class="control-label bolder text-danger starremove starremovefirefox" style="position: absolute;" for="InterestStartDate">*</label>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="widget-main">
                        <form class="form-inline">
                            <div class="row">
                                <div class="col-md-2">
                                    <label>Interest Start Date</label>
                                </div>
                                <div class="col-md-3">
                                    <input class="col-xs-15 date-picker" type="text" autocomplete="off" data-date-format="dd-M-yyyy" id="InterestStartDate" name="InterestStartDate" data-rule-required="true" data-rule-value="true" data-bind="value: InterestStartDate" />
                                    <span><i class="icon-calendar bigger-110"></i></span>
                                    <label class="control-label bolder text-danger starremove starremovefirefox" style="position: absolute;" for="InterestStartDate">*</label>
                                </div>
                                <div class="col-md-2">
                                    <label>Interest Frequency</label>
                                </div>
                                <div class="col-md-3">
                                    <select id="InterestFrequency" name="InterestFrequency" data-bind="value: InterestFrequency().InterestFrequencyID, options: ddlFrequency, optionsText: 'PeggingFrequencyName', optionsValue: 'PeggingFrequencyID', optionsCaption: 'Please Select...', disable: $root.Readonly" class="valid col-xs-10 col-sm-11" data-rule-required="true"></select><label class="control-label bolder text-danger" for="InterestFrequency" data-bind="    disable: $root.Readonly, visible: $root.Readonly() == false">*</label>
                                    
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="widget-main">
                        <form class="form-inline">
                            <div class="row">
                                <div class="col-md-2">
                                    <label>No Of Installment</label>
                                </div>
                                <div class="col-md-3">
                                    <input type="text" id="NoOfInstallment" name="NoOfInstallment" data-bind="value: NoOfInstallment, disable: $root.Readonly" class="col-sm-12 valid" data-rule-required="true" data-rule-maxlength="255" />
                                </div>
                                <div class="col-md-2">
                                    <label></label>
                                </div>
                                <div class="col-md-3">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>

            <div class="widget-box ui-sortable-handle">
                <div class="widget-header">
                    <h4 class="widget-title">V.LIMIT</h4>
                </div>
                <div class="widget-body">
                    <div class="widget-main">
                        <form class="form-inline">
                            <div class="row">
                                <div class="col-md-2">
                                    <label>Limit ID in Fin 10</label>
                                </div>
                                <div class="col-md-3">
                                    <input type="text" id="LimitIDinFin10" name="LimitIDinFin10" data-bind="value: LimitIDinFin10, disable: $root.Readonly" class="col-sm-12" data-rule-required="true" data-rule-maxlength="255" />
                                    <label class="control-label bolder text-danger starremove starremovefirefox" style="position: absolute;" for="InterestStartDate">*</label>
                                </div>
                                <div class="col-md-2">
                                    <label>Sanction Limit</label>
                                </div>
                                <div class="col-md-1">
                                    <select id="SanctionLimitCurrID" name="SanctionLimitCurrID" data-bind="value: SanctionLimitCurr().ID, options: ddlSanctionLimitCurrID, optionsText: 'CodeDescription', optionsValue: 'ID', optionsCaption: 'Please Select...', disable: $root.Readonly" class="valid col-xs-10 col-sm-11" data-rule-required="true" style="width: 80px"></select>
                                   
                                </div>
                                <div class="col-md-3">
                                    <input type="text" id="SanctionLimitAmount" data-in="" name="SanctionLimitAmount" data-bind="value: SanctionLimitAmount, disable: $root.Readonly" class="col-sm-15"
                                        data-rule-required="true" data-rule-maxlength="255" />
                                    <label class="control-label bolder text-danger" for="SanctionLimitCurrID" data-bind="disable: $root.Readonly, visible: $root.Readonly() == false">
                                        *</label>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="widget-main">
                        <form class="form-inline">
                            <div class="row">
                                <div class="col-md-2">
                                    <label>Limit Expire Date</label>
                                </div>
                                <div class="col-md-3">
                                    <input class="col-xs-15 date-picker" type="text" autocomplete="off" data-date-format="dd-M-yyyy" id="LimitExpiryDate" name="LimitExpiryDate" data-rule-required="true" data-rule-value="true" data-bind="value: LimitExpiryDate" />
                                    <span><i class="icon-calendar bigger-110"></i></span>
                                  <label class="control-label bolder text-danger starremove starremovefirefox" style="position: absolute;" for="LimitExpiryDate">*</label>
                                </div>
                                <div class="col-md-2">
                                    <label>Utilisation</label>
                                </div>
                                <div class="col-md-1">
                                    <select id="utilization" name="utilization" data-bind="value: utilizationID().ID, options: ddlUtilizationCurrID, optionsText: 'CodeDescription', optionsValue: 'ID', optionsCaption: 'Please Select...', disable: $root.Readonly" class="valid col-xs-10 col-sm-11" data-rule-required="true" style="width: 80px"></select>
                                </div>
                                <div class="col-md-3">
                                    <input type="text" id="utilizationAmount" data-in="" name="utilizationAmount" data-bind="value: utilizationAmount, disable: $root.Readonly" class="col-sm-15" data-rule-required="true" data-rule-maxlength="255" />
                                    
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="widget-main">
                        <form class="form-inline">
                            <div class="row">
                                <div class="col-md-2">
                                    <label></label>
                                </div>
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-2">
                                    <label>Outstanding</label>
                                </div>
                                <div class="col-md-1">
                                    <select id="Outstanding" name="Outstanding" data-bind="value: OutstandingID().ID, options: ddlOutstandingCurrID, optionsText: 'CodeDescription', optionsValue: 'ID', optionsCaption: 'Please Select...', disable: $root.Readonly" class="valid col-xs-10 col-sm-11" data-rule-required="true" style="width: 80px"></select>
                                </div>
                                <div class="col-md-3">
                                    <input type="text" id="OutstandingAmount" name="OutstandingAmount" data-in="" data-bind="value: OutstandingAmount, disable: $root.Readonly" class="col-sm-15" data-rule-required="true" data-rule-maxlength="255" />
                                    <%--<label class="control-label bolder text-danger" for="SanctionLimitCurrID" data-bind="disable: $root.Readonly, visible: $root.Readonly() == false">
                                        *</label>--%>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="widget-box ui-sortable-handle">
                <div class="widget-header">
                    <h4 class="widget-title">VI.CONTACT DETAIL</h4>
                </div>

                <div class="widget-body">
                    <div class="widget-main">
                        <form class="form-inline">
                            <div class="row">
                                <div class="col-md-2">
                                    <label>CSO Name</label>
                                </div>
                                <div class="col-md-7">
                                    <textarea class="col-sm-8" style="width:800px" name="Employee" id="Employee" rows="2" data-rule-required="false" data-bind="value: Employee, disable: true"></textarea>
                                </div>
                                <div class="col-md-3">
                                    <button class="btn btn-primary" id="GetCSOName" style="left:120px" data-bind="click: $root.GetCSOName">
                                        <i class="icon-edit"></i>
                                        Get CSO
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- modal header end -->
            <!-- modal body start -->

            <div class="widget-box ui-sortable-handle">
                <div class="widget-header">
                    <h4 class="widget-title">Required Approval Based on DOA</h4>
                </div>

                <div class="widget-body">
                    <div class="widget-main">
                        <form class="form-inline">
                            <div class="row">
                                <div class="col-md-10">
                                    <textarea class="col-sm-12" name="DOAName" id="DOAName" rows="5" data-rule-required="false" data-bind="value: ApprovalDoA, disable: true"></textarea>
                                    
                                </div>
                                <div class="col-md-2">
                                    <button class="btn btn-sm btn-primary" data-bind=" click: $root.GetDOA">
                        <i class="icon-edit"></i>
                        Get DOA
                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="padding-4">
                <h3 class="header smaller lighter dark">
                    <button class="btn btn-sm btn-primary pull-right" data-bind="click: $root.UploadDocument">
                        <i class="icon-plus"></i>
                        Attach
                    </button>
                </h3>
                <div class="dataTables_wrapper" role="grid">
                    <table class="table table-striped table-bordered table-hover dataTable">
                        <thead>
                            <tr>
                                <th style="width: 50px">No.</th>
                                <th>File Name</th>
                                <th>Purpose of Docs</th>
                                <th>Type of Docs</th>
                                <th>Modified</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody data-bind="foreach: $root.Documents, visible: $root.Documents().length > 0">
                            <tr>
                                <!--data-bind="click: $root.EditDocument"-->
                                <td><span data-bind="text: $index() + 1"></span></td>
                                <td><a data-bind="attr: { href: DocumentPath, target: '_blank' }, text: FileName"></a></td>
                                <td><span data-bind="text: Purpose.Name"></span></td>
                                <td><span data-bind="text: Type.Name"></span></td>
                                <td><span data-bind="text: moment(DocumentPath.lastModifiedDate).format

(config.format.dateTime)"></span></td>
                                <!--$root.LocalDate(LastModifiedDate)-->
                                <td><a href="#" data-bind="click: $root.RemoveDocument">Remove</a></td>
                            </tr>
                        </tbody>
                        <tbody data-bind="visible: $root.Documents().length == 0">
                            <tr>
                                <td colspan="6" align="center">No entries available.</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>

            <!-- modal body end -->
            <!-- modal footer start -->
            <div class="modal-footer">
                <button class="btn btn-sm btn-primary" id="btnsave" data-bind="click: $root.save, visible: $root.IsNewData()">
                    <i class="icon-save"></i>
                    Save
                </button>
                <button class="btn btn-sm btn-primary" data-bind="click: $root.update, visible: $root.IsUpdateData()">
                    <i class="icon-edit"></i>
                    Update
                </button>
                <button class="btn btn-sm btn-warning" data-bind="click: $root.delete, visible: $root.IsDelete()">
                    <i class="icon-trash"></i>
                    Delete
                </button>
                <button class="btn btn-sm" data-dismiss="modal">
                    <i class="icon-remove"></i>
                    Close
                </button>
            </div>
            <!-- modal footer end -->
        </div>
    </div>
</div>



<div id="modal-form-Attach1" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="0" role="dialog">
    <div class="modal-dialog" style="width: 80%">
        <div class="modal-content">
            <!-- modal header start Attach FIle Form -->
            <div class="modal-header">
                <h5 class="blue bigger">
                    <span>Add Attachment File</span>
                </h5>
            </div>
            <!-- modal header end Attach file -->
            <!-- modal body start Attach File -->
            <div class="modal-body overflow-visible">
                <div class="row">
                    <div class="col-xs-12">
                        <!-- modal body form start -->
                        <div id="customer-form1" class="form-horizontal" role="form">
                            <div class="space-4"></div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="Name">
                                    Purpose of Doc
                                </label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <select id="documentPurpose" name="documentPurpose" data-bind="value: $root.Selected().DocumentPurpose_a, options: ddlDocumentPurpose_a, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                        <label class="control-label bolder text-danger">*</label>
                                    </div>
                                </div>
                            </div>

                            <div class="space-4"></div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="description">
                                    Type of Doc
                                </label>

                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <select id="documentType" name="documentType" data-bind="value: $root.Selected().DocumentType_a, options: ddlDocumentType_a, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                        <label class="control-label bolder text-danger">*</label>
                                    </div>
                                </div>
                            </div>
                            <div class="space-4"></div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="document-path-upload1">Document</label>

                                <div class="col-sm-7">
                                    <div class="clearfix">
                                        <input type="file" id="document-path-upload1" name="document-path-upload1" data-bind="file: DocumentPath_a, value: $root.Selected().DocumentPath_a" class="col-xs-7" />
                                    </div>
                                </div>
                                <label class="control-label bolder text-danger">*</label>
                            </div>
                        </div>
                        <!-- modal body form Attach File End -->

                    </div>
                </div>
            </div>
            <!-- modal body end Attach File -->
            <!-- modal footer start Attach File-->
            <div class="modal-footer">
                <button class="btn btn-sm btn-primary" data-bind="click: $root.AddDocument, disable: $root.IsUploading()">
                    <i class="icon-save"></i>
                    Save
                </button>
                <button class="btn btn-sm" data-bind="click: $root.cancel_a">
                    <i class="icon-remove"></i>
                    Cancel
                </button>
            </div>
            <!-- modal footer end attach file -->
        </div>
    </div>
</div>
<!-- modal form end Attach Document Underlying Form end -->

<!--<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.validate.min.js"></script>-->
<%--<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/additional-methods.min.js"></script>--%>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/Knockout/knockout.mapping-latest.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/FundingMemoTL.js"></script>

