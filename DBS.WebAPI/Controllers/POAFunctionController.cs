﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class POAFunctionController : ApiController
    {
        private string message = string.Empty;

        private readonly IPOAFunctionRepository repository = null;

        public POAFunctionController()
        {
            this.repository = new POAFunctionRepository();
        }

        public POAFunctionController(IPOAFunctionRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all currencies.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<POAFunctionModel>))]
        public HttpResponseMessage Get()
        {
            IList<POAFunctionModel> output = new List<POAFunctionModel>();

            if (!repository.GetPOAFunction(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<POAFunctionModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get currencies with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<POAFunctionModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<POAFunctionModel> output = new List<POAFunctionModel>();

            if (!repository.GetPOAFunction(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<POAFunctionModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get currencies for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(POAFunctionGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<POAFunctionFilter> filters)
        {
            POAFunctionGrid output = new POAFunctionGrid();

            if (!repository.GetPOAFunction(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<POAFunctionGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find currencies using criterias Code or Description.
        /// </summary>
        /// <param name="query">Code or Description.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<POAFunctionModel>))]
        [HttpGet]
        [Route("api/POAFunction/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<POAFunctionModel> output = new List<POAFunctionModel>();

            if (!repository.GetPOAFunction(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<POAFunctionModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get POAFunction by ID.
        /// </summary>
        /// <param name="id">POAFunction ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(POAFunctionModel))]
        [Route("api/POAFunction/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            POAFunctionModel output = new POAFunctionModel();

            if (!repository.GetPOAFunctionByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<POAFunctionModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new POA Function.
        /// </summary>
        /// <param name="poaFunction">POA Function data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(POAFunctionModel poaFunction)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddPOAFunction(poaFunction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New POA Function data has been created." });
                }
            }
        }

        /// <summary>
        /// Modify existing POAFunction.
        /// </summary>
        /// <param name="id">ID of POAFunction to be modify.</param>
        /// <param name="poaFunction">POAFunction data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/POAFunction/{id}")]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(int id, POAFunctionModel poaFunction)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdatePOAFunction(id, poaFunction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "POA Function data has been updated." });
                }
            }
        }

        /// <summary>
        /// Remove exisiting Bene Segment.
        /// </summary>
        /// <param name="id">ID of POA Function to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/POAFunction/{id}")]
        [ResponseType(typeof(string))]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeletePOAFunction(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "POA Function data has been deleted." });
                }
            }
        }
        
        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}