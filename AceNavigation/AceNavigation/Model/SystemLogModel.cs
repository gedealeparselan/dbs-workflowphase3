﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AceNavigation.Model
{
    public class SystemLogModel
    {
        public int RowNumber { get; set; }
        public long LogID { get; set; }
        public string UserName{ get; set; }
        public string LogType{ get; set; }
        public string Remark { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
