﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class RateTypeController : ApiController
    {
        private string message = string.Empty;

        private readonly IRateTypeRepository repository = null;

        public RateTypeController()
        {
            this.repository = new RateTypeRepository();
        }

        public RateTypeController(IRateTypeRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all Rate Types.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<RateTypeModel>))]
        public HttpResponseMessage Get()
        {
            IList<RateTypeModel> output = new List<RateTypeModel>();

            if (!repository.GetRateType(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<RateTypeModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Rate Types with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<RateTypeModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<RateTypeModel> output = new List<RateTypeModel>();

            if (!repository.GetRateType(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<RateTypeModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Rate Types for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(RateTypeGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<RateTypeFilter> filters)
        {
            RateTypeGrid output = new RateTypeGrid();

            if (!repository.GetRateType(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<RateTypeGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find Rate Types using Rate Types.
        /// </summary>
        /// <param name="query">Code or Description.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<RateTypeModel>))]
        [HttpGet]
        [Route("api/RateType/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<RateTypeModel> output = new List<RateTypeModel>();

            if (!repository.GetRateType(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<RateTypeModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Rate Type by ID.
        /// </summary>
        /// <param name="id">Rate Type ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(RateTypeModel))]
        [Route("api/RateType/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            RateTypeModel output = new RateTypeModel();

            if (!repository.GetRateTypeByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<RateTypeModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new Rate Type.
        /// </summary>
        /// <param name="RateType">Rate Type data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(RateTypeModel RateType)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddRateType(RateType, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New Rate Type data has been created." });
                }
            }
        }

        /// <summary>
        /// Modify existing Rate Type.
        /// </summary>
        /// <param name="id">ID of RateType to be modify.</param>
        /// <param name="RateType">Rate Type data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/RateType/{id}")]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(int id, RateTypeModel RateType)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateRateType(id, RateType, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Rate Type data has been updated." });
                }
            }
        }

        /// <summary>
        /// Remove exisiting Rate Type.
        /// </summary>
        /// <param name="id">ID of Rate Type to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/RateType/{id}")]
        [Authorize(Roles = "DBS Admin")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteRateType(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "RateType data has been deleted." });
                }
            }
        }
        
        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}