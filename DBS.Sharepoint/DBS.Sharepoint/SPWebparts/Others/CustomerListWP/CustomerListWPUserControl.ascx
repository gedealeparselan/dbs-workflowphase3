﻿<%@ Assembly Name="DBS.Sharepoint, Version=1.0.0.0, Culture=neutral, PublicKeyToken=f75c72caf4809f4b" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomerListWPUserControl.ascx.cs" Inherits="DBS.Sharepoint.SPWebparts.Others.CustomerListWP.CustomerListWPUserControl" %>

<link rel="stylesheet" href="/_catalogs/masterpage/Ace/assets/css/bootstrap-timepicker.css" />

<div id="gridCustomerList" data-bind="visible: $root.IsGridEnable()">
    <h1 class="header smaller lighter dark">Customer List</h1>
    <!-- widget box start -->
    <div id="widget-box" class="widget-box">
        <!-- widget header start -->
        <div class="widget-header widget-hea1der-small header-color-dark">
            <h6>Customer Table</h6>

            <div class="widget-toolbar">
                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                    <i class="blue icon-filter"></i>
                    <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: GridProperties().AllowFilter" />
                    <span class="lbl"></span>
                </label>
                <a href="#" data-bind="click: GetData" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                    <i class="blue icon-refresh"></i>
                </a>
            </div>
        </div>
        <!-- widget header end -->

        <!-- widget body start -->
        <div class="widget-body">
            <!-- widget main start -->
            <div class="widget-main padding-0">
                <!-- widget slim control start -->
                <div class="slim-scroll" data-height="400">
                    <!-- widget content start -->
                    <div class="content">

                        <!-- table responsive start -->
                        <div class="table-responsive">
                            <div class="dataTables_wrapper" role="grid">
                                <table id="Customer List-table" class="table table-striped table-bordered table-hover dataTable">
                                    <thead>
                                        <tr data-bind="with: GridProperties">
                                            <th style="width: 50px">No.</th>
                                            <th data-bind="click: function () { Sorting('CIF'); }, css: GetSortedColumn('CIF')">CIF</th>
                                            <th data-bind="click: function () { Sorting('Name'); }, css: GetSortedColumn('Name')">Name</th>
                                            <!--<th data-bind="click: function () { Sorting('POAName'); }, css: GetSortedColumn('POAName')">POA Name</th>-->
                                            <th data-bind="click: function () { Sorting('Branch'); }, css: GetSortedColumn('Branch')">Branch</th>
                                            <th data-bind="click: function () { Sorting('BizSegment'); }, css: GetSortedColumn('BizSegment')">Biz Segment</th>
                                            <th data-bind="click: function () { Sorting('Type'); }, css: GetSortedColumn('Type')">Type</th>
                                            <th data-bind="click: function () { Sorting('LastModifiedBy'); }, css: GetSortedColumn('LastModifiedBy')">Modified By</th>
                                            <th data-bind="click: function () { Sorting('LastModifiedDate'); }, css: GetSortedColumn('LastModifiedDate')">Modified Date</th>
                                        </tr>
                                    </thead>
                                    <thead data-bind="visible: GridProperties().AllowFilter" class="table-filter">
                                        <tr>
                                            <th class="clear-filter">
                                                <a href="#" data-bind="click: ClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                    <i class="green icon-trash"></i>
                                                </a>
                                            </th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCIF, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterName, event: { change: GridProperties().Filter }" /></th>
                                            <!--<th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterPOAName, event: { change: GridProperties().Filter }" /></th>-->
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterBranch, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterBizSegment, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterType, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterLastModifiedBy, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.FilterLastModifiedDate, event: { change: GridProperties().Filter }" /></th>
                                        </tr>
                                    </thead>
                                    <tbody data-bind="foreach: Customers, visible: Customers().length > 0">
                                        <tr data-bind="click: $root.GetSelectedRow" data-toggle="modal">
                                            <td><span data-bind="text: RowID"></span></td>
                                            <td><span data-bind="text: CIF"></span></td>
                                            <td><span data-bind="text: Name"></span></td>
                                            <!--<td><span data-bind="text: POAName"></span></td>-->
                                            <td><span data-bind="text: Branch"></span></td>
                                            <td><span data-bind="text: BizSegment"></span></td>
                                            <td><span data-bind="text: Type"></span></td>
                                            <td><span data-bind="text: LastModifiedBy"></span></td>
                                            <td><span data-bind="text: $root.LocalDate(LastModifiedDate)"></span></td>
                                        </tr>
                                    </tbody>
                                    <tbody data-bind="visible: Customers().length == 0">
                                        <tr>
                                            <td colspan="9" class="text-center">No entries available.
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- table responsive end -->
                    </div>
                    <!-- widget content end -->
                </div>
                <!-- widget slim control end -->

                <!-- widget footer start -->
                <div class="widget-toolbox padding-8 clearfix">
                    <div class="row" data-bind="with: GridProperties">
                        <!-- pagination size start -->
                        <div class="col-sm-6">
                            <div class="dataTables_paginate paging_bootstrap pull-left">
                                Showing
                                <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                rows of <span data-bind="text: Total"></span>
                                entries
                            </div>
                        </div>
                        <!-- pagination size end -->

                        <!-- pagination page jump start -->
                        <div class="col-sm-3">
                            <div class="dataTables_paginate paging_bootstrap">
                                Page
                                <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                of <span data-bind="text: TotalPages"></span>
                            </div>
                        </div>
                        <!-- pagination page jump end -->

                        <!-- pagination navigation start -->
                        <div class="col-sm-3">
                            <div class="dataTables_paginate paging_bootstrap">
                                <ul class="pagination">
                                    <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                            <i class="icon-double-angle-left"></i>
                                        </a>
                                    </li>
                                    <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                            <i class="icon-angle-left"></i>
                                        </a>
                                    </li>
                                    <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                            <i class="icon-angle-right"></i>
                                        </a>
                                    </li>
                                    <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                            <i class="icon-double-angle-right"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- pagination navigation end -->

                    </div>
                </div>
                <!-- widget footer end -->

            </div>
            <!-- widget main end -->
        </div>
        <!-- widget body end -->
    </div>
    <!-- widget box end -->

    <!-- <h4 class="pink">
        <i class="icon-hand-right green"></i>
        <a href="#modal-form" role="button" class="blue" data-toggle="modal" data-bind="click: NewData">
        New Customer List</a> 
    </h4>-->
</div>
<div class="col-sm-6" style="width: 100%; display: none;" data-bind="visible: !$root.IsGridEnable()">
    <div class="widget-box">
        <div class="widget-header">
            <h4 class="smaller">
                <span data-bind="if: $root.IsNewData()">Create a new customer</span>
                <span data-bind="if: !$root.IsNewData()">Modify a customer</span></h4>
        </div>
        <div class="widget-body">
            <div class="widget-main">
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="customer-form" class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="cif">
                                        CIF</label>

                                    <div class="col-sm-10">
                                        <div class="clearfix">
                                            <input type="text" id="cif" name="cif" data-bind="value: $root.CIF, enable: $root.IsNewData()" class="col-xs-10 col-sm-3" data-rule-required="true" />
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="name">
                                        Name</label>

                                    <div class="col-sm-10">
                                        <div class="clearfix">
                                            <input type="text" id="name" name="name" data-bind="value: $root.Name, enable: $root.IsNewData()" class="col-xs-12" data-rule-required="true" />
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="biz-segment">Biz Segment</label>

                                    <div class="col-sm-10">
                                        <div class="clearfix">
                                            <input type="text" id="biz-segment" name="biz-segment" data-bind="value: $root.BizSegment().Description, enable: $root.IsNewData()" class="col-xs-10 col-sm-5" />
                                        </div>
                                    </div>
                                    <%--<div class="col-sm-10">
                                        <div class="clearfix">
                                            <select type="text" id="biz-segment" name="biz-segment" data-bind="options: BizSegments, optionsText: 'Description', optionsValue: 'ID', optionsCaption: 'Please Select...', value: BizSegment().ID"></select>
                                        </div>
                                    </div>--%>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="branch">Branch</label>

                                    <div class="col-sm-10">
                                        <div class="clearfix">
                                            <input type="text" id="branch" name="branch" data-bind="value: $root.Branch().Name, enable: $root.IsNewData()" class="col-xs-10 col-sm-8" />
                                        </div>
                                    </div>
                                    <%--<div class="col-sm-10">
                                        <div class="clearfix">
                                            <select type="text" id="branch" name="branch" data-bind="options: Branchs, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Please Select...', value: Branch().ID"></select>
                                        </div>
                                    </div>--%>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="customer-type">Customer Type</label>

                                    <div class="col-sm-10">
                                        <div class="clearfix">
                                            <input type="text" id="customer-type" name="customer-type" data-bind="value: $root.Type().Name, enable: $root.IsNewData()" class="col-xs-10 col-sm-3" />
                                        </div>
                                    </div>
                                    <%--<div class="col-sm-10">
                                        <div class="clearfix">
                                            <select type="text" id="customer-type" name="customer-type" data-bind="options: Types, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Please Select...', value: Type().ID"></select>
                                        </div>
                                    </div>--%>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="rm-code">RM Code</label>

                                    <div class="col-sm-10">
                                        <div class="clearfix">
                                            <input type="text" id="rm-code" name="rm-code" data-bind="value: $root.UserApproval().Name(), enable: $root.IsNewData()" class="col-xs-10 col-sm-5" />
                                        </div>
                                    </div>
                                    <%--<div class="col-sm-10">
                                        <div class="clearfix">
                                            <select type="text" id="rm-code" name="rm-code" data-bind="options: UserApprovals, optionsText: 'FullName', optionsValue: 'ID', optionsCaption: 'Please Select...', value: UserApproval().ID"></select>
                                        </div>
                                    </div>--%>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="customer-type">Total Eqv USD</label>
                                    <div class="col-sm-10">
                                        <div class="clearfix">
                                            <input type="text" id="totaleqvusd" name="totaleqvusd" data-bind="value: $root.TotalEqvUsd, enable: $root.IsNewData()" class="col-xs-10 col-sm-3" />
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="check-citizen">Citizen</label>

                                    <div class="col-xs-3">
                                        <div class="clearfix">
                                            <input name="check-citizen" class="ace ace-switch ace-switch-6" type="checkbox" data-bind="checked: $root.IsCitizen(), enable: $root.IsNewData()" />
                                            <span class="lbl"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="check-resident">Resident</label>

                                    <div class="col-xs-3">
                                        <div class="clearfix">
                                            <input name="check-resident" class="ace ace-switch ace-switch-6" type="checkbox" data-bind="checked: $root.IsResident(), enable: $root.IsNewData()" />
                                            <span class="lbl"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="check-dormant">Dormant</label>

                                    <div class="col-xs-3">
                                        <div class="clearfix">
                                            <input name="check-dormant" class="ace ace-switch ace-switch-6" type="checkbox" data-bind="checked: $root.IsDormant(), enable: $root.IsNewData()" />
                                            <span class="lbl"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="check-freeze">Freeze</label>

                                    <div class="col-xs-3">
                                        <div class="clearfix">
                                            <input name="check-freeze" class="ace ace-switch ace-switch-6" type="checkbox" data-bind="checked: $root.IsFreeze(), enable: $root.IsNewData()" />
                                            <span class="lbl"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>
                                <!------------- Start Tab Customer  -->
                                <div class="form-horizontal">
                                    <div class="col-sm-6" style="width: 100%">
                                        <div class="tabbable">
                                            <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="tabCustomer">
                                                <li class="active">
                                                    <a data-toggle="tab" href="#tabaccount">Account</a>
                                                </li>
                                                <li>
                                                    <a data-toggle="tab" href="#tabfunction">Function</a>
                                                </li>

                                                <li>
                                                    <a data-toggle="tab" href="#tabcontact">POA Contact</a>
                                                </li>

                                                <li>
                                                    <a data-toggle="tab" href="#tabunderlying">Underlying</a>
                                                </li>
                                                <li data-bind="visible: $root.IsCustomerCSOViewer">
                                                    <a data-toggle="tab" href="#tabcso">CSO</a>
                                                </li>
                                                <li data-bind="visible: $root.IsCustomerPhoneNumberViewer">
                                                    <a data-toggle="tab" href="#tabcustomercontact">Customer Contact</a>
                                                </li>
                                                <li data-bind="visible: $root.IsCustomerCallbackViewer">
                                                    <a data-toggle="tab" href="#tabcallback">Call Back</a>
                                                </li>
                                                <li data-bind="visible: $root.IsCustomerPOAEmailViewer">
                                                    <a data-toggle="tab" href="#tabpoaemail">POA Email</a>
                                                </li>
                                            </ul>
                                            <div class="tab-content" style="border: 1px solid #c5d0dc">
                                                <div id="tabaccount" class="tab-pane in active">
                                                    <!-- widget box start -->
                                                    <div class="widget-box">
                                                        <!-- widget header start -->
                                                        <div class="widget-header widget-hea1der-small header-color-dark">
                                                            <h6>Customer Account Table</h6>

                                                            <div class="widget-toolbar">
                                                                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                                                    <i class="blue icon-filter"></i>
                                                                    <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: AccountGridProperties().AllowFilter" />
                                                                    <span class="lbl"></span>
                                                                </label>
                                                                <a href="#" data-bind="click: GetAccountData" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                                                    <i class="blue icon-refresh"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <!-- widget header end -->
                                                        <!-- widget body start -->
                                                        <div class="widget-body">
                                                            <!-- widget main start -->
                                                            <div class="widget-main padding-0">
                                                                <!-- widget slim control start -->
                                                                <div class="slim-scroll" data-height="300">
                                                                    <!-- widget content start -->
                                                                    <div class="content">

                                                                        <!-- table responsive start -->
                                                                        <div class="table-responsive">
                                                                            <div class="dataTables_wrapper" role="grid">
                                                                                <table id="Customer-Account-table" class="table table-striped table-bordered table-hover dataTable">
                                                                                    <thead>
                                                                                        <tr data-bind="with: AccountGridProperties">
                                                                                            <th style="width: 50px">No.</th>
                                                                                            <th data-bind="click: function () { Sorting('AccountNumber'); }, css: GetSortedColumn('AccountNumber')">Account Number</th>
                                                                                            <th data-bind="click: function () { Sorting('Currency.Code'); }, css: GetSortedColumn('CurrencyCode')">Currency</th>
                                                                                            <th data-bind="click: function () { Sorting('Currency.Description'); }, css: GetSortedColumn('CurrencyDesc')">Description</th>
                                                                                            <th data-bind="click: function () { Sorting('IsJointAccount'); }, css: GetSortedColumn('IsJointAccount')">Account Type</th>
                                                                                            <th data-bind="click: function () { Sorting('LastModifiedBy'); }, css: GetSortedColumn('LastModifiedBy')">Modified By</th>
                                                                                            <th data-bind="click: function () { Sorting('LastModifiedDate'); }, css: GetSortedColumn('LastModifiedDate')">Modified Date</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <thead data-bind="visible: AccountGridProperties().AllowFilter" class="table-filter">
                                                                                        <tr>
                                                                                            <th class="clear-filter">
                                                                                                <a href="#" data-bind="click: ClearAccountFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                                                    <i class="green icon-trash"></i>
                                                                                                </a>
                                                                                            </th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterAccountNumber_a, event: { change: AccountGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCurrencyCode_a, event: { change: AccountGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCurrencyDesc_a, event: { change: AccountGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterIsJointAccount_a, event: { change: AccountGridProperties().Filter }" /></th>

                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterModifiedBy_a, event: { change: AccountGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.FilterModifiedDate_a, event: { change: AccountGridProperties().Filter }" /></th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody data-bind="foreach: CustomerAccounts, visible: CustomerAccounts().length > 0">
                                                                                        <tr data-bind="click: $root.GetAccountSelectedRow" data-toggle="modal">
                                                                                            <td><span data-bind="text: RowID"></span></td>
                                                                                            <td><span data-bind="text: AccountNumber"></span></td>
                                                                                            <td><span data-bind="text: Currency.Code"></span></td>
                                                                                            <td><span data-bind="text: Currency.Description"></span></td>
                                                                                            <td><span data-bind="text: (IsJointAccount) ? 'Join' : 'Single'"></span></td>
                                                                                            <td><span data-bind="text: LastModifiedBy"></span></td>
                                                                                            <td><span data-bind="text: $root.LocalDate(LastModifiedDate)"></span></td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                    <tbody data-bind="visible: CustomerAccounts().length == 0">
                                                                                        <tr>
                                                                                            <td colspan="7" class="text-center">No entries available.
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                        <!-- table responsive end -->
                                                                    </div>
                                                                    <!-- widget content end -->
                                                                </div>
                                                                <!-- widget slim control end -->

                                                                <!-- widget footer start -->
                                                                <div class="widget-toolbox padding-8 clearfix">
                                                                    <div class="row" data-bind="with: AccountGridProperties">
                                                                        <!-- pagination size start -->
                                                                        <div class="col-sm-6">
                                                                            <div class="dataTables_paginate paging_bootstrap pull-left">
                                                                                Showing
                                                                                <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                                                                rows of <span data-bind="text: Total"></span>
                                                                                entries
                                                                            </div>
                                                                        </div>
                                                                        <!-- pagination size end -->

                                                                        <!-- pagination page jump start -->
                                                                        <div class="col-sm-3">
                                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                                Page
                                                                                <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                                                of <span data-bind="text: TotalPages"></span>
                                                                            </div>
                                                                        </div>
                                                                        <!-- pagination page jump end -->

                                                                        <!-- pagination navigation start -->
                                                                        <div class="col-sm-3">
                                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                                <ul class="pagination">
                                                                                    <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                                            <i class="icon-double-angle-left"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                                            <i class="icon-angle-left"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                                            <i class="icon-angle-right"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                                            <i class="icon-double-angle-right"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <!-- pagination navigation end -->

                                                                    </div>
                                                                </div>
                                                                <!-- widget footer end -->

                                                            </div>
                                                            <!-- widget main end -->
                                                        </div>
                                                        <!-- widget body end -->
                                                    </div>
                                                    <!-- widget box end -->
                                                    <div data-bind="visible: true">
                                                        <!--<div data-bind="visible: !$root.IsNewData()">-->
                                                        <!-- <h4 class="pink">
                                                                <i class="icon-hand-right green"></i>
                                                                <a href="#account-modal-form" role="button" class="blue" data-toggle="modal" data-bind="click: NewAccountData">
                                                                New Customer Account</a>
                                                            </h4> -->
                                                    </div>
                                                </div>

                                                <div id="tabfunction" class="tab-pane">

                                                    <!-- widget box start -->
                                                    <div class="widget-box">
                                                        <!-- widget header start -->
                                                        <div class="widget-header widget-hea1der-small header-color-dark">
                                                            <h6>Customer Function Table</h6>

                                                            <div class="widget-toolbar">
                                                                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                                                    <i class="blue icon-filter"></i>
                                                                    <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: FunctionsGridProperties().AllowFilter" />
                                                                    <span class="lbl"></span>
                                                                </label>
                                                                <a href="#" data-bind="click: GetFunctionData" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                                                    <i class="blue icon-refresh"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <!-- widget header end -->

                                                        <!-- widget body start -->
                                                        <div class="widget-body">
                                                            <!-- widget main start -->
                                                            <div class="widget-main padding-0">
                                                                <!-- widget slim control start -->
                                                                <div class="slim-scroll" data-height="300">
                                                                    <!-- widget content start -->
                                                                    <div class="content">

                                                                        <!-- table responsive start -->
                                                                        <div class="table-responsive">
                                                                            <div class="dataTables_wrapper" role="grid">
                                                                                <table id="Customer Function-table" class="table table-striped table-bordered table-hover dataTable">
                                                                                    <thead>
                                                                                        <tr data-bind="with: FunctionsGridProperties">
                                                                                            <th style="width: 50px">No.</th>
                                                                                            <th data-bind="click: function () { Sorting('POAFunction'); }, css: GetSortedColumn('POAFunction')">Function Name</th>
                                                                                            <th data-bind="click: function () { Sorting('POAFunction'); }, css: GetSortedColumn('POAFunction')">Function Description</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <thead data-bind="visible: FunctionsGridProperties().AllowFilter" class="table-filter">
                                                                                        <tr>
                                                                                            <th class="clear-filter">
                                                                                                <a href="#" data-bind="click: ClearFunctionFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                                                    <i class="green icon-trash"></i>
                                                                                                </a>
                                                                                            </th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterPOAFunctionName, event: { change: FunctionsGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterPOAFunctionDescription, event: { change: FunctionsGridProperties().Filter }" /></th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody data-bind="foreach: CustomerFunctions, visible: CustomerFunctions().length > 0">
                                                                                        <tr data-bind="click: $root.GetFunctionSelectedRow" data-toggle="modal">
                                                                                            <td><span data-bind="text: RowID"></span></td>
                                                                                            <td><span data-bind="text: POAFunction.Name"></span></td>
                                                                                            <td><span data-bind="text: POAFunction.Description"></span></td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                    <tbody data-bind="visible: CustomerFunctions().length == 0">
                                                                                        <tr>
                                                                                            <td colspan="6" class="text-center">No entries available.
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                        <!-- table responsive end -->
                                                                    </div>
                                                                    <!-- widget content end -->
                                                                </div>
                                                                <!-- widget slim control end -->

                                                                <!-- widget footer start -->
                                                                <div class="widget-toolbox padding-8 clearfix">
                                                                    <div class="row" data-bind="with: FunctionsGridProperties">
                                                                        <!-- pagination size start -->
                                                                        <div class="col-sm-6">
                                                                            <div class="dataTables_paginate paging_bootstrap pull-left">
                                                                                Showing
                                                                                <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                                                                rows of <span data-bind="text: Total"></span>
                                                                                entries
                                                                            </div>
                                                                        </div>
                                                                        <!-- pagination size end -->

                                                                        <!-- pagination page jump start -->
                                                                        <div class="col-sm-3">
                                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                                Page
                                                                                <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                                                of <span data-bind="text: TotalPages"></span>
                                                                            </div>
                                                                        </div>
                                                                        <!-- pagination page jump end -->

                                                                        <!-- pagination navigation start -->
                                                                        <div class="col-sm-3">
                                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                                <ul class="pagination">
                                                                                    <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                                            <i class="icon-double-angle-left"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                                            <i class="icon-angle-left"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                                            <i class="icon-angle-right"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                                            <i class="icon-double-angle-right"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <!-- pagination navigation end -->

                                                                    </div>
                                                                </div>
                                                                <!-- widget footer end -->

                                                            </div>
                                                            <!-- widget main end -->
                                                        </div>
                                                        <!-- widget body end -->
                                                    </div>
                                                    <!-- widget box end -->
                                                    <div data-bind="visible: true">
                                                        <!--<div data-bind="visible: !$root.IsNewData()">-->
                                                        <!-- <h4 class="pink">
                                                               <i class="icon-hand-right green"></i>
                                                               <a href="#function-modal-form" role="button" class="blue" data-toggle="modal" data-bind="click: NewFunctionData">
                                                               New Customer Function</a>
                                                           </h4> -->
                                                    </div>
                                                </div>

                                                <div id="tabcontact" class="tab-pane">
                                                    <!-- widget box start -->
                                                    <div class="widget-box">
                                                        <!-- widget header start -->
                                                        <div class="widget-header widget-hea1der-small header-color-dark">
                                                            <h6>Customer Contact Table</h6>

                                                            <div class="widget-toolbar">
                                                                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                                                    <i class="blue icon-filter"></i>
                                                                    <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: ConctactGridProperties().AllowFilter" />
                                                                    <span class="lbl"></span>
                                                                </label>
                                                                <a href="#" data-bind="click: GetDataContact" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                                                    <i class="blue icon-refresh"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <!-- widget header end -->

                                                        <!-- widget body start -->
                                                        <div class="widget-body">
                                                            <!-- widget main start -->
                                                            <div class="widget-main padding-0">
                                                                <!-- widget slim control start -->
                                                                <div class="slim-scroll" data-height="300">
                                                                    <!-- widget content start -->
                                                                    <div class="content">

                                                                        <!-- table responsive start -->
                                                                        <div class="table-responsive">
                                                                            <div class="dataTables_wrapper" role="grid">
                                                                                <table id="Customer Contact-table" class="table table-striped table-bordered table-hover dataTable">
                                                                                    <thead>
                                                                                        <tr data-bind="with: ConctactGridProperties">
                                                                                            <th style="width: 50px">No.</th>
                                                                                            <th data-bind="click: function () { Sorting('NameContact'); }, css: GetSortedColumn('NameContact')">Name</th>
                                                                                            <th data-bind="click: function () { Sorting('PhoneNumber'); }, css: GetSortedColumn('PhoneNumber')">Phone Number</th>
                                                                                            <th data-bind="click: function () { Sorting('DateOfBirth'); }, css: GetSortedColumn('DateOfBirth')">Date Of Birth</th>
                                                                                            <th data-bind="click: function () { Sorting('IDNumber'); }, css: GetSortedColumn('IDNumber')">ID Number</th>
                                                                                            <th data-bind="click: function () { Sorting('POAFunctionVW'); }, css: GetSortedColumn('POAFunctionVW')">POA Function</th>
                                                                                            <th data-bind="click: function () { Sorting('OccupationInID'); }, css: GetSortedColumn('OccupationInID')">OccupationIn ID</th>
                                                                                            <th data-bind="click: function () { Sorting('PlaceOfBirth'); }, css: GetSortedColumn('PlaceOfBirth')">Place of Birth</th>
                                                                                            <th data-bind="click: function () { Sorting('EffectiveDate'); }, css: GetSortedColumn('EffectiveDate')">Effective Date</th>
                                                                                            <th data-bind="click: function () { Sorting('CancellationDate'); }, css: GetSortedColumn('CancellationDate')">Cancellation Date</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <thead data-bind="visible: ConctactGridProperties().AllowFilter" class="table-filter">
                                                                                        <tr>
                                                                                            <th class="clear-filter">
                                                                                                <a href="#" data-bind="click: ClearContactFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                                                    <i class="green icon-trash"></i>
                                                                                                </a>
                                                                                            </th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterName_c, event: { change: ConctactGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterPhoneNumber_c, event: { change: ConctactGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.FilterDateOfBirth_c, event: { change: ConctactGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterIDNumber_c, event: { change: ConctactGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterPOAFunction_c, event: { change: ConctactGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterOccupationInID_c, event: { change: ConctactGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterPlaceOfBirth_c, event: { change: ConctactGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.FilterEffectiveDate_c, event: { change: ConctactGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.FilterCancellationDate_c, event: { change: ConctactGridProperties().Filter }" /></th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody data-bind="foreach: CustomerContacts, visible: CustomerContacts().length > 0">
                                                                                        <tr data-bind="click: $root.GetContactSelectedRow" data-toggle="modal">
                                                                                            <td><span data-bind="text: RowID"></span></td>
                                                                                            <td><span data-bind="text: NameContact"></span></td>
                                                                                            <td><span data-bind="text: PhoneNumber"></span></td>
                                                                                            <td><span data-bind="text: $root.LocalDate(DateOfBirth, true, false)"></span></td>
                                                                                            <td><span data-bind="text: IDNumber"></span></td>
                                                                                            <td><span data-bind="text: POAFunctionVW"></span></td>
                                                                                            <td><span data-bind="text: OccupationInID"></span></td>
                                                                                            <td><span data-bind="text: PlaceOfBirth"></span></td>
                                                                                            <td><span data-bind="text: $root.LocalDate(EffectiveDate, true, false)"></span></td>
                                                                                            <td><span data-bind="text: $root.LocalDate(CancellationDate, true, false)"></span></td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                    <tbody data-bind="visible: CustomerContacts().length == 0">
                                                                                        <tr>
                                                                                            <td colspan="10" class="text-center">No entries available.
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                        <!-- table responsive end -->
                                                                    </div>
                                                                    <!-- widget content end -->
                                                                </div>
                                                                <!-- widget slim control end -->

                                                                <!-- widget footer start -->
                                                                <div class="widget-toolbox padding-8 clearfix">
                                                                    <div class="row" data-bind="with: ConctactGridProperties">
                                                                        <!-- pagination size start -->
                                                                        <div class="col-sm-6">
                                                                            <div class="dataTables_paginate paging_bootstrap pull-left">
                                                                                Showing
                                                                                <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                                                                rows of <span data-bind="text: Total"></span>
                                                                                entries
                                                                            </div>
                                                                        </div>
                                                                        <!-- pagination size end -->

                                                                        <!-- pagination page jump start -->
                                                                        <div class="col-sm-3">
                                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                                Page
                                                                                <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                                                of <span data-bind="text: TotalPages"></span>
                                                                            </div>
                                                                        </div>
                                                                        <!-- pagination page jump end -->

                                                                        <!-- pagination navigation start -->
                                                                        <div class="col-sm-3">
                                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                                <ul class="pagination">
                                                                                    <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                                            <i class="icon-double-angle-left"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                                            <i class="icon-angle-left"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                                            <i class="icon-angle-right"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                                            <i class="icon-double-angle-right"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <!-- pagination navigation end -->

                                                                    </div>
                                                                </div>
                                                                <!-- widget footer end -->

                                                            </div>
                                                            <!-- widget main end -->
                                                        </div>
                                                        <!-- widget body end -->
                                                    </div>
                                                    <!-- widget box end -->
                                                    <div data-bind="visible: !$root.IsNewData()">
                                                        <h4 class="pink">
                                                            <i class="icon-hand-right green"></i>
                                                            <a href="#contact-modal-form" role="button" class="blue" data-toggle="modal" data-bind="click: NewData_c, visible: $root.IsEditCustomerContact()">New Customer Contact</a>
                                                        </h4>
                                                    </div>

                                                </div>
                                                <%--dayat--%>
                                                <div id="tabpoaemail" class="tab-pane" data-bind="visible: $root.IsCustomerPOAEmailViewer">
                                                    <!-- widget box start -->
                                                    <div class="widget-box">
                                                        <!-- widget header start -->
                                                        <div class="widget-header widget-hea1der-small header-color-dark">
                                                            <h6>POA Email Table</h6>
                                                            <div class="widget-toolbar">
                                                                <!--<label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                                                    <i class="blue icon-filter"></i>
                                                                    <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: POAEmailGridProperties().AllowFilter" />
                                                                    <span class="lbl"></span>
                                                                </label>-->
                                                                <a href="#" data-bind="click: $root.GetDataPOAEmail" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                                                    <i class="blue icon-refresh"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <!-- widget header end -->
                                                        <!-- widget body start -->
                                                        <div class="widget-body">
                                                            <!-- widget main start -->
                                                            <div class="widget-main padding-0">
                                                                <!-- widget slim control start -->
                                                                <div class="slim-scroll" data-height="300">
                                                                    <!-- widget content start -->
                                                                    <div class="content">
                                                                        <!-- table responsive start -->
                                                                        <div class="table-responsive">
                                                                            <div class="dataTables_wrapper" role="grid">
                                                                                <table id="Customer-POAE-table" class="table table-striped table-bordered table-hover dataTable">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th style="width: 50px">No.</th>
                                                                                            <th>POA Email</th>
                                                                                            <!--ko foreach: $root.cblPOAEmailProduct-->
                                                                                            <th data-bind="text: $data.ProductName"></th>
                                                                                            <!--/ko-->
                                                                                            <th>Created By</th>
                                                                                            <th>Modified Date</th>
                                                                                            <th>Modified By</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <thead data-bind="visible: POAEmailGridProperties().AllowFilter" class="table-filter">
                                                                                        <tr>
                                                                                            <th class="clear-filter">
                                                                                                <a href="#" data-bind="click: ClearPOAEmailFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                                                    <i class="green icon-trash"></i>
                                                                                                </a>
                                                                                            </th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterPOAEmail_POAE, event: { change: POAEmailGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCreateBy_POAE, event: { change: POAEmailGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.FilterUpdateDate_POAE, event: { change: POAEmailGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterUpdateBy_POAE, event: { change: POAEmailGridProperties().Filter }" /></th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody data-bind="foreach: Object.keys(CustomerPOAEmails())">
                                                                                        <tr data-bind="click: $root.GetPOAEmailSelectedRow" data-toggle="modal">
                                                                                            <td>
                                                                                                <p data-bind="text: $index() + 1"></p>
                                                                                            </td>
                                                                                            <td>
                                                                                                <p data-bind="text: $data"></p>
                                                                                            </td>
                                                                                            <!-- ko foreach:  $root.cblPOAEmailProduct()-->
                                                                                            <td>
                                                                                                <!-- ko if: $root.CustomerPOAEmails()[$parent][$index() + 1] -->
                                                                                                <input type="checkbox" data-bind="checked: true" disabled="disabled" />
                                                                                                <!-- /ko -->
                                                                                                <!-- ko ifnot: $root.CustomerPOAEmails()[$parent][$index() + 1] -->
                                                                                                <input type="checkbox" data-bind="checked: false" disabled="disabled" />
                                                                                                <!-- /ko -->
                                                                                            </td>
                                                                                            <!-- /ko -->
                                                                                            <td>
                                                                                                <p data-bind="text: $root.CustomerPOAEmails()[$data][Object.keys($root.CustomerPOAEmails()[$data])[0]]['CreateBy']"></p>
                                                                                            </td>
                                                                                            <td>
                                                                                                <p data-bind="text: $root.LocalDate($root.CustomerPOAEmails()[$data][Object.keys($root.CustomerPOAEmails()[$data])[0]]['UpdateDate'] || $root.CustomerPOAEmails()[$data][Object.keys($root.CustomerPOAEmails()[$data])[0]]['CreateDate'], true, false)"></p>
                                                                                            </td>
                                                                                            <td>
                                                                                                <p data-bind="text: $root.CustomerPOAEmails()[$data][Object.keys($root.CustomerPOAEmails()[$data])[0]]['UpdateBy']"></p>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                    <!--<tbody>
                                                                                        <tr>
                                                                                            <td colspan="10" class="text-center">No entries available.
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>-->
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                        <!-- table responsive end -->
                                                                    </div>
                                                                    <!-- widget content end -->
                                                                </div>
                                                                <!-- widget slim control end -->

                                                                <!-- widget footer start -->
                                                                <div class="widget-toolbox padding-8 clearfix">
                                                                    <div class="row" data-bind="with: POAEmailGridProperties">
                                                                        <!-- pagination size start -->
                                                                        <div class="col-sm-6">
                                                                            <div class="dataTables_paginate paging_bootstrap pull-left">
                                                                                Showing
                                                                                <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                                                                rows of <span data-bind="text: Total"></span>
                                                                                entries
                                                                            </div>
                                                                        </div>
                                                                        <!-- pagination size end -->

                                                                        <!-- pagination page jump start -->
                                                                        <div class="col-sm-3">
                                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                                Page
                                                                                <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                                                of <span data-bind="text: TotalPages"></span>
                                                                            </div>
                                                                        </div>
                                                                        <!-- pagination page jump end -->

                                                                        <!-- pagination navigation start -->
                                                                        <div class="col-sm-3">
                                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                                <ul class="pagination">
                                                                                    <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                                            <i class="icon-double-angle-left"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                                            <i class="icon-angle-left"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                                            <i class="icon-angle-right"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                                            <i class="icon-double-angle-right"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <!-- pagination navigation end -->

                                                                    </div>
                                                                </div>
                                                                <!-- widget footer end -->

                                                            </div>
                                                            <!-- widget main end -->
                                                        </div>
                                                        <!-- widget body end -->
                                                    </div>
                                                    <!-- widget box end -->
                                                    <div data-bind="visible: $root.IsCustomerPOAEmailMaker">
                                                        <h4 class="pink">
                                                            <i class="icon-hand-right green"></i>
                                                            <a href="#poaEmail-modal-form" role="button" class="blue" data-toggle="modal" data-bind="click: NewData_POAE">New POA Email</a>
                                                        </h4>

                                                    </div>
                                                    <div class="form-group" data-bind="visible: $root.IsCustomerPOAEmailViewer">
                                                        <div class="col-sm-5">
                                                            <h4 class="pink">
                                                                <i class="icon-hand-right green"></i>
                                                                <a href="#poaEmail-modal-form" role="button" class="blue" data-bind="click: $root.GenaratePOAEmail">Generate POA Email</a>
                                                            </h4>
                                                        </div>
                                                        <div class="col-sm-5" data-bind="visible: $root.IsSuccess()">
                                                            <label class="col-sm-6 control-label padding-right" for="uri">File Name : </label>
                                                            <div class="col-sm-1 control-label">
                                                                <div class="clearfix">
                                                                    <a data-bind="attr: { href: $root.InsertedURL, target: '_blank' }, text: $root.FileName"></a>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <%-- start dayat draft --%>
                                                    <div id="widget-box-email" class="widget-box">
                                                        <div class="widget-header widget-hea1der-small header-color-dark">
                                                            <h6>Workflow</h6>
                                                            <div class="widget-toolbar">
                                                                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                                                    <i class="blue icon-filter"></i>
                                                                    <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: CustomerPOAEmailDraftGridProperties().AllowFilter" />
                                                                    <span class="lbl"></span>
                                                                </label>
                                                                <a href="#" data-bind="click: GetDataPOAEmailDraft" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                                                    <i class="blue icon-refresh"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="widget-body">
                                                            <div class="widget-main padding-0">
                                                                <div class="slim-scroll" data-height="400">
                                                                    <div class="content">
                                                                        <div class="table-responsive">
                                                                            <div class="dataTables_wrapper" role="grid">
                                                                                <table id="WorkflowEntity-table-Email" class="table table-striped table-bordered table-hover dataTable">
                                                                                    <thead>
                                                                                        <tr data-bind="with: CustomerPOAEmailDraftGridProperties">
                                                                                            <th style="width: 50px">No.</th>
                                                                                            <th data-bind="click: function () { Sorting('TaskName'); }, css: GetSortedColumn('TaskName')">Name</th>
                                                                                            <th data-bind="click: function () { Sorting('ActionType'); }, css: GetSortedColumn('ActionType')">Action</th>
                                                                                            <th data-bind="click: function () { Sorting('LastModifiedBy'); }, css: GetSortedColumn('LastModifiedBy')">Modified By</th>
                                                                                            <th data-bind="click: function () { Sorting('LastModifiedDate'); }, css: GetSortedColumn('LastModifiedDate')">Modified Date</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <thead data-bind="visible: CustomerPOAEmailDraftGridProperties().AllowFilter" class="table-filter">
                                                                                        <tr>
                                                                                            <th class="clear-filter">
                                                                                                <a href="#" data-bind="click: $root.ClearCustomerPOAEmailFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                                                    <i class="green icon-trash"></i>
                                                                                                </a>
                                                                                            </th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterTaskName_POAE, event: { change: CustomerPOAEmailDraftGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterActionType_POAE, event: { change: CustomerPOAEmailDraftGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterModifiedBy_POAE, event: { change: CustomerPOAEmailDraftGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.FilterModifiedDate_POAE, event: { change: CustomerPOAEmailDraftGridProperties().Filter }" /></th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody data-bind="foreach: POAEmailRows, visible: Rows().length > 0">
                                                                                        <tr data-bind="click: $root.GetSelectedRowCustomerPOAEmailDraft" data-toggle="modal">
                                                                                            <td><span data-bind="text: RowID"></span></td>
                                                                                            <td><span data-bind="text: TaskName"></span></td>
                                                                                            <td><span data-bind="text: ActionType"></span></td>
                                                                                            <td><span data-bind="text: LastModifiedBy"></span></td>
                                                                                            <td><span data-bind="text: $root.LocalDate(LastModifiedDate, true, false)"></span></td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                    <tbody data-bind="visible: Rows().length == 0">
                                                                                        <tr>
                                                                                            <td colspan="6" class="text-center">No entries available.
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="widget-toolbox padding-8 clearfix">
                                                                    <div class="row" data-bind="with: CustomerPOAEmailDraftGridProperties">
                                                                        <div class="col-sm-6">
                                                                            <div class="dataTables_paginate paging_bootstrap pull-left">
                                                                                Showing
                                                                                <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                                                                rows of <span data-bind="text: Total"></span>
                                                                                entries
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                                Page
                                                                                <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                                                of <span data-bind="text: TotalPages"></span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                                <ul class="pagination">
                                                                                    <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                                            <i class="icon-double-angle-left"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                    '
        								                                            <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                                            <i class="icon-angle-left"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                                            <i class="icon-angle-right"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                                            <i class="icon-double-angle-right"></i>'
                                                                                        </a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <%--end dayat draft--%>
                                                </div>
                                                <%--POAEmail--%>

                                                <%--CUstomer Contact--%>
                                                <div id="tabcustomercontact" class="tab-pane" data-bind="visible: $root.IsCustomerPhoneNumberViewer">
                                                    <!-- widget box start -->
                                                    <div class="widget-box">
                                                        <!-- widget header start -->
                                                        <div class="widget-header widget-hea1der-small header-color-dark">
                                                            <h6>Customer Phone Number Table</h6>

                                                            <div class="widget-toolbar">
                                                                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                                                    <i class="blue icon-filter"></i>
                                                                    <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: PhoneNumberGridProperties().AllowFilter" />
                                                                    <span class="lbl"></span>
                                                                </label>
                                                                <a href="#" data-bind="click: GetDataPhoneNumber" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                                                    <i class="blue icon-refresh"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <!-- widget header end -->

                                                        <!-- widget body start -->
                                                        <div class="widget-body">
                                                            <!-- widget main start -->
                                                            <div class="widget-main padding-0">
                                                                <!-- widget slim control start -->
                                                                <div class="slim-scroll" data-height="300">
                                                                    <!-- widget content start -->
                                                                    <div class="content">

                                                                        <!-- table responsive start -->
                                                                        <div class="table-responsive">
                                                                            <div class="dataTables_wrapper" role="grid">
                                                                                <table id="CustomerPhoneNumber-table" class="table table-striped table-bordered table-hover dataTable">
                                                                                    <thead>
                                                                                        <tr data-bind="with: PhoneNumberGridProperties">
                                                                                            <th style="width: 50px">No.</th>
                                                                                            <th data-bind="click: function () { Sorting('ContactType'); }, css: GetSortedColumn('ContactType_p')">Contact Type</th>
                                                                                            <th data-bind="click: function () { Sorting('CountryCode'); }, css: GetSortedColumn('CountryCode_p')">Country Code</th>
                                                                                            <th data-bind="click: function () { Sorting('CityCode'); }, css: GetSortedColumn('CityCode_p')">City Code</th>
                                                                                            <th data-bind="click: function () { Sorting('PhoneNumber'); }, css: GetSortedColumn('PhoneNumber_p')">Phone Number</th>
                                                                                            <th data-bind="click: function () { Sorting('CreateBy'); }, css: GetSortedColumn('CreateBy_p')">Created By</th>
                                                                                            <th data-bind="click: function () { Sorting('UpdateDate'); }, css: GetSortedColumn('UpdateDate_p')">Modified Date</th>
                                                                                            <th data-bind="click: function () { Sorting('UpdateBy'); }, css: GetSortedColumn('UpdateBy_p')">Modified By</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <thead data-bind="visible: PhoneNumberGridProperties().AllowFilter" class="table-filter">
                                                                                        <tr>
                                                                                            <th class="clear-filter">
                                                                                                <a href="#" data-bind="click: ClearPhoneNumberFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                                                    <i class="green icon-trash"></i>
                                                                                                </a>
                                                                                            </th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterContactType, event: { change: PhoneNumberGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCountryCode, event: { change: PhoneNumberGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCityCode, event: { change: PhoneNumberGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterPhoneNumber, event: { change: PhoneNumberGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCreateBy, event: { change: PhoneNumberGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.FilterUpdateDate, event: { change: PhoneNumberGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterUpdateBy, event: { change: PhoneNumberGridProperties().Filter }" /></th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody data-bind="foreach: CustomerPhoneNumbers, visible: CustomerPhoneNumbers().length > 0">
                                                                                        <tr>
                                                                                            <td><span data-bind="text: RowID"></span></td>
                                                                                            <td><span data-bind="text: ContactType"></span></td>
                                                                                            <td><span data-bind="text: CountryCode"></span></td>
                                                                                            <td><span data-bind="text: CityCode"></span></td>
                                                                                            <td><span data-bind="text: PhoneNumber"></span></td>
                                                                                            <td><span data-bind="text: CreateBy"></span></td>
                                                                                            <td><span data-bind="text: $root.LocalDate(UpdateDate, true, false)"></span></td>
                                                                                            <td><span data-bind="text: UpdateBy"></span></td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                    <tbody data-bind="visible: CustomerPhoneNumbers().length == 0">
                                                                                        <tr>
                                                                                            <td colspan="10" class="text-center">No entries available.
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                        <!-- table responsive end -->
                                                                    </div>
                                                                    <!-- widget content end -->
                                                                </div>
                                                                <!-- widget slim control end -->

                                                                <!-- widget footer start -->
                                                                <div class="widget-toolbox padding-8 clearfix">
                                                                    <div class="row" data-bind="with: PhoneNumberGridProperties">
                                                                        <!-- pagination size start -->
                                                                        <div class="col-sm-6">
                                                                            <div class="dataTables_paginate paging_bootstrap pull-left">
                                                                                Showing
                                                                                <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                                                                rows of <span data-bind="text: Total"></span>
                                                                                entries
                                                                            </div>
                                                                        </div>
                                                                        <!-- pagination size end -->

                                                                        <!-- pagination page jump start -->
                                                                        <div class="col-sm-3">
                                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                                Page
                                                                                <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                                                of <span data-bind="text: TotalPages"></span>
                                                                            </div>
                                                                        </div>
                                                                        <!-- pagination page jump end -->

                                                                        <!-- pagination navigation start -->
                                                                        <div class="col-sm-3">
                                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                                <ul class="pagination">
                                                                                    <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                                            <i class="icon-double-angle-left"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                                            <i class="icon-angle-left"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                                            <i class="icon-angle-right"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                                            <i class="icon-double-angle-right"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <!-- pagination navigation end -->

                                                                    </div>
                                                                </div>
                                                                <!-- widget footer end -->

                                                            </div>
                                                            <!-- widget main end -->
                                                        </div>
                                                        <!-- widget body end -->
                                                    </div>
                                                    <!-- widget box end -->

                                                </div>
                                                <%--dayat--%>
                                                <div id="tabunderlying" class="tab-pane">

                                                    <!-- widget box start -->
                                                    <div class="widget-box">
                                                        <!-- widget header start -->
                                                        <div class="widget-header widget-hea1der-small header-color-dark">
                                                            <h6>Customer Underlying Table</h6>

                                                            <div class="widget-toolbar">
                                                                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                                                    <i class="blue icon-filter"></i>
                                                                    <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: UnderlyingGridProperties().AllowFilter" />
                                                                    <span class="lbl"></span>
                                                                </label>
                                                                <a href="#" data-bind="click: GetDataUnderlying" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                                                    <i class="blue icon-refresh"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <!-- widget header end -->

                                                        <!-- widget body start -->
                                                        <div class="widget-body">
                                                            <!-- widget main start -->
                                                            <div class="widget-main padding-0">
                                                                <!-- widget slim control start -->
                                                                <div class="slim-scroll" data-height="300">
                                                                    <!-- widget content start -->
                                                                    <div class="content">

                                                                        <!-- table responsive start -->
                                                                        <div class="table-responsive">
                                                                            <div class="dataTables_wrapper" role="grid">
                                                                                <table id="Customer Underlying-table" class="table table-striped table-bordered table-hover dataTable">
                                                                                    <thead>
                                                                                        <tr data-bind="with: UnderlyingGridProperties">
                                                                                            <th style="width: 50px">No.</th>
                                                                                            <th style="width: 50px">Utilize</th>
                                                                                            <th data-bind="click: function () { Sorting('StatementLetter'); }, css: GetSortedColumn('StatementLetter')">Statement Letter</th>
                                                                                            <th data-bind="click: function () { Sorting('UnderlyingDocument'); }, css: GetSortedColumn('UnderlyingDocument')">Underlying Document</th>
                                                                                            <th data-bind="click: function () { Sorting('DocumentType'); }, css: GetSortedColumn('DocumentType')">Type of Doc</th>
                                                                                            <th data-bind="click: function () { Sorting('Currency'); }, css: GetSortedColumn('Currency')">Currency</th>
                                                                                            <th data-bind="click: function () { Sorting('Amount'); }, css: GetSortedColumn('Amount')">Amount</th>
                                                                                            <th data-bind="click: function () { Sorting('AvailableAmount'); }, css: GetSortedColumn('AvailableAmount')">Available Amount in USD</th>
                                                                                            <th data-bind="click: function () { Sorting('DateOfUnderlying'); }, css: GetSortedColumn('DateOfUnderlying')">Date</th>
                                                                                            <th data-bind="click: function () { Sorting('SupplierName'); }, css: GetSortedColumn('SupplierName')">Supplier Name</th>
                                                                                            <th data-bind="click: function () { Sorting('InvoiceNumber'); }, css: GetSortedColumn('InvoiceNumber')">Invoice Number</th>
                                                                                            <th data-bind="click: function () { Sorting('AccountNumber'); }, css: GetSortedColumn('AccountNumber')">Account Number</th>
                                                                                            <th data-bind="click: function () { Sorting('IsJointAccount'); }, css: GetSortedColumn('IsJointAccount')">Joint Account</th>
                                                                                            <th data-bind="click: function () { Sorting('IsExpiredDate'); }, css: GetSortedColumn('IsExpiredDate')">Is Expired </th>
                                                                                            <th data-bind="click: function () { Sorting('AttachmentNo'); }, css: GetSortedColumn('AttachmentNo')">Attachment No</th>

                                                                                            <!-- <th data-bind="click: function () { Sorting('ReferenceNumber'); }, css: GetSortedColumn('ReferenceNumber')">Reference Number</th> -->
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <thead data-bind="visible: UnderlyingGridProperties().AllowFilter" class="table-filter">
                                                                                        <tr>
                                                                                            <th class="clear-filter">
                                                                                                <a href="#" data-bind="click: UnderlyingClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                                                    <i class="green icon-trash"></i>
                                                                                                </a>
                                                                                            </th>
                                                                                            <th class="clear-filter"></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterStatementLetter, event: { change: UnderlyingGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterUnderlyingDocument, event: { change: UnderlyingGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterDocumentType, event: { change: UnderlyingGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterCurrency, event: { change: UnderlyingGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterAmount, event: { change: UnderlyingGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterAvailableAmount, event: { change: $root.UnderlyingGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.UnderlyingFilterDateOfUnderlying, event: { change: UnderlyingGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterSupplierName, event: { change: UnderlyingGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterInvoiceNumber, event: { change: UnderlyingGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterAccountNumber, event: { change: UnderlyingGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterIsJointAccount, event: { change: UnderlyingGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterIsExpired, event: { change: UnderlyingGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterAttachmentNo, event: { change: UnderlyingGridProperties().Filter }" /></th>
                                                                                            <!--    <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterReferenceNumber, event: { change: UnderlyingGridProperties().Filter }" /></th> -->
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody data-bind="foreach: CustomerUnderlyings, visible: CustomerUnderlyings().length > 0">
                                                                                        <tr data-bind="click: $root.GetUnderlyingSelectedRow" data-toggle="modal" data-target="#modal-form-Underlying">
                                                                                            <td><span data-bind="text: RowID"></span></td>
                                                                                            <td align="center">
                                                                                                <input type="checkbox" id="isUtilize" name="isUtilize" data-bind="checked: IsEnable, event: { change: function (data) { $root.onSelectionUtilize($index(), $data) } }" /></td>
                                                                                            <td><span data-bind="text: StatementLetter.Name"></span></td>
                                                                                            <td><span data-bind="text: UnderlyingDocument.Name"></span></td>
                                                                                            <td><span data-bind="text: DocumentType.Name"></span></td>
                                                                                            <td><span data-bind="text: Currency.Code"></span></td>
                                                                                            <td align="right"><span data-bind="text: formatNumber(Amount)"></span></td>
                                                                                            <td align="right"><span data-bind="text: formatNumber(AvailableAmount)"></span></td>
                                                                                            <td><span data-bind="text: $root.LocalDate(DateOfUnderlying, true, false)"></span></td>
                                                                                            <td><span data-bind="text: SupplierName"></span></td>
                                                                                            <td><span data-bind="text: InvoiceNumber"></span></td>
                                                                                            <td><span data-bind="text: AccountNumber"></span></td>
                                                                                            <td><span data-bind="text: (IsJointAccount) ? 'Join' : 'Single'"></span></td>
                                                                                            <td><span data-bind="text: (IsExpiredDate) ? 'Yes' : 'No' "></span></td>
                                                                                            <td><span data-bind="text: AttachmentNo"></span></td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                    <tbody data-bind="visible: CustomerUnderlyings().length == 0">
                                                                                        <tr>
                                                                                            <td colspan="15" class="text-center">No entries available.
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                        <!-- table responsive end -->
                                                                    </div>
                                                                    <!-- widget content end -->
                                                                </div>
                                                                <!-- widget slim control end -->

                                                                <!-- widget footer start -->
                                                                <div class="widget-toolbox padding-8 clearfix">
                                                                    <div class="row" data-bind="with: UnderlyingGridProperties">
                                                                        <!-- pagination size start -->
                                                                        <div class="col-sm-6">
                                                                            <div class="dataTables_paginate paging_bootstrap pull-left">
                                                                                Showing
                                                                                <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                                                                rows of <span data-bind="text: Total"></span>
                                                                                entries
                                                                            </div>
                                                                        </div>
                                                                        <!-- pagination size end -->

                                                                        <!-- pagination page jump start -->
                                                                        <div class="col-sm-3">
                                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                                Page
                                                                                <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                                                of <span data-bind="text: TotalPages"></span>
                                                                            </div>
                                                                        </div>
                                                                        <!-- pagination page jump end -->

                                                                        <!-- pagination navigation start -->
                                                                        <div class="col-sm-3">
                                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                                <ul class="pagination">
                                                                                    <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                                            <i class="icon-double-angle-left"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                                            <i class="icon-angle-left"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                                            <i class="icon-angle-right"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                                            <i class="icon-double-angle-right"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <!-- pagination navigation end -->

                                                                    </div>
                                                                </div>
                                                                <!-- widget footer end -->

                                                            </div>
                                                            <!-- widget main end -->
                                                        </div>
                                                        <!-- widget body end -->
                                                    </div>
                                                    <!-- widget box end -->

                                                    <div data-bind="visible: !$root.IsNewData()">
                                                        <div data-bind="visible: $root.IsEditCustomerUnderlying()">
                                                            <h4 class="pink">
                                                                <i class="icon-hand-right green"></i>
                                                                <a href="#modal-form-Underlying" role="button" class="blue" data-toggle="modal" data-bind="click: NewDataUnderlying">New Customer Underlying</a>
                                                            </h4>
                                                        </div>
                                                        <div data-bind="visible: false">
                                                            <h4 class="pink">
                                                                <i class="icon-hand-right green"></i>
                                                                <a href="#Underlying-modal-form" role="button" class="blue" data-toggle="modal" data-bind="click: NewDataUnderlyingExcel">New Customer Underlying Excel</a>
                                                            </h4>
                                                        </div>
                                                    </div>
                                                    <!-- widget header start -->
                                                    <div class="widget-header widget-hea1der-small header-color-dark">
                                                        <h6>Attach File Table</h6>

                                                        <div class="widget-toolbar">
                                                            <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                                                <i class="blue icon-filter"></i>
                                                                <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: AttachGridProperties().AllowFilter" />
                                                                <span class="lbl"></span>
                                                            </label>
                                                            <a href="#" data-bind="click: GetDataAttachFile" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                                                <i class="blue icon-refresh"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <!-- widget header end -->
                                                    <div class="widget-body">
                                                        <!-- widget main start -->
                                                        <div class="widget-main padding-0">
                                                            <!-- widget slim control start -->
                                                            <div class="slim-scroll" data-height="300">
                                                                <!-- widget content start -->
                                                                <div class="content">

                                                                    <!-- table responsive start -->
                                                                    <div class="table-responsive">
                                                                        <div class="dataTables_wrapper" role="grid">
                                                                            <table id="Customer Underlying-table" class="table table-striped table-bordered table-hover dataTable">
                                                                                <thead>
                                                                                    <tr data-bind="with: AttachGridProperties">
                                                                                        <th style="width: 50px">No.</th>
                                                                                        <th data-bind="click: function () { Sorting('FileName'); }, css: GetSortedColumn('FileName')">File Name</th>
                                                                                        <th data-bind="click: function () { Sorting('DocumentPurpose'); }, css: GetSortedColumn('DocumentPurpose')">Document Purpose</th>
                                                                                        <th data-bind="click: function () { Sorting('LastModifiedDate'); }, css: GetSortedColumn('LastModifiedDate')">Update</th>
                                                                                        <th data-bind="click: function () { Sorting('DocumentRefNumber'); }, css: GetSortedColumn('DocumentRefNumber')">Document Ref Number</th>
                                                                                        <th data-bind="click: function () { Sorting('AttachmentReference'); }, css: GetSortedColumn('AttachmentReference')">Attachment Reference</th>
                                                                                        <th style="width: 0px; display: none"></th>
                                                                                        <th style="width: 50px">Delete</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <thead data-bind="visible: AttachGridProperties().AllowFilter" class="table-filter">
                                                                                    <tr>
                                                                                        <th class="clear-filter">
                                                                                            <a href="#" data-bind="click: AttachClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                                                <i class="green icon-trash"></i>
                                                                                            </a>
                                                                                        </th>
                                                                                        <th>
                                                                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.AttachFilterFileName, event: { change: AttachGridProperties().Filter }" /></th>
                                                                                        <th>
                                                                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.AttachFilterDocumentPurpose, event: { change: AttachGridProperties().Filter }" /></th>
                                                                                        <th>
                                                                                            <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.AttachFilterLastModifiedDate, event: { change: AttachGridProperties().Filter }" /></th>
                                                                                        <th>
                                                                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.AttachFilterDocumentRefNumber, event: { change: AttachGridProperties().Filter }" /></th>
                                                                                        <th>
                                                                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.AttachFilterAttachmentReference, event: { change: AttachGridProperties().Filter }" /></th>
                                                                                        <th class="clear-filter" style="display: none"></th>
                                                                                        <th class="clear-filter"></th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody data-bind="foreach: CustomerUnderlyingFiles, visible: CustomerUnderlyingFiles().length > 0">
                                                                                    <tr>
                                                                                        <td><span data-bind="text: RowID"></span></td>
                                                                                        <td><a data-bind="attr: { href: DocumentPath }, text: FileName"></a></td>
                                                                                        <td><span data-bind="text: DocumentPurpose.Name"></span></td>
                                                                                        <td><span data-bind="text: $root.LocalDate(LastModifiedDate, false, false)"></span></td>
                                                                                        <td><span data-bind="text: DocumentRefNumber"></span></td>
                                                                                        <td><span data-bind="text: AttachmentReference"></span></td>
                                                                                        <td align="center">
                                                                                            <a href="#" data-bind="click: $root.delete_a, visible: $root.IsEditCustomerUnderlying()" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                                                <i class="green icon-trash"></i>
                                                                                            </a>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                                <tbody data-bind="visible: CustomerUnderlyingFiles().length == 0">
                                                                                    <tr>
                                                                                        <td colspan="10" class="text-center">No entries available.
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <!-- table responsive end -->
                                                                </div>
                                                                <!-- widget content end -->
                                                            </div>
                                                            <!-- widget slim control end -->

                                                            <!-- widget footer start -->
                                                            <div class="widget-toolbox padding-8 clearfix">
                                                                <div class="row" data-bind="with: AttachGridProperties">
                                                                    <!-- pagination size start -->
                                                                    <div class="col-sm-6">
                                                                        <div class="dataTables_paginate paging_bootstrap pull-left">
                                                                            Showing
                                                                            <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                                                            rows of <span data-bind="text: Total"></span>
                                                                            entries
                                                                        </div>
                                                                    </div>
                                                                    <!-- pagination size end -->

                                                                    <!-- pagination page jump start -->
                                                                    <div class="col-sm-3">
                                                                        <div class="dataTables_paginate paging_bootstrap">
                                                                            Page
                                                                            <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                                            of <span data-bind="text: TotalPages"></span>
                                                                        </div>
                                                                    </div>
                                                                    <!-- pagination page jump end -->

                                                                    <!-- pagination navigation start -->
                                                                    <div class="col-sm-3">
                                                                        <div class="dataTables_paginate paging_bootstrap">
                                                                            <ul class="pagination">
                                                                                <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                                        <i class="icon-double-angle-left"></i>
                                                                                    </a>
                                                                                </li>
                                                                                <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                                        <i class="icon-angle-left"></i>
                                                                                    </a>
                                                                                </li>
                                                                                <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                                        <i class="icon-angle-right"></i>
                                                                                    </a>
                                                                                </li>
                                                                                <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                                        <i class="icon-double-angle-right"></i>
                                                                                    </a>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <!-- pagination navigation end -->

                                                                </div>
                                                            </div>
                                                            <!-- widget footer end -->

                                                            <div data-bind="visible: !$root.IsNewData()">
                                                                <h4 class="pink">
                                                                    <i class="icon-hand-right green"></i>
                                                                    <a href="#modal-form-Attach" role="button" class="blue" data-toggle="modal" data-bind="click: NewAttachFile, visible: $root.IsEditCustomerUnderlying()">Attach Document</a>
                                                                </h4>
                                                            </div>
                                                        </div>
                                                        <!-- widget main end -->
                                                    </div>
                                                    <!-- widget body end -->
                                                    <!-- widget box start -->
                                                    <div class="widget-box">
                                                        <!-- widget header start -->
                                                        <div class="widget-header widget-hea1der-small header-color-dark">
                                                            <h6>Workflow</h6>

                                                            <div class="widget-toolbar">
                                                                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                                                    <i class="blue icon-filter"></i>
                                                                    <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: UnderlyingDraftGridProperties().AllowFilter" />
                                                                    <span class="lbl"></span>
                                                                </label>
                                                                <a href="#" data-bind="click: GetDataUnderlyingDraft" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                                                    <i class="blue icon-refresh"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <!-- widget header end -->
                                                    </div>
                                                    <!-- widget box end -->

                                                    <!-- widget body start -->
                                                    <div class="widget-body">
                                                        <!-- widget main start -->
                                                        <div class="widget-main padding-0">
                                                            <!-- widget slim control start -->
                                                            <div class="slim-scroll" data-height="300">
                                                                <!-- widget content start -->
                                                                <div class="content">

                                                                    <!-- table responsive start -->
                                                                    <div class="table-responsive">
                                                                        <div class="dataTables_wrapper" role="grid">
                                                                            <table id="Customer Underlying-table" class="table table-striped table-bordered table-hover dataTable">
                                                                                <thead>
                                                                                    <tr data-bind="with: UnderlyingDraftGridProperties">
                                                                                        <th style="width: 50px">No.</th>
                                                                                        <th data-bind="click: function () { Sorting('StatementLetter'); }, css: GetSortedColumn('StatementLetter')">Statement Letter</th>
                                                                                        <th data-bind="click: function () { Sorting('UnderlyingDocument'); }, css: GetSortedColumn('UnderlyingDocument')">Underlying Document</th>
                                                                                        <th data-bind="click: function () { Sorting('DocumentType'); }, css: GetSortedColumn('DocumentType')">Type of Doc</th>
                                                                                        <th data-bind="click: function () { Sorting('Currency'); }, css: GetSortedColumn('Currency')">Currency</th>
                                                                                        <th data-bind="click: function () { Sorting('Amount'); }, css: GetSortedColumn('Amount')">Amount</th>
                                                                                        <th data-bind="click: function () { Sorting('AvailableAmount'); }, css: GetSortedColumn('AvailableAmount')">Available Amount in USD</th>
                                                                                        <th data-bind="click: function () { Sorting('DateOfUnderlying'); }, css: GetSortedColumn('DateOfUnderlying')">Date</th>
                                                                                        <th data-bind="click: function () { Sorting('SupplierName'); }, css: GetSortedColumn('SupplierName')">Supplier Name</th>
                                                                                        <th data-bind="click: function () { Sorting('InvoiceNumber'); }, css: GetSortedColumn('InvoiceNumber')">Invoice Number</th>
                                                                                        <th data-bind="click: function () { Sorting('IsJointAccount'); }, css: GetSortedColumn('IsJointAccount')">Joint Account</th>
                                                                                        <th data-bind="click: function () { Sorting('AttachmentNo'); }, css: GetSortedColumn('AttachmentNo')">Attachment No</th>
                                                                                        <!-- <th data-bind="click: function () { Sorting('ReferenceNumber'); }, css: GetSortedColumn('ReferenceNumber')">Reference Number</th> -->
                                                                                    </tr>
                                                                                </thead>
                                                                                <thead data-bind="visible: UnderlyingDraftGridProperties().AllowFilter" class="table-filter">
                                                                                    <tr>
                                                                                        <th class="clear-filter">
                                                                                            <a href="#" data-bind="click: UnderlyingDraftClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                                                <i class="green icon-trash"></i>
                                                                                            </a>
                                                                                        </th>
                                                                                        <th>
                                                                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingDraftFilterStatementLetter, event: { change: UnderlyingDraftGridProperties().Filter }" /></th>
                                                                                        <th>
                                                                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingDraftFilterUnderlyingDocument, event: { change: UnderlyingDraftGridProperties().Filter }" /></th>
                                                                                        <th>
                                                                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingDraftFilterDocumentType, event: { change: UnderlyingDraftGridProperties().Filter }" /></th>
                                                                                        <th>
                                                                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingDraftFilterCurrency, event: { change: UnderlyingDraftGridProperties().Filter }" /></th>
                                                                                        <th>
                                                                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingDraftFilterAmount, event: { change: UnderlyingDraftGridProperties().Filter }" /></th>
                                                                                        <th>
                                                                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingDraftFilterAvailableAmount, event: { change: $root.UnderlyingDraftGridProperties().Filter }" /></th>
                                                                                        <th>
                                                                                            <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.UnderlyingDraftFilterDateOfUnderlying, event: { change: UnderlyingDraftGridProperties().Filter }" /></th>
                                                                                        <th>
                                                                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingDraftFilterSupplierName, event: { change: UnderlyingDraftGridProperties().Filter }" /></th>
                                                                                        <th>
                                                                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingDraftFilterInvoiceNumber, event: { change: UnderlyingDraftGridProperties().Filter }" /></th>
                                                                                        <th>
                                                                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingDraftFilterIsJointAccount, event: { change: UnderlyingDraftGridProperties().Filter }" /></th>
                                                                                        <th>
                                                                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingDraftFilterAttachmentNo, event: { change: UnderlyingDraftGridProperties().Filter }" /></th>
                                                                                        <!--    <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingFilterReferenceNumber, event: { change: UnderlyingGridProperties().Filter }" /></th> -->
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody data-bind="foreach: CustomerUnderlyingDrafts, visible: CustomerUnderlyingDrafts().length > 0">
                                                                                    <tr data-bind="click: $root.GetUnderlyingSelectedViewRow" data-toggle="modal" data-target="#modal-form-Underlying">
                                                                                        <td><span data-bind="text: RowID"></span></td>
                                                                                        <td><span data-bind="text: StatementLetter.Name"></span></td>
                                                                                        <td><span data-bind="text: UnderlyingDocument.Name"></span></td>
                                                                                        <td><span data-bind="text: DocumentType.Name"></span></td>
                                                                                        <td><span data-bind="text: Currency.Code"></span></td>
                                                                                        <td align="right"><span data-bind="text: formatNumber(Amount)"></span></td>
                                                                                        <td align="right"><span data-bind="text: formatNumber(AvailableAmount)"></span></td>
                                                                                        <td><span data-bind="text: $root.LocalDate(DateOfUnderlying, true, false)"></span></td>
                                                                                        <td><span data-bind="text: SupplierName"></span></td>
                                                                                        <td><span data-bind="text: InvoiceNumber"></span></td>
                                                                                        <td><span data-bind="text: (IsJointAccount) ? 'Join' : 'Single'"></span></td>
                                                                                        <td><span data-bind="text: AttachmentNo"></span></td>
                                                                                    </tr>
                                                                                </tbody>
                                                                                <tbody data-bind="visible: CustomerUnderlyingDrafts().length == 0">
                                                                                    <tr>
                                                                                        <td colspan="13" class="text-center">No entries available.
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <!-- table responsive end -->
                                                                </div>
                                                                <!-- widget content end -->
                                                            </div>
                                                            <!-- widget slim control end -->
                                                        </div>
                                                        <!-- widget main end -->


                                                        <!-- widget footer start -->
                                                        <div class="widget-toolbox padding-8 clearfix">
                                                            <div class="row" data-bind="with: UnderlyingDraftGridProperties">
                                                                <!-- pagination size start -->
                                                                <div class="col-sm-6">
                                                                    <div class="dataTables_paginate paging_bootstrap pull-left">
                                                                        Showing
                                                                                        <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                                                        rows of <span data-bind="text: Total"></span>
                                                                        entries
                                                                    </div>
                                                                </div>
                                                                <!-- pagination size end -->

                                                                <!-- pagination page jump start -->
                                                                <div class="col-sm-3">
                                                                    <div class="dataTables_paginate paging_bootstrap">
                                                                        Page
                                                                                        <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                                        of <span data-bind="text: TotalPages"></span>
                                                                    </div>
                                                                </div>
                                                                <!-- pagination page jump end -->

                                                                <!-- pagination navigation start -->
                                                                <div class="col-sm-3">
                                                                    <div class="dataTables_paginate paging_bootstrap">
                                                                        <ul class="pagination">
                                                                            <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                                    <i class="icon-double-angle-left"></i>
                                                                                </a>
                                                                            </li>
                                                                            <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                                    <i class="icon-angle-left"></i>
                                                                                </a>
                                                                            </li>
                                                                            <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                                    <i class="icon-angle-right"></i>
                                                                                </a>
                                                                            </li>
                                                                            <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                                    <i class="icon-double-angle-right"></i>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <!-- pagination navigation end -->

                                                            </div>
                                                        </div>
                                                        <!-- widget footer end -->
                                                    </div>
                                                    <!-- widget body end -->
                                                </div>
                                                <!-- tab cso start-->
                                                <div id="tabcso" class="tab-pane" data-bind="visible: $root.IsCustomerCSOViewer">
                                                    <!-- widget box start -->
                                                    <div class="widget-box">
                                                        <!-- widget header start -->
                                                        <div class="widget-header widget-hea1der-small header-color-dark">
                                                            <h6>CSO Table</h6>

                                                            <div class="widget-toolbar">
                                                                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                                                    <i class="blue icon-filter"></i>
                                                                    <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: CSOGridProperties().AllowFilter" />
                                                                    <span class="lbl"></span>
                                                                </label>
                                                                <a href="#" data-bind="click: GetDataCSO" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                                                    <i class="blue icon-refresh"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <!-- widget header end -->
                                                        <!-- widget body start -->
                                                        <div class="widget-body">
                                                            <!-- widget main start -->
                                                            <div class="widget-main padding-0">
                                                                <!-- widget slim control start -->
                                                                <div class="slim-scroll" data-height="300">
                                                                    <!-- widget content start -->
                                                                    <div class="content">

                                                                        <!-- table responsive start -->
                                                                        <div class="table-responsive">
                                                                            <div class="dataTables_wrapper" role="grid">
                                                                                <table id="Customer-CSO-table" class="table table-striped table-bordered table-hover dataTable">
                                                                                    <thead>
                                                                                        <tr data-bind="with: CSOGridProperties">
                                                                                            <th style="width: 50px">No.</th>
                                                                                            <th data-bind="click: function () { Sorting('Employee.EmployeeName'); }, css: GetSortedColumn('EmployeeName')">Employee Name</th>
                                                                                            <th data-bind="click: function () { Sorting('Employee.EmployeeEmail'); }, css: GetSortedColumn('EmployeeEmail')">Employee Email</th>
                                                                                            <th data-bind="click: function () { Sorting('Employee.Branch.Name'); }, css: GetSortedColumn('LocationName')">Branch</th>
                                                                                            <th data-bind="click: function () { Sorting('LastModifiedBy'); }, css: GetSortedColumn('LastModifiedBy')">Modified By</th>
                                                                                            <th data-bind="click: function () { Sorting('LastModifiedDate'); }, css: GetSortedColumn('LastModifiedDate')">Modified Date</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <thead data-bind="visible: CSOGridProperties().AllowFilter" class="table-filter">
                                                                                        <tr>
                                                                                            <th class="clear-filter">
                                                                                                <a href="#" data-bind="click: ClearCSOFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                                                    <i class="green icon-trash"></i>
                                                                                                </a>
                                                                                            </th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterEmployeeName_c, event: { change: CSOGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterEmployeeEmail_c, event: { change: CSOGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterLocationName_c, event: { change: CSOGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterModifiedBy_c, event: { change: CSOGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.FilterModifiedDate_c, event: { change: CSOGridProperties().Filter }" /></th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody data-bind="foreach: CustomerCSO, visible: CustomerCSO().length > 0">
                                                                                        <tr data-bind="click: $root.GetCSOSelectedRow" data-toggle="modal">
                                                                                            <td><span data-bind="text: RowID"></span></td>
                                                                                            <td><span data-bind="text: Employee.EmployeeName"></span></td>
                                                                                            <td><span data-bind="text: Employee.EmployeeEmail"></span></td>
                                                                                            <td><span data-bind="text: Employee.Branch.Name"></span></td>
                                                                                            <td><span data-bind="text: LastModifiedBy"></span></td>
                                                                                            <td><span data-bind="text: $root.LocalDate(LastModifiedDate)"></span></td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                    <tbody data-bind="visible: CustomerCSO().length == 0">
                                                                                        <tr>
                                                                                            <td colspan="6" class="text-center">No entries available.
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                        <!-- table responsive end -->
                                                                    </div>
                                                                    <!-- widget content end -->
                                                                </div>
                                                                <!-- widget slim control end -->

                                                                <!-- widget footer start -->
                                                                <div class="widget-toolbox padding-8 clearfix">
                                                                    <div class="row" data-bind="with: CSOGridProperties">
                                                                        <!-- pagination size start -->
                                                                        <div class="col-sm-6">
                                                                            <div class="dataTables_paginate paging_bootstrap pull-left">
                                                                                Showing
                                                                                <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                                                                rows of <span data-bind="text: Total"></span>
                                                                                entries
                                                                            </div>
                                                                        </div>
                                                                        <!-- pagination size end -->

                                                                        <!-- pagination page jump start -->
                                                                        <div class="col-sm-3">
                                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                                Page
                                                                                <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                                                of <span data-bind="text: TotalPages"></span>
                                                                            </div>
                                                                        </div>
                                                                        <!-- pagination page jump end -->

                                                                        <!-- pagination navigation start -->
                                                                        <div class="col-sm-3">
                                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                                <ul class="pagination">
                                                                                    <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                                            <i class="icon-double-angle-left"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                                            <i class="icon-angle-left"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                                            <i class="icon-angle-right"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                                            <i class="icon-double-angle-right"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <!-- pagination navigation end -->

                                                                    </div>
                                                                </div>
                                                                <!-- widget footer end -->

                                                            </div>
                                                            <!-- widget main end -->
                                                        </div>
                                                        <!-- widget body end -->
                                                    </div>
                                                    <!-- widget box end -->

                                                    <div data-bind="visible: $root.IsCustomerCSOMaker">
                                                        <h4 class="pink">
                                                            <i class="icon-hand-right green"></i>
                                                            <a href="#CSO-modal-form" role="button" class="blue" data-toggle="modal" data-bind="click: NewCSOData">New CSO</a>
                                                        </h4>
                                                    </div>

                                                    <%-- start azam --%>
                                                    <div id="widget-box" class="widget-box">
                                                        <div class="widget-header widget-hea1der-small header-color-dark">
                                                            <h6>Workflow</h6>
                                                            <div class="widget-toolbar">
                                                                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                                                    <i class="blue icon-filter"></i>
                                                                    <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: CustomerCsoDraftGridProperties().AllowFilter" />
                                                                    <span class="lbl"></span>
                                                                </label>
                                                                <a href="#" data-bind="click: GetDataCsoDraft" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                                                    <i class="blue icon-refresh"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="widget-body">
                                                            <div class="widget-main padding-0">
                                                                <div class="slim-scroll" data-height="400">
                                                                    <div class="content">
                                                                        <div class="table-responsive">
                                                                            <div class="dataTables_wrapper" role="grid">
                                                                                <table id="WorkflowEntity-table" class="table table-striped table-bordered table-hover dataTable">
                                                                                    <thead>
                                                                                        <tr data-bind="with: CustomerCsoDraftGridProperties">
                                                                                            <th style="width: 50px">No.</th>
                                                                                            <th data-bind="click: function () { Sorting('TaskName'); }, css: GetSortedColumn('TaskName')">Name</th>
                                                                                            <th data-bind="click: function () { Sorting('ActionType'); }, css: GetSortedColumn('ActionType')">Action</th>
                                                                                            <th data-bind="click: function () { Sorting('LastModifiedBy'); }, css: GetSortedColumn('LastModifiedBy')">Modified By</th>
                                                                                            <th data-bind="click: function () { Sorting('LastModifiedDate'); }, css: GetSortedColumn('LastModifiedDate')">Modified Date</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <thead data-bind="visible: CustomerCsoDraftGridProperties().AllowFilter" class="table-filter">
                                                                                        <tr>
                                                                                            <th class="clear-filter">
                                                                                                <a href="#" data-bind="click: ClearCustomerCsoFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                                                    <i class="green icon-trash"></i>
                                                                                                </a>
                                                                                            </th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.TaskName, event: { change: CustomerCsoDraftGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.ActionType, event: { change: CustomerCsoDraftGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterModifiedBy, event: { change: CustomerCsoDraftGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.FilterModifiedDate, event: { change: CustomerCsoDraftGridProperties().Filter }" /></th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody data-bind="foreach: Rows, visible: Rows().length > 0">
                                                                                        <tr data-bind="click: $root.GetSelectedRowCustomerDraft" data-toggle="modal">
                                                                                            <td><span data-bind="text: RowID"></span></td>
                                                                                            <td><span data-bind="text: TaskName"></span></td>
                                                                                            <td><span data-bind="text: ActionType"></span></td>
                                                                                            <td><span data-bind="text: LastModifiedBy"></span></td>
                                                                                            <td><span data-bind="text: $root.LocalDate(LastModifiedDate)"></span></td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                    <tbody data-bind="visible: Rows().length == 0">
                                                                                        <tr>
                                                                                            <td colspan="6" class="text-center">No entries available.
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="widget-toolbox padding-8 clearfix">
                                                                    <div class="row" data-bind="with: CustomerCsoDraftGridProperties">
                                                                        <div class="col-sm-6">
                                                                            <div class="dataTables_paginate paging_bootstrap pull-left">
                                                                                Showing
                                                                                <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                                                                rows of <span data-bind="text: Total"></span>
                                                                                entries
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                                Page
                                                                                <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                                                of <span data-bind="text: TotalPages"></span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                                <ul class="pagination">
                                                                                    <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                                            <i class="icon-double-angle-left"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                    '
        								                                            <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                                            <i class="icon-angle-left"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                                            <i class="icon-angle-right"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                                            <i class="icon-double-angle-right"></i>'
                                                                                        </a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <%--end azam--%>
                                                </div>
                                                <!--end tab cso-->
                                                <div id="tabcallback" class="tab-pane" data-bind="visible: $root.IsCustomerCallbackViewer">
                                                    <div class="widget-box" data-bind="visible: $root.IsCustomerCallbackMaker">
                                                        <div class="widget-header widget-hea1der-small header-color-dark">
                                                            <h6>Customer Contact Table</h6>

                                                            <div class="widget-toolbar">
                                                                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                                                    <i class="blue icon-filter"></i>
                                                                    <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: CallbackContactGridProperties().AllowFilter" />
                                                                    <span class="lbl"></span>
                                                                </label>
                                                                <a href="#" data-bind="click: GetDataCallbackContact" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                                                    <i class="blue icon-refresh"></i>
                                                                </a>
                                                            </div>
                                                        </div>

                                                        <div class="widget-body">
                                                            <!-- widget main start -->
                                                            <div class="widget-main padding-0">
                                                                <!-- widget slim control start -->
                                                                <div class="slim-scroll" data-height="300">
                                                                    <!-- widget content start -->
                                                                    <div class="content">

                                                                        <!-- table responsive start -->
                                                                        <div class="table-responsive">
                                                                            <div class="dataTables_wrapper" role="grid">
                                                                                <table id="Customer-Callback-Contact-table" class="table table-striped table-bordered table-hover dataTable">
                                                                                    <thead>
                                                                                        <tr data-bind="with: CallbackContactGridProperties">
                                                                                            <th style="width: 50px">No.</th>
                                                                                            <th data-bind="click: function () { Sorting('NameContact'); }, css: GetSortedColumn('NameContact')">Name</th>
                                                                                            <th data-bind="click: function () { Sorting('PhoneNumber'); }, css: GetSortedColumn('PhoneNumber')">Phone Number</th>
                                                                                            <th data-bind="click: function () { Sorting('DateOfBirth'); }, css: GetSortedColumn('DateOfBirth')">Date Of Birth</th>
                                                                                            <th data-bind="click: function () { Sorting('IDNumber'); }, css: GetSortedColumn('IDNumber')">ID Number</th>
                                                                                            <th data-bind="click: function () { Sorting('POAFunction.Description'); }, css: GetSortedColumn('POAFunction.Description')">POA Function</th>
                                                                                            <th data-bind="click: function () { Sorting('OccupationInID'); }, css: GetSortedColumn('OccupationInID')">OccupationIn ID</th>
                                                                                            <th data-bind="click: function () { Sorting('PlaceOfBirth'); }, css: GetSortedColumn('PlaceOfBirth')">Place of Birth</th>
                                                                                            <th data-bind="click: function () { Sorting('EffectiveDate'); }, css: GetSortedColumn('EffectiveDate')">Effective Date</th>
                                                                                            <th data-bind="click: function () { Sorting('CancellationDate'); }, css: GetSortedColumn('CancellationDate')">Cancellation Date</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <thead data-bind="visible: CallbackContactGridProperties().AllowFilter" class="table-filter">
                                                                                        <tr>
                                                                                            <th class="clear-filter">
                                                                                                <a href="#" data-bind="click: ClearCallbackContactFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                                                    <i class="green icon-trash"></i>
                                                                                                </a>
                                                                                            </th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCallbackContactName, event: { change: CallbackContactGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCallbackContactPhoneNumber, event: { change: CallbackContactGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.FilterCallbackContactDateOfBirth, event: { change: CallbackContactGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCallbackContactIDNumber, event: { change: CallbackContactGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCallbackContactPOAFunction, event: { change: CallbackContactGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCallbackContactOccupationInID, event: { change: CallbackContactGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCallbackContactPlaceOfBirth, event: { change: CallbackContactGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.FilterCallbackContactEffectiveDate, event: { change: CallbackContactGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.FilterCallbackContactCancellationDate, event: { change: CallbackContactGridProperties().Filter }" /></th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody data-bind="foreach: CustomerCallbackContacts, visible: CustomerCallbackContacts().length > 0">
                                                                                        <tr data-bind="click: $root.GetCallbackContactSelectedRow" data-toggle="modal">
                                                                                            <td><span data-bind="text: RowID"></span></td>
                                                                                            <td><span data-bind="text: NameContact"></span></td>
                                                                                            <td><span data-bind="text: PhoneNumber"></span></td>
                                                                                            <td><span data-bind="text: $root.LocalDate(DateOfBirth, true, false)"></span></td>
                                                                                            <td><span data-bind="text: IDNumber"></span></td>
                                                                                            <td><span data-bind="text: POAFunction.Description"></span></td>
                                                                                            <td><span data-bind="text: OccupationInID"></span></td>
                                                                                            <td><span data-bind="text: PlaceOfBirth"></span></td>
                                                                                            <td><span data-bind="text: $root.LocalDate(EffectiveDate, true, false)"></span></td>
                                                                                            <td><span data-bind="text: $root.LocalDate(CancellationDate, true, false)"></span></td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                    <tbody data-bind="visible: CustomerCallbackContacts().length == 0">
                                                                                        <tr>
                                                                                            <td colspan="10" class="text-center">No entries available.
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                        <!-- table responsive end -->
                                                                    </div>
                                                                    <!-- widget content end -->
                                                                </div>
                                                                <!-- widget slim control end -->

                                                                <!-- widget footer start -->
                                                                <div class="widget-toolbox padding-8 clearfix">
                                                                    <div class="row" data-bind="with: CallbackContactGridProperties">
                                                                        <!-- pagination size start -->
                                                                        <div class="col-sm-6">
                                                                            <div class="dataTables_paginate paging_bootstrap pull-left">
                                                                                Showing
                                                                                <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                                                                rows of <span data-bind="text: Total"></span>
                                                                                entries
                                                                            </div>
                                                                        </div>
                                                                        <!-- pagination size end -->

                                                                        <!-- pagination page jump start -->
                                                                        <div class="col-sm-3">
                                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                                Page
                                                                                <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                                                of <span data-bind="text: TotalPages"></span>
                                                                            </div>
                                                                        </div>
                                                                        <!-- pagination page jump end -->

                                                                        <!-- pagination navigation start -->
                                                                        <div class="col-sm-3">
                                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                                <ul class="pagination">
                                                                                    <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                                            <i class="icon-double-angle-left"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                                            <i class="icon-angle-left"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                                            <i class="icon-angle-right"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                                            <i class="icon-double-angle-right"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <!-- pagination navigation end -->

                                                                    </div>
                                                                </div>
                                                                <!-- widget footer end -->

                                                            </div>
                                                            <!-- widget main end -->
                                                        </div>
                                                        
                                                        <div class="space-16"></div>
                                                    </div>

                                                    <div class="widget-box">
                                                        <div class="widget-header widget-hea1der-small header-color-dark">
                                                            <h6>Call Back Table</h6>

                                                            <div class="widget-toolbar">
                                                                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                                                    <i class="blue icon-filter"></i>
                                                                    <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: CallbackGridProperties().AllowFilter" />
                                                                    <span class="lbl"></span>
                                                                </label>
                                                                <a href="#" data-bind="click: GetDataCallback" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                                                    <i class="blue icon-refresh"></i>
                                                                </a>
                                                            </div>
                                                        </div>

                                                        <div class="widget-body">
                                                            <!-- widget main start -->
                                                            <div class="widget-main padding-0">
                                                                <!-- widget slim control start -->
                                                                <div class="slim-scroll" data-height="300">
                                                                    <!-- widget content start -->
                                                                    <div class="content">

                                                                        <!-- table responsive start -->
                                                                        <div class="table-responsive">
                                                                            <div class="dataTables_wrapper" role="grid">
                                                                                <table id="Customer-Callback-table" class="table table-striped table-bordered table-hover dataTable">
                                                                                    <thead>
                                                                                        <tr data-bind="with: CallbackGridProperties">
                                                                                            <th style="width: 50px">No.</th>
                                                                                            <th data-bind="click: function () { Sorting('ContactName'); }, css: GetSortedColumn('ContactName')">Contacted Name</th>
                                                                                            <th data-bind="click: function () { Sorting('ProductName'); }, css: GetSortedColumn('ProductName')">Product</th>
                                                                                            <th data-bind="click: function () { Sorting('Time'); }, css: GetSortedColumn('Time')">Call Back Time</th>
                                                                                            <th data-bind="click: function () { Sorting('IsUTC'); }, css: GetSortedColumn('IsUTC')">IsUTC</th>
                                                                                            <th data-bind="click: function () { Sorting('Remark'); }, css: GetSortedColumn('Remark')">Remark</th>
                                                                                            <th data-bind="click: function () { Sorting('LastModifiedBy'); }, css: GetSortedColumn('LastModifiedBy')">Modified By</th>
                                                                                            <th data-bind="click: function () { Sorting('LastModifiedDate'); }, css: GetSortedColumn('LastModifiedDate')">Modified Date</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <thead data-bind="visible: CallbackGridProperties().AllowFilter" class="table-filter">
                                                                                        <tr>
                                                                                            <th class="clear-filter">
                                                                                                <a href="#" data-bind="click: ClearCallbackFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                                                    <i class="green icon-trash"></i>
                                                                                                </a>
                                                                                            </th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCallbackNameContact, event: { change: CallbackGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCallbackProductName, event: { change: CallbackGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.FilterCallbackTime, event: { change: CallbackGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCallbackIsUTC, event: { change: CallbackGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCallbackRemark, event: { change: CallbackGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCallbackModifiedBy, event: { change: CallbackGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.FilterCallbackModifiedDate, event: { change: CallbackGridProperties().Filter }" /></th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody data-bind="foreach: CustomerCallbacks, visible: CustomerCallbacks().length > 0">
                                                                                        <tr>
                                                                                            <td><span data-bind="text: RowID"></span></td>
                                                                                            <td><span data-bind="text: ContactName"></span></td>
                                                                                            <td><span data-bind="text: ProductName"></span></td>
                                                                                            <td><span data-bind="text: $root.LocalDate(Time)"></span></td>
                                                                                            <td><span data-bind="text: IsUTCDescription"></span></td>
                                                                                            <td><span data-bind="text: Remark"></span></td>
                                                                                            <td><span data-bind="text: LastModifiedBy"></span></td>
                                                                                            <td><span data-bind="text: $root.LocalDate(LastModifiedDate)"></span></td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                    <tbody data-bind="visible: CustomerCallbacks().length == 0">
                                                                                        <tr>
                                                                                            <td colspan="8" class="text-center">No entries available.
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                        <!-- table responsive end -->
                                                                    </div>
                                                                    <!-- widget content end -->
                                                                </div>
                                                                <!-- widget slim control end -->

                                                                <!-- widget footer start -->
                                                                <div class="widget-toolbox padding-8 clearfix">
                                                                    <div class="row" data-bind="with: CallbackGridProperties">
                                                                        <!-- pagination size start -->
                                                                        <div class="col-sm-6">
                                                                            <div class="dataTables_paginate paging_bootstrap pull-left">
                                                                                Showing
                                                                                <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                                                                rows of <span data-bind="text: Total"></span>
                                                                                entries
                                                                            </div>
                                                                        </div>
                                                                        <!-- pagination size end -->

                                                                        <!-- pagination page jump start -->
                                                                        <div class="col-sm-3">
                                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                                Page
                                                                                <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                                                of <span data-bind="text: TotalPages"></span>
                                                                            </div>
                                                                        </div>
                                                                        <!-- pagination page jump end -->

                                                                        <!-- pagination navigation start -->
                                                                        <div class="col-sm-3">
                                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                                <ul class="pagination">
                                                                                    <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                                            <i class="icon-double-angle-left"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                                            <i class="icon-angle-left"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                                            <i class="icon-angle-right"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                                            <i class="icon-double-angle-right"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <!-- pagination navigation end -->

                                                                    </div>
                                                                </div>
                                                                <!-- widget footer end -->

                                                            </div>
                                                            <!-- widget main end -->
                                                        </div>
                                                    </div>

                                                    <div class="space-16"></div>

                                                    <div class="widget-box">
                                                        <div class="widget-header widget-hea1der-small header-color-dark">
                                                            <h6>Workflow</h6>

                                                            <div class="widget-toolbar">
                                                                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                                                    <i class="blue icon-filter"></i>
                                                                    <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: CallbackDraftGridProperties().AllowFilter" />
                                                                    <span class="lbl"></span>
                                                                </label>
                                                                <a href="#" data-bind="click: GetDataCallbackDraft" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                                                    <i class="blue icon-refresh"></i>
                                                                </a>
                                                            </div>
                                                        </div>

                                                        <div class="widget-body">
                                                            <div class="widget-main padding-0">
                                                                <div class="slim-scroll" data-height="400">
                                                                    <div class="content">
                                                                        <div class="table-responsive">
                                                                            <div class="dataTables_wrapper" role="grid">
                                                                                <table id="Customer-Callback-Draft-table" class="table table-striped table-bordered table-hover dataTable">
                                                                                    <thead>
                                                                                        <tr data-bind="with: CallbackDraftGridProperties">
                                                                                            <th style="width: 50px">No.</th>
                                                                                            <th data-bind="click: function () { Sorting('TaskName'); }, css: GetSortedColumn('TaskName')">Name</th>
                                                                                            <th data-bind="click: function () { Sorting('ActionType'); }, css: GetSortedColumn('ActionType')">Action</th>
                                                                                            <th data-bind="click: function () { Sorting('LastModifiedBy'); }, css: GetSortedColumn('LastModifiedBy')">Modified By</th>
                                                                                            <th data-bind="click: function () { Sorting('LastModifiedDate'); }, css: GetSortedColumn('LastModifiedDate')">Modified Date</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <thead data-bind="visible: CallbackDraftGridProperties().AllowFilter" class="table-filter">
                                                                                        <tr>
                                                                                            <th class="clear-filter">
                                                                                                <a href="#" data-bind="click: ClearCallbackDraftFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                                                    <i class="green icon-trash"></i>
                                                                                                </a>
                                                                                            </th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCallbackDraftTaskName, event: { change: CallbackDraftGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCallbackDraftActionType, event: { change: CallbackDraftGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCallbackDraftModifiedBy, event: { change: CallbackDraftGridProperties().Filter }" /></th>
                                                                                            <th>
                                                                                                <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.FilterCallbackDraftModifiedDate, event: { change: CallbackDraftGridProperties().Filter }" /></th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody data-bind="foreach: CustomerCallbackDrafts, visible: CustomerCallbackDrafts().length > 0">
                                                                                        <tr data-bind="click: $root.GetCallbackDraftSelectedRow" data-toggle="modal">
                                                                                            <td><span data-bind="text: RowID"></span></td>
                                                                                            <td><span data-bind="text: TaskName"></span></td>
                                                                                            <td><span data-bind="text: ActionType"></span></td>
                                                                                            <td><span data-bind="text: LastModifiedBy"></span></td>
                                                                                            <td><span data-bind="text: $root.LocalDate(LastModifiedDate)"></span></td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                    <tbody data-bind="visible: CustomerCallbackDrafts().length == 0">
                                                                                        <tr>
                                                                                            <td colspan="6" class="text-center">No entries available.
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="widget-toolbox padding-8 clearfix">
                                                                    <div class="row" data-bind="with: CallbackDraftGridProperties">
                                                                        <div class="col-sm-6">
                                                                            <div class="dataTables_paginate paging_bootstrap pull-left">
                                                                                Showing
                                                                                <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                                                                rows of <span data-bind="text: Total"></span>
                                                                                entries
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                                Page
                                                                                <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                                                of <span data-bind="text: TotalPages"></span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                                <ul class="pagination">
                                                                                    <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                                            <i class="icon-double-angle-left"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                    '
        								                                            <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                                            <i class="icon-angle-left"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                                            <i class="icon-angle-right"></i>
                                                                                        </a>
                                                                                    </li>
                                                                                    <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                                            <i class="icon-double-angle-right"></i>'
                                                                                        </a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!------------- End Tab Customer  -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!--<button class="btn btn-sm btn-primary" data-bind="click: $root.save, visible: $root.IsNewData()">
                    <i class="icon-save"></i>
                    Save
                </button>
                <button class="btn btn-sm btn-primary" data-bind="click: $root.update, visible: !$root.IsNewData()">
                    <i class="icon-edit"></i>
                    Update
                </button>
                <button class="btn btn-sm btn-warning" data-bind="click: $root.delete, visible: !$root.IsNewData()">
                    <i class="icon-trash"></i>
                    Delete
                </button> -->
                <button class="btn btn-sm" data-bind="click: $root.cancel">
                    <i class="icon-remove"></i>
                    Cancel
                </button>
            </div>
        </div>
    </div>
</div>
<!-- ALL MODAL POPUP -->


<!-- modal form start Account Form -->
<div id="account-modal-form" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- modal header start -->
            <div class="modal-header">
                <h4 class="blue bigger">
                    <span data-bind="if: $root.IsNewAccountData()">Create a new Customer Account Parameter</span>
                    <span data-bind="if: !$root.IsNewAccountData()">Modify a Customer Account Parameter</span>
                </h4>
            </div>
            <!-- modal header end -->

            <!-- modal body start -->
            <div class="modal-body overflow-visible">
                <div class="row">
                    <div class="col-xs-12">
                        <!-- modal body form start -->
                        <div id="customer-form" class="form-horizontal" role="form">
                            <div class="space-4"></div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="code">
                                    Account Number</label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <input type="text" id="accountNumber" name="accountNumber" data-bind="value: $root.AccountNumber_a, disable: !$root.IsNewAccountData()" class="col-xs-10 col-sm-5" data-rule-required="true" data-rule-maxlength="50" />
                                    </div>
                                </div>
                            </div>

                            <div class="space-4"></div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="description">
                                    Currency</label>

                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <select id="currencya" name="currencya" data-bind="value: Currency_a().ID, options: ddlCurrency_a, optionsText: 'FullName', optionsValue: 'ID', optionsCaption: 'Please Select ...'" data-rule-required="true" data-rule-value="true" class="col-xs-10 col-sm-7"></select>
                                    </div>
                                </div>
                            </div>

                            <div class="space-4"></div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="description">
                                    Account Type
                                </label>


                                <div class="clearfix">
                                    <select id="IsJointAccount" name="IsJointAccount" data-bind="value: $root.IsJointAccount_a, options: CIFJointAccounts, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Please Select ...'" data-rule-required="true" data-rule-value="true" class="col-xs-10 col-sm-7"></select>
                                </div>
                            </div>

                        </div>
                        <!-- modal body form end -->
                    </div>
                </div>
            </div>
            <!-- modal body end -->

            <!-- modal footer start -->
            <div class="modal-footer">
                <button class="btn btn-sm btn-primary" data-bind="click: $root.save_account, visible: $root.IsNewAccountData() && false">
                    <i class="icon-save"></i>
                    Save
                </button>
                <button class="btn btn-sm btn-primary" data-bind="click: $root.update_account, visible: !$root.IsNewAccountData() && false">
                    <i class="icon-edit"></i>
                    Update
                </button>
                <button class="btn btn-sm btn-warning" data-bind="click: $root.delete_account, visible: !$root.IsNewAccountData() && false">
                    <i class="icon-trash"></i>
                    Delete
                </button>
                <button class="btn btn-sm" data-dismiss="modal">
                    <i class="icon-remove"></i>
                    Cancel
                </button>
            </div>
            <!-- modal footer end -->

        </div>
    </div>
</div>
<!-- modal form end Account Form-->

<!-- modal form start -->
<div id="function-modal-form" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
    <div class="modal-dialog" style="width: 100%">
        <div class="modal-content">
            <!-- modal header start -->
            <div class="modal-header">
                <h4 class="blue bigger">
                    <span data-bind="if: $root.IsNewFunctionData()">Create a new Customer Function Parameter</span>
                    <span data-bind="if: !$root.IsNewFunctionData()">Modify a Customer Function Parameter</span>
                </h4>
            </div>
            <!-- modal header end -->

            <!-- modal body start -->
            <div class="modal-body overflow-visible">
                <div class="row">
                    <div class="col-xs-12">
                        <!-- modal body form start -->
                        <div id="customer-form" class="form-horizontal" role="form">
                            <div class="space-4"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label no-padding-right" for="Name">
                                    Function Name</label>
                                <div class="col-sm-10">
                                    <div class="clearfix">
                                        <select type="text" id="branch" name="branch" data-bind="options: ddlPOAFunction_f, optionsText: 'FullName', optionsValue: 'ID', optionsCaption: 'Please Select...', value: POAFunction_f().ID" data-rule-required="true" data-rule-value="true"></select>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <!-- modal body form end -->
                    </div>
                </div>
            </div>
            <!-- modal body end -->

            <!-- modal footer start -->
            <div class="modal-footer">
                <button class="btn btn-sm btn-primary" data-bind="click: $root.save_f, visible: $root.IsNewFunctionData() && false">
                    <i class="icon-save"></i>
                    Save
                </button>
                <button class="btn btn-sm btn-primary" data-bind="click: $root.update_f, visible: !$root.IsNewFunctionData() && false">
                    <i class="icon-edit"></i>
                    Update
                </button>
                <button class="btn btn-sm btn-warning" data-bind="click: $root.delete_f, visible: !$root.IsNewFunctionData() && false">
                    <i class="icon-trash"></i>
                    Delete
                </button>
                <button class="btn btn-sm" data-dismiss="modal">
                    <i class="icon-remove"></i>
                    Cancel
                </button>
            </div>
            <!-- modal footer end -->

        </div>
    </div>
</div>
<!-- modal form end -->
<!-- modal form contact start -->
<div id="contact-modal-form" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
    <div class="modal-dialog" style="width: 80%">
        <div class="modal-content">
            <!-- modal header start -->
            <div class="modal-header">
                <h4 class="blue bigger">
                    <span data-bind="if: $root.NewData_c()">Create a new Customer Contact Parameter</span>
                    <span data-bind="if: !$root.NewData_c()">Modify a Customer Contact Parameter</span>
                </h4>
            </div>
            <!-- modal header end -->

            <!-- modal body start -->

            <div class="modal-body overflow-visible">
                <div class="row" data-bind="ifnot: $data == null">
                    <div class="col-xs-12">
                        <!-- modal body form start -->
                        <div id="customer-form" class="form-horizontal" role="form">
                            <div class="widget-box">
                                <div class="widget-header widget-header-flat widget-header-small">
                                    <h5>Customer Contact</h5>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="cif">CIF</label>
                                    <div class="col-sm-10">
                                        <div class="clearfix">
                                            <input type="text" id="cif" name="cif" data-bind="value: CIF, enable: $root.IsNewData()" class="col-xs-10 col-sm-5" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="name">Name</label>
                                    <div class="col-sm-10">
                                        <div class="clearfix">
                                            <input type="text" id="name" name="name" data-bind="value: Name" class="col-xs-12" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="biz-segment">Biz Segment</label>
                                    <div class="col-sm-10">
                                        <div class="clearfix">
                                            <input type="text" id="biz-segment" name="biz-segment" data-bind="value: BizSegment().Description" class="col-xs-10 col-sm-5" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="branch">Branch</label>

                                    <div class="col-sm-10">
                                        <div class="clearfix">
                                            <input type="text" id="branch" name="branch" data-bind="value: Branch().Name" class="col-xs-10 col-sm-8" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="customer-type">Customer Type</label>

                                    <div class="col-sm-10">
                                        <div class="clearfix">
                                            <input type="text" id="customer-type" name="customer-type" data-bind="value: Type().Name" class="col-xs-10 col-sm-3" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="rm-code">RM Code</label>
                                    <div class="col-sm-10">
                                        <div class="clearfix">
                                            <input type="text" id="rm-code" name="rm-code" data-bind="value: UserApproval().Name()" class="col-xs-10 col-sm-5" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>
                                <div class="hr hr8 hr-double"></div>
                                <div class="form-group" data-bind="visible: false">
                                    <label class="col-sm-3 control-label bolder no-padding-right" for="id">
                                        ID</label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="id" name="id" data-bind="value: ID_c, enable: $root.NewData_c(), disable: $root.Readonly" class="col-xs-10 col-sm-5" />
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="contactname">
                                        Contact Name</label>
                                    <div class="col-sm-10">
                                        <div class="clearfix">
                                            <input type="text" id="Namecontact" name="NameContact" data-bind="value: Name_c, disable: $root.Readonly" class="col-xs-10 col-sm-5" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="phn-number">
                                        Phone Number</label>
                                    <div class="col-sm-10">
                                        <div class="clearfix">
                                            <input type="text" id="PhoneNumber" name="PhoneNumber" data-bind="value: PhoneNumber_c, disable: $root.Readonly" class="col-xs-10 col-sm-5" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="dateofBirth">Date of Birth</label>
                                    <div class="col-sm-10">
                                        <div class="clearfix input-group col-sm-3 no-padding">
                                            <input class="form-control date-picker" type="text" data-date-format="dd-M-yyyy" id="dateOfBirth" name="dateOfBirth" data-bind="value: DateOfBirth_c, disable: $root.Readonly">
                                            <span class="input-group-addon">
                                                <i class="icon-calendar bigger-110"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="IDNumber">
                                        ID Number</label>
                                    <div class="col-sm-10">
                                        <div class="clearfix">
                                            <input type="text" id="IDNumber" name="IDNumber" data-bind="value: IDNumber_c, disable: $root.Readonly" class="col-xs-10 col-sm-5" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="POAFunction">
                                        POA Function</label>
                                    <div class="col-sm-10">
                                        <div class="clearfix">
                                            <select id="matrix" name="matrix" data-bind="value: POAFunction_c().ID, options: ddlPOAFunction_c, optionsText: 'Description', optionsValue: 'ID', optionsCaption: 'Please Select...', disable: $root.Readonly" data-rule-required="true" data-rule-value="true" class="col-xs-9"></select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" data-bind="visible: POAFunction_c().ID == 9">
                                    <label class="col-sm-2 control-label no-padding-right" for="POAFunction">
                                        POA Function Other</label>
                                    <div class="col-sm-10">
                                        <div class="clearfix">
                                            <input type="text" id="functionOther" name="functionOther" data-bind="value: FunctionOther_c, disable: $root.Readonly" class="col-xs-10 col-sm-5" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="OccupationInID">
                                        OccupationIn ID</label>
                                    <div class="col-sm-10">
                                        <div class="clearfix">
                                            <input type="text" id="OccupationInID" name="OccupationInID" data-bind="value: OccupationInID_c, disable: $root.Readonly" class="col-xs-10 col-sm-5" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="PlaceOfBirth">
                                        Place of Birth</label>
                                    <div class="col-sm-10">
                                        <div class="clearfix">
                                            <input type="text" id="PlaceOfBirth" name="PlaceOfBirth" data-bind="value: PlaceOfBirth_c, disable: $root.Readonly" class="col-xs-10 col-sm-5" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="EffectiveDate">Effective Date</label>

                                    <div class="col-sm-10">
                                        <div class="clearfix input-group col-sm-3 no-padding">
                                            <input class="form-control date-picker" type="text" data-date-format="dd-M-yyyy" id="EffectiveDate" name="EffectiveDate" data-bind="value: EffectiveDate_c, disable: $root.Readonly">
                                            <span class="input-group-addon">
                                                <i class="icon-calendar bigger-110"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="cancellationDate">Cancellation Date</label>

                                    <div class="col-sm-10">
                                        <div class="clearfix input-group col-sm-3 no-padding">
                                            <input class="form-control date-picker" type="text" data-date-format="dd-M-yyyy" id="cancellationDate" name="cancellationDate" data-bind="value: CancellationDate_c, disable: $root.Readonly">
                                            <span class="input-group-addon">
                                                <i class="icon-calendar bigger-110"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- modal body form end -->
                    </div>
                </div>
            </div>
            <!-- modal body end -->

            <!-- modal footer start -->
            <div class="modal-footer">
                <button class="btn btn-sm btn-primary" data-bind="click: $root.save_c, disable: !$root.IsEditable(), visible: $root.IsNewData_c() && $root.IsEditCustomerContact()">
                    <i class="icon-save"></i>
                    Submit
                </button>
                <button class="btn btn-sm btn-primary" data-bind="click: $root.update_c, visible: !$root.IsNewData_c() && $root.IsEditCustomerContact(), disable: $root.Readonly">
                    <i class="icon-edit"></i>
                    Update
                </button>
                <button class="btn btn-sm btn-warning" data-bind="click: $root.delete_c, visible: !$root.IsNewData_c() && $root.IsEditCustomerContact(), disable: $root.Readonly">
                    <i class="icon-trash"></i>
                    Delete
                </button>
                <button class="btn btn-sm" data-dismiss="modal">
                    <i class="icon-remove"></i>
                    Close
                </button>
            </div>
            <!-- modal footer end -->

        </div>
    </div>
</div>
<!-- modal form contact end-->

<!-- modal form Underlying start -->
<div id="Underlying-modal-form" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
    <div class="modal-dialog" style="width: 100%">
        <div class="modal-content">
            <!-- modal header start -->
            <div class="modal-header">
                <h4 class="blue bigger">
                    <span>Modify a Customer Underlying</span>
                </h4>
            </div>
            <!-- modal header end -->

            <!-- modal body start -->

            <div class="modal-body overflow-visible">
                <div class="row" data-bind="ifnot: $data == null">
                    <div class="col-xs-12">
                        <!-- modal body form start -->
                        <div id="Underlying-customer-form" class="form-horizontal" role="form">
                            <div class="widget-box">
                                <div class="widget-header widget-header-flat widget-header-small">
                                    <h5>Customer Underlying</h5>
                                </div>
                                <div class="space-6"></div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="cif">CIF</label>
                                    <div class="col-sm-10">
                                        <div class="clearfix">
                                            <input type="text" id="cif" name="cif" data-bind="value: CIF, enable: $root.IsNewData()" class="col-xs-10 col-sm-5" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="name">Name</label>
                                    <div class="col-sm-10">
                                        <div class="clearfix">
                                            <input type="text" id="name" name="name" data-bind="value: Name" class="col-xs-12" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="biz-segment">Biz Segment</label>
                                    <div class="col-sm-10">
                                        <div class="clearfix">
                                            <input type="text" id="biz-segment" name="biz-segment" data-bind="value: BizSegment().Description" class="col-xs-10 col-sm-5" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="branch">Branch</label>

                                    <div class="col-sm-10">
                                        <div class="clearfix">
                                            <input type="text" id="branch" name="branch" data-bind="value: Branch().Name" class="col-xs-10 col-sm-8" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="customer-type">Customer Type</label>

                                    <div class="col-sm-10">
                                        <div class="clearfix">
                                            <input type="text" id="customer-type" name="customer-type" data-bind="value: Type().Name" class="col-xs-10 col-sm-3" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="rm-code">RM Code</label>
                                    <div class="col-sm-10">
                                        <div class="clearfix">
                                            <input type="text" id="rm-code" name="rm-code" data-bind="value: UserApproval().Name()" class="col-xs-10 col-sm-5" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>
                                <div class="hr hr8 hr-double"></div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="ht_clone_top handsontable" style="position: static; top: 0px; left: 0px; overflow: hidden; width: 1290px; height: auto;">
                                            <div id="dataTable" class="wtHolder" style="position: relative; width: 1290px; height: auto;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- modal body form end -->
                    </div>
                </div>
            </div>
            <!-- modal body end -->

            <!-- modal footer start -->
            <div class="modal-footer">
                <button class="btn btn-sm btn-primary" data-bind="click: $root.save_cu">
                    <i class="icon-save"></i>
                    Save
                </button>
                <button class="btn btn-sm" data-dismiss="modal">
                    <i class="icon-remove"></i>
                    Cancel
                </button>
            </div>
            <!-- modal footer end -->

        </div>
    </div>
</div>
<!-- modal form Underlying end-->

<!--modal form underlying start-->
<!-- modal form start Attach Form -->
<div id="modal-form-Attach" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
    <div class="modal-dialog" style="width: 100%">
        <div class="modal-content">
            <!-- modal header start Attach FIle Form -->
            <div class="modal-header">
                <h4 class="blue bigger">
                    <span>Added Attachment File</span>
                </h4>
            </div>
            <!-- modal header end Attach file -->

            <!-- modal body start Attach File -->
            <div class="modal-body overflow-visible">
                <div class="row">
                    <div class="col-xs-12">
                        <!-- modal body form start -->
                        <div id="customer-form1" class="form-horizontal" role="form">
                            <div class="space-4"></div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="Name">
                                    Purpose of Doc
                                </label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <select id="documentPurpose" name="documentPurpose" data-bind="value: $root.DocumentPurpose_a().ID, options: ddlDocumentPurpose_a, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                        <label class="control-label bolder text-danger">*</label>
                                    </div>
                                </div>
                            </div>

                            <div class="space-4"></div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="description">
                                    Type of Doc
                                </label>

                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <select id="documentType" name="documentType" data-bind="value: $root.DocumentType_a().ID, options: ddlDocumentType_a, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Please Choose...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                        <label class="control-label bolder text-danger">*</label>
                                    </div>
                                </div>
                            </div>
                            <div class="space-4"></div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="document-path">Document</label>

                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <input type="file" id="document-path" class="col-xs-7" name="document-path" data-bind="file: DocumentPath_a" />
                                        <label class="control-label bolder text-danger">*</label>
                                        <!---->
                                        <!--<span data-bind="text: DocumentPath"></span>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- modal body form Attach File End -->

                        <!-- widget box Underlying Attach start -->
                        <div class="widget-box">
                            <!-- widget header Underlying Attach start -->
                            <div class="widget-header widget-hea1der-small header-color-dark">
                                <h6>Underlying Form Table</h6>

                                <div class="widget-toolbar">
                                    <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                        <i class="blue icon-filter"></i>
                                        <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: UnderlyingAttachGridProperties().AllowFilter" />
                                        <span class="lbl"></span>
                                    </label>
                                    <a href="#" data-bind="click: GetDataUnderlyingAttach" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                        <i class="blue icon-refresh"></i>
                                    </a>
                                </div>
                            </div>
                            <!-- widget header end -->

                            <!-- widget body underlying Attach start -->
                            <div class="widget-body">
                                <!-- widget main start -->
                                <div class="widget-main padding-0">
                                    <!-- widget slim control start -->
                                    <div class="slim-scroll" data-height="400">
                                        <!-- widget content start -->
                                        <div class="content">
                                            <!-- table responsive start -->
                                            <div class="table-responsive">
                                                <div class="dataTables_wrapper" role="grid">
                                                    <table id="Customer Underlying-table" class="table table-striped table-bordered table-hover dataTable">
                                                        <thead>
                                                            <tr data-bind="with: UnderlyingAttachGridProperties">
                                                                <!-- <th style="width:50px">Select</th> -->
                                                                <th style="width: 50px">No.</th>
                                                                <th style="width: 50px">Select</th>
                                                                <th data-bind="click: function () { Sorting('StatementLetter'); }, css: GetSortedColumn('StatementLetter')">Statement Letter</th>
                                                                <th data-bind="click: function () { Sorting('UnderlyingDocument'); }, css: GetSortedColumn('UnderlyingDocument')">Underlying Document</th>
                                                                <th data-bind="click: function () { Sorting('DocumentType'); }, css: GetSortedColumn('DocumentType')">Type of Doc</th>
                                                                <th data-bind="click: function () { Sorting('Currency'); }, css: GetSortedColumn('Currency')">Currency</th>
                                                                <th data-bind="click: function () { Sorting('Amount'); }, css: GetSortedColumn('Amount')">Amount</th>
                                                                <th data-bind="click: function () { Sorting('SupplierName'); }, css: GetSortedColumn('SupplierName')">Supplier Name</th>
                                                                <!--  <th data-bind="click: function () { Sorting('ReferenceNumber'); }, css: GetSortedColumn('ReferenceNumber')">Reference Number</th> -->
                                                                <th data-bind="click: function () { Sorting('InvoiceNumber'); }, css: GetSortedColumn('Invoice Number')">Invoice Number</th>
                                                                <th data-bind="click: function () { Sorting('DateOfUnderlying'); }, css: GetSortedColumn('DateOfUnderlying')">Date</th>
                                                                <th data-bind="click: function () { Sorting('ExpiredDate'); }, css: GetSortedColumn('ExpiredDate')">Expiry Date</th>
                                                            </tr>
                                                        </thead>
                                                        <thead data-bind="visible: UnderlyingAttachGridProperties().AllowFilter" class="table-filter">
                                                            <tr>
                                                                <th class="clear-filter">
                                                                    <a href="#" data-bind="click: UnderlyingAttachClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                        <i class="green icon-trash"></i>
                                                                    </a>
                                                                </th>
                                                                <th class="clear-filter"></th>
                                                                <th>
                                                                    <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterStatementLetter, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                <th>
                                                                    <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterUnderlyingDocument, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                <th>
                                                                    <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterDocumentType, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                <th>
                                                                    <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterCurrency, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                <th>
                                                                    <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterAmount, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                <th>
                                                                    <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterSupplierName, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                <th>
                                                                    <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterInvoiceNumber, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                <th>
                                                                    <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.UnderlyingAttachFilterDateOfUnderlying, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                <th>
                                                                    <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.UnderlyingAttachFilterExpiredDate, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody data-bind="foreach: { data: CustomerAttachUnderlyings, as: 'AttachData' }, visible: CustomerAttachUnderlyings().length > 0">
                                                            <tr>
                                                                <td><span data-bind="text: RowID"></span></td>
                                                                <td align="center">
                                                                    <input type="checkbox" id="isSelected2" name="isSelect2" data-bind="checked: IsSelectedAttach, event: { change: function (data) { $root.onSelectionAttach($index(), $data) } }" /></td>
                                                                <td><span data-bind="text: StatementLetter.Name"></span></td>
                                                                <td><span data-bind="text: UnderlyingDocument.Name"></span></td>
                                                                <td><span data-bind="text: DocumentType.Name"></span></td>
                                                                <td><span data-bind="text: Currency.Code"></span></td>
                                                                <td align="right"><span data-bind="text: formatNumber(Amount)"></span></td>
                                                                <td><span data-bind="text: SupplierName"></span></td>
                                                                <td><span data-bind="text: InvoiceNumber"></span></td>
                                                                <td><span data-bind="text: $root.LocalDate(DateOfUnderlying, true, false)"></span></td>
                                                                <td><span data-bind="text: $root.LocalDate(ExpiredDate, true, false)"></span></td>
                                                            </tr>
                                                        </tbody>
                                                        <tbody data-bind="visible: CustomerAttachUnderlyings().length == 0">
                                                            <tr>
                                                                <td colspan="11" class="text-center">No entries available.
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <!-- table responsive end -->
                                        </div>
                                        <!-- widget content end -->
                                    </div>
                                    <!-- widget slim control end -->

                                    <!-- widget footer start -->
                                    <div class="widget-toolbox padding-8 clearfix">
                                        <div class="row" data-bind="with: UnderlyingAttachGridProperties">
                                            <!-- pagination size start -->
                                            <div class="col-sm-6">
                                                <div class="dataTables_paginate paging_bootstrap pull-left">
                                                    Showing
                                                    <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                                    rows of <span data-bind="text: Total"></span>
                                                    entries
                                                </div>
                                            </div>
                                            <!-- pagination size end -->

                                            <!-- pagination page jump start -->
                                            <div class="col-sm-3">
                                                <div class="dataTables_paginate paging_bootstrap">
                                                    Page
                                                    <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                    of <span data-bind="text: TotalPages"></span>
                                                </div>
                                            </div>
                                            <!-- pagination page jump end -->

                                            <!-- pagination navigation start -->
                                            <div class="col-sm-3">
                                                <div class="dataTables_paginate paging_bootstrap">
                                                    <ul class="pagination">
                                                        <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                            <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                <i class="icon-double-angle-left"></i>
                                                            </a>
                                                        </li>
                                                        <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                            <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                <i class="icon-angle-left"></i>
                                                            </a>
                                                        </li>
                                                        <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                            <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                <i class="icon-angle-right"></i>
                                                            </a>
                                                        </li>
                                                        <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                            <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                <i class="icon-double-angle-right"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <!-- pagination navigation end -->

                                        </div>
                                    </div>
                                    <!-- widget footer end -->

                                </div>
                                <!-- widget main end -->
                            </div>
                            <!-- widget body end -->
                        </div>
                        <!-- widget box end -->



                    </div>
                </div>
            </div>
            <!-- modal body end Attach File -->


            <!-- modal footer start Attach File-->
            <div class="modal-footer">
                <button class="btn btn-sm btn-primary" data-bind="click: $root.save_a">
                    <i class="icon-save"></i>
                    Save
                </button>
                <button class="btn btn-sm" data-dismiss="modal">
                    <i class="icon-remove"></i>
                    Cancel
                </button>
            </div>
            <!-- modal footer end attach file -->

        </div>
    </div>
</div>
<!-- modal form end Attach Form -->

<!-- modal form start Underlying Form -->
<div id="modal-form-Underlying" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
    <div class="modal-dialog" style="width: 100%">
        <div id="modal-form-Underlying-content" class="modal-content col-xs-12">
            <!-- modal header start Underlying Form -->
            <div class="modal-header" style="border-bottom: none;">
                <h4 class="blue bigger">
                    <span data-bind="if: $root.IsNewDataUnderlying()">Create a new Customer Underlying Parameter</span>
                    <span data-bind="if: !$root.IsNewDataUnderlying()">Modify a Customer Underlying Parameter</span>
                </h4>
            </div>
            <!-- modal header end -->

            <div class="tabbable" style="margin-bottom: 10px;">
                <ul class="nav nav-tabs" id="tabPopupUnderlying">
                    <li id="liUnderlyingDetails" class="active">
                        <a data-toggle="tab" href="#tabunderlyingdetails">Underlying Details</a>
                    </li>
                    <li id="liUnderlyingHistory">
                        <a data-toggle="tab" href="#tabunderlyinghistory">Underlying History</a>
                    </li>
                    <li id="liTransactionHistory">
                        <a data-toggle="tab" href="#tabtransactionhistory">Transaction History</a>
                    </li>
                </ul>

                <div class="tab-content" style="border: 1px solid #c5d0dc">
                    <div id="tabunderlyingdetails" class="tab-pane in active">
                        <!-- modal body start -->
                        <div class="modal-body overflow-visible">
                            <div class="row">
                                <div class="col-xs-12">
                                    <!-- modal body form start -->
                                    <div id="customer-form2" class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="JointAccountID">
                                                Joint Account</label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <select id="book-joinAcc" name="book-statement" data-bind="value: IsJointAccount_u,
    options: $root.CIFJointAccounts,
    optionsText: 'Name',
    optionsValue: 'ID',
    event: { change: $root.OnChangeJointAcc }, enable: $root.IsJointAccount"
                                                        class="col-xs-3">
                                                    </select>&nbsp;      
                                                    <select id="debit-acc-number" name="debit-acc-number" data-rule-required="true" data-bind="options: $root.JointAccountNumbers,
    optionsText: function (item) {
        if (item.CustomerName != null) {
            return item.AccountNumber + ' (' + item.Currency.Code + ') - ' + item.CustomerName
        } return item.AccountNumber + ' (' + item.Currency.Code + ')'
    },
    optionsValue: 'AccountNumber',
    optionsCaption: 'Please Select...',
    value: AccountNumber_u,
    event: { change: $root.CheckEmptyAccountNumber }, visible: $root.IsJointAccount_u"
                                                        class="col-xs-5">
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="StatementLetterID">
                                                Statement Letter</label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <select id="statementLetter" name="statementLetter" data-bind="value: $root.StatementLetter_u().ID, options: DynamicStatementLetter_u, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Please Choose...', enable: $root.IsStatementA(), event: { change: $root.OnChangeStatementLetterUnderlying }" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                                    <label class="control-label bolder text-danger" for="StatementLetter_u">*</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" data-bind="visible: !($root.StatementLetter_u().ID() == 4)">
                                            <label class="col-sm-3 control-label no-padding-right" for="UnderlyingDocument">
                                                Underlying Document</label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <select id="underlyingDocument" name="underlyingDocument" data-bind="value: UnderlyingDocument_u().ID, options: ddlUnderlyingDocument_u, optionsText: 'CodeName', optionsValue: 'ID', optionsCaption: 'Please Choose...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                                    <label class="control-label bolder text-danger" for="UnderlyingDocument_u">*</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div data-bind="visible: UnderlyingDocument_u().ID() == '50' && !($root.StatementLetter_u().ID() == 4)" class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="otherUnderlying">
                                                Other Underlying</label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <input type="text" id="otherUnderlying" autocomplete="off" name="otherUnderlying" data-bind="value: $root.OtherUnderlying_u" class="col-xs-7" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" data-bind="visible: !($root.StatementLetter_u().ID() == 4)">
                                            <label class="col-sm-3 control-label no-padding-right" for="DocumentType">
                                                Type of Document</label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <select id="documentType" name="documentType" data-bind="value: $root.DocumentType_u().ID, options: ddlDocumentType_u, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Choose...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                                    <label class="control-label bolder text-danger" for="DocumentType_u">*</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" data-bind="visible: !($root.StatementLetter_u().ID() == 4)">
                                            <label class="col-sm-3 control-label no-padding-right" for="Currency">
                                                Transaction Currency</label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <select id="currency" name="currency" data-bind="value: $root.Currency_u().ID, options: ddlCurrency_u, disable: $root.StatementLetter_u().ID() == 1, optionsText: 'Code', optionsValue: 'ID', optionsCaption: 'Choose...', event: { change: function (data) { OnCurrencyChange(Currency_u().ID()) } }" data-rule-required="true" data-rule-value="true" class="col-xs-2"></select>
                                                    <label class="control-label bolder text-danger" for="Currency_u">*</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" data-bind="visible: !($root.StatementLetter_u().ID() == 4)">
                                            <label class="col-sm-3 control-label no-padding-right" for="Amount">
                                                Invoice Amount</label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <input type="text" id="Amount_u" name="Amount_u" autocomplete="off" data-bind="value: $root.Amount_u, disable: $root.StatementLetter_u().ID() == 1, valueUpdate: ['afterkeydown', 'propertychange', 'input'], event: { 'keyup': Read_u }" data-rule-required="true" data-rule-number="true" class="col-xs-4 align-right" />
                                                    <label class="control-label bolder text-danger" for="Amount_u">*</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" data-bind="visible: !($root.StatementLetter_u().ID() == 4)">
                                            <label class="col-sm-3 control-label no-padding-right" for="rate">Rate</label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <input type="text" class="col-sm-4 align-right" name="rate_u" id="rate_u" disabled="disabled" data-rule-required="true" data-rule-number="true" data-bind="value: formatNumber(Rate_u)" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" data-bind="visible: !($root.StatementLetter_u().ID() == 4)">
                                            <label class="col-sm-3 control-label no-padding-right" for="eqv-usd">Eqv. USD</label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <input type="text" class="col-sm-4 align-right" disabled="disabled" name="eqv-usd_u" id="eqv-usd_u" data-rule-required="true" data-rule-number="true" data-bind="value: formatNumber(AmountUSD_u)" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="DateOfUnderlying">
                                                Date of Underlying</label>
                                            <div class="col-sm-2">
                                                <div class="input-group">
                                                    <input type="text" id="DateOfUnderlying" autocomplete="off" name="DateOfUnderlying_u" data-date-format="dd-M-yyyy" data-bind="value: DateOfUnderlying_u, event: { change: $root.onChangeDateOfUnderlying }" class="form-control date-picker col-xs-6" data-rule-required="true" />
                                                    <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                                    <label class="control-label bolder text-danger starremove" for="DateOfUnderlying_u" style="position: absolute;">*</label>
                                                </div>
                                            </div>
                                            <label class="col-sm-2 control-label no-padding-right" for="ExpiredDate">
                                                Expiry Date</label>
                                            <div class="col-sm-2">
                                                <div class="input-group">
                                                    <input type="text" id="ExpiredDate" autocomplete="off" name="ExpiredDate" data-bind="value: ExpiredDate_u" data-date-format="dd-M-yyyy" class="form-control date-picker col-xs-6" data-rule-required="true" />
                                                    <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                                    <label class="control-label bolder text-danger starremove" for="ExpiredDate" style="position: absolute;">*</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" data-bind="visible: false">
                                            <label class="col-sm-3 control-label no-padding-right" for="IsDeclarationOfException">
                                                Declaration of Exception</label>
                                            <div class="col-sm-9">
                                                <label>
                                                    <input name="switch-field-1" id="IsDeclarationOfException" data-bind="checked: $root.IsDeclarationOfException_u" class="ace ace-switch ace-switch-6" type="checkbox" />
                                                    <span class="lbl"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group" data-bind="visible: false">
                                            <label class="col-sm-3 control-label no-padding-right" for="StartDate">
                                                Start Date</label>
                                            <div class="col-sm-2">
                                                <div class="clearfix">
                                                    <div class="input-group">
                                                        <input id="StartDate" name="StartDate" data-bind="value: StartDate_u" data-date-format="dd-M-yyyy" class="form-control date-picker col-xs-6" type="text" />
                                                        <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <label class="col-sm-2 control-label no-padding-right" for="EndDate">
                                                End Date</label>
                                            <div class="col-sm-2">
                                                <div class="clearfix">
                                                    <div class="input-group">
                                                        <input type="text" id="EndDate" name="EndDate" data-bind="value: EndDate_u" data-date-format="dd-M-yyyy" class="form-control date-picker col-xs-6" />
                                                        <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" data-bind="visible: !($root.StatementLetter_u().ID() == 4)">
                                            <label class="col-sm-3 control-label no-padding-right" for="ReferenceNumber">
                                                Reference Number</label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <input type="text" id="ReferenceNumber" name="ReferenceNumber" data-bind="value: $root.ReferenceNumber_u" disabled="disabled" class="col-xs-9" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" data-bind="visible: !($root.StatementLetter_u().ID() == 4)">
                                            <label class="col-sm-3 control-label no-padding-right" for="SupplierName">
                                                Supplier Name</label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <input type="text" id="SupplierName" autocomplete="off" name="SupplierName" data-bind="value: $root.SupplierName_u" class="col-xs-8" data-rule-required="true" />
                                                    <label class="control-label bolder text-danger" for="SupplierName_u">*</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" data-bind="visible: !($root.StatementLetter_u().ID() == 4)">
                                            <label class="col-sm-3 control-label no-padding-right" for="InvoiceNumber">
                                                Invoice Number</label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <input type="text" id="InvoiceNumber" autocomplete="off" name="InvoiceNumber" data-bind="value: $root.InvoiceNumber_u" class="col-xs-8" data-rule-required="true" />
                                                    <label class="control-label bolder text-danger" for="InvoiceNumber_u">*</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" data-bind="visible: !($root.StatementLetter_u().ID() == 4)">
                                            <label class="col-sm-3 control-label no-padding-right" for="AttachmentNo">
                                                Is This doc Purposed to replace the proforma doc</label>
                                            <div class="col-sm-9">
                                                <label>
                                                    <input name="switch-field-1" id="IsProforma" data-bind="checked: IsProforma_u, disable: DocumentType_u().ID() == 2" class="ace ace-switch ace-switch-6" type="checkbox" />
                                                    <span class="lbl"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <!-- grid proforma begin -->

                                        <!-- widget box start -->
                                        <div class="widget-box" data-bind="visible: IsProforma_u() && !($root.StatementLetter_u().ID() == 4)">
                                            <!-- widget header start -->
                                            <div class="widget-header widget-hea1der-small header-color-dark">
                                                <h6>Customer Underlying Proforma Table</h6>
                                                <div class="widget-toolbar">
                                                    <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                                        <i class="blue icon-filter"></i>
                                                        <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: UnderlyingProformaGridProperties().AllowFilter" />
                                                        <span class="lbl"></span>
                                                    </label>
                                                    <a href="#" data-bind="click: GetDataUnderlyingProforma" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                                        <i class="blue icon-refresh"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- widget header end -->
                                            <!-- widget body start -->
                                            <div class="widget-body">
                                                <!-- widget main start -->
                                                <div class="widget-main padding-0">
                                                    <!-- widget slim control start -->
                                                    <div class="slim-scroll" data-height="400">
                                                        <!-- widget content start -->
                                                        <div class="content">

                                                            <!-- table responsive start -->
                                                            <div class="table-responsive">
                                                                <div class="dataTables_wrapper" role="grid">
                                                                    <table id="Customer Underlying-table" class="table table-striped table-bordered table-hover dataTable">
                                                                        <thead>
                                                                            <tr data-bind="with: UnderlyingProformaGridProperties">
                                                                                <th style="width: 50px">No.</th>
                                                                                <th style="width: 50px">Select</th>
                                                                                <th data-bind="click: function () { Sorting('StatementLetter'); }, css: GetSortedColumn('StatementLetter')">Statement Letter</th>
                                                                                <th data-bind="click: function () { Sorting('UnderlyingDocument'); }, css: GetSortedColumn('UnderlyingDocument')">Underlying Document</th>
                                                                                <th data-bind="click: function () { Sorting('DocumentType'); }, css: GetSortedColumn('DocumentType')">Type of Doc</th>
                                                                                <th data-bind="click: function () { Sorting('Currency'); }, css: GetSortedColumn('Currency')">Currency</th>
                                                                                <th data-bind="click: function () { Sorting('Amount'); }, css: GetSortedColumn('Amount')">Amount</th>
                                                                                <th data-bind="click: function () { Sorting('SupplierName'); }, css: GetSortedColumn('SupplierName')">Supplier Name</th>
                                                                                <th data-bind="click: function () { Sorting('InvoiceNumber'); }, css: GetSortedColumn('InvoiceNumber')">Invoice Number</th>
                                                                                <th data-bind="click: function () { Sorting('DateOfUnderlying'); }, css: GetSortedColumn('DateOfUnderlying')">Date</th>
                                                                                <th data-bind="click: function () { Sorting('ExpiredDate'); }, css: GetSortedColumn('ExpiredDate')">Expiry Date</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <thead data-bind="visible: UnderlyingProformaGridProperties().AllowFilter" class="table-filter">
                                                                            <tr>
                                                                                <th class="clear-filter">
                                                                                    <a href="#" data-bind="click: ProformaClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                                        <i class="green icon-trash"></i>
                                                                                    </a>
                                                                                </th>
                                                                                <th class="clear-filter"></th>
                                                                                <th>
                                                                                    <input type="text" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterStatementLetter, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                                <th>
                                                                                    <input type="text" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterUnderlyingDocument, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                                <th>
                                                                                    <input type="text" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterDocumentType, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                                <th>
                                                                                    <input type="text" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterCurrency, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                                <th>
                                                                                    <input type="text" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterAmount, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                                <th>
                                                                                    <input type="text" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterSupplierName, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                                <th>
                                                                                    <input type="text" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterInvoiceNumber, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                                <th>
                                                                                    <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.ProformaFilterDateOfUnderlying, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                                <th>
                                                                                    <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.ProformaFilterExpiredDate, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody data-bind="foreach: { data: CustomerUnderlyingProformas, as: 'ProformasData' }, visible: CustomerUnderlyingProformas().length > 0">
                                                                            <tr>
                                                                                <td><span data-bind="text: RowID"></span></td>
                                                                                <td align="center">
                                                                                    <input type="checkbox" id="isSelectedProforma" name="isSelect" data-bind="checked: ProformasData.IsSelectedProforma, event: { change: function (data) { $root.onSelectionProforma($index(), $data) } }" /></td>
                                                                                <td><span data-bind="text: StatementLetter.Name"></span></td>
                                                                                <td><span data-bind="text: UnderlyingDocument.Name"></span></td>
                                                                                <td><span data-bind="text: DocumentType.Name"></span></td>
                                                                                <td><span data-bind="text: Currency.Code"></span></td>
                                                                                <td align="right"><span data-bind="text: formatNumber(Amount)"></span></td>
                                                                                <td><span data-bind="text: SupplierName"></span></td>
                                                                                <td><span data-bind="text: InvoiceNumber"></span></td>
                                                                                <td><span data-bind="text: $root.LocalDate(DateOfUnderlying, true, false)"></span></td>
                                                                                <td><span data-bind="text: $root.LocalDate(ExpiredDate, true, false)"></span></td>
                                                                            </tr>
                                                                        </tbody>
                                                                        <tbody data-bind="visible: CustomerUnderlyingProformas().length == 0">
                                                                            <tr>
                                                                                <td colspan="11" class="text-center">No entries available.
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <!-- table responsive end -->
                                                        </div>
                                                        <!-- widget content end -->
                                                    </div>
                                                    <!-- widget slim control end -->
                                                    <!-- widget footer start -->
                                                    <div class="widget-toolbox padding-8 clearfix">
                                                        <div class="row" data-bind="with: UnderlyingProformaGridProperties">
                                                            <!-- pagination size start -->
                                                            <div class="col-sm-6">
                                                                <div class="dataTables_paginate paging_bootstrap pull-left">
                                                                    Showing
                                                                    <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                                                    rows of <span data-bind="text: Total"></span>
                                                                    entries
                                                                </div>
                                                            </div>
                                                            <!-- pagination size end -->

                                                            <!-- pagination page jump start -->
                                                            <div class="col-sm-3">
                                                                <div class="dataTables_paginate paging_bootstrap">
                                                                    Page
                                                                    <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                                    of <span data-bind="text: TotalPages"></span>
                                                                </div>
                                                            </div>
                                                            <!-- pagination page jump end -->

                                                            <!-- pagination navigation start -->
                                                            <div class="col-sm-3">
                                                                <div class="dataTables_paginate paging_bootstrap">
                                                                    <ul class="pagination">
                                                                        <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                            <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                                <i class="icon-double-angle-left"></i>
                                                                            </a>
                                                                        </li>
                                                                        <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                            <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                                <i class="icon-angle-left"></i>
                                                                            </a>
                                                                        </li>
                                                                        <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                            <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                                <i class="icon-angle-right"></i>
                                                                            </a>
                                                                        </li>
                                                                        <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                            <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                                <i class="icon-double-angle-right"></i>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <!-- pagination navigation end -->

                                                        </div>
                                                    </div>
                                                    <!-- widget footer end -->

                                                </div>
                                                <!-- widget main end -->
                                            </div>
                                            <!-- widget body end -->
                                        </div>
                                        <!-- widget box end -->
                                        <!-- grid proforma end -->

                                        <div class="form-group" data-bind="visible: !($root.StatementLetter_u().ID() == 4)">
                                            <label class="col-sm-3 control-label no-padding-right" for="IsBulkUnderlying">
                                                Is This Bulk Underlying</label>
                                            <div class="col-sm-9">
                                                <label>
                                                    <input name="switch-field-1" id="IsBulkUnderlying" data-bind="checked: IsBulkUnderlying_u" class="ace ace-switch ace-switch-6" type="checkbox" />
                                                    <span class="lbl"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <!-- grid Bulk begin -->

                                        <!-- widget box start -->
                                        <div class="widget-box" data-bind="visible: IsBulkUnderlying_u() && !($root.StatementLetter_u().ID() == 4)">
                                            <!-- widget header start -->
                                            <div class="widget-header widget-hea1der-small header-color-dark">
                                                <h6>Customer Bulk Underlying Table</h6>

                                                <div class="widget-toolbar">
                                                    <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                                        <i class="blue icon-filter"></i>
                                                        <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: UnderlyingBulkGridProperties().AllowFilter" />
                                                        <span class="lbl"></span>
                                                    </label>
                                                    <a href="#" data-bind="click: GetDataBulkUnderlying" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                                        <i class="blue icon-refresh"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- widget header end -->

                                            <!-- widget body start -->
                                            <div class="widget-body">
                                                <!-- widget main start -->
                                                <div class="widget-main padding-0">
                                                    <!-- widget slim control start -->
                                                    <div class="slim-scroll" data-height="400">
                                                        <!-- widget content start -->
                                                        <div class="content">

                                                            <!-- table responsive start -->
                                                            <div class="table-responsive">
                                                                <div class="dataTables_wrapper" role="grid">
                                                                    <table id="Customer Underlying-table" class="table table-striped table-bordered table-hover dataTable">
                                                                        <thead>
                                                                            <tr data-bind="with: UnderlyingBulkGridProperties">
                                                                                <th style="width: 50px">No.</th>
                                                                                <th style="width: 50px">Select</th>
                                                                                <th data-bind="click: function () { Sorting('StatementLetter'); }, css: GetSortedColumn('StatementLetter')">Statement Letter</th>
                                                                                <th data-bind="click: function () { Sorting('UnderlyingDocument'); }, css: GetSortedColumn('UnderlyingDocument')">Underlying Document</th>
                                                                                <th data-bind="click: function () { Sorting('DocumentType'); }, css: GetSortedColumn('DocumentType')">Type of Doc</th>
                                                                                <th data-bind="click: function () { Sorting('Currency'); }, css: GetSortedColumn('Currency')">Currency</th>
                                                                                <th data-bind="click: function () { Sorting('Amount'); }, css: GetSortedColumn('Amount')">Amount</th>
                                                                                <th data-bind="click: function () { Sorting('SupplierName'); }, css: GetSortedColumn('SupplierName')">Supplier Name</th>
                                                                                <th data-bind="click: function () { Sorting('InvoiceNumber'); }, css: GetSortedColumn('InvoiceNumber')">Invoice Number</th>
                                                                                <th data-bind="click: function () { Sorting('DateOfUnderlying'); }, css: GetSortedColumn('DateOfUnderlying')">Date</th>
                                                                                <th data-bind="click: function () { Sorting('ExpiredDate'); }, css: GetSortedColumn('ExpiredDate')">Expiry Date</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <thead data-bind="visible: UnderlyingBulkGridProperties().AllowFilter" class="table-filter">
                                                                            <tr>
                                                                                <th class="clear-filter">
                                                                                    <a href="#" data-bind="click: BulkClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                                        <i class="green icon-trash"></i>
                                                                                    </a>
                                                                                </th>
                                                                                <th class="clear-filter"></th>
                                                                                <th>
                                                                                    <input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterStatementLetter, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                                <th>
                                                                                    <input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterUnderlyingDocument, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                                <th>
                                                                                    <input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterDocumentType, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                                <th>
                                                                                    <input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterCurrency, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                                <th>
                                                                                    <input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterSupplierName, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                                <th>
                                                                                    <input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterAmount, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                                <th>
                                                                                    <input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterInvoiceNumber, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                                <th>
                                                                                    <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.BulkFilterDateOfUnderlying, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                                <th>
                                                                                    <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.BulkFilterExpiredDate, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody data-bind="foreach: { data: CustomerBulkUnderlyings, as: 'BulkDatas' }, visible: CustomerBulkUnderlyings().length > 0">
                                                                            <tr>
                                                                                <td><span data-bind="text: RowID"></span></td>
                                                                                <td align="center">
                                                                                    <input type="checkbox" id="isSelectedBulk" name="isSelect" data-bind="checked: BulkDatas.IsSelectedBulk, event: { change: function (data) { $root.onSelectionBulk($index(), $data) } }" /></td>
                                                                                <td><span data-bind="text: StatementLetter.Name"></span></td>
                                                                                <td><span data-bind="text: UnderlyingDocument.Name"></span></td>
                                                                                <td><span data-bind="text: DocumentType.Name"></span></td>
                                                                                <td><span data-bind="text: Currency.Code"></span></td>
                                                                                <td align="right"><span data-bind="text: formatNumber(Amount)"></span></td>
                                                                                <td><span data-bind="text: SupplierName"></span></td>
                                                                                <td><span data-bind="text: InvoiceNumber"></span></td>
                                                                                <td><span data-bind="text: $root.LocalDate(DateOfUnderlying, true, false)"></span></td>
                                                                                <td><span data-bind="text: $root.LocalDate(ExpiredDate, true, false)"></span></td>
                                                                            </tr>
                                                                        </tbody>
                                                                        <tbody data-bind="visible: CustomerBulkUnderlyings().length == 0">
                                                                            <tr>
                                                                                <td colspan="11" class="text-center">No entries available.
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <!-- table responsive end -->
                                                        </div>
                                                        <!-- widget content end -->
                                                    </div>
                                                    <!-- widget slim control end -->

                                                    <!-- widget footer start -->
                                                    <div class="widget-toolbox padding-8 clearfix">
                                                        <div class="row" data-bind="with: UnderlyingBulkGridProperties">
                                                            <!-- pagination size start -->
                                                            <div class="col-sm-6">
                                                                <div class="dataTables_paginate paging_bootstrap pull-left">
                                                                    Showing
                                                                    <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                                                    rows of <span data-bind="text: Total"></span>
                                                                    entries
                                                                </div>
                                                            </div>
                                                            <!-- pagination size end -->

                                                            <!-- pagination page jump start -->
                                                            <div class="col-sm-3">
                                                                <div class="dataTables_paginate paging_bootstrap">
                                                                    Page
                                                                    <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                                    of <span data-bind="text: TotalPages"></span>
                                                                </div>
                                                            </div>
                                                            <!-- pagination page jump end -->

                                                            <!-- pagination navigation start -->
                                                            <div class="col-sm-3">
                                                                <div class="dataTables_paginate paging_bootstrap">
                                                                    <ul class="pagination">
                                                                        <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                            <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                                <i class="icon-double-angle-left"></i>
                                                                            </a>
                                                                        </li>
                                                                        <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                            <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                                <i class="icon-angle-left"></i>
                                                                            </a>
                                                                        </li>
                                                                        <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                            <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                                <i class="icon-angle-right"></i>
                                                                            </a>
                                                                        </li>
                                                                        <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                            <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                                <i class="icon-double-angle-right"></i>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <!-- pagination navigation end -->

                                                        </div>
                                                    </div>
                                                    <!-- widget footer end -->

                                                </div>
                                                <!-- widget main end -->
                                            </div>
                                            <!-- widget body end -->

                                        </div>
                                        <!-- widget box end -->
                                        <!-- grid Bulk end -->
                                    </div>
                                    <!-- modal body form end -->
                                </div>
                            </div>
                        </div>
                        <!-- modal body end -->
                    </div>

                    <div id="tabunderlyinghistory" class="tab-pane" style="min-height: 100px; font-size: 90%;">
                        <div class="space-4"></div>
                        <div class="dataTables_wrapper" role="grid" style="overflow-x: auto">
                            <table class="table table-striped table-bordered table-hover dataTable">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Statement Letter</th>
                                        <th>Underlying Document</th>
                                        <th>Type of Doc</th>
                                        <th>Currency</th>
                                        <th>Amount</th>
                                        <th>Available Amount in USD</th>
                                        <th>Date</th>
                                        <th>Supplier Name</th>
                                        <th>Invoice Number</th>
                                        <th>Account Number</th>
                                        <th>Joint Account</th>
                                        <th>Is Expired</th>
                                        <th>Attachment No</th>
                                        <th>Modified By</th>
                                        <th>Modified Date</th>
                                    </tr>
                                </thead>
                                <tbody data-bind="foreach: $root.CustomerUnderlyingHistories, visible: CustomerUnderlyingHistories().length > 0">
                                    <tr>
                                        <td><span data-bind="text: $index() + 1"></span></td>
                                        <td><span data-bind="text: StatementLetterName"></span></td>
                                        <td><span data-bind="text: UnderlyingDocName"></span></td>
                                        <td><span data-bind="text: DocTypeName"></span></td>
                                        <td align="center"><span data-bind="text: CurrencyCode"></span></td>
                                        <td align="right"><span data-bind="text: formatNumber(Amount)"></span></td>
                                        <td align="right"><span data-bind="text: formatNumber(AvailableAmountUSD)"></span></td>
                                        <td><span data-bind="text: $root.LocalDate(DateOfUnderlying, true, false)"></span></td>
                                        <td><span data-bind="text: SupplierName"></span></td>
                                        <td><span data-bind="text: InvoiceNumber"></span></td>
                                        <td><span data-bind="text: AccountNumber"></span></td>
                                        <td><span data-bind="text: IsJointAccount"></span></td>
                                        <td align="center"><span data-bind="text: IsExpired"></span></td>
                                        <td><span data-bind="text: AttachmentNo"></span></td>
                                        <td><span data-bind="text: UpdateBy"></span></td>
                                        <td><span data-bind="text: $root.LocalDate(UpdateDate)"></span></td>
                                    </tr>
                                </tbody>
                                <tbody data-bind="visible: CustomerUnderlyingHistories().length == 0">
                                    <tr>
                                        <td colspan="16" class="text-center">No entries available.
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div id="tabtransactionhistory" class="tab-pane" style="min-height: 100px; font-size: 90%;">
                        <div class="space-4"></div>
                        <div class="dataTables_wrapper" role="grid" style="overflow-x: auto">
                            <table class="table table-striped table-bordered table-hover dataTable">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Application ID</th>
                                        <th>Application Date</th>
                                        <th>Product</th>
                                        <th>Currency</th>
                                        <th>Amount</th>
                                        <th>Amount USD</th>
                                        <th>Utilized Amount In USD</th>
                                        <th>Transaction Status</th>
                                    </tr>
                                </thead>
                                <tbody data-bind="foreach: $root.CustomerUnderlyingTransactionHistories, visible: CustomerUnderlyingTransactionHistories().length > 0">
                                    <tr>
                                        <td><span data-bind="text: $index() + 1"></span></td>
                                        <td><span data-bind="text: ApplicationID"></span></td>
                                        <td align="center"><span data-bind="text: $root.LocalDate(ApplicationDate, true, false)"></span></td>
                                        <td><span data-bind="text: ProductCode"></span></td>
                                        <td align="center"><span data-bind="text: CurrencyCode"></span></td>
                                        <td align="right"><span data-bind="text: formatNumber(Amount)"></span></td>
                                        <td align="right"><span data-bind="text: formatNumber(AmountUSD)"></span></td>
                                        <td align="right"><span data-bind="text: formatNumber(UtilizedAmountInUSD)"></span></td>
                                        <td><span data-bind="text: TransactionStatus"></span></td>
                                    </tr>
                                </tbody>
                                <tbody data-bind="visible: CustomerUnderlyingTransactionHistories().length == 0">
                                    <tr>
                                        <td colspan="9" class="text-center">No entries available.
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <!-- modal footer start -->
                    <div class="modal-footer">
                        <button class="btn btn-sm btn-primary" data-bind="click: $root.save_u, visible: $root.IsNewDataUnderlying() && $root.IsEditCustomerUnderlying() && !$root.IsViewWorkflow(), disable: !$root.IsEditTableUnderlying()">
                            <i class="icon-save"></i>
                            Save
                        </button>
                        <button class="btn btn-sm btn-primary" data-bind="click: $root.update_u, visible: (!$root.IsNewDataUnderlying() && !$root.IsUtilize_u() && $root.IsEditCustomerUnderlying() && !$root.IsViewWorkflow())">
                            <i class="icon-edit"></i>
                            Update
                        </button>
                        <button class="btn btn-sm btn-warning" data-bind="click: $root.delete_u, visible: !$root.IsNewDataUnderlying() && !$root.IsUtilize_u() && $root.IsEditCustomerUnderlying() && !$root.IsViewWorkflow()">
                            <i class="icon-trash"></i>
                            Delete
                        </button>
                        <button class="btn btn-sm" data-dismiss="modal">
                            <i class="icon-remove"></i>
                            Cancel
                        </button>
                    </div>
                    <!-- modal footer end -->
                </div>

            </div>

        </div>
    </div>
</div>
<!-- modal form end underlying form -->

<!-- modal form start CSO Form -->
<div id="CSO-modal-form" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- modal header start -->
            <div class="modal-header">
                <h4 class="blue bigger">
                    <span data-bind="if: $root.IsNewCSOData()">Create a new Customer CSO Parameter</span>
                    <span data-bind="if: !$root.IsNewCSOData()">Modify a Customer CSO Parameter</span>
                </h4>
            </div>
            <!-- modal header end -->

            <!-- modal body start -->
            <div class="modal-body overflow-visible">
                <div class="row">
                    <div class="col-xs-12">
                        <!-- modal body form start -->
                        <div id="customer-form" class="form-horizontal" role="form">
                            <div class="space-4"></div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="code">
                                    Employee</label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <input type="text" id="employeecso" name="employeecso" data-bind="value: $root.Employee().EmployeeName, disable: $root.Readonly" class="col-xs-8" data-rule-required="true" data-rule-maxlength="255" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="code">
                                    Employee Email</label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <input type="text" id="employeeemailcso" name="employeeemailcso" data-bind="value: $root.Employee().EmployeeEmail" class="col-xs-8" data-rule-required="true" disabled="disabled" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- modal body form end -->
                    </div>
                </div>
            </div>
            <!-- modal body end -->

            <!-- modal footer start -->
            <div class="modal-footer">
                <button class="btn btn-sm btn-primary" data-bind="click: $root.save_cso, visible: $root.IsNewCSOData()">
                    <i class="icon-save"></i>
                    Save
                </button>
                <%-- <button class="btn btn-sm btn-primary" data-bind="click: $root.update_cso, visible: !$root.IsNewCSOData()">
                    <i class="icon-edit"></i>
                    Update
                </button>--%>
                <button id="btnDelete" class="btn btn-sm btn-warning" data-bind="click: $root.delete_cso, visible: $root.IsEditCSOData() && $root.IsCustomerCSOMaker()">
                    <i class="icon-trash"></i>
                    Delete
                </button>
                <button class="btn btn-sm" data-dismiss="modal">
                    <i class="icon-remove"></i>
                    Cancel
                </button>
            </div>
            <!-- modal footer end -->

        </div>
    </div>
</div>
<!-- modal form end CSO Form-->

<!-- modal form CallbackTime -->
<div id="modal-form-CallbackTime" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="0" role="dialog">
    <div class="modal-dialog" style="width: 50%">
        <div class="modal-content">
            <!-- modal header start -->
            <div class="modal-header">
                <h4 class="blue bigger">
                    <span>Call Back Time</span>
                </h4>
            </div>
            <!-- modal header end -->
            <!-- modal body start -->
            <div class="modal-body overflow-visible">
                <div class="row">
                    <div class="col-xs-12">
                        <!-- modal body form start -->
                        <div id="customer-form-callback" class="form-horizontal" role="form">

                            <div class="form-group">
                                <label class="col-sm-3 control-label bolder no-padding-right" for="CallbackContactName">
                                    Contact Name
                                </label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <input type="text" id="CallbackContactName" name="CallbackContactName" data-bind="value: $root.CallbackContactName" class="col-xs-12 col-sm-12" disabled="disabled" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label bolder no-padding-right" for="CallbackProductName">
                                    Product
                                </label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <select id="CallbackProductName" name="CallbackProductName" data-rule-required="true" data-rule-value="true" data-bind="options: $root.Products, optionsText: function (item) { if (item.Code != null) return item.Code + ' (' + item.Name + ')' }, optionsValue: 'ID', optionsCaption: 'Please Select...', value: $root.CallbackProductID, disable: $root.CallbackReadOnly"></select>
                                        <label class="control-label bolder text-danger" for="CallbackProductName">*</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label bolder no-padding-right" for="CallbackTimePicker">
                                    Call Back Time
                                </label>
                                <div class="col-sm-4" data-bind="visible: !$root.CallbackReadOnly()">
                                    <div class="clearfix">
                                        <div class="input-group bootstrap-timepicker">
                                            <input type="text" id="CallbackTimePicker" class="form-control time-picker" data-bind="timepicker: true, disable: $root.CallbackReadOnly" data-rule-required="true" data-rule-value="true" />
                                            <span class="input-group-addon">
                                                <i class="icon-time bigger-110"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6" data-bind="visible: $root.CallbackReadOnly()">
                                    <input type="text" id="CallbackTime" class="col-sm-9" data-bind="value: $root.CallbackTime" disabled="disabled" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label bolder no-padding-right" for="IsUtc">
                                    Is UTC
                                </label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <input id="CallbackIsUTC" class="ace ace-switch ace-switch-6" type="checkbox" data-bind="checked: $root.CallbackIsUTC, disable: $root.CallbackReadOnly" />
                                        <span class="lbl"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label bolder no-padding-right">
                                    Remark
                                </label>

                                <div class="col-sm-9">
                                    <textarea name="CallbackRemark" id="remarks" class="col-sm-12" rows="5" data-bind="value: $root.CallbackRemark, disable: $root.CallbackReadOnly"></textarea>
                                </div>
                            </div>

                        </div>
                        <!-- modal body form end -->
                    </div>
                </div>
            </div>
            <!-- modal body end -->
            <!-- modal footer start -->
            <div class="modal-footer">
                <button class="btn btn-sm btn-primary" data-bind="click: $root.SaveCallBackTime, visible: !$root.CallbackReadOnly()">
                    <i class="icon-save"></i>
                    Save
                </button>
                <button class="btn btn-sm" data-bind="click: $root.CloseCallBackTime">
                    <i class="icon-remove"></i>
                    Cancel
                </button>
            </div>
            <!-- modal footer end -->

        </div>
    </div>
</div>
<!-- modal form callbacktime end-->

<%--hidayat dialog POAEmail--%>
<div id="poaEmail-modal-form" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- modal header start -->
            <div class="modal-header">
                <h4 class="blue bigger">
                    <span data-bind="if: $root.IsNewData_POAE()">Create a new POA Email</span>
                    <span data-bind="if: !$root.IsNewData_POAE()">Modify a POA Email</span>
                </h4>
            </div>
            <!-- modal header end -->
            <div class="modal-body overflow-visible">
                <div class="row">
                    <div class="col-xs-12">
                        <!-- modal body form start -->
                        <div class="form-horizontal" role="form">
                            <div class="space-4"></div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="code">POA Email</label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <input type="text" class="col-xs-8" data-bind="value: $root.POAEmail_POAE, attr: { ID: 'POAEmail' }" data-rule-required="true" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="code">POA Email Product</label>
                                <div class="col-sm-9">
                                    <table class="table table-striped table-bordered table-hover dataTable">
                                        <thead>
                                            <tr>
                                                <th>POA Email Product Name</th>
                                                <th>
                                                    <input type="checkbox" data-bind="event: { change: $root.POAEmailCheckAll }, disable: $root.Readonly" value="false" id="POAEmailChkAll" /></th>
                                            </tr>
                                        </thead>
                                        <tbody data-bind="foreach: cblPOAEmailProduct" id="POAEmailTable">
                                            <tr>
                                                <td data-bind="text: ProductName"></td>
                                                <td>

                                                    <input type="checkbox" name="POAEmailChkBox" data-bind="attr: { ID: 'POAEmailcheckBox' + POAEmailProductID }, disable: $root.Readonly, event: { change: $parent.AddPOAEmail }" /></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- modal body form end -->
                    </div>
                </div>
            </div>

            <!-- modal footer start -->
            <div class="modal-footer">
                <button class="btn btn-sm btn-primary" data-bind="click: $root.save_POAE, visible: $root.IsNewData_POAE(), disable: !$root.IsEditable(), attr: { ID: 'BSave' }">
                    <i class="icon-save"></i>
                    Save
                </button>
                <button class="btn btn-sm btn-primary" data-bind="click: $root.update_POAE, visible: !$root.IsNewData_POAE(), attr: { ID: 'BUpdate' }">
                    <i class="icon-edit"></i>
                    Update                                                                                                    
                </button>
                <button class="btn btn-sm btn-warning" data-bind="click: $root.delete_POAE, visible: !$root.IsNewData_POAE(), attr: { ID: 'BDelete' }">
                    <i class="icon-trash"></i>
                    Delete
                </button>
                <button class="btn btn-sm" data-dismiss="modal" data-bind="click: $root.close_POAE">
                    <i class="icon-remove"></i>
                    Cancel
                </button>
            </div>
            <!-- modal footer end -->

        </div>
    </div>
</div>
<%--hidayat dialog POAEmail End--%>
<!--modal AccounNumber-->
<div id="AccountNumber-modal-form" class="modal fade" tabindex="-1" data-focus-on="input:first" style="display: none;" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- modal header start -->
            <div class="modal-header">
                <h4 class="blue bigger">
                    <span data-bind="if: $root.IsNewData_AccountNumber_c()">Create a Account Number</span>
                    <span data-bind="if: !$root.IsNewData_AccountNumber_c()">Modify a Account Number</span>
                </h4>
            </div>
            <!-- modal header end -->
            <div class="modal-body overflow-visible">
                <div class="row">
                    <div class="col-xs-12">
                        <!-- modal body form start -->
                        <div class="form-horizontal" role="form">
                            <div class="space-4"></div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="code">Account Number</label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <input type="text" maxlength="250" class="col-xs-8" data-bind="value: $root.AccountNumber_c"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- modal body form end -->
                    </div>
                </div>
            </div>

            <!-- modal footer start -->
            <div class="modal-footer">
                <button class="btn btn-sm btn-primary" data-bind="click: $root.AccountNumberSave, visible: $root.IsNewData_AccountNumber_c(), attr: { ID: 'AccountNumberSave' }">
                    <i class="icon-save"></i>
                    Save
                </button>
                <button class="btn btn-sm btn-primary" data-bind="click: $root.AccountNumberUpdate, visible: !$root.IsNewData_AccountNumber_c(), attr: { ID: 'AccountNumberUpdate' }">
                    <i class="icon-edit"></i>
                    Update
                </button>
                <button class="btn btn-sm" data-dismiss="modal" data-bind="">
                    <i class="icon-remove"></i>
                    Cancel
                </button>
            </div>
            <!-- modal footer end -->

        </div>
    </div>
</div>
<!--modal AccounNumber-->
<!--modal POAFunction-->
<div id="POAFunction-modal-form" class="modal fade" tabindex="-1" data-focus-on="input:first" style="display: none;" role="dialog">
    <div class="modal-dialog" style="width: 70%">
        <div class="modal-content">
            <!-- modal header start -->
            <div class="modal-header">
                <h4 class="blue bigger">
                    <span data-bind="if: $root.IsNewData_POAFunction_c()">Create a POA Function</span>
                    <span data-bind="if: !$root.IsNewData_POAFunction_c()">Modify a POA Function</span>
                </h4>
            </div>
            <!-- modal header end -->
            <div class="modal-body overflow-visible">
                <div class="row">
                    <div class="col-xs-12">
                        <!-- modal body form start -->
                        <div class="form-horizontal" role="form">
                            <div class="space-4"></div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="code">POA Function</label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <select id="matrix" name="matrix" data-bind="value: POAFunctionID_c, options: ddlPOAFunction_c, optionsText: 'Description', optionsValue: 'ID', optionsCaption: 'Please Select...', event: { change: $root.OnchangePOAFunction }, disable: $root.Readonly" class="col-xs-8"></select>
                                        <label class="control-label bolder text-danger" for="POAFunctionID_c">*</label>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group" data-bind="visible: POAFunctionID_c() == 9" id="Other">
                                <label class="col-sm-3 control-label no-padding-right" for="code">POA Function Other</label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <input type="text" id="functionOther" maxlength="250" name="functionOther" autocomplete="off" data-bind="value: POAFunctionOther_c, disable: $root.Readonly" class="col-sm-8" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- modal body form end -->
                    </div>
                </div>
            </div>

            <!-- modal footer start -->
            <div class="modal-footer">
                <button class="btn btn-sm btn-primary" data-bind="click: $root.POAFunctionSave, visible: $root.IsNewData_POAFunction_c()">
                    <i class="icon-save"></i>
                    Save
                </button>
                <button class="btn btn-sm btn-primary" data-bind="click: $root.POAFunctionUpdate, visible: !$root.IsNewData_POAFunction_c()">
                    <i class="icon-edit"></i>
                    Update
                </button>
                <button class="btn btn-sm" data-dismiss="modal" data-bind="">
                    <i class="icon-remove"></i>
                    Cancel
                </button>
            </div>
            <!-- modal footer end -->

        </div>
    </div>
</div>
<!--modal POAFunction-->
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.validate.min.js"></script>
<!-- <script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/additional-methods.min.js"></script> -->
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/Knockout/knockout.mapping-latest.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/CustomerList.js"></script>

<link rel="stylesheet" media="screen" href="/SiteAssets/Scripts/handsontable/dist/handsontable.full.css">
<script src="/SiteAssets/Scripts/handsontable/dist/handsontable.full.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/parser.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/ruleJS.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/lodash.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/underscore.string.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/moment.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/numeral.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/md5.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/jstat.js"></script>
<%--<script src="/SiteAssets/Scripts/handsontable/ruleJS/formula.js"></script>

<link rel="stylesheet" media="screen" href="/SiteAssets/Scripts/handsontable/ruleJS/handsontable.formula.css">
<script src="/SiteAssets/Scripts/handsontable/ruleJS/handsontable.formula.js"></script>--%>


<style>
    .wtHolder {
        height: 300px !important;
    }
</style>
