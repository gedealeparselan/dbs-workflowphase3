﻿using System;
using System.Collections.Generic;
using System.Linq;
using DBS.Entity;
using DBS.WebAPI.Helpers;
using System.Linq.Dynamic;

namespace DBS.WebAPI.Models
{
    #region Property
    /// <summary>
    /// Information of Human Workflow Tasks on Nintex Workflow
    /// </summary>
    public class SPUserTask
    {
        /// <summary>
        /// Contains basic information about Nintex Workflow task.
        /// </summary>
        public WorkflowContext WorkflowContext { get; set; }

        /// <summary>
        /// Contain all information about current workflow task.
        /// </summary>
        public WorkflowTask CurrentTask { get; set; }

        /// <summary>
        /// Contain all information about latest progress of workflow task.
        /// </summary>
        public WorkflowTask ProgressTask { get; set; }

        /// <summary>
        /// Transaction data
        /// </summary>
        public TransactionTaskModel Transaction { get; set; }
    }

    /*
    public class Workflow
    {
        public long ApproverId { get; set; }
        public Guid SPListID { get; set; }
        public int SPListItemID { get; set; }
        public Guid SPTaskListID { get; set; }
        public int SPTaskListItemID { get; set; }
        public string WorkflowInitiator { get; set; }
        public Guid WorkflowInstanceID { get; set; }
        public string WorkflowName { get; set; }
        public DateTime WorkflowStartTime { get; set; }
        public int WorkflowState { get; set; }
        public string WorkflowStateDescription { get; set; }
        public string WorkflowTaskTitle { get; set; }
        public string WorkflowTaskActivityTitle { get; set; }
        public string WorkflowTaskUser { get; set; }
        public bool WorkflowTaskIsSPGroup { get; set; }
        public DateTime WorkflowTaskEntryTime { get; set; }
        public DateTime WorkflowTaskExitTime { get; set; }
        public int WorkflowOutcome { get; set; }
        public string WorkflowOutcomeDescription { get; set; }
        public int WorkflowCustomOutcome { get; set; }
        public string WorkflowCustomOutcomeDescription { get; set; }
        public string WorkflowComments { get; set; }
        public string WorkflowTaskTitleLatest { get; set; }
        public string WorkflowTaskActivityTitleLatest { get; set; }
        public string WorkflowTaskUserLatest { get; set; }
        public DateTime WorkflowTaskEntryTimeLatest { get; set; }
        public DateTime WorkflowTaskExitTimeLatest { get; set; }
        public int WorkflowOutcomeLatest { get; set; }
        public string WorkflowOutcomeDescriptionLatest { get; set; }
        public int WorkflowCustomOutcomeLatest { get; set; }
        public string WorkflowCustomOutcomeDescriptionLatest { get; set; }
        public string WorkflowCommentsLatest { get; set; }
    }*/

    public class WorkflowContext
    {
        /// <summary>
        /// 
        /// </summary>
        public long ApproverID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Guid SPWebID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Guid SPSiteID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Guid SPListID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int SPListItemID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Guid SPTaskListID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int SPTaskListItemID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Initiator { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Guid WorkflowInstanceID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Guid WorkflowID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string WorkflowName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? StartTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int StateID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string StateDescription { get; set; }

        /// <summary>
        /// 
        /// </summary>

        public string TransactionStatus { get; set; }

        /// <summary>
        /// Current user have an authorization to completing the task.
        /// true : if the task is assinged to current user or current user groups
        /// false : if the task is not assigned to current user or current user groups
        /// </summary>
        public bool IsAuthorized { get; set; }

    }

    public class WorkflowTask
    {
        /// <summary>
        /// Nintex Task Type
        /// </summary>
        public int TaskTypeID { get; set; }

        /// <summary>
        /// Nintex Task Type Description
        /// </summary>
        public string TaskTypeDescription { get; set; }

        /// <summary>
        /// Nintex task title. Reference from Sharepoint Task List title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Activity title from current nintex task.
        /// </summary>
        public string ActivityTitle { get; set; }

        /// <summary>
        /// Sharepoint User or Group that was assigned for current nintex task.
        /// </summary>
        public string User { get; set; }

        /// <summary>
        /// Current user is a Sharepoint user or Group.
        /// </summary>
        public bool IsSPGroup { get; set; }

        /// <summary>
        /// Time when the task is received to assigned user / group.
        /// </summary>
        public DateTime? EntryTime { get; set; }

        /// <summary>
        ///  Time when the task is completed by assigned user / group.
        /// </summary>
        public DateTime? ExitTime { get; set; }

        /// <summary>
        /// Outcome id of current task.
        /// </summary>
        public int? OutcomeID { get; set; }

        /// <summary>
        /// Outcome Description of current task.
        /// </summary>
        public string OutcomeDescription { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? CustomOutcomeID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CustomOutcomeDescription { get; set; }

        /// <summary>
        /// User comment when completing the task.
        /// </summary>
        public string Comments { get; set; }

        public string Approver { get; set; }
        public string FinalOutcome { get; set; }
    }

    public class WorkflowTaskLatest
    {
        /// <summary>
        /// Nintex Task Type
        /// </summary>
        public int TaskTypeIDLatest { get; set; }

        /// <summary>
        /// Nintex Task Type Description
        /// </summary>
        public string TaskTypeDescriptionLatest { get; set; }

        /// <summary>
        /// Nintex task title. Reference from Sharepoint Task List title.
        /// </summary>
        public string TitleLatest { get; set; }

        /// <summary>
        /// Activity title from current nintex task.
        /// </summary>
        public string ActivityTitleLatest { get; set; }

        /// <summary>
        /// Sharepoint User or Group that was assigned for current nintex task.
        /// </summary>
        public string UserLatest { get; set; }

        /// <summary>
        /// Current user is a Sharepoint user or Group.
        /// </summary>
        public bool IsSPGroupLatest { get; set; }

        /// <summary>
        /// Time when the task is received to assigned user / group.
        /// </summary>
        public DateTime? EntryTimeLatest { get; set; }

        /// <summary>
        ///  Time when the task is completed by assigned user / group.
        /// </summary>
        public DateTime? ExitTimeLatest { get; set; }

        /// <summary>
        /// Outcome id of current task.
        /// </summary>
        public int? OutcomeIDLatest { get; set; }

        /// <summary>
        /// Outcome Description of current task.
        /// </summary>
        public string OutcomeDescriptionLatest { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? CustomOutcomeIDLatest { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CustomOutcomeDescriptionLatest { get; set; }

        /// <summary>
        /// User comment when completing the task.
        /// </summary>
        public string CommentsLatest { get; set; }
    }

    public class SPUserTaskRow : SPUserTask
    {
        public int RowID { get; set; }
    }

    public class NintexTaskRow : V_NintexTasks
    {
        public int RowID { get; set; }
    }

    public class SPUserTaskGrid : Grid
    {
        public IList<SPUserTaskRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class SPUserTaskFilter : Filter { }
    #endregion

    #region Interface
    public interface ISPUserTaskRepository : IDisposable
    {
        /// <summary>
        /// Get all user tasks by multiple worflow id.
        /// </summary>
        /// <param name="webID">Sharepoint WebID</param>
        /// <param name="siteID">Sharepoint SiteID</param>
        /// <param name="workflowIDs">Nintex Workflow ID Collection</param>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortOrder"></param>
        /// <param name="filters"></param>
        /// <param name="userTaskGrid"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        bool GetUserTasks(Guid webID, Guid siteID, IList<Guid> workflowIDs, IList<string> state, IList<string> outcome, IList<string> customOutcome, bool showContribute, bool showActiveTask, int? page, int? size, string sortColumn, string sortOrder, IList<SPUserTaskFilter> filters, ref SPUserTaskGrid userTaskGrid, ref string message);
        bool GetMyUserTasks(Guid webID, Guid siteID, IList<Guid> workflowIDs, IList<string> state, IList<string> outcome, IList<string> customOutcome, bool showContribute, bool showActiveTask, int? page, int? size, string sortColumn, string sortOrder, IList<SPUserTaskFilter> filters, ref SPUserTaskGrid userTaskGrid, ref string message);

    }
    #endregion

    #region Repository
    public class SPUserTaskRepository : ISPUserTaskRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);

            GC.Collect();
            GC.SuppressFinalize(this);
        }

        public bool GetUserTasks(Guid webID, Guid siteID, IList<Guid> workflowIDs, IList<string> state, IList<string> outcome, IList<string> customOutcome, bool showContribute, bool showActiveTask, int? page, int? size, string sortColumn, string sortOrder, IList<SPUserTaskFilter> filters, ref SPUserTaskGrid userTaskGrid, ref string message)
        {
            bool isSuccess = false;

            // disable EF auto detect changes
            context.Configuration.AutoDetectChangesEnabled = false;

            try
            {
                int skip = (page.Value - 1) * size.Value;
                int take = size.Value;
                //int StateID = state.Value;
                //byte OutcomeID = (byte)outcome.Value;
                string orderBy = string.Format("{0} {1}", sortColumn, sortOrder);

                SPUserTask filter = new SPUserTask();
                filter.WorkflowContext = new WorkflowContext();
                filter.CurrentTask = new WorkflowTask();
                filter.ProgressTask = new WorkflowTask();
                filter.Transaction = new TransactionTaskModel();

                DateTime maxDateEntryTime = new DateTime();

                DateTime maxDateExitTime = new DateTime();

                if (filters != null)
                {
                    #region Transaction
                    filter.Transaction.ApplicationID = filters.Where(sp => sp.Field.Equals("ApplicationID")).Select(sp => sp.Value).SingleOrDefault();
                    filter.Transaction.Customer = filters.Where(sp => sp.Field.Equals("Customer")).Select(sp => sp.Value).SingleOrDefault();
                    filter.Transaction.Product = filters.Where(sp => sp.Field.Equals("Product")).Select(sp => sp.Value).SingleOrDefault();
                    filter.Transaction.Currency = filters.Where(sp => sp.Field.Equals("Currency")).Select(sp => sp.Value).SingleOrDefault();
                    filter.Transaction.DebitAccNumber = (string)filters.Where(sp => sp.Field.Equals("DebitAccNumber")).Select(sp => sp.Value).SingleOrDefault();

                    filter.CurrentTask.ActivityTitle = filters.Where(sp => sp.Field.Equals("ActivityTitle")).Select(sp => sp.Value).SingleOrDefault();

                    string amount = filters.Where(sp => sp.Field.Equals("Amount")).Select(sp => sp.Value).SingleOrDefault();
                    if (!string.IsNullOrEmpty(amount))
                    {
                        decimal valueAmount;
                        bool resultAmount = decimal.TryParse(amount, out valueAmount);
                        if (resultAmount)
                        {
                            filter.Transaction.Amount = valueAmount;
                        }
                        else
                        {
                            userTaskGrid.Page = page.Value;
                            userTaskGrid.Size = size.Value;
                            userTaskGrid.Total = 0;
                            return true;
                        }
                    }

                    string amountUSD = filters.Where(sp => sp.Field.Equals("AmountUSD")).Select(sp => sp.Value).SingleOrDefault();
                    if (!string.IsNullOrEmpty(amountUSD))
                    {
                        filter.Transaction.AmountUSD = Decimal.Parse(amountUSD);
                    }
                    string fxTransaction = filters.Where(sp => sp.Field.Equals("IsFXTransaction")).Select(sp => sp.Value).SingleOrDefault();
                    if (!string.IsNullOrEmpty(fxTransaction))
                    {
                        bool valueFx;
                        bool resultFx = bool.TryParse(fxTransaction, out valueFx);
                        if (resultFx)
                        {
                            filter.Transaction.IsFXTransaction = valueFx;
                        }
                        else
                        {
                            userTaskGrid.Page = page.Value;
                            userTaskGrid.Size = size.Value;
                            userTaskGrid.Total = 0;
                            return true;
                        }
                    }

                    string topUrgent = filters.Where(sp => sp.Field.Equals("IsTopUrgent")).Select(sp => sp.Value).SingleOrDefault();
                    /*if (!string.IsNullOrEmpty(topUrgent))
                    {
                        bool valueFx;
                        bool resultFx = bool.TryParse(topUrgent, out valueFx);
                        if (resultFx)
                        {
                            filter.Transaction.IsTopUrgent = valueFx;
                        }
                        else
                        {
                            userTaskGrid.Page = page.Value;
                            userTaskGrid.Size = size.Value;
                            userTaskGrid.Total = 0;
                            return true;

                        }
                    } */

                    
                    filter.CurrentTask.User = filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
                    filter.ProgressTask.User = filters.Where(a => a.Field.Equals("UserApprover")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("EntryTime")).Any())
                    {
                        filter.CurrentTask.EntryTime = DateTime.Parse((string)filters.Where(a => a.Field.Equals("EntryTime")).Select(a => a.Value).SingleOrDefault());
                        maxDateEntryTime = filter.CurrentTask.EntryTime.Value.AddDays(1);
                        //minDateEntryTime = filter.CurrentTask.EntryTime.Value;
                    }

                    if (filters.Where(a => a.Field.Equals("ExitTime")).Any())
                    {
                        filter.CurrentTask.ExitTime = DateTime.Parse(filters.Where(a => a.Field.Equals("ExitTime")).Select(a => a.Value).SingleOrDefault());
                        maxDateExitTime = filter.CurrentTask.ExitTime.Value.AddDays(1);
                    }
                    #endregion
                }

                string userName = currentUser.GetCurrentUser().LoginName;
                string[] roles = currentUser.GetCurrentUser().Roles.Select(r => r.Name).ToArray();
                Guid[] workflowID = workflowIDs.ToArray();

                var data = (//from z in context.Transactions.AsNoTracking() //-> Rizki 2014-11-25
                            from z in context.V_Transaction.AsNoTracking()
                            join a in context.V_NintexWorkflowTasks on z.WorkflowInstanceID.Value equals a.WorkflowInstanceID into ag                            
                            from a in ag.DefaultIfEmpty()
                            join b in context.Users on a.WorkflowInitiator equals b.UserName into bg
                            from b in bg.DefaultIfEmpty()
                            join c in context.Users on a.WorkflowTaskUser equals c.UserName into cg
                            from c in cg.DefaultIfEmpty()

                            // dynamic where clause
                            let isActivityTitle = !string.IsNullOrEmpty(filter.CurrentTask.ActivityTitle)
                            let isUser = !string.IsNullOrEmpty(filter.CurrentTask.User)
                            let isEntryTime = filter.CurrentTask.EntryTime.HasValue
                            let isExitTime = filter.CurrentTask.ExitTime.HasValue
                            let isUserApprover = !string.IsNullOrEmpty(filter.ProgressTask.User)

                            #region Transaction
                            let isApplicationID = !string.IsNullOrEmpty(filter.Transaction.ApplicationID)
                            let isCustomer = !string.IsNullOrEmpty(filter.Transaction.Customer)
                            let isProduct = !string.IsNullOrEmpty(filter.Transaction.Product)
                            let isCurrency = !string.IsNullOrEmpty(filter.Transaction.Currency)
                            let isAmount = filter.Transaction.Amount.HasValue
                            let isAmountUSD = filter.Transaction.AmountUSD.HasValue
                            let isFXTransaction = filter.Transaction.IsFXTransaction.HasValue
                            let isTopUrgent = filter.Transaction.IsTopUrgent.HasValue
                            let isDebitAccNumber = !string.IsNullOrEmpty(filter.Transaction.DebitAccNumber)
                            #endregion

                            where

                            a.SPWebID.Equals(webID) && a.SPSiteID.Equals(siteID) && workflowIDs.Contains(a.WorkflowID.Value) &&
                                (
                                    showContribute ?
                                        roles.Any(x => a.WorkflowTaskUserContribute.Contains(x)) ||
                                        roles.Any(x => z.InitiatorGroup.Contains(x)) ||
                                        a.WorkflowInitiator.Equals(userName) ||
                                        a.WorkflowTaskUserContribute.ToLower().Contains(userName.ToLower())

                                    : (showActiveTask ?
                                    (a.WorkflowOutcomeDescription.ToLower().Equals("pending")) &&
                                    (roles.Contains(a.WorkflowTaskUser.ToLower()) ||
                                    a.WorkflowTaskUser.Equals(userName)) &&
                                    (z.IsCanceled == false) : true)
                                ) &&

                                (state.Count() > 0 ? state.Contains(a.WorkflowStateDescription.ToLower()) : true) &&

                                (customOutcome.Count() > 0 ? 
                                (a.WorkflowOutcomeDescription.ToLower().Equals("custom") ? 
                                customOutcome.Contains(a.WorkflowCustomOutcomeDescription) : true) : true) &&
                            (isActivityTitle ? a.WorkflowTaskActivityTitle.Contains(filter.CurrentTask.ActivityTitle) : true) &&
                            (isUser ? a.WorkflowTaskUser.Contains(filter.CurrentTask.User) : true) &&
                            (isUserApprover ? (string.IsNullOrEmpty(a.WorkflowLastDisplayName) ? (z.UpdateDate == null ? z.CreateBy : z.UpdateBy) : a.WorkflowLastDisplayName).Contains(filter.ProgressTask.User) : true) &&
                            (isEntryTime ? (a.WorkflowTaskEntryTime >= filter.CurrentTask.EntryTime.Value && a.WorkflowTaskEntryTime <= maxDateEntryTime) : true) &&
                            (isExitTime ? (a.WorkflowTaskExitTime.Value >= filter.CurrentTask.ExitTime.Value && a.WorkflowTaskExitTime.Value <= maxDateExitTime) : true) &&

                            #region Transaction
                                    (isApplicationID ? z.ApplicationID.ToLower().Contains(filter.Transaction.ApplicationID) : true) &&
                                    (isCustomer ? z.CustomerName.ToLower().Contains(filter.Transaction.Customer.ToLower()) : true) &&
                                    (isProduct ? z.ProductName.Contains(filter.Transaction.Product) : true) &&
                                    (isCurrency ? z.CurrencyCode.Contains(filter.Transaction.Currency) : true) &&
                                    (isAmount ? z.Amount.Equals(filter.Transaction.Amount.Value) : true) &&
                                    (isAmountUSD ? z.AmountUSD.Equals(filter.Transaction.AmountUSD.Value) : true) &&
                                    (isFXTransaction ? (z.CurrencyCode != "IDR" && z.DebitCurrencyCode == "IDR").Equals(filter.Transaction.IsFXTransaction.Value) : true) &&
                                    (isTopUrgent ? z.IsTopUrgent.Equals(filter.Transaction.IsTopUrgent.Value) : true) &&
                                    (isDebitAccNumber ? z.AccountNumber.Contains(filter.Transaction.DebitAccNumber) : true)

                            #endregion

                            select new
                            {
                                #region Workflow Context
                                SPWebID = a.SPWebID,
                                SPSiteID = a.SPSiteID,
                                SPListID = a.SPListID,
                                SPListItemID = a.SPListItemID,
                                SPTaskListID = a.SPTaskListID,
                                SPTaskListItemID = a.SPTaskListItemID,
                                ApproverID = a.WorkflowApproverID,
                                Initiator = a.WorkflowInitiator,
                                InitiatorDisplayName = b.UserDisplayName,
                                StartTime = a.WorkflowStartTime,
                                StateID = a.WorkflowState,
                                StateDescription = a.WorkflowStateDescription,
                                WorkflowID = a.WorkflowID,
                                WorkflowName = a.WorkflowName,
                                WorkflowInstanceID = a.WorkflowInstanceID,


                                IsAuthorized = a.WorkflowTaskIsSPGroup ? (roles.Contains(a.WorkflowTaskUser) ? true : false) : (a.WorkflowTaskUser.Equals(userName) ? true : false),
                                #endregion

                                #region Workflow Task
                                TaskType = a.WorkflowTaskType,
                                TaskTypeDescription = a.WorkflowTaskTypeDescription,
                                Title = a.WorkflowTaskTitle,
                                ActivityTitle = a.WorkflowTaskActivityTitle,
                                User = a.WorkflowTaskUser,
                                UserDisplayName = c.UserDisplayName,
                                IsSPGroup = a.WorkflowTaskIsSPGroup,
                                EntryTime = a.WorkflowTaskEntryTime,
                                ExitTime = a.WorkflowTaskExitTime,
                                OutcomeID = a.WorkflowOutcome,
                                OutcomeDescription = a.WorkflowOutcomeDescription,
                                CustomOutcomeID = a.WorkflowCustomOutcome,
                                CustomOutcomeDescription = a.WorkflowCustomOutcomeDescription,
                                Comments = a.WorkflowComments,
                                Approver = a.WorkflowDisplayName,
                                //FinalOutcome = a.WorkflowFinalOutcome,
                                #endregion

                                #region Workflow Task Latest
                                ActivityTitleLatest = a.WorkflowLastActivityTitle,
                                CommentsLatest = a.WorkflowLastComments,
                                CustomOutcomeIDLatest = a.WorkflowLastCustomOutcome,
                                CustomOutcomeDescriptionLatest = a.WorkflowLastCustomOutcomeDescription,
                                UserDisplayNameLatest = a.WorkflowLastDisplayName,
                                OutcomeIDLatest = a.WorkflowLastOutcome,
                                OutcomeDescriptionLatest = a.WorkflowLastOutcomeDescription,
                                IsSPGroupLatest = a.WorkflowLastTaskIsSPGroup,
                                UserLatest = a.WorkflowLastTaskUser,
                                EntryTimeLatest = a.WorkflowLastTaskEntryTime,
                                ExitTimeLatest = a.WorkflowLastTaskExitTime,
                                #endregion

                                #region Transaction
                                ID = z.TransactionID,

                                ApplicationID = z.ApplicationID,
                                AccountNumber = z.AccountNumber,


                                //Customer = z.Customer.CustomerName,
                                Customer = z.CustomerName,
                                //Product = z.Product.ProductName,
                                Product = z.ProductName,
                                //Currency = z.Currency.CurrencyCode,
                                Currency = z.CurrencyCode,
                                Amount = z.Amount,
                                AmountUSD = z.AmountUSD,
                                IsTopUrgent = z.IsTopUrgent,
                                //IsFXTransaction = (z.Currency.CurrencyCode != "IDR" && z.CustomerAccount.Currency.CurrencyCode == "IDR") ? true: false,
                                IsFXTransaction = (z.CurrencyCode != "IDR" && z.DebitCurrencyCode == "IDR") ? true : false,
                                LastModifiedBy = z.UpdateDate == null ? z.CreateBy : z.UpdateBy,
                                LastModifiedDate = z.UpdateDate == null ? z.CreateDate : z.UpdateDate.Value,

                                #endregion
                            });

                userTaskGrid.Page = page.Value;
                userTaskGrid.Size = size.Value;
                userTaskGrid.Total = data.Count();
                userTaskGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };

                userTaskGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                    .Select((a, i) => new SPUserTaskRow()
                    {
                        RowID = i + 1,

                        #region Workflow Context
                        WorkflowContext = new WorkflowContext()
                        {
                            SPWebID = a.SPWebID,
                            SPSiteID = a.SPSiteID,
                            SPListID = a.SPListID,
                            SPListItemID = a.SPListItemID,
                            SPTaskListID = a.SPTaskListID.Value,
                            SPTaskListItemID = a.SPTaskListItemID.Value,
                            Initiator = a.InitiatorDisplayName, //a.WorkflowInitiator,
                            ApproverID = a.ApproverID,
                            StartTime = a.StartTime,
                            StateID = a.StateID.Value,
                            StateDescription = a.StateDescription,
                            WorkflowInstanceID = a.WorkflowInstanceID,
                            WorkflowID = a.WorkflowID.Value,
                            WorkflowName = a.WorkflowName,
                            IsAuthorized = a.IsAuthorized
                        },
                        #endregion

                        #region Workflow Task
                        CurrentTask = new WorkflowTask()
                        {
                            TaskTypeID = a.TaskType.Value,
                            TaskTypeDescription = a.TaskTypeDescription,
                            Title = a.Title,
                            ActivityTitle = a.ActivityTitle,
                            User = a.IsSPGroup ? a.User : a.UserDisplayName, //a.WorkflowTaskUser,
                            IsSPGroup = a.IsSPGroup,
                            EntryTime = a.EntryTime,
                            ExitTime = a.ExitTime,
                            OutcomeID = a.OutcomeID,
                            OutcomeDescription = a.OutcomeDescription,
                            CustomOutcomeID = a.CustomOutcomeID,
                            CustomOutcomeDescription = a.CustomOutcomeDescription,
                            Comments = a.Comments,
                            Approver = a.Approver,
                            //FinalOutcome = a.FinalOutcome
                        },
                        #endregion

                        #region Workflow Task Progress
                        ProgressTask = new WorkflowTask()
                        {
                            ActivityTitle = a.ActivityTitleLatest,
                            User = a.IsSPGroupLatest.HasValue ? (a.IsSPGroupLatest.Value ? a.UserLatest : a.UserDisplayNameLatest) : a.LastModifiedBy,
                            IsSPGroup = a.IsSPGroupLatest.HasValue ? a.IsSPGroupLatest.Value : false,
                            EntryTime = a.EntryTimeLatest.HasValue ? a.EntryTimeLatest : a.LastModifiedDate,
                            ExitTime = a.ExitTimeLatest.HasValue ? a.ExitTimeLatest : a.LastModifiedDate,
                            OutcomeID = a.OutcomeIDLatest,
                            OutcomeDescription = string.IsNullOrEmpty(a.OutcomeDescriptionLatest) ? "Submit Transaction" : a.OutcomeDescriptionLatest,
                            CustomOutcomeID = a.CustomOutcomeIDLatest,
                            CustomOutcomeDescription = a.CustomOutcomeDescriptionLatest,
                            Comments = a.CommentsLatest,
                            Approver = string.IsNullOrEmpty(a.UserDisplayNameLatest) ? a.LastModifiedBy : a.UserDisplayNameLatest
                        },
                        #endregion

                        #region Transaction
                        Transaction = new TransactionTaskModel()
                        {
                            ID = a.ID,
                            ApplicationID = a.ApplicationID,
                            Customer = a.Customer,
                            Product = a.Product,
                            Currency = a.Currency,
                            Amount = a.Amount,
                            AmountUSD = a.AmountUSD,
                            IsTopUrgent = a.IsTopUrgent,
                            IsFXTransaction = a.IsFXTransaction,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate,
                            DebitAccNumber = a.AccountNumber
                        }
                        #endregion
                    })
                    .Skip(skip)
                    .Take(take)
                    .ToList();

                data = null;
                filter = null;
                filters = null;
                roles = null;
                workflowID = null;

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.StackTrace.ToString();
            }
            finally
            {
                // enable EF auto detect changes
                context.Configuration.AutoDetectChangesEnabled = true;
            }

            return isSuccess;
        }
        public bool GetMyUserTasks(Guid webID, Guid siteID, IList<Guid> workflowIDs, IList<string> state, IList<string> outcome, IList<string> customOutcome, bool showContribute, bool showActiveTask, int? page, int? size, string sortColumn, string sortOrder, IList<SPUserTaskFilter> filters, ref SPUserTaskGrid userTaskGrid, ref string message)
        {
            bool isSuccess = false;

            // disable EF auto detect changes
            context.Configuration.AutoDetectChangesEnabled = false;

            try
            {
                int skip = (page.Value - 1) * size.Value;
                int take = size.Value;
                //int StateID = state.Value;
                //byte OutcomeID = (byte)outcome.Value;
                string orderBy = string.Format("{0} {1}", sortColumn, sortOrder);

                SPUserTask filter = new SPUserTask();
                filter.WorkflowContext = new WorkflowContext();
                filter.CurrentTask = new WorkflowTask();
                filter.ProgressTask = new WorkflowTask();
                filter.Transaction = new TransactionTaskModel();

                DateTime maxDateEntryTime = new DateTime();

                DateTime maxDateExitTime = new DateTime();

                if (filters != null)
                {
                    #region Transaction
                    filter.Transaction.ApplicationID = filters.Where(sp => sp.Field.Equals("ApplicationID")).Select(sp => sp.Value).SingleOrDefault();
                    filter.Transaction.Customer = filters.Where(sp => sp.Field.Equals("Customer")).Select(sp => sp.Value).SingleOrDefault();
                    filter.Transaction.Product = filters.Where(sp => sp.Field.Equals("Product")).Select(sp => sp.Value).SingleOrDefault();
                    filter.Transaction.Currency = filters.Where(sp => sp.Field.Equals("Currency")).Select(sp => sp.Value).SingleOrDefault();
                    filter.Transaction.DebitAccNumber = (string)filters.Where(sp => sp.Field.Equals("DebitAccNumber")).Select(sp => sp.Value).SingleOrDefault();

                    filter.CurrentTask.ActivityTitle = filters.Where(sp => sp.Field.Equals("ActivityTitle")).Select(sp => sp.Value).SingleOrDefault();

                    string amount = filters.Where(sp => sp.Field.Equals("Amount")).Select(sp => sp.Value).SingleOrDefault();
                    if (!string.IsNullOrEmpty(amount))
                    {
                        decimal valueAmount;
                        bool resultAmount = decimal.TryParse(amount, out valueAmount);
                        if (resultAmount)
                        {
                            filter.Transaction.Amount = valueAmount;
                        }
                        else
                        {
                            userTaskGrid.Page = page.Value;
                            userTaskGrid.Size = size.Value;
                            userTaskGrid.Total = 0;
                            return true;
                        }
                    }

                    string amountUSD = filters.Where(sp => sp.Field.Equals("AmountUSD")).Select(sp => sp.Value).SingleOrDefault();
                    if (!string.IsNullOrEmpty(amountUSD))
                    {
                        filter.Transaction.AmountUSD = Decimal.Parse(amountUSD);
                    }
                    string fxTransaction = filters.Where(sp => sp.Field.Equals("IsFXTransaction")).Select(sp => sp.Value).SingleOrDefault();
                    if (!string.IsNullOrEmpty(fxTransaction))
                    {
                        bool valueFx;
                        bool resultFx = bool.TryParse(fxTransaction, out valueFx);
                        if (resultFx)
                        {
                            filter.Transaction.IsFXTransaction = valueFx;
                        }
                        else
                        {
                            userTaskGrid.Page = page.Value;
                            userTaskGrid.Size = size.Value;
                            userTaskGrid.Total = 0;
                            return true;
                        }
                    }

                    string topUrgent = filters.Where(sp => sp.Field.Equals("IsTopUrgent")).Select(sp => sp.Value).SingleOrDefault();
                    /*if (!string.IsNullOrEmpty(topUrgent))
                    {
                        bool valueFx;
                        bool resultFx = bool.TryParse(topUrgent, out valueFx);
                        if (resultFx)
                        {
                            filter.Transaction.IsTopUrgent = valueFx;
                        }
                        else
                        {
                            userTaskGrid.Page = page.Value;
                            userTaskGrid.Size = size.Value;
                            userTaskGrid.Total = 0;
                            return true;

                        }
                    } */


                    filter.CurrentTask.User = filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
                    filter.ProgressTask.User = filters.Where(a => a.Field.Equals("UserApprover")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("EntryTime")).Any())
                    {
                        filter.CurrentTask.EntryTime = DateTime.Parse((string)filters.Where(a => a.Field.Equals("EntryTime")).Select(a => a.Value).SingleOrDefault());
                        maxDateEntryTime = filter.CurrentTask.EntryTime.Value.AddDays(1);
                        //minDateEntryTime = filter.CurrentTask.EntryTime.Value;
                    }

                    if (filters.Where(a => a.Field.Equals("ExitTime")).Any())
                    {
                        filter.CurrentTask.ExitTime = DateTime.Parse(filters.Where(a => a.Field.Equals("ExitTime")).Select(a => a.Value).SingleOrDefault());
                        maxDateExitTime = filter.CurrentTask.ExitTime.Value.AddDays(1);
                    }
                    #endregion
                }

                string userName = currentUser.GetCurrentUser().LoginName;
                string[] roles = currentUser.GetCurrentUser().Roles.Select(r => r.Name).ToArray();
                Guid[] workflowID = workflowIDs.ToArray();

                var data = (//from z in context.Transactions.AsNoTracking() //-> Rizki 2014-11-25
                            from z in context.V_Transaction.AsNoTracking()
                            //join a in context.V_NintexWorkflowTasks on z.WorkflowInstanceID.Value equals a.WorkflowInstanceID into ag
                            // Update to my task #reizvan20150506
                            join a in context.V_NintexWorkflowMyTasks on z.WorkflowInstanceID.Value equals a.WorkflowInstanceID into ag
                            from a in ag.DefaultIfEmpty()
                            join b in context.Users on a.WorkflowInitiator equals b.UserName into bg
                            from b in bg.DefaultIfEmpty()
                            join c in context.Users on a.WorkflowTaskUser equals c.UserName into cg
                            from c in cg.DefaultIfEmpty()

                            // dynamic where clause
                            let isActivityTitle = !string.IsNullOrEmpty(filter.CurrentTask.ActivityTitle)
                            let isUser = !string.IsNullOrEmpty(filter.CurrentTask.User)
                            let isEntryTime = filter.CurrentTask.EntryTime.HasValue
                            let isExitTime = filter.CurrentTask.ExitTime.HasValue
                            let isUserApprover = !string.IsNullOrEmpty(filter.ProgressTask.User)

                            #region Transaction
                            let isApplicationID = !string.IsNullOrEmpty(filter.Transaction.ApplicationID)
                            let isCustomer = !string.IsNullOrEmpty(filter.Transaction.Customer)
                            let isProduct = !string.IsNullOrEmpty(filter.Transaction.Product)
                            let isCurrency = !string.IsNullOrEmpty(filter.Transaction.Currency)
                            let isAmount = filter.Transaction.Amount.HasValue
                            let isAmountUSD = filter.Transaction.AmountUSD.HasValue
                            let isFXTransaction = filter.Transaction.IsFXTransaction.HasValue
                            let isTopUrgent = filter.Transaction.IsTopUrgent.HasValue
                            let isDebitAccNumber = !string.IsNullOrEmpty(filter.Transaction.DebitAccNumber)
                            #endregion

                            where

                            a.SPWebID.Equals(webID) && a.SPSiteID.Equals(siteID) && workflowIDs.Contains(a.WorkflowID.Value) &&
                                (
                                    showContribute ?
                                        roles.Any(x => a.WorkflowTaskUserContribute.Contains(x)) ||
                                        roles.Any(x => z.InitiatorGroup.Contains(x)) ||
                                        a.WorkflowInitiator.Equals(userName)

                                    : (showActiveTask ?
                                    (a.WorkflowOutcomeDescription.ToLower().Equals("pending")) &&
                                    (roles.Contains(a.WorkflowTaskUser.ToLower()) ||
                                    a.WorkflowTaskUser.Equals(userName)) &&
                                    (z.IsCanceled == false) : true)
                                ) &&

                                (state.Count() > 0 ? state.Contains(a.WorkflowStateDescription.ToLower()) : true) &&

                                (customOutcome.Count() > 0 ?
                                (a.WorkflowOutcomeDescription.ToLower().Equals("custom") ?
                                customOutcome.Contains(a.WorkflowCustomOutcomeDescription) : true) : true) &&
                            (isActivityTitle ? a.WorkflowTaskActivityTitle.Contains(filter.CurrentTask.ActivityTitle) : true) &&
                            (isUser ? a.WorkflowTaskUser.Contains(filter.CurrentTask.User) : true) &&
                            (isUserApprover ? (string.IsNullOrEmpty(a.WorkflowLastDisplayName) ? (z.UpdateDate == null ? z.CreateBy : z.UpdateBy) : a.WorkflowLastDisplayName).Contains(filter.ProgressTask.User) : true) &&
                            (isEntryTime ? (a.WorkflowTaskEntryTime >= filter.CurrentTask.EntryTime.Value && a.WorkflowTaskEntryTime <= maxDateEntryTime) : true) &&
                            (isExitTime ? (a.WorkflowTaskExitTime.Value >= filter.CurrentTask.ExitTime.Value && a.WorkflowTaskExitTime.Value <= maxDateExitTime) : true) &&

                            #region Transaction
 (isApplicationID ? z.ApplicationID.ToLower().Contains(filter.Transaction.ApplicationID) : true) &&
                                    (isCustomer ? z.CustomerName.ToLower().Contains(filter.Transaction.Customer.ToLower()) : true) &&
                                    (isProduct ? z.ProductName.Contains(filter.Transaction.Product) : true) &&
                                    (isCurrency ? z.CurrencyCode.Contains(filter.Transaction.Currency) : true) &&
                                    (isAmount ? z.Amount.Equals(filter.Transaction.Amount.Value) : true) &&
                                    (isAmountUSD ? z.AmountUSD.Equals(filter.Transaction.AmountUSD.Value) : true) &&
                                    (isFXTransaction ? (z.CurrencyCode != "IDR" && z.DebitCurrencyCode == "IDR").Equals(filter.Transaction.IsFXTransaction.Value) : true) &&
                                    (isTopUrgent ? z.IsTopUrgent.Equals(filter.Transaction.IsTopUrgent.Value) : true) &&
                                    (isDebitAccNumber ? z.AccountNumber.Contains(filter.Transaction.DebitAccNumber) : true)

                            #endregion

                            select new
                            {
                                #region Workflow Context
                                SPWebID = a.SPWebID,
                                SPSiteID = a.SPSiteID,
                                SPListID = a.SPListID,
                                SPListItemID = a.SPListItemID,
                                SPTaskListID = a.SPTaskListID,
                                SPTaskListItemID = a.SPTaskListItemID,
                                ApproverID = a.WorkflowApproverID,
                                Initiator = a.WorkflowInitiator,
                                InitiatorDisplayName = b.UserDisplayName,
                                StartTime = a.WorkflowStartTime,
                                StateID = a.WorkflowState,
                                StateDescription = a.WorkflowStateDescription,
                                WorkflowID = a.WorkflowID,
                                WorkflowName = a.WorkflowName,
                                WorkflowInstanceID = a.WorkflowInstanceID,


                                IsAuthorized = a.WorkflowTaskIsSPGroup ? (roles.Contains(a.WorkflowTaskUser) ? true : false) : (a.WorkflowTaskUser.Equals(userName) ? true : false),
                                #endregion

                                #region Workflow Task
                                TaskType = a.WorkflowTaskType,
                                TaskTypeDescription = a.WorkflowTaskTypeDescription,
                                Title = a.WorkflowTaskTitle,
                                ActivityTitle = a.WorkflowTaskActivityTitle,
                                User = a.WorkflowTaskUser,
                                UserDisplayName = c.UserDisplayName,
                                IsSPGroup = a.WorkflowTaskIsSPGroup,
                                EntryTime = a.WorkflowTaskEntryTime,
                                ExitTime = a.WorkflowTaskExitTime,
                                OutcomeID = a.WorkflowOutcome,
                                OutcomeDescription = a.WorkflowOutcomeDescription,
                                CustomOutcomeID = a.WorkflowCustomOutcome,
                                CustomOutcomeDescription = a.WorkflowCustomOutcomeDescription,
                                Comments = a.WorkflowComments,
                                Approver = a.WorkflowDisplayName,
                                //FinalOutcome = a.WorkflowFinalOutcome,
                                #endregion

                                #region Workflow Task Latest
                                ActivityTitleLatest = a.WorkflowLastActivityTitle,
                                CommentsLatest = a.WorkflowLastComments,
                                CustomOutcomeIDLatest = a.WorkflowLastCustomOutcome,
                                CustomOutcomeDescriptionLatest = a.WorkflowLastCustomOutcomeDescription,
                                UserDisplayNameLatest = a.WorkflowLastDisplayName,
                                OutcomeIDLatest = a.WorkflowLastOutcome,
                                OutcomeDescriptionLatest = a.WorkflowLastOutcomeDescription,
                                IsSPGroupLatest = a.WorkflowLastTaskIsSPGroup,
                                UserLatest = a.WorkflowLastTaskUser,
                                EntryTimeLatest = a.WorkflowLastTaskEntryTime,
                                ExitTimeLatest = a.WorkflowLastTaskExitTime,
                                #endregion

                                #region Transaction
                                ID = z.TransactionID,

                                ApplicationID = z.ApplicationID,
                                AccountNumber = z.AccountNumber,


                                //Customer = z.Customer.CustomerName,
                                Customer = z.CustomerName,
                                //Product = z.Product.ProductName,
                                Product = z.ProductName,
                                //Currency = z.Currency.CurrencyCode,
                                Currency = z.CurrencyCode,
                                Amount = z.Amount,
                                AmountUSD = z.AmountUSD,
                                IsTopUrgent = z.IsTopUrgent,
                                //IsFXTransaction = (z.Currency.CurrencyCode != "IDR" && z.CustomerAccount.Currency.CurrencyCode == "IDR") ? true: false,
                                IsFXTransaction = (z.CurrencyCode != "IDR" && z.DebitCurrencyCode == "IDR") ? true : false,
                                LastModifiedBy = z.UpdateDate == null ? z.CreateBy : z.UpdateBy,
                                LastModifiedDate = z.UpdateDate == null ? z.CreateDate : z.UpdateDate.Value,

                                #endregion
                            });

                userTaskGrid.Page = page.Value;
                userTaskGrid.Size = size.Value;
                userTaskGrid.Total = data.Count();
                userTaskGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };

                userTaskGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                    .Select((a, i) => new SPUserTaskRow()
                    {
                        RowID = i + 1,

                        #region Workflow Context
                        WorkflowContext = new WorkflowContext()
                        {
                            SPWebID = a.SPWebID,
                            SPSiteID = a.SPSiteID,
                            SPListID = a.SPListID,
                            SPListItemID = a.SPListItemID,
                            SPTaskListID = a.SPTaskListID.Value,
                            SPTaskListItemID = a.SPTaskListItemID.Value,
                            Initiator = a.InitiatorDisplayName, //a.WorkflowInitiator,
                            ApproverID = a.ApproverID,
                            StartTime = a.StartTime,
                            StateID = a.StateID.Value,
                            StateDescription = a.StateDescription,
                            WorkflowInstanceID = a.WorkflowInstanceID,
                            WorkflowID = a.WorkflowID.Value,
                            WorkflowName = a.WorkflowName,
                            IsAuthorized = a.IsAuthorized
                        },
                        #endregion

                        #region Workflow Task
                        CurrentTask = new WorkflowTask()
                        {
                            TaskTypeID = a.TaskType.Value,
                            TaskTypeDescription = a.TaskTypeDescription,
                            Title = a.Title,
                            ActivityTitle = a.ActivityTitle,
                            User = a.IsSPGroup ? a.User : a.UserDisplayName, //a.WorkflowTaskUser,
                            IsSPGroup = a.IsSPGroup,
                            EntryTime = a.EntryTime,
                            ExitTime = a.ExitTime,
                            OutcomeID = a.OutcomeID,
                            OutcomeDescription = a.OutcomeDescription,
                            CustomOutcomeID = a.CustomOutcomeID,
                            CustomOutcomeDescription = a.CustomOutcomeDescription,
                            Comments = a.Comments,
                            Approver = a.Approver,
                            //FinalOutcome = a.FinalOutcome
                        },
                        #endregion

                        #region Workflow Task Progress
                        ProgressTask = new WorkflowTask()
                        {
                            ActivityTitle = a.ActivityTitleLatest,
                            User = a.IsSPGroupLatest.HasValue ? (a.IsSPGroupLatest.Value ? a.UserLatest : a.UserDisplayNameLatest) : a.LastModifiedBy,
                            IsSPGroup = a.IsSPGroupLatest.HasValue ? a.IsSPGroupLatest.Value : false,
                            EntryTime = a.EntryTimeLatest.HasValue ? a.EntryTimeLatest : a.LastModifiedDate,
                            ExitTime = a.ExitTimeLatest.HasValue ? a.ExitTimeLatest : a.LastModifiedDate,
                            OutcomeID = a.OutcomeIDLatest,
                            OutcomeDescription = string.IsNullOrEmpty(a.OutcomeDescriptionLatest) ? "Submit Transaction" : a.OutcomeDescriptionLatest,
                            CustomOutcomeID = a.CustomOutcomeIDLatest,
                            CustomOutcomeDescription = a.CustomOutcomeDescriptionLatest,
                            Comments = a.CommentsLatest,
                            Approver = string.IsNullOrEmpty(a.UserDisplayNameLatest) ? a.LastModifiedBy : a.UserDisplayNameLatest
                        },
                        #endregion

                        #region Transaction
                        Transaction = new TransactionTaskModel()
                        {
                            ID = a.ID,
                            ApplicationID = a.ApplicationID,
                            Customer = a.Customer,
                            Product = a.Product,
                            Currency = a.Currency,
                            Amount = a.Amount,
                            AmountUSD = a.AmountUSD,
                            IsTopUrgent = a.IsTopUrgent,
                            IsFXTransaction = a.IsFXTransaction,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate,
                            DebitAccNumber = a.AccountNumber
                        }
                        #endregion
                    })
                    .Skip(skip)
                    .Take(take)
                    .ToList();

                data = null;
                filter = null;
                filters = null;
                roles = null;
                workflowID = null;

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.StackTrace.ToString();
            }
            finally
            {
                // enable EF auto detect changes
                context.Configuration.AutoDetectChangesEnabled = true;
            }

            return isSuccess;
        }

    }
    #endregion
}