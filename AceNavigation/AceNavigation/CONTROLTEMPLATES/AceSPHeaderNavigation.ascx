﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AceSPHeaderNavigation.ascx.cs" Inherits="AceNavigation.CONTROLTEMPLATES.AceSPHeaderNavigation" %>

<ul class="nav ace-nav">
    <!--
    <li class="white">
        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
            <i class="icon-bookmark icon-animated-bell"></i>
            <span id="systemBookmarkCount" class="badge badge-grey">0</span>
        </a>
        <ul id="systemBookmark" class="pull-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
            <li class="dropdown-header">
                <i class="icon-bookmark"></i>
            </li>
        </ul>
    </li>
    <li class="white">
        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
            <i class="icon-tasks icon-animated-bell"></i>
            <span class="badge badge-headef">3</span>
        </a>
        <ul id="systemStatus" class="pull-right dropdown-navbar navbar-pink dropdown-menu dropdown-caret dropdown-close">
        </ul>
    </li>
    -->
    <li class="white">
        <a data-toggle="dropdown" href="#" class="dropdown-toggle">
            <img class="nav-user-photo" src="/_catalogs/masterpage/Ace/assets/avatars/user.jpg" alt="User's Photo" />
            <span class="user-info">
                <asp:Label ID="lblDisplayName" runat="server"></asp:Label>
            </span>
            <i class="icon-caret-down"></i>
        </a>

        <ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
            <%--<li>
                <a href="#">
                    <i class="icon-user"></i>
                    NIP : <asp:Label ID="lblNIP" runat="server"></asp:Label>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="icon-group"></i>
                    Group : <asp:Label ID="lblGroup" runat="server"></asp:Label>
                </a>
            </li>
            <li class="divider"></li>--%>
            <li>
                <%--<a id="aLogout" runat="server">--%>
                <asp:LinkButton ID="lbLogout" runat="server" OnClick="lbLogout_Click"><i class="icon-off"></i>Logout</asp:LinkButton>
                <%--</a>--%>
            </li>
        </ul>

    </li>
</ul>
