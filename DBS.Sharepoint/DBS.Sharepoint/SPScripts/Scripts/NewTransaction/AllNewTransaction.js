﻿var viewModel = null;
var ChildModels = {};

$(document).ready(function () {
    Helper.GetToken(function (token) {
        console.log("Token : " + token);
        accessToken = token;
        $.getScript("/SiteAssets/Scripts/NewTransaction/Models.js").done(function () {
            console.log("Models loaded!");
            $.getScript("/SiteAssets/Scripts/NewTransaction/ViewModel.js").done(function () {
                console.log("ViewModel loaded!");
                viewModel = new ViewModel();
                viewModel.GetPaymentStatus(function (data) {
                    viewModel.TransactionModel().ModePayment(data);

                    $box = $('#widget-box');
                    var event;
                    $box.trigger(event = $.Event('reload.ace.widget'))
                    if (event.isDefaultPrevented()) return
                    $box.blur();

                    // block enter key from user to prevent submitted data.
                    $(this).keypress(function (event) {
                        var code = event.charCode || event.keyCode;
                        if (code == 13) {
                            ShowNotification("Page Information", "Enter key is disabled for this form.", "gritter-warning", false);

                            return false;
                        }
                    });

                    // Load all templates
                    $('script[src][type="text/html"]').each(function () {
                        //alert($(this).attr("src"))
                        var src = $(this).attr("src");
                        if (src != undefined) {
                            $(this).load(src);
                        }
                    });

                    // scrollables
                    $('.slim-scroll').each(function () {
                        var $this = $(this);
                        $this.slimscroll({
                            height: $this.data('height') || 100,
                            railVisible: true,
                            alwaysVisible: true,
                            color: '#D15B47'
                        });
                    });

                    // tooltip
                    $('[data-rel=tooltip]').tooltip();

                    $.fn.ForceNumericOnly = function () {

                        return this.each(function () {

                            $(this).keydown(function (e) {

                                var key = e.charCode || e.keyCode || 0;

                                return (

                                    key == 8 ||
                                    key == 9 ||
                                    key == 13 ||
                                    key == 46 ||
                                    key == 110 ||
                                    key == 190 ||
                                    (key >= 35 && key <= 40) ||
                                    (key >= 48 && key <= 57) ||
                                    (key >= 96 && key <= 105));

                            });

                        });

                    };
                    $(".input-numericonly").ForceNumericOnly();

                    $('.btn').each(function () {
                        dataBind = $(this).attr('data-bind');
                        if (dataBind == "click: SaveAsDraft, disable: !IsEditable(), visible: IsRole('DBS Admin')") {
                            $(this).attr('data-bind', "click: SaveAsDraft, disable: !IsEditable(), visible: true")
                        }
                    });
                    $("input").keypress(function (event) {
                        var code = event.charCode || event.keyCode;
                        if (code == 13) {
                            ShowNotification("Page Information", "Enter key is disabled for this form.", "gritter-warning", false);

                            return false;
                        }
                    });
                    $('#bank-name').keyup(function (event) {
                        var code = event.charCode || event.keyCode;
                        if (code == 46 || code == 8) {
                            if (event.target.value == '') {
                                viewModel.TransactionModel().Bank().SwiftCode = '';
                                viewModel.TransactionModel().Bank().BankAccount = '';
                                viewModel.TransactionModel().Bank().Code = '999';
                                viewModel.IsOtherBank(false);
                                viewModel.TransactionModel().IsOtherBeneBank(true);
                                $('#bank-code').val('');
                            }
                        }
                    });
                    $('#aspnetForm').after('<form id="frmUnderlying"></form>');
                    //Andi
                    $('#aspnetForm').after('<form id="frmFD"></form>');
                    //End Andi
                    $("#modal-form-Underlying").appendTo("#frmUnderlying");
                    $("#modal-form-Attach").appendTo("#frmUnderlying");
                    $box = $('#widget-box');
                    var event;
                    $box.trigger(event = $.Event('reload.ace.widget'))
                    if (event.isDefaultPrevented()) return
                    $box.blur();

                    // datepicker
                    $('.date-picker').datepicker({
                        autoclose: true,
                        onSelect: function () {
                            this.focus();
                        }
                    }).next().on(ace.click_event, function () {
                        $(this).prev().focus();
                    });

                    // validation
                    //$('#aspnetForm1').validate({
                    $('#aspnetForm').validate({
                        errorElement: 'div',
                        errorClass: 'help-block',
                        focusInvalid: true,
                        focusCleanup: true,
                        highlight: function (e) {
                            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
                        },

                        success: function (e) {
                            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
                            $(e).remove();
                        },

                        errorPlacement: function (error, element) {
                            if (element.is(':checkbox') || element.is(':radio')) {
                                var controls = element.closest('div[class*="col-"]');
                                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                            }
                            else if (element.is('.select2')) {
                                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                            }
                            else if (element.is('.chosen-select')) {
                                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                            }
                            else error.insertAfter(element.parent());
                        }
                    });

                    $('#frmUnderlying').validate({
                        errorElement: 'div',
                        errorClass: 'help-block',
                        focusInvalid: true,
                        focusCleanup: true,
                        highlight: function (e) {
                            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
                        },

                        success: function (e) {
                            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
                            $(e).remove();
                        },

                        errorPlacement: function (error, element) {
                            if (element.is(':checkbox') || element.is(':radio')) {
                                var controls = element.closest('div[class*="col-"]');
                                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                            }
                            else if (element.is('.select2')) {
                                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                            }
                            else if (element.is('.chosen-select')) {
                                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                            }
                            else error.insertAfter(element.parent());
                        }
                    });
                    //Andi
                    $('#frmFD').validate({
                        errorElement: 'div',
                        errorClass: 'help-block',
                        focusInvalid: true,
                        focusCleanup: true,
                        highlight: function (e) {
                            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
                        },

                        success: function (e) {
                            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
                            $(e).remove();
                        },

                        errorPlacement: function (error, element) {
                            if (element.is(':checkbox') || element.is(':radio')) {
                                var controls = element.closest('div[class*="col-"]');
                                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                            }
                            else if (element.is('.select2')) {
                                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                            }
                            else if (element.is('.chosen-select')) {
                                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                            }
                            else error.insertAfter(element.parent());
                        }
                    });

                    //End Andi
                    // autocomplete customer

                    if (viewModel != undefined) {
                        viewModel.SetBankAutoCompleted();
                        viewModel.SetBankBranchCompleted();
                        viewModel.SetBankChagingAccount();
                        viewModel.SetLLDAutoCompleted();

                    }
                    // ace file upload
                    $('#document-path').ace_file_input({
                        no_file: 'No File ...',
                        btn_choose: 'Choose',
                        btn_change: 'Change',
                        droppable: false,
                        //onchange:null,
                        thumbnail: false, //| true | large
                        //whitelist:'gif|png|jpg|jpeg'
                        blacklist: 'exe|dll'
                        //onchange:''
                        //
                    });

                    $('#document-path-upload').ace_file_input({
                        no_file: 'No File ...',
                        btn_choose: 'Choose',
                        btn_change: 'Change',
                        droppable: false,
                        //onchange:null,
                        thumbnail: false, //| true | large
                        //whitelist:'gif|png|jpg|jpeg'
                        blacklist: 'exe|dll'
                        //onchange:''
                        //
                    });

                    // Knockout Datepicker custom binding handler
                    ko.bindingHandlers.datepicker = {
                        init: function (element) {
                            $(element).datepicker({ autoclose: true }).next().on(ace.click_event, function () {
                                $(this).prev().focus();
                            });
                        },
                        update: function (element) {
                            $(element).datepicker("refresh");
                        }
                    };

                    // Knockout custom file handler
                    ko.bindingHandlers.file = {
                        init: function (element, valueAccessor) {
                            $(element).change(function () {
                                var file = this.files[0];

                                if (ko.isObservable(valueAccessor()))
                                    valueAccessor()(file);
                            });
                        }
                    };


                    // Dependant Observable for dropdown auto fill
                    if (viewModel != undefined) {
                        ko.dependentObservable(function () {

                            if (viewModel.Selected().Account() == '-' && !viewModel.TransactionModel().IsNewCustomer()) {
                                viewModel.IsEmptyAccountNumber(true);
                            }

                            var IsCurrencyChanged;
                            var account;
                            var product = ko.utils.arrayFirst(viewModel.Parameter().Products(), function (item) {
                                return item.ID == viewModel.Selected().Product();
                            });
                            var chooseproduct = ko.utils.arrayFirst(viewModel.Parameter().Products(), function (item) {
                                return item.ID == viewModel.Selected().ChooseProduct();
                            });
                            if (ko.isObservable(viewModel.TransactionModel().Customer)) {
                                if (viewModel.TransactionModel().Customer() != null) {
                                    account = ko.utils.arrayFirst(viewModel.TransactionModel().Customer().Accounts, function (item) {
                                        if (viewModel.Selected().Account() == '-') {
                                            viewModel.TransactionModel().IsOtherAccountNumber(true);
                                        } else {
                                            viewModel.TransactionModel().IsOtherAccountNumber(false);
                                        } return item.AccountNumber == viewModel.Selected().Account();
                                    });
                                }
                            }
                            var debitcurrency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) {
                                if (viewModel.Selected().Account() == '-') {
                                    viewModel.TransactionModel().IsOtherAccountNumber(false);
                                    return item.ID == viewModel.Selected().DebitCurrency();
                                } else {
                                    viewModel.TransactionModel().IsOtherAccountNumber(true);
                                    if (account != null) {
                                        return item.ID == account.Currency.ID;
                                    }
                                }
                            });
                            var channel = ko.utils.arrayFirst(viewModel.Parameter().Channels(), function (item) {
                                return item.ID == viewModel.Selected().Channel();
                            });

                            //started by haqi
                            var transactionType = ko.utils.arrayFirst(viewModel.Parameter().TransactionTypes(), function (item) {
                                return item.TransTypeID == viewModel.Selected().TransactionType();
                            });
                            var maintenanceType = ko.utils.arrayFirst(viewModel.Parameter().MaintenanceTypes(), function (item) {
                                return item.ID == viewModel.Selected().MaintenanceType();
                            });
                            var cellPhoneMethodID = ko.utils.arrayFirst(viewModel.Parameter().CellPhoneMethodIDs(), function (item) {
                                return item.ID == viewModel.Selected().CellPhoneMethodID();
                            });
                            var homePhoneMethodID = ko.utils.arrayFirst(viewModel.Parameter().HomePhoneMethodIDs(), function (item) {
                                return item.ID == viewModel.Selected().HomePhoneMethodID();
                            });
                            var officePhoneMethodID = ko.utils.arrayFirst(viewModel.Parameter().OfficePhoneMethodIDs(), function (item) {
                                return item.ID == viewModel.Selected().OfficePhoneMethodID();
                            });
                            var faxMethodID = ko.utils.arrayFirst(viewModel.Parameter().FaxMethodIDs(), function (item) {
                                return item.ID == viewModel.Selected().FaxMethodID();
                            });
                            var maritalStatusID = ko.utils.arrayFirst(viewModel.Parameter().MaritalStatusIDs(), function (item) {
                                return item.ID == viewModel.Selected().MaritalStatusID();
                            });
                            var dispatchModeID = ko.utils.arrayFirst(viewModel.Parameter().DispatchModeTypes(), function (item) {
                                return item.ID == viewModel.Selected().DispatchModeType();
                            });
                            var transactionSubType = ko.utils.arrayFirst(viewModel.Parameter().TransactionSubTypes(), function (item) {
                                return item.ID == viewModel.Selected().TransactionSubType();
                            });
                            var identityTypeID = ko.utils.arrayFirst(viewModel.Parameter().IdentityTypeIDs(), function (item) {
                                return item.ID == viewModel.Selected().IdentityTypeID();
                            });
                            var fundSource = ko.utils.arrayFirst(viewModel.Parameter().FundSources(), function (item) {
                                return item.ID == viewModel.Selected().FundSource();
                            });
                            var netAsset = ko.utils.arrayFirst(viewModel.Parameter().NetAssets(), function (item) {
                                return item.ID == viewModel.Selected().NetAsset();
                            });
                            var monthlyIncome = ko.utils.arrayFirst(viewModel.Parameter().MonthlyIncomes(), function (item) {
                                return item.ID == viewModel.Selected().MonthlyIncome();
                            });
                            var monthlyExtraIncome = ko.utils.arrayFirst(viewModel.Parameter().MonthlyExtraIncomes(), function (item) {
                                return item.ID == viewModel.Selected().MonthlyExtraIncome();
                            });
                            var job = ko.utils.arrayFirst(viewModel.Parameter().Jobs(), function (item) {
                                return item.ID == viewModel.Selected().Job();
                            });
                            var accountPurpose = ko.utils.arrayFirst(viewModel.Parameter().AccountPurposes(), function (item) {
                                return item.ID == viewModel.Selected().AccountPurpose();
                            });
                            var incomeForecast = ko.utils.arrayFirst(viewModel.Parameter().IncomeForecasts(), function (item) {
                                return item.ID == viewModel.Selected().IncomeForecast();
                            });
                            var outcomeForecast = ko.utils.arrayFirst(viewModel.Parameter().OutcomeForecasts(), function (item) {
                                return item.ID == viewModel.Selected().OutcomeForecast();
                            });
                            var transactionForecast = ko.utils.arrayFirst(viewModel.Parameter().TransactionForecasts(), function (item) {
                                return item.ID == viewModel.Selected().TransactionForecast();
                            });
                            var currencycif = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) {
                                return item.ID == viewModel.Selected().Currency();
                            });
                            var riskRatingResult = ko.utils.arrayFirst(viewModel.Parameter().RiskRatingResults(), function (item) {
                                return item.ID == viewModel.Selected().RiskRatingResult();
                            });
                            var cifAccountNumber = ko.utils.arrayFirst(viewModel.CIFAccountsNumber(), function (item) {
                                return item.AccountNumber == viewModel.Selected().cifAccNumber();
                            });

                            var sTagging = ko.utils.arrayFirst(viewModel.Parameter().DynamicTagUntages(), function (item) {
                                return item.ID == viewModel.Selected().TagUntag();
                            });

                            if (sTagging != null) viewModel.CIFTransactionModel().StaffTagging(sTagging);


                            //end by haqi

                            // Tambah Agung
                            var join = ko.utils.arrayFirst(viewModel.ParameterUT().Joins(), function (item) {
                                return item.ID == viewModel.Selected().Join();
                            });
                            var fnacore = ko.utils.arrayFirst(viewModel.ParameterUT().FNACores(), function (item) {
                                return item.ID == viewModel.Selected().FNACore();
                            });
                            var functiontype = ko.utils.arrayFirst(viewModel.ParameterUT().FunctionTypes(), function (item) {
                                return item.ID == viewModel.Selected().FunctionType();
                            });
                            var accounttype = ko.utils.arrayFirst(viewModel.ParameterUT().AccountTypes(), function (item) {
                                return item.ID == viewModel.Selected().AccountType();
                            });
                            var transaction_type = ko.utils.arrayFirst(viewModel.ParameterUT().TransactionTypes(), function (item) {
                                return item.TransTypeID == viewModel.Selected().Transaction_Type();
                            });
                            //End Agung

                            var bizSegment = ko.utils.arrayFirst(viewModel.Parameter().BizSegments(), function (item) {
                                return item.ID == viewModel.Selected().BizSegment();
                            });
                            //var bank = ko.utils.arrayFirst(viewModel.Parameter().Banks(), function (item){ return item.ID == viewModel.Selected().Bank(); });
                            var lld = ko.utils.arrayFirst(viewModel.Parameter().LLDs(), function (item) {
                                return item.ID == viewModel.Selected().LLD();
                            });
                            var llddocument = ko.utils.arrayFirst(viewModel.Parameter().LLDDocuments(), function (item) {
                                return item.ID == viewModel.Selected().LLDDocument();
                            });
                            var underlyingDoc = ko.utils.arrayFirst(viewModel.Parameter().UnderlyingDocs(), function (item) {
                                return item.ID == viewModel.Selected().UnderlyingDoc();
                            });
                            var fxcomplianceCode = ko.utils.arrayFirst(viewModel.Parameter().FXCompliances(), function (item) {
                                return item.ID == viewModel.Selected().FXCompliance();
                            })
                            var ChargingAccountCurrency = ko.utils.arrayFirst(viewModel.Parameter().ChargingAccountCurrencies(), function (item) {
                                return item.ID == viewModel.Selected().ChargingAccountCurrency();
                            })


                            var docPurpose = ko.utils.arrayFirst(viewModel.Parameter().DocumentPurposes(), function (item) {
                                return item.ID == viewModel.Selected().DocumentPurpose();
                            });
                            var docType = ko.utils.arrayFirst(viewModel.Parameter().DocumentTypes(), function (item) {
                                return item.ID == viewModel.Selected().DocumentType();
                            });
                            //Edit Rizki 2015-12-01
                            var bankCharges = ko.utils.arrayFirst(viewModel.Parameter().BankCharges(), function (item) {
                                //if (!viewModel.IsLoadDraftPayment()) { //Remark Rizki 2016-02-14
                                return item.ID == viewModel.Selected().BankCharges();
                                // }
                            });
                            var agentCharges = ko.utils.arrayFirst(viewModel.Parameter().AgentCharges(), function (item) {
                                //if (!viewModel.IsLoadDraftPayment()) { //Remark Rizki 2016-02-14
                                return item.ID == viewModel.Selected().AgentCharges();
                                //}
                            });
                            //End Rizki
                            var productType = ko.utils.arrayFirst(viewModel.Parameter().ProductType(), function (item) {
                                if (viewModel.Selected().Account() == '-')//&&  == false)
                                {
                                    viewModel.TransactionModel().IsOtherAccountNumber(true);
                                    viewModel.IsEmptyAccountNumber(false);
                                } else {
                                    viewModel.TransactionModel().IsOtherAccountNumber(false);
                                    viewModel.IsEmptyAccountNumber(true);
                                } return item.ID == viewModel.Selected().ProductType();
                            });
                            if (viewModel.DocumentPurpose_a() != null) {
                                if (viewModel.DocumentPurpose_a().ID() == 2) {
                                    viewModel.SelectingUnderlying(true);
                                }
                                else {
                                    viewModel.SelectingUnderlying(false);
                                }
                            }
                            if (account != null) {
                                viewModel.TransactionModel().Account(account);
                            }
                            if (productType != null) {
                                if (viewModel.TransactionModel().IsNewCustomer()) {
                                    var new_currency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) { return item.ID == viewModel.Selected().NewCustomer().Currency(); });
                                    if (new_currency != null) viewModel.TransactionModel().Account().Currency = new_currency;
                                }

                                viewModel.TransactionModel().ProductType(productType);

                                if (!viewModel.TransactionModel().IsOtherAccountNumber())
                                    IsCurrencyChanged = viewModel.TransactionModel().Account().Currency.Code;
                                else
                                    IsCurrencyChanged = viewModel.TransactionModel().DebitCurrency().Code;

                                var IsFxTransaction = (viewModel.TransactionModel().Currency().Code != 'IDR' && IsCurrencyChanged == 'IDR');
                                var IsFxTransactionToIDR = (viewModel.TransactionModel().Currency().Code == 'IDR' && IsCurrencyChanged != 'IDR');


                                viewModel.IsFxTransaction(IsFxTransaction);
                                viewModel.IsFxTransactionToIDR(IsFxTransactionToIDR);
                            }

                            if (underlyingDoc != null) {
                                viewModel.TransactionModel().UnderlyingDoc(underlyingDoc);
                                viewModel.isNewUnderlying(false); //return default disabled

                                // available type to input for new underlying name if code is 999
                                if (underlyingDoc.Code == '999') {
                                    viewModel.TransactionModel().OtherUnderlyingDoc('');
                                    viewModel.isNewUnderlying(true);
                                    $('#underlying-desc').focus();

                                    $('#underlying-desc').attr('data-rule-required', true);
                                }
                                else {
                                    viewModel.isNewUnderlying(false);
                                    //viewModel.TransactionModel().OtherUnderlyingDoc('');

                                    $('#underlying-desc').attr('data-rule-required', false);
                                }
                            }

                            if (fxcomplianceCode != null) {
                                viewModel.TransactionModel().Compliance(fxcomplianceCode);
                            }

                            //Tambahan IPE Agung
                            var sundrydata = ko.utils.arrayFirst(viewModel.Parameter().Sundries(), function (item) {
                                return item.ID == viewModel.Selected().Sundry();
                            })
                            var beneficiarycountry = ko.utils.arrayFirst(viewModel.Parameter().BeneficiaryCountry(), function (item) {
                                return item.ID == viewModel.Selected().BeneficiaryCountry();
                            })
                            var transactionrelationship = ko.utils.arrayFirst(viewModel.Parameter().TransactionRelationship(), function (item) {
                                return item.ID == viewModel.Selected().TransactionRelationship();
                            })
                            var cbgcustomer = ko.utils.arrayFirst(viewModel.Parameter().CBGCustomer(), function (item) {
                                return item.ID == viewModel.Selected().CBGCustomer();
                            })
                            var transactionusingdebitsundry = ko.utils.arrayFirst(viewModel.Parameter().TransactionUsingDebitSundry(), function (item) {
                                return item.ID == viewModel.Selected().TransactionUsingDebitSundry();
                            })

                            var nostro = ko.utils.arrayFirst(viewModel.Parameter().Nostroes(), function (item) {
                                return item.ID == viewModel.Selected().Nostro();
                            })

                            if (nostro != null) {
                                viewModel.TransactionModel().Nostro(nostro);
                            }
                            if (sundrydata != null) {
                                viewModel.TransactionModel().Sundry(sundrydata);
                            }

                            if (beneficiarycountry != null) {
                                viewModel.TransactionModel().BeneficiaryCountry(beneficiarycountry);
                            }
                            if (transactionrelationship != null) {
                                viewModel.TransactionModel().TransactionRelationship(transactionrelationship);
                            }
                            if (cbgcustomer != null) {
                                viewModel.TransactionModel().CBGCustomer(cbgcustomer);
                            }
                            if (transactionusingdebitsundry != null) {
                                viewModel.TransactionModel().TransactionUsingDebitSundry(transactionusingdebitsundry);
                            }
                            //End


                            var currency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) {
                                if (viewModel.Selected().Account() == '-') { //&& viewModel.IsEmptyAccountNumber() == false) {
                                    viewModel.TransactionModel().IsOtherAccountNumber(true);
                                    viewModel.IsEmptyAccountNumber(true);
                                } else {
                                    viewModel.TransactionModel().IsOtherAccountNumber(false);
                                    viewModel.IsEmptyAccountNumber(false);
                                }
                                return item.ID == viewModel.Selected().Currency();
                            });

                            if (debitcurrency != null) {
                                if (!viewModel.TransactionModel().IsOtherAccountNumber())
                                    IsCurrencyChanged = viewModel.TransactionModel().Account().Currency.Code;
                                else
                                    //IsCurrencyChanged = viewModel.TransactionModel().DebitCurrency().Code;
                                    IsCurrencyChanged = viewModel.TransactionModel().DebitCurrency().Code;
                                if (viewModel.TransactionModel().IsNewCustomer()) {
                                    var new_currency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) { return item.ID == viewModel.Selected().NewCustomer().Currency(); });
                                    if (new_currency != null) viewModel.TransactionModel().Account().Currency = new_currency;
                                }

                                viewModel.TransactionModel().DebitCurrency(debitcurrency);
                                //viewModel.GetRateAmount(currency.ID);

                                if (!viewModel.TransactionModel().IsOtherAccountNumber())
                                    IsCurrencyChanged = viewModel.TransactionModel().Account().Currency.Code;
                                else
                                    IsCurrencyChanged = viewModel.TransactionModel().DebitCurrency().Code;

                                var IsFxTransaction = (viewModel.TransactionModel().Currency().Code != 'IDR' && IsCurrencyChanged == 'IDR');
                                var IsFxTransactionToIDR = (viewModel.TransactionModel().Currency().Code == 'IDR' && IsCurrencyChanged != 'IDR');
                                viewModel.IsFxTransaction(IsFxTransaction);
                                viewModel.IsFxTransactionToIDR(IsFxTransactionToIDR);
                            }

                            if (currency != null) {
                                if (viewModel.TransactionModel().IsNewCustomer()) {
                                    var new_currency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) { return item.ID == viewModel.Selected().NewCustomer().Currency(); });
                                    if (new_currency != null) viewModel.TransactionModel().Account().Currency = new_currency;
                                }
                                viewModel.TransactionModel().Currency(currency);

                                //Dani
                                viewModel.TransactionTMOModel().Currency(currency);
                                //End Dani

                                //Afif
                                viewModel.TransactionLoanModel().Currency(currency);
                                //End Afif

                                viewModel.GetRateAmount(currency.ID);

                                if (!viewModel.TransactionModel().IsOtherAccountNumber()) {
                                    if (viewModel.TransactionModel().Account().Currency.Code != null) { //Rizki - 2016-02-11
                                        IsCurrencyChanged = viewModel.TransactionModel().Account().Currency.Code;
                                    }
                                    else {
                                        IsCurrencyChanged = "";
                                    }
                                }
                                else {
                                    if (viewModel.TransactionModel().DebitCurrency().Code != null) {
                                        IsCurrencyChanged = viewModel.TransactionModel().DebitCurrency().Code;
                                    }
                                    else {
                                        IsCurrencyChanged = "";
                                    }
                                }

                                var IsFxTransaction = (viewModel.TransactionModel().Currency().Code != 'IDR' && IsCurrencyChanged == 'IDR');
                                var IsFxTransactionToIDR = (viewModel.TransactionModel().Currency().Code == 'IDR' && IsCurrencyChanged != 'IDR' && IsCurrencyChanged != "");
                                viewModel.IsFxTransaction(IsFxTransaction);
                                viewModel.IsFxTransactionToIDR(IsFxTransactionToIDR);
                            }

                            //Started by haqi
                            if (transactionType != null) viewModel.CIFTransactionModel().TransactionType(transactionType);
                            if (maintenanceType != null) viewModel.CIFTransactionModel().MaintenanceType(maintenanceType);
                            if (maritalStatusID != null) {
                                viewModel.CIFTransactionModel().RetailCIFCBO().MaritalStatusID(maritalStatusID);
                                switch (maritalStatusID.ID) {
                                    case ConsMaritalStatus.SudahMenikah:
                                        viewModel.IsMaritalStatusSudahMenikah(true);
                                        break;
                                    case ConsMaritalStatus.BelumMenikah:
                                    case ConsMaritalStatus.DudaJanda:
                                        viewModel.IsMaritalStatusSudahMenikah(false);
                                    default:
                                        break;
                                }
                            }
                            if (dispatchModeID != null) viewModel.CIFTransactionModel().RetailCIFCBO().DispatchModeType(dispatchModeID);
                            if (cellPhoneMethodID != null) viewModel.CIFTransactionModel().RetailCIFCBO().CellPhoneMethodID(cellPhoneMethodID);
                            if (homePhoneMethodID != null) viewModel.CIFTransactionModel().RetailCIFCBO().HomePhoneMethodID(homePhoneMethodID);
                            if (officePhoneMethodID != null) viewModel.CIFTransactionModel().RetailCIFCBO().OfficePhoneMethodID(officePhoneMethodID);
                            if (faxMethodID != null) viewModel.CIFTransactionModel().RetailCIFCBO().FaxMethodID(faxMethodID);
                            if (transactionSubType != null) viewModel.CIFTransactionModel().TransactionSubType(transactionSubType);
                            if (currencycif != null) viewModel.CIFTransactionModel().Currency(currencycif);
                            if (identityTypeID != null) viewModel.CIFTransactionModel().RetailCIFCBO().IdentityTypeID(identityTypeID);
                            if (fundSource != null) viewModel.CIFTransactionModel().RetailCIFCBO().FundSource(fundSource);
                            if (netAsset != null) viewModel.CIFTransactionModel().RetailCIFCBO().NetAsset(netAsset);
                            if (monthlyIncome != null) viewModel.CIFTransactionModel().RetailCIFCBO().MonthlyIncome(monthlyIncome);
                            if (monthlyExtraIncome != null) viewModel.CIFTransactionModel().RetailCIFCBO().MonthlyExtraIncome(monthlyExtraIncome);
                            if (job != null) viewModel.CIFTransactionModel().RetailCIFCBO().Job(job);
                            if (accountPurpose != null) viewModel.CIFTransactionModel().RetailCIFCBO().AccountPurpose(accountPurpose);
                            if (incomeForecast != null) viewModel.CIFTransactionModel().RetailCIFCBO().IncomeForecast(incomeForecast);
                            if (outcomeForecast != null) viewModel.CIFTransactionModel().RetailCIFCBO().OutcomeForecast(outcomeForecast);
                            if (transactionForecast != null) viewModel.CIFTransactionModel().RetailCIFCBO().TransactionForecast(transactionForecast);
                            if (riskRatingResult != null) viewModel.CIFTransactionModel().RetailCIFCBO().RiskRatingResult(riskRatingResult);

                            if (cifAccountNumber != null) {
                                viewModel.CIFTransactionModel().AccountNumber(cifAccountNumber);
                            }
                            //End by haqi

                            if (channel != null) {
                                viewModel.TransactionModel().Channel(channel);
                                //Dani
                                viewModel.TransactionTMOModel().Channel(channel);
                                viewModel.FDModel().Channel(channel);
                                //End Dani

                                //Afif
                                viewModel.TransactionLoanModel().Channel(channel);
                                //End Afif
                            }
                            //Agung
                            if (join != null) {
                                viewModel.TransactionUTModel().Join(join);
                            }
                            if (fnacore != null) {
                                viewModel.TransactionUTModel().FNACore(fnacore);
                            }
                            if (functiontype != null) {
                                viewModel.TransactionUTModel().FunctionType(functiontype);
                            }
                            if (accounttype != null) {
                                viewModel.TransactionUTModel().AccountType(accounttype);
                            }
                            if (transaction_type != null) {
                                viewModel.TransactionUTModel().Transaction_Type(transaction_type);
                            }
                            //Agung
                            if (bizSegment != null) {
                                viewModel.TransactionModel().BizSegment(bizSegment);
                                //Dani
                                viewModel.TransactionTMOModel().BizSegment(bizSegment);
                                //End Dani

                                //Afif
                                viewModel.TransactionLoanModel().BizSegment(bizSegment);
                                //End Afif
                            }
                            //if(bank != null) viewModel.TransactionModel().Bank(bank);
                            if (lld != null) viewModel.TransactionModel().LLD(lld);
                            if (account != null) {
                                if (viewModel.TransactionModel().IsNewCustomer()) {
                                    var new_currency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) { return item.ID == viewModel.Selected().NewCustomer().Currency(); });
                                    if (new_currency != null) viewModel.TransactionModel().Account().Currency = new_currency;
                                }
                                viewModel.TransactionModel().Account(account);

                                if (!viewModel.TransactionModel().IsOtherAccountNumber()) {
                                    IsCurrencyChanged = viewModel.TransactionModel().Account().Currency.Code;
                                }
                                else {
                                    IsCurrencyChanged = viewModel.TransactionModel().DebitCurrency().Code;
                                }

                                var IsFxTransaction = (viewModel.TransactionModel().Currency().Code != 'IDR' && IsCurrencyChanged == 'IDR');
                                var IsFxTransactionToIDR = (viewModel.TransactionModel().Currency().Code == 'IDR' && IsCurrencyChanged != 'IDR');
                                viewModel.IsFxTransaction(IsFxTransaction);
                                viewModel.IsFxTransactionToIDR(IsFxTransactionToIDR);
                            };

                            if (product != null) {
                                viewModel.TransactionModel().Product(product);
                                //Dani
                                viewModel.TransactionTMOModel().Product(product);
                                //End Dani
                                //Afif
                                viewModel.TransactionLoanModel().Product(product);
                                //End Afif
                                viewModel.TransactionUTModel().Product(product);
                                viewModel.CIFTransactionModel().Product(product);
                                var productSelected = viewModel.TransactionModel().Product().Name;
                                if (!viewModel.IsLoadDraftPayment()) { //Rizki 2015-12-01
                                    if (productSelected == 'RTGS') {
                                        viewModel.Selected().BankCharges(viewModel.TransactionModel().BankCharges);
                                        //viewModel.Selected().AgentCharges(viewModel.TransactionModel().AgentCharges);
                                    } else if (productSelected == 'OTT' || productSelected == 'SKN') {
                                        viewModel.Selected().BankCharges(viewModel.TransactionModel().BankCharges);
                                        viewModel.Selected().AgentCharges(viewModel.TransactionModel().AgentCharges);
                                    }
                                }


                            }
                            if (chooseproduct != null) {
                                viewModel.TransactionModel().Product(chooseproduct);
                                //Andi
                                viewModel.FDModel().Product(chooseproduct);
                                //End Andi
                                viewModel.TransactionTMOModel().Product(chooseproduct);
                                viewModel.TransactionLoanModel().Product(chooseproduct);
                                viewModel.TransactionUTModel().Product(chooseproduct);

                                viewModel.Selected().Product(chooseproduct.ID)
                                viewModel.ProductID(chooseproduct.ID);
                                viewModel.ProductTitle(chooseproduct.Name);
                            }
                            if (docType != null) viewModel.DocumentType(docType);


                            if (docPurpose != null) viewModel.DocumentPurpose(docPurpose);
                            if (bankCharges != null) viewModel.TransactionModel().BankCharges(bankCharges);
                            if (agentCharges != null) viewModel.TransactionModel().AgentCharges(agentCharges);
                            //henggar bene bank filled if product selected is over booking
                            //if (viewModel.TransactionModel().Product().ID == Const_OverBookingProduct.SelectedProduct) {
                            // GetBankAllTunning();
                            // var new_BeneBank = ko.utils.arrayFirst(DataBank, function (item) { return item.ID == Const_OverBookingProduct.SelectedBeneBank });
                            //if (new_BeneBank != null) viewModel.TransactionModel().Bank(new_BeneBank); 
                            //}

                            if (viewModel.TransactionModel().Product().ID == Const_OverBookingProduct.SelectedProduct) {
                                viewModel.IsDraft(true)
                                viewModel.IsOtherBank(true)
                                viewModel.SetBankAutoCompleted();
                                viewModel.SetBankBranchCompleted();
                                viewModel.SetBankChagingAccount();
                            }
                            else {
                                viewModel.IsDraft(false)
                                //viewModel.IsOtherBank(false)
                            }
                            //end henggar

                            if (viewModel.TransactionModel().IsNewCustomer()) {
                                var new_currency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) { return item.ID == viewModel.Selected().NewCustomer().Currency(); });
                                var new_bizSegment = ko.utils.arrayFirst(viewModel.Parameter().BizSegments(), function (item) { return item.ID == viewModel.Selected().NewCustomer().BizSegment(); });

                                if (new_currency != null) viewModel.TransactionModel().Account().Currency = new_currency;
                                if (new_bizSegment != null) viewModel.TransactionModel().Customer().BizSegment(new_bizSegment);
                            }

                            if (viewModel.TransactionModel().IsNewCustomer() && !viewModel.IsLoadDraft() && (viewModel.IsFxTransaction() || viewModel.IsFxTransactionToIDR())) {
                                ShowNotification("Page Information", NewCustomerFXWarning, "gritter-warning", false);
                                //NewCustomerFXWarning
                            }

                            //Andi
                            var TType = ko.utils.arrayFirst(viewModel.Parameter().TransactionType(), function (item) {
                                return item.TransTypeID == viewModel.Selected().TransactionType();
                            });
                            var fdRemarks = ko.utils.arrayFirst(viewModel.Parameter().FDRemarks(), function (item) {
                                return item.ID == viewModel.Selected().FDRemarks();
                            });
                            if (TType != null) viewModel.TransactionType(TType);
                            if (fdRemarks != null) viewModel.FDRemarks(fdRemarks);
                            if (currency != null) viewModel.Currencies(currency);
                            if (product != null) {
                                viewModel.FDModel().Product(product);
                            }
                            if (ChargingAccountCurrency !== null && ChargingAccountCurrency !== undefined) {
                                viewModel.TransactionModel().ChargingAccountCurrency = ChargingAccountCurrency.ID;
                            }
                            //End Andi
                        });
                    }
                    //viewModel['Calculate'] = calculate();
                    //================================= End Eqv Calculation ====================================
                    self.SPUser = ko.observable();
                    // Token Validation
                    if ($.cookie(api.cookie.name) == undefined) {
                        Helper.Token.Request(OnSuccessToken, OnError);
                    } else {
                        // read token from cookie
                        accessToken = $.cookie(api.cookie.name);

                        // read spuser from cookie
                        if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
                            spUser = $.cookie(api.cookie.spUser);
                            self.SPUser(ko.mapping.toJS(spUser));
                        }

                        // call get data inside view model
                        viewModel.GetParameters();
                        //PPUModel.token = accessToken;
                        //GetParameterData(PPUModel, OnSuccessGetTotal, OnErrorDeal);
                        PPUModel.token = accessToken;
                        //GetThresholdParameter(PPUModel, OnSuccessThresholdPrm, OnErrorDeal);
                        //ResetBeneBank();
                        //GetEmployeeLocation();
                    }
                    //GetPaymentStatus();
                    ko.applyBindings(viewModel);

                    //ko.applyBindings(viewModel, document.getElementById('new-transaction'));
                    GetRateIDR();
                    function GetRateIDR() {
                        var options = {
                            url: api.server + api.url.currency + "/CurrencyRate/" + "1",
                            params: {
                            },
                            token: accessToken
                        };

                        Helper.Ajax.Get(options, OnSuccessGetRateIDR, OnError, OnAlways);
                    }

                    function OnSuccessGetRateIDR(data, textStatus, jqXHR) {
                        if (jqXHR.status = 200) {
                            // bind result to observable array
                            vSystem.idrrate = data.RupiahRate;
                        } else {
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
                        }
                    }
                    function OnError(jqXHR, textStatus, errorThrown) {
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
                    }
                    function OnAlways() {
                        // $box.trigger('reloaded.ace.widget');
                        if (viewModel != undefined) {
                            // enabling form input controls
                            viewModel.IsEditable(true);
                        }
                    }
                    //================================= Eqv eion ====================================        
                });
            });
        });
    });
});

function enforceMaxlength(data, event) {
    if (viewModel.TransactionModel().Product().Name == 'SKN') {
        if (event.target.value.length >= 70) {
            return false;
        }
    }
    else {
        if (viewModel.TransactionModel().Product().Name != 'OTT') {
            if (event.target.value.length >= 35) {
                return false;
            }
        }
        else {
            if (event.target.value.length >= 255) {
                return false;
            }
        }
    }

    return true;
}

function UploadFileRecuresive(context, document, callBack, numFile) {
        var indexDocument = document.length - numFile;
        var IsDraft;
        //default value to payment module
        IsDraft = viewModel.TransactionModel().IsDraft();
        switch (viewModel.ProductID()) {
            case ConsProductID.SKNBulkProductIDCons: //add aridya 20161012 skn bulk ~OFFLINE~
            case ConsProductID.RTGSProductIDCons:
            case ConsProductID.SKNProductIDCons:
            case ConsProductID.OTTProductIDCons:
            case ConsProductID.OverbookingProductIDCons:
                IsDraft = viewModel.TransactionModel().IsDraft();
                break;
            case ConsProductID.TMOProductIDCons:
                IsDraft = viewModel.TransactionTMOModel().IsDraft();
                break;
            case ConsProductID.FDProductIDCons:
                IsDraft = viewModel.FDModel().IsDraft();
                break;
            case ConsProductID.LoanDisbursmentProductIDCons:
            case ConsProductID.LoanIMProductIDCons:
            case ConsProductID.LoanRolloverProductIDCons:
            case ConsProductID.LoanSettlementProductIDCons:
                IsDraft = viewModel.TransactionLoanModel().IsDraft();
                break;
            case ConsProductID.UTOnshoreproductIDCons:
            case ConsProductID.UTOffshoreProductIDCons:
            case ConsProductID.UTCPFProductIDCons:
            case ConsProductID.SavingPlanProductIDCons:
            case ConsProductID.IDInvestmentProductIDCons:
                IsDraft = viewModel.TransactionUTModel().IsDraft();
                break;
            case ConsProductID.CIFProductIDCons:
                IsDraft = viewModel.CIFTransactionModel().IsDraft();
                break;
        }

        var serverRelativeUrlToFolder = '';
        if (IsDraft == true)
            serverRelativeUrlToFolder = '/DraftDocument';
        else {
            //add aridya 20161024 skn bulk ~OFFLINE~
            if (viewModel.ProductID() == ConsProductID.SKNBulkProductIDCons) {
                serverRelativeUrlToFolder = '/SKNBulkData';
            } else {
                serverRelativeUrlToFolder = '/Instruction Documents';
            }
            //end add
        }

        var parts = document[indexDocument].DocumentPath.name.split('.');
        var fileExtension = parts[parts.length - 1];
        var timeStamp = new Date().getTime();
        var fileName = timeStamp + "." + fileExtension; //document.DocumentPath.name;

        // Get the server URL.
        var serverUrl = _spPageContextInfo.webAbsoluteUrl;

        // output variable
        var output;

        // Initiate method calls using jQuery promises.
        // Get the local file as an array buffer.
        var getFile = getFileBuffer();
        getFile.done(function (arrayBuffer) {

            // Add the file to the SharePoint folder.
            var addFile = addFileToFolder(arrayBuffer);
            addFile.done(function (file, status, xhr) {
                output = file.d;

                // Get the list item that corresponds to the uploaded file.
                var getItem = getListItem(file.d.ListItemAllFields.__deferred.uri);
                getItem.done(function (listItem, status, xhr) {

                    // Change the display name and title of the list item.
                    var changeItem = updateListItem(listItem.d.__metadata);
                    changeItem.done(function (data, status, xhr) {
                        //alert('file uploaded and updated');
                        //return output;
                        var newDoc = {
                            ID: 0,
                            Type: document[indexDocument].Type,
                            Purpose: document[indexDocument].Purpose,
                            LastModifiedDate: null,
                            LastModifiedBy: null,
                            DocumentPath: output.ServerRelativeUrl,
                            FileName: document[indexDocument].DocumentPath.name
                        };
                        switch (viewModel.ProductID()) {
                            case ConsProductID.SKNBulkProductIDCons: //add aridya 20161012 skn bulk ~OFFLINE~
                            case ConsProductID.RTGSProductIDCons:
                            case ConsProductID.SKNProductIDCons:
                            case ConsProductID.OTTProductIDCons:
                            case ConsProductID.OverbookingProductIDCons:
                                viewModel.TransactionModel().Documents.push(newDoc);
                                break;
                            case ConsProductID.TMOProductIDCons:
                                viewModel.TransactionTMOModel().Documents.push(newDoc);
                                break;
                            case ConsProductID.FDProductIDCons:
                                viewModel.FDModel().Documents.push(newDoc);
                                break;
                            case ConsProductID.LoanDisbursmentProductIDCons:
                            case ConsProductID.LoanIMProductIDCons:
                            case ConsProductID.LoanRolloverProductIDCons:
                            case ConsProductID.LoanSettlementProductIDCons:
                                viewModel.TransactionLoanModel().Documents.push(newDoc);
                                break;
                            case ConsProductID.UTOnshoreproductIDCons:
                            case ConsProductID.UTOffshoreProductIDCons:
                            case ConsProductID.UTCPFProductIDCons:
                            case ConsProductID.SavingPlanProductIDCons:
                            case ConsProductID.IDInvestmentProductIDCons:
                                viewModel.TransactionUTModel().Documents.push(newDoc);
                                break;
                            case ConsProductID.CIFProductIDCons:
                                viewModel.CIFTransactionModel().DocumentsCIF.push(newDoc);
                                break;
                        }
                        if (numFile > 1) {
                            UploadFileRecuresive(context, document, callBack, numFile - 1); // recursive function
                        }
                        callBack();
                    });
                    changeItem.fail(viewModel.OnError);
                });
                getItem.fail(viewModel.OnError);
            });
            addFile.fail(viewModel.OnError);
        });
        getFile.fail(viewModel.OnError);

        // Get the local file as an array buffer.
        function getFileBuffer() {
            var deferred = $.Deferred();
            var reader = new FileReader();
            reader.onloadend = function (e) {
                deferred.resolve(e.target.result);
            }
            reader.onerror = function (e) {
                deferred.reject(e.target.error);
            }

            if (document[indexDocument].DocumentPath.type == Util.spitemdoc) {
                var fileURI = serverUrl + document[indexDocument].DocumentPath.DocPath;
                // load file:
                fetch(fileURI, convert, alert);

                function convert(buffer) {
                    var blob = new Blob([buffer], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });

                    //var domURL = self.URL || self.webkitURL || self,
                    var domURL = URL || webkitURL,
                      url = domURL.createObjectURL(blob),
                      img = new Image;

                    img.onload = function () {
                        domURL.revokeObjectURL(url); // clean up
                        //document.body.appendChild(this);
                        // this = image
                    };
                    img.src = url;
                    reader.readAsArrayBuffer(blob);
                }

                function fetch(url, callback, error) {

                    var xhr = new XMLHttpRequest();
                    try {
                        xhr.open("GET", url);
                        xhr.responseType = "arraybuffer";
                        xhr.onerror = function () {
                            error("Network error")
                        };
                        xhr.onload = function () {
                            if (xhr.status === 200) callback(xhr.response);
                            else error(xhr.statusText);
                        };
                        xhr.send();
                    } catch (err) {
                        error(err.message)
                    }
                }


            } else {
                reader.readAsArrayBuffer(document[indexDocument].DocumentPath);
            }
            //bangkit

            //reader.readAsDataURL(document.DocumentPath);
            return deferred.promise();
        }

        // Add the file to the file collection in the Shared Documents folder.
        function addFileToFolder(arrayBuffer) {

            // Construct the endpoint.
            var fileCollectionEndpoint = String.format(
                    "{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files" +
                    "/add(overwrite=false, url='{2}')",
                serverUrl, serverRelativeUrlToFolder, fileName);

            // Send the request and return the response.
            // This call returns the SharePoint file.
            return $.ajax({
                url: fileCollectionEndpoint,
                type: "POST",
                data: arrayBuffer,
                processData: false,
                headers: {
                    "accept": "application/json;odata=verbose",
                    "X-RequestDigest": $("#__REQUESTDIGEST").val()
                    //"content-length": arrayBuffer.byteLength
                }
            });
        }

        // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
        function getListItem(fileListItemUri) {

            // Send the request and return the response.
            return $.ajax({
                url: fileListItemUri,
                type: "GET",
                headers: { "accept": "application/json;odata=verbose" }
            });
        }

        // Change the display name and title of the list item.
        function updateListItem(itemMetadata) {
            var body = {
                Title: document[indexDocument].DocumentPath.name,
                Application_x0020_ID: context.ApplicationID,
                CIF: context.CIF,
                Customer_x0020_Name: context.Name,
                Document_x0020_Type: document[indexDocument].Type.Name,
                Document_x0020_Purpose: document[indexDocument].Purpose.Name,
                __metadata: {
                    type: itemMetadata.type,
                    FileLeafRef: fileName,
                    Title: fileName
                }
            };

            // Send the request and return the promise.
            // This call does not return response content from the server.
            return $.ajax({
                url: itemMetadata.uri,
                type: "POST",
                data: ko.toJSON(body),
                headers: {
                    "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                    "content-type": "application/json;odata=verbose",
                    //"content-length": body.length,
                    "IF-MATCH": itemMetadata.etag,
                    "X-HTTP-Method": "MERGE"
                }
            });
        }
    
}

