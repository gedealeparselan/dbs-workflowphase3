﻿using DBS.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class NewTransactionUTController : ApiController
    {

        private string message = string.Empty;

        private readonly INewTransactionUTRepository repository = null;
        public NewTransactionUTController()
        {
            this.repository = new NewTransactionUTRepository();
        }

        public NewTransactionUTController(INewTransactionUTRepository repo)
        {
            this.repository = repo;
        }

        [ResponseType(typeof(string))]
        [Route("api/NewTransactionUTIN")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage AddUTIN(TransactionUTINModel transaction)
        {
            long transactionID = 0;
            string ApplicationID = string.Empty;

            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddTransactionUTIN(transaction, ref transactionID, ref  ApplicationID, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new
                    {
                        ID = transactionID,
                        AppID = ApplicationID,
                        Message = transaction.IsDraft ? "Transaction has been saved as draft." : "New Transaction has been submitted."
                    });
                }
            }
        }

        [ResponseType(typeof(string))]
        [Route("api/NewTransactionUTSP")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage AddUTSP(TransactionUTSPModel transaction)
        {
            long transactionID = 0;
            string ApplicationID = string.Empty;

            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddTransactionUTSP(transaction, ref transactionID, ref  ApplicationID, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new
                    {
                        ID = transactionID,
                        AppID = ApplicationID,
                        Message = transaction.IsDraft ? "Transaction has been saved as draft." : "New Transaction has been submitted."
                    });
                }
            }
        }

        [ResponseType(typeof(InvestmentIDModel))]
        [Route("api/NewTransactionUTIN/Investment")]
        [HttpGet]
        public HttpResponseMessage GetParam(string query,string cif)
        {
            IList<InvestmentIDModel> result = new List<InvestmentIDModel>();

            if (!repository.GetInvestmentID(query,cif, ref result, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<IList<InvestmentIDModel>>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(InvestmentIDGenerateModel))]
        [Route("api/NewTransactionUTIN/investID/{investment}")]
        [HttpGet]
        public HttpResponseMessage BindInvestment(string investment)
        {
            InvestmentIDGenerateModel output = new InvestmentIDGenerateModel();

            if (!repository.PopulateInvestmentID(investment, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<InvestmentIDGenerateModel>(HttpStatusCode.OK, output);
            }

        }
        [ResponseType(typeof(IList<TransactitonMutualFundModel>))]
        [Route("api/NewTransactionUTSP/investIDSP/{investment}")]
        [HttpGet]
        public HttpResponseMessage BindInvestmentMutual(string investment)
        {
            IList<TransactitonMutualFundModel> output = new List<TransactitonMutualFundModel>();

            if (!repository.PopulateInvestmentIDMutual(investment, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<IList<TransactitonMutualFundModel>>(HttpStatusCode.OK, output);
            }

        }

        [ResponseType(typeof(TransactionUTINDraftModel))]
        [Route("api/NewTransactionUTIN/Draft/{id}")]
        [HttpGet]
        public HttpResponseMessage GetDraftIN(Int64 id)
        {
            TransactionUTINDraftModel output = new TransactionUTINDraftModel();
            if (!repository.GetTransactionDraftINByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<TransactionUTINDraftModel>(HttpStatusCode.OK, output);
            }
        }
        [ResponseType(typeof(TransactionUTSPDraftModel))]
        [Route("api/NewTransactionUTSP/Draft/{id}")]
        [HttpGet]
        public HttpResponseMessage GetDraftSP(Int64 id)
        {
            TransactionUTSPDraftModel output = new TransactionUTSPDraftModel();
            if (!repository.GetTransactionDraftSPByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<TransactionUTSPDraftModel>(HttpStatusCode.OK, output);
            }
        }
        [HttpDelete]
        [Route("api/NewTransactionUTIN/Draft/Delete/{id}")]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage DeleteDraftIN(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteTransactionDraftByID(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Draft." });
                }
            }
        }

        [HttpDelete]
        [Route("api/NewTransactionUTSP/Draft/Delete/{id}")]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage DeleteDraftSP(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteTransactionDraftByID(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Draft." });
                }
            }
        }

        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/NewTransactionUTIN/Draft/{id}")]
        //[Authorize(Roles = "Admin, DBS PPU Maker")]
        public HttpResponseMessage PutDraftIN(long id, TransactionUTINModel transaction)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateTransactionDraftIN(id, transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction data has been updated." });
                }
            }
        }

        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/NewTransactionUTSP/Draft/{id}")]
        //[Authorize(Roles = "Admin, DBS PPU Maker")]
        public HttpResponseMessage PutDraftSP(long id, TransactionUTSPModel transaction)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateTransactionDraftSP(id, transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction data has been updated." });
                }
            }
        }
    }

}