﻿using FastMember;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace DBS.WebAPI.Helpers
{
    public static class DataTableConverter
    {
        public static DataTable CreateDataTableFromList<T>(this IList<T> listToConvert)
        {
            DataTable dt = new DataTable();
            using (var reader = ObjectReader.Create(listToConvert))
            {
                dt.Load(reader);
            }
            return dt;
        }
    }
}