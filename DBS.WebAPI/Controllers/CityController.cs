﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    public class CityController : ApiController
    {
        private string message = string.Empty;

        private readonly ICityModel repository = null;

        public CityController()
        {
            this.repository = new CityRepository();
        }

        public CityController(ICityModel repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all City.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<CityModel>))]
        [HttpGet]
        public HttpResponseMessage Get()
        {
            IList<CityModel> output = new List<CityModel>();

            if (!repository.GetCity(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<CityModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Product Type Threshold with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<ProvinsiModel>))]
        [HttpGet]
        [Route("api/City/Search")]
        public HttpResponseMessage Get(string query, int? limit)
        {
            IList<ProvinsiModel> output = new List<ProvinsiModel>();

            if (!repository.GetCity(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<IList<ProvinsiModel>>(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(IList<CityModel>))]
        [HttpGet]
        [Route("api/City/SearchDetail")]
        public HttpResponseMessage SerachDetail(string query, int? limit)
        {
            IList<CityModel> output = new List<CityModel>();

            if (!repository.GetCity(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<CityModel>>(HttpStatusCode.OK, output);
            }
        }


        /// <summary>
        /// Get City for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(CityModelGrid))]
        [AcceptVerbs("GET", "POST")]
        [Route("api/City")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<CityModelFilter> filters)
        {
            CityModelGrid output = new CityModelGrid();

            if (!repository.GetCity(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Group Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<CityModelGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get City by ID.
        /// </summary>
        /// <param name="id">ProductMappingID</param>
        /// <returns></returns>
        [ResponseType(typeof(CityModel))]
        [Route("api/City/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            CityModel output = new CityModel();

            if (!repository.GetCityByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<CityModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new city
        //Add new
        /// </summary>
        /// <param name="ProductTypeCode">Product Type Code</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        [Route("api/City")]
        public HttpResponseMessage Post(CityModel City)
        {

            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddCity(City, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New City has been created." });
                }
            }
        }

        /// <summary>
        /// Modify existing product type threshold mapping.
        /// </summary>
        /// <param name="id">ID of product type mapping to be modify.</param>
        /// <param name="threshold">Product type mapping data to be updated.</param>
        /// <returns></returns>
        /// Update Data ThresholdGroup
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/City/{ID}")]
        public HttpResponseMessage Update(int id, ref CityModel City)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateCity(id, ref City, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "City data has been updated." });
                }
            }
        }


        //Delete Data
        [HttpDelete]
        [Route("api/City/{id}")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteCity(id, ref message))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse<string>(HttpStatusCode.OK, "City data has been deleted.");
                }
            }
        }

    }
}
