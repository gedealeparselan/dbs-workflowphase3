var accessToken;
var $box;
var $remove = false;
var DocumentModels = ko.observable({ FileName: '', DocumentPath: '' });

var cifData = '0';
var DealID = '0';
var idrrate = 0;
var fromName = '';

var formatNumber = function (num) {
    if(num != null) {
        //num = num.toString().replace(/,/g, '');
        //num = parseFloat(num).toFixed(2);
        var n = num.toString(), p = n.indexOf('.');
        return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
            return p < 0 || i < p ? ($0 + ',') : $0;
        });
    }else {return 0;}
}

var formatNumber_r = function (num) {
    if(num != null) {
        num = num.toString().replace(/,/g, '');
        num = parseFloat(num).toFixed(2);
        var n = num.toString(), p = n.indexOf('.');
        return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
            return p < 0 || i < p ? ($0 + ',') : $0;
        });
    }else {return 0;}
}

var resetNumber = function (num) {
    if(num != null) {
        num = num.toString().replace(/,/g, '');
        return parseFloat(num).toFixed(2);
    } else {return 0;}
}

var formatAccount = function (num) {

    num = num.replace(/-/g, '');

    sheet_a = (num.length > 0) ? num.substr(0, 4) : '';
    sheet_b = (num.length > 4) ? '-' + num.substr(4, 4) : '';
    sheet_c = (num.length > 8) ? '-' + num.substr(8) : '';

    return sheet_a + sheet_b +sheet_c;
}

var formatMakerAccount = function (task) {
    //Transaction.BeneAccNumber
    var num = document.getElementById("bene-acc-number").value;
    num = num.replace(/-/g, '');

    switch (task) {
        case "PaymentMaker":
            viewModel.PaymentMaker().Payment.BeneAccNumber = num;
            break;
        case "TransactionMaker":
            viewModel.TransactionMaker().Transaction.BeneAccNumber = num;
            break;
        case "TransactionMakerAfterCaller":
            viewModel.TransactionMaker().Transaction.BeneAccNumber = num;
            break;
        case "TransactionCaller":
            viewModel.TransactionCallback().Transaction.BeneAccNumber = num;
            break;
    }

    sheet_a = (num.length > 0) ? num.substr(0, 4) : '';
    sheet_b = (num.length > 4) ? '-' + num.substr(4, 4) : '';
    sheet_c = (num.length > 8) ? '-' + num.substr(8) : '';

    viewModel.BeneAccNumberMask(sheet_a + sheet_b +sheet_c);
}

var GridPropertiesModel = function (callback) {
    var self = this;

    self.SortColumn = ko.observable();
    self.SortOrder = ko.observable();
    self.AllowFilter = ko.observable(false);
    self.AvailableSizes = ko.observableArray(config.sizes);
    self.Page = ko.observable(1);
    self.Size = ko.observable(config.sizeDefault);
    self.Total = ko.observable(0);
    self.TotalPages = ko.observable(0);

    self.Callback = function () {
        callback();
    };

    self.OnPageSizeChange = function () {
        self.Page(1);

        self.Callback();
    };

    self.OnPageChange = function () {
        if (self.Page() < 1) {
            self.Page(1);
        } else {
            if (self.Page() > self.TotalPages())
                self.Page(self.TotalPages());
        }

        self.Callback();
    };

    self.NextPage = function () {
        var page = self.Page();
        if (page < self.TotalPages()) {
            self.Page(page + 1);

            self.Callback();
        }
    };

    self.PreviousPage = function () {
        var page = self.Page();
        if (page > 1) {
            self.Page(page - 1);

            self.Callback();
        }
    };

    self.FirstPage = function () {
        self.Page(1);

        self.Callback();
    };

    self.LastPage = function () {
        self.Page(self.TotalPages());

        self.Callback();
    };

    self.Filter = function (data) {
        self.Page(1);

        self.Callback();
    };

    // get sorted column
    self.GetSortedColumn = function (columnName) {
        var sort = "sorting";

        if (self.SortColumn() == columnName) {
            if (self.SortOrder() == "DESC")
                sort = sort + "_asc";
            else
                sort = sort + "_desc";
        }

        return sort;
    };

    // Sorting data
    self.Sorting = function (column) {
        if (self.SortColumn() == column) {
            if (self.SortOrder() == "ASC")
                self.SortOrder("DESC");
            else
                self.SortOrder("ASC");
        } else {
            self.SortOrder("ASC");
        }

        self.SortColumn(column);

        self.Page(1);

        self.Callback();
    };
};


var FXModel = {cif : cifData,token : accessToken}
var AmountModel = {
    TotalPPUAmountsUSD: ko.observable(0),
    TotalDealAmountsUSD: ko.observable(0),
    RemainingBalance: ko.observable(0),
    TotalPPUUtilization : ko.observable(0),
    TotalDealUtilization : ko.observable(0)
}


var SPRole = {
    ID: ko.observable(),
    Name: ko.observable()
};


var SPUser = {
    DisplayName: ko.observable(),
    Email: ko.observable(),
    ID: ko.observable(),
    LoginName: ko.observable(),
    Roles: ko.observableArray([SPRole])
};

var TZModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable(),
    Class: ko.observable()
}

var BizSegmentModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var BranchModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var TypeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var ProductModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Name: ko.observable(),
    WorkflowID: ko.observable(),
    Workflow: ko.observable()
};

var ProductTypeModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    //IsFlowValas:ko.observable(),
    Description: ko.observable()
};

var LLDModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};

var ChannelModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var BankModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    BranchCode: ko.observable(),
    SwiftCode: ko.observable(),
    BankAccount: ko.observable(),
    Description: ko.observable(),
    CommonName: ko.observable(),
    Currency: ko.observable(),
    PGSL: ko.observable()
};

var RMModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    RankID: ko.observable(),
    Rank: ko.observable(),
    SegmentID: ko.observable(),
    Segment: ko.observable(),
    BranchID: ko.observable(),
    Branch: ko.observable(),
    Email: ko.observable()
};

var ContactModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    PhoneNumber: ko.observable(),
    DateOfBirth: ko.observable(),
    Address: ko.observable(),
    IDNumber: ko.observable()
};

var FunctionModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var CurrencyModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var DealUnderlyingModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};

var StatementLetterModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var RateTypeModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var AccountModel = {
    AccountNumber: ko.observable(),
    //Currency: ko.observable(CurrencyModel)
};

var UnderlyingDocumentModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Name: ko.observable()
};

var DocumentTypeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var FXComplianceModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var ChargesModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};

var DocumentPurposeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var UnderlyingModel = {
    ID: ko.observable(),
    UnderlyingDocument: ko.observable(UnderlyingDocumentModel),
    DocumentType: ko.observable(DocumentTypeModel),
    Currency: ko.observable(CurrencyModel),
    Amount: ko.observable(),
    Rate: ko.observable(),
    AmountUSD:ko.observable(),
    AttachmentNo: ko.observable(),
    IsStatementLetter: ko.observable(false),
    IsDeclarationOfException: ko.observable(false),
    StartDate: ko.observable(),
    EndDate: ko.observable(),
    DateOfUnderlying: ko.observable(),
    ExpiredDate: ko.observable(),
    ReferenceNumber: ko.observable(),
    SupplierName: ko.observable(),
    InvoiceNumber: ko.observable()
};

var DocumentModel = {
    ID: ko.observable(),
    Type: ko.observable(DocumentTypeModel),
    Purpose: ko.observable(DocumentPurposeModel),
    DocumentPath: ko.observable(),
    IsNewDocument:ko.observable(false),
    LastModifiedDate: ko.observable(new Date),
    LastModifiedBy: ko.observable()
};

var CustomerModel = {
    CIF: ko.observable(),
    Name: ko.observable(),
    POAName: ko.observable(),
    BizSegment: ko.observable(BizSegmentModel),
    Branch: ko.observable(BranchModel),
    Type: ko.observable(TypeModel),
    RM: ko.observable(RMModel),
    Contacts: ko.observableArray([ContactModel]),
    Functions: ko.observableArray([FunctionModel]),
    Accounts: ko.observableArray([AccountModel]),
    Underlyings: ko.observableArray([UnderlyingModel])
};

//add by fandi //retailcif
var ChangeRMModel = {
    ChangeRMTransactionID: ko.observable(),
    TransactionID: ko.observable(),
    CIF: ko.observable(),
    ChangeSegmentFrom: ko.observable(),
    ChangeSegmentTo: ko.observable(),
    ChangeFromBranchCode: ko.observable(),
    ChangeFromRMCode: ko.observable(),
    ChangeFromBU: ko.observable(),
    ChangeToBranchCode: ko.observable(),
    ChangeToRMCode: ko.observable(),
    ChangeToBU: ko.observable(),
    Account: ko.observable(),
    PCCode: ko.observable(),
    EATPB: ko.observable(),
    Remarks: ko.observable(),
};
var TransactionTypeModel = {
    TransTypeID: ko.observable(),
    TransactionTypeName: ko.observable()
}
var MaintenanceTypeModel = {
    MaintenanceTypeID: ko.observable(),
    MaintenanceTypeName: ko.observable(),
}
var DispatchModeTypeModel = {
    ID: ko.observable(),
    Name: ko.observable()
};
var BrachRiskRatingModel = {
    ID: ko.observable(),
    Name: ko.observable()
};
var CIFSUMBERDANAModel = {
    ID: ko.observable(),
    Name: ko.observable()
};
var CIFASSETBERSIHModel = {
    ID: ko.observable(),
    Name: ko.observable()
};
var CIFPENDPATANBULANANModel = {
    ID: ko.observable(),
    Name: ko.observable()
};
var CIFPENGHASILANTAMBAHANModel = {
    ID: ko.observable(),
    Name: ko.observable()
};
var CIFPEKERJAANModel = {
    ID: ko.observable(),
    Name: ko.observable()
};
var CIFTUJUANPEMBUKAANREKENINGModel = {
    ID: ko.observable(),
    Name: ko.observable()
};
var CIFPERKIRAANDANAMASUKModel = {
    ID: ko.observable(),
    Name: ko.observable()
};
var CIFPERKIRAANDANAKELUARModel = {
    ID: ko.observable(),
    Name: ko.observable()
};
var CIFPERKIRAANTRANSAKSIKELUARModel = {
    ID: ko.observable(),
    Name: ko.observable()
};
var CIFJENISIDENTITASModel = {
    ID: ko.observable(),
    Name: ko.observable()
};
var ModificationData = [{ ID: 1, Name: 'Add' }, { ID: 2, Name: 'Edit' }, { ID: 3, Name: 'Delete' }]
var MaritalStatusData = [{ ID: 0, Name: 'Belum Menikah' }, { ID: 1, Name: 'Duda / Janda' }, { ID: 2, Name: 'Sudah Menikah' }]
var DispatchModeData = [{ ID: 1, Name: 'BSPL Delivery and Email' }, { ID: 2, Name: 'Collect by Person' }, { ID: 3, Name: 'Email' }, { ID: 4, Name: 'No Dispatch' }, { ID: 5, Name: 'Post' }, { ID: 6, Name: 'SSPL Delivery' }]
var RiskRatingResultData = [{ ID: 0, Name: 'Low Risk' }, { ID: 1, Name: 'Medium Risk' }, { ID: 2, Name: 'High Risk' }]
//end

var TransactionModel = {
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    IsTopUrgent: ko.observable(false),
    Customer: ko.observable(CustomerModel),
    IsNewCustomer: ko.observable(false),
    Product: ko.observable(ProductModel),
    ProductType: ko.observable(ProductTypeModel),
    DebitCurrency: ko.observable(CurrencyModel),
    Currency: ko.observable(CurrencyModel),
    Amount: ko.observable(),
    Rate: ko.observable(),
    AmountUSD: ko.observable(),
    Channel: ko.observable(ChannelModel),
    Account: ko.observable(AccountModel),
    ApplicationDate: ko.observable(),
    BizSegment: ko.observable(BizSegmentModel),
    Bank: ko.observable(BankModel),
    IsSignatureVerified: ko.observable(false),
    IsDormantAccount: ko.observable(false),
    IsFreezeAccount: ko.observable(false),
    Others: ko.observable(),
    IsDraft: ko.observable(false),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([DocumentModel]), //DocumentModel
    DetailCompliance: ko.observable(),
    BeneName: ko.observable(),
    TZNumber: ko.observable(),
    LLD: ko.observable(LLDModel),
    IsOtherBeneBank: ko.observable(false),
    OtherBeneBankName: ko.observable(),
    OtherBeneBankSwift: ko.observable(),
    IsDocumentComplete: ko.observable(false),
    IsOtherAccountNumber: ko.observable(false),
    OtherAccountNumber: ko.observable(),
    SwapType: ko.observable(),
    IsNettingTransaction: ko.observable(),
    NB: ko.observable(),
    SwapTransactionID: ko.observable()
};

//add by fandi for transactionHistory
var GetTransactionMakerModel = {
    ID: ko.observable(),
    TransactionID: ko.observable(),
    Amount: ko.observable(),
    Rate: ko.observable(),
    AmountUSD: ko.observable(),
    DebitAccNumber: ko.observable(),
    DebitCurrencyCode: ko.observable(),
    BeneName: ko.observable(),
    BeneBankName: ko.observable(),
    BeneAccNumber: ko.observable(),
    BeneBankSwift: ko.observable(),
    BankChargesCode: ko.observable(),
    AgentChargesCode: ko.observable(),
    IsResident: ko.observable(false),
    IsCitizen: ko.observable(false),
    LLDDescription: ko.observable(),
    PaymentDetails: ko.observable(),
    LastModifiedBy: ko.observable(),
    LastModifiedDate: ko.observable(),

    WorkflowInstanceID: ko.observable(),
    ApplicationID: ko.observable(),
    IsTopUrgent: ko.observable(false),
    Customer: ko.observable(CustomerModel),
    IsNewCustomer: ko.observable(false),
    Product: ko.observable(ProductModel),
    ProductType: ko.observable(ProductTypeModel),
    DebitCurrency: ko.observable(CurrencyModel),
    Currency: ko.observable(CurrencyModel),
    Channel: ko.observable(ChannelModel),
    Account: ko.observable(AccountModel),
    ApplicationDate: ko.observable(),
    BizSegment: ko.observable(BizSegmentModel),
    Bank: ko.observable(BankModel),
    IsSignatureVerified: ko.observable(false),
    IsDormantAccount: ko.observable(false),
    IsFreezeAccount: ko.observable(false),
    Others: ko.observable(),
    IsDraft: ko.observable(false),
    CreateDate: ko.observable(new Date),
    DetailCompliance: ko.observable(),
    TZNumber: ko.observable(),
    LLD: ko.observable(LLDModel),
    IsOtherBeneBank: ko.observable(false),
    OtherBeneBankName: ko.observable(),
    OtherBeneBankSwift: ko.observable(),
    IsDocumentComplete: ko.observable(false),
    IsOtherAccountNumber: ko.observable(false),
    OtherAccountNumber: ko.observable(),
    BankID: ko.observable(),
    BankCharges: ko.observable(),
    AgentCharges: ko.observable(),
    LLDID: ko.observable()
};

var RetailCIFCBO = {//retail
    RetailCIFCBOID: ko.observable(),
    TransactionID: ko.observable(),
    DispatchModeTypeID: ko.observable(),
    AccountNumbe: ko.observable(),
    RequestTypeID: ko.observable(),
    MaintenanceTypeID: ko.observable(),
    IsNameMaintenance: ko.observable(false),
    IsIdentityTypeMaintenance: ko.observable(false),
    IsNPWPMaintenance: ko.observable(false),
    IsMaritalStatusMaintenance: ko.observable(false),
    IsCorrespondenceMaintenance: ko.observable(false),
    IsIdentityAddressMaintenance: ko.observable(false),
    IsOfficeAddressMaintenance: ko.observable(false),
    IsCorrespondenseAddressMaintenance: ko.observable(false),
    IsPhoneFaxEmailMaintenance: ko.observable(false),
    IsNationalityMaintenance: ko.observable(false),
    IsFundSourceMaintenance: ko.observable(false),
    IsNetAssetMaintenance: ko.observable(false),
    IsMonthlyIncomeMaintenance: ko.observable(false),
    IsJobMaintenance: ko.observable(false),
    IsAccountPurposeMaintenance: ko.observable(false),//add
    Name: ko.observable(),
    IdentityTypeID: ko.observable(),
    IdentityNumber: ko.observable(),
    IdentityStartDate: ko.observable(),
    IdentityEndDate: ko.observable(),
    IdentityAddress: ko.observable(),
    IdentityKelurahan: ko.observable(),
    IdentityKecamatan: ko.observable(),
    IdentityCity: ko.observable(),
    IdentityProvince: ko.observable(),
    IdentityCountry: ko.observable(),
    IdentityPostalCode: ko.observable(),
    NPWPNumber: ko.observable(),
    IsNPWPReceived: ko.observable(false),
    MaritalStatusID: ko.observable(),
    SpouseName: ko.observable(),
    IsCorrespondenseToEmail: ko.observable(false),
    CorrespondenseAddress: ko.observable(),
    CorrespondenseKelurahan: ko.observable(),
    CorrespondenseKecamatan: ko.observable(),
    CorrespondenseCity: ko.observable(),
    CorrespondenseProvince: ko.observable(),
    CorrespondenseCountry: ko.observable(),
    CorrespondensePostalCode: ko.observable(),
    CellPhoneMethodID: ko.observable(),
    CellPhone: ko.observable(),
    UpdatedCellPhone: ko.observable(),
    HomePhoneMethodID: ko.observable(),
    HomePhone: ko.observable(),
    UpdatedHomePhone: ko.observable(),
    OfficePhoneMethodID: ko.observable(),
    OfficePhone: ko.observable(),
    UpdatedOfficePhone: ko.observable(),
    FaxMethodID: ko.observable(),
    Fax: ko.observable(),
    UpdatedFax: ko.observable(),
    EmailAddress: ko.observable(),
    OfficeAddress: ko.observable(),
    OfficeKelurahan: ko.observable(),
    OfficeKecamatan: ko.observable(),
    OfficeCity: ko.observable(),
    OfficeProvince: ko.observable(),
    OfficeCountry: ko.observable(),
    OfficePostalCode: ko.observable(),
    Nationality: ko.observable(),
    FundSource: ko.observable(),
    UBOName: ko.observable(),
    UBOIdentityType: ko.observable(),
    UBOPhone: ko.observable(),
    UBOJob: ko.observable(),
    NetAsset: ko.observable(),
    MonthlyIncome: ko.observable(),
    MonthlyExtraIncome: ko.observable(),
    Job: ko.observable(),
    CompanyName: ko.observable(),
    Position: ko.observable(),
    WorkPeriod: ko.observable(),
    IndustryType: ko.observable(),
    AccountPurpose: ko.observable(),
    IncomeForecast: ko.observable(),
    OutcomeForecast: ko.observable(),
    TransactionForecast: ko.observable(),
    SubType: ko.observable(),
    IdentityType: ko.observable(),
    MaritalStatus: ko.observable(),
    FundSources: ko.observable(),
    NetAssets: ko.observable(),
    MonthlyIncomes: ko.observable(),
    MonthlyExtraIncomes: ko.observable(),
    Jobs: ko.observable(),
    AccountPurposes: ko.observable(),
    IncomeForecasts: ko.observable(),
    OutcomeForecasts: ko.observable(),
    TransactionForecasts: ko.observable(),
    SubTypes: ko.observable(),
    DispatchModeType: ko.observable(),
    AtmNumber: ko.observable(),
    RiskRatingReportDate: ko.observable(),
    RiskRatingNextReviewDate: ko.observable(),
    RiskRatingResultID: ko.observable(),
    Remarks: ko.observable(),
    MaintenanceTypes: ko.observable(),
    RequestTypes: ko.observable(),
    RiskRatingResult: ko.observable(),
    CreateDate: ko.observable(),
    CreateBy: ko.observable(),
    UpdateDate: ko.observable(),
    UpdateBy: ko.observable()
};

var TransactionSubType = {
    ID: ko.observable(),
    Name: ko.observable()
};

var TransactionCBOModel = {
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    IsTopUrgent: ko.observable(false),
    IsChainTopUrgent: ko.observable(false),
    Customer: ko.observable(CustomerModel),
    Product: ko.observable(ProductModel),
    TransactionType: ko.observable(TransactionTypeModel),
    MaintenanceType: ko.observable(MaintenanceTypeModel),
    DispatchModeType: ko.observable(DispatchModeData),
    StaffID: ko.observable(),
    ATMNumber: ko.observable(),
    TransactionSubType: ko.observable(TransactionSubType),
    CIFSUMBERDANA: ko.observable(CIFSUMBERDANAModel),
    CIFASSETBERSIH: ko.observable(CIFASSETBERSIHModel),
    CIFPENDPATANBULANAN: ko.observable(CIFPENDPATANBULANANModel),
    CIFPENGHASILANTAMBAHAN: ko.observable(CIFPENGHASILANTAMBAHANModel),
    CIFPEKERJAAN: ko.observable(CIFPEKERJAANModel),
    CIFTUJUANPEMBUKAANREKENING: ko.observable(CIFTUJUANPEMBUKAANREKENINGModel),
    CIFPERKIRAANDANAMASUK: ko.observable(CIFPERKIRAANDANAMASUKModel),
    CIFPERKIRAANDANAKELUAR: ko.observable(CIFPERKIRAANDANAKELUARModel),
    CIFPERKIRAANTRANSAKSIKELUAR: ko.observable(CIFPERKIRAANTRANSAKSIKELUARModel),
    CIFJENISIDENTITAS: ko.observable(CIFJENISIDENTITASModel),
    ValueDate: ko.observable(),
    IsSignatureVerified: ko.observable(false),
    IsDormantAccount: ko.observable(false),
    IsFreezeAccount: ko.observable(false),
    Others: ko.observable(),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([DocumentModel]),
    //RetailCIFCBO: ko.observable(CustomerModel),
    RetailCIFCBO: ko.observable(RetailCIFCBO),
    ChangeRMModel: ko.observableArray([ChangeRMModel]),
    AddJoinTableCustomerCIF: ko.observableArray([CustomerCIFModel]),
    AddJoinTableFFDAccountCIF: ko.observableArray([FFDAccountCIFModel]),
    AddJoinTableAccountCIF: ko.observableArray([AccountCIFModel]),
    AddJoinTableDormantCIF: ko.observableArray([DormantAccountCIFModel]),
    AddJoinTableFreezeUnfreezeCIF: ko.observableArray([FreezeUnfreezeAccountCIFModel])
};

var CustomerCIFModel = {
    CIF: ko.observable(),
    Name: ko.observable()
};

var AccountCIFModel = {
    AccountNumber: ko.observable(),
    IsAddFFDAccount: ko.observable(false)
};

var FFDAccountCIFModel = {
    AccountNumber: ko.observable(),
    Currency: ko.observable()
};

var DormantAccountCIFModel = {
    AccountNumber: ko.observable(),
    Currency: ko.observable(false)
};

var FreezeUnfreezeAccountCIFModel = {
    AccountNumber: ko.observable(),
    IsFreezeAccount: ko.observable()
};

//TMO by fandi
var TMOModel = {
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    Customer: ko.observable(CustomerModel),
    Product: ko.observable(ProductModel),
    Channel: ko.observable(ChannelModel),
    Currency: ko.observable(CurrencyModel),
    Amount: ko.observable(),
    DealNumber: ko.observable(),
    ValueDate: ko.observable(),
    TMODetails: ko.observable(),
    Remarks: ko.observable(),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([DocumentModel]),
};



//end

var PaymentModel = {
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    IsTopUrgent: ko.observable(false),
    Customer: ko.observable(CustomerModel),
    IsNewCustomer: ko.observable(false),
    Product: ko.observable(ProductModel),
    ProductType: ko.observable(ProductTypeModel),
    Currency: ko.observable(CurrencyModel),
    DebitCurrency: ko.observable(CurrencyModel),
    Amount: ko.observable(),
    Rate: ko.observable(),
    AmountUSD: ko.observable(),
    Channel: ko.observable(ChannelModel),
    Account: ko.observable(AccountModel),
    ApplicationDate: ko.observable(),
    BizSegment: ko.observable(BizSegmentModel),
    Bank: ko.observable(BankModel),
    BankCharges: ko.observable(ChargesModel),
    AgentCharges: ko.observable(ChargesModel),
    Type: ko.observable(),
    IsCitizen: ko.observable(false),
    IsResident: ko.observable(false),
    /*LLDCode: ko.observable(),
     LLDInfo: ko.observable(),*/
    PaymentDetails: ko.observable(),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([DocumentModel]), //DocumentModel
    DetailCompliance: ko.observable(),
    BeneName: ko.observable(),
    TZNumber: ko.observable(),
    FXCompliance: ko.observable(FXComplianceModel),
    LLD: ko.observable(LLDModel),
    IsOtherBeneBank: ko.observable(false)

};

var CheckerModel = {
    ID: ko.observable(0),
    LLD: ko.observable(LLDModel)
    /*LLDCode: ko.observable(""),
     LLDInfo: ko.observable("")*/
};

//bangkit 20141119
var CallBackModel = {
    ID: ko.observable(0),
    ApproverID: ko.observable(""),
    Contact: ko.observable(ContactModel),
    Time: ko.observable(new Date),
    Remark: ko.observable("")
};
//Chandra 2015.02.24
var DocumentPurposeModel =  {
    ID : ko.observable(0),
    Name :ko.observable(""),
    Description : ko.observable("")
}


var VerifyModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    IsVerified: ko.observable()
};

var TimelineModel = {
    ApproverID: ko.observable(),
    Activity: ko.observable(),
    Time: ko.observable(),
    Outcome: ko.observable(),
    UserOrGroup: ko.observable(),
    IsGroup: ko.observable(),
    DisplayName: ko.observable(),
    Comment: ko.observable()
};

var TransactionDealModel = function () {
    this.Transaction = ko.observable(TransactionDealDetailModel);
    this.Verify = ko.observableArray([VerifyModel]);
};

var TransactionMakerModel = function () {
    this.Transaction = ko.observable(TransactionModel);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};

var TransactionCheckerModel = function () {
    this.Transaction = ko.observable(TransactionModel);
    this.Checker = ko.observable(CheckerModel);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};

var TransactionAllNormalModel = function () {
    this.Transaction = ko.observable(TransactionModel);
    this.Checker = ko.observable(CheckerModel);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
    this.Callbacks = ko.observableArray([CallBackModel]);
    this.Payment = ko.observable(PaymentModel);
    //add by fandi transaction histroty
    this.TransactionMaker = ko.observableArray([GetTransactionMakerModel]);
    //end
};

//add by fandi transaction history
var TransactionHistoryModel = function () {
    this.Transaction = ko.observable(TransactionModel);
    this.TransactionMaker = ko.observableArray([GetTransactionMakerModel]);
    this.Payment = ko.observable(TransactionModel);
}

// for retail cif
var TransactionCBO = function () {
    this.Transaction = ko.observable(TransactionCBOModel);
    this.Checker = ko.observable(CheckerModel);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};
//end 

//for TMO
var TransactionCheckerTMOModel = function () {
    this.Transaction = ko.observable(TMOModel);
    this.Checker = ko.observable(CheckerModel);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};
//end

var TransactionCheckerCallbackModel = function () {
    this.Transaction = ko.observable(TransactionModel);
    this.Calback = ko.observable();
    this.Checker = ko.observable(CheckerModel);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};

//bangkit 20141119
var TransactionCallbackModel = function () {
    this.Transaction = ko.observable(TransactionModel);
    //this.Checker = ko.observable(CheckerModel);
    this.Callbacks = ko.observableArray([CallBackModel]);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};

var TransactionContactModel = function () {
    this.Contacts = ko.observableArray([ContactModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};

var PaymentMakerModel = function () {
    this.Payment = ko.observable(PaymentModel);
    this.Timelines = ko.observableArray([TimelineModel]);
};

var PaymentCheckerModel = function () {
    this.Payment = ko.observable(PaymentModel);
    this.Timelines = ko.observableArray([TimelineModel]);
    this.Verify = ko.observableArray([VerifyModel]);
};

/*var TransactionCheckerModel = function () {
 this.Transaction = ko.observable(TransactionDealDetailModel);
 this.Checker = ko.observable(CheckerModel);
 this.Verify = ko.observableArray([VerifyModel]);
 this.Timelines = ko.observableArray([TimelineModel]);
 };*/

var Documents = {
    Documents: self.Documents,
    DocumentPath: self.DocumentPath_a,
    Type: self.DocumentType_a,
    Purpose: self.DocumentPurpose_a
}


var CustomerUnderlyingMappingModel = {
    ID : ko.observable(0),
    UnderlyingID : ko.observable(0),
    UnderlyingFileID : ko.observable(0),
    AttachmentNo :ko.observable("")
}

var ProductTypeModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    IsFlowValas: ko.observable(),
    Description: ko.observable()
};

var TransactionDealDetailModel = {
    ValueDate: ko.observable(),
    AccountNumber: ko.observable(),
    StatementLetter: ko.observable(StatementLetterModel),
    ProductType: ko.observable(ProductTypeModel),
    TZNumber: ko.observable(),
    RateTypeID: ko.observable(),
    Rate: ko.observable(),
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    Customer: ko.observable(CustomerModel),
    Currency: ko.observable(CurrencyModel),
    DebitCurrency: ko.observable(CurrencyModel),
    Amount: ko.observable(),
    Rate: ko.observable(),
    AmountUSD: ko.observable(),
    Account: ko.observable(AccountModel),
    ValueDate: ko.observable(),
    Type: ko.observable(20),
    CreateDate: ko.observable(new Date),
    //CreateBy: ko.observable(spUser.DisplayName),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([]), //DocumentModel
    DetailCompliance: ko.observable(),
    BeneName: ko.observable(),
    BeneAccNumber: ko.observable(),
    TZRef: ko.observable(),
    utilizationAmount: ko.observable(0.00),
    underlyings: ko.observableArray([]),
    TZReference: ko.observable(),
    IsBookDeal: ko.observable(false),
    bookunderlyingamount: ko.observable(),
    bookunderlyingcurrency: ko.observable(),
    bookunderlyingcode: ko.observable(),
    CreateBy: ko.observable(),
    AccountCurrencyCode: ko.observable(),
    IsFxTransaction: ko.observable(false),
    OtherUnderlying: ko.observable(),
    bookunderlyingdoccode: ko.observable(),
    SwapType: ko.observable(),
    IsNettingTransaction: ko.observable(),
    NB: ko.observable(),
    SwapTransactionID : ko.observable()

    //TZRef: ko.observable()
    //UnderlyingGridProperties : self.UnderlyingGridProperties(new GridPropertiesModel(GetDataUnderlying))

};

var SelectedModel = {
    Product: ko.observable(),
    Currency: ko.observable(),
    Channel: ko.observable(),
    Account: ko.observable(),
    BizSegment: ko.observable(),
    Bank: ko.observable(),
    DocumentType: ko.observable(),
    DocumentPurpose: ko.observable(),
    NewCustomer: ko.observable({
        Currency: ko.observable(),
        BizSegment: ko.observable()
    }),
    BankCharges: ko.observable(2),
    AgentCharges: ko.observable(2),
    FXCompliaance: ko.observable(),
    LLD: ko.observable(),
    POAFunction: ko.observable(),
    ProductType: ko.observable(),
    UnderlyingDoc: ko.observable(),
    DocumentPurpose_a:ko.observable(),
    DocumentType_a: ko.observable(),
    DebitCurrency: ko.observable()
};

var statusValueModel = function (value, status) {
    var self = this;
    self.Value = ko.observable(value);
    self.Status = ko.observable(status);
}

// Original Value PPU Maker -- chandra a

var OriginalValueModel = function (amountOri, amountUSDOri, rateOri, beneNameOri, banksOri, beneAccNumberOri, swiftCodeOri, bankChargesOri, agentChargesOri, isCitizenOri, isResidentOri) {
    var self = this;
    self.AmountOri = ko.observable(amountOri);
    self.AmountUSDOri = ko.observable(amountUSDOri);
    self.RateOri = ko.observable(rateOri);
    self.BeneNameOri = ko.observable(beneNameOri);
    self.BanksOri = ko.observable(banksOri);
    self.BeneAccNumberOri = ko.observable(beneAccNumberOri);
    //self.AccountOri = ko.observable(accountOri);
    self.SwiftCodeOri = ko.observable(swiftCodeOri);
    self.BankChargesOri = ko.observable(bankChargesOri);
    self.AgentChargesOri = ko.observable(agentChargesOri);
    self.IsCitizenOri = ko.observable(isCitizenOri);
    self.IsResidentOri = ko.observable(isResidentOri);
}

var StatementModel = {
    ID: ko.observable(0),
    Name: ko.observable("")
};

var NettingPurposeModel = {
    ID: ko.observable(),
    name: ko.observable()
};

var TransactionMakerNettingModel = function () {
    this.Transaction = ko.observable(TransactionNettingModel);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};

var TransactionCheckerNettingModel = function () {
    this.Transaction = ko.observable(TransactionNettingModel);
    this.Checker = ko.observable(CheckerModel);
    this.Verify = ko.observableArray([VerifyModel]);
    this.Timelines = ko.observableArray([TimelineModel]);
};


var TransactionNettingModel = {
    AccountNumber: ko.observable(),
    StatementLetter: ko.observable(StatementModel),
    Product: ko.observable(ProductModel),
    TZReference: ko.observable(),
    IDTZReference: ko.observable(),
    MurexNumber: ko.observable(),
    SwapDealNumber: ko.observable(),
    NettingTZReference: ko.observable(),
    IDNettingTZReference: ko.observable(),
    ExpectedDateStatementLetter: ko.observable(),
    ExpectedDateUnderlying: ko.observable(),
    ActualDateStatementLetter: ko.observable(),
    ActualDateUnderlying: ko.observable(),
    RateType: ko.observable(),
    ProductType: ko.observable(ProductTypeModel),
    Rate: ko.observable(),
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    Customer: ko.observable(CustomerModel),
    Currency: ko.observable(CurrencyModel),
    AmountUSD: ko.observable(0),
    Account: ko.observable(AccountModel),
    Type: ko.observable(20),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([]),
    utilizationAmount: ko.observable(0.00),
    underlyings: ko.observableArray([]),
    IsDraft: ko.observable(false),
    OtherUnderlying: ko.observable(),
    bookunderlyingamount: ko.observable(),
    bookunderlyingcurrency: ko.observable(),
    bookunderlyingcode: ko.observable(),
    bookunderlyingdesc: ko.observable(),
    Remarks: ko.observable(),
    IsTMO: ko.observable(true),
    IsNettingTransaction: ko.observable(true),
    NettingPurpose: ko.observable(NettingPurposeModel)
};