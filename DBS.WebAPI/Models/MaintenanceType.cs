﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Models
{
    #region Property
    [ModelName("MaintenanceType")]

    public class MaintenanceTypeModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public DateTime? LastModifiedDate { get; set; }     
        public string LastModifiedBy { get; set; }
    }

    public class StaffTaggingModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }

    public class MaintenanceTypeRow : MaintenanceTypeModel
    {
        public int RowID { get; set; }

    }

    public class MaintenanceTypeGrid : Grid
    {
        public IList<MaintenanceTypeRow> Rows { get; set; }
    }
    #endregion

    #region Filter
    public class MaintenanceTypeFilter : Filter { }

    #endregion

    #region Interface
    public interface IMaintenanceTypeRepository : IDisposable
    {
        bool GetMaintenanceTypeByID(int id, ref MaintenanceTypeModel MaintenanceType, ref string message);
        bool GetMaintenanceType(ref IList<MaintenanceTypeModel> MaintenanceTypes, int limit, int index, ref string message);
        bool GetMaintenanceType(ref IList<MaintenanceTypeModel> MaintenanceTypes, ref string message);
        bool GetMaintenanceType(int page, int size, IList<MaintenanceTypeFilter> filters, string sortColumn, string sortOrder, ref MaintenanceTypeGrid MaintenanceType, ref string message);
        bool GetMaintenanceType(MaintenanceTypeFilter filter, ref IList<MaintenanceTypeModel> MaintenanceTypes, ref string message);
        bool GetMaintenanceType(string key, int limit, ref IList<MaintenanceTypeModel> MaintenanceTypes, ref string message);
        bool AddMaintenanceType(MaintenanceTypeModel MaintenanceType, ref string message);
        bool UpdateMaintenanceType(int id, MaintenanceTypeModel MaintenanceType, ref string message);
        bool DeleteMaintenanceType(int id, ref string message);
    }
    #endregion

    #region Repository
    public class MaintenanceTypeRepository : IMaintenanceTypeRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetMaintenanceTypeByID(int id, ref MaintenanceTypeModel MaintenanceType, ref string message)
        {
            bool isSuccess = false;

            try
            {
                MaintenanceType = (from a in context.MaintenanceTypes                          
                           where a.IsDeleted.Equals(false) && a.CBOMaintainID.Equals(id)
                           select new MaintenanceTypeModel
                           {
                               ID = a.CBOMaintainID,
                               Name = a.MaintenanceTypeName,                              
                               LastModifiedDate = a.CreateDate,
                               LastModifiedBy = a.CreateBy
                           }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetMaintenanceType(ref IList<MaintenanceTypeModel> MaintenanceTypes, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    MaintenanceTypes = (from a in context.MaintenanceTypes
                               //join b in context.ProductWorkflows on a.ProductWorkflowID equals b.WorkflowID
                               where a.IsDeleted.Equals(false)
                               orderby new { a.CBOMaintainID }
                               select new MaintenanceTypeModel
                               {
                                   ID = a.CBOMaintainID,
                                   Name = a.MaintenanceTypeName,
                                   LastModifiedDate = a.CreateDate,
                                   LastModifiedBy = a.CreateBy
                               }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetMaintenanceType(ref IList<MaintenanceTypeModel> MaintenanceTypes, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    MaintenanceTypes = (from a in context.MaintenanceTypes
                               //join b in context.ProductWorkflows on a.ProductWorkflowID equals b.WorkflowID
                               where a.IsDeleted.Equals(false) 
                               orderby new { a.CBOMaintainID }
                               select new MaintenanceTypeModel
                               {
                                   ID = a.CBOMaintainID,
                                   Name = a.MaintenanceTypeName,
                                   LastModifiedDate = a.CreateDate,
                                   LastModifiedBy = a.CreateBy
                               }).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetMaintenanceType(int page, int size, IList<MaintenanceTypeFilter> filters, string sortColumn, string sortOrder, ref MaintenanceTypeGrid MaintenanceType, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                MaintenanceTypeModel filter = new MaintenanceTypeModel();
                if (filters != null)
                {
                    filter.ID = int.Parse(filters.Where(a => a.Field.Equals("ID")).Select(a => a.Value).SingleOrDefault());
                    filter.Name = filters.Where(a => a.Field.Equals("Name")).Select(a => a.Value).SingleOrDefault();                    
                    filter.LastModifiedBy = filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse(filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.MaintenanceTypes
                                //join b in context.ProductWorkflows on a.ProductWorkflowID equals b.WorkflowID
                                //let isID = !int.Equals(int.Parse(filter.ID))
                                let isName = !string.IsNullOrEmpty(filter.Name)                              
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) &&
                                   // (isCode ? a.ProductCode.Contains(filter.Code) : true) &&
                                    (isName ? a.MaintenanceTypeName.Contains(filter.Name) : true) &&                                   
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new MaintenanceTypeModel
                                {
                                    ID = a.CBOMaintainID,                                   
                                    Name = a.MaintenanceTypeName,                                   
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    MaintenanceType.Page = page;
                    MaintenanceType.Size = size;
                    MaintenanceType.Total = data.Count();
                    MaintenanceType.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    MaintenanceType.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new MaintenanceTypeRow
                        {
                            RowID = i + 1,
                            ID = a.ID,                         
                            Name = a.Name,                          
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetMaintenanceType(MaintenanceTypeFilter filter, ref IList<MaintenanceTypeModel> MaintenanceTypes, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetMaintenanceType(string key, int limit, ref IList<MaintenanceTypeModel> MaintenanceType, ref string message)
        {
            bool isSuccess = false;

            try
            {
                MaintenanceType = (from a in context.MaintenanceTypes
                           //join b in context.ProductWorkflows on a.ProductWorkflowID equals b.WorkflowID
                           where a.IsDeleted.Equals(false) &&
                                (a.CBOMaintainID.Equals(key) || a.MaintenanceTypeName.Contains(key))
                           select new MaintenanceTypeModel
                           {
                               ID = a.CBOMaintainID,
                               Name = a.MaintenanceTypeName,                             
                               LastModifiedDate = a.CreateDate,
                               LastModifiedBy = a.CreateBy
                           }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddMaintenanceType(MaintenanceTypeModel MaintenanceType, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (!context.MaintenanceTypes.Where(a => a.CBOMaintainID.Equals(MaintenanceType.ID) && a.IsDeleted.Equals(false)).Any())
                {
                    context.MaintenanceTypes.Add(new Entity.MaintenanceType()
                    {
                        CBOMaintainID = MaintenanceType.ID,
                        MaintenanceTypeName = MaintenanceType.Name,                        
                        IsDeleted = false,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    });

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Maintenance Type data for ID {0} is already exist.", MaintenanceType.ID);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateMaintenanceType(int id, MaintenanceTypeModel MaintenanceType, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.MaintenanceType data = context.MaintenanceTypes.Where(a => a.CBOMaintainID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    if (context.MaintenanceTypes.Where(a => a.CBOMaintainID != MaintenanceType.ID && a.IsDeleted.Equals(false)).Count() >= 1)
                    {
                        message = string.Format("Maintenance Type ID {0} is exist.", MaintenanceType.ID);
                    }
                    else
                    {
                        data.CBOMaintainID = MaintenanceType.ID;
                        data.MaintenanceTypeName = MaintenanceType.Name;                  
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        context.SaveChanges();

                        isSuccess = true;
                    }
                }
                else
                {
                    message = string.Format("Maintenance Type data for ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeleteMaintenanceType(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.MaintenanceType data = context.MaintenanceTypes.Where(a => a.CBOMaintainID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("MaintenanceType data for ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

       
    }
    #endregion
}