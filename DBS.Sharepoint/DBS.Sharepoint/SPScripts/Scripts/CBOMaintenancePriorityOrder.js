﻿
var ViewModel = function () {
    var self = this;
    self.Readonly = ko.observable(false);
    self.IsWorkflow = ko.observable(false);


    // self.SavePriority = ko.observable("");
    self.MaintenanceTypeNamesDrop = ko.observableArray([]);
    self.MaintenanceTypeNamesPriorityDrop = ko.observableArray([]);


    self.IsRoleMaker = ko.observable(false);
    self.IsPermited = ko.observable(false);

    // New Data flag
    self.IsNewData = ko.observable(false);
    //Data
    var CBOMaintenancePriorityOrder = {
        CBOMaintenancePriorityOrder: self.MaintenanceTypeNamesPriorityDrop
    };
    // Declare an ObservableArray for Storing the JSON Response
    self.MaintainID = ko.observableArray([]);
    self.OrderNo = ko.observableArray([]);
    self.MaintenanceTypeName = ko.observableArray([]);
    self.MaintenanceTypeNamePriority = ko.observableArray([]);
    self.SelectedMaintenanceTypeName = ko.observableArray([]);
    self.SelectedMaintenanceTypeNamePriority = ko.observableArray([]);
    self.MaintenanceTypeNamePriorityApproval = ko.observableArray([]);


    self.searchParameter = ko.observable("");

    // bind get data function to viewget
    //self.GetData = function () {
    //    GetData();
    //};

    self.IsPermited = function (IsPermitedResult) {

        spUser = $.cookie(api.cookie.spUser);
        console.log(spUser);
        console.log(_spFriendlyUrlPageContextInfo.title);
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/CheckUserPermited/" + _spFriendlyUrlPageContextInfo.title,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    //compare user role to page permission to get checker validation
                    console.log(data);
                    for (i = 0; i < spUser.Roles.length; i++) {
                        for (j = 0; j < data.length; j++) {
                            if (Const_RoleName[spUser.Roles[i].ID] == data[j]) { //if (spUser.Roles[i].Name == data[j]) {
                                if (Const_RoleName[spUser.Roles[i].ID].endsWith('Maker')) { //if (spUser.Roles[i].Name.endsWith('Maker')) {
                                    console.log(Const_RoleName[spUser.Roles[i].ID].endsWith('Maker'));
                                    self.IsRoleMaker(true);
                                    return;
                                }
                                else {
                                    self.IsRoleMaker(false);
                                }
                            }
                        }
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    };
    console.log(self.IsRoleMaker());
    self.GetDropdown = function () {

        GetDropdown();
    };

    function GetDropdown() {
        $.ajax({
            async: false,
            type: "GET",
            url: api.server + api.url.parameter + '?select=MaintenanceTypeNames,MaintenanceTypeNamePrioritys',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                console.log(data)
                if (jqXHR.status == 200) {
                    console.log(data['MaintenanceTypeNames']);
                    console.log(data['MaintenanceTypeNamePrioritys']);
                    console.log(self.IsRoleMaker());
                    self.MaintenanceTypeNamesDrop(data['MaintenanceTypeNames']);
                    self.MaintenanceTypeNamesPriorityDrop(data['MaintenanceTypeNamePrioritys']);
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }

        });

    }

    self.save = function () {
        // validation

        var form = $("#aspnetForm");
        form.validate();
        if (form.valid) {
            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {
                    console.log(ko.toJSON(self.MaintenanceTypeNamesPriorityDrop));

                    $.ajax({
                        type: "POST",
                        url: api.server + api.url.cbomaintenancepriorityOrder,
                        data: ko.toJSON(CBOMaintenancePriorityOrder.CBOMaintenancePriorityOrder), //Convert the Observable Data into JSON
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {
                                // send notification                                
                                ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // hide current popup window
                                $("#modal-form").modal('hide');

                                // refresh data
                                //GetData();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });


                }

            });



        }

    };

    self.pullLeft = function () {
        var sel = self.SelectedMaintenanceTypeNamePriority();
        console.log(sel);
        for (var i = 0; i < sel.length; i++) {
            var selCat = sel[i];
            var result = self.MaintenanceTypeNamesPriorityDrop.remove(function (item) {
                return item.CBOMaintainPriority == selCat;
            });
            //  $('.MaintenanceTypeNamePriority' + "option:selCat").clone().appendTo('.MaintenanceTypeName');
            if (result && result.length > 0) {
                self.MaintenanceTypeNamesDrop.push(result[0]);
                console.log(ko.toJSON(self.MaintenanceTypeNamesDrop));
            }
        }
        self.SelectedMaintenanceTypeNamePriority.removeAll();
        console.log(ko.toJSON(self.MaintenanceTypeNamesDrop));


    }.bind(this);
    self.pullRight = function () {
        var sel = self.SelectedMaintenanceTypeName();
        for (var i = 0; i < sel.length; i++) {
            var selCat = sel[i];
            var result = self.MaintenanceTypeNamesDrop.remove(function (item) {
                return item.CBOMaintainPriority == selCat;
            });
            // $('.MaintenanceTypeName' + "option:selCat").appendTo('.MaintenanceTypeNamePriority');
            if (result && result.length > 0) {
                self.MaintenanceTypeNamesPriorityDrop.push(result[0]);
                console.log(ko.toJSON(self.MaintenanceTypeNamesPriorityDrop));
            }
        }
        self.SelectedMaintenanceTypeName.removeAll();
        console.log(ko.toJSON(self.MaintenanceTypeNamesPriorityDrop));



    }.bind(this);

    $.holdReady(true);
    var intervalRoleName = setInterval(function () {
        if (Object.keys(Const_RoleName).length != 0) {
            $.holdReady(false);
            clearInterval(intervalRoleName);
        }
    }, 100);
    //Pull UP / D
    //$(document).on("RoleNameReady", function() {
    $(document).ready(function () {
        $('input[type="button"]').click(function () {
            //console.log(ko.toJSON(self.MaintenanceTypeNamesPriorityDrop));
            var $op = $('#MaintenanceTypeNamePriority option:selected'),
                $this = $(this);
            if ($op.length) {
                console.log($this.val());
                if ($this.val() == 'Λ') {
                    $op.first().prev().before($op);
                } else {
                    $op.last().next().after($op);
                }
            }

            var optionValues = [];

            $('#MaintenanceTypeNamePriority option').each(function () {
                optionValues.push($(this).val());
            });
            //console.log(optionValues);
            var tempArr1 = self.MaintenanceTypeNamesPriorityDrop();
            var tempArr2 = [];
            for (var ii = 0; ii < optionValues.length; ii++) {
                for (var iii = 0; iii < tempArr1.length; iii++) {
                    if (optionValues[ii] == tempArr1[iii].CBOMaintainPriority) {
                        tempArr2.push(tempArr1[iii]);
                    }
                }
            }
            //console.log(tempArr2);
            self.MaintenanceTypeNamesPriorityDrop([]);
            self.MaintenanceTypeNamesPriorityDrop(tempArr2);
            //console.log(ko.toJSON(self.MaintenanceTypeNamesPriorityDrop));
        });
    });



    self.UpdateMaintenanceTypeName = function () {
        // validation
        IsvalidField();
        var form = $("#aspnetForm");
        form.validate();

        if (self.selectedDays().length == 0) {
            alert('Days of Ageing must be selected');
            return;
        }

        self.Days(self.selectedDays().join(","));

        if (form.valid()) {
            //Ajax call to insert the AgeingReportParameter
            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {

                    $.ajax({
                        type: "POST",
                        url: api.server + api.url.ageingreport,
                        data: ko.toJSON(AgeingReportParameter), //Convert the Observable Data into JSON
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        contentType: "application/json",
                        success: function (data, textStatus, jqXHR) {
                            // send notification
                            ShowNotification('Create Success', jqXHR.responseText, 'gritter-success', false);

                            // hide current popup window
                            $("#modal-form").modal('hide');

                            // refresh data
                            //GetData();
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification('An Error Occured', jqXHR.responseText, 'gritter-error', false);
                        }
                    });
                }
            });
        }
    };


    // Local Date
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return ""
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-17
    };

    //Function to Display record to be updated
    //self.GetSelectedRow = function (data) {
    //    //fandi
    //    //var count = data.length();

    //    self.IsNewData(false);
    //    if (self.IsWorkflow()) $("#modal-form-workflow").modal('show');
    //    else $("#modal-form").modal('show');

    //    for (i = 0; i < data.length; i++)
    //    {
    //       // text += self.MaintenanceTypeName(data[i].MaintenanceTypeName) + "<br>";
    //       self.MaintenanceTypeName(data[i].MaintenanceTypeName);
    //        //document.getElementById("MaintenanceTypeName").innerHTML = text;

    //        //console.log(data[i].MaintenanceTypeName);
    //      //  self.MaintenanceTypeName(data.MaintenanceTypeName);
    //    }

    //};
    self.GetSelectedRow = function (data) {

        self.IsNewData(false);
        if (self.IsWorkflow()) $("#modal-form-workflow").modal('show');
        else $("#modal-form").modal('show');

        if (ko.toJSON(self.Readonly() == false)) {
            self.MaintenanceTypeNamePriorityApproval(data);
            console.log(data);

        }

    };

}


// On Error callback
function OnError(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}

// On Always callback
function OnAlways() {
    $box.trigger('reloaded.ace.widget');
}
$('#aspnetForm').validate({
    errorElement: 'div',
    errorClass: 'help-block',
    focusInvalid: true,
    highlight: function (e) {
        $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
    },

    success: function (e) {
        $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
        $(e).remove();
    },

    errorPlacement: function (error, element) {
        if (element.is(':checkbox') || element.is(':radio')) {
            var controls = element.closest('div[class*="col-"]');
            if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
            else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
        }
        else if (element.is('.select2')) {
            error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
        }
        else if (element.is('.chosen-select')) {
            error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
        }
        else error.insertAfter(element.parent());
    }

});


//$(document).ready(function () {
//    window.onmousedown = function (e) {
//        var el = e.target;
//        if (el.tagName.toLowerCase() == 'option' && el.parentNode.hasAttribute('multiple')) {
//            e.preventDefault();

//            // toggle selection
//            if (el.hasAttribute('selected')) el.removeAttribute('selected');
//            else el.setAttribute('selected', '');

//            // hack to correct buggy behavior
//            var select = el.parentNode.cloneNode(true);
//            el.parentNode.parentNode.replaceChild(select, el.parentNode);
//        }
//    }


//});
