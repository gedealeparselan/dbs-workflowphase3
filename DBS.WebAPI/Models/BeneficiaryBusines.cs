﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Models
{
    #region Property
    [ModelName("BeneficiaryBusines")]
    public class BeneficiaryBusinesModel
    {
        /// <summary>
        /// Beneficiary Busines ID.
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// BeneficiaryBusines Code.
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// BeneficiaryBusines Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// BeneficiaryBusines Description.
        /// </summary>     
        public string Description { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }
    }

    public class BeneficiaryBusinesRow : BeneficiaryBusinesModel
    {
        public int RowID { get; set; }

    }

    public class BeneficiaryBusinesGrid : Grid
    {
        public IList<BeneficiaryBusinesRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class BeneficiaryBusinesFilter : Filter { }
    #endregion

    #region Interface
    public interface IBeneficiaryBusinesRepository : IDisposable
    {
        bool GetBeneficiaryBusinesByID(int id, ref BeneficiaryBusinesModel BeneficiaryBusines, ref string message);
        bool GetBeneficiaryBusines(ref IList<BeneficiaryBusinesModel> BeneficiaryBusiness, int limit, int index, ref string message);
        bool GetBeneficiaryBusines(ref IList<BeneficiaryBusinesModel> BeneficiaryBusiness, ref string message);
        bool GetBeneficiaryBusines(int page, int size, IList<BeneficiaryBusinesFilter> filters, string sortColumn, string sortOrder, ref BeneficiaryBusinesGrid BeneficiaryBusines, ref string message);
        bool GetBeneficiaryBusines(BeneficiaryBusinesFilter filter, ref IList<BeneficiaryBusinesModel> BeneficiaryBusiness, ref string message);
        bool GetBeneficiaryBusines(string key, int limit, ref IList<BeneficiaryBusinesModel> BeneficiaryBusiness, ref string message);
        bool AddBeneficiaryBusines(BeneficiaryBusinesModel BeneficiaryBusines, ref string message);
        bool UpdateBeneficiaryBusines(int id, BeneficiaryBusinesModel BeneficiaryBusines, ref string message);
        bool DeleteBeneficiaryBusines(int id, ref string message);
    }
    #endregion

    #region Repository
    public class BeneficiaryBusinesRepository : IBeneficiaryBusinesRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetBeneficiaryBusinesByID(int id, ref BeneficiaryBusinesModel BeneficiaryBusines, ref string message)
        {
            bool isSuccess = false;

            try
            {
                BeneficiaryBusines = (from a in context.BeneficiaryBusinessTypes
                               where a.IsDeleted.Equals(false) && a.BeneficiaryBusinessTypeID.Equals(id)
                               select new BeneficiaryBusinesModel
                               {
                                   ID = a.BeneficiaryBusinessTypeID,
                                   Code = a.BeneficiaryBusinessTypeCode,
                                   Name = a.BeneficiaryBusinessTypeName,
                                   Description = a.BeneficiaryBusinessTypeDescription,
                                   LastModifiedDate = a.CreateDate,
                                   LastModifiedBy = a.CreateBy
                               }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetBeneficiaryBusines(ref IList<BeneficiaryBusinesModel> BeneficiaryBusiness, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    BeneficiaryBusiness = (from a in context.BeneficiaryBusinessTypes
                                    where a.IsDeleted.Equals(false)
                                           orderby new { a.BeneficiaryBusinessTypeName, a.BeneficiaryBusinessTypeDescription }
                                    select new BeneficiaryBusinesModel
                                    {
                                        ID = a.BeneficiaryBusinessTypeID,
                                        Code = a.BeneficiaryBusinessTypeCode,
                                        Name = a.BeneficiaryBusinessTypeName,
                                        Description = a.BeneficiaryBusinessTypeDescription,
                                        LastModifiedBy = a.CreateBy,
                                        LastModifiedDate = a.CreateDate
                                    }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetBeneficiaryBusines(ref IList<BeneficiaryBusinesModel> BeneficiaryBusiness, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    BeneficiaryBusiness = (from a in context.BeneficiaryBusinessTypes
                                    where a.IsDeleted.Equals(false)
                                    orderby new { a.BeneficiaryBusinessTypeID}
                                    select new BeneficiaryBusinesModel
                                    {
                                        ID = a.BeneficiaryBusinessTypeID,
                                        Code = a.BeneficiaryBusinessTypeCode,
                                        Name = a.BeneficiaryBusinessTypeName,
                                        Description = a.BeneficiaryBusinessTypeDescription,
                                        LastModifiedBy = a.CreateBy,
                                        LastModifiedDate = a.CreateDate
                                    }).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetBeneficiaryBusines(int page, int size, IList<BeneficiaryBusinesFilter> filters, string sortColumn, string sortOrder, ref BeneficiaryBusinesGrid BeneficiaryBusinesGrid, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                BeneficiaryBusinesModel filter = new BeneficiaryBusinesModel();
                if (filters != null)
                {
                    filter.Name = (string)filters.Where(a => a.Field.Equals("Name")).Select(a => a.Value).SingleOrDefault();
                    filter.Description = (string)filters.Where(a => a.Field.Equals("Description")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.BeneficiaryBusinessTypes
                                let isBeneficiaryBusinesName = !string.IsNullOrEmpty(filter.Name)
                                let isBeneficiaryBusinesCode = !string.IsNullOrEmpty(filter.Code)
                                let isBeneficiaryBusinesDescription = !string.IsNullOrEmpty(filter.Description)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) &&
                                    (isBeneficiaryBusinesName ? a.BeneficiaryBusinessTypeName.Contains(filter.Name) : true) &&
                                    (isBeneficiaryBusinesDescription ? a.BeneficiaryBusinessTypeDescription.Contains(filter.Description) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new BeneficiaryBusinesModel
                                {
                                    ID = a.BeneficiaryBusinessTypeID,
                                    Code = a.BeneficiaryBusinessTypeCode,
                                    Name = a.BeneficiaryBusinessTypeName,
                                    Description = a.BeneficiaryBusinessTypeDescription,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    BeneficiaryBusinesGrid.Page = page;
                    BeneficiaryBusinesGrid.Size = size;
                    BeneficiaryBusinesGrid.Total = data.Count();
                    BeneficiaryBusinesGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    BeneficiaryBusinesGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new BeneficiaryBusinesRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            Code = a.Code,
                            Name = a.Name,
                            Description = a.Description,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetBeneficiaryBusines(BeneficiaryBusinesFilter filter, ref IList<BeneficiaryBusinesModel> BeneficiaryBusiness, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetBeneficiaryBusines(string key, int limit, ref IList<BeneficiaryBusinesModel> BeneficiaryBusiness, ref string message)
        {
            bool isSuccess = false;

            try
            {
                BeneficiaryBusiness = (from a in context.BeneficiaryBusinessTypes
                                where a.IsDeleted.Equals(false) &&
                                    a.BeneficiaryBusinessTypeName.Contains(key) || a.BeneficiaryBusinessTypeDescription.Contains(key)
                                orderby new { a.BeneficiaryBusinessTypeName, a.BeneficiaryBusinessTypeDescription }
                                select new BeneficiaryBusinesModel
                                {
                                    ID = a.BeneficiaryBusinessTypeID,
                                    Code = a.BeneficiaryBusinessTypeCode,
                                    Name = a.BeneficiaryBusinessTypeName,
                                    Description = a.BeneficiaryBusinessTypeDescription
                                }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddBeneficiaryBusines(BeneficiaryBusinesModel BeneficiaryBusines, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (!context.BeneficiaryBusinessTypes.Where(a => a.BeneficiaryBusinessTypeName.Equals(BeneficiaryBusines.Name) && a.IsDeleted.Equals(false)).Any())
                {
                    context.BeneficiaryBusinessTypes.Add(new Entity.BeneficiaryBusinessType()
                    {
                        BeneficiaryBusinessTypeID = BeneficiaryBusines.ID,
                        BeneficiaryBusinessTypeCode = BeneficiaryBusines.Code,
                        BeneficiaryBusinessTypeName = BeneficiaryBusines.Name,
                        BeneficiaryBusinessTypeDescription = BeneficiaryBusines.Description,
                        IsDeleted = false,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    });

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Beneficiary Business Type data for Beneficiary Business Type Name {0} is already exist.", BeneficiaryBusines.Name);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateBeneficiaryBusines(int id, BeneficiaryBusinesModel BeneficiaryBusines, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.BeneficiaryBusinessType data = context.BeneficiaryBusinessTypes.Where(a => a.BeneficiaryBusinessTypeID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    if (context.BeneficiaryBusinessTypes.Where(a => a.BeneficiaryBusinessTypeID != BeneficiaryBusines.ID && a.BeneficiaryBusinessTypeName.Equals(BeneficiaryBusines.Name) && a.IsDeleted.Equals(false)).Count() >= 1)
                    {
                        message = string.Format("Beneficiary Business Type Name {0} is exist.", BeneficiaryBusines.Name);
                    }
                    else
                    {
                        data.BeneficiaryBusinessTypeName = BeneficiaryBusines.Name;
                        data.BeneficiaryBusinessTypeCode = BeneficiaryBusines.Code;
                        data.BeneficiaryBusinessTypeDescription = BeneficiaryBusines.Description;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        context.SaveChanges();

                        isSuccess = true;
                    }
                }
                else
                {
                    message = string.Format("Beneficiary Business Type data for Beneficiary Business Type ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeleteBeneficiaryBusines(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.BeneficiaryBusinessType data = context.BeneficiaryBusinessTypes.Where(a => a.BeneficiaryBusinessTypeID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Beneficiary Business Type data for Beneficiary Business Type ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}