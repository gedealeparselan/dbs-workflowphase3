﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    public class SPUserTaskTunningController : ApiController
    {
        private string message = string.Empty;

        private readonly ISPUserTaskTunningRepository repository = null;

        #region Default Workflow Values
        private const string _state = "Running, Complete, Cancelled, Error";
        private const string _outcome = "None, Approved, Rejected, Pending, Cancelled, NotRequired, Continue, Delegated, Custom, Override Approved/Rejected/Continue";
        private const string _customOutcome = "Approve, Reject, Pending, Cancel, Revise, Submit, Revise to Payment Maker, Revise to PPU Maker, UTC, Submit Revise";
        #endregion

        public SPUserTaskTunningController()
        {
            this.repository = new SPUserTaskTunningRepository();
        }

        public SPUserTaskTunningController(ISPUserTaskTunningRepository repository)
        {
            this.repository = repository;
        }


        #region old Api (already deleted)
        /*/// <summary>
        /// Get all user tasks by multiple workflow id.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(SPUserTaskGrid))]
        [AcceptVerbs("GET", "POST")]
        
        public HttpResponseMessage Get(Guid webID, Guid siteID, string workflowIDs, string sort_column, string sort_order, IList<SPUserTaskFilter> filters, int? page = 1, int? size = 10, string state = _state, string outcome = _outcome, string customOutcome = _customOutcome, bool showContribute = false, bool showActiveTask = true)
        {
            IList<Guid> _workflowIDs = new List<Guid>();
            IList<string> _state = new List<string>();
            IList<string> _outcome = new List<string>();
            IList<string> _customOutcome = new List<string>();

            try
            {
                #region Workflow Parameters Validation
                // validate workflow id
                if (!string.IsNullOrEmpty(workflowIDs))
                {
                    string[] strWorkflowIDs = workflowIDs.Split(',');
                    foreach (var item in strWorkflowIDs)
                    {
                        _workflowIDs.Add(Guid.Parse(item));
                    }
                }

                // validate state id
                if (!string.IsNullOrEmpty(state))
                {
                    string[] strStateIDs = state.Split(',');
                    foreach (var item in strStateIDs)
                    {
                        _state.Add(item.Trim());
                    }
                }

                // validate oucome id
                if (!string.IsNullOrEmpty(outcome))
                {
                    string[] strOutcomeIDs = outcome.Split(',');
                    foreach (var item in strOutcomeIDs)
                    {
                        _outcome.Add(item.Trim());
                    }
                }

                // validate custom outcome id
                if (!string.IsNullOrEmpty(customOutcome))
                {
                    string[] strCustomOutcomeIDs = customOutcome.Split(',');
                    foreach (var item in strCustomOutcomeIDs)
                    {
                        _customOutcome.Add(item.Trim());
                    }
                }
                #endregion
                
                SPUserTaskGrid output = new SPUserTaskGrid();

                if (!repository.GetUserTasks(webID, siteID, _workflowIDs, _state, _outcome, _customOutcome, showContribute, showActiveTask, page, size, sort_column, sort_order, filters, ref output, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse<SPUserTaskGrid>(HttpStatusCode.OK, output);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = ex.Message });
            }
            finally
            {
                _workflowIDs = null;
            }
        }



        [ResponseType(typeof(SPUserTaskGrid))]
        [Route("api/SPUserTaskTunning/SP")]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetSP(Guid webID, Guid siteID, string workflowIDs, string sort_column, string sort_order, IList<SPUserTaskFilter> filters, int? page = 1, int? size = 10, string state = _state, string outcome = _outcome, string customOutcome = _customOutcome, bool showContribute = false, bool showActiveTask = true)
        {
            IList<Guid> _workflowIDs = new List<Guid>();
            IList<string> _state = new List<string>();
            IList<string> _outcome = new List<string>();
            IList<string> _customOutcome = new List<string>();

            try
            {
                #region Workflow Parameters Validation
                // validate workflow id
                if (!string.IsNullOrEmpty(workflowIDs))
                {
                    string[] strWorkflowIDs = workflowIDs.Split(',');
                    foreach (var item in strWorkflowIDs)
                    {
                        _workflowIDs.Add(Guid.Parse(item));
                    }
                }

                // validate state id
                if (!string.IsNullOrEmpty(state))
                {
                    string[] strStateIDs = state.Split(',');
                    foreach (var item in strStateIDs)
                    {
                        _state.Add(item.Trim());
                    }
                }

                // validate oucome id
                if (!string.IsNullOrEmpty(outcome))
                {
                    string[] strOutcomeIDs = outcome.Split(',');
                    foreach (var item in strOutcomeIDs)
                    {
                        _outcome.Add(item.Trim());
                    }
                }

                // validate custom outcome id
                if (!string.IsNullOrEmpty(customOutcome))
                {
                    string[] strCustomOutcomeIDs = customOutcome.Split(',');
                    foreach (var item in strCustomOutcomeIDs)
                    {
                        _customOutcome.Add(item.Trim());
                    }
                }
                #endregion

                SPUserTaskGrid output = new SPUserTaskGrid();

                if (!repository.GetUserTasksSP(webID, siteID, _workflowIDs, _state, _outcome, _customOutcome, showContribute, showActiveTask, page, size, sort_column, sort_order, filters, ref output, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse<SPUserTaskGrid>(HttpStatusCode.OK, output);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = ex.Message });
            }
            finally
            {
                _workflowIDs = null;
            }
        }
        */
        #endregion

        [ResponseType(typeof(SPUserTaskGrid))]
        [Route("api/SPUserTaskTuning/Home")]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetHome(Guid webID, Guid siteID, string workflowIDs, string sort_column, string sort_order, IList<SPUserTaskFilter> filters, int? page = 1, int? size = 10, string state = _state, string outcome = _outcome, string customOutcome = _customOutcome, bool showContribute = false, bool showActiveTask = true)
        {
            IList<Guid> _workflowIDs = new List<Guid>();
            IList<string> _state = new List<string>();
            IList<string> _outcome = new List<string>();
            IList<string> _customOutcome = new List<string>();

            try
            {
                #region Workflow Parameters Validation
                // validate workflow id
                if (!string.IsNullOrEmpty(workflowIDs))
                {
                    string[] strWorkflowIDs = workflowIDs.Split(',');
                    foreach (var item in strWorkflowIDs)
                    {
                        _workflowIDs.Add(Guid.Parse(item));
                    }
                }

                // validate state id
                if (!string.IsNullOrEmpty(state))
                {
                    string[] strStateIDs = state.Split(',');
                    foreach (var item in strStateIDs)
                    {
                        _state.Add(item.Trim());
                    }
                }

                // validate oucome id
                if (!string.IsNullOrEmpty(outcome))
                {
                    string[] strOutcomeIDs = outcome.Split(',');
                    foreach (var item in strOutcomeIDs)
                    {
                        _outcome.Add(item.Trim());
                    }
                }

                // validate custom outcome id
                if (!string.IsNullOrEmpty(customOutcome))
                {
                    string[] strCustomOutcomeIDs = customOutcome.Split(',');
                    foreach (var item in strCustomOutcomeIDs)
                    {
                        _customOutcome.Add(item.Trim());
                    }
                }
                #endregion

                SPUserTaskGrid output = new SPUserTaskGrid();

                if (!repository.GetHomeUserTasks(webID, siteID, _workflowIDs, _state, _outcome, _customOutcome, showContribute, showActiveTask, page, size, sort_column, sort_order, filters, ref output, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse<SPUserTaskGrid>(HttpStatusCode.OK, output);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = ex.Message });
            }
            finally
            {
                _workflowIDs = null;
            }
        }

        #region Home Loan
        [ResponseType(typeof(SPUserTaskGrid))]
        [Route("api/SPUserTaskTuning/HomeLoanIM")]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetHomeLoanIM(Guid webID, Guid siteID, string workflowIDs, string sort_column, string sort_order, IList<SPUserTaskFilter> filters, int? page = 1, int? size = 10, string state = _state, string outcome = _outcome, string customOutcome = _customOutcome, bool showContribute = false, bool showActiveTask = true)
        {
            IList<Guid> _workflowIDs = new List<Guid>();
            IList<string> _state = new List<string>();
            IList<string> _outcome = new List<string>();
            IList<string> _customOutcome = new List<string>();

            try
            {
                #region Workflow Parameters Validation
                // validate workflow id
                if (!string.IsNullOrEmpty(workflowIDs))
                {
                    string[] strWorkflowIDs = workflowIDs.Split(',');
                    foreach (var item in strWorkflowIDs)
                    {
                        _workflowIDs.Add(Guid.Parse(item));
                    }
                }

                // validate state id
                if (!string.IsNullOrEmpty(state))
                {
                    string[] strStateIDs = state.Split(',');
                    foreach (var item in strStateIDs)
                    {
                        _state.Add(item.Trim());
                    }
                }

                // validate oucome id
                if (!string.IsNullOrEmpty(outcome))
                {
                    string[] strOutcomeIDs = outcome.Split(',');
                    foreach (var item in strOutcomeIDs)
                    {
                        _outcome.Add(item.Trim());
                    }
                }

                // validate custom outcome id
                if (!string.IsNullOrEmpty(customOutcome))
                {
                    string[] strCustomOutcomeIDs = customOutcome.Split(',');
                    foreach (var item in strCustomOutcomeIDs)
                    {
                        _customOutcome.Add(item.Trim());
                    }
                }
                #endregion

                SPUserTaskGrid output = new SPUserTaskGrid();

                if (!repository.GetHomeUserLoanIMTasks(webID, siteID, _workflowIDs, _state, _outcome, _customOutcome, showContribute, showActiveTask, page, size, sort_column, sort_order, filters, ref output, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse<SPUserTaskGrid>(HttpStatusCode.OK, output);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = ex.Message });
            }
            finally
            {
                _workflowIDs = null;
            }
        }

        [ResponseType(typeof(SPUserTaskGrid))]
        [Route("api/SPUserTaskTuning/HomeLoanRO")]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetHomeLoanRO(Guid webID, Guid siteID, string workflowIDs, string sort_column, string sort_order, IList<SPUserTaskFilter> filters, int? page = 1, int? size = 10, string state = _state, string outcome = _outcome, string customOutcome = _customOutcome, bool showContribute = false, bool showActiveTask = true)
        {
            IList<Guid> _workflowIDs = new List<Guid>();
            IList<string> _state = new List<string>();
            IList<string> _outcome = new List<string>();
            IList<string> _customOutcome = new List<string>();

            try
            {
                #region Workflow Parameters Validation
                // validate workflow id
                if (!string.IsNullOrEmpty(workflowIDs))
                {
                    string[] strWorkflowIDs = workflowIDs.Split(',');
                    foreach (var item in strWorkflowIDs)
                    {
                        _workflowIDs.Add(Guid.Parse(item));
                    }
                }

                // validate state id
                if (!string.IsNullOrEmpty(state))
                {
                    string[] strStateIDs = state.Split(',');
                    foreach (var item in strStateIDs)
                    {
                        _state.Add(item.Trim());
                    }
                }

                // validate oucome id
                if (!string.IsNullOrEmpty(outcome))
                {
                    string[] strOutcomeIDs = outcome.Split(',');
                    foreach (var item in strOutcomeIDs)
                    {
                        _outcome.Add(item.Trim());
                    }
                }

                // validate custom outcome id
                if (!string.IsNullOrEmpty(customOutcome))
                {
                    string[] strCustomOutcomeIDs = customOutcome.Split(',');
                    foreach (var item in strCustomOutcomeIDs)
                    {
                        _customOutcome.Add(item.Trim());
                    }
                }
                #endregion

                SPUserTaskGrid output = new SPUserTaskGrid();

                if (!repository.GetHomeUserLoanROTasks(webID, siteID, _workflowIDs, _state, _outcome, _customOutcome, showContribute, showActiveTask, page, size, sort_column, sort_order, filters, ref output, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse<SPUserTaskGrid>(HttpStatusCode.OK, output);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = ex.Message });
            }
            finally
            {
                _workflowIDs = null;
            }
        }

        #endregion


        [ResponseType(typeof(SPUserTaskGrid))]
        [Route("api/SPUserTaskTuning/AllTransaction")]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetAllTransaction(Guid webID, Guid siteID, string workflowIDs, string sort_column, string sort_order, IList<SPUserTaskFilter> filters, int? page = 1, int? size = 10, string state = _state, string outcome = _outcome, string customOutcome = _customOutcome, bool showContribute = false, bool showActiveTask = true)
        {
            IList<Guid> _workflowIDs = new List<Guid>();
            IList<string> _state = new List<string>();
            IList<string> _outcome = new List<string>();
            IList<string> _customOutcome = new List<string>();

            try
            {
                #region Workflow Parameters Validation
                // validate workflow id
                if (!string.IsNullOrEmpty(workflowIDs))
                {
                    string[] strWorkflowIDs = workflowIDs.Split(',');
                    foreach (var item in strWorkflowIDs)
                    {
                        _workflowIDs.Add(Guid.Parse(item));
                    }
                }

                // validate state id
                if (!string.IsNullOrEmpty(state))
                {
                    string[] strStateIDs = state.Split(',');
                    foreach (var item in strStateIDs)
                    {
                        _state.Add(item.Trim());
                    }
                }

                // validate oucome id
                if (!string.IsNullOrEmpty(outcome))
                {
                    string[] strOutcomeIDs = outcome.Split(',');
                    foreach (var item in strOutcomeIDs)
                    {
                        _outcome.Add(item.Trim());
                    }
                }

                // validate custom outcome id
                if (!string.IsNullOrEmpty(customOutcome))
                {
                    string[] strCustomOutcomeIDs = customOutcome.Split(',');
                    foreach (var item in strCustomOutcomeIDs)
                    {
                        _customOutcome.Add(item.Trim());
                    }
                }
                #endregion

                SPUserTaskGrid output = new SPUserTaskGrid();

                //if (!repository.GetAllTransactionUserTasks(webID, siteID, _workflowIDs, _state, _outcome, _customOutcome, showContribute, showActiveTask, page, size, sort_column, sort_order, filters, ref output, ref message))
                if (!repository.GetAllTransactionSPUserTasks(webID, siteID, _workflowIDs, _state, _outcome, _customOutcome, showContribute, showActiveTask, page, size, sort_column, sort_order, filters, ref output, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse<SPUserTaskGrid>(HttpStatusCode.OK, output);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = ex.Message });
            }
            finally
            {
                _workflowIDs = null;
            }
        }



        [ResponseType(typeof(SPUserTaskGrid))]
        [Route("api/SPUserTaskTuning/Canceled")]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetCanceledItem(Guid webID, Guid siteID, string workflowIDs, string sort_column, string sort_order, IList<SPUserTaskFilter> filters, int? page = 1, int? size = 10, string state = _state, string outcome = _outcome, string customOutcome = _customOutcome, bool showContribute = false, bool showActiveTask = true)
        {
            IList<Guid> _workflowIDs = new List<Guid>();
            IList<string> _state = new List<string>();
            IList<string> _outcome = new List<string>();
            IList<string> _customOutcome = new List<string>();

            try
            {
                #region Workflow Parameters Validation
                // validate workflow id
                if (!string.IsNullOrEmpty(workflowIDs))
                {
                    string[] strWorkflowIDs = workflowIDs.Split(',');
                    foreach (var item in strWorkflowIDs)
                    {
                        _workflowIDs.Add(Guid.Parse(item));
                    }
                }

                // validate state id
                if (!string.IsNullOrEmpty(state))
                {
                    string[] strStateIDs = state.Split(',');
                    foreach (var item in strStateIDs)
                    {
                        _state.Add(item.Trim());
                    }
                }

                // validate oucome id
                if (!string.IsNullOrEmpty(outcome))
                {
                    string[] strOutcomeIDs = outcome.Split(',');
                    foreach (var item in strOutcomeIDs)
                    {
                        _outcome.Add(item.Trim());
                    }
                }

                // validate custom outcome id
                if (!string.IsNullOrEmpty(customOutcome))
                {
                    string[] strCustomOutcomeIDs = customOutcome.Split(',');
                    foreach (var item in strCustomOutcomeIDs)
                    {
                        _customOutcome.Add(item.Trim());
                    }
                }
                #endregion

                SPUserTaskGrid output = new SPUserTaskGrid();

                //if (!repository.GetCanceledUserTasks(webID, siteID, _workflowIDs, _state, _outcome, _customOutcome, showContribute, showActiveTask, page, size, sort_column, sort_order, filters, ref output, ref message))
                if (!repository.GetCanceledSPUserTasks(webID, siteID, _workflowIDs, _state, _outcome, _customOutcome, showContribute, showActiveTask, page, size, sort_column, sort_order, filters, ref output, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse<SPUserTaskGrid>(HttpStatusCode.OK, output);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = ex.Message });
            }
            finally
            {
                _workflowIDs = null;
            }
        }



        [ResponseType(typeof(SPUserTaskGrid))]
        [Route("api/SPUserTaskTuning/Completed")]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetCompletedItem(Guid webID, Guid siteID, string workflowIDs, string sort_column, string sort_order, IList<SPUserTaskFilter> filters, int? page = 1, int? size = 10, string state = _state, string outcome = _outcome, string customOutcome = _customOutcome, bool showContribute = false, bool showActiveTask = true)
        {
            IList<Guid> _workflowIDs = new List<Guid>();
            IList<string> _state = new List<string>();
            IList<string> _outcome = new List<string>();
            IList<string> _customOutcome = new List<string>();

            try
            {
                #region Workflow Parameters Validation
                // validate workflow id
                if (!string.IsNullOrEmpty(workflowIDs))
                {
                    string[] strWorkflowIDs = workflowIDs.Split(',');
                    foreach (var item in strWorkflowIDs)
                    {
                        _workflowIDs.Add(Guid.Parse(item));
                    }
                }

                // validate state id
                if (!string.IsNullOrEmpty(state))
                {
                    string[] strStateIDs = state.Split(',');
                    foreach (var item in strStateIDs)
                    {
                        _state.Add(item.Trim());
                    }
                }

                // validate oucome id
                if (!string.IsNullOrEmpty(outcome))
                {
                    string[] strOutcomeIDs = outcome.Split(',');
                    foreach (var item in strOutcomeIDs)
                    {
                        _outcome.Add(item.Trim());
                    }
                }

                // validate custom outcome id
                if (!string.IsNullOrEmpty(customOutcome))
                {
                    string[] strCustomOutcomeIDs = customOutcome.Split(',');
                    foreach (var item in strCustomOutcomeIDs)
                    {
                        _customOutcome.Add(item.Trim());
                    }
                }
                #endregion

                SPUserTaskGrid output = new SPUserTaskGrid();

                //if (!repository.GetCompletedUserTasks(webID, siteID, _workflowIDs, _state, _outcome, _customOutcome, showContribute, showActiveTask, page, size, sort_column, sort_order, filters, ref output, ref message))
                if (!repository.GetCompletedSPUserTasks(webID, siteID, _workflowIDs, _state, _outcome, _customOutcome, showContribute, showActiveTask, page, size, sort_column, sort_order, filters, ref output, ref message))

                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse<SPUserTaskGrid>(HttpStatusCode.OK, output);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = ex.Message });
            }
            finally
            {
                _workflowIDs = null;
            }
        }



        [ResponseType(typeof(SPUserTaskGrid))]
        [Route("api/SPUserTaskTuning/Monitoring")]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetMonitoring(Guid webID, Guid siteID, string workflowIDs, string sort_column, string sort_order, IList<SPUserTaskFilter> filters, int? page = 1, int? size = 10, string state = _state, string outcome = _outcome, string customOutcome = _customOutcome, bool showContribute = false, bool showActiveTask = true)
        {
            IList<Guid> _workflowIDs = new List<Guid>();
            IList<string> _state = new List<string>();
            IList<string> _outcome = new List<string>();
            IList<string> _customOutcome = new List<string>();

            try
            {
                #region Workflow Parameters Validation
                // validate workflow id
                if (!string.IsNullOrEmpty(workflowIDs))
                {
                    string[] strWorkflowIDs = workflowIDs.Split(',');
                    foreach (var item in strWorkflowIDs)
                    {
                        _workflowIDs.Add(Guid.Parse(item));
                    }
                }

                // validate state id
                if (!string.IsNullOrEmpty(state))
                {
                    string[] strStateIDs = state.Split(',');
                    foreach (var item in strStateIDs)
                    {
                        _state.Add(item.Trim());
                    }
                }

                // validate oucome id
                if (!string.IsNullOrEmpty(outcome))
                {
                    string[] strOutcomeIDs = outcome.Split(',');
                    foreach (var item in strOutcomeIDs)
                    {
                        _outcome.Add(item.Trim());
                    }
                }

                // validate custom outcome id
                if (!string.IsNullOrEmpty(customOutcome))
                {
                    string[] strCustomOutcomeIDs = customOutcome.Split(',');
                    foreach (var item in strCustomOutcomeIDs)
                    {
                        _customOutcome.Add(item.Trim());
                    }
                }
                #endregion

                SPUserTaskGrid output = new SPUserTaskGrid();

                //if (!repository.GetMonitoringUserTasks(webID, siteID, _workflowIDs, _state, _outcome, _customOutcome, showContribute, showActiveTask, page, size, sort_column, sort_order, filters, ref output, ref message))

                if (!repository.GetMonitoringSPUserTasks(webID, siteID, _workflowIDs, _state, _outcome, _customOutcome, showContribute, showActiveTask, page, size, sort_column, sort_order, filters, ref output, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse<SPUserTaskGrid>(HttpStatusCode.OK, output);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = ex.Message });
            }
            finally
            {
                _workflowIDs = null;
            }
        }


        //Basri 05.10.2015
        [ResponseType(typeof(SPUserTaskGrid))]
        [Route("api/SPUserTaskTuning/TMOHome")]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetTMOHome(Guid webID, Guid siteID, string workflowIDs, string sort_column, string sort_order, IList<SPUserTaskFilter> filters, int? page = 1, int? size = 10, string state = _state, string outcome = _outcome, string customOutcome = _customOutcome, bool showContribute = false, bool showActiveTask = true)
        {
            IList<Guid> _workflowIDs = new List<Guid>();
            IList<string> _state = new List<string>();
            IList<string> _outcome = new List<string>();
            IList<string> _customOutcome = new List<string>();

            try
            {
                #region Workflow Parameters Validation
                // validate workflow id
                if (!string.IsNullOrEmpty(workflowIDs))
                {
                    string[] strWorkflowIDs = workflowIDs.Split(',');
                    foreach (var item in strWorkflowIDs)
                    {
                        _workflowIDs.Add(Guid.Parse(item));
                    }
                }

                // validate state id
                if (!string.IsNullOrEmpty(state))
                {
                    string[] strStateIDs = state.Split(',');
                    foreach (var item in strStateIDs)
                    {
                        _state.Add(item.Trim());
                    }
                }

                // validate oucome id
                if (!string.IsNullOrEmpty(outcome))
                {
                    string[] strOutcomeIDs = outcome.Split(',');
                    foreach (var item in strOutcomeIDs)
                    {
                        _outcome.Add(item.Trim());
                    }
                }

                // validate custom outcome id
                if (!string.IsNullOrEmpty(customOutcome))
                {
                    string[] strCustomOutcomeIDs = customOutcome.Split(',');
                    foreach (var item in strCustomOutcomeIDs)
                    {
                        _customOutcome.Add(item.Trim());
                    }
                }
                #endregion

                SPUserTaskGrid output = new SPUserTaskGrid();

                if (!repository.GetHomeTMOUserTasks(webID, siteID, _workflowIDs, _state, _outcome, _customOutcome, showContribute, showActiveTask, page, size, sort_column, sort_order, filters, ref output, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse<SPUserTaskGrid>(HttpStatusCode.OK, output);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = ex.Message });
            }
            finally
            {
                _workflowIDs = null;
            }
        }
        //End Basri

        #region Loan
        //Agung 05.10.2015
        [ResponseType(typeof(SPUserTaskGrid))]
        [Route("api/SPUserTaskTuning/LOANHome")]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetLOANHome(Guid webID, Guid siteID, string workflowIDs, string sort_column, string sort_order, IList<SPUserTaskFilter> filters, int? page = 1, int? size = 10, string state = _state, string outcome = _outcome, string customOutcome = _customOutcome, bool showContribute = false, bool showActiveTask = true)
        {
            IList<Guid> _workflowIDs = new List<Guid>();
            IList<string> _state = new List<string>();
            IList<string> _outcome = new List<string>();
            IList<string> _customOutcome = new List<string>();

            try
            {
                #region Workflow Parameters Validation
                // validate workflow id
                if (!string.IsNullOrEmpty(workflowIDs))
                {
                    string[] strWorkflowIDs = workflowIDs.Split(',');
                    foreach (var item in strWorkflowIDs)
                    {
                        _workflowIDs.Add(Guid.Parse(item));
                    }
                }

                // validate state id
                if (!string.IsNullOrEmpty(state))
                {
                    string[] strStateIDs = state.Split(',');
                    foreach (var item in strStateIDs)
                    {
                        _state.Add(item.Trim());
                    }
                }

                // validate oucome id
                if (!string.IsNullOrEmpty(outcome))
                {
                    string[] strOutcomeIDs = outcome.Split(',');
                    foreach (var item in strOutcomeIDs)
                    {
                        _outcome.Add(item.Trim());
                    }
                }

                // validate custom outcome id
                if (!string.IsNullOrEmpty(customOutcome))
                {
                    string[] strCustomOutcomeIDs = customOutcome.Split(',');
                    foreach (var item in strCustomOutcomeIDs)
                    {
                        _customOutcome.Add(item.Trim());
                    }
                }
                #endregion

                SPUserTaskGrid output = new SPUserTaskGrid();

                if (!repository.GetHomeLOANTasks(webID, siteID, _workflowIDs, _state, _outcome, _customOutcome, showContribute, showActiveTask, page, size, sort_column, sort_order, filters, ref output, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse<SPUserTaskGrid>(HttpStatusCode.OK, output);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = ex.Message });
            }
            finally
            {
                _workflowIDs = null;
            }
        }
        //End Basri
        #endregion


    }
}