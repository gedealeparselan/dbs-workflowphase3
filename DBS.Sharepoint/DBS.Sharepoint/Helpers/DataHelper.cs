﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBS.Sharepoint.Helpers
{
    public class DataHelper
    {
        public static string StrConn =
        System.Configuration.ConfigurationManager.ConnectionStrings["DBSDatabase"].ToString();

        //public static string StrConn = "Data Source=.;Initial Catalog=DBS;Integrated Security=SSPI";

        public static int SQLCommandTimeout =
            Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings.Get("SqlCommandTimeOut"));

        public static DataTable ExecuteDataTable(string sql)
        {
            DataTable dsData = new DataTable();
            try
            {
                using (SqlConnection conn = new SqlConnection(StrConn))
                {
                    if (conn.State == ConnectionState.Closed) conn.Open();

                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = sql;
                        cmd.CommandTimeout = SQLCommandTimeout;
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = conn;

                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dsData);
                    }
                }
                return dsData;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static void ExecuteNonQuery(string sql)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(StrConn))
                {
                    if (conn.State == ConnectionState.Closed) conn.Open();

                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = sql;
                        cmd.CommandTimeout = SQLCommandTimeout;
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = conn;

                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static int InsertUploadLog(string sql)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(StrConn))
                {
                    if (conn.State == ConnectionState.Closed) conn.Open();
                    int ret = 0;
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = sql + ";SELECT CAST(scope_identity() AS int)";
                        cmd.CommandTimeout = SQLCommandTimeout;
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = conn;
                        ret = (int)cmd.ExecuteScalar();
                    }
                    return ret;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
