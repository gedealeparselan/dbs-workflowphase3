﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class NewTransactionNettingController : ApiController
    {
        private string message = string.Empty;
        private readonly INewTransactionNettingRepository repository = null;
        public NewTransactionNettingController()
        {
            this.repository = new NewTransactionNettingRepository();
        }

        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }

        [HttpPost]
        [ResponseType(typeof(string))]
        [Route("api/NewTransactionNetting")]
        public HttpResponseMessage Post(TransactionNettingModel transaction)
        {
            long transactionID = 0;
            string ApplicationID = string.Empty;
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddTransactionNetting(transaction, ref transactionID, ref ApplicationID, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new
                    {
                        ID = transactionID,
                        AppID = ApplicationID,
                        Message = transaction.IsDraft ? "Transaction has been saved as draft." : "New Transaction has been submitted."
                    });
                }
            }
        }

        [ResponseType(typeof(TransactionNettingDraftModel))]
        [Route("api/TransactionNetting/Draft/{id}")]
        [HttpGet]
        public HttpResponseMessage GetDraft(Int64 id)
        {
            TransactionNettingDraftModel output = new TransactionNettingDraftModel();

            if (!repository.GetTransactionDraftByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<TransactionNettingDraftModel>(HttpStatusCode.OK, output);
            }
        }
        [HttpDelete]
        [Route("api/TransactionNetting/Draft/Delete/{id}")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage DeleteDraft(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteTransactionDraftByID(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Draft." });
                }
            }
        }

    }
}
