﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using System.Data.Entity.SqlServer;

namespace DBS.WebAPI.Models
{
    #region Property
    [ModelName("TransactionSubType")]
    public class TransactionSubTypeModel
    {
        public int ID { get; set; }
        public int TransTypeID { get; set; }
        public string Name { get; set; }
        public bool isDeleted { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
    }

    public class TransactionSubTypeRow : TransactionSubTypeModel
    {
        public int RowID { get; set; }
    }
    public class TransactionSubTypeGrid : Grid
    {
        public IList<TransactionSubTypeRow> Rows { get; set; }
    }
    #endregion
    #region Filter
    public class TransactionSubTypeFilter : Filter { }
    #endregion
    #region Interface
    public interface ITransactionSubTypeRepository : IDisposable
    {
        bool GetTransactionSubTypeByID(int id, ref TransactionSubTypeModel TransactionSubType, ref string message);
        bool GetTransactionSubType(ref IList<TransactionSubTypeModel> transactionSubTypes, int limit, int index, ref string message);
        bool GetTransactionSubType(ref IList<TransactionSubTypeModel> transactionSubTypes, ref string message);
        bool GetTransactionSubType(int page, int size, IList<TransactionSubTypeFilter> filters, string sortColumn, string sortOrder, ref TransactionSubTypeGrid transactionSubTypeGrid, ref string message);
        bool GetTransactionSubType(TransactionSubTypeFilter filter, ref IList<TransactionSubTypeModel> transactionSubTypes, ref string message);
        bool GetTransactionSubType(string key, int limit, ref IList<TransactionSubTypeModel> transactionSubTypes, ref string message);
        bool AddTransactionSubType(TransactionSubTypeModel transactionsubtype, ref string message);
        bool UpdateTransactionSubType(int id, TransactionSubTypeModel transactionSubType, ref string message);
        bool DeleteTransactionSubType(int id, ref string message);
    }
    #endregion

    #region Repository
    public class TransactionSubTypeRepository : ITransactionSubTypeRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetTransactionSubTypeByID(int id, ref TransactionSubTypeModel TransactionSubType, ref string message)
        {
            bool isSuccess = false;
            try
            {
                TransactionSubType = (from a in context.TransactionSUBTypes
                                      join b in context.TransactionTypes on a.TransTypeID equals b.TransTypeID
                                      where a.isDeleted.Equals(false) && a.TransactionSubTypeID.Equals(id)
                                      select new TransactionSubTypeModel
                                      {
                                          ID = a.TransactionSubTypeID,
                                          TransTypeID = a.TransTypeID,                                       
                                          Name = a.TransactionSubType1,
                                          LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                          LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                      }).SingleOrDefault();
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool GetTransactionSubType(ref IList<TransactionSubTypeModel> transactionSubTypes, int limit, int index, ref string message)
        {
            bool isSuccess = false;
            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    transactionSubTypes = (from a in context.TransactionSUBTypes
                                           join b in context.TransactionTypes on a.TransTypeID equals b.TransTypeID
                                           where a.isDeleted.Equals(false)
                                           orderby new { a.TransactionSubTypeID, a.TransTypeID, a.TransactionSubType1, a.CreateDate, a.CreateBy, a.UpdateBy, a.UpdateDate }
                                        select new TransactionSubTypeModel
                                        {
                                            ID = a.TransactionSubTypeID,
                                            TransTypeID = a.TransTypeID,
                                            Name = a.TransactionSubType1,
                                            LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                            LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                        }).Skip(skip).Take(limit).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool GetTransactionSubType(ref IList<TransactionSubTypeModel> transactionSubTypes, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    transactionSubTypes = (from a in context.TransactionSUBTypes
                                        join b in context.TransactionTypes on a.TransTypeID equals b.TransTypeID
                                        where a.isDeleted.Equals(false)
                                        orderby new { a.TransactionSubTypeID, a.TransactionSubType1 }
                                        select new TransactionSubTypeModel
                                        {
                                            ID = a.TransactionSubTypeID,
                                            TransTypeID = a.TransTypeID,
                                            Name = a.TransactionSubType1,
                                            LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                            LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                        }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool GetTransactionSubType(int page, int size, IList<TransactionSubTypeFilter> filters, string sortColumn, string sortOrder, ref TransactionSubTypeGrid transactionSubTypeGrid, ref string message)
        {
            bool isSuccess = false;
            //try
            //{
            //    int skip = (page - 1) * size;
            //    string orderBy = sortColumn + " " + sortOrder;
            //    DateTime maxDate = new DateTime();
            //    TransactionSubTypeModel filter = new TransactionSubTypeModel();

            //    if (filters != null)
            //    {
            //        filter.ID = int.Parse((string)filters.Where(a => a.Field.Equals("ID")).Select(a => a.Value).SingleOrDefault());
            //        filter.Name = (string)filters.Where(a => a.Field.Equals("Name")).Select(a => a.Value).SingleOrDefault();
            //        filter.ProductID = int.Parse((string)filters.Where(a => a.Field.Equals("ProductID")).Select(a => a.Value).SingleOrDefault());
            //        filter.ProductName = (string)filters.Where(a => a.Field.Equals("ProductName")).Select(a => a.Value).SingleOrDefault();
            //        filter.LastModifiedBy = filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

            //        if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
            //        {
            //            filter.LastModifiedDate = DateTime.Parse(filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
            //            maxDate = filter.LastModifiedDate.Value.AddDays(1);
            //        }
            //    }
            //    using (DBSEntities context = new DBSEntities())
            //    {
            //        var data = (from a in context.TransactionTypes
            //                    join b in context.Products on a.ProductID equals b.ProductID
            //                    let isTransactionType = !string.IsNullOrEmpty(filter.Name)
            //                    let isProductName = !string.IsNullOrEmpty(filter.ProductName)
            //                    let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
            //                    let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
            //                    where a.IsDeleted.Equals(false) &&
            //                     (isTransactionType ? a.TransactionType1.Contains(filter.Name) : true) &&
            //                     (isProductName ? a.Product.ProductName.Contains(filter.ProductName) : true) &&
            //                     (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
            //                     (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)

            //                    select new TransactionTypeModel
            //                    {
            //                        ID = a.TransTypeID,
            //                        ProductID = a.ProductID,
            //                        ProductName = b.ProductName,
            //                        Name = a.TransactionType1,
            //                        LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
            //                        LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
            //                    });
            //        transactionTypeGrid.Page = page;
            //        transactionTypeGrid.Size = size;
            //        transactionTypeGrid.Total = data.Count();
            //        transactionTypeGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
            //        transactionTypeGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
            //            .Select((a, i) => new TransactionTypeRow
            //            {
            //                RowID = i + 1,
            //                ID = a.ID,
            //                ProductID = a.ProductID,
            //                ProductName = a.ProductName,
            //                Name = a.Name,
            //                LastModifiedBy = a.LastModifiedBy,
            //                LastModifiedDate = a.LastModifiedDate
            //            })
            //            .Skip(skip)
            //            .Take(size)
            //            .ToList();
            //    }
            //    isSuccess = true;
            //}
            //catch (Exception ex)
            //{
            //    message = ex.Message;
            //}
            return isSuccess;
        }
        public bool GetTransactionSubType(TransactionSubTypeFilter filter, ref IList<TransactionSubTypeModel> transactionSubTypes, ref string message)
        {
            throw new NotImplementedException();
        }
        public bool GetTransactionSubType(string key, int limit, ref IList<TransactionSubTypeModel> transactionSubTypes, ref string message)
        {
            bool isSuccess = false;
            try
            {
                transactionSubTypes = (from a in context.TransactionSUBTypes
                                    join b in context.TransactionTypes on a.TransTypeID equals b.TransTypeID
                                    where (a.TransactionSubType1.Contains(key) && a.isDeleted.Equals(false))
                                    orderby new { a.TransactionSubTypeID, a.TransactionSubType1 }
                                    select new TransactionSubTypeModel
                                    {
                                        ID = a.TransactionSubTypeID,
                                        TransTypeID = a.TransTypeID,
                                        Name = a.TransactionSubType1,
                                        LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                        LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                    }).Take(limit).ToList();
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool AddTransactionSubType(TransactionSubTypeModel transactionsubtype, ref string message)
        {
            bool isSuccess = false;
            //if (transactionsubtype != null)
            //{
            //    try
            //    {
            //        if (!context.TransactionTypes.Where(a => a.TransTypeID.Equals(transactiontype.ID) && a.IsDeleted.Equals(false)).Any())
            //        {
            //            Entity.TransactionType data = new Entity.TransactionType()
            //            {
            //                TransTypeID = transactiontype.ID,
            //                ProductID = transactiontype.ProductID,
            //                //ProductName = transactiontype.ProductName,
            //                TransactionType1 = transactiontype.Name,
            //                CreateDate = DateTime.UtcNow,
            //                CreateBy = currentUser.GetCurrentUser().DisplayName
            //            };
            //            context.TransactionTypes.Add(data);
            //            context.SaveChanges();

            //            isSuccess = true;
            //        }
            //        else
            //        {
            //            message = string.Format("TransactionType Data for TransactionType Code {0} is already exist.", transactiontype.ID);

            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        message = ex.Message;
            //    }
            //}
            return isSuccess;
        }
        public bool UpdateTransactionSubType(int id, TransactionSubTypeModel transactionSubType, ref string message)
        {
            bool isSuccess = false;
            //try
            //{
            //    Entity.TransactionType data = context.TransactionTypes.Where(a => a.TransTypeID.Equals(id)).SingleOrDefault();

            //    if (data != null)
            //    {
            //        if (context.TransactionTypes.Where(a => a.TransTypeID != transactionType.ID && a.ProductID.Equals(transactionType.ProductID) && a.IsDeleted.Equals(false)).Count() >= 1)
            //        {
            //            message = string.Format("Transaction ID {0} is exist.", transactionType.ID);
            //        }
            //        else
            //        {
            //            data.TransTypeID = transactionType.ID;
            //            data.TransactionType1 = transactionType.Name;
            //            data.ProductID = transactionType.ProductID;
            //            data.UpdateDate = DateTime.UtcNow;
            //            data.UpdateBy = currentUser.GetCurrentUser().DisplayName;
            //            context.SaveChanges();
            //            isSuccess = true;
            //        }
            //    }
            //    else
            //    {
            //        message = string.Format("TransactionType data for transactionType ID {0} is does not exist.", id);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    message = ex.Message;

            //}
            return isSuccess;
        }
        public bool DeleteTransactionSubType(int id, ref string message)
        {
            bool isSuccess = false;
            //try
            //{
            //    Entity.TransactionType data = context.TransactionTypes.Where(a => a.TransTypeID.Equals(id)).SingleOrDefault();
            //    if (data != null)
            //    {
            //        data.IsDeleted = true;
            //        data.UpdateDate = DateTime.UtcNow;
            //        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

            //        context.SaveChanges();

            //        isSuccess = true;
            //    }
            //    else
            //    {
            //        message = string.Format("TransactionType data for TransactionType ID {0} is does not exist.", id);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    message = ex.Message;
            //}
            return isSuccess;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}