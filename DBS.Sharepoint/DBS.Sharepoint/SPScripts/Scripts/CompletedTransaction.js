/**
 * Created by Roman Bangkit on 11/10/2014.
 */

var accessToken;
var $box;
var $remove = false;

var DataInformation = {
    GeneratedTransactionID: ko.observable(),
    TransactionID: ko.observable(),
    ApplicationID: ko.observable(),
    FileName: ko.observable(),
    CreateDate: ko.observable(new Date()),
    CreateBy: ko.observable()
};

var DataInformationCollection = {
    DataInformation: ko.observableArray()
};

var GeneratedFileInfo = {
    InsertedID: ko.observable(),
    InsertedURL: ko.observable(),
    IsSuccess: ko.observable(),
    Message: ko.observable()
};

var CurrencyModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};

var ChannelModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var AccountModel = {
    AccountNumber: ko.observable(),
    Currency: ko.observable(CurrencyModel)
};

var BizSegmentModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var BankModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    BranchCode: ko.observable(),
    SwiftCode: ko.observable(),
    BankAccount: ko.observable(),
    Description: ko.observable(),
    CommonName: ko.observable(),
    Currency: ko.observable(),
    PGSL: ko.observable()
};

var ChargesModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};

var ChargesModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};

var TransactionCompletedModel = {
    TransactionID: ko.observable(),
    ApproverID: ko.observable(),
    ApplicationDate: ko.observable(new Date()),
    Currency: ko.observable(CurrencyModel),
    Amount: ko.observable(),
    AmountUSD: ko.observable(),
    Rate: ko.observable(),
    Channel: ko.observable(ChannelModel),
    Account: ko.observable(AccountModel),
    BizSegment: ko.observable(BizSegmentModel),
    BeneName: ko.observable(),
    LLDCode: ko.observable(),
    LLDInfo: ko.observable(),
    Bank: ko.observable(BankModel),
    BankCharges: ko.observable(ChargesModel),
    AgentCharges: ko.observable(ChargesModel),
    Type: ko.observable(),
    IsResident: ko.observable(),
    IsCitizen: ko.observable(),
    PaymentDetails: ko.observable(),
    DetailCompliance: ko.observable(),
    TZNumber: ko.observable(),
    ModifiedDate: ko.observable(new Date()),
    ModifiedBy: ko.observable()
};

// 13-sept 2016 add by adi for IPE
var PaymentMode = {
    ModeID: ko.observable(),
    ModeName: ko.observable(),
    Status: ko.observable()
};

var BeneficiaryCountryModel = {
    ID: ko.observable(),
    Code: ko.observable()
};

var TransactionRelationshipModel = {
    ID: ko.observable(),
    Code: ko.observable()
};

var CBGCustomerModel = {
    ID: ko.observable(),
    Code: ko.observable()
};

var TransactionUsingDebitSundryModel = {
    ID: ko.observable(),
    Code: ko.observable()
};

var FXComplianceModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var BranchModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Code: ko.observable()
};

var CityModel = {
    CityID: ko.observable(),
    CityCode: ko.observable(),
    Description: ko.observable()
};

var StatusIPE = {
    Value: ""
}
//end

var TransactionModel = {
    RowID: ko.observable(),
    workflowInstanceID: ko.observable(),
    ID: ko.observable(),
    applicationID: ko.observable(),
    topUrgent: ko.observable(),
    topUrgentDesc: ko.observable(),
    customerName: ko.observable(),
    newCustomer: ko.observable(),
    cif: ko.observable(),
    productID: ko.observable(),
    productName: ko.observable(),
    currencyID: ko.observable(),
    currencyDesc: ko.observable(),
    transactionAmount: ko.observable(),
    rate: ko.observable(),
    eqvUsd: ko.observable(),
    channelID: ko.observable(),
    debitAccNumber: ko.observable(),
    debitAccCcy: ko.observable(),
    applicationDate: ko.observable(),
    bizSegmentID: ko.observable(),
    lldCode: ko.observable(),
    lldInfo: ko.observable(),
    signatureVerification: ko.observable(),
    dormantAccount: ko.observable(),
    freezeAccount: ko.observable(),
    others: ko.observable(),
    stateID: ko.observable(),
    isDraft: ko.observable(),
    tzNumber: ko.observable(),
    fxTransaction: ko.observable(),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    transactionDocument: ko.observable(),

    //IPE add by adi
    ChargingAccountName: ko.observable(),
    ChargingAccountBank: ko.observable(),
    BeneficiaryAddress: ko.observable(),
    AccountNumber: ko.observable(),
    Compliance: ko.observable(FXComplianceModel),
    CCY: ko.observable(),
    BranchID: ko.observable(),
    CityID: ko.observable(),
    NostroID: ko.observable(),
    ChargingACCNumber: ko.observable(),
    PaymentMode: ko.observable(PaymentMode),
    ChargingACCcurrency: ko.observable(),
    PaymentModeStatus: ko.observable(),
    selectedOptionId: ko.observable(),
    ModePayment: ko.observable(),
    Branch: ko.observable(BranchModel),
    City: ko.observable(CityModel),
    //ChargingAccountCurrency: ko.observable(ChargingAccountCurrencyModel),
    //Tambah Ipe
    BeneficiaryCountry: ko.observable(BeneficiaryCountryModel),
    TransactionRelationship: ko.observable(TransactionRelationshipModel),
    CBGCustomer: ko.observable(CBGCustomerModel),
    TransactionUsingDebitSundry: ko.observable(TransactionUsingDebitSundryModel)
};

var DataInformation = {
    GeneratedTransactionID: ko.observable(),
    TransactionID: ko.observable(),
    ApplicationID: ko.observable(),
    FileName: ko.observable(),
    CreateDate: ko.observable(new Date()),
    CreateBy: ko.observable()
};

var GenerateTransactionModel = {
    RowID: ko.observable(),
    ApplicationID: ko.observable(),
    FileName: ko.observable(),
    CreateDate: ko.observable(new Date()),
    CreateBy: ko.observable()
}

// add by adi
//var GenerateFileXMLModel = {
//    ItemID: ko.observable(),
//    ApplicationID:ko.observable(),
//    ApplicationIDXML : ko.observable(),
//    FilenameXML: ko.observable(),
//    FileName : ko.observable(),
//    CreateDate: ko.observable(new Date()),
//    CreateBy : ko.observable()
//}
//end

var SelectedModel = {
    Product: ko.observable(0)
}

var formatNumber = function (num) {
    if (num != null) {
        //num = num.toString().replace(/,/g, '');
        //num = parseFloat(num).toFixed(2);
        var n = num.toString(), p = n.indexOf('.');
        return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
            return p < 0 || i < p ? ($0 + ',') : $0;
        });
    } else { return 0; }
}

var formatNumber_r = function (num) {
    if (num != null) {
        num = num.toString().replace(/,/g, '');
        num = parseFloat(num).toFixed(2);
        var n = num.toString(), p = n.indexOf('.');
        return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
            return p < 0 || i < p ? ($0 + ',') : $0;
        });
    } else { return 0; }
}

var GridPropertiesModel = function (callback) {
    var self = this;

    self.SortColumn = ko.observable();
    self.SortOrder = ko.observable();
    self.AllowFilter = ko.observable(false);
    self.AvailableSizes = ko.observableArray(config.sizes);
    self.Page = ko.observable(1);
    self.Size = ko.observable(config.sizeDefault);
    self.Total = ko.observable(0);
    self.TotalPages = ko.observable(0);

    self.Callback = function () {
        callback();
    };

    self.OnPageSizeChange = function () {
        self.Page(1);

        self.Callback();
    };

    self.OnPageChange = function () {
        if (self.Page() < 1) {
            self.Page(1);
        } else {
            if (self.Page() > self.TotalPages())
                self.Page(self.TotalPages());
        }

        self.Callback();
    };

    self.NextPage = function () {
        var page = self.Page();
        if (page < self.TotalPages()) {
            self.Page(page + 1);

            self.Callback();
        }
    };

    self.PreviousPage = function () {
        var page = self.Page();
        if (page > 1) {
            self.Page(page - 1);

            self.Callback();
        }
    };

    self.FirstPage = function () {
        self.Page(1);

        self.Callback();
    };

    self.LastPage = function () {
        self.Page(self.TotalPages());

        self.Callback();
    };

    self.Filter = function (data) {
        self.Page(1);

        self.Callback();
    };

    // get sorted column
    self.GetSortedColumn = function (columnName) {
        var sort = "sorting";

        if (self.SortColumn() == columnName) {
            if (self.SortOrder() == "DESC")
                sort = sort + "_asc";
            else
                sort = sort + "_desc";
        }

        return sort;
    };

    // Sorting data
    self.Sorting = function (column) {
        if (self.SortColumn() == column) {
            if (self.SortOrder() == "ASC")
                self.SortOrder("DESC");
            else
                self.SortOrder("ASC");
        } else {
            self.SortOrder("ASC");
        }

        self.SortColumn(column);

        self.Page(1);

        self.Callback();
    };
};

var ViewModel = function () {
    //Make the self as 'this' reference
    var self = this;

    self.FileFormat = ko.observable();
    self.Username = ko.observable();

    self.FileNameXML = ko.observable("");
    self.ApplicationIDXML = ko.observable("");


    // filter
    self.FilterapplicationID = ko.observable("");
    self.FiltercustomerName = ko.observable("");
    self.FilterproductName = ko.observable("");
    self.FiltercurrencyDesc = ko.observable("");
    self.FiltertransactionAmount = ko.observable("");
    self.FiltereqvUsd = ko.observable("");
    self.FilterfxTransaction = ko.observable("");
    self.FiltertopUrgentDesc = ko.observable("");
    self.FilterstateID = ko.observable("");
    self.FilterModifiedBy = ko.observable("");
    self.FilterModifiedDate = ko.observable("");

    //filter Generated File
    self.FilterFileName = ko.observable("");
    self.FilterApplicationID = ko.observable("");

    //filter Generated File XML
    self.FilterFileNameXML = ko.observable("");
    self.FilterApplicationIDXML = ko.observable("");

    // New Data flag
    self.IsNewData = ko.observable(false);

    // Declare an ObservableArray for Storing the JSON Response
    self.Transactions = ko.observableArray([TransactionModel]);
    self.DataInformationCollection = ko.observableArray([]);
    self.GeneratedTransactions = ko.observableArray([]);

    // add by adi for Generated File XML
    self.GeneratedTransactionXML = ko.observableArray([]);
    //self.DataXML = ko.observableArray([]);
    self.SelectedMode = ko.observableArray([{ ID: 0, Name: 'Please Select ...' }, { ID: 1, Name: 'Finacle' }, { ID: 2, Name: 'IPE' }]);
    self.ModeValue = ko.observable(0);
    self.ButtonGenerateFile = ko.observable(false);


    //Parameter
    self.Products = ko.observableArray([]);
    self.ProductsIPE = ko.observableArray([]);
    self.GeneratePaymentMode = ko.observable();


    // Options selected value
    self.Selected = ko.observable(SelectedModel);

    // grid properties
    self.GridProperties = ko.observable();
    self.GridProperties(new GridPropertiesModel(GetData));
    self.GridGeneratedProperties = ko.observable();
    self.GridGeneratedProperties(new GridPropertiesModel(GetGeneratedFileData));

    // add by adi
    self.GridGeneratedXMLProperties = ko.observable();
    self.GridGeneratedXMLProperties(new GridPropertiesModel(GetGeneratedFileXMLData));
    //end

    // set default sorting
    self.GridProperties().SortColumn("applicationID");
    self.GridProperties().SortOrder("DESC");

    // set default generated file sorting
    self.GridGeneratedProperties().SortColumn("ItemID");
    self.GridGeneratedProperties().SortOrder("DESC");

    //set default generated file xml sorting by adi
    self.GridGeneratedXMLProperties().SortColumn("FileName");
    self.GridGeneratedXMLProperties().SortOrder("DESC");

    // bind clear filters
    self.ClearFilters = function () {
        self.FilterapplicationID("");
        self.FiltercustomerName("");
        self.FilterproductName("");
        self.FiltercurrencyDesc("");
        self.FiltertransactionAmount("");
        self.FiltereqvUsd("");
        self.FilterfxTransaction("");
        self.FiltertopUrgentDesc("");
        self.FilterstateID("");
        self.FilterModifiedBy("");
        self.FilterModifiedDate("");

        GetData();
    };

    self.ClearGeneratedFileFilters = function () {
        self.FilterFileName("");
        self.FilterApplicationID("");

        GetGeneratedFileData();

    };

    self.ClearGeneratedFileXMLFilter = function () {
        self.FileNameXML("");
        self.FilterApplicationIDXML("");

        GetGeneratedFileXMLData();
    };

    // bind get data function to view
    self.GetData = function () {
        GetData();
    };

    self.GetGeneratedFileData = function () {
        GetGeneratedFileData();
    };

    self.GetGeneratedFileXMLData = function () {
        GetGeneratedFileXMLData();
    }

    // Local Date
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return "";
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-17
    };

    //Function to Read All Customers
    function GetData() {
        $("#gridCompleted").hide();
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end
        //console.log(self.ModeValue());
        if (self.Selected().Product() === undefined || self.Selected().Product() === null) {
            self.Selected().Product(0);
        }
        //console.log(self.Selected().Product());
        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.transactionComplete(self.ModeValue(), self.Selected().Product()),
            params: {
                page: self.GridProperties().Page(),
                size: self.GridProperties().Size(),
                sort_column: self.GridProperties().SortColumn(),
                sort_order: self.GridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetData, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetData, OnError, OnAlways);
        }
    }

    self.GenerateData = function () {
        GetGeneratedFileData();
    }

    //Function to Read Generated File
    function GetGeneratedFileData() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.generatedfile,
            params: {
                page: self.GridGeneratedProperties().Page(),
                size: self.GridGeneratedProperties().Size(),
                sort_column: self.GridGeneratedProperties().SortColumn(),
                sort_order: self.GridGeneratedProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetGeneratedFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGeneratedFileData, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGeneratedFileData, OnError, OnAlways);
        }
    }

    //function for Read Generated File XML by adi
    function GetGeneratedFileXMLData() {
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {

            url: api.server + api.url.generatedfile + "/FileXML",

            //url: api.server + api.url.generatedfilexml,
            params: {
                page: self.GridGeneratedXMLProperties().Page(),
                size: self.GridGeneratedXMLProperties().Size(),
                sort_column: self.GridGeneratedXMLProperties().SortColumn(),
                sort_order: self.GridGeneratedXMLProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetGeneratedFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccesGeneratedFileXMLData, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccesGeneratedFileXMLData, OnError, OnAlways);
        }
    }

    // Get filtered columns value
    function GetFilteredColumns() {
        // define filter
        var filters = [];

        if (self.FilterapplicationID() != "") filters.push({ Field: 'applicationID', Value: self.FilterapplicationID().toUpperCase() });
        if (self.FiltercustomerName() != "") filters.push({ Field: 'customerName', Value: self.FiltercustomerName() });
        if (self.FilterproductName() != "") filters.push({ Field: 'productName', Value: self.FilterproductName() });
        if (self.FiltercurrencyDesc() != "") filters.push({ Field: 'currencyDesc', Value: self.FiltercurrencyDesc() });
        if (self.FiltertransactionAmount() != "") filters.push({ Field: 'transactionAmount', Value: self.FiltertransactionAmount() });
        if (self.FiltereqvUsd() != "") filters.push({ Field: 'eqvUsd', Value: self.FiltereqvUsd() });
        if (self.FilterfxTransaction() != "") filters.push({ Field: 'fxTransaction', Value: self.FilterfxTransaction() });
        if (self.FiltertopUrgentDesc() != "") filters.push({ Field: 'topUrgentDesc', Value: self.FiltertopUrgentDesc() });
        if (self.FilterstateID() != "") filters.push({ Field: 'stateID', Value: self.FilterstateID() });
        if (self.FilterModifiedBy() != "") filters.push({ Field: 'LastModifiedBy', Value: self.FilterModifiedBy() });
        if (self.FilterModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.FilterModifiedDate() });

        return filters;
    };

    // Get Generated File filtered columns value
    function GetGeneratedFilteredColumns() {
        // define filter
        var filters = [];

        if (self.FilterFileName() != "") filters.push({ Field: 'FileName', Value: self.FilterFileName() });
        if (self.FilterApplicationID() != "") filters.push({ Field: 'ApplicationID', Value: self.FilterApplicationID().toUpperCase() });

        return filters;
    };

    // add by adi
    function GetGeneratedXMLFilteredColumns() {
        var filters = [];
        if (self.FilterFileName() != "") filters.push({ Field: 'FileName', Value: self.FilterFileName() });
        if (self.FilterApplicationID() != "") filters.push({ Field: 'ApplicationID', Value: self.FilterApplicationID().toUpperCase() });
        return filters;
    }

    // On success GetData callback
    function OnSuccessGetData(data, textStatus, jqXHR) {
        if (jqXHR.status == 200) {
            self.Transactions(data.Rows);

            self.GridProperties().Page(data['Page']);
            self.GridProperties().Size(data['Size']);
            self.GridProperties().Total(data['Total']);
            self.GridProperties().TotalPages(Math.ceil(self.GridProperties().Total() / self.GridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    // On success GetGeneratedFileData callback
    function OnSuccessGeneratedFileData(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.GeneratedTransactions(data.Rows);
            // console.log(self.GeneratedTransactions());
            self.GridGeneratedProperties().Page(data['Page']);
            self.GridGeneratedProperties().Size(data['Size']);
            self.GridGeneratedProperties().Total(data['Total']);
            self.GridGeneratedProperties().TotalPages(Math.ceil(self.GridGeneratedProperties().Total() / self.GridGeneratedProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    //On succes GetGeneratedFileXMLData callback by adi
    function OnSuccesGeneratedFileXMLData(data, textStatus, jqXHR) {
        if (jqXHR.status == 200) {
            self.GeneratedTransactionXML(data.Rows);
            // console.log(self.GeneratedTransactionXML());
            self.GridGeneratedXMLProperties().Page(data['Page']);
            self.GridGeneratedXMLProperties().Size(data['Size']);
            self.GridGeneratedXMLProperties().Total(data['Total']);
            self.GridGeneratedXMLProperties().TotalPages(Math.ceil(self.GridGeneratedXMLProperties().Total() / self.GridGeneratedXMLProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    // On Error callback
    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    // On Always callback
    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }

    self.SelectedTransaction = function (item) {
        var isSelected = false;
        var itemFilter = ko.utils.arrayFirst(self.DataInformationCollection(), function (data) {
            if (data.ApplicationID == item.applicationID) {
                return isSelected = true;
            }
        });

        if (isSelected) {
            removeSelected(itemFilter);
        } else {
            self.DataInformationCollection.push({
                //GeneratedTransactionID: item.customerName,
                ApplicationID: item.applicationID,
                TransactionID: item.ID
                //FileName: item.customerName,
                //CreateDate: new Date(),
                //CreateBy: item.customerName
            });
        }
    };

    function removeSelected(item) {
        self.DataInformationCollection.remove(item);
    };

    self.GetParameters = function () { GetParameters(); }

    self.GetparametersIPE = function () { GetParametersIPE();}

    //Get parameters
    function GetParameters() {
        var options = {
            url: api.server + api.url.parameter,
            params: {
                select: "Product"
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetParameters, OnError, OnAlways);
    }

    //function OnSuccessGetParameters(data, textStatus, jqXHR) {
    //    if (jqXHR.status = 200) {
    //        // bind result to observable array
    //        if (data.Product != null) {
    //            var dataProduct = ko.utils.arrayFilter(data.Product, function (item) {
    //                    return item.ID == ConsProductID.RTGSProductIDCons || item.ID == ConsProductID.SKNProductIDCons || item.ID == ConsProductID.OverbookingProductIDCons;
    //            });

    //            if (dataProduct != null) {
    //                self.Products(dataProduct);
    //            }
    //        }
    //    } else {
    //        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    //    }
    //}

    function OnSuccessGetParameters(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            if(self.ModeValue() == 1) {
                // bind result to observable array
                if (data.Product != null) {
                    var dataProduct = ko.utils.arrayFilter(data.Product, function (item) {
                        return item.ID == ConsProductID.RTGSProductIDCons || item.ID == ConsProductID.SKNProductIDCons || item.ID == ConsProductID.OverbookingProductIDCons ;
                    });

                    if (dataProduct != null) {
                        self.Products(dataProduct);
                    }
                }
            }
            if (self.ModeValue() == 2) {
                if (data.Product != null) {
                    var dataProductIPE = ko.utils.arrayFilter(data.Product, function (item) {
                        return item.ID == ConsProductID.RTGSProductIDCons || item.ID == ConsProductID.SKNProductIDCons || item.ID == ConsProductID.OverbookingProductIDCons || item.ID == ConsProductID.OTTProductIDCons || item.ID == ConsProductID.SKNBulkProductIDCons;
                    });

                    if (dataProductIPE != null) {
                        self.Products(dataProductIPE);
                    }
                }
            }
            
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }   

    //self.GetPaymentMode = function () { GetPaymentMode(); }

    self.GetPaymentMode = function () {
        var options = {
            url: api.server + api.url.paymentmode,
            params: {
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetPaymentMode, OnError, OnAlways);
    }

    //add by adi
    function OnSuccessGetPaymentMode(data, textStatus, jqXHR) {
        self.GeneratePaymentMode = data;

        if (self.GeneratePaymentMode == 'IPE' || self.GenerateData == 'BCP1') {//self.GenerateData=='BCP1'self.
            if (self.SelectedMode() != null && self.SelectedMode !== undefined) {
                var SelectedMode = self.SelectedMode();
                self.SelectedMode = SelectedMode;
                self.ModeValue(2);
            }
        } else if (self.GenerateData == 'Finacle') { //self.GenerateData == 'Finacle'
            if (self.SelectedMode() != null && self.SelectedMode !== undefined) {
                var SelectedMode = self.SelectedMode();
                self.SelectedMode = SelectedMode;
                self.ModeValue(1);
            }
        }
        GetParameters();
    }

    self.OnChangeMode = function () {
       // console.log(self.ModeValue());
        if (self.ModeValue() == 1) {
            //self.Transactions("");
            self.ButtonGenerateFile(true);            
            //self.ButtonGenerateFile(false);
            // console.log(self.ButtonGenerateFile());
            self.FilterapplicationID("");
            self.FiltercustomerName("");
            self.FilterproductName("");
            self.FiltercurrencyDesc("");
            self.FiltertransactionAmount("");
            self.FiltereqvUsd("");
            self.FilterfxTransaction("");
            self.FiltertopUrgentDesc("");
            self.FilterstateID("");
            self.FilterModifiedBy("");
            self.FilterModifiedDate("");
            //viewModel.GetParameters();
            GetParameters();
            GetData();
        }
        if (self.ModeValue() == 2) {
            //self.Transactions("");
            self.ButtonGenerateFile(false);
            //self.ButtonGenerateFile(true);
            //console.log(self.ButtonGenerateFile());
            self.FilterapplicationID("");
            self.FiltercustomerName("");
            self.FilterproductName("");
            self.FiltercurrencyDesc("");
            self.FiltertransactionAmount("");
            self.FiltereqvUsd("");
            self.FilterfxTransaction("");
            self.FiltertopUrgentDesc("");
            self.FilterstateID("");
            self.FilterModifiedBy("");
            self.FilterModifiedDate("");
            //viewModel.GetParameters();
            GetParameters();
            GetData();
        }
    }

    self.HelperGetSelectedProduct = ko.dependentObservable(function () {
        var product = ko.utils.arrayFirst(self.Products(), function (item) { return item.ID == self.Selected().Product(); });

        if (product != null) {
            self.FilterproductName(product.Name); self.FileFormat(product.Code);
        } else {
            self.FilterproductName(""); self.FileFormat("");
        }
    });

    self.OnProductChange = function () {
        self.DataInformationCollection([]);
        GetData();
    };

    self.GenerateFile = function () { GenerateFile() };

    self.GenerateFileXML = function () { GenerateFileXML() };

    function GenerateFile() {
        var form = $("#aspnetForm");
        form.validate();

        //if (form.valid() && (self.FileFormat() != 'OT')) {
        if (form.valid() && (self.FileFormat() == 'RT') || (self.FileFormat() == 'SK') || (self.FileFormat() == 'OV') || (self.FileFormat() == 'SB')) {
            if (self.DataInformationCollection().length > 0) {
                var DataInformationCollection = {
                    Username: self.Username(),
                    Format: self.FileFormat(),
                    DataInformation: self.DataInformationCollection()
                };

                var options = {
                    url: "/_vti_bin/DBSGenerateFile/GenerateFileServices.svc/GenerateFile",
                    data: ko.toJSON(DataInformationCollection)
                };

                Helper.Sharepoint.Nintex.Post(options, OnSuccessGenerateFile, OnError, OnAlways);
            }
            else {
                ShowNotification("Form Validation Warning", "Please Tick Completed Transaction to Generate", 'gritter-warning', false);
            }
        }
        else if (form.valid() && (self.FileFormat() == 'OT')) {
            ShowNotification("Form Validation Warning", "Product OT Cann't Generate File TXT", 'gritter-warning', false);
        }
        else {
            ShowNotification("Form Validation Warning", "Please select the correct file format", 'gritter-warning', false);
        }
    }

    function GenerateFileXML() {
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid() && (self.FileFormat() == 'OT')||(self.FileFormat() == 'RT') || (self.FileFormat() == 'SK') || (self.FileFormat() == 'OV')) {
            if (self.DataInformationCollection().length > 0) {
                var DataInformationCollection = {
                    Username: self.Username(),
                    Format: self.FileFormat(),
                    DataInformation: self.DataInformationCollection()
                };

                var options = {
                    url: "/_vti_bin/DBSGenerateFile/GenerateFileServices.svc/GenerateFileXML",
                    data: ko.toJSON(DataInformationCollection)
                };

                Helper.Sharepoint.Nintex.Post(options, OnSuccessGenerateFileXML, OnError, OnAlways);
            }
            else {
                ShowNotification("Form Validation Warning", "Please Tick Completed Transaction to Generate", 'gritter-warning', false);
            }
        }
        else if (form.valid() && (self.FileFormat() == 'SB')) {
            ShowNotification("Form Validation Warning", "Product SKN Bulk Can't Generate File XML", 'gritter-warning', false);
        }
        else {
            ShowNotification("Form Validation Warning", "Please select the correct file format", 'gritter-warning', false);
        }
    }

    function OnSuccessGenerateFile(data, textStatus, jqXHR) {
        if (data.IsSuccess) {
            // send notification
            ShowNotification('Generate File Success', jqXHR.responseJSON.Message, 'gritter-success', false);

            // reload tasks
            GetData();
            GetGeneratedFileData();
            self.DataInformationCollection([]);
            
        } else {
            ShowNotification('Generate File Failed', jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    // add by Adi
    function OnSuccessGenerateFileXML(data, textStatus, jqXHR) {
        if (data.IsSuccess) {
            // send notification
            ShowNotification('Generate File XML Success', jqXHR.responseJSON.Message, 'gritter-success', false);
            // reload tasks
            GetData();
            // ClearGeneratedFileXMLFilter();
            GetGeneratedFileXMLData();
            self.DataInformationCollection([]);
        } else {
            ShowNotification('Generate File XML Failed', jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }


    //function Generate File Switch to IPE by
    //self.GenerateFileIPE = function () {
    //    if (self.GeneratePaymentMode == "BCP2") {
    //        GenerateFile();
    //    }
    //    else {
    //        GenerateFileXML();
    //    }
    //};

    //end Generate File Switch to IPE by adi

    // rename status fx transaction & top urgent
    self.RenameStatus = function (type, value) {
        if (type == "FX Transaction") {
            if (value != null) {
                return "Yes";
            } else {
                return "No";
            }
        } else if (type == "Urgency") {
            if (value) {
                return "Top Urgent";
            } else {
                return "Normal";
            }
        } else {
            return "Unknown";
        }
    };

    self.DownloadFile = function (data) { DownloadFile(data); };

    function DownloadFile(data) {
        try {
            //var arrfilename = data.FileName.split('_');
            //document.location.href = _spPageContextInfo.webAbsoluteUrl + '/_layouts/15/download.aspx?SourceUrl=%2FShared%20Documents%2FReport%5F' + arrfilename[1] + '%5F' + arrfilename[2] + '%5F' + arrfilename[3] + '&FldUrl=&Source=http%3A%2F%2Fmysweetlife%3A90%2FShared%2520Documents%2FForms%2FAllItems%2Easpx';
            filename = data.FileName;
            document.location.href = _spPageContextInfo.webAbsoluteUrl + '/_layouts/15/download.aspx?SourceUrl=%2FShared%20Documents%2F' + filename + '&FldUrl=&Source=http%3A%2F%2Fmysweetlife%3A90%2FShared%2520Documents%2FForms%2FAllItems%2Easpx';
        } catch (e) {
            console.log(e);
        }
    }
};

$(document).ready(function () {

    // add payment mode by adi for generate Transaction
    //GetPaymentStatus(function (data) {
    // viewModel.TransactionModel().ModePayment(StatusIPE);

    $box = $('#widget-box');
    var event;
    $box.trigger(event = $.Event('reload.ace.widget'))
    if (event.isDefaultPrevented()) return
    $box.blur();

    /// block enter key from user to prevent submitted data.
    $("input").keypress(function (event) {
        var code = event.charCode || event.keyCode;
        if (code == 13) {
            ShowNotification("Page Information", "Enter key is disabled for this form.", "gritter-warning", false);

            return false;
        }
    });

    // scrollables
    $('.slim-scroll').each(function () {
        var $this = $(this);
        $this.slimscroll({
            height: $this.data('height') || 100,
            //width: $this.data('width') || 100,
            railVisible: true,
            alwaysVisible: true,
            color: '#D15B47'
        });
    });

    var viewModel = new ViewModel();
    ko.applyBindings(viewModel);

    // Token Validation
    if ($.cookie(api.cookie.name) == undefined) {
        Helper.Token.Request(TokenOnSuccess, TokenOnError);
    } else {
        // read token from cookie
        accessToken = $.cookie(api.cookie.name);

        // call spuser function
        GetCurrentUser(viewModel);

        // call get data inside view model
        viewModel.GetData();
        viewModel.GetGeneratedFileData();
        viewModel.GetGeneratedFileXMLData();
        //viewModel.GetParameters();
        //viewModel.GetparametersIPE();
        viewModel.GetPaymentMode();
    }

    $('.date-picker').datepicker({ autoclose: true }).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });

    //$("#aspnetForm").validate({onsubmit: false});
    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }

    });
});

// Get SPUser from cookies
function GetCurrentUser(vm) {
    if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
        spUser = $.cookie(api.cookie.spUser);

        vm.SPUser = spUser;
        vm.Username(spUser.DisplayName);
    }
}


// Token validation On Success function
function TokenOnSuccess(data, textStatus, jqXHR) {
    // store token on browser cookies
    $.cookie(api.cookie.name, data.AccessToken);
    accessToken = $.cookie(api.cookie.name);

    // call spuser function
    GetCurrentUser();

    // call get data inside view model
    //viewModel.GetParameters();
    //ViewModel.GetParametersIPE();
    viewModel.GetPaymentMode();
    viewModel.GetData();
}

// Token validation On Error function
function TokenOnError(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}
