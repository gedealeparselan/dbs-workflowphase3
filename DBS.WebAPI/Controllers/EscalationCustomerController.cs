﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    public class EscalationCustomerController : ApiController
    {
        private string message = string.Empty;

        private readonly IEscalationCustomerRepository repository = null;

        public EscalationCustomerController()
        {
            this.repository = new EscalationCustomerRepository();
        }

        public EscalationCustomerController(IEscalationCustomerRepository repository)
        {
            this.repository = repository;
        }

        [ResponseType(typeof(EscalationCustomerGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetEscalatedCustomer(int? page, int? size, string sort_column, string sort_order, IList<EscalationCustomerFilter> filters)
        {
            EscalationCustomerGrid output = new EscalationCustomerGrid(); 
            if (!repository.GetEscalationCustomer(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<EscalationCustomerGrid>(HttpStatusCode.OK, output);
            }
        }
        /// <summary>
        /// Create a new Matrix .
        /// </summary>
        /// <param name="Matrix">Matrix  data.</param>
        /// <returns></returns>
        [HttpPost]       
        [ResponseType(typeof(string))]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(IList<EscalationCustomerModel> escalationcustomer)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddEscalationCustomer(escalationcustomer, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New Escalation Customer data has been created." });
                }
            }
        }
        
    }
}
