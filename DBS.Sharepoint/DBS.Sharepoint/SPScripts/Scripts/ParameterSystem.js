﻿var ViewModel = function () {
    var self = this;
    var queryDropdown;
    var query;
    getAPIUrl();


    self.ProductTypeID = ko.observable(null);
    self.ProductTypeCode = ko.observable("");
    self.ProductType = ko.observable(new ProductTypeModel('', ''));

    function getUrlVars() {
        //var href = window.location.href.slice(window.location.href.indexOf('/') + 1).split("-");
        //return href[href.length - 1]; 
        var href = window.location.href.slice(window.location.href.indexOf('/') + 1);
        return href.toLowerCase();
    };

    function getAPIUrl() {
        //console.log(getUrlVars());
        var getModul = getUrlVars();
        var qs = (getModul != undefined) && (getModul != null) ? getModul : "system";

        /*if (qs.con == "collateral") {
            query = api.server + api.url.parametersystemcollateral;
            queryDropdown = api.server + api.url.parametersystemcollateral + '/SD';
        } else if (qs == "fd") {
            query = api.server + api.url.parametersystemfd;
            queryDropdown = api.server + api.url.parametersystemfd + '/SD';
        } else if (qs == "ut") {
            query = api.server + api.url.parametersystemut;
            queryDropdown = api.server + api.url.parametersystemut + '/SD';
        } else if (qs == "cif") {
            query = api.server + api.url.parametersystemcif;
            queryDropdown = api.server + api.url.parametersystemcif + '/SD';
        } else if (qs == "tmo") {
            query = api.server + api.url.parametersystemtmo;
            queryDropdown = api.server + api.url.parametersystemtmo + '/SD';
        } else if (qs == "loan") {
            query = api.server + api.url.parametersystemloan;
            queryDropdown = api.server + api.url.parametersystemloan + '/SD';
        } else {
            query = api.server + api.url.parametersystem;
            queryDropdown = api.server + api.url.parametersystem + '/SD';
        }*/
        if (qs.indexOf("collateral") > -1) {
            query = api.server + api.url.parametersystemcollateral;
            queryDropdown = api.server + api.url.parametersystemcollateral + '/SD';
        } else if (qs.indexOf("fd") > -1) {
            query = api.server + api.url.parametersystemfd;
            queryDropdown = api.server + api.url.parametersystemfd + '/SD';
        } else if (qs.indexOf("ut") > -1) {
            query = api.server + api.url.parametersystemut;
            queryDropdown = api.server + api.url.parametersystemut + '/SD';
        } else if (qs.indexOf("cif") > -1) {
            query = api.server + api.url.parametersystemcif;
            queryDropdown = api.server + api.url.parametersystemcif + '/SD';
        } else if (qs.indexOf("tmo") > -1) {
            query = api.server + api.url.parametersystemtmo;
            queryDropdown = api.server + api.url.parametersystemtmo + '/SD';
        } else if (qs.indexOf("loan") > -1) {
            query = api.server + api.url.parametersystemloan;
            queryDropdown = api.server + api.url.parametersystemloan + '/SD';
        } else {
            query = api.server + api.url.parametersystem;
            queryDropdown = api.server + api.url.parametersystem + '/SD';
        }
        return;
    };
    //console.log(query);
    //console.log(queryDropdown);

    //Model StaticDropdown
    var SD = function (ddItem, ddItemValue) {
        var self = this;
        self.DropdownItem = ko.observable(ddItem);
        self.DropdownItemValue = ko.observable(ddItemValue);
    }

    self.ID = ko.observable(0);
    self.ParameterType = ko.observable("");
    self.ParameterValue = ko.observable("");
    self.LastModifiedBy = ko.observable("");
    self.LastModifiedDate = ko.observable("");

    self.ddlStaticDropdown = ko.observableArray([]);
    self.ddlProductType = ko.observableArray([]);

    //Filter
    self.FilterProductType = ko.observable("");
    self.FilterParameterType = ko.observable("");
    self.FilterParameterValue = ko.observable("");
    self.FilterModifiedDate = ko.observable("");
    self.FilterModifiedBy = ko.observable("");

    //Grid Properties
    self.AvailableSizes = ko.observableArray([10, 25, 50, 75, 100]);
    self.Page = ko.observable(1);
    self.Size = ko.observableArray([10]);
    self.Total = ko.observable(0);
    self.TotalPages = ko.observable(0);

    self.GridProperties = ko.observable();
    self.GridProperties(new GridPropertiesModel(GetData));
    self.GridProperties().SortColumn("ParameterType");
    self.GridProperties().SortOrder("ASC");

    //Clear filter
    self.ClearFilters = function () {
        self.FilterProductType("");
        self.FilterParameterType("");
        self.FilterParameterValue("");
        self.FilterModifiedBy("");
        self.FilterModifiedDate("");
        GetData();
    };

    //Flag
    self.IsNewData = ko.observable(false);
    self.Readonly = ko.observable(false);
    self.ReadonlyFDRate = ko.observable(false);
    self.IsWorkflow = ko.observable(false);
    self.IsRoleMaker = ko.observable(false);
    self.IsPermited = ko.observable(false);
    self.IsParameterSystemTMO = ko.observable(false);
    //
    var ParamSystem = {
        ID: self.ID,
        ProductTypeID: null,
        ProductTypeCode: null,
        ProductType: self.ProductType,
        ParameterType: self.ParameterType,
        ParameterValue: self.ParameterValue,
        LastModifiedBy: self.LastModifiedBy,
        LastModifiedDate: self.LastModifiedDate
    }

    self.GetData = function () {
        GetData();
    }

    self.ParamSystems = ko.observableArray([]);
    
    self.save = function () {

        // validation
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            var ps = ko.toJS(ParamSystem);
            //console.log(ps);
            //console.log(ps.ParameterType);
            //start dani
            var existing = false;
            if (ps.ParameterType == "FD_MAX_RATE" || ps.ParameterType == "FD_MIN_RATE") {                
                $.ajax({
                    type: "GET",
                    url: query + "/ExistingMaxMinRate/" + ps.ParameterType,
                    data: ko.toJSON(ParamSystem), //Convert the Observable Data into JSON
                    contentType: "application/json",
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        if (jqXHR.status == 200) {
                            // send notification
                            ShowNotification('Data Existing', jqXHR.responseJSON.Message, 'gritter-warning', false);
                            // hide current popup window
                            $("#modal-form").modal('hide');
                            existing = true;
                        } 
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
                return existing;
            }
            //end dani 27-7-2016
            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {
                    if (ko.toJS(self.IsParameterSystemTMO())) {
                        var ProductTypeID = $('#producttype').find('option:selected').val();
                        var ProductTypeCode = $('#producttype').find('option:selected').text();

                        self.ProductTypeCode = ProductTypeCode;
                        ParamSystem.ProductTypeID = self.ProductTypeID();
                        ParamSystem.ProductTypeCode = self.ProductTypeCode;
                        ParamSystem.ProductType(new ProductTypeModel(ProductTypeID, ProductTypeCode));
                    } else {
                        self.ProductTypeCode = '';
                        ParamSystem.ProductTypeID = null;
                        ParamSystem.ProductTypeCode = null;
                        ParamSystem.ProductType(new ProductTypeModel('', ''));

                    }
                    //Ajax call to insert 
                    $.ajax({
                        type: "POST",
                        url: query,
                        data: ko.toJSON(ParamSystem), //Convert the Observable Data into JSON
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status == 200) {
                                // send notification
                                ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // hide current popup window
                                $("#modal-form").modal('hide');

                                // refresh data
                                GetData();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });
                }
            });
        }
    };
    self.update = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {
                    if (ko.toJS(self.IsParameterSystemTMO())) {
                        var ProductTypeID = $('#producttype').find('option:selected').val();
                        var ProductTypeCode = $('#producttype').find('option:selected').text();

                        self.ProductTypeCode = ProductTypeCode;
                        ParamSystem.ProductTypeID = self.ProductTypeID();
                        ParamSystem.ProductTypeCode = self.ProductTypeCode;
                        ParamSystem.ProductType(new ProductTypeModel(ProductTypeID, ProductTypeCode));
                    } else {
                        self.ProductTypeCode = '';
                        ParamSystem.ProductTypeID = null;
                        ParamSystem.ProductTypeCode = null;
                        ParamSystem.ProductType(new ProductTypeModel('', ''));

                    }
                    $.ajax({
                        type: "PUT",
                        url: query + "/" + ParamSystem.ID(),
                        data: ko.toJSON(ParamSystem),
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status == 200) {
                                // send notification
                                ShowNotification('Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // hide current popup window
                                $("#modal-form").modal('hide');

                                // refresh data
                                GetData();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });
                }
            })
        }
    };
    self.delete = function () {
        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                $("#modal-form").modal('show');
            } else {
                if (ko.toJS(self.IsParameterSystemTMO())) {
                    var ProductTypeID = $('#producttype').find('option:selected').val();
                    var ProductTypeCode = $('#producttype').find('option:selected').text();

                    self.ProductTypeCode = ProductTypeCode;
                    ParamSystem.ProductTypeID = self.ProductTypeID();
                    ParamSystem.ProductTypeCode = self.ProductTypeCode;
                    ParamSystem.ProductType(new ProductTypeModel(ProductTypeID, ProductTypeCode));
                } else {
                    self.ProductTypeCode = '';
                    ParamSystem.ProductTypeID = null;
                    ParamSystem.ProductTypeCode = null;
                    ParamSystem.ProductType(new ProductTypeModel('', ''));

                }


                $.ajax({
                    type: "DELETE",
                    url: query + "/" + ParamSystem.ID(),
                    data: ko.toJSON(ParamSystem),
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if (jqXHR.status == 200) {
                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                            // refresh data
                            GetData();
                        } else
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
            }
        });
    };

    // bind get data function to view
    function GetData() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: query,//
            params: {
                page: self.GridProperties().Page(),
                size: self.GridProperties().Size(),
                sort_column: self.GridProperties().SortColumn(),
                sort_order: self.GridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetData, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetData, OnError, OnAlways);
        }
    };

    function GetDropdown() {
        var GetURL = getUrlVars();

        if (GetURL.indexOf("tmo") > -1 || self.ProductTypeID != null || self.ProductTypeID != 0) {
            self.IsParameterSystemTMO(true);
        }

        //console.log(ko.toJS(self.IsParameterSystemTMO()));
        $.ajax({
            async: false,
            type: "GET",
            url: queryDropdown,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status == 200) {
                    console.log("dropdown : " + ko.toJSON(data));                   
                    if (data != null) { self.ddlStaticDropdown(data); }
                    if (ko.toJS(self.IsParameterSystemTMO()) == true) {
                        var Url = api.server + api.url.parameter;
                        var params = {
                            select: "ProductType"
                        };
                        if (params != null || params != undefined) {
                            Url += DecodeParams(params);
                        }

                        $.ajax({
                            url: Url,
                            async: false,
                            type: "GET",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            headers: {
                                "Authorization": "Bearer " + accessToken
                            },
                            success: function (data, textStatus, jqXHR) {
                                var mapping = {
                                    'ignore': ["LastModifiedDate", "LastModifiedBy"]
                                };

                                self.ddlProductType(ko.mapping.toJS(data.ProductType, mapping));

                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                            }
                        });
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }

        });
    };

    self.GetDropdown = function () {
        GetDropdown();
    };

    self.NewData = function () {
        self.IsNewData(true);
        self.Readonly(false);
        self.ID(0);
        self.ProductTypeID(null);
        self.ParameterType("");
        self.ParameterValue("");
        self.ProductTypeCode("");
        self.ProductType(new ProductTypeModel('', ''));
    };
    self.IsPermited = function (IsPermitedResult) {
        var GetURL = getUrlVars();
        if (GetURL.indexOf("tmo") > -1) {
            self.IsParameterSystemTMO(true);
        }
        //Ajax call to delete the Customer
        spUser = $.cookie(api.cookie.spUser);

        $.ajax({
            async: false,
            type: "GET",
            url: api.server + api.url.helper + "/CheckUserPermited/" + _spFriendlyUrlPageContextInfo.title,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status == 200) {

                    //compare user role to page permission to get checker validation
                    //console.log("spUser Roles : " + ko.toJSON(spUser.Roles));
                    //console.log("spUser Permited : " + data);
                    for (i = 0; i < spUser.Roles.length; i++) {
                        for (j = 0; j < data.length; j++) {
                            if (Const_RoleName[spUser.Roles[i].ID] == data[j]) { //if (spUser.Roles[i].Name == data[j]) {
                                if (Const_RoleName[spUser.Roles[i].ID].endsWith('Maker')) { //if (spUser.Roles[i].Name.endsWith('Maker')) {
                                    self.IsRoleMaker(true);
                                    return;
                                }
                                else {
                                    self.IsRoleMaker(false);
                                }
                            }
                        }
                    }
                    console.log("IsRoleMaker : " + self.IsRoleMaker());

                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    };

    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return "";
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-18
    };

    // Get filtered columns value
    function GetFilteredColumns() {
        // define filter
        var filters = [];
        if (self.FilterProductType() != "") filters.push({ Field: 'ProductType', Value: self.FilterProductType() });
        if (self.FilterParameterType() != "") filters.push({ Field: 'ParameterType', Value: self.FilterParameterType() });
        if (self.FilterParameterValue() != "") filters.push({ Field: 'ParameterValue', Value: self.FilterParameterValue() });
        if (self.FilterModifiedBy() != "") filters.push({ Field: 'LastModifiedBy', Value: self.FilterModifiedBy() });
        if (self.FilterModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.FilterModifiedDate() });
        return filters;
    };

    //Function to Display record to be updated
    self.GetSelectedRow = function (data) {
        console.log(data);
        self.ParameterType("");
        self.ParameterValue("");
        self.IsNewData(false);
        if (data.ParameterType == "FD_MIN_RATE" || data.ParameterType == "FD_MAX_RATE") {
            self.ReadonlyFDRate(true);
        }

        //console.log("query : " + query);
        //console.log("queryDropdown : " + queryDropdown);
        if (self.IsWorkflow()) $("#modal-form-workflow").modal('show');
        else $("#modal-form").modal('show');
        //console.log(data);
        self.ID(data.ID);
        if (ko.toJS(self.IsParameterSystemTMO())) {
            self.ProductTypeID(data.ProductType.ID != 0 ? data.ProductType.ID : null);
        } else {
            self.ProductTypeID(null);
        }
        self.ParameterType(data.ParameterType);
        self.ParameterValue(data.ParameterValue);
    };

    // On success GetData callback
    function OnSuccessGetData(data, textStatus, jqXHR) {
        if (jqXHR.status == 200) {
            self.ParamSystems(data.Rows);
            //console.log(data.Rows);
            self.GridProperties().Page(data['Page']);
            self.GridProperties().Size(data['Size']);
            self.GridProperties().Total(data['Total']);
            self.GridProperties().TotalPages(Math.ceil(self.GridProperties().Total() / self.GridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    // On Error callback
    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }

    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }

    });

}