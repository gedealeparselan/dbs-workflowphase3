//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DBS.Entity
{
    using System;
    
    public partial class SP_ETL_TZReconcile_Result
    {
        public string CustomerCIF { get; set; }
        public string AccountNumber { get; set; }
        public string CustomerName { get; set; }
        public string DealReference { get; set; }
        public string ProductType { get; set; }
        public Nullable<System.DateTime> TradeDate { get; set; }
        public Nullable<System.DateTime> ValueDate { get; set; }
        public string CustBuyCCY { get; set; }
        public decimal CustBuyAmount { get; set; }
        public string CustSELLCCY { get; set; }
        public decimal CustSELLAmount { get; set; }
        public decimal USDEquivalent { get; set; }
        public decimal Cust_Rate { get; set; }
        public string ProcessingDept { get; set; }
        public string BranchCode { get; set; }
        public string SourceSystem { get; set; }
        public int StatementLetterID { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public int IsFx { get; set; }
    }
}
