/// Requesting api token for sharepoint user purpose only.
/// Ruddy Cahyadi 20-08-2014
/// DBS Sharepoint & Web API

$.cookie.json = true;
//alert(JSON.stringify($.cookie()));
//$.removeCookie(api['cookie']['name']);
var spUser;

// checking stored token on browser cookies
if($.cookie(api.cookie.name) == undefined){
	// the token is doesn't exist
	
	/*
	if(confirm("Continue requesting new token to web API?")){
		// get current user information from sharepoint API
		getCurrentUser();
	
		// set cookie
		//$.cookie(api['cookie']['name'], 'Test api token', { expires : 30, path: "/" });
	}*/
	
	// get current user information from sharepoint API
	getCurrentUser();
}else{
	// set cookie
	//var token = $.cookie(api['cookie']['name']);

	//alert("Your token is "+ token)
}

function getCurrentUser(){
	//SharepointAPI("GET", "/_api/web/currentuser?$select=id", getCurrentUserOnSuccess);
	//alert(JSON.stringify(spUser));
	
	$.ajax({
		type: "GET",
		url: "/_api/web/currentuser?$select=id,LoginName",
		headers: {
			"accept": "application/json;odata=verbose",
		},
		success: function (data, textStatus, jqXHR) {
			//Function.createDelegate(onSuccess(data)); 
			getCurrentUserOnSuccess(data);            
		},
		error: function (jqXHR, textStatus, errorThrown) {
			// send notification
			ShowNotification('An Error Occured', jqXHR.responseText, 'gritter-error', false);
		}
	});

}

function SharepointAPI(method, url, onSuccess){
	$.ajax({
		type: "GET",
		url: "/_api/web/currentuser?$select=id",
		headers: {
			"accept": "application/json;odata=verbose",
		},
		success: function (data, textStatus, jqXHR) {
			Function.createDelegate(onSuccess(data));               
		},
		error: function (jqXHR, textStatus, errorThrown) {
			// send notification
			ShowNotification('An Error Occured', jqXHR.responseText, 'gritter-error', false);
		}
	});
}

function getCurrentUserOnSuccess(data){
	//var url = "/_api/web/GetUserById("+ data.d.Id +")";
	//alert(url);
	//SharepointAPI("GET", url, getUserByIDOnSuccess);
	//alert("Done")
	
	$.ajax({
		type: "GET",
		url: "/_api/web/GetUserById("+ data.d.Id +")?$select=Id,LoginName,Title,Email",
		headers: {
			"accept": "application/json;odata=verbose",
		},
		success: function (data, textStatus, jqXHR) {
			//Function.createDelegate(onSuccess(data));
			getUserByIDOnSuccess(data);
		},
		error: function (jqXHR, textStatus, errorThrown) {
			// send notification
			ShowNotification('An Error Occured', jqXHR.responseText, 'gritter-error', false);
		}
	});

}

function getUserByIDOnSuccess(datas){
	// fill data to spUser 
	spUser = {
		ID : datas.d.Id,
		LoginName : datas.d.LoginName,
		DisplayName : datas.d.Title,
		Email : datas.d.Email,
		Roles : {}
	};
	
	$.ajax({
		type: "GET",
		url: "/_api/web/GetUserById("+ datas.d.Id +")/Groups?$select=Id,Title",
		headers: {
			"accept": "application/json;odata=verbose",
		},
		success: function (data, textStatus, jqXHR) {
			//alert(JSON.stringify(data.d.results[0].Title));
			//spUser['ID'] = '0000000000';
			//spUser['Roles'] = {ID  : data.d.results[0].ID, Name: data.d.results[0].Title };
			var roles = []
			for(i=0; i < data.d.results.length; i++){
				roles.push({
					ID: data.d.results[i].Id, 
					//Name: data.d.results[i].Title
				});	
			}
			
			spUser['Roles'] = roles;
			//alert(JSON.stringify(spUser))
			
			requestToken();
		},
		error: function (jqXHR, textStatus, errorThrown) {
			// send notification
			ShowNotification('An Error Occured', jqXHR.responseText, 'gritter-error', false);
		}
	});
	
	
}

function requestToken(){
	$.ajax({
		type: "POST",
		url: api.server + api.url.auth,
		//crossDomain: true,
		//cache: false,
		data: JSON.stringify(spUser),
		contentType: "application/json",
		dataType: "json",
		success: function (data, textStatus, jqXHR) {
			//alert(data.LifeTime.Expires)
			var expireDate = new Date(data.LifeTime.Expires);
			//alert(d)
			// store token to cookie
			//$.cookie(api['cookie']['name'], data.AccessToken, { expires : expireDate });
			$.cookie(api.cookie.name, data.AccessToken);
		
			// send notification
			ShowNotification('Access Token Success', data.AccessToken, 'gritter-success', false);
			                   
		},
		error: function (jqXHR, textStatus, errorThrown) {
			// send notification
			ShowNotification('An Error Occured', jqXHR.responseText, 'gritter-error', false);
		}
	});

}

function ShowNotification(title, text, className, isSticky){
	$.gritter.add({
		title: title,
		text: text,
		class_name: className,
		sticky: isSticky,
	});
}


//var token = $.cookie();
//alert(JSON.stringify($.cookie()));

//$.cookie('APIToken', 'Test api token');
//$.removeCookie('APIToken');




/* --------------------------------------------------------
var cookie = document.cookie;
//alert(cookie);

//deleteCookie("APIToken");
checkCookie("APIToken");

function getCookie(cname){
	var name = cname + "=";
	var ca = document.cookie.split(';');
	
	for(var i=0; i < ca.length; i++){
		var c = ca[i];
		
		while(c.charAt(0) == '') c = c.substring(1);
			
		if(c.indexOf(name) != -1)
			var value = c.substring(name.length, c.length);
			return value.replace('=', '');
	}
	
	return "";
}

function setCookie(cname, cvalue, exdays){
	var d = new Date();
	d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
	
	var expires = "expires=" + d.toGMTString();
	
	document.cookie = cname + "=" + cvalue + ";" + expires;
}

function deleteCookie(cname){
	document.cookie = cname + "=" + ";expires=Thu, 01 Jan 1970, 00:00:00 GMT";
}

function checkCookie(cname){
	var token = getCookie(cname);
	
	if(token != "")
	{
		alert("cookie name " + cname + " = "+ getCookie(cname));
	}else{
		token = prompt("Please enter "+ cname +" value", "");
		
		if(token != "" || token != null)
			setCookie(cname, token, 30);
	}
}
*/
