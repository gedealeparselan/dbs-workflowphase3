﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UploadDocumentUTWPUserControl.ascx.cs" Inherits="DBS.Sharepoint.SPWebparts.Others.UploadDocumentUTWP.UploadDocumentUTWPUserControl" %>

<div id="new-transaction" class="modal-body overflow-visible">
    <div class="row">
        <div class="col-xs-12">
        </div>
    </div>
    <!-- modal header start -->
    <div class="modal-body overflow-visible" id="header-transaction">
        <div class="row">
            <div class="col-xs-12">
                <div id="upload-form" class="form-horizontal" role="form">
                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="upload-type">Upload Type</label>
                        <div class="col-sm-3">
                            <div class="clearfix">
                                <select id="upload-type" name="upload-type" data-rule-required="true" data-rule-value="true" data-bind="options: $root.Parameter().UploadTypes, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Please Select...', value: $root.Selected().UploadTypes"></select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="file-name">File Name</label>
                        <div class="col-sm-9">
                            <div class="clearfix">
                                <input type="file" id="document-upload" name="document-path-upload" data-bind="file: DocumentPath" class="col-xs-12" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="upload"></label>
                        <div class="col-sm-3">
                            <div class="clearfix">
                                <button class="btn btn-sm btn-primary" data-bind="click: $root.btnUpload"><i class="icon-edit"></i>Upload</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-form-Upload" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="blue bigger">Upload Progress
                    </h4>
                </div>
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="transaction-progress" class="col-sm-12" style="display: none">
                                <h4 class="smaller blue">Upload in progress, please wait...</h4>
                                <span class="space-4"></span>
                                <div class="progress progress-small progress-striped active">
                                    <div class="progress-bar progress-bar-info" style="width: 100%"></div>
                                </div>
                            </div>
                            <div id="transaction-result" class="col-sm-12" style="display: none">
                                <div class="dataTables_wrapper" role="grid">
                                    <h4><span data-bind="text: $root.resultHeader()" /></h4>
                                    <span data-bind="text: $root.resultMessage()" />
                                </div>
                            </div>
                            <div id="transaction-grid" class="col-sm-12" style="display: none">
                                <div class="dataTables_wrapper" role="grid">
                                    <table class="table table-striped table-bordered table-hover dataTable">
                                        <thead>
                                            <tr>
                                                <th style="width: 50px">No.</th>
                                                <th><span data-bind="text: $root.Column1Name"></th>
                                                <th><span data-bind="text: $root.Column2Name"></th>
                                            </tr>
                                        </thead>
                                        <tbody data-bind="foreach: $root.RowFailed, visible: $root.RowFailed().length > 0">
                                            <tr>
                                                <!--data-bind="click: $root.EditDocument"-->
                                                <td><span data-bind="text: $index() + 1"></span></td>
                                                <td><span data-bind="text: Column1"></span></td>
                                                <td><span data-bind="text: Column2"></span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.CloseProgres">
                        <i class="icon-save"></i>
                        Close
                    </button>
                </div>
            </div>
        </div>
    </div>

    <%--<div>
        <!-- modal header start Underlying Form -->
        <div class="modal-header">
            <h5 class="blue bigger">
                <span>List of Failed Upload</span>
            </h5>
        </div>
        <!-- modal header end -->
        <!-- modal body start -->
        <div class="modal-body overflow-visible">
            <div id="modalformTz">
                <div class="row">
                    <div class="col-xs-12">

                        <!-- modal body form start -->
                        <div class="dataTables_wrapper" role="grid">
                            <table class="table table-striped table-bordered table-hover dataTable">
                                <thead>
                                    <tr>
                                        <!--<th>Choose</th>-->
                                        <th>No</th>
                                        <th>Row Number</th>
                                        <th>Item Name</th>
                                        <th>Status</th>
                                        <th>Modified by</th>
                                        <th>Modified Date</th>
                                    </tr>
                                </thead>
                                <tbody data-bind="foreach: $root.FailedUploads, visible: $root.FailedUploads().length > 0">
                                    <tr>
                                        <td><span data-bind="text: RowID"></span></td>
                                        <td><span data-bind="text: RowNumber"></span></td>
                                        <td><span data-bind="text: ItemName"></span></td>
                                        <td><span data-bind="text: Status"></span></td>
                                        <td><span data-bind="text: ModifiedBy"></span></td>
                                        <td><span data-bind="text: $root.LocalDate(ModifiedDate, true, false)"></span></td>
                                    </tr>
                                </tbody>
                                <tbody data-bind="visible: $root.FailedUploads().length == 0">
                                    <tr>
                                        <td colspan="13" class="text-center">No entries available.
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- modal body form end -->

                        <!-- widget footer start -->
                        <div class="widget-toolbox padding-8 clearfix">
                            <div class="row" data-bind="with: GridProperties">
                                <!-- pagination size start -->
                                <div class="col-sm-6">
                                    <div class="dataTables_paginate paging_bootstrap pull-left">
                                        Showing
                                        <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                        rows of <span data-bind="text: Total"></span>
                                        entries
                                    </div>
                                </div>
                                <!-- pagination size end -->
                                <!-- pagination page jump start -->
                                <div class="col-sm-3">
                                    <div class="dataTables_paginate paging_bootstrap">
                                        Page
                                        <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                        of <span data-bind="text: TotalPages"></span>
                                    </div>
                                </div>
                                <!-- pagination page jump end -->
                                <!-- pagination navigation start -->
                                <div class="col-sm-3">
                                    <div class="dataTables_paginate paging_bootstrap">
                                        <ul class="pagination">
                                            <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                    <i class="icon-double-angle-left"></i>
                                                </a>
                                            </li>
                                            <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                    <i class="icon-angle-left"></i>
                                                </a>
                                            </li>
                                            <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                    <i class="icon-angle-right"></i>
                                                </a>
                                            </li>
                                            <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                    <i class="icon-double-angle-right"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- pagination navigation end -->

                            </div>
                        </div>
                        <!-- widget footer end -->
                    </div>
                </div>
            </div>
        </div>
        <!-- modal body end -->
    </div>--%>
</div>

<!-- additional page scripts start -->
<script type="text/javascript" src="/SiteAssets/Scripts/Helper.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/date-time/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/additional-methods.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/Knockout/knockout.mapping-latest.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/UploadDocumentsUT.js"></script>
