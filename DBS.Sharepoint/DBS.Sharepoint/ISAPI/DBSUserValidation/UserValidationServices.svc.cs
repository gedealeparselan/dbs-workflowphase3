﻿using Microsoft.SharePoint;
using Nintex.Workflow.HumanApproval;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.ServiceModel.Activation;
using System.Text;
using System.Threading.Tasks;
//using LDAPAuthentication;
using System.DirectoryServices;
using DBS.Sharepoint.Entity;
using DBS.Sharepoint.Repository;

namespace DBS.Sharepoint
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class UserValidationServices : IUserValidationServices
    {
        private bool IsSuccess { get; set; }
        private string ErrorMessage { get; set; }


        public string UserAD(UserData userData)
        {
            bool sStatus = false;
            string claimUser = string.Format("i:0#.f|{0}|{1}", "DBSMembership", userData.UserID);
            string[] usr = SPContext.Current.Web.CurrentUser.LoginName.ToString().Split('|');
            string claimUser2 = string.Format("i:0#.f|{0}|{1}", "DBSMembership", usr[2]);
            if (CekNilai(userData.UserID, userData.Password))
            {

                //Repository.UserValidationServices UserValidation = new Repository.UserValidationServices();
                //string userRoleMaker = UserValidation.GetUserRoleModel2(claimUser2);
                //string userRoleChecker = UserValidation.GetUserRoleModel2(claimUser);

                //return userRoleMaker + " - " + userRoleChecker;
                //===============================================================

                //IList<UserRoleModel> userRoleMaker = null;
                //IList<UserRoleModel> userRoleChecker = null;

                //Repository.UserValidationServices UserValidation = new Repository.UserValidationServices();
                //userRoleMaker = UserValidation.GetUserRoleModel(claimUser2);
                //userRoleChecker = UserValidation.GetUserRoleModel(claimUser);
                //if (userRoleMaker != null && userRoleChecker != null)
                //{

                //    var data =
                //                (from UM in userRoleMaker
                //                 join UC in userRoleChecker on UM.RoleName.Substring(0, UM.RoleName.Length - 5) equals
                //                     UC.RoleName.Substring(0, UC.RoleName.Length - 7)

                //                 select new UserRoleModel
                //                 {
                //                     EmployeeID = UM.EmployeeID + UC.EmployeeID,
                //                     EmployeeUsername = UM.EmployeeUsername + UC.EmployeeUsername,
                //                     RoleName = UM.RoleName.Substring(0, UM.RoleName.Length - 5) + " - " +
                //                     UC.RoleName.Substring(0, UC.RoleName.Length - 7)
                //                 }).ToList();

                //    if (data != null || data.Count > 0)
                //    {
                //        return data.Select(x => x.EmployeeID).FirstOrDefault() + " - " + data.Select(x => x.EmployeeUsername).FirstOrDefault() + " - " +data.Select(x => x.RoleName).FirstOrDefault();
                //    }
                //    else
                //    {
                //        return "kosong";
                //    }
                //}
                //else
                //{
                //    return "kosong dua2nya";
                //}

                //============================================
                if (IsSameGroupApproval(claimUser2, claimUser))
                {
                    string sFullName = System.Web.Configuration.WebConfigurationManager.AppSettings["DomainName"].ToString() + @"\" + userData.UserID;
                    string sDomain = System.Web.Configuration.WebConfigurationManager.AppSettings["Domain"].ToString();
                    DirectoryEntry entry = new DirectoryEntry(sDomain, sFullName, userData.Password);
                    try
                    {
                        Object obj = entry.NativeObject;
                        DirectorySearcher search = new DirectorySearcher(entry);
                        search.Filter = "(SAMAccountName=" + sFullName + ")";
                        search.PropertiesToLoad.Add("cn");
                        //search.PropertiesToLoad.Add("users");
                        //search.PropertiesToLoad.Add("DBS");
                        SearchResult result = search.FindOne();
                        if (null == result)
                        {
                            //return sStatus;
                            return "Succeded";
                        }
                        else
                        {
                            //return sStatus;
                            return "Incorrect Username or Password";
                        }
                    }
                    catch (Exception ex)
                    {
                        //return sStatus;
                        return ex.Message;
                    }
                }
                else
                {
                    //return sStatus;
                    //return SPContext.Current.Web.CurrentUser.LoginName.ToString() + " - " + claimUser;
                    return "User doesn't have permission to validate";
                }
            }
            else
            {
                //return sStatus;
                return "UserName and Password Required";
            }
        }

        public bool CekNilai(string sUserName, string sPassword)
        {
            if (sUserName == string.Empty || sUserName == null)
            {
                return false;
            }

            if (sPassword == string.Empty || sPassword == null)
            {
                return false;
            }

            return true;
        }

        public bool IsSameGroupApproval(string sMakerUserName, string sCheckerUserName)
        {
            IList<UserRoleModel> userRoleMaker = null;
            IList<UserRoleModel> userRoleChecker = null;

            Repository.UserValidationServices UserValidation = new Repository.UserValidationServices();
            userRoleMaker = UserValidation.GetUserRoleModel(sMakerUserName);
            userRoleChecker = UserValidation.GetUserRoleModel(sCheckerUserName);
            if (userRoleMaker != null && userRoleChecker != null)
            {

                var data =
                            (from UM in userRoleMaker
                            join UC in userRoleChecker on UM.RoleName.Substring(0, UM.RoleName.Length - 5) equals
                                UC.RoleName.Substring(0, UC.RoleName.Length - 7)

                            select new UserRoleModel
                            {
                                EmployeeID = UM.EmployeeID,
                                EmployeeUsername = UM.EmployeeUsername,
                                RoleName = UC.RoleName
                            }).ToList();

                if (data != null || data.Count > 0)
                {
                    if (data.Select(x => x.EmployeeID).FirstOrDefault().ToString() != "0")
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }                    
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }      
        
        //Approval Group Management  

        public string UserUnlockReleaseCCU(UserData userData)
        {
            string claimUser = string.Format("i:0#.f|{0}|{1}", "DBSMembership", userData.UserID);
            if (CekNilai(userData.UserID, userData.Password))
            {
                if (IsGroupToRelease(claimUser))
                {
                    string sFullName = System.Web.Configuration.WebConfigurationManager.AppSettings["DomainName"].ToString() + @"\" + userData.UserID;
                    string sDomain = System.Web.Configuration.WebConfigurationManager.AppSettings["Domain"].ToString();
                    DirectoryEntry entry = new DirectoryEntry(sDomain, sFullName, userData.Password);
                    try
                    {
                        Object obj = entry.NativeObject;
                        DirectorySearcher search = new DirectorySearcher(entry);
                        search.Filter = "(SAMAccountName=" + sFullName + ")";
                        search.PropertiesToLoad.Add("cn");
                        SearchResult result = search.FindOne();
                        if (null == result)
                        {
                            //return sStatus;
                            return "Succeded";
                        }
                        else
                        {
                            //return sStatus;
                            return "Incorrect Username or Password";
                        }
                    }
                    catch (Exception ex)
                    {
                        //return sStatus;
                        return ex.Message;
                    }
                }
                else
                {
                    //return sStatus;
                    //return SPContext.Current.Web.CurrentUser.LoginName.ToString() + " - " + claimUser;
                    return "User doesn't have permission to validate";
                }
            }
            else
            {
                //return sStatus;
                return "UserName and Password Required";
            }
        }

        private bool IsGroupToRelease(string claimUser)
        {
            IList<UserRoleModelCCU> userRoleRelease = null;

            Repository.UserValidationServices UserValidation = new Repository.UserValidationServices();
            userRoleRelease = UserValidation.GetUserRoleReleaseModel(claimUser);
            if (userRoleRelease != null)
            {

                var data = (from UM in userRoleRelease
                            where UM.RoleName.Contains("CCU Unlock Release")//("Maker")
                             select new UserRoleModel
                             {
                                 EmployeeID = UM.EmployeeID,
                                 EmployeeUsername = UM.EmployeeUsername,
                                 RoleName = UM.RoleName
                             }).ToList();

                if (data != null || data.Count > 0)
                {
                    if (data.Select(x => x.EmployeeID).FirstOrDefault().ToString() != "0")
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
}
