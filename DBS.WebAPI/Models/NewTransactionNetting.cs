﻿using DBS.Entity;
using DBS.WebAPI.Models;
using DBS.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Transactions;
using System.Web;

namespace DBS.WebAPI.Models
{
    #region Property
    public class NettingTZReferenceModel
    {
        public long ID { get; set; }
        public string CIF { get; set; }
        public string Name { get; set; }
        public string TZReference { get; set; }
        public string MurexNumber { get; set; }
        public string SwapDealNumber { get; set; }
    }   
    public class NettingPurposeModel
    {
        public long? ID { get; set; }

        public string Name { get; set; }
    }
    public class TransactionNettingDraftModel
    {
        /// <summary>
        /// CIF
        /// </summary>
        public string CIF { get; set; }
        /// <summary>
        /// StatementLetter
        /// </summary>
        public StatementLetterModel StatementLetter { get; set; }

        /// <summary>
        /// TZReference
        /// </summary>
        public NettingTZReferenceModel TZReference { get; set; }

        /// <summary>
        /// Netting TZReference
        /// </summary>
        public NettingTZReferenceModel NettingTZReference { get; set; }

        /// <summary>
        /// Product
        /// </summary>
        public ProductModel Product { get; set; }

        /// <summary>
        /// Product Type
        /// </summary>
        public ProductTypeModel ProductType { get; set; }

        /// <summary>
        /// RateType
        /// </summary>
        public RateTypeModel RateType { get; set; }

        /// <summary>
        /// Rate
        /// </summary>
        public string Rate { get; set; }

        /// <summary>
        /// Workflow Instance ID
        /// </summary>
        public Guid? WorkflowInstanceID { get; set; }

        /// <summary>
        /// Transaction Deal ID.
        /// </summary>
        public long ID { get; set; }

        /// <summary>
        /// Application ID
        /// </summary>
        public string ApplicationID { get; set; }

        /// <summary>
        /// Customer Data Details
        /// </summary>
        public CustomerModel Customer { get; set; }

        /// <summary>
        /// Currency Details
        /// </summary>
        public CurrencyModel Currency { get; set; }

        /// <summary>
        /// Transaction Amount
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Eqv.USD
        /// </summary>
        public decimal AmountUSD { get; set; }

        /// <summary>
        /// Debit Acc Number
        /// </summary>
        public CustomerAccountModel Account { get; set; }

        /// <summary>
        /// Type
        /// </summary>
        public decimal? Type { get; set; }

        /// <summary>
        /// Create Date
        /// </summary>
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }

        /// <summary>
        /// Actual Submission Date Statement Letter
        /// </summary>
        public DateTime? ActualDateStatementLetter { get; set; }

        /// <summary>
        /// Actual Submission Date Underlying
        /// </summary>
        public DateTime? ActualDateUnderlying { get; set; }

        /// <summary>
        /// Actual Submission Date Statement Letter
        /// </summary>
        public DateTime? ExpectedDateStatementLetter { get; set; }


        /// <summary>
        /// Expected Submission Date Underlying
        /// </summary>
        public DateTime? ExpectedDateUnderlying { get; set; }

        /// <summary>
        /// List Transaction Document Details
        /// </summary>
        public IList<TransactionNettingDocumentDraftModel> Documents { get; set; }

        /// <summary>
        /// TZ Number
        /// </summary>
        public string TZNumber { get; set; }

        /// <summary>
        /// Is Draft
        /// </summary>
        public bool? IsDraft { get; set; }

        public bool? IsNettingTransaction { get; set; }
        public bool? IsTMO { get; set; }
        public long? NettingTransactionID { get; set; }
        public NettingPurposeModel NettingPurpose { get; set; }

        /// <summary>
        /// Remarks
        /// </summary>
        public string Remarks { get; set; }

        /// <summary>
        /// Underlyings
        /// </summary> 
        public IList<TransDetailUnderlyingModel> Underlyings { get; set; }

        /// <summary>
        /// Transaction Create By
        /// </summary> 
        public string CreateBy { get; set; }

        /// <summary>
        /// is book deal not new transaction
        /// </summary> 
        public bool IsBookDeal { get; set; }

        /// <summary>
        /// book underlying amount
        /// </summary>
        public decimal? bookunderlyingamount { get; set; }

        /// <summary>
        /// book underlying currency id
        /// </summary>
        public int? bookunderlyingcurrency { get; set; }

        /// <summary>
        /// book underlying id
        /// </summary>
        public long? bookunderlyingcode { get; set; }
        public string bookunderlyingdoccode { get; set; }
        public string bookunderlyingcurrencydesc { get; set; }
        public string bookunderlyingdesc { get; set; }
        public string AccountCurrencyCode { get; set; }
        public int RateTypeID { get; set; }
        public decimal? utilizationAmount { get; set; }
        public bool? IsFxTransaction { get; set; }
        public string OtherUnderlying { get; set; }
        public UnderlyingDocModel DealUnderlying { get; set; }

        public bool? IsOtherAccountNumber { get; set; }
        public string OtherAccountNumber { get; set; }
        public CurrencyModel DebitCurrency { get; set; }
        public string TransactionStatus { get; set; }
        public string TzStatus { get; set; }
        public IList<ReviseDealDocumentModel> ReviseDealDocuments { get; set; }
        public string MurexNumber { get; set; }
        public string SwapDealNumber { get; set; }
        public bool? IsJointAccount { get; set; }
        public bool? IsResident { get; set; }
        public int? ProductTypeID { get; set; }


    }
    public class TransactionNettingModel
    {
        /// <summary>
        /// CIF
        /// </summary>
        public string CIF { get; set; }
        /// <summary>
        /// StatementLetter
        /// </summary>
        public StatementLetterModel StatementLetter { get; set; }

        /// <summary>
        /// TZReference
        /// </summary>
        public NettingTZReferenceModel TZReference { get; set; }

        /// <summary>
        /// Netting TZReference
        /// </summary>
        public NettingTZReferenceModel NettingTZReference { get; set; }

        /// <summary>
        /// Product
        /// </summary>
        public ProductModel Product { get; set; }

        /// <summary>
        /// Product Type
        /// </summary>
        public ProductTypeModel ProductType { get; set; }

        /// <summary>
        /// RateType
        /// </summary>
        public RateTypeModel RateType { get; set; }

        /// <summary>
        /// Rate
        /// </summary>
        public string Rate { get; set; }

        /// <summary>
        /// Workflow Instance ID
        /// </summary>
        public Guid? WorkflowInstanceID { get; set; }

        /// <summary>
        /// Transaction Deal ID.
        /// </summary>
        public long ID { get; set; }

        /// <summary>
        /// Application ID
        /// </summary>
        public string ApplicationID { get; set; }

        /// <summary>
        /// Customer Data Details
        /// </summary>
        public CustomerModel Customer { get; set; }

        /// <summary>
        /// Currency Details
        /// </summary>
        public CurrencyModel Currency { get; set; }

        /// <summary>
        /// Transaction Amount
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Eqv.USD
        /// </summary>
        public decimal AmountUSD { get; set; }

        /// <summary>
        /// Debit Acc Number
        /// </summary>
        public CustomerAccountModel Account { get; set; }

        /// <summary>
        /// Type
        /// </summary>
        public decimal? Type { get; set; }

        /// <summary>
        /// Create Date
        /// </summary>
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }

        /// <summary>
        /// Actual Submission Date Statement Letter
        /// </summary>
        public DateTime? ActualDateStatementLetter { get; set; }

        /// <summary>
        /// Actual Submission Date Underlying
        /// </summary>
        public DateTime? ActualDateUnderlying { get; set; }

        /// <summary>
        /// Actual Submission Date Statement Letter
        /// </summary>
        public DateTime? ExpectedDateStatementLetter { get; set; }


        /// <summary>
        /// Expected Submission Date Underlying
        /// </summary>
        public DateTime? ExpectedDateUnderlying { get; set; }

        /// <summary>
        /// List Transaction Document Details
        /// </summary>
        public IList<TransactionNettingDocumentModel> Documents { get; set; }

        /// <summary>
        /// TZ Number
        /// </summary>
        public string TZNumber { get; set; }

        /// <summary>
        /// Is Draft
        /// </summary>
        public bool IsDraft { get; set; }

        public bool? IsNettingTransaction { get; set; }
        public bool? IsTMO { get; set; }
        public long? NettingTransactionID { get; set; }
        public NettingPurposeModel NettingPurpose { get; set; }

        /// <summary>
        /// Remarks
        /// </summary>
        public string Remarks { get; set; }

        /// <summary>
        /// Underlyings
        /// </summary> 
        public IList<TransDetailUnderlyingModel> Underlyings { get; set; }

        /// <summary>
        /// Transaction Create By
        /// </summary> 
        public string CreateBy { get; set; }

        /// <summary>
        /// is book deal not new transaction
        /// </summary> 
        public bool IsBookDeal { get; set; }

        /// <summary>
        /// book underlying amount
        /// </summary>
        public decimal? bookunderlyingamount { get; set; }

        /// <summary>
        /// book underlying currency id
        /// </summary>
        public int? bookunderlyingcurrency { get; set; }

        /// <summary>
        /// book underlying id
        /// </summary>
        public long? bookunderlyingcode { get; set; }
        public string bookunderlyingdoccode { get; set; }
        public string bookunderlyingcurrencydesc { get; set; }
        public string bookunderlyingdesc { get; set; }
        public string AccountCurrencyCode { get; set; }
        public int RateTypeID { get; set; }
        public decimal? utilizationAmount { get; set; }
        public bool? IsFxTransaction { get; set; }
        public string OtherUnderlying { get; set; }
        public UnderlyingDocModel DealUnderlying { get; set; }

        public bool? IsOtherAccountNumber { get; set; }
        public string OtherAccountNumber { get; set; }
        public CurrencyModel DebitCurrency { get; set; }
        public string TransactionStatus { get; set; }
        public string TzStatus { get; set; }
        public IList<ReviseDealDocumentModel> ReviseDealDocuments { get; set; }
        public string MurexNumber { get; set; }
        public string SwapDealNumber { get; set; }

    }
    public class TransactionNettingDocumentDraftModel
    {
        /// <summary>
        /// Transaction Document ID
        /// </summary>
        public long ID { get; set; }

        /// <summary>
        /// Document Type details
        /// </summary>
        public DocumentTypeModel Type { get; set; }

        /// <summary>
        /// Document Type details
        /// </summary>
        public DocumentPurposeModel Purpose { get; set; }

        /// <summary>
        /// Document filename
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// DocumentPath
        /// </summary>
        public string DocumentPath { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }

    }
    #endregion

    #region Interface
    public interface INewTransactionNettingRepository : IDisposable
    {
        bool AddTransactionNetting(TransactionNettingModel data, ref long transactionNettingID, ref string ApplicationID, ref string message);
        bool DeleteTransactionDraftByID(int id, ref string Message);
        bool GetTransactionDraftByID(long id, ref TransactionNettingDraftModel transaction, ref string message);

    }

    #endregion

    #region Repository

    public class NewTransactionNettingRepository : INewTransactionNettingRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool AddTransactionNetting(TransactionNettingModel data, ref long transactionNettingID, ref string ApplicationID, ref string message)
        {
            bool IsSuccess = false;
            long TransactionNettingID = 0;
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    if (data.IsDraft)
                    {
                        if (data.ID > 0)
                        {
                            #region resubmit draft
                            TransactionNettingDraft dataExist = context.TransactionNettingDrafts.Where(a => a.TransactionNettingID.Equals(data.ID)).SingleOrDefault();
                            if (dataExist != null)
                            {

                                dataExist.ApplicationID = data.ApplicationID;
                                dataExist.CIF = data.Customer.CIF;
                                dataExist.StatementLetterID = data.StatementLetter.ID;
                                dataExist.ProductID = data.Product.ID;
                                dataExist.ExpectedSubmissionDateSL = data.ExpectedDateStatementLetter;
                                dataExist.ExpectedSubmissionDateUnd = data.ExpectedDateUnderlying;
                                dataExist.ActualSubmissionDateSL = data.ActualDateStatementLetter;
                                dataExist.ActualSubmissionDateUnd = data.ActualDateUnderlying;
                                dataExist.TZRef = data.TZReference.TZReference;
                                dataExist.NettingPurposeID = data.NettingPurpose.ID;
                                dataExist.NettingTransactionID = data.NettingTZReference.ID;
                                dataExist.NettingDealNo = data.NettingTZReference.TZReference;
                                dataExist.MurexNumber = data.MurexNumber;
                                dataExist.SwapDealNumber = data.SwapDealNumber;
                                dataExist.UtilizedAmount = data.utilizationAmount;
                                dataExist.Remarks = data.Remarks;
                                dataExist.IsDraft = data.IsDraft;
                                dataExist.IsNettingTransaction = data.IsNettingTransaction;
                                dataExist.IsTMO = data.IsTMO;
                                dataExist.CreatedDate = DateTime.UtcNow;
                                dataExist.CreatedBy = currentUser.GetCurrentUser().DisplayName;

                                context.SaveChanges();

                                long transactionDealID = data.NettingTZReference.ID;
                                TransactionNettingID = dataExist.TransactionNettingID;
                                #region draft underlying
                                /*if (data.Underlyings.Count > 0)
                                {
                                    decimal availableAmount = data.AmountUSD;
                                    for (int i = 0; data.Underlyings.Count > i; i++)
                                    {
                                        long UnderlyingID = data.Underlyings[i].ID;
                                        Entity.CustomerUnderlying customerUnderlying = context.CustomerUnderlyings.Where(a => a.UnderlyingID.Equals(UnderlyingID)).SingleOrDefault();
                                        if (customerUnderlying != null)
                                        {
                                            decimal tempAvailableAmount = (decimal)customerUnderlying.AvailableAmountUSD;
                                            availableAmount = (decimal)customerUnderlying.AvailableAmountUSD - availableAmount;
                                            customerUnderlying.IsUtilize = true;
                                            customerUnderlying.UpdateDate = DateTime.UtcNow;
                                            customerUnderlying.AvailableAmountUSD = (decimal?)(availableAmount > 0 ? availableAmount : 0);
                                            customerUnderlying.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                                            context.SaveChanges();
                                            // set amount for transaction underlying
                                            data.Underlyings[i].USDAmount = tempAvailableAmount - (availableAmount > 0 ? availableAmount : 0);
                                            availableAmount = (availableAmount > 0 ? 0 : Math.Abs(availableAmount));
                                        }
                                    }
                                }
                                //add transaction underlying
                                if (data.Underlyings.Count > 0)
                                {
                                    List<long> UnderlyingID = new List<long>();
                                    foreach (TransDetailUnderlyingModel underlyingItem in data.Underlyings)
                                    {
                                        Entity.TransactionNettingUnderlyingDraft Underlying = new Entity.TransactionNettingUnderlyingDraft()
                                        {
                                            TransactionNettingID = TransactionNettingID,
                                            UnderlyingID = underlyingItem.ID,
                                            Amount = underlyingItem.USDAmount,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().DisplayName
                                        };
                                        context.TransactionNettingUnderlyingDrafts.Add(Underlying);
                                        UnderlyingID.Add(underlyingItem.ID);
                                    }
                                    context.SaveChanges();
                                    IList<TransactionNettingDocumentModel> documents = GetDocumentUnderlying(UnderlyingID);
                                    foreach (var item in documents)
                                    {
                                        data.Documents.Add(item);
                                    }
                                    IList<TransactionNettingDocumentModel> documentsBulk = GetDocumentBulkUnderlying(UnderlyingID);
                                    foreach (var item in documentsBulk)
                                    {
                                        data.Documents.Add(item);
                                    }
                                }*/
                                #endregion draft underlying
                                #region documents
                                /*if (data.Documents.Count > 0)
                                {
                                    var existingDocuments = (from a in context.TransactionNettingDocumentDrafts
                                                             where a.TransactionNettingID == TransactionNettingID
                                                                 && a.IsDeleted == false
                                                             select a);
                                    if (existingDocuments != null && existingDocuments.ToList().Count > 0)
                                    {
                                        foreach (var item in existingDocuments.ToList())
                                        {

                                            context.TransactionNettingDocumentDrafts.Remove(item);
                                            context.SaveChanges();
                                        }
                                        foreach (var item in data.Documents)
                                        {
                                            DBS.Entity.TransactionNettingDocumentDraft documents = new DBS.Entity.TransactionNettingDocumentDraft()
                                            {
                                                TransactionNettingID = TransactionNettingID,
                                                DocTypeID = item.Type.ID,
                                                PurposeID = item.Purpose.ID,
                                                Filename = item.FileName,
                                                IsDeleted = false,
                                                DocumentPath = item.DocumentPath,
                                                CreateDate = DateTime.UtcNow,
                                                CreateBy = currentUser.GetCurrentUser().LoginName

                                            };
                                            context.TransactionNettingDocumentDrafts.Add(documents);
                                            context.SaveChanges();
                                        }

                                    }
                                    else
                                    {
                                        foreach (var item in data.Documents)
                                        {
                                            DBS.Entity.TransactionNettingDocumentDraft documents = new DBS.Entity.TransactionNettingDocumentDraft()
                                            {
                                                TransactionNettingID = TransactionNettingID,
                                                DocTypeID = item.Type.ID,
                                                PurposeID = item.Purpose.ID,
                                                Filename = item.FileName,
                                                IsDeleted = false,
                                                DocumentPath = item.DocumentPath,
                                                CreateDate = DateTime.UtcNow,
                                                CreateBy = currentUser.GetCurrentUser().LoginName

                                            };
                                            context.TransactionNettingDocumentDrafts.Add(documents);
                                            context.SaveChanges();
                                        }
                                    }

                                }*/
                                #endregion

                            }

                        }
                            #endregion
                        else
                        {
                            #region draft netting transaction
                            using (DBSEntities context = new DBSEntities())
                            {

                                DBS.Entity.TransactionNettingDraft datanettingdraft = new DBS.Entity.TransactionNettingDraft()
                                {
                                    ApplicationID = data.ApplicationID,
                                    CIF = data.Customer.CIF,
                                    StatementLetterID = data.StatementLetter.ID,
                                    ProductID = data.Product.ID,
                                    ExpectedSubmissionDateSL = data.ExpectedDateStatementLetter,
                                    ExpectedSubmissionDateUnd = data.ExpectedDateUnderlying,
                                    ActualSubmissionDateSL = data.ActualDateStatementLetter,
                                    ActualSubmissionDateUnd = data.ActualDateUnderlying,
                                    TZRef = data.TZReference.TZReference,
                                    NettingPurposeID = data.NettingPurpose.ID,
                                    NettingTransactionID = data.NettingTZReference.ID,
                                    NettingDealNo = data.NettingTZReference.TZReference,
                                    MurexNumber = data.MurexNumber,
                                    SwapDealNumber = data.SwapDealNumber,
                                    UtilizedAmount = data.utilizationAmount,
                                    Remarks = data.Remarks,
                                    IsDraft = data.IsDraft,
                                    IsNettingTransaction = data.IsNettingTransaction,
                                    IsTMO = data.IsTMO,
                                    CreatedDate = DateTime.UtcNow,
                                    CreatedBy = currentUser.GetCurrentUser().DisplayName
                                };

                                context.TransactionNettingDrafts.Add(datanettingdraft);
                                context.SaveChanges();

                                long transactionDealID = data.NettingTZReference.ID;
                                TransactionNettingID = datanettingdraft.TransactionNettingID;

                                #region draft underlying
                                /*
                                if (data.Underlyings.Count > 0)
                                {
                                    decimal availableAmount = data.AmountUSD;
                                    for (int i = 0; data.Underlyings.Count > i; i++)
                                    {
                                        long UnderlyingID = data.Underlyings[i].ID;
                                        Entity.CustomerUnderlying customerUnderlying = context.CustomerUnderlyings.Where(a => a.UnderlyingID.Equals(UnderlyingID)).SingleOrDefault();
                                        if (customerUnderlying != null)
                                        {
                                            decimal tempAvailableAmount = (decimal)customerUnderlying.AvailableAmountUSD;
                                            availableAmount = (decimal)customerUnderlying.AvailableAmountUSD - availableAmount;
                                            customerUnderlying.IsUtilize = true;
                                            customerUnderlying.UpdateDate = DateTime.UtcNow;
                                            customerUnderlying.AvailableAmountUSD = (decimal?)(availableAmount > 0 ? availableAmount : 0);
                                            customerUnderlying.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                                            context.SaveChanges();
                                            // set amount for transaction underlying
                                            data.Underlyings[i].USDAmount = tempAvailableAmount - (availableAmount > 0 ? availableAmount : 0);
                                            availableAmount = (availableAmount > 0 ? 0 : Math.Abs(availableAmount));
                                        }
                                    }
                                }
                                //add transaction underlying
                                if (data.Underlyings.Count > 0)
                                {
                                    List<long> UnderlyingID = new List<long>();
                                    foreach (TransDetailUnderlyingModel underlyingItem in data.Underlyings)
                                    {
                                        Entity.TransactionNettingUnderlyingDraft Underlying = new Entity.TransactionNettingUnderlyingDraft()
                                        {
                                            TransactionNettingID = TransactionNettingID,
                                            UnderlyingID = underlyingItem.ID,
                                            Amount = underlyingItem.USDAmount,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().DisplayName
                                        };
                                        context.TransactionNettingUnderlyingDrafts.Add(Underlying);
                                        UnderlyingID.Add(underlyingItem.ID);
                                    }
                                    context.SaveChanges();
                                    IList<TransactionNettingDocumentModel> documents = GetDocumentUnderlying(UnderlyingID);
                                    foreach (var item in documents)
                                    {
                                        data.Documents.Add(item);
                                    }
                                    IList<TransactionNettingDocumentModel> documentsBulk = GetDocumentBulkUnderlying(UnderlyingID);
                                    foreach (var item in documentsBulk)
                                    {
                                        data.Documents.Add(item);
                                    }
                                }*/

                                #endregion draft underlying
                                #region documents
                                /*if (data.Documents.Count > 0)
                                {
                                    var existingDocuments = (from a in context.TransactionNettingDocumentDrafts
                                                             where a.TransactionNettingID == TransactionNettingID
                                                                 && a.IsDeleted == false
                                                             select a);
                                    if (existingDocuments != null && existingDocuments.ToList().Count > 0)
                                    {
                                        foreach (var item in existingDocuments.ToList())
                                        {

                                            context.TransactionNettingDocumentDrafts.Remove(item);
                                            context.SaveChanges();
                                        }
                                        foreach (var item in data.Documents)
                                        {
                                            DBS.Entity.TransactionNettingDocumentDraft documents = new DBS.Entity.TransactionNettingDocumentDraft()
                                            {
                                                TransactionNettingID = TransactionNettingID,
                                                DocTypeID = item.Type.ID,
                                                PurposeID = item.Purpose.ID,
                                                Filename = item.FileName,
                                                IsDeleted = false,
                                                DocumentPath = item.DocumentPath,
                                                CreateDate = DateTime.UtcNow,
                                                CreateBy = currentUser.GetCurrentUser().LoginName

                                            };
                                            context.TransactionNettingDocumentDrafts.Add(documents);
                                            context.SaveChanges();
                                        }

                                    }
                                    else
                                    {
                                        foreach (var item in data.Documents)
                                        {
                                            DBS.Entity.TransactionNettingDocumentDraft documents = new DBS.Entity.TransactionNettingDocumentDraft()
                                            {
                                                TransactionNettingID = TransactionNettingID,
                                                DocTypeID = item.Type.ID,
                                                PurposeID = item.Purpose.ID,
                                                Filename = item.FileName,
                                                IsDeleted = false,
                                                DocumentPath = item.DocumentPath,
                                                CreateDate = DateTime.UtcNow,
                                                CreateBy = currentUser.GetCurrentUser().LoginName

                                            };
                                            context.TransactionNettingDocumentDrafts.Add(documents);
                                            context.SaveChanges();
                                        }
                                    }

                                }*/
                                #endregion

                            }
                            #endregion
                        }

                    }
                    else
                    {
                        #region submit netting transaction
                        using (DBSEntities context = new DBSEntities())
                        {

                            DBS.Entity.TransactionNetting datanetting = new DBS.Entity.TransactionNetting()
                            {
                                ApplicationID = data.ApplicationID,
                                CIF = data.Customer.CIF,
                                StatementLetterID = data.StatementLetter.ID,
                                ProductID = data.Product.ID,
                                ExpectedSubmissionDateSL = data.ExpectedDateStatementLetter,
                                ExpectedSubmissionDateUnd = data.ExpectedDateUnderlying,
                                ActualSubmissionDateSL = data.ActualDateStatementLetter,
                                ActualSubmissionDateUnd = data.ActualDateUnderlying,
                                TZRef = data.TZReference.TZReference,
                                NettingPurposeID = data.NettingPurpose.ID,
                                NettingTransactionID = data.NettingTZReference.ID,
                                NettingDealNo = data.NettingTZReference.TZReference,
                                MurexNumber = data.MurexNumber,
                                SwapDealNumber = data.SwapDealNumber,
                                UtilizedAmount = data.utilizationAmount,
                                Remarks = data.Remarks,
                                IsDraft = data.IsDraft,
                                IsNettingTransaction = data.IsNettingTransaction,
                                IsTMO = data.IsTMO,
                                CreatedDate = DateTime.UtcNow,
                                CreatedBy = currentUser.GetCurrentUser().DisplayName
                            };

                            context.TransactionNettings.Add(datanetting);
                            context.SaveChanges();

                            long transactionDealID = data.NettingTZReference.ID;
                            TransactionNettingID = datanetting.TransactionNettingID;
                            #region underlyings                            
                            /*if (data.Underlyings.Count > 0)
                            {
                                decimal availableAmount = data.AmountUSD;
                                for (int i = 0; data.Underlyings.Count > i; i++)
                                {
                                    long UnderlyingID = data.Underlyings[i].ID;
                                    Entity.CustomerUnderlying customerUnderlying = context.CustomerUnderlyings.Where(a => a.UnderlyingID.Equals(UnderlyingID)).SingleOrDefault();
                                    if (customerUnderlying != null)
                                    {
                                        decimal tempAvailableAmount = (decimal)customerUnderlying.AvailableAmountUSD;
                                        availableAmount = (decimal)customerUnderlying.AvailableAmountUSD - availableAmount;
                                        customerUnderlying.IsUtilize = true;
                                        customerUnderlying.UpdateDate = DateTime.UtcNow;
                                        customerUnderlying.AvailableAmountUSD = (decimal?)(availableAmount > 0 ? availableAmount : 0);
                                        customerUnderlying.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                                        context.SaveChanges();
                                        // set amount for transaction underlying
                                        data.Underlyings[i].USDAmount = tempAvailableAmount - (availableAmount > 0 ? availableAmount : 0);
                                        availableAmount = (availableAmount > 0 ? 0 : Math.Abs(availableAmount));
                                    }
                                }
                            }
                            //add transaction underlying
                            if (data.Underlyings.Count > 0)
                            {
                                List<long> UnderlyingID = new List<long>();
                                foreach (TransDetailUnderlyingModel underlyingItem in data.Underlyings)
                                {
                                    Entity.TransactionNettingUnderlying Underlying = new Entity.TransactionNettingUnderlying()
                                    {
                                        TransactionNettingID = TransactionNettingID,
                                        UnderlyingID = underlyingItem.ID,
                                        Amount = underlyingItem.USDAmount,
                                        CreateDate = DateTime.UtcNow,
                                        CreateBy = currentUser.GetCurrentUser().DisplayName
                                    };
                                    context.TransactionNettingUnderlyings.Add(Underlying);
                                    UnderlyingID.Add(underlyingItem.ID);
                                }
                                context.SaveChanges();
                                IList<TransactionNettingDocumentModel> documents = GetDocumentUnderlying(UnderlyingID);
                                foreach (var item in documents)
                                {
                                    data.Documents.Add(item);
                                }
                                IList<TransactionNettingDocumentModel> documentsBulk = GetDocumentBulkUnderlying(UnderlyingID);
                                foreach (var item in documentsBulk)
                                {
                                    data.Documents.Add(item);
                                }
                            }*/
                            #endregion
                            #region documents
                            /*if (data.Documents.Count > 0)
                            {
                                var existingDocuments = (from a in context.TransactionNettingDocuments
                                                         where a.TransactionNettingID == TransactionNettingID
                                                             && a.IsDeleted == false
                                                         select a);
                                if (existingDocuments != null && existingDocuments.ToList().Count > 0)
                                {
                                    foreach (var item in existingDocuments.ToList())
                                    {

                                        context.TransactionNettingDocuments.Remove(item);
                                        context.SaveChanges();
                                    }
                                    foreach (var item in data.Documents)
                                    {
                                        DBS.Entity.TransactionNettingDocument documents = new DBS.Entity.TransactionNettingDocument()
                                        {
                                            TransactionNettingID = TransactionNettingID,
                                            DocTypeID = item.Type.ID,
                                            PurposeID = item.Purpose.ID,
                                            Filename = item.FileName,
                                            IsDeleted = false,
                                            DocumentPath = item.DocumentPath,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().LoginName

                                        };
                                        context.TransactionNettingDocuments.Add(documents);
                                        context.SaveChanges();
                                    }

                                }
                                else
                                {
                                    foreach (var item in data.Documents)
                                    {
                                        DBS.Entity.TransactionNettingDocument documents = new DBS.Entity.TransactionNettingDocument()
                                        {
                                            TransactionNettingID = TransactionNettingID,
                                            DocTypeID = item.Type.ID,
                                            PurposeID = item.Purpose.ID,
                                            Filename = item.FileName,
                                            IsDeleted = false,
                                            DocumentPath = item.DocumentPath,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().LoginName

                                        };
                                        context.TransactionNettingDocuments.Add(documents);
                                        context.SaveChanges();
                                    }
                                }

                            }*/
                            #endregion

                        }
                        #endregion
                    }
                    ts.Complete();
                    IsSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.InnerException.Message;
                    ts.Dispose();
                }

            }
            if (!data.IsDraft)
            {
                using (DBSEntities contextAppID = new DBSEntities())
                {
                    var LatestTransaction = (from a in contextAppID.TransactionNettings
                                             where a.TransactionNettingID == TransactionNettingID
                                             select a).SingleOrDefault();
                    ApplicationID = LatestTransaction.ApplicationID;
                }
                IsSuccess = true;
            }
            return IsSuccess;
        }
        public bool DeleteTransactionDraftByID(int id, ref string Message)
        {
            bool IsSuccess = false;
            using (DBSEntities context = new DBSEntities())
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    try
                    {
                        Entity.TransactionNettingDraft data = context.TransactionNettingDrafts.Where(a => a.TransactionNettingID.Equals(id)).SingleOrDefault();
                        if (data != null)
                        {
                            data.IsDeleted = true;
                            context.SaveChanges();

                            var existingDocs = context.TransactionNettingDocumentDrafts.Where(a => a.TransactionNettingID.Equals(id)).ToList();
                            if (existingDocs != null)
                            {
                                if (existingDocs.Count() > 0)
                                {
                                    foreach (var item in existingDocs)
                                    {
                                        var deleteDoc = context.TransactionNettingDocumentDrafts.Where(a => a.TransactionDocumentID.Equals(item.TransactionDocumentID)).SingleOrDefault();
                                        deleteDoc.IsDeleted = true;
                                        context.SaveChanges();
                                    }
                                }
                            }
                            context.SaveChanges();
                            IsSuccess = true;
                        }
                        else
                        {
                            Message = string.Format("Draft ID {0} is does not exist.", id);
                            IsSuccess = false;
                        }
                        ts.Complete();
                    }
                    catch (Exception ex)
                    {
                        ts.Dispose();
                        Message = ex.Message;
                    }
                }
            }
            return IsSuccess;
        }
        public bool GetTransactionDraftByID(long id, ref TransactionNettingDraftModel transaction, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                context.Transactions.AsNoTracking();

                TransactionNettingDraftModel datatransaction = (from a in context.TransactionNettingDrafts
                                                                where a.TransactionNettingID.Equals(id)
                                                                select new TransactionNettingDraftModel
                                                                {
                                                                    ID = a.TransactionNettingID,
                                                                    ApplicationID = a.ApplicationID,
                                                                    CIF = a.Customer.CIF,
                                                                    StatementLetter = new StatementLetterModel()
                                                                    {
                                                                        ID = a.StatementLetterID
                                                                    },
                                                                    Product = new ProductModel()
                                                                    {
                                                                        ID = a.ProductID
                                                                    },                                                                    
                                                                    ExpectedDateStatementLetter = a.ExpectedSubmissionDateSL,
                                                                    ExpectedDateUnderlying = a.ExpectedSubmissionDateUnd,
                                                                    ActualDateStatementLetter = a.ActualSubmissionDateSL,
                                                                    ActualDateUnderlying = a.ActualSubmissionDateUnd,
                                                                    TZReference = context.TransactionDeals.Where(x => x.TZRef.Equals(a.TZRef)).Select(x => new NettingTZReferenceModel()
                                                                    {
                                                                        ID = x.TransactionDealID,
                                                                        CIF = x.Customer.CIF,
                                                                        Name = x.Customer.CustomerName,
                                                                        TZReference = x.TZRef,
                                                                        MurexNumber = x.NB != null ? x.NB : "",
                                                                        SwapDealNumber = x.SwapTZDealNo != null ? x.SwapTZDealNo : ""
                                                                    }).FirstOrDefault(),
                                                                    NettingPurpose = new NettingPurposeModel()
                                                                    {
                                                                        ID = a.NettingPurposeID
                                                                    },
                                                                    NettingTransactionID = a.NettingTransactionID,
                                                                    NettingTZReference = context.TransactionDeals.Where(x => x.TZRef.Equals(a.NettingDealNo)).Select(x => new NettingTZReferenceModel()
                                                                    {
                                                                        ID = x.TransactionDealID,
                                                                        CIF = x.Customer.CIF,
                                                                        Name = x.Customer.CustomerName,
                                                                        TZReference = x.TZRef,
                                                                        MurexNumber = x.NB != null ? x.NB : "",
                                                                        SwapDealNumber = x.SwapTZDealNo != null ? x.SwapTZDealNo : ""
                                                                    }).FirstOrDefault(),
                                                                    MurexNumber = a.MurexNumber,
                                                                    SwapDealNumber = a.SwapDealNumber,
                                                                    utilizationAmount = a.UtilizedAmount,
                                                                    Remarks = a.Remarks,
                                                                    IsDraft = a.IsDraft,
                                                                    IsNettingTransaction = a.IsNettingTransaction,
                                                                    IsTMO = a.IsTMO,
                                                                    CreateDate = a.CreatedDate,
                                                                    CreateBy = a.CreatedBy
                                                                }).SingleOrDefault();

                // fill CIF Retail data
                if (datatransaction != null)
                {
                    string DFNumber = datatransaction.TZReference.TZReference;
                    string MurexNumber = datatransaction.MurexNumber;
                    long TransactionDealID = 0;
                    if (!string.IsNullOrEmpty(DFNumber))
                    {
                        TransactionDealID = context.TransactionDeals.Where(x => x.TZRef.Equals(DFNumber)).Select(x => x.TransactionDealID).FirstOrDefault();
                    }
                    else
                    {
                        TransactionDealID = context.TransactionDeals.Where(x => x.NB.Equals(MurexNumber)).Select(x => x.TransactionDealID).FirstOrDefault();
                    }
                    var deal = context.TransactionDeals.Where(a => a.TransactionDealID.Equals(TransactionDealID)).Select(a => a).FirstOrDefault();
                    string accountNumber = deal.AccountNumber;
                    long underlyingcode = Convert.ToInt32(datatransaction.bookunderlyingcode);

                    string cif = datatransaction.CIF;
                    
                    datatransaction.AccountCurrencyCode = datatransaction.Account != null ? datatransaction.Account.Currency.Code : "";
                    ICustomerRepository customerRepo = new CustomerRepository();
                    CustomerModel customer = new CustomerModel();
                    bool isUnderlying = context.TransactionDealUnderlyings.Any(x => x.TransactionDealID.Equals(TransactionDealID));                   

                    if (isUnderlying)
                    {
                        if (customerRepo.GetCustomerByTransactionDeal(TransactionDealID, ref customer, ref message))
                        {
                            datatransaction.Customer = customer;
                            datatransaction.Documents = context.TransactionDealDocuments.Where(x => x.TransactionDealID.Equals(TransactionDealID) && x.IsDeleted.Equals(false)).Select(x => new TransactionNettingDocumentDraftModel()
                                                {
                                                    ID = x.TransactionDocumentID,
                                                    Type = new DocumentTypeModel()
                                                    {
                                                        ID = x.DocumentType.DocTypeID,
                                                        Name = x.DocumentType.DocTypeName,
                                                        Description = x.DocumentType.DocTypeDescription,
                                                        LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                                        LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                                    },
                                                    Purpose = new DocumentPurposeModel()
                                                    {
                                                        ID = x.DocumentPurpose.PurposeID,
                                                        Name = x.DocumentPurpose.PurposeName,
                                                        Description = x.DocumentPurpose.PurposeDescription,
                                                        LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                                        LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                                    },
                                                    FileName = x.Filename,
                                                    DocumentPath = x.DocumentPath,
                                                    LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                                    LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                                                }).ToList();
                            datatransaction.Currency = context.TransactionDeals.Where(x => x.TransactionDealID.Equals(TransactionDealID)).Select(x => new CurrencyModel()
                                                {
                                                    ID = x.CurrencyID,
                                                    Code = context.Currencies.Where(y => y.CurrencyID.Equals(x.CurrencyID)).Select(y => y.CurrencyCode).FirstOrDefault()
                                                }).FirstOrDefault();
                            datatransaction.IsJointAccount = context.TransactionDeals.Where(x => x.TransactionDealID.Equals(TransactionDealID)).Select(x => x.IsJointAccount).FirstOrDefault();
                            datatransaction.IsResident = context.TransactionDeals.Where(x => x.TransactionDealID.Equals(TransactionDealID)).Select(x => x.IsResident).FirstOrDefault();
                            datatransaction.ProductTypeID = context.TransactionDeals.Where(x => x.TransactionDealID.Equals(TransactionDealID)).Select(x => x.ProductTypeID).FirstOrDefault();
                            datatransaction.ProductType = context.TransactionDeals.Where(x => x.TransactionDealID.Equals(TransactionDealID)).Select(x => new ProductTypeModel()
                            {
                                ID = x.ProductTypeID,
                                Code = context.ProductTypes.Where(y => y.ProductTypeID.Equals(x.ProductTypeID)).Select(y => y.ProductTypeCode).FirstOrDefault()
                            }).FirstOrDefault();
                        }
                    }
                    else
                    {
                        if (customerRepo.GetCustomerByCIF(cif, ref customer, ref message))
                        {
                            datatransaction.Customer = customer;
                            datatransaction.Customer.Underlyings = null;
                            datatransaction.Documents = context.TransactionDealDocuments.Where(x => x.TransactionDealID.Equals(TransactionDealID) && x.IsDeleted.Equals(false)).Select(x => new TransactionNettingDocumentDraftModel()
                                                    {
                                                        ID = x.TransactionDocumentID,
                                                        Type = new DocumentTypeModel()
                                                        {
                                                            ID = x.DocumentType.DocTypeID,
                                                            Name = x.DocumentType.DocTypeName,
                                                            Description = x.DocumentType.DocTypeDescription,
                                                            LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                                            LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                                        },
                                                        Purpose = new DocumentPurposeModel()
                                                        {
                                                            ID = x.DocumentPurpose.PurposeID,
                                                            Name = x.DocumentPurpose.PurposeName,
                                                            Description = x.DocumentPurpose.PurposeDescription,
                                                            LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                                            LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                                        },
                                                        FileName = x.Filename,
                                                        DocumentPath = x.DocumentPath,
                                                        LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                                        LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                                                    }).ToList();
                            datatransaction.Currency = context.TransactionDeals.Where(x => x.TransactionDealID.Equals(TransactionDealID)).Select(x => new CurrencyModel()
                            {
                                ID = x.CurrencyID,
                                Code = context.Currencies.Where(y => y.CurrencyID.Equals(x.CurrencyID)).Select(y => y.CurrencyCode).FirstOrDefault()
                            }).FirstOrDefault();
                            datatransaction.IsJointAccount = context.TransactionDeals.Where(x => x.TransactionDealID.Equals(TransactionDealID)).Select(x => x.IsJointAccount).FirstOrDefault();
                            datatransaction.IsResident = context.TransactionDeals.Where(x => x.TransactionDealID.Equals(TransactionDealID)).Select(x => x.IsResident).FirstOrDefault();
                            datatransaction.ProductTypeID = context.TransactionDeals.Where(x => x.TransactionDealID.Equals(TransactionDealID)).Select(x => x.ProductTypeID).FirstOrDefault();
                            datatransaction.ProductType = context.TransactionDeals.Where(x => x.TransactionDealID.Equals(TransactionDealID)).Select(x => new ProductTypeModel()
                            {
                                ID = x.ProductTypeID,
                                Code = context.ProductTypes.Where(y => y.ProductTypeID.Equals(x.ProductTypeID)).Select(y => y.ProductTypeCode).FirstOrDefault()
                            }).FirstOrDefault();
                        }

                    }

                    if (deal.IsOtherAccountNumber != null && deal.IsOtherAccountNumber == true)
                    {
                        CustomerAccountModel Account = new CustomerAccountModel();
                        datatransaction.Account = Account;
                        datatransaction.OtherAccountNumber = deal.OtherAccountNumber;
                    }
                    else
                    {
                        datatransaction.Account = (from j in context.SP_GETJointAccount(cif)
                                               where j.AccountNumber.Equals(accountNumber)
                                               select new CustomerAccountModel
                                               {
                                                   AccountNumber = j.AccountNumber,
                                                   Currency = new CurrencyModel
                                                   {
                                                       ID = j.CurrencyID.Value,
                                                       Code = j.CurrencyCode,
                                                       Description = j.CurrencyDescription
                                                   },
                                                   CustomerName = j.CustomerName,
                                                   IsJointAccount = j.IsJointAccount
                                               }).FirstOrDefault();
                        

                    }

                    CurrencyModel AccountCurrency = new CurrencyModel();
                    AccountCurrency.ID = deal.DebitCurrency.CurrencyID;
                    AccountCurrency.Code = deal.DebitCurrency.CurrencyCode;
                    AccountCurrency.Description = deal.DebitCurrency.CurrencyDescription;

                    datatransaction.DebitCurrency = AccountCurrency;
                    datatransaction.AccountCurrencyCode = AccountCurrency.Code;

                    customerRepo.Dispose();
                    customer = null;

                    transaction = datatransaction;
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }

            return IsSuccess;
        }
        public IList<TransactionNettingDocumentModel> GetDocumentUnderlying(List<long> IDUnderlying)
        {
            try
            {
                IList<TransactionNettingDocumentModel> documents = new List<TransactionNettingDocumentModel>();

                documents = (from a in context.CustomerUnderlyingMappings.Where(x => x.IsDeleted.Equals(false))
                             join b in context.CustomerUnderlyingFiles on a.UnderlyingFileID equals b.UnderlyingFileID
                             where (from x in IDUnderlying select x).Contains(a.UnderlyingID)
                             select new TransactionNettingDocumentModel
                             {
                                 ID = 0,
                                 FileName = b.FileName,
                                 Type = new DocumentTypeModel { ID = b.DocTypeID },
                                 Purpose = new DocumentPurposeModel { ID = b.PurposeID },
                                 DocumentPath = b.DocumentPath,
                             }).ToList();

                return documents;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<TransactionNettingDocumentModel> GetDocumentBulkUnderlying(List<long> IDUnderlyings)
        {

            try
            {
                IList<TransactionNettingDocumentModel> documents = new List<TransactionNettingDocumentModel>();
                List<long> childs = (from a in context.MappingBulkUnderlyings where IDUnderlyings.Contains(a.ParentID) select a.UnderlyingID).ToList();

                if (childs != null)
                {
                    documents = (from a in context.CustomerUnderlyingMappings.Where(x => x.IsDeleted.Equals(false))
                                 join b in context.CustomerUnderlyingFiles on a.UnderlyingFileID equals b.UnderlyingFileID
                                 where (from x in childs select x).Contains(a.UnderlyingID) && a.IsDeleted.Equals(false)
                                 select new TransactionNettingDocumentModel
                                 {
                                     ID = 0,
                                     FileName = b.FileName,
                                     Type = new DocumentTypeModel { ID = b.DocTypeID },
                                     Purpose = new DocumentPurposeModel { ID = b.PurposeID },
                                     DocumentPath = b.DocumentPath,
                                 }).ToList();

                }
                return documents;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);

            GC.Collect();
            GC.SuppressFinalize(this);
        }
    }
    #endregion


}