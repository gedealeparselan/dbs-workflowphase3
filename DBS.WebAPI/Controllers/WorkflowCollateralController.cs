﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class WorkflowCollateralController : ApiController
    {
        private string message = string.Empty;
        /// <summary>
        /// Define repo here
        /// </summary>
        private readonly IWorkflowCollateralRepository repoCollateral = null;
        public WorkflowCollateralController()
        {
            this.repoCollateral = new WorkflowCollateralRepository();
        }
        /// <summary>
        /// Add Controller below...
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (repoCollateral != null)
                repoCollateral.Dispose();

            base.Dispose(disposing);
        }
    }
}
