﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Models
{
    #region Property
    [ModelName("LLD")]
    public class LLDModel
    {
        /// <summary>
        /// LLD ID.
        /// </summary>
        public int? ID { get; set; }

        /// <summary>
        /// LLD Code.
        /// </summary>
        [MaxLength(25, ErrorMessage = "LLD Code is must be in {1} characters.")]
        public string Code { get; set; }
        
        /// <summary>
        /// LLD Description.
        /// </summary>
        [MaxLength(255, ErrorMessage = "Maximum length is {1} characters.")]
        public string Description { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }
        public bool? IsRTGS { get; set; }
        public bool? IsSKN { get; set; }
        public bool? IsOTT { get; set; } 
        public bool? IsOVB { get; set; }
        
        public bool IsDelete { get; set; }
        
    }

    public class LLDCheckerModel
    {
        public int LLDID { get; set; }
        public string LLDCode { get; set; }
        public string LLDDescription { get; set; }
    }

    public class LLDRow : LLDModel
    {
        public int RowID { get; set; }

    }

    public class LLDGrid : Grid
    {
        public IList<LLDRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class LLDFilter : Filter { }
    #endregion

    #region Interface
    public interface ILLDRepository : IDisposable
    {
        bool GetLLDByID(int id, ref LLDModel LLD, ref string message);
        bool GetLLD(ref IList<LLDModel> currencies, int limit, int index, ref string message);
        bool GetLLD(int page, int size, IList<LLDFilter> filters, string sortColumn, string sortOrder, ref LLDGrid LLD, ref string message);
        bool GetLLD(LLDFilter filter, ref IList<LLDModel> currencies, ref string message);
        bool GetLLD(ref IList<LLDModel> LLD, ref string message);
        bool GetLLDbyProduct(ref IList<LLDModel> LLD, int id, string name, ref string message); 
        bool GetLLD(string key, int limit, ref IList<LLDModel> currencies, ref string message);
        bool AddLLD(LLDModel LLD, ref string message);
        bool UpdateLLD(int id, LLDModel LLD, ref string message);
        bool DeleteLLD(int id, ref string message);
    }
    #endregion

    #region Repository
    public class LLDRepository : ILLDRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetLLDByID(int id, ref LLDModel LLD, ref string message)
        {
            bool isSuccess = false;

            try
            {
                LLD = (from a in context.LLDs
                        where a.IsDeleted.Equals(false) && a.LLDID.Equals(id)
                        select new LLDModel
                        {
                            ID = a.LLDID,
                            Code = a.LLDCode,
                            Description = a.LLDDescription,
                            LastModifiedDate = a.CreateDate,
                            LastModifiedBy = a.CreateBy,
                            IsRTGS = a.IsRTGS,
                            IsSKN = a.IsSKN,
                            IsOTT = a.IsOTT,
                            IsOVB = a.IsOVB,
                        }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetLLD(ref IList<LLDModel> LLD, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    LLD = (from a in context.LLDs
                            where a.IsDeleted.Equals(false)
                            orderby new { a.LLDCode, a.LLDDescription }
                            select new LLDModel
                            {
                                ID = a.LLDID,
                                Code = a.LLDCode,
                                Description = a.LLDDescription,
                                LastModifiedBy = a.CreateBy,
                                LastModifiedDate = a.CreateDate,
                                IsRTGS = a.IsRTGS,
                                IsSKN = a.IsSKN,
                                IsOTT = a.IsOTT,
                                IsOVB = a.IsOVB,
                            }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetLLD(ref IList<LLDModel> LLD, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    LLD = (from a in context.LLDs
                            where a.IsDeleted.Equals(false)
                            orderby new { a.LLDCode, a.LLDDescription }
                            select new LLDModel
                            {
                                ID = a.LLDID,
                                Code = a.LLDCode,
                                Description = a.LLDDescription,
                                LastModifiedBy = a.CreateBy,
                                LastModifiedDate = a.CreateDate,
                                IsRTGS = a.IsRTGS,
                                IsSKN = a.IsSKN,
                                IsOTT = a.IsOTT,
                                IsOVB = a.IsOVB,
                            }).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetLLD(int page, int size, IList<LLDFilter> filters, string sortColumn, string sortOrder, ref LLDGrid customer, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                LLDModel filter = new LLDModel();
                if (filters != null)
                {
                    filter.Code = (string)filters.Where(a => a.Field.Equals("Code")).Select(a => a.Value).SingleOrDefault();
                    filter.Description = (string)filters.Where(a => a.Field.Equals("Description")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.LLDs
                                let isCode = !string.IsNullOrEmpty(filter.Code)
                                let isDesc = !string.IsNullOrEmpty(filter.Description)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) &&
                                    (isCode ? a.LLDCode.Contains(filter.Code) : true) &&
                                    (isDesc ? a.LLDDescription.Contains(filter.Description) : true) &&
                                    //(a.IsRTGS != null) && (a.IsSKN != null) && (a.IsOTT != null) && (a.IsOVB != null) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new LLDModel
                                {
                                    ID = a.LLDID,
                                    Code = a.LLDCode,
                                    Description = a.LLDDescription,
                                    IsRTGS = a.IsRTGS ?? false,
                                    IsSKN = a.IsSKN ?? false ,
                                    IsOTT = a.IsOTT ?? false ,
                                    IsOVB = a.IsOVB ?? false ,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    customer.Page = page;
                    customer.Size = size;
                    customer.Total = data.Count();
                    customer.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    customer.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new LLDRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            Code = a.Code,
                            Description = a.Description,
                            IsRTGS = a.IsRTGS,
                            IsSKN = a.IsSKN,
                            IsOTT = a.IsOTT,
                            IsOVB = a.IsOVB,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetLLD(LLDFilter filter, ref IList<LLDModel> customer, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetLLD(string key, int limit, ref IList<LLDModel> LLD, ref string message)
        {
            bool isSuccess = false;

            try
            {
                LLD = (from a in context.LLDs
                        where a.IsDeleted.Equals(false) &&
                            a.LLDCode.Contains(key) || a.LLDDescription.Contains(key)
                        orderby new { a.LLDCode, a.LLDDescription }
                        select new LLDModel
                        {
                            ID = a.LLDID,
                            Code = a.LLDCode,
                            Description = a.LLDDescription,
                            IsRTGS = a.IsRTGS,
                            IsSKN = a.IsSKN,
                            IsOTT = a.IsOTT,
                            IsOVB = a.IsOVB,
                            LastModifiedDate = a.UpdateDate,
                            LastModifiedBy = a.UpdateBy
                        }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddLLD(LLDModel LLD, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (!context.LLDs.Where(a => a.LLDCode.Equals(LLD.Code) && a.IsDeleted.Equals(false)).Any())
                {
                    context.LLDs.Add(new Entity.LLD()
                    {
                        LLDCode = LLD.Code,
                        LLDDescription = LLD.Description,
                        IsDeleted = false,
                        IsRTGS = LLD.IsRTGS,
                        IsSKN = LLD.IsSKN,
                        IsOTT = LLD.IsOTT,
                        IsOVB = LLD.IsOVB,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    });

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("LLD data for LLD code {0} is already exist.", LLD.Code);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateLLD(int id, LLDModel LLD, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.LLD data = context.LLDs.Where(a => a.LLDID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    if (context.LLDs.Where(a => a.LLDID != LLD.ID && a.LLDCode.Equals(LLD.Code) && a.IsDeleted.Equals(false)).Count() >= 1)//a => a.LLDID != LLD.ID &&
                    {
                        message = string.Format("LLD Code {0} is exist.", LLD.Code);
                    }
                    else
                    {
                        data.LLDCode = LLD.Code;
                        data.LLDDescription = LLD.Description;
                        data.IsRTGS = LLD.IsRTGS;
                        data.IsSKN = LLD.IsSKN;
                        data.IsOTT = LLD.IsOTT;
                        data.IsOVB = LLD.IsOVB;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        context.SaveChanges();

                        isSuccess = true;
                    }
                }
                else
                {
                    message = string.Format("LLD data for LLD ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeleteLLD(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.LLD data = context.LLDs.Where(a => a.LLDID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("LLD data for LLD ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public bool GetLLDbyProduct(ref IList<LLDModel> LLD, int id, string name, ref string message) 
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    LLD = (from a in context.LLDs
                           where a.IsDeleted.Equals(false)
                           orderby new { a.LLDCode, a.LLDDescription }
                           select new LLDModel
                           {
                               ID = a.LLDID,
                               Code = a.LLDCode,
                               Description = a.LLDDescription,
                               LastModifiedBy = a.CreateBy,
                               LastModifiedDate = a.CreateDate,
                               IsSKN = a.IsSKN.Value,
                               IsRTGS = a.IsRTGS.Value,
                               IsOTT = a.IsOTT.Value,
                               IsOVB = a.IsOVB.Value,
                               IsDelete = a.IsDeleted
                           }).ToList();
                }

                if (name == "OTT")
                {
                    LLD = LLD.Where(a => a.IsOTT == true).ToList();
                }
                else if (name == "SKN")
                {
                    LLD = LLD.Where(a => a.IsSKN == true).ToList();
                }
                else if (name == "RTGS")
                {
                    LLD = LLD.Where(a => a.IsRTGS == true).ToList();
                }
                else if (name == "OVB")
                {
                    LLD = LLD.Where(a => a.IsOVB == true).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
    }
    #endregion
}