﻿using DBS.Entity;
using DBS.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;

namespace DBS.WebAPI.Models
{
   
        #region Class Property
        public class TransactionCallbackDraftModel
        {
            public long ApproverID { get; set; }
            public string CreateBy { get; set; }
            public DateTime CreateDate { get; set; }
            public bool? IsUTC { get; set; }
            public int? ReasonID { get; set; }
            public string Remark { get; set; }
            public DateTime Time { get; set; }
            public long TransactionCallbackID { get; set; }
            public long TransactionID { get; set; }
            public string UpdateBy { get; set; }
            public DateTime? UpdateDate { get; set; }           
            public CustomerContactModel Contact { get; set; }
        }       
        #endregion

        #region Interface
        public interface ICallbackDraftRepository : IDisposable
        {
            bool AddTransactionCallbackDraft(IList<TransactionCallbackDraftModel> data, ref string message);
            bool GetTransactionCallbackDraft(long id, ref IList<TransactionCallbackDraftModel> data, ref string message);
        }         
        #endregion

        #region Repository
        
        #endregion
        public class TransactionCallbackDraft : ICallbackDraftRepository
        {
            private DBSEntities context = new DBSEntities();
            private CurrentUser currentUser = new CurrentUser();

            private bool disposed = false;
            protected virtual void Dispose(bool disposing)
            {
                if (!this.disposed)
                {
                    if (disposing)
                    {
                        if (context != null)
                        {
                            context.Dispose();
                            currentUser = null;
                        }
                    }
                }
                this.disposed = true;
            }
            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
            public bool AddTransactionCallbackDraft(IList<TransactionCallbackDraftModel> data, ref string message)
            {
                bool IsSuccess = false;
                using (TransactionScope ts = new TransactionScope())
                {
                    try
                    {
                        if (data.Count > 0)
                        {
                            foreach(var item in data) {
                                DBS.Entity.TransactionCallbackDraft dataCallback = new DBS.Entity.TransactionCallbackDraft();
                                dataCallback.ApproverID = item.ApproverID;
                                dataCallback.TransactionID = item.TransactionID;
                                dataCallback.IsUTC = item.IsUTC;
                                dataCallback.ReasonID = item.ReasonID;
                                dataCallback.Remark = item.Remark;
                                dataCallback.Time = item.Time.ToUniversalTime();
                                dataCallback.ContactID = item.Contact != null ? item.Contact.ID : (long?)null;
                                dataCallback.IsDeleted = false;
                                dataCallback.CreateBy = currentUser.GetCurrentUser().LoginName;
                                dataCallback.CreateDate = DateTime.UtcNow;

                                context.TransactionCallbackDrafts.Add(dataCallback);
                                context.SaveChanges();
                            }
                            
                        }                      

                        ts.Complete();
                        IsSuccess = true;
                    }                       
                    catch (Exception ex)
                    {
                        message = ex.Message;
                        ts.Dispose();
                    }
                }
                return IsSuccess;
            }
            public bool GetTransactionCallbackDraft(long id, ref IList<TransactionCallbackDraftModel> data, ref string message)
            {
                bool IsSuccess = false;
                try
                {
                    context.Transactions.AsNoTracking();
                    if (id > 0)
                    {
                        IList<TransactionCallbackDraftModel> dataCallback = (from a in context.TransactionCallbackDrafts
                                                                             where a.TransactionID == id
                                                                             && (a.IsDeleted == null || a.IsDeleted == false)
                                                                             select new TransactionCallbackDraftModel
                                                                             {
                                                                                 ApproverID = a.ApproverID ?? 0,
                                                                                 CreateBy = a.CreateBy,
                                                                                 CreateDate = a.CreateDate ?? DateTime.Now,
                                                                                 IsUTC = a.IsUTC,
                                                                                 ReasonID = a.ReasonID ?? 0,
                                                                                 Remark = a.Remark,
                                                                                 Time = a.Time ?? DateTime.Now,
                                                                                 TransactionCallbackID = a.TransactionCallbackID,
                                                                                 TransactionID = a.TransactionID ?? 0,
                                                                                 Contact = (from b in context.CustomerContacts
                                                                                  where b.ContactID == (a.ContactID ?? 0)
                                                                                  select new CustomerContactModel
                                                                                  {
                                                                                      ID = b.ContactID,
                                                                                      Name = b.ContactName,
                                                                                      PhoneNumber = b.PhoneNumber,
                                                                                      CIF = b.CIF
                                                                                  }).FirstOrDefault() 
                                                                             }).ToList();
                        if (dataCallback != null && dataCallback.Count > 0)
                        {   
                            data = dataCallback;
                            IsSuccess = true;
                        }
                    }
                    IsSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.InnerException.Message;
                }
                return IsSuccess;
            }
        }       

}