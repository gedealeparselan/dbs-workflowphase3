﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Models
{
    #region Property

    [ModelName("RoleDraftMenuDetail")]
    public class RoleDraftMenuDetailModel
    {
        /// <summary>
        /// Menu ID.
        /// </summary>
        public string ID { get; set; }
    }

    [ModelName("RoleDraft")]
    public class RoleDraftModel
    {
        /// <summary>
        /// Role ID.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Role ID.
        /// </summary>
        public int MasterID { get; set; }

        /// <summary>
        /// Roles Name.
        /// </summary>
        [MaxLength(50, ErrorMessage = "Role Name is must be in {1} characters.")]
        public string Name { get; set; }

        /// <summary>
        /// Role Description.
        /// </summary>
        [MaxLength(255, ErrorMessage = "Maximum length is {1} characters.")]
        public string Description { get; set; }

        /// <summary>
        /// Action Type.
        public string ActionType { get; set; }

        /// <summary>
        /// Role Menu Detail.
        /// </summary>
        public IEnumerable<RoleDraftMenuDetailModel> Menus { get; set; }
        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }
    }

    public class RoleDraftRow : RoleDraftModel
    {
        public int RowID { get; set; }

    }

    public class RoleDraftGrid : Grid
    {
        public IList<RoleDraftRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class RoleDraftFilter : Filter { }
    #endregion

    #region Interface
    public interface IRoleDraftRepository : IDisposable
    {
        bool GetRoleByID(string id, ref RoleDraftModel role, ref string message);
        bool GetRole(ref IList<RoleDraftModel> roles, int limit, int index, ref string message);
        bool GetRole(ref IList<RoleDraftModel> roles, ref string message);
        bool GetRole(int page, int size, IList<RoleDraftFilter> filters, string sortColumn, string sortOrder, ref RoleDraftGrid roleGrid, ref string message);
        bool GetRole(RoleDraftFilter filter, ref IList<RoleDraftModel> roles, ref string message);
        bool GetRole(string key, int limit, ref IList<RoleDraftModel> roles, ref string message);
        bool AddRole(RoleDraftModel role, ref long roleid, ref string message);
        bool UpdateRole(RoleDraftModel role, ref long roleid, ref string message);
        bool UpdateRole(int id, RoleDraftModel role, ref string message);
        bool DeleteRole(int id, ref string message);
    }
    #endregion

    #region Repository
    public class RoleDraftRepository : IRoleDraftRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        /*public bool GetRoleByID(string id, ref RoleDraftModel role, ref string message)
        {
            bool isSuccess = false;

            try
            {
                role = (from a in context.RoleDrafts
                        where a.IsDeleted.Equals(false) && a.RoleID.Equals(id)
                        select new RoleDraftModel
                        {
                            ID = a.RoleID,
                            Name = a.RoleName,
                            Description = a.RoleDescription,
                            ActionType = a.ActionType,
                            LastModifiedDate = a.CreateDate,
                            LastModifiedBy = a.CreateBy
                        }).SingleOrDefault();

                int roleID = role.ID;

                var details = from a in context.RoleMenuMappingDrafts
                              where a.RoleID == roleID
                              select a;

                var items = new List<RoleDraftMenuDetailModel>();
                foreach (var item in details)
                {
                    items.Add(new RoleDraftMenuDetailModel()
                    {
                        ID = item.MenuID.ToString()
                    });
                }
                role.Menus = items;

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }*/

        public bool GetRoleByID(string id, ref RoleDraftModel role, ref string message)
        {
            Guid gid = Guid.Empty;
            bool isWorkflow = Guid.TryParse(id, out gid);

            bool isSuccess = false;

            try
            {
                if (isWorkflow)
                {
                    role = (from a in context.RoleDrafts
                            where a.IsDeleted.Equals(false) && a.WorkflowInstanceID.Value == gid
                            select new RoleDraftModel
                            {
                                ID = a.RoleID,
                                Name = a.RoleName,
                                Description = a.RoleDescription,
                                ActionType = a.ActionType,
                                LastModifiedDate = a.CreateDate,
                                LastModifiedBy = a.CreateBy
                            }).SingleOrDefault();
                    if (role != null)
                    {
                        int roleID = role.ID;

                        var details = from a in context.RoleMenuMappingDrafts
                                      where a.RoleID == roleID
                                      select a;

                        var items = new List<RoleDraftMenuDetailModel>();
                        foreach (var item in details)
                        {
                            items.Add(new RoleDraftMenuDetailModel()
                            {
                                ID = item.MenuID.ToString()
                            });
                        }
                        role.Menus = items;
                    }
                }
                else
                {
                    int iid = 0;
                    int.TryParse(id, out iid);

                    role = (from a in context.RoleDrafts
                            where a.IsDeleted.Equals(false) && a.RoleID == iid
                            select new RoleDraftModel
                            {
                                ID = a.RoleID,
                                Name = a.RoleName,
                                Description = a.RoleDescription,
                                ActionType = a.ActionType,
                                LastModifiedDate = a.CreateDate,
                                LastModifiedBy = a.CreateBy
                            }).SingleOrDefault();
                    if (role != null)
                    {
                        int roleID = role.ID;

                        var details = from a in context.RoleMenuMappingDrafts
                                      where a.RoleID == roleID
                                      select a;

                        var items = new List<RoleDraftMenuDetailModel>();
                        foreach (var item in details)
                        {
                            items.Add(new RoleDraftMenuDetailModel()
                            {
                                ID = item.MenuID.ToString()
                            });
                        }
                        role.Menus = items;
                    }
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message + " gid : " + gid.ToString();
            }

            return isSuccess;
        }

        public bool GetRole(ref IList<RoleDraftModel> roles, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    roles = (from a in context.RoleDrafts
                             where a.IsDeleted.Equals(false)
                             orderby new { a.RoleName, a.RoleDescription }
                             select new RoleDraftModel
                             {
                                 ID = a.RoleID,
                                 MasterID = a.MasterRoleID == null ? 0 : (int)a.MasterRoleID,
                                 Name = a.RoleName,
                                 Description = a.RoleDescription,
                                 ActionType = a.ActionType,
                                 LastModifiedBy = a.CreateBy,
                                 LastModifiedDate = a.CreateDate
                             }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetRole(ref IList<RoleDraftModel> roles, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    roles = (from a in context.RoleDrafts
                             where a.IsDeleted.Equals(false)
                             orderby new { a.RoleName, a.RoleDescription }
                             select new RoleDraftModel
                             {
                                 ID = a.RoleID,
                                 MasterID = a.MasterRoleID == null ? 0 : (int)a.MasterRoleID,
                                 Name = a.RoleName,
                                 Description = a.RoleDescription,
                                 ActionType = a.ActionType,
                                 LastModifiedBy = a.CreateBy,
                                 LastModifiedDate = a.CreateDate
                             }).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetRole(int page, int size, IList<RoleDraftFilter> filters, string sortColumn, string sortOrder, ref RoleDraftGrid roleGrid, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                RoleDraftModel filter = new RoleDraftModel();
                if (filters != null)
                {
                    filter.Name = (string)filters.Where(a => a.Field.Equals("Name")).Select(a => a.Value).SingleOrDefault();
                    filter.Description = (string)filters.Where(a => a.Field.Equals("Description")).Select(a => a.Value).SingleOrDefault();
                    filter.ActionType = (string)filters.Where(a => a.Field.Equals("Action")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.RoleDrafts
                                let isRoleName = !string.IsNullOrEmpty(filter.Name)
                                let isRoleDescription = !string.IsNullOrEmpty(filter.Description)
                                let isRoleAction = !string.IsNullOrEmpty(filter.ActionType)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) &&
                                    (isRoleName ? a.RoleName.Contains(filter.Name) : true) &&
                                    (isRoleDescription ? a.RoleDescription.Contains(filter.Description) : true) &&
                                    (isRoleAction ? a.ActionType.Contains(filter.ActionType) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new RoleDraftModel
                                {
                                    ID = a.RoleID,
                                    MasterID = a.MasterRoleID == null ? 0 : (int)a.MasterRoleID,
                                    Name = a.RoleName,
                                    Description = a.RoleDescription,
                                    ActionType = a.ActionType,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    roleGrid.Page = page;
                    roleGrid.Size = size;
                    roleGrid.Total = data.Count();
                    roleGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    roleGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new RoleDraftRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            Name = a.Name,
                            Description = a.Description,
                            ActionType = a.ActionType,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetRole(RoleDraftFilter filter, ref IList<RoleDraftModel> roles, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetRole(string key, int limit, ref IList<RoleDraftModel> roles, ref string message)
        {
            bool isSuccess = false;

            try
            {
                roles = (from a in context.RoleDrafts
                         where a.IsDeleted.Equals(false) &&
                             a.RoleName.Contains(key) || a.RoleDescription.Contains(key)
                         orderby new { a.RoleName, a.RoleDescription }
                         select new RoleDraftModel
                         {
                             Name = a.RoleName,
                             Description = a.RoleDescription
                         }).Take(limit).ToList();



                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddRole(RoleDraftModel role, ref long roleid, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (!context.Roles.Where(a => a.RoleName.Equals(role.Name) && a.IsDeleted.Equals(false)).Any() &&
                    !context.RoleDrafts.Where(a => a.RoleName.Equals(role.Name) && a.IsDeleted.Equals(false)).Any())
                {
                    Entity.RoleDraft item = new Entity.RoleDraft()
                    {
                        MasterRoleID = role.MasterID,
                        RoleName = role.Name,
                        RoleDescription = role.Description,
                        IsDeleted = false,
                        ActionType = role.ActionType,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    };
                    foreach (var sitem in role.Menus)
                    {
                        item.RoleMenuMappingDrafts.Add(new RoleMenuMappingDraft()
                        {
                            MenuID = Guid.Parse(sitem.ID),
                            IsHasAccess = true,
                            CreateDate = DateTime.UtcNow,
                            CreateBy = currentUser.GetCurrentUser().DisplayName
                        });
                    }

                    context.RoleDrafts.Add(item);
                    context.SaveChanges();

                    roleid = item.RoleID;
                    
                    isSuccess = true;
                    message = "New Role Approval data has been created.";
                }
                else
                {
                    isSuccess = true;
                    message = string.Format("Role Draft data for Role Draft Name {0} for action {1} is already exist or waiting for approval.", role.Name, role.ActionType);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateRole(RoleDraftModel role, ref long roleid, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (!context.RoleDrafts.Where(a => a.RoleName.Equals(role.Name) && a.IsDeleted.Equals(false)).Any())
                {
                    Entity.RoleDraft item = new Entity.RoleDraft()
                    {
                        MasterRoleID = role.MasterID,
                        RoleName = role.Name,
                        RoleDescription = role.Description,
                        IsDeleted = false,
                        ActionType = role.ActionType,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    };
                    foreach (var sitem in role.Menus)
                    {
                        item.RoleMenuMappingDrafts.Add(new RoleMenuMappingDraft()
                        {
                            MenuID = Guid.Parse(sitem.ID),
                            IsHasAccess = true,
                            CreateDate = DateTime.UtcNow,
                            CreateBy = currentUser.GetCurrentUser().DisplayName
                        });
                    }

                    context.RoleDrafts.Add(item);
                    context.SaveChanges();

                    roleid = item.RoleID;

                    message = "New Role Approval data has been created.";
                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Data for Role ID {0} is waiting for approval proccess.", role.Name, role.ActionType);
                    isSuccess = true;
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateRole(int id, RoleDraftModel role, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.RoleDraft data = context.RoleDrafts.Where(a => a.RoleID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    if (context.RoleDrafts.Where(a => a.RoleID != role.ID && a.RoleName.Equals(role.Name) && a.IsDeleted.Equals(false)).Count() >= 1)
                    {
                        message = string.Format("Role Draft Name {0} is exist.", role.Name);
                    }
                    else
                    {
                        data.RoleName = role.Name;
                        data.RoleDescription = role.Description;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        context.SaveChanges();

                        isSuccess = true;
                    }
                }
                else
                {
                    message = string.Format("Role data for Role ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeleteRole(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.RoleDraft data = context.RoleDrafts.Where(a => a.RoleID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Role data for Role ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}