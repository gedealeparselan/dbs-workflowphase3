﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Models
{
    #region Property
    [ModelName("POAFunction")]
    public class POAFunctionModel
    {
        /// <summary>
        /// POAFunction ID.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// POAFunctions Name.
        /// </summary>
       // [MaxLength(50, ErrorMessage = "POA Function Name is must be in {1} characters.")]
        public string Name { get; set; }

        /// <summary>
        /// POAFunction Description.
        /// </summary>
       // [MaxLength(255, ErrorMessage = "Maximum length is {1} characters.")]
        public string Description { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }

        public string POAFunctionOther { get; set; }
    }

    public class POAFunctionRow : POAFunctionModel
    {
        public int RowID { get; set; }

    }

    public class POAFunctionGrid : Grid
    {
        public IList<POAFunctionRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class POAFunctionFilter : Filter { }
    #endregion

    #region Interface
    public interface IPOAFunctionRepository : IDisposable
    {
        bool GetPOAFunctionByID(int id, ref POAFunctionModel pOAFunction, ref string message);
        bool GetPOAFunction(ref IList<POAFunctionModel> pOAFunctions, int limit, int index, ref string message);
        bool GetPOAFunction(ref IList<POAFunctionModel> pOAFunctions, ref string message);
        bool GetPOAFunction(int page, int size, IList<POAFunctionFilter> filters, string sortColumn, string sortOrder, ref POAFunctionGrid pOAFunctionGrid, ref string message);
        bool GetPOAFunction(POAFunctionFilter filter, ref IList<POAFunctionModel> pOAFunctions, ref string message);
        bool GetPOAFunction(string key, int limit, ref IList<POAFunctionModel> pOAFunctions, ref string message);
        bool AddPOAFunction(POAFunctionModel pOAFunction, ref string message);
        bool UpdatePOAFunction(int id, POAFunctionModel pOAFunction, ref string message);
        bool DeletePOAFunction(int id, ref string message);
    }
    #endregion

    #region Repository
    public class POAFunctionRepository : IPOAFunctionRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetPOAFunctionByID(int id, ref POAFunctionModel pOAFunction, ref string message)
        {
            bool isSuccess = false;

            try
            {
                pOAFunction = (from a in context.POAFunctions
                               where a.IsDeleted.Equals(false) && a.POAFunctionID.Equals(id)
                               select new POAFunctionModel
                               {
                                   ID = a.POAFunctionID,
                                   Name = a.POAFunctionName,
                                   Description = a.POAFunctionDescription,
                                   LastModifiedDate = a.CreateDate,
                                   LastModifiedBy = a.CreateBy
                               }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool GetPOAFunction(ref IList<POAFunctionModel> pOAFunctions, ref string message)
        { 
          bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    pOAFunctions = (from a in context.POAFunctions
                                    where a.IsDeleted.Equals(false)
                                    orderby new { a.POAFunctionName, a.POAFunctionDescription }
                                    select new POAFunctionModel
                                    {
                                        ID = a.POAFunctionID,
                                        Name = a.POAFunctionName,
                                        Description = a.POAFunctionDescription,
                                        LastModifiedBy = a.CreateBy,
                                        LastModifiedDate = a.CreateDate
                                    }).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool GetPOAFunction(ref IList<POAFunctionModel> pOAFunctions, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    pOAFunctions = (from a in context.POAFunctions
                                    where a.IsDeleted.Equals(false)
                                    orderby new { a.POAFunctionName, a.POAFunctionDescription }
                                    select new POAFunctionModel
                                    {
                                        ID = a.POAFunctionID,
                                        Name = a.POAFunctionName,
                                        Description = a.POAFunctionDescription,
                                        LastModifiedBy = a.CreateBy,
                                        LastModifiedDate = a.CreateDate
                                    }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetPOAFunction(int page, int size, IList<POAFunctionFilter> filters, string sortColumn, string sortOrder, ref POAFunctionGrid pOAFunctionGrid, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                POAFunctionModel filter = new POAFunctionModel();
                if (filters != null)
                {
                    filter.Name = (string)filters.Where(a => a.Field.Equals("Name")).Select(a => a.Value).SingleOrDefault();
                    filter.Description = (string)filters.Where(a => a.Field.Equals("Description")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.POAFunctions
                                let isPOAFunctionName = !string.IsNullOrEmpty(filter.Name)
                                let isPOAFunctionDescription = !string.IsNullOrEmpty(filter.Description)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) &&
                                    (isPOAFunctionName ? a.POAFunctionName.Contains(filter.Name) : true) &&
                                    (isPOAFunctionDescription ? a.POAFunctionDescription.Contains(filter.Description) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new POAFunctionModel
                                {
                                    ID = a.POAFunctionID,
                                    Name = a.POAFunctionName,
                                    Description = a.POAFunctionDescription,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    pOAFunctionGrid.Page = page;
                    pOAFunctionGrid.Size = size;
                    pOAFunctionGrid.Total = data.Count();
                    pOAFunctionGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    pOAFunctionGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new POAFunctionRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            Name = a.Name,
                            Description = a.Description,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetPOAFunction(POAFunctionFilter filter, ref IList<POAFunctionModel> pOAFunctions, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetPOAFunction(string key, int limit, ref IList<POAFunctionModel> pOAFunctions, ref string message)
        {
            bool isSuccess = false;

            try
            {
                pOAFunctions = (from a in context.POAFunctions
                                where a.IsDeleted.Equals(false) &&
                                    a.POAFunctionName.Contains(key) || a.POAFunctionDescription.Contains(key)
                                orderby new { a.POAFunctionName, a.POAFunctionDescription }
                                select new POAFunctionModel
                                {
                                    Name = a.POAFunctionName,
                                    Description = a.POAFunctionDescription
                                }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddPOAFunction(POAFunctionModel pOAFunction, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (!context.POAFunctions.Where(a => a.POAFunctionName.Equals(pOAFunction.Name) && a.IsDeleted.Equals(false)).Any())
                {
                    context.POAFunctions.Add(new Entity.POAFunction()
                    {
                        POAFunctionName = pOAFunction.Name,
                        POAFunctionDescription = pOAFunction.Description,
                        IsDeleted = false,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    });

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("POA Function data for POA Function Name {0} is already exist.", pOAFunction.Name);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdatePOAFunction(int id, POAFunctionModel pOAFunction, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.POAFunction data = context.POAFunctions.Where(a => a.POAFunctionID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    if (context.POAFunctions.Where(a => a.POAFunctionID != pOAFunction.ID && a.POAFunctionName.Equals(pOAFunction.Name) && a.IsDeleted.Equals(false)).Count() >= 1)
                    {
                        message = string.Format("POA Function Name {0} is exist.", pOAFunction.Name);
                    }
                    else
                    {
                        data.POAFunctionName = pOAFunction.Name;
                        data.POAFunctionDescription = pOAFunction.Description;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        context.SaveChanges();

                        isSuccess = true;
                    }
                }
                else
                {
                    message = string.Format("POA Function data for POA Function ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeletePOAFunction(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.POAFunction data = context.POAFunctions.Where(a => a.POAFunctionID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("POA Function data for POA Function ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    } 
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}