﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.EntityClient;
using System.Data.SqlClient;
using DBS.Entity;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Transactions;

namespace DBS.WebAPI.Models
{
    #region Property
    public class ExceptionHandlingModel
    {
        public int ID { get; set; }

        [MaxLength(50, ErrorMessage = "Exception Handling Name is must be in {1} characters.")]
        public string CaseName { get; set; }

        public MatrixModel Matrix { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public string LastModifiedBy { get; set; }

        public IList<ExceptionHandlingDetail> ExceptionHandlingsDetail { get; set; }


    }

    public class ExceptionHandlingDetail
    {
        public int ID { get; set; }

        public string InternalCondition { get; set; }

        public string ExternalCondition { get; set; }

        public IList<ExceptionHandlingSubDetail> ExceptionHandlingsSubDetail { get; set; }

    }

    public class ExceptionHandlingSubDetail
    {
        public int ID { get; set; }

        public int ParamID { get; set; }

        public string ParamField { get; set; }

        public int OperatorID { get; set; }

        public string OperatorField { get; set; }

        public byte? ValueType { get; set; }

        public string Value { get; set; }

    }

    public class ExceptionHandlingRow : ExceptionHandlingModel
    {
        public int RowID { get; set; }

    }

    public class ExceptionHandlingGrid : Grid
    {
        public IList<ExceptionHandlingRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class ExceptionHandlingFilter : Filter { }
    #endregion

    #region Interface
    public interface IExceptionHandlingRepository : IDisposable
    {
        bool GetExceptionHandlingByID(int id, ref ExceptionHandlingModel exceptionHandling, ref string message);
        bool GetExceptionHandling(ref IList<ExceptionHandlingModel> exceptionHandlings, ref string message);
        bool GetExceptionHandling(ref IList<ExceptionHandlingModel> exceptionHandlings, int limit, int index, ref string message);
        bool GetExceptionHandling(int page, int size, IList<ExceptionHandlingFilter> filters, string sortColumn, string sortOrder, ref ExceptionHandlingGrid ExceptionHandling, ref string message);
        bool GetExceptionHandling(ExceptionHandlingFilter filter, ref IList<ExceptionHandlingModel> exceptionHandlings, ref string message);
        bool GetExceptionHandling(string key, int limit, ref IList<ExceptionHandlingModel> exceptionHandlings, ref string message);
        bool AddExceptionHandling(ExceptionHandlingModel exceptionHandling, ref string message);
        bool UpdateExceptionHandling(int id, ExceptionHandlingModel exceptionHandling, ref string message);
        bool DeleteExceptionHandling(int id, ref string message);        
    }
    #endregion

    #region Repository
    public class ExceptionHandlingRepository : IExceptionHandlingRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();       
        
        public bool GetExceptionHandlingByID(int id, ref ExceptionHandlingModel exceptionHandling, ref string message)
        {
            bool isSuccess = false;

            try
            {
                exceptionHandling = (from a in context.ExceptionHandlings
                                     where a.IsDeleted.Equals(false) && a.ExceptionHandlingID.Equals(id)
                                     select new ExceptionHandlingModel
                                     {
                                         ID = a.ExceptionHandlingID,
                                         CaseName = a.CaseName,
                                         Matrix = new MatrixModel { ID = a.Matrix.MatrixID, Name = a.Matrix.MatrixName, Description = a.Matrix.MatrixDescription },
                                         LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                         LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,
                                         ExceptionHandlingsDetail = context.ExceptionHandlingDetails.Where(e => e.ExceptionHandlingID == a.ExceptionHandlingID && e.IsDeleted.Equals(false)).Select(e => new ExceptionHandlingDetail
                                         {
                                             ID = e.ExceptionHandlingDetailID,
                                             InternalCondition = e.ConditionInternal,
                                             ExternalCondition = e.ConditionExternal,
                                             ExceptionHandlingsSubDetail = context.ExceptionHandlingSubDetails.Where(n => n.ExceptionHandlingDetailID == e.ExceptionHandlingDetailID && n.IsDeleted.Equals(false)).Select(n => new ExceptionHandlingSubDetail
                                             {
                                                 ID = n.ExceptionHandlingSubDetailID,
                                                 ParamID = n.ExceptionHandlingParameterID,
                                                 OperatorID = n.ExceptionHandlingOperatorID,
                                                 ValueType = n.ValueType.Value,
                                                 Value = n.Value.Trim()
                                             }).ToList(),
                                         }).ToList(),
                                     }).SingleOrDefault();
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool GetExceptionHandling(ref IList<ExceptionHandlingModel> exceptionHandlings, ref string message)
        {

            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    var result = (from a in context.ExceptionHandlings
                                  where a.IsDeleted.Equals(false)
                                  orderby new { a.CaseName }
                                  select new ExceptionHandlingModel
                                  {
                                      ID = a.ExceptionHandlingID,
                                      CaseName = a.CaseName,
                                      Matrix = new MatrixModel { ID = a.Matrix.MatrixID, Name = a.Matrix.MatrixName, Description = a.Matrix.MatrixDescription },

                                      LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                      LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,
                                      ExceptionHandlingsDetail = context.ExceptionHandlingDetails.Where(e => e.ExceptionHandlingID == a.ExceptionHandlingID && e.IsDeleted.Equals(false)).Select(e => new ExceptionHandlingDetail
                                      {
                                          ID = e.ExceptionHandlingDetailID,
                                          InternalCondition = e.ConditionInternal,
                                          ExternalCondition = e.ConditionExternal,
                                          ExceptionHandlingsSubDetail = context.ExceptionHandlingSubDetails.Where(n => n.ExceptionHandlingDetailID == e.ExceptionHandlingDetailID && n.IsDeleted.Equals(false)).Select(n => new ExceptionHandlingSubDetail
                                          {
                                              ID = n.ExceptionHandlingSubDetailID,
                                              ParamID = n.ExceptionHandlingParameterID,
                                              OperatorID = n.ExceptionHandlingOperatorID,
                                              ValueType = n.ValueType.Value,
                                              Value = n.Value.Trim()
                                          }).ToList(),
                                      }).ToList(),
                                  }).ToList();
                    exceptionHandlings = result.ToList<ExceptionHandlingModel>();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;

        }
        public bool GetExceptionHandling(ref IList<ExceptionHandlingModel> exceptionHandlings, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    var result = (from a in context.ExceptionHandlings
                                  where a.IsDeleted.Equals(false)
                                  orderby new { a.CaseName }
                                  select new ExceptionHandlingModel
                                  {
                                      ID = a.ExceptionHandlingID,
                                      CaseName = a.CaseName,
                                      Matrix = new MatrixModel { ID = a.Matrix.MatrixID, Name = a.Matrix.MatrixName, Description = a.Matrix.MatrixDescription },
                                      LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                      LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,
                                      ExceptionHandlingsDetail = context.ExceptionHandlingDetails.Where(e => e.ExceptionHandlingID == a.ExceptionHandlingID && e.IsDeleted.Equals(false)).Select(e => new ExceptionHandlingDetail
                                      {
                                          ID = e.ExceptionHandlingDetailID,
                                          InternalCondition = e.ConditionInternal,
                                          ExternalCondition = e.ConditionExternal,
                                          ExceptionHandlingsSubDetail = context.ExceptionHandlingSubDetails.Where(n => n.ExceptionHandlingDetailID == e.ExceptionHandlingDetailID && n.IsDeleted.Equals(false)).Select(n => new ExceptionHandlingSubDetail
                                          {
                                              ID = n.ExceptionHandlingSubDetailID,
                                              ParamID = n.ExceptionHandlingParameterID,
                                              OperatorID = n.ExceptionHandlingOperatorID,
                                              ValueType = n.ValueType.Value,
                                              Value = n.Value.Trim()
                                          }).ToList(),
                                      }).ToList(),
                                  }).Skip(skip).Take(limit).ToList();
                    exceptionHandlings = result.ToList<ExceptionHandlingModel>();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetExceptionHandling(int page, int size, IList<ExceptionHandlingFilter> filters, string sortColumn, string sortOrder, ref ExceptionHandlingGrid ExceptionHandlingGrid, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                ExceptionHandlingModel filter = new ExceptionHandlingModel();
                filter.Matrix = new MatrixModel { Name = string.Empty };

                if (filters != null)
                {
                    filter.Matrix.Name = (string)filters.Where(a => a.Field.Equals("Matrix")).Select(a => a.Value).SingleOrDefault();
                    filter.CaseName = (string)filters.Where(a => a.Field.Equals("CaseName")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.ExceptionHandlings
                                let isMatrix = !string.IsNullOrEmpty(filter.Matrix.Name)
                                let isCaseName = !string.IsNullOrEmpty(filter.CaseName)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) &&
                                    (isMatrix ? a.Matrix.MatrixName.Contains(filter.Matrix.Name) : true) &&
                                    (isCaseName ? a.CaseName.Contains(filter.CaseName) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new ExceptionHandlingModel
                                {
                                    ID = a.ExceptionHandlingID,
                                    CaseName = a.CaseName,
                                    Matrix = new MatrixModel { ID = a.Matrix.MatrixID, Name = a.Matrix.MatrixName, Description = a.Matrix.MatrixDescription },
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    ExceptionHandlingGrid.Page = page;
                    ExceptionHandlingGrid.Size = size;
                    ExceptionHandlingGrid.Total = data.Count();
                    ExceptionHandlingGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    ExceptionHandlingGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new ExceptionHandlingRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            CaseName = a.CaseName,
                            Matrix = a.Matrix,
                            ExceptionHandlingsDetail = context.ExceptionHandlingDetails.Where(e => e.ExceptionHandlingID == a.ID && e.IsDeleted.Equals(false)).Select(e => new ExceptionHandlingDetail
                            {
                                ID = e.ExceptionHandlingDetailID,
                                InternalCondition = e.ConditionInternal,
                                ExternalCondition = e.ConditionExternal,
                                ExceptionHandlingsSubDetail = context.ExceptionHandlingSubDetails.Where(n => n.ExceptionHandlingDetailID == e.ExceptionHandlingDetailID && n.IsDeleted.Equals(false)).Select(n => new ExceptionHandlingSubDetail
                                {
                                    ID = n.ExceptionHandlingSubDetailID,
                                    ParamID = n.ExceptionHandlingParameterID,
                                    OperatorID = n.ExceptionHandlingOperatorID,
                                    ValueType = n.ValueType.Value,
                                    Value = n.Value.Trim()
                                }).ToList(),
                            }).ToList(),
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetExceptionHandling(ExceptionHandlingFilter filter, ref IList<ExceptionHandlingModel> Matrices, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetExceptionHandling(string key, int limit, ref IList<ExceptionHandlingModel> exceptionHandlings, ref string message)
        {
            bool isSuccess = false;

            try
            {
                exceptionHandlings = (from a in context.ExceptionHandlings
                                      join b in context.Matrices on a.MatrixID equals b.MatrixID
                                      where a.IsDeleted.Equals(false) && b.IsDeleted.Equals(false) && (a.CaseName.Contains(key))
                                      orderby new { a.CaseName }
                                      select new ExceptionHandlingModel
                                      {
                                          ID = a.ExceptionHandlingID,
                                          CaseName = a.CaseName,
                                          Matrix = new MatrixModel { ID = a.Matrix.MatrixID, Name = a.Matrix.MatrixName, Description = a.Matrix.MatrixDescription },
                                          ExceptionHandlingsDetail = context.ExceptionHandlingDetails.Where(e => e.ExceptionHandlingID == a.ExceptionHandlingID && e.IsDeleted.Equals(false)).Select(e => new ExceptionHandlingDetail
                                          {
                                              ID = e.ExceptionHandlingDetailID,
                                              InternalCondition = e.ConditionInternal,
                                              ExternalCondition = e.ConditionExternal,
                                              ExceptionHandlingsSubDetail = context.ExceptionHandlingSubDetails.Where(n => n.ExceptionHandlingDetailID == e.ExceptionHandlingDetailID && n.IsDeleted.Equals(false)).Select(n => new ExceptionHandlingSubDetail
                                              {
                                                  ID = n.ExceptionHandlingSubDetailID,
                                                  ParamID = n.ExceptionHandlingParameterID,
                                                  OperatorID = n.ExceptionHandlingOperatorID,
                                                  ValueType = n.ValueType.Value,
                                                  Value = n.Value.Trim()
                                              }).ToList(),
                                          }).ToList(),
                                          LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                          LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                      }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddExceptionHandling(ExceptionHandlingModel exceptionHandling, ref string message)
        {
            bool isSuccess = false;
            string strvalue = string.Empty;
            if (exceptionHandling != null)
            {
                try
                {
                    if (!context.ExceptionHandlings.Where(a => a.ExceptionHandlingID.Equals(exceptionHandling.ID) &&
                                                               a.MatrixID.Equals(exceptionHandling.Matrix.ID) &&
                                                               a.IsDeleted.Equals(false)).Any())
                    {
                        using(TransactionScope ts = new TransactionScope())
                        {
                            try
                            {
                                Entity.ExceptionHandling result = new Entity.ExceptionHandling()
                                {
                                    CaseName = exceptionHandling.CaseName,
                                    MatrixID = exceptionHandling.Matrix.ID,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().DisplayName
                                };

                                context.ExceptionHandlings.Add(result);
                                context.SaveChanges();

                                foreach (var data in exceptionHandling.ExceptionHandlingsDetail)
                                {
                                    Entity.ExceptionHandlingDetail detail = new Entity.ExceptionHandlingDetail()
                                    {
                                        ExceptionHandlingID = result.ExceptionHandlingID,
                                        ConditionInternal = data.InternalCondition,
                                        ConditionExternal = data.ExternalCondition,
                                        IsDeleted = false,
                                        CreateDate = DateTime.UtcNow,
                                        CreateBy = currentUser.GetCurrentUser().DisplayName

                                    };
                                    context.ExceptionHandlingDetails.Add(detail);
                                    context.SaveChanges();

                                    foreach (var subdata in data.ExceptionHandlingsSubDetail)
                                    {
                                        if (subdata.Value.Contains('(') && subdata.Value.Contains(')'))
                                        {
                                            var posStart = 0;
                                            var posEnd = subdata.Value.IndexOf('(');
                                            var lengthStr = (int)posEnd - (int)posStart;
                                            strvalue = subdata.Value.Substring(posStart, lengthStr);
                                        }
                                        else
                                        {
                                            strvalue = subdata.Value;
                                        }

                                        Entity.ExceptionHandlingSubDetail subdetail = new Entity.ExceptionHandlingSubDetail()
                                        {

                                            ExceptionHandlingDetailID = detail.ExceptionHandlingDetailID,
                                            ExceptionHandlingParameterID = subdata.ParamID,
                                            ExceptionHandlingOperatorID = subdata.OperatorID,
                                            ValueType = subdata.ValueType,
                                            Value = strvalue,
                                            IsDeleted = false,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().DisplayName
                                        };

                                        context.ExceptionHandlingSubDetails.Add(subdetail);
                                        context.SaveChanges();
                                    }
                                }

                                int lastExceptionHandlingID = context.ExceptionHandlings.Max(item => item.ExceptionHandlingID);

                                using (context)
                                {
                                    using (context.Database.Connection)
                                    {
                                        context.Database.Connection.Open();
                                        DbCommand cmd = context.Database.Connection.CreateCommand();
                                        cmd.CommandText = "SP_WF_GenerateExceptionHandlingCondition";
                                        cmd.CommandType = CommandType.StoredProcedure;
                                        cmd.Parameters.Add(new SqlParameter("ExceptionHandlingID", lastExceptionHandlingID));
                                        cmd.ExecuteNonQuery();
                                        context.Database.Connection.Close();

                                    }

                                }


                                ts.Complete();
                                isSuccess = true;
                            }
                            catch (Exception ex)
                            {
                                ts.Dispose();
                                message = ex.Message;
                            }
                        }
                    }
                    else
                    {
                        message = string.Format("Exception Handling data for Exception Handling {0},{1} is already exist.", exceptionHandling.CaseName, exceptionHandling.Matrix);
                    }
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
            }

            return isSuccess;
        }

        public bool UpdateExceptionHandling(int id, ExceptionHandlingModel exceptionHandling, ref string message)
        {
            bool isSuccess = false;
            string strvalue = string.Empty;

            try
            {
                Entity.ExceptionHandling data = context.ExceptionHandlings.Where(a => a.ExceptionHandlingID.Equals(id)).SingleOrDefault();


                if (data != null)
                {
                    
                    using (TransactionScope ts = new TransactionScope())
                    {
                        try
                        {
                            data.CaseName = exceptionHandling.CaseName;
                            data.MatrixID = exceptionHandling.Matrix.ID;
                            data.UpdateDate = DateTime.UtcNow;
                            data.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                            context.SaveChanges();

                            var detailID = context.ExceptionHandlingDetails.Where(a => a.ExceptionHandlingID.Equals(id)).ToList();
                            foreach (var item in detailID)
                            {
                                DeletedSubDetail(item.ExceptionHandlingDetailID);
                            }

                            DeletedDetail(id);

                            foreach (var detail in exceptionHandling.ExceptionHandlingsDetail)
                            {

                                Entity.ExceptionHandlingDetail dataDetail = new Entity.ExceptionHandlingDetail()
                                {
                                    ExceptionHandlingID = id,
                                    ConditionInternal = detail.InternalCondition,
                                    ConditionExternal = detail.ExternalCondition,
                                    IsDeleted = false,
                                    CreateDate = data.CreateDate,
                                    CreateBy = data.CreateBy,
                                };
                                context.ExceptionHandlingDetails.Add(dataDetail);
                                context.SaveChanges();

                                foreach (var subdetail in detail.ExceptionHandlingsSubDetail)
                                {
                                    if (subdetail.Value.Contains('(') && subdetail.Value.Contains(')'))
                                    {
                                        var posStart = 0;
                                        var posEnd = subdetail.Value.IndexOf('(');
                                        var lengthStr = (int)posEnd - (int)posStart;
                                        strvalue = subdetail.Value.Substring(posStart, lengthStr);
                                    }
                                    else
                                    {
                                        strvalue = subdetail.Value;
                                    }
                                    Entity.ExceptionHandlingSubDetail dataSubdetail = new Entity.ExceptionHandlingSubDetail()
                                    {
                                        ExceptionHandlingDetailID = dataDetail.ExceptionHandlingDetailID,
                                        ExceptionHandlingParameterID = subdetail.ParamID,
                                        ExceptionHandlingOperatorID = subdetail.OperatorID,
                                        ValueType = subdetail.ValueType,
                                        Value = strvalue,
                                        IsDeleted = false,
                                        CreateDate = data.CreateDate,
                                        CreateBy = data.CreateBy
                                    };
                                    context.ExceptionHandlingSubDetails.Add(dataSubdetail);
                                    context.SaveChanges();
                                }

                            }

                            using (context)
                            {
                                using (context.Database.Connection)
                                {
                                    context.Database.Connection.Open();
                                    DbCommand cmd = context.Database.Connection.CreateCommand();
                                    cmd.CommandText = "SP_WF_GenerateExceptionHandlingCondition";
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.Parameters.Add(new SqlParameter("ExceptionHandlingID", id));
                                    cmd.ExecuteNonQuery();
                                    context.Database.Connection.Close();
                                }

                            }
                            ts.Complete();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            ts.Dispose();
                            message = ex.Message;
                        }

                    }
                }
               
            }

            catch (Exception ex)
            {
                
                message = ex.Message;

            }

            return isSuccess;
        }

        public bool DeleteExceptionHandling(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                var data = context.ExceptionHandlings.Where(a => a.ExceptionHandlingID.Equals(id)).SingleOrDefault();
                if (data != null)
                {
                    using (TransactionScope ts = new TransactionScope())
                    {
                        try
                        {
                            data.IsDeleted = true;
                            data.UpdateDate = DateTime.UtcNow;
                            data.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                            context.SaveChanges();
                            var dataDetail = context.ExceptionHandlingDetails.Where(a => a.ExceptionHandlingID.Equals(id)).ToList();

                            foreach (var subdetail in dataDetail)
                            {
                                if (subdetail.ExceptionHandlingDetailID != null)
                                {
                                    var dataSubdetail = context.ExceptionHandlingSubDetails.Where(a => a.ExceptionHandlingDetailID.Equals(subdetail.ExceptionHandlingDetailID)).ToList();
                                    if (dataSubdetail != null && dataSubdetail.Count > 0)
                                    {
                                        dataSubdetail.ForEach(a =>
                                        {
                                            a.IsDeleted = true;
                                        });
                                        context.SaveChanges();
                                    }
                                }
                            }


                            if (dataDetail != null && dataDetail.Count > 0)
                            {
                                dataDetail.ForEach(a =>
                                {
                                    a.IsDeleted = true;

                                });
                                context.SaveChanges();
                            }
                            ts.Complete();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            ts.Dispose();
                            message = ex.Message;
                        }
                        
                    }
                   
                }
                else
                {
                    message = string.Format("Exception Handling data for ExceptionHandling Approval ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        private void DeletedDetail(int id)
        {

            try
            {
                var datadetail = context.ExceptionHandlingDetails.Where(a => a.ExceptionHandlingID.Equals(id) && a.IsDeleted.Equals(false)).ToList();
                if (datadetail != null && datadetail.Count > 0)
                {
                    datadetail.ForEach(a =>
                        {
                            a.IsDeleted = true;

                        }
                    );
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       
        private void DeletedSubDetail(int id)
        {

            try
            {
                var datasubdetail = context.ExceptionHandlingSubDetails.Where(a => a.ExceptionHandlingDetailID.Equals(id) && a.IsDeleted.Equals(false)).ToList();
                if (datasubdetail != null)
                {
                    datasubdetail.ForEach(a =>
                        {
                            a.IsDeleted = true;

                        }
                    );

                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }
       
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}