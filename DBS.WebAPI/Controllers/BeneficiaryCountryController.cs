﻿using System;
using DBS.WebAPI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Description;
using System.Web;
using System.Web.Http;

namespace DBS.WebAPI.Controllers
{
    //[Authorize]
    public class BeneficiaryCountryController : ApiController
    {
        private string message = string.Empty;

        private readonly IBeneficiaryCountryRepository repository = null;

        public BeneficiaryCountryController()
        {
            this.repository = new BeneficiaryCountryRepository();
        }

        public BeneficiaryCountryController(IBeneficiaryCountryRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all bene countries.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<BeneficiaryCountryModel>))]
        public HttpResponseMessage Get()
        {
            IList<BeneficiaryCountryModel> output = new List<BeneficiaryCountryModel>();

            if (!repository.GetBeneficiaryCountry(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<BeneficiaryCountryModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get bene countries with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<BeneficiaryCountryModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<BeneficiaryCountryModel> output = new List<BeneficiaryCountryModel>();

            if (!repository.GetBeneficiaryCountry(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<BeneficiaryCountryModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get bene countries for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(BeneficiaryCountryGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<BeneficiaryCountryFilter> filters)
        {
            BeneficiaryCountryGrid output = new BeneficiaryCountryGrid();

            if (!repository.GetBeneficiaryCountry(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<BeneficiaryCountryGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find bene countries using criterias.
        /// </summary>
        /// <param name="query">Code or Description.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<BeneficiaryCountryModel>))]
        [HttpGet]
        [Route("api/BeneficiaryCountry/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<BeneficiaryCountryModel> output = new List<BeneficiaryCountryModel>();

            if (!repository.GetBeneficiaryCountry(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<BeneficiaryCountryModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get bene country by ID.
        /// </summary>
        /// <param name="id">Bene Country ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(BeneficiaryCountryModel))]
        [Route("api/BeneficiaryCountry/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            BeneficiaryCountryModel output = new BeneficiaryCountryModel();

            if (!repository.GetBeneficiaryCountryByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<BeneficiaryCountryModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new bene country.
        /// </summary>
        /// <param name="beneCountry">Bene Country data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(BeneficiaryCountryModel beneCountry)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddBeneficiaryCountry(beneCountry, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New Beneficiary Country data has been created." });
                }
            }
        }

        /// <summary>
        /// Modify existing bene country.
        /// </summary>
        /// <param name="id">ID of Bene Country to be modify.</param>
        /// <param name="beneCountry">Bene country data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/BeneficiaryCountry/{id}")]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(int id, BeneficiaryCountryModel beneCountry)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateBeneficiaryCountry(id, beneCountry, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Beneficiary Country data has been updated." });
                }
            }
        }

        /// <summary>
        /// Remove exisiting bene country.
        /// </summary>
        /// <param name="id">ID of bene country to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/BeneficiaryCountry/{id}")]
        //[Authorize(Roles = "DBS Admin")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteBeneficiaryCountry(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Beneficiary Country data has been deleted." });
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }

}