﻿using DBS.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace DBS.WebAPI.Controllers
{
    public class MurexMappingController : ApiController
    {
        private string message = string.Empty;

        private readonly IMurexMappingRepository repository = null;
        public MurexMappingController()
        {
            this.repository = new MurexMappingRepository();
        }
        public MurexMappingController(IMurexMappingRepository repository)
        {
            this.repository = repository;
        }
        
        [ResponseType(typeof(IList<MurexMappingModel>))]
        public HttpResponseMessage GetAll()
        {
            IList<MurexMappingModel> result = new List<MurexMappingModel>();
            if (!this.repository.GetMurexByLimit(ref result, 10, 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            } else {
                return Request.CreateResponse<IList<MurexMappingModel>>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(IList<MurexMappingModel>))]
        public HttpResponseMessage GetAll(int? limit, int? index)
        {
            IList<MurexMappingModel> result = new List<MurexMappingModel>();
            if (!repository.GetMurexByLimit(ref result, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<IList<MurexMappingModel>>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(MurexMappingGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<MurexMappingFilter> filters)
        {
            MurexMappingGrid result = new MurexMappingGrid();
            if (!repository.GetMurexByPagination(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "MurexName" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref result, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<MurexMappingGrid>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(IList<MurexMappingModel>))]
        [HttpGet]
        [Route("api/MurexMapping/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<MurexMappingModel> result = new List<MurexMappingModel>();

            if (!repository.GetMurexBySearch(query, limit.HasValue ? limit.Value : 10, ref result, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<MurexMappingModel>>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(MurexMappingModel))]
        [Route("api/MurexMapping/{id}")]
        [HttpGet]
        public HttpResponseMessage GetById(int id)
        {
            MurexMappingModel result = new MurexMappingModel();

            if (!repository.GetMurexById(id, ref result, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<MurexMappingModel>(HttpStatusCode.OK, result);
            }
        }

        [HttpPost]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Post(MurexMappingModel murex)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddMurex(murex, ref message))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse<string>(HttpStatusCode.Created, "New Murex data has been created.");
                }
            }
        }

        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/MurexMapping/{id}")]
        public HttpResponseMessage Put(int id, MurexMappingModel murex)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateMurex(id, murex, ref message))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Murex data has been updated.");
                }
            }
        }

        [HttpDelete]
        [Route("api/MurexMapping/{id}")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteMurex(id, ref message))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse<string>(HttpStatusCode.OK, "Murex data has been deleted.");
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}