﻿
var ViewModel = function () {
    var self = this;

    //Model   
    var CustomerModel = {
        CIF: ko.observable(),
        Name: ko.observable(),
        POAName: ko.observable(),
        BizSegment: ko.observable(),
        Branch: ko.observable(),
        Type: ko.observable(),
        RM: ko.observable(),
        Contacts: ko.observableArray([]),
        Functions: ko.observableArray([]),
        Accounts: ko.observableArray([]),
        Underlyings: ko.observableArray([]),
        LastModifiedBy: ko.observable(),
        LastModifiedDate: ko.observable()        
    };

    //propertiesBank
    self.ID = ko.observable(0);
    self.MurexName = ko.observable("");
    self.Customer = ko.observable(CustomerModel);
    self.LastModifiedBy = ko.observable("");
    self.LastModifiedDate = ko.observable("");

    //self.ddlCurrency = ko.observableArray([]);
    //self.ddlBank = ko.observableArray([]);

    //filters
    self.FilterMurexName = ko.observable("");
    self.FilterCIF = ko.observable("");
    self.FilterCustomerName = ko.observable("");
    self.FilterModifiedBy = ko.observable("");
    self.FilterModifiedDate = ko.observable("");

    // grid properties
    //self.allowFilter = ko.observable(false);
    self.AvailableSizes = ko.observableArray([10, 25, 50, 75, 100]);
    self.Page = ko.observable(1);
    self.Size = ko.observableArray([10]);
    self.Total = ko.observable(0);
    self.TotalPages = ko.observable(0);

    self.GridProperties = ko.observable();
    self.GridProperties(new GridPropertiesModel(GetData));
    self.GridProperties().SortColumn("MurexName");
    self.GridProperties().SortOrder("ASC");

    // bind clear filters
    self.ClearFilters = function () {
        self.FilterMurexName = ko.observable("");
        self.FilterCIF = ko.observable("");
        self.FilterCustomerName = ko.observable("");
        self.FilterModifiedBy = ko.observable("");
        self.FilterModifiedDate = ko.observable("");
        GetData();
    };

    // flag
    self.BarLoad = ko.observable(false);
    self.IsNewData = ko.observable(false);
    self.Readonly = ko.observable(false);
    self.IsWorkflow = ko.observable(false);
    self.IsRoleMaker = ko.observable(false);
    self.IsPermited = ko.observable(false);

    // Object for storing data from view while entered
    var Murex = {
        ID: self.ID,
        MurexName: self.MurexName,
        Customer: self.Customer,
        LastModifiedBy: self.LastModifiedBy,
        LastModifiedDate: self.LastModifiedDate
    };

    self.GetData = function () {
        GetData();
    }

    // storing for json respon
    self.Murexs = ko.observableArray([]);

    // event handler
    self.update = function () {
        // validation
        var form = $("#aspnetForm");
        //var CurrencyCodeDescription = $('#currency option:selected').text();
        //console.log(CurrencyCodeDescription);        
        //var CurrencyUpdate = $('#currencyhidden option:selected').val();
        //self.Currency().CodeDescription(CurrencyCodeDescription);
        form.validate();

        if (form.valid()) {
            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {
                    $.ajax({
                        type: "PUT",
                        url: api.server + api.url.murexmapping + "/" + Murex.ID(),
                        data: ko.toJSON(Murex),
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {
                                // send notification
                                ShowNotification('Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // hide current popup window
                                $("#modal-form").modal('hide');

                                // refresh data
                                GetData();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });
                }
            })
        }

    };

    self.save = function () {
        // validation
        var form = $("#aspnetForm");    
        form.validate();
        //console.log(ko.toJSON(Murex));
        //return;
        if (form.valid()) {
            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {

                    //Ajax call to insert 
                    $.ajax({
                        type: "POST",
                        url: api.server + api.url.murexmapping,
                        data: ko.toJSON(Murex), //Convert the Observable Data into JSON
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status == 200) {
                                // send notification
                                ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // hide current popup window
                                $("#modal-form").modal('hide');

                                // refresh data
                                GetData();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });
                }
            });
        }
    };

    self.delete = function () {
        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                $("#modal-form").modal('show');
            } else {

                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.murexmapping + "/" + Murex.ID(),
                    data: ko.toJSON(Murex),
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if (jqXHR.status == 200) {
                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                            // refresh data
                            GetData();
                        } else
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
            }
        });
    };

    //insert new
    self.NewData = function () {
        // flag as new Data
        self.IsNewData(true);
        self.Readonly(false);
        // bind empty data
        self.ID(0); //= ko.observable(0);
        self.MurexName("");// = ko.observable("");
        self.Customer(CustomerModel);// = ko.observable(CustomerModel);
    };

    self.IsPermited = function (IsPermitedResult) {
        //Ajax call to delete the Customer
        spUser = $.cookie(api.cookie.spUser);

        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/CheckUserPermited/" + _spFriendlyUrlPageContextInfo.title,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    //compare user role to page permission to get checker validation
                    console.log("spUser Roles : " + ko.toJSON(spUser.Roles));
                    console.log("spUser Permited : " + data);
                    for (i = 0; i < spUser.Roles.length; i++) {
                        for (j = 0; j < data.length; j++) {
                            if (Const_RoleName[spUser.Roles[i].ID] == data[j]) { //if (spUser.Roles[i].Name == data[j]) {
                                if (Const_RoleName[spUser.Roles[i].ID].endsWith('Maker')) { //if (spUser.Roles[i].Name.endsWith('Maker')) {
                                    self.IsRoleMaker(true);
                                    return;
                                }
                                else {
                                    self.IsRoleMaker(false);
                                }
                            }
                        }
                    }
                    console.log("IsRoleMaker : " + self.IsRoleMaker());
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return "";
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-17
    };

    
    self.SetAutoCompleted = function () {
        // autocomplete
        $("#customer-name").autocomplete({
            source: function (request, response) {
                // declare options variable for ajax get request
                var options = {
                    url: api.server + api.url.customer + "/Search",
                    data: {
                        query: request.term,
                        limit: 20
                    },
                    token: accessToken
                };

                // exec ajax request
                Helper.Ajax.AutoComplete(options, response, OnSuccessAutoComplete, OnError);
            },
            minLength: 2,
            select: function (event, ui) {
                // set customer data model
                if (ui.item.data != undefined || ui.item.data != null) {
                    var mapping = {
                        'ignore': ["LastModifiedDate", "LastModifiedBy"]
                    };
                    self.Customer(ko.mapping.toJS(ui.item.data, mapping));
                }
                else
                    self.Customer(null);
            }
        });
    };

    // bind get data function to view
    function GetData() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.murexmapping,//
            params: {
                page: self.GridProperties().Page(),
                size: self.GridProperties().Size(),
                sort_column: self.GridProperties().SortColumn(),
                sort_order: self.GridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetData, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetData, OnError, OnAlways);
        }
    };

    

    // Get filtered columns value
    function GetFilteredColumns() {
        // define filters array
        var filters = [];

        //add data to array with key value ( in here Field and Value are key)
        if (self.FilterMurexName() != "") filters.push({ Field: 'MurexName', Value: self.FilterMurexName() });
        if (self.FilterCIF() != "") filters.push({ Field: 'CIF', Value: self.FilterCIF() });
        if (self.FilterCustomerName() != "") filters.push({ Field: 'CustomerName', Value: self.FilterCustomerName() });
        if (self.FilterModifiedBy() != "") filters.push({ Field: 'LastModifiedBy', Value: self.FilterModifiedBy() });
        if (self.FilterModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.FilterModifiedDate() });
        return filters;
    };

    //Function to Display record to be updated
    self.GetSelectedRow = function (data) {
        //self.ID = ko.observable(0);
        //self.MurexName = ko.observable("");
        //self.Customer = ko.observable(CustomerModel);

        self.IsNewData(false);

        if (self.IsWorkflow()) $("#modal-form-workflow").modal('show');
        else $("#modal-form").modal('show');
        
        self.ID(data.ID);
        self.MurexName(data.MurexName);
        self.Customer(ko.mapping.toJS(data.Customer));
    };

    // On success GetData callback
    function OnSuccessGetData(data, textStatus, jqXHR) {
        if (jqXHR.status == 200) {
            console.log(data);
            self.Murexs(data.Rows);
            self.GridProperties().Page(data['Page']);
            self.GridProperties().Size(data['Size']);
            self.GridProperties().Total(data['Total']);
            self.GridProperties().TotalPages(Math.ceil(self.GridProperties().Total() / self.GridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    // On Error callback
    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    // On Always callback
    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }

    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }

    });

    function OnSuccessAutoComplete(response, data, textStatus, jqXHR) {

        response($.map(data, function (item) {
            return {
                // default autocomplete object
                label: item.CIF + " - " + item.Name,
                value: item.Name,

                // custom object binding
                data: {
                    CIF: item.CIF,
                    Name: item.Name,
                    POAName: item.POAName,
                    Accounts: item.Accounts,
                    BizSegment: item.BizSegment,
                    Branch: item.Branch,
                    Contacts: item.Contacts,
                    Functions: item.Functions,
                    RM: item.RM,
                    Type: item.Type,
                    Underlyings: item.Underlyings,
                    LastModifiedBy: item.LastModifiedBy,
                    LastModifiedDate: item.LastModifiedDate
                }
            }
        })
    );
        self.BarLoad(false);
    }
};

