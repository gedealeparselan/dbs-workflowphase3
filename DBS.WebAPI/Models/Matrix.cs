﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Transactions;
using DBS.Entity;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using System.Data.Entity.Validation;
using System.Diagnostics;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;

namespace DBS.WebAPI.Models
{
    #region Property
    [ModelName("Matrix")]
    public class MatrixModel
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string MailSubject { get; set; }

        public string MailBody { get; set; }

        public string IsRank { get; set;  }

        public DateTime? LastModifiedDate { get; set; }

        public string LastModifiedBy { get; set; }

        public IList<MatrixDetail> MatrixDetails { get; set; }

    }

    public class MatrixDetail
    {
        public int ID { get; set; }

        public int? FieldID { get; set; }

        public IList<MatrixSubDetail> MatrixSubDetails { get; set; }
    }

    public class MatrixSubDetail
    {
        public int ID { get; set; }

        public int? FieldValueID { get; set; }

        public int? RankID { get; set; }

        public int? FunctionRoleID { get; set; }

    }


    public class MatrixRow : MatrixModel
    {
        public int RowID { get; set; }

    }

    public class MatrixGrid : Grid
    {
        public IList<MatrixRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class MatrixFilter : Filter { }
    #endregion

    #region Interface
    public interface IMatrixRepository : IDisposable
    {
        bool GetMatrixByID(int id, ref MatrixModel matrix, ref string message);
        bool GetMatrix(ref IList<MatrixModel> Matrices, int limit, int index, ref string message);
        bool GetMatrix(ref IList<MatrixModel> Matrices, ref string message);
        bool GetMatrix(int page, int size, IList<MatrixFilter> filters, string sortColumn, string sortOrder, ref MatrixGrid matrix, ref string message);
        bool GetMatrix(MatrixFilter filter, ref IList<MatrixModel> Matrices, ref string message);
        bool GetMatrix(string key, int limit, ref IList<MatrixModel> Matrices, ref string message);
        bool AddMatrix(MatrixModel matrix, ref string message);
        bool UpdateMatrix(int id, MatrixModel matrix, ref string message);
        bool DeleteMatrix(int id, ref string message);
        bool GetParameters(ref IList<ExceptionHandlingParameter> parameters, ref string message);

    }
    #endregion

    #region Repository
    public class MatrixRepository : IMatrixRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetParameters(ref IList<ExceptionHandlingParameter> parameters, ref string message)
        {
            bool isSuccess = false;

            try
            {
                parameters = (from a in context.ExceptionHandlingParameters
                              select new ExceptionHandlingParameter
                              {
                                  Code = a.Code,
                                  Name = a.Name

                              }).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetMatrixByID(int id, ref MatrixModel matrix, ref string message)
        {
            bool isSuccess = false;

            try
            {
                matrix = (from a in context.Matrices
                          where a.IsDeleted.Equals(false) && a.MatrixID.Equals(id)
                          select new MatrixModel
                          {
                              ID = a.MatrixID,
                              Name = a.MatrixName,
                              Description = a.MatrixDescription,
                              MailSubject = a.MatrixMailSubject,
                              MailBody = a.MatrixMailBody,
                              LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                              LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,
                              MatrixDetails = context.Matrices.Where(b => b.MatrixID == a.MatrixID && b.IsDeleted.Equals(false)).Select(b => new MatrixDetail
                              {
                                  ID = b.MatrixID,
                                  FieldID = a.FieldID,
                                  MatrixSubDetails = context.MatrixDetails.Where(c => c.MatrixID == b.MatrixID && c.IsDeleted.Equals(false)).Select(c => new MatrixSubDetail
                                  {
                                      ID = c.MatrixDetailID,
                                      FieldValueID = b.FieldID == 1 ? (int)c.RankID : (int)c.FunctionRoleID
                                  }).ToList(),
                              }).ToList(),
                          }).SingleOrDefault();
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetMatrix(ref IList<MatrixModel> Matrices, ref string message)
        {
            bool isSuccess = false;

            try
            {

                using (DBSEntities context = new DBSEntities())
                {
                    var result = (from a in context.Matrices
                                  where a.IsDeleted.Equals(false)
                                  orderby new { a.MatrixName }
                                  select new MatrixModel
                                  {
                                      ID = a.MatrixID,
                                      Name = a.MatrixName,
                                      Description = a.MatrixDescription,
                                      MailSubject = a.MatrixMailSubject,
                                      MailBody = a.MatrixMailBody,
                                      LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                      LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,
                                      MatrixDetails = context.Matrices.Where(b => b.MatrixID == a.MatrixID && b.IsDeleted.Equals(false)).Select(b => new MatrixDetail
                                      {
                                          ID = b.MatrixID,
                                          FieldID = a.FieldID,
                                          MatrixSubDetails = context.MatrixDetails.Where(c => c.MatrixID == b.MatrixID && c.IsDeleted.Equals(false)).Select(c => new MatrixSubDetail
                                          {
                                              ID = c.MatrixDetailID,
                                              FieldValueID = b.FieldID == 1 ? (int)c.RankID : (int)c.FunctionRoleID
                                          }).ToList(),
                                      }).ToList(),
                                  }).ToList();
                    Matrices = result.ToList<MatrixModel>();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool GetMatrix(ref IList<MatrixModel> Matrices, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    var result = (from a in context.Matrices
                                  where a.IsDeleted.Equals(false)
                                  orderby new { a.MatrixName }
                                  select new MatrixModel
                                  {
                                      ID = a.MatrixID,
                                      Name = a.MatrixName,
                                      Description = a.MatrixDescription,
                                      MailSubject = a.MatrixMailSubject,
                                      MailBody = a.MatrixMailBody,
                                      LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                      LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,
                                      MatrixDetails = context.Matrices.Where(b => b.MatrixID == a.MatrixID  && b.IsDeleted.Equals(false)).Select(b => new MatrixDetail
                                      {
                                          ID = b.MatrixID,
                                          FieldID = a.FieldID,
                                          MatrixSubDetails = context.MatrixDetails.Where(c => c.MatrixID == b.MatrixID && c.IsDeleted.Equals(false)).Select(c => new MatrixSubDetail
                                          {
                                              ID = c.MatrixDetailID,
                                              FieldValueID = b.FieldID == 1 ? (int)c.RankID : (int)c.FunctionRoleID
                                          }).ToList(),
                                      }).ToList(),
                                  }).Skip(skip).Take(limit).ToList();
                    Matrices = result.ToList<MatrixModel>();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetMatrix(int page, int size, IList<MatrixFilter> filters, string sortColumn, string sortOrder, ref MatrixGrid matrixGrid, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                MatrixModel filter = new MatrixModel();
                if (filters != null)
                {
                    filter.Name = (string)filters.Where(a => a.Field.Equals("Name")).Select(a => a.Value).SingleOrDefault();
                    filter.Description = (string)filters.Where(a => a.Field.Equals("Description")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.Matrices
                                let isMatrixName = !string.IsNullOrEmpty(filter.Name)
                                let isMatrixDescription = !string.IsNullOrEmpty(filter.Description)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) &&
                                    (isMatrixName ? a.MatrixName.Contains(filter.Name) : true) &&
                                    (isMatrixDescription ? a.MatrixDescription.Contains(filter.Description) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new MatrixModel
                                {
                                    ID = a.MatrixID,
                                    Name = a.MatrixName,
                                    Description = a.MatrixDescription,
                                    MailSubject = a.MatrixMailSubject,                                    
                                    MailBody = a.MatrixMailBody,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    matrixGrid.Page = page;
                    matrixGrid.Size = size;
                    matrixGrid.Total = data.Count();
                    matrixGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    matrixGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new MatrixRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            Name = a.Name,
                            Description = a.Description,
                            MailSubject = a.MailSubject,
                            MailBody = a.MailBody,
                            MatrixDetails = context.Matrices.Where(b => b.MatrixID == a.ID && b.IsDeleted.Equals(false)).Select(b => new MatrixDetail
                            {
                                ID = b.MatrixID,
                                FieldID = b.FieldID,
                                MatrixSubDetails = context.MatrixDetails.Where(c => c.MatrixID == b.MatrixID && c.IsDeleted.Equals(false)).Select(c => new MatrixSubDetail
                                {
                                    ID = c.MatrixID,
                                    FieldValueID = b.FieldID == 1 ? (int)c.RankID : (int)c.FunctionRoleID
                                }).ToList(),
                            }).ToList(),
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetMatrix(MatrixFilter filter, ref IList<MatrixModel> Matrices, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetMatrix(string key, int limit, ref IList<MatrixModel> Matrices, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Matrices = (from a in context.Matrices
                            where a.IsDeleted.Equals(false) && (a.MatrixName.Contains(key) || a.MatrixDescription.Contains(key))
                            orderby new { a.MatrixName, a.MatrixDescription }
                            select new MatrixModel
                            {
                                Name = a.MatrixName,
                                Description = a.MatrixDescription,
                                MailSubject = a.MatrixMailSubject,
                                MailBody = a.MatrixMailBody,
                                MatrixDetails = context.Matrices.Where(b => b.MatrixID == a.MatrixID && b.IsDeleted.Equals(false)).Select(b => new MatrixDetail
                                {
                                    ID = b.MatrixID,
                                    FieldID = a.FieldID,
                                    MatrixSubDetails = context.MatrixDetails.Where(c => c.MatrixID == b.MatrixID && c.IsDeleted.Equals(false)).Select(c => new MatrixSubDetail
                                    {
                                        ID = c.MatrixDetailID,
                                        FieldValueID = b.FieldID == 1 ? (int)c.RankID : (int)c.FunctionRoleID
                                    }).ToList(),
                                }).ToList(),
                                LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                            }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddMatrix(MatrixModel matrix, ref string message)
        {
            bool isSuccess = false;
            string isRank;
            if (matrix != null)
            {
                try
                {
                    using (TransactionScope ts = new TransactionScope())
                    {
                        if (!context.Matrices.Where(a => a.MatrixID.Equals(matrix.ID) && a.IsDeleted.Equals(false)).Any())
                        {
                            try
                            {
                                isRank = matrix.IsRank.ToLower();
                                Entity.Matrix result = new Entity.Matrix()
                                {
                                    MatrixName = matrix.Name,
                                    MatrixDescription = matrix.Description,
                                    MatrixMailSubject = matrix.MailSubject,
                                    MatrixMailBody = matrix.MailBody,
                                    FieldID = matrix.MatrixDetails.FirstOrDefault().FieldID.Value,                                   
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().DisplayName
                                };
                                context.Matrices.Add(result);
                                context.SaveChanges();

                                foreach (var data in matrix.MatrixDetails)
                                {                                    
                                    
                                    foreach (var subDetail in data.MatrixSubDetails)
                                    {                                        
                                        context.MatrixDetails.Add(new Entity.MatrixDetail()
                                        {
                                            MatrixID = result.MatrixID,
                                            RankID = isRank == "true" ? subDetail.RankID.Value : 0,
                                            FunctionRoleID = isRank == "true" ? 0 : subDetail.FunctionRoleID.Value,
                                            IsDeleted = false,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().DisplayName
                                        });

                                        context.SaveChanges();
                                    }
                                }
                                
                                ts.Complete();
                                isSuccess = true;
                            }
                            catch (Exception ex)
                            {
                                ts.Dispose();
                                message = ex.Message;
                            }

                        }
                        else
                        {
                            message = string.Format("Matrix Approval data for Matrix Approval Name {0} is already exist.", matrix.Name);
                        }
                    }
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
            }

            return isSuccess;
        }

        public bool UpdateMatrix(int id, MatrixModel matrix, ref string message)
        {
            bool isSuccess = false;
            string isRank;
            try
            {
                Entity.Matrix data = context.Matrices.Where(a => a.MatrixID.Equals(id)).SingleOrDefault();

                using (TransactionScope ts = new TransactionScope())
                {

                    if (data != null)
                    {
                        if (context.Matrices.Where(a => a.MatrixID != matrix.ID && a.MatrixName.Equals(matrix.Name) && a.IsDeleted.Equals(false)).Count() >= 1)
                        {
                            message = string.Format("Matrix Name {0} is exist.", matrix.Name);
                        }
                        else
                        {
                            try
                            {
                                data.MatrixName = matrix.Name;
                                data.MatrixDescription = matrix.Description;
                                data.MatrixMailSubject = matrix.MailSubject;
                                data.MatrixMailBody = matrix.MailBody;
                                data.MatrixDescription = matrix.Description;
                                data.FieldID = matrix.MatrixDetails.FirstOrDefault().FieldID.Value; 
                                data.UpdateDate = DateTime.UtcNow;
                                data.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                                context.SaveChanges();
                                                                
                                DeletedDetail(id);

                                isRank = matrix.IsRank.ToLower();

                                foreach (var detail in matrix.MatrixDetails)
                                {
                                    foreach (var subDetail in detail.MatrixSubDetails)
                                    {
                                        Entity.MatrixDetail dataDetail = new Entity.MatrixDetail()
                                        {
                                            MatrixID = id,     
                                            RankID = isRank == "true" ? subDetail.RankID.Value : 0,
                                            FunctionRoleID = isRank == "true" ? 0 : subDetail.FunctionRoleID.Value,
                                            IsDeleted = false,
                                            CreateDate = data.CreateDate,
                                            CreateBy = data.CreateBy,
                                            UpdateDate = DateTime.UtcNow,
                                            UpdateBy = currentUser.GetCurrentUser().DisplayName
                                           
                                        };
                                        context.MatrixDetails.Add(dataDetail);
                                        context.SaveChanges();
                                    
                                    }
                                }
                                ts.Complete();
                                isSuccess = true;
                            }
                            catch (Exception ex)
                            {
                                ts.Dispose();
                                message = ex.Message;
                            }

                        }
                    }
                    else
                    {
                        ts.Dispose();
                        message = string.Format("Matrix Approval  data for Matrix ID {0} is does not exist.", id);
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeleteMatrix(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                var data = context.Matrices.Where(a => a.MatrixID.Equals(id)).SingleOrDefault();
                if (data != null)
                {
                    using (TransactionScope ts = new TransactionScope())
                    {
                        try
                        {
                            data.IsDeleted = true;
                            data.UpdateDate = DateTime.UtcNow;
                            data.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                            context.SaveChanges();
                            var dataDetail = context.MatrixDetails.Where(a => a.MatrixID.Equals(id)).ToList();
                            if (dataDetail != null && dataDetail.Count > 0)
                            {
                                if (dataDetail != null && dataDetail.Count > 0)
                                {
                                    dataDetail.ForEach(a =>
                                    {
                                        a.IsDeleted = true;

                                    });
                                    context.SaveChanges();
                                }
                            }
                            ts.Complete();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            ts.Dispose();
                            message = ex.Message;
                        }

                    }
                }
                else
                {
                    message = string.Format("Matrix Approval data for Matrix Approval ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        private void DeletedDetail(int id)
        {

            try
            {
                var datadetail = context.MatrixDetails.Where(a => a.MatrixID.Equals(id) && a.IsDeleted.Equals(false)).ToList();
                if (datadetail != null && datadetail.Count > 0)
                {
                    datadetail.ForEach(a =>
                    {
                        a.IsDeleted = true;
                        a.UpdateDate = DateTime.UtcNow;
                        a.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                    });
                    context.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /*private void DeletedSubDetail(int id)
        {

            try
            {
                var datasubdetail = context.MatrixSubDetails.Where(a => a.MatrixDetailID.Equals(id) && a.IsDeleted.Equals(false)).ToList();
                if (datasubdetail != null && datasubdetail.Count > 0)
                {
                    datasubdetail.ForEach(a =>
                    {
                        a.IsDeleted = true;
                        a.UpdateDate = DateTime.UtcNow;
                        a.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                    });
                    context.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }*/

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}