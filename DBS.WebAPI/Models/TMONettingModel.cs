﻿using DBS.Entity;
using DBS.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.EntityClient;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Transactions;

namespace DBS.WebAPI.Models
{
    #region Property
    public class TMONettingModel
    {
        public bool? IsDraft { get; set; }
        public CustomerModel Customer { get; set; }
        public ProductModel Product { get; set; }
        public NettingPurposeModel NettingPurpose { get; set; }
        public string TZReference { get; set; }

        public string SwapNumber { get; set; }

        public DateTime? ActualDateStatementLetter { get; set; }

        public DateTime? ActualDateUnderlying { get; set; }

        public DateTime? ExpectedDateStatementLetter { get; set; }

        public DateTime? ExpectedDateUnderlying { get; set; }

        public string Remarks { get; set; }

        public string ApplicationID { get; set; }

        public List<NettingDealNumberModel> NettingDealNumber { get; set; }
        public List<UtilizeUnderlyingModel> Underlyings { get; set; }

        public long ID { get; set; }

        public Guid? WorkflowInstanceID { get; set; }

        public long? ApproverID { get; set; }

        public DateTime CreatedDate { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime LastModifiedDate { get; set; }

        public List<TransactionDocumentNettingModel> Documents { get; set; }
    }
    public class NettingDealNumberModel
    {
        public string DFNumber { get; set; }
        public string SwapDF { get; set; }
    }
    public class TMONettingDetailModel
    {
        public TMONettingModel Transaction { get; set; }
        public IList<TransactionTimelineModel> Timelines { get; set; }
        public TransactionTMOCheckerDataModel Checker { get; set; }
        public IList<VerifyModel> Verify { get; set; }
    }

    public class TransactionDocumentNettingModel
    {
        /// <summary>
        /// Transaction Document ID
        /// </summary>
        public long ID { get; set; }

        /// <summary>
        /// Document Type details
        /// </summary>
        public DocumentTypeModel Type { get; set; }

        /// <summary>
        /// Document Type details
        /// </summary>
        public DocumentPurposeModel Purpose { get; set; }

        /// <summary>
        /// Document filename
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// DocumentPath
        /// </summary>
        public string DocumentPath { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }

        public bool? IsDormant { get; set; }


        public bool IsNewDocument { get; set; }
    }
    #endregion
    #region Interface
    public interface ITMONettingRepository : IDisposable
    {
        bool AddTMONetting(TMONettingModel transaction, ref long transactionID, ref string ApplicationID, ref string message);
        bool UpdateTMONetting(Guid workflowInstanceID, long approverID, TMONettingModel transaction, ref string message);
        bool AddOrReviseTMONettingChecker(Guid workflowInstanceID, long approverID, string typeapproval, TMONettingDetailModel data, ref string message);
        bool GetTransactionTimeline(Guid workflowInstanceID, ref IList<TransactionTimelineModel> timelines, ref string message);
        bool GetTMONettingDetails(Guid instanceID, ref TMONettingModel transaction, ref string message);
        bool GetTMONettingCheckerDetails(Guid workflowInstanceID, long approverID, ref TMONettingDetailModel output, ref string message);
    }
    #endregion
    #region Repo
    public class TMONettingRepository : ITMONettingRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();
        private bool disposed = false;

        public bool AddTMONetting(TMONettingModel transaction, ref long transactionID, ref string ApplicationID, ref string message)
        {
            bool IsSuccess = false;
            long TrIDInserted = 0;
            bool IsException = false;
            EmployeeModelRepository repoEmployee = new EmployeeModelRepository();

            BranchModel dataBranch = new BranchModel();
            using (DBSEntities context_ts = new DBSEntities())
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    try
                    {
                        if (transaction.IsDraft.Value)
                        {
                        }
                        else
                        {
                            #region Submit Transaction
                            DBS.Entity.TMONetting data = new DBS.Entity.TMONetting()
                            {
                                CIF = transaction.Customer.CIF,
                                ProductID = transaction.Product.ID,
                                ApplicationID = transaction.ApplicationID,
                                NettingPurposeID = transaction.NettingPurpose.ID,
                                TZRef = transaction.TZReference,
                                SwapDealNumber = transaction.SwapNumber,
                                ActualSubmissionDateSL = transaction.ActualDateStatementLetter,
                                ActualSubmissionDateUnd = transaction.ActualDateUnderlying,
                                ExpectedSubmissionDateSL = transaction.ExpectedDateStatementLetter,
                                ExpectedSubmissionDateUnd = transaction.ExpectedDateUnderlying,
                                IsCanceled = false,
                                IsDraft = transaction.IsDraft,
                                IsTMO = true,
                                Remarks = transaction.Remarks,
                                CreatedDate = DateTime.UtcNow,
                                CreatedBy = currentUser.GetCurrentUser().LoginName
                            };
                            context_ts.TMONettings.Add(data);
                            context_ts.SaveChanges();
                            TrIDInserted = data.TMONettingID;

                            foreach (var item in transaction.NettingDealNumber)
                            {
                                Entity.TMONettingDetail tmoNettingDetail = new Entity.TMONettingDetail()
                                {
                                    TMONettingID = TrIDInserted,
                                    TZRef = item.DFNumber,
                                    SwapDealNumber = item.SwapDF,
                                    IsCanceled = false,
                                    CreatedDate = DateTime.UtcNow,
                                    CreatedBy = currentUser.GetCurrentUser().LoginName
                                };

                                context_ts.TMONettingDetails.Add(tmoNettingDetail);
                            }

                            foreach (var item in transaction.Documents)
                            {
                                Entity.TMONettingDocument tmoNettingDocument = new Entity.TMONettingDocument()
                                {
                                    TMONettingID = TrIDInserted,
                                    PurposeID = item.Purpose.ID,
                                    DocTypeID = item.Type.ID,
                                    Filename = item.FileName,
                                    DocumentPath = item.DocumentPath,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                };

                                context_ts.TMONettingDocuments.Add(tmoNettingDocument);
                            }

                            foreach (var item in transaction.Underlyings)
                            {
                                Entity.CustomerUnderlying customerUnderlying = context.CustomerUnderlyings.Where(a => a.UnderlyingID.Equals(item.ID)).SingleOrDefault();
                                if (customerUnderlying != null)
                                {
                                    Entity.TMONettingUnderlying tmoNettingUnderlying = new Entity.TMONettingUnderlying()
                                    {
                                        TMONettingID = TrIDInserted,
                                        UnderlyingID = item.ID,
                                        Amount = (item.USDAmount * customerUnderlying.Rate.Value) / Convert.ToDecimal((customerUnderlying.Amount * customerUnderlying.Rate.Value / customerUnderlying.AmountUSD.Value).ToString("#.##")),
                                        AvailableAmount = item.USDAmount,
                                        IsDeleted = false,
                                        CreateDate = DateTime.UtcNow,
                                        CreateBy = currentUser.GetCurrentUser().LoginName
                                    };
                                    context_ts.TMONettingUnderlyings.Add(tmoNettingUnderlying);
                                }
                            }

                            context_ts.SaveChanges();

                            ts.Complete();
                            #endregion
                        }
                    }
                    catch (Exception ex)
                    {
                        ts.Dispose();
                        message = ex.Message;
                    }
                }
            }
            if (!transaction.IsDraft.Value && !IsException)
            {
                using (DBSEntities context = new DBSEntities())
                {
                    var getTR = (from TR in context.TMONettings
                                 where TR.TMONettingID == TrIDInserted
                                 select TR).SingleOrDefault();
                    ApplicationID = getTR.ApplicationID;
                    IsSuccess = true;
                }
            }
            return IsSuccess;
        }
        public bool UpdateTMONetting(Guid workflowInstanceID, long approverID, TMONettingModel transaction, ref string message)
        {
            bool IsSuccess = false;

            using (DBSEntities context_ts = new DBSEntities())
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    try
                    {
                        #region Submit Transaction
                        Entity.TMONetting TMONettingData = context_ts.TMONettings.Where(a => a.TMONettingID.Equals(transaction.ID)).SingleOrDefault();
                        if (TMONettingData != null)
                        {
                            TMONettingData.CIF = transaction.Customer.CIF;
                            TMONettingData.ProductID = transaction.Product.ID;
                            TMONettingData.ApplicationID = transaction.ApplicationID;
                            TMONettingData.NettingPurposeID = transaction.NettingPurpose.ID;
                            TMONettingData.TZRef = transaction.TZReference;
                            TMONettingData.SwapDealNumber = transaction.SwapNumber;
                            TMONettingData.ActualSubmissionDateSL = transaction.ActualDateStatementLetter;
                            TMONettingData.ActualSubmissionDateUnd = transaction.ActualDateUnderlying;
                            TMONettingData.ExpectedSubmissionDateSL = transaction.ExpectedDateStatementLetter;
                            TMONettingData.ExpectedSubmissionDateUnd = transaction.ExpectedDateUnderlying;
                            TMONettingData.IsCanceled = false;
                            TMONettingData.IsDraft = transaction.IsDraft;
                            TMONettingData.IsTMO = true;
                            TMONettingData.Remarks = transaction.Remarks;
                            TMONettingData.UpdateDate = DateTime.UtcNow;
                            TMONettingData.UpdateBy = currentUser.GetCurrentUser().LoginName;
                        }
                        context_ts.SaveChanges();

                        List<Entity.TMONettingDetail> TMD = context_ts.TMONettingDetails.Where(a => a.TMONettingID.Equals(transaction.ID)).ToList();
                        if (TMD != null)
                        {
                            foreach (var itemtmd in TMD)
                            {
                                context_ts.TMONettingDetails.Remove(itemtmd);
                            }
                            context_ts.SaveChanges();
                            if (transaction.NettingDealNumber != null)
                            {
                                if (transaction.NettingDealNumber.Count > 0)
                                {
                                    foreach (var itemndn in transaction.NettingDealNumber)
                                    {
                                        Entity.TMONettingDetail tmoNettingDetail = new Entity.TMONettingDetail()
                                        {
                                            TMONettingID = transaction.ID,
                                            TZRef = itemndn.DFNumber,
                                            SwapDealNumber = itemndn.SwapDF,
                                            IsCanceled = false,
                                            CreatedDate = DateTime.UtcNow,
                                            CreatedBy = currentUser.GetCurrentUser().LoginName
                                        };
                                        context_ts.TMONettingDetails.Add(tmoNettingDetail);
                                    }
                                    context_ts.SaveChanges();
                                }
                            }
                        }

                        List<Entity.TMONettingDocument> TMDoc = context_ts.TMONettingDocuments.Where(a => a.TMONettingID.Equals(transaction.ID)).ToList();
                        if (TMDoc != null)
                        {
                            foreach (var itemtmdoc in TMDoc)
                            {
                                context_ts.TMONettingDocuments.Remove(itemtmdoc);
                            }
                            context.SaveChanges();
                            if (transaction.Documents != null)
                            {
                                if (transaction.Documents.Count > 0)
                                {
                                    foreach (var itemdoc in transaction.Documents)
                                    {
                                        Entity.TMONettingDocument tmoNettingDocument = new Entity.TMONettingDocument()
                                        {
                                            TMONettingID = transaction.ID,
                                            PurposeID = itemdoc.Purpose.ID,
                                            DocTypeID = itemdoc.Type.ID,
                                            Filename = itemdoc.FileName,
                                            DocumentPath = itemdoc.DocumentPath,
                                            IsDeleted = false,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().LoginName
                                        };
                                        context_ts.TMONettingDocuments.Add(tmoNettingDocument);
                                    }
                                    context_ts.SaveChanges();
                                }
                            }
                        }
                        List<Entity.TMONettingUnderlying> TMUnd = context_ts.TMONettingUnderlyings.Where(a => a.TMONettingID.Equals(transaction.ID)).ToList();
                        if (TMUnd != null)
                        {
                            foreach (var itemtmund in TMUnd)
                            {
                                context_ts.TMONettingUnderlyings.Remove(itemtmund);
                            }
                            context_ts.SaveChanges();

                            if (transaction.Underlyings != null)
                            {
                                if (transaction.Underlyings.Count > 0)
                                {
                                    foreach (var itemtmund in transaction.Underlyings)
                                    {
                                        Entity.TMONettingUnderlying tmoNettingUnderlying = new Entity.TMONettingUnderlying()
                                        {
                                            TMONettingID = transaction.ID,
                                            UnderlyingID = itemtmund.ID,
                                            Amount = itemtmund.USDAmount,
                                            AvailableAmount = itemtmund.USDAmount,
                                            IsDeleted = false,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().LoginName
                                        };
                                        context_ts.TMONettingUnderlyings.Add(tmoNettingUnderlying);
                                    }
                                    context_ts.SaveChanges();
                                }
                            }
                        }
                        IsSuccess = true;
                        ts.Complete();
                        #endregion
                    }
                    catch (Exception ex)
                    {
                        ts.Dispose();
                        message = ex.Message;
                    }
                }
            }
            return IsSuccess;
        }
        public bool AddOrReviseTMONettingChecker(Guid workflowInstanceID, long approverID, string typeapproval, TMONettingDetailModel data, ref string message)
        {
            bool IsSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    if (data != null)
                    {
                        List<Entity.TMONettingChecker> checkerdata = context.TMONettingCheckers.Where(a => a.TMONettingID.Equals(data.Transaction.ID)).ToList();
                        if (checkerdata != null)
                        {
                            foreach (var item in checkerdata)
                            {
                                context.TMONettingCheckers.Remove(item);
                            }
                            context.SaveChanges();
                        }

                        foreach (var item in data.Verify)
                        {
                            context.TMONettingCheckers.Add(new TMONettingChecker()
                            {
                                ApproverID = approverID,
                                TMONettingID = data.Transaction.ID,
                                TransactionColumnID = item.ID,
                                IsVerified = item.IsVerified,
                                IsDeleted = false,
                                CreateDate = DateTime.UtcNow,
                                CreateBy = currentUser.GetCurrentUser().LoginName
                            });
                            context.SaveChanges();
                        }
                        if (typeapproval == "6")
                        {
                            #region Rollback
                            List<long> deallist = new List<long>();

                            Entity.TransactionDeal dataDeal = context.TransactionDeals.Where(a => a.TZRef.Equals(data.Transaction.TZReference)).FirstOrDefault();
                            if (dataDeal != null)
                            {
                                deallist.Add(dataDeal.TransactionDealID);
                            }
                            List<long> DetailNetting = (from a in context.TMONettingDetails
                                                        join b in context.TransactionDeals on a.TZRef equals b.TZRef
                                                        where a.TMONettingID.Equals(data.Transaction.ID) && a.IsCanceled.Value.Equals(false)
                                                        select b.TransactionDealID).ToList();

                            if (DetailNetting != null)
                            {
                                foreach (long dns in DetailNetting)
                                {
                                    deallist.Add(dns);
                                }
                            }
                            if (deallist != null && deallist.Count > 0)
                            {
                                foreach (long iddeal in deallist)
                                {
                                    List<Entity.TransactionDealUnderlying> items = context.TransactionDealUnderlyings.Where(a => a.TransactionDealID.Equals(iddeal) && a.IsDeleted.Equals(false)).ToList();
                                    if (items != null && items.Count > 0)
                                    {
                                        foreach (Entity.TransactionDealUnderlying datas in items)
                                        {
                                            // set update customer underlying
                                            var dataCustomerUnderlying = context.CustomerUnderlyings.Where(a => a.UnderlyingID.Equals(datas.UnderlyingID)).SingleOrDefault();
                                            dataCustomerUnderlying.AvailableAmountUSD += datas.Amount;
                                            dataCustomerUnderlying.IsUtilize = dataCustomerUnderlying.AmountUSD == dataCustomerUnderlying.AvailableAmountUSD ? false : true;
                                            dataCustomerUnderlying.UpdateDate = DateTime.UtcNow;
                                            dataCustomerUnderlying.UpdateBy = currentUser.GetCurrentUser().LoginName;
                                            context.SaveChanges();
                                            // set update transaction Deal underlying
                                            var dataTransactionDealUnderlying = context.TransactionDealUnderlyings.Where(a => a.TransactionDealUnderlyingID.Equals(datas.TransactionDealUnderlyingID)).SingleOrDefault();
                                            dataTransactionDealUnderlying.IsDeleted = true;
                                            dataTransactionDealUnderlying.UpdateDate = DateTime.UtcNow;
                                            dataTransactionDealUnderlying.UpdateBy = currentUser.GetCurrentUser().LoginName;
                                            context.SaveChanges();
                                        }
                                    }
                                }
                            }
                            #endregion
                            #region Tambah Agung
                            List<Entity.TMONettingUnderlying> tmonettingUnderlying = context.TMONettingUnderlyings.Where(a => a.TMONettingID.Equals(data.Transaction.ID)).ToList();
                            if (tmonettingUnderlying != null && tmonettingUnderlying.Count > 0)
                            {
                                Entity.TransactionDeal dealID = context.TransactionDeals.Where(a => a.TZRef.Equals(data.Transaction.TZReference)).FirstOrDefault();

                                foreach (Entity.TMONettingUnderlying underlyingItem in tmonettingUnderlying)
                                {
                                    Entity.CustomerUnderlying customerUnderlying = context.CustomerUnderlyings.Where(a => a.UnderlyingID.Equals(underlyingItem.UnderlyingID)).SingleOrDefault();
                                    if (customerUnderlying != null)
                                    {
                                        decimal tempAvailableAmount = (decimal)customerUnderlying.AvailableAmountUSD;
                                        customerUnderlying.IsUtilize = true;
                                        customerUnderlying.UpdateDate = DateTime.UtcNow;
                                        customerUnderlying.AvailableAmountUSD = tempAvailableAmount - underlyingItem.Amount;
                                        customerUnderlying.UpdateBy = currentUser.GetCurrentUser().LoginName;
                                        context.SaveChanges();

                                        Entity.TransactionDealUnderlying Underlying = new Entity.TransactionDealUnderlying()
                                        {
                                            TransactionDealID = dealID.TransactionDealID,
                                            UnderlyingID = underlyingItem.UnderlyingID,
                                            Amount = underlyingItem.Amount,
                                            AvailableAmount = underlyingItem.AvailableAmount,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().LoginName
                                        };
                                        context.TransactionDealUnderlyings.Add(Underlying);
                                        context.SaveChanges();

                                    }
                                }
                            }
                            #endregion
                        }
                    }

                    ts.Complete();

                    IsSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }

            return IsSuccess;
        }
        public bool GetTransactionTimeline(Guid workflowInstanceID, ref IList<TransactionTimelineModel> timelines, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                timelines = (from a in context.SP_GetNintexTaskTimeline(workflowInstanceID)
                             select new TransactionTimelineModel
                             {
                                 ApproverID = a.WorkflowApproverID,
                                 Activity = a.ActivityTitle,
                                 Time = a.ActivityTime,
                                 UserOrGroup = a.UserOrGroup,
                                 IsGroup = a.IsSPGroup.Value,
                                 Outcome = a.Outcome,
                                 DisplayName = a.DisplayName,
                                 Comment = a.Comment
                             }).ToList();

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }
        public bool GetTMONettingDetails(Guid instanceID, ref TMONettingModel transaction, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                context.TMONettings.AsNoTracking();
                Entity.TMONetting datatransaction = context.TMONettings.Where(a => a.WorkflowInstanceID.Value.Equals(instanceID)).SingleOrDefault();
                transaction.ID = datatransaction.TMONettingID;
                transaction.WorkflowInstanceID = datatransaction.WorkflowInstanceID.Value;
                transaction.ApplicationID = datatransaction.ApplicationID;
                transaction.ApproverID = datatransaction.ApproverID != null ? datatransaction.ApproverID : 0;
                transaction.TZReference = datatransaction.TZRef;
                transaction.SwapNumber = datatransaction.SwapDealNumber;
                transaction.NettingDealNumber = context.TMONettingDetails.Where(tmd => tmd.IsCanceled.Value.Equals(false) && tmd.TMONettingID.Equals(datatransaction.TMONettingID)).Select(tmd => new NettingDealNumberModel()
                {
                    DFNumber = tmd.TZRef,
                    SwapDF = tmd.SwapDealNumber
                }).ToList();
                transaction.ActualDateStatementLetter = datatransaction.ActualSubmissionDateSL;
                transaction.ActualDateUnderlying = datatransaction.ActualSubmissionDateUnd;
                transaction.ExpectedDateStatementLetter = datatransaction.ExpectedSubmissionDateSL;
                transaction.ExpectedDateUnderlying = datatransaction.ExpectedSubmissionDateUnd;
                transaction.NettingPurpose = new NettingPurposeModel()
                {
                    ID = datatransaction.NettingPurposeID
                };
                transaction.Product = context.Products.Where(x => x.ProductID.Equals(datatransaction.ProductID)).Select(x => new ProductModel()
                {
                    ID = x.ProductID,
                    Code = x.ProductCode,
                    Name = x.ProductName,
                    WorkflowID = x.ProductWorkflow.WorkflowID,
                    Workflow = x.ProductWorkflow.WorkflowName,
                    LastModifiedBy = x.UpdateDate.HasValue ? x.CreateBy : x.UpdateBy,
                    LastModifiedDate = x.UpdateDate.HasValue ? x.CreateDate : x.UpdateDate.Value
                }).SingleOrDefault();
                transaction.IsDraft = datatransaction.IsDraft;
                transaction.Documents = context.TMONettingDocuments.Where(x => x.IsDeleted.Equals(false) && x.TMONettingID.Equals(datatransaction.TMONettingID)).Select(x => new TransactionDocumentNettingModel()
                {
                    ID = x.TransactionDocumentID,
                    Type = new DocumentTypeModel()
                    {
                        ID = x.DocumentType.DocTypeID,
                        Name = x.DocumentType.DocTypeName,
                        Description = x.DocumentType.DocTypeDescription,
                        LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                        LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                    },
                    Purpose = new DocumentPurposeModel()
                    {
                        ID = x.DocumentPurpose.PurposeID,
                        Name = x.DocumentPurpose.PurposeName,
                        Description = x.DocumentPurpose.PurposeDescription,
                        LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                        LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                    },
                    FileName = x.Filename,
                    DocumentPath = x.DocumentPath,
                    LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                    LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value,
                    IsDormant = x.IsDormant == null ? false : x.IsDormant,
                    IsNewDocument = false
                }).ToList();
                transaction.CreatedDate = datatransaction.CreatedDate;
                transaction.LastModifiedBy = datatransaction.UpdateDate.HasValue ? datatransaction.CreatedBy : datatransaction.UpdateBy;
                transaction.LastModifiedDate = datatransaction.UpdateDate.HasValue ? datatransaction.CreatedDate : datatransaction.UpdateDate.Value;
                transaction.Remarks = string.IsNullOrEmpty(datatransaction.Remarks) ? "-" : datatransaction.Remarks;

                ICustomerRepository customerRepo = new CustomerRepository();
                CustomerModel customer = new CustomerModel();

                if (customerRepo.GetCustomerByTMONetting(datatransaction.TMONettingID, ref customer, ref message))
                {
                    transaction.Customer = customer;
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }

            return IsSuccess;
        }
        public bool GetTMONettingCheckerDetails(Guid workflowInstanceID, long approverID, ref TMONettingDetailModel output, ref string message)
        {
            bool IsSuccess = false;

            long TMONettingID = context.TMONettings
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.TMONettingID)
                        .SingleOrDefault();

            try
            {
                List<Entity.TMONettingChecker> Verifys = context.TMONettingCheckers
                        .Where(a => a.TMONettingID.Equals(TMONettingID) && a.IsDeleted.Equals(false)).ToList();

                if (Verifys.Count > 0)
                {
                    output = (from a in context.TMONettings.AsNoTracking()
                              where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                              select new TMONettingDetailModel
                              {
                                  Verify = context.TMONettingCheckers
                                    .Where(x => x.TMONettingID.Equals(TMONettingID) && x.IsDeleted.Equals(false))
                                    .Select(x => new VerifyModel()
                                    {
                                        ID = x.TransactionColumnID,
                                        Name = x.TransactionColumn.ColumnName,
                                        IsVerified = x.IsVerified
                                    }).ToList()
                              }).SingleOrDefault();
                }
                else
                {
                    output = (from a in context.TMONettings.AsNoTracking()
                              where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                              select new TMONettingDetailModel
                              {
                                  Verify = context.TransactionColumns
                                    .Where(x => x.IsPPUCheckerNetting.Value.Equals(true) && x.IsDeleted.Equals(false))
                                    .Select(x => new VerifyModel()
                                    {
                                        ID = x.TransactionColumnID,
                                        Name = x.ColumnName,
                                        IsVerified = false
                                    }).ToList()
                              }).SingleOrDefault();
                }
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}