//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DBS.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class WorkflowHistory
    {
        public long HistoryID { get; set; }
        public System.Guid SPWebID { get; set; }
        public System.Guid SPSiteID { get; set; }
        public System.Guid SPListID { get; set; }
        public int SPListItemID { get; set; }
        public System.Guid SPTaskListID { get; set; }
        public int SPTaskListItemID { get; set; }
        public System.Guid WorkflowID { get; set; }
        public string WorkflowName { get; set; }
        public System.Guid WorkflowInstanceID { get; set; }
        public string WorkflowInitiator { get; set; }
        public System.DateTime WorkflowStartTime { get; set; }
        public int WorkflowState { get; set; }
        public string WorkflowStateDescription { get; set; }
        public long WorkflowApproverID { get; set; }
        public int WorkflowTaskType { get; set; }
        public string WorkflowTaskTypeDescription { get; set; }
        public string WorkflowTaskTitle { get; set; }
        public string WorkflowTaskActivityTitle { get; set; }
        public string WorkflowTaskUser { get; set; }
        public bool WorkflowTaskIsSPGroup { get; set; }
        public System.DateTime WorkflowTaskEntryTime { get; set; }
        public Nullable<System.DateTime> WorkflowTaskExitTime { get; set; }
        public int WorkflowOutcome { get; set; }
        public string WorkflowOutcomeDescription { get; set; }
        public Nullable<int> WorkflowCustomOutcome { get; set; }
        public string WorkflowCustomOutcomeDescription { get; set; }
        public string WorkflowComments { get; set; }
    }
}
