﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Models
{
    #region Property
    [ModelName("DocumenType")]
    public class DocumentTypeModel
    {
        /// <summary>
        /// Document Type ID.
        /// </summary>
        public int ID { get; set; }
        public int Type { get; set; } //add azam
        /// <summary>
        /// Document Type Name
        /// </summary>
        [MaxLength(50, ErrorMessage = "DocType Name is must be in {1} characters.")]
        public string Name { get; set; }

        /// <summary>
        /// Document Type Description.
        /// </summary>
        [MaxLength(255, ErrorMessage = "Maximum length is {1} characters.")]
        public string Description { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }
    }

    public class DocumentTypeRow : DocumentTypeModel
    {
        public int RowID { get; set; }

    }

    public class DocumentTypeGrid : Grid
    {
        public IList<DocumentTypeRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class DocumentTypeFilter : Filter { }
    #endregion

    #region Interface
    public interface IDocumentTypeRepository : IDisposable
    {
        bool GetDocTypeByID(int id, ref DocumentTypeModel docType, ref string message);
        bool GetDocType(ref IList<DocumentTypeModel> docTypes, int limit, int index, ref string message);
        bool GetDocType(int page, int size, IList<DocumentTypeFilter> filters, string sortColumn, string sortOrder, ref DocumentTypeGrid docType, ref string message);
        bool GetDocType(DocumentTypeFilter filter, ref IList<DocumentTypeModel> currencies, ref string message);
        bool GetDocType(string key, int limit, ref IList<DocumentTypeModel> currencies, ref string message);
        bool AddDocType(DocumentTypeModel docType, ref string message);
        bool UpdateDocType(int id, DocumentTypeModel docType, ref string message);
        bool DeleteDocType(int id, ref string message);

        bool GetDocType(ref IList<DocumentTypeModel> documentType, ref string message);
    }
    #endregion

    #region Repository
    public class DocumentTypeRepository : IDocumentTypeRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetDocTypeByID(int id, ref DocumentTypeModel docType, ref string message)
        {
            bool isSuccess = false;

            try
            {
                docType = (from a in context.DocumentTypes
                           where a.IsDeleted.Equals(false) && a.DocTypeID.Equals(id)
                           select new DocumentTypeModel
                           {
                               ID = a.DocTypeID,
                               Name = a.DocTypeName,
                               Description = a.DocTypeDescription,
                               LastModifiedDate = a.CreateDate,
                               LastModifiedBy = a.CreateBy
                           }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetDocType(ref IList<DocumentTypeModel> docType, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    docType = (from a in context.DocumentTypes
                               where a.IsDeleted.Equals(false)
                               orderby new { a.DocTypeName, a.DocTypeDescription }
                               select new DocumentTypeModel
                               {
                                   ID = a.DocTypeID,
                                   Name = a.DocTypeName,
                                   Description = a.DocTypeDescription,
                                   LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                   LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                               }).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetDocType(ref IList<DocumentTypeModel> docType, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    docType = (from a in context.DocumentTypes
                               where a.IsDeleted.Equals(false)
                               orderby new { a.DocTypeName, a.DocTypeDescription }
                               select new DocumentTypeModel
                               {
                                   ID = a.DocTypeID,
                                   Name = a.DocTypeName,
                                   Description = a.DocTypeDescription,
                                   LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                   LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                               }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetDocType(int page, int size, IList<DocumentTypeFilter> filters, string sortColumn, string sortOrder, ref DocumentTypeGrid docTypeGrid, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                DocumentTypeModel filter = new DocumentTypeModel();
                if (filters != null)
                {
                    filter.Name = filters.Where(a => a.Field.Equals("DocTypeName")).Select(a => a.Value).SingleOrDefault();
                    filter.Description = filters.Where(a => a.Field.Equals("DocTypeDescription")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse(filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.DocumentTypes
                                let isName = !string.IsNullOrEmpty(filter.Name)
                                let isDesc = !string.IsNullOrEmpty(filter.Description)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) &&
                                    (isName ? a.DocTypeName.Contains(filter.Name) : true) &&
                                    (isDesc ? a.DocTypeDescription.Contains(filter.Description) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new DocumentTypeModel
                                {
                                    ID = a.DocTypeID,
                                    Name = a.DocTypeName,
                                    Description = a.DocTypeDescription,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    docTypeGrid.Page = page;
                    docTypeGrid.Size = size;
                    docTypeGrid.Total = data.Count();
                    docTypeGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    docTypeGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new DocumentTypeRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            Name = a.Name,
                            Description = a.Description,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetDocType(DocumentTypeFilter filter, ref IList<DocumentTypeModel> docType, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetDocType(string key, int limit, ref IList<DocumentTypeModel> docType, ref string message)
        {
            bool isSuccess = false;

            try
            {
                docType = (from a in context.DocumentTypes
                           where a.IsDeleted.Equals(false) &&
                               a.DocTypeName.Contains(key) || a.DocTypeDescription.Contains(key)
                           orderby new { a.DocTypeName, a.DocTypeDescription }
                           select new DocumentTypeModel
                           {
                               Name = a.DocTypeName,
                               Description = a.DocTypeDescription
                           }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddDocType(DocumentTypeModel docType, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (!context.DocumentTypes.Where(a => a.DocTypeName.Equals(docType.Name) && a.IsDeleted.Equals(false)).Any())
                {
                    context.DocumentTypes.Add(new Entity.DocumentType()
                    {
                        DocTypeName = docType.Name,
                        DocTypeDescription = docType.Description,
                        IsDeleted = false,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    });

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Document Type data for Document Name {0} is already exist.", docType.Name);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateDocType(int id, DocumentTypeModel docType, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.DocumentType data = context.DocumentTypes.Where(a => a.DocTypeID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    if (context.DocumentTypes.Where(a => a.DocTypeID != docType.ID && a.DocTypeName.Equals(docType.Name) && a.IsDeleted.Equals(false)).Count() >= 1)
                    {
                        message = string.Format("Document Type Name {0} is exist.", docType.Name);
                    }
                    else
                    {
                        data.DocTypeName = docType.Name;
                        data.DocTypeDescription = docType.Description;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        context.SaveChanges();

                        isSuccess = true;
                    }
                }
                else
                {
                    message = string.Format("Document type data for Document Type ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeleteDocType(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.DocumentType data = context.DocumentTypes.Where(a => a.DocTypeID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Document type data for Document type ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}