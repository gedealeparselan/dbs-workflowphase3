﻿using DBS.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace DBS.WebAPI.Controllers
{
    public enum ChangeRMType
    {
        ChangeRMSolID = 1,
        SaveChangeRM = 2
    }
    public class ChangeRMTaskController : ApiController
    {
        private string aplicationid = string.Empty;
        private string message = string.Empty;

        private readonly IChangeRMTask repository = null;
        public ChangeRMTaskController()
        {
            this.repository = new ChangeRMModelModal();
        }
        public ChangeRMTaskController(IChangeRMTask repository)
        {
            this.repository = repository;
        }

        public HttpResponseMessage Get(string logname, int taskType, string taskDate)
        {
            HttpResponseMessage result = null;
            DateTime _date;
            if (!DateTime.TryParse(taskDate, out _date))
            {
                _date = DateTime.Now;
            }
            switch (taskType)
            {
                #region OLD
                /*
                case (int)CSOTaskType.ChangeRMSolID:
                    IList<ChangeRMTransactionModel> output4 = new List<ChangeRMTransactionModel>();
                    if (!repository.GetRM(ref output4, _date, ref message))
                    {
                        result = Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
                    }
                    else
                    {
                        result = Request.CreateResponse<IList<ChangeRMTransactionModel>>(HttpStatusCode.OK, output4);
                    }
                    break;
                     * */
                #endregion
                case (int)ChangeRMType.ChangeRMSolID:
                    IList<ChangeRM> output4 = new List<ChangeRM>();
                    if (!repository.GetRM(ref output4, _date, ref message))
                    {
                        result = Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
                    }
                    else
                    {
                        result = Request.CreateResponse<IList<ChangeRM>>(HttpStatusCode.OK, output4);
                    }
                    break;
                default:
                    break;
            }
            return result;
        }
        [ResponseType(typeof(string))]
        [Route("api/ChangeRMTask/SaveExcel")]
        [AcceptVerbs("POST")]
        //public HttpResponseMessage AddTransaction(IList<ChangeRMTransactionModel> TransactionModel)
        public HttpResponseMessage AddTransaction(TransactionChangeRM TransactionModel)
        {
            long transactionid = 0;

            if (!repository.AddTransaction(TransactionModel, ref transactionid, ref  aplicationid, ref  message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                //return Request.CreateResponse<IList<ChangeRMTransactionModel>>(HttpStatusCode.OK, TransactionModel);
                return Request.CreateResponse(HttpStatusCode.Created, new
                {
                    transactionid = transactionid,
                    aplicationid = aplicationid,
                    Message = "Workflow ChangeRM Create SUccess"
                });
            }
        }

        [ResponseType(typeof(string))]
        [Route("api/ChangeRMTask/SaveExcelDraft")]
        [AcceptVerbs("POST")]

        #region OLD1
        //public HttpResponseMessage AddTransactionDraft(IList<ChangeRMTransactionModel> TransactionModel)
        //{
        //    if (!repository.AddTransactionDraft(TransactionModel))
        //    {
        //        return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
        //    }
        //    else
        //    {
        //        return Request.CreateResponse<IList<ChangeRMTransactionModel>>(HttpStatusCode.OK, TransactionModel);
        //    }
        //}
        #endregion
        public HttpResponseMessage AddTransactionDraft(TransactionChangeRM TransactionModel)
        {
            if (!repository.AddTransactionDraft(TransactionModel))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<TransactionChangeRM>(HttpStatusCode.OK, TransactionModel);
            }
        }
        
    }
}