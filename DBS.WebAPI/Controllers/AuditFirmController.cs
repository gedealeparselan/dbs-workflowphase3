﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class AuditFirmController : ApiController
    {
        private string message = string.Empty;

        private readonly IAuditFirmRepository repository = null;

        public AuditFirmController()
        {
            this.repository = new AuditFirmRepository();
        }

        public AuditFirmController(IAuditFirmRepository repository)
        {
            this.repository = repository;
        }
        
        [ResponseType(typeof(IList<AuditFirmModel>))]
        public HttpResponseMessage Get()
        {
            IList<AuditFirmModel> output = new List<AuditFirmModel>();

            if (!repository.GetAuditFirm(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<AuditFirmModel>>(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(IList<AuditFirmModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<AuditFirmModel> output = new List<AuditFirmModel>();

            if (!repository.GetAuditFirm(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<AuditFirmModel>>(HttpStatusCode.OK, output);
            }
        }
    
        [ResponseType(typeof(AuditFirmGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<AuditFirmFilter> filters)
        {
            AuditFirmGrid output = new AuditFirmGrid();

            if (!repository.GetAuditFirm(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "CIF" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<AuditFirmGrid>(HttpStatusCode.OK, output);
            }
        }

        
        [ResponseType(typeof(IList<AuditFirmModel>))]
        [HttpGet]
        [Route("api/AuditFirm/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<AuditFirmModel> output = new List<AuditFirmModel>();

            if (!repository.GetAuditFirm(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<AuditFirmModel>>(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(AuditFirmModel))]
        [Route("api/AuditFirm/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            AuditFirmModel output = new AuditFirmModel();

            if (!repository.GetAuditFirmByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<AuditFirmModel>(HttpStatusCode.OK, output);
            }
        }

        [HttpPost]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(AuditFirmModel auditFirm)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddAuditFirm(auditFirm, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message});
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New Audit Firm data has been created." });
                }
            }
        }

        
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/AuditFirm/{id}")]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(int id, AuditFirmModel auditFirm)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateAuditFirm(id, auditFirm, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message});
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Audit Firm data has been updated." });
                }
            }
        }

        
        [HttpDelete]
        [Route("api/AuditFirm/{id}")]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteAuditFirm(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message});
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Audit Firm data has been deleted." });
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}