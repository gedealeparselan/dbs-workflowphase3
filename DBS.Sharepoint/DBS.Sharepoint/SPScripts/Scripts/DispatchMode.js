﻿var DispatchModeData = [{ ID: 1, Name: 'BSPL Delivery and Email' }, { ID: 2, Name: 'Collect by Person' }, { ID: 3, Name: 'Email' }, { ID: 4, Name: 'No Dispatch' }, { ID: 5, Name: 'Post' }, { ID: 6, Name: 'SSPL Delivery' }, { ID: 9, Name: '-' }];
var ViewModel = function () {
    var self = this;
    //properties
    self.SPUser = ko.observable();
    self.CreateDate = ko.observable(new Date().toUTCString());
    self.IsDraftForm = ko.observable();
    self.DocumentPath = ko.observable();
    self.DocumentType = ko.observable();
    self.DocumentPurpose = ko.observable();
    self.CutOffTime = ko.observable();
    self.IsCuttOff = ko.observable(false);
    self.strCuttOffTime = ko.observable('');
    self.NotifHeader = ko.observable();
    self.NotifTitle = ko.observable();
    self.NotifMessage = ko.observable();
    self.Documents = ko.observableArray([]);
    self.IsEditable = ko.observable(true);
    self.RetIDColl = ko.observableArray([]);
    self.ApplicationIDColl = ko.observableArray([]);
    self.ddlDispatchModeTypes = ko.observableArray([]);
    self.AddListItem = ko.observableArray([]);
    self.ddlDispatchModeTypes(DispatchModeData);
    self.IsLoadDraft = ko.observable(false);
    self.Parameter = ko.observable({
        DocumentTypes: ko.observableArray([]),
        DocumentPurposes: ko.observableArray([]),
    });
    self.Selected = ko.observable({
        DocumentType: ko.observable(),
        DocumentPurpose: ko.observable(),
    });
    self.TransactionMakerDispatchMode = ko.observable({
        ProductID: ko.observable(),
        RequestTypeID: ko.observable(),
        MaintenanceTypeID: ko.observable(),
        IsDraft: ko.observable(false),
        ApplicationID: ko.observable(),
        TransactionID: ko.observable(),
        DispatchModeTypeID: ko.observable(),
        DispatchModeOther: ko.observable(),
        Documents: ko.observableArray([]),
        IsBringupTask: ko.observable(false)
    });
    //--

    //function
    self.RemoveDocument = function (data) {
        self.Documents.remove(data);
    };
    self.UploadDocument = function () {
        self.DocumentPath(null);
        self.Selected().DocumentType(null);
        self.Selected().DocumentPurpose(null);
        self.DocumentType(null);
        $("#modal-form-upload").modal('show');
        $('.remove').click();
    };
    self.AddDocument = function () {
        var doc = {
            ID: 0,
            Type: self.DocumentType(),
            Purpose: self.DocumentPurpose(),
            FileName: self.DocumentPath().name.replace(/[<>:"\/\\|?!@#$%^&*]+/g, '_'),//file.name
            DocumentPath: self.DocumentPath(),
            LastModifiedDate: new Date(),
            LastModifiedBy: null
        };

        if (doc.Type == null || doc.DocumentPath == null) {
            alert("Please complete the upload form fields.")
        } else {
            self.Documents.push(doc);
            $('.remove').click();

            $("#modal-form-upload").modal('hide');
        }
    };
    self.Submit = function () {
        var form = $("#aspnetForm");
        form.validate();
        viewModel.IsEditable(false);
        self.TransactionMakerDispatchMode().ProductID(ConsProductID.CIFProductIDCons);
        self.TransactionMakerDispatchMode().MaintenanceTypeID(ConsCIFMaintenanceType.DispatchMode);
        self.TransactionMakerDispatchMode().RequestTypeID(ConsRequestType.MaintenanceCons);
        if (form.valid()) {
            if (self.Documents().length > 0) {
                self.TransactionMakerDispatchMode().IsDraft(false);
                self.IsEditable(false);
                var data = {
                    ApplicationID: self.TransactionMakerDispatchMode().ApplicationID(),
                    CIF: "DUMMYCIF",
                    Name: "Dummy CIF"
                };
                CheckCutOff();
                if (self.Documents().length > 0) {
                    self.UploadFileRecuresive(data, self.Documents(), self.SaveTransaction, self.Documents().length);
                } else {
                    self.SaveTransaction();
                }
            } else {
                ShowNotification("Attention", "Please attach document", 'gritter-warning', true);
                self.IsEditable(true);
            }
        }
        else {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);
            self.IsEditable(true);
        }
    };
    self.SaveAsDraft = function () {
        viewModel.IsEditable(false);
        self.TransactionMakerDispatchMode().IsDraft(true)
        self.TransactionMakerDispatchMode().ProductID(ConsProductID.CIFProductIDCons);
        self.TransactionMakerDispatchMode().MaintenanceTypeID(ConsCIFMaintenanceType.DispatchMode);
        self.TransactionMakerDispatchMode().RequestTypeID(ConsRequestType.MaintenanceCons);
        self.IsEditable(false);
        var data = {
            ApplicationID: self.TransactionMakerDispatchMode().ApplicationID(),
            CIF: "DUMMYCIF",
            Name: "Dummy CIF"
        };
        if (self.Documents().length > 0) {
            self.UploadFileRecuresive(data, self.Documents(), self.SaveTransaction, self.Documents().length);
        } else {
            self.SaveTransaction();
        }
    };
    self.CloseTransaction = function () {
        window.location = "/home";
        return;
    };
    self.SaveTransaction = function () {
        if (self.TransactionMakerDispatchMode().IsDraft()) {
            if (self.TransactionMakerDispatchMode().Documents().length == self.Documents().length) {
                if (viewModel.IsCuttOff() == true) {
                    viewModel.TransactionMakerDispatchMode().IsBringupTask(true);
                }
                var options = {
                    url: api.server + api.url.transactioncif + "/SubmitDispatchMode",
                    token: self.secretToken,
                    data: ko.toJSON(self.TransactionMakerDispatchMode())
                };
                if (self.TransactionMakerDispatchMode().TransactionID() == null) {//draft baru
                    Helper.Ajax.Post(options, function (dataB, textStatus, jqXHR) {
                        //console.log(dataB);
                        if (dataB.ID != null || dataB.ID != undefined) {
                            var AppID = {
                                TransactionID: dataB.ID,
                                ApplicationID: dataB.AppID
                            };
                            self.RetIDColl([]);
                            self.RetIDColl.push(AppID);
                            self.TransactionMakerDispatchMode().TransactionID(dataB.ID);
                            self.TransactionMakerDispatchMode().ApplicationID(dataB.AppID);
                            if (viewModel.IsCuttOff() == true) {
                                viewModel.NotifHeader("Cut Off " + viewModel.strCuttOffTime());
                                viewModel.NotifTitle("Attention");
                                viewModel.NotifMessage("Transaction has exceeded cut off time, you need to attach email approval to continue cut off transaction. Please open Bring up file menu.");
                                $("#modal-form-Notif").modal('show');
                            }
                            else {
                                ShowNotification("Transaction Draft Success", "Transaction draft save attachments", "gritter-success", true);
                                window.location = "/home/draft-transactions";
                            }
                            return;
                        }
                    }, self.OnError, self.OnAlways);
                } else {//draft lama, yang pernah ada
                    Helper.Ajax.Post(options, function (dataC, textStatus, jqXHR) {
                        //console.log(dataC);
                        if (dataC.ID != null || dataC.ID != undefined) {
                            if (self.TransactionMakerDispatchMode().TransactionID() != null) {
                                var DraftID = self.TransactionMakerDispatchMode().TransactionID();
                                var AppID = {
                                    TransactionID: dataC.ID,
                                    ApplicationID: dataC.AppID
                                };
                                self.RetIDColl([]);
                                self.RetIDColl.push(AppID);
                                self.TransactionMakerDispatchMode().TransactionID(dataC.ID);
                                self.TransactionMakerDispatchMode().ApplicationID(dataC.AppID);
                                if (viewModel.IsCuttOff() == true) {
                                    viewModel.NotifHeader("Cut Off " + viewModel.strCuttOffTime());
                                    viewModel.NotifTitle("Attention");
                                    viewModel.NotifMessage("Transaction has exceeded cut off time, you need to attach email approval to continue cut off transaction. Please open Bring up file menu.");
                                    $("#modal-form-Notif").modal('show');
                                }
                                else {
                                    ShowNotification("Transaction Draft Success", "Transaction draft save attachments", "gritter-success", true);
                                    window.location = "/home/draft-transactions";
                                }
                                return;
                            }
                        }
                    }, self.OnError, self.OnAlways);
                }
                return;
            }
        };

        if (self.TransactionMakerDispatchMode().Documents().length == self.Documents().length) {
            var options = {
                url: api.server + api.url.transactioncif + "/SubmitDispatchMode",
                token: self.secretToken,
                data: ko.toJSON(self.TransactionMakerDispatchMode())
            };
            console.log((self.TransactionMakerDispatchMode()))
            if (self.TransactionMakerDispatchMode().TransactionID() == null) {//baru yang dulunya bukan draft
                Helper.Ajax.Post(options, function (dataB, textStatus, jqXHR) {
                    //console.log(dataB);
                    if (dataB.ID != null || dataB.ID != undefined) {
                        var AppID = {
                            TransactionID: dataB.ID,
                            ApplicationID: dataB.AppID
                        };
                        self.RetIDColl([]);
                        self.RetIDColl.push(AppID);
                        self.TransactionMakerDispatchMode().TransactionID(dataB.ID);
                        self.TransactionMakerDispatchMode().ApplicationID(dataB.AppID);
                        self.AddListItem();
                    }
                }, self.OnError, self.OnAlways);
            } else {//baru yang dulunya draft
                Helper.Ajax.Post(options, function (dataC, textStatus, jqXHR) {
                    //console.log(dataC);
                    if (dataC.ID != null || dataC.ID != undefined) {
                        if (self.TransactionMakerDispatchMode().TransactionID() != null) {
                            var DraftID = self.TransactionMakerDispatchMode().TransactionID();
                            var AppID = {
                                TransactionID: dataC.ID,
                                ApplicationID: dataC.AppID
                            };
                            self.RetIDColl([]);
                            self.RetIDColl.push(AppID);
                            self.DeleteDraftTr(self.TransactionMakerDispatchMode().TransactionID());
                            self.TransactionMakerDispatchMode().TransactionID(dataC.ID);
                            self.TransactionMakerDispatchMode().ApplicationID(dataC.AppID);
                            self.AddListItem();
                        }
                    }
                }, self.OnError, self.OnAlways);
            }
        };
    };
    self.UploadFileRecuresive = function (context, document, callBack, numFile) {
        var indexDocument = document.length - numFile;
        var IsDraft = self.TransactionMakerDispatchMode().IsDraft();
        var d = new Date();
        var year = d.getFullYear();
        var month = d.getMonth();
        if (month != '10' || month != '11' || month != '12') {
            month = '0' + month;
        }

        var serverRelativeUrlToFolder = '';
        if (IsDraft == true)
            serverRelativeUrlToFolder = '/DraftDocument';
        else {
            serverRelativeUrlToFolder = '/Instruction Documents';
        }

        var parts = document[indexDocument].DocumentPath.name.split('.');
        var fileExtension = parts[parts.length - 1];
        var timeStamp = new Date().getTime();
        var fileName = timeStamp + "." + fileExtension; //document.DocumentPath.name;

        // Get the server URL.
        var serverUrl = _spPageContextInfo.webAbsoluteUrl;

        // output variable
        var output;

        // Initiate method calls using jQuery promises.
        // Get the local file as an array buffer.
        var getFile = getFileBuffer();
        getFile.done(function (arrayBuffer) {
            // Add the file to the SharePoint folder.
            var addFile = addFileToFolder(arrayBuffer);
            addFile.done(function (file, status, xhr) {
                output = file.d;

                // Get the list item that corresponds to the uploaded file.
                var getItem = getListItem(file.d.ListItemAllFields.__deferred.uri);
                getItem.done(function (listItem, status, xhr) {
                    // Change the display name and title of the list item.
                    var changeItem = updateListItem(listItem.d.__metadata);
                    changeItem.done(function (data, status, xhr) {
                        //alert('file uploaded and updated');
                        //return output;
                        var newDoc = {
                            ID: 0,
                            Type: document[indexDocument].Type,
                            Purpose: document[indexDocument].Purpose,
                            LastModifiedDate: null,
                            LastModifiedBy: null,
                            DocumentPath: {
                                DocPath: output.ServerRelativeUrl,
                                name: document[indexDocument].DocumentPath.name,
                                //type: null,
                                //lastModifiedDate: null
                            },
                            FileName: document[indexDocument].DocumentPath.name
                        };

                        self.TransactionMakerDispatchMode().Documents.push(newDoc);

                        if (numFile > 1) {
                            self.UploadFileRecuresive(context, document, callBack, numFile - 1); // recursive function
                        }
                        callBack();
                    });
                    changeItem.fail(self.OnError);
                });
                getItem.fail(self.OnError);
            });
            addFile.fail(self.OnError);
        });
        getFile.fail(self.OnError);

        // Get the local file as an array buffer.
        function getFileBuffer() {
            var deferred = $.Deferred();
            var reader = new FileReader();
            reader.onloadend = function (e) {
                deferred.resolve(e.target.result);
            }
            reader.onerror = function (e) {
                deferred.reject(e.target.error);
            }

            if (document[indexDocument].DocumentPath.type == Util.spitemdoc) {
                var fileURI = serverUrl + document[indexDocument].DocumentPath.DocPath;
                // load file:
                fetch(fileURI, convert, alert);

                function convert(buffer) {
                    var blob; // 
                    if (fileExtension == "txt") {
                        blob = new Blob([buffer], { type: "text/plain" });
                    } else if (fileExtension == "xlsx") {
                        blob = new Blob([buffer], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
                    } else if (fileExtension == "docx") {
                        blob = new Blob([buffer], { type: "application/vnd.openxmlformats-officedocument.wordprocessingml.document" });
                    } else if (fileExtension == "doc") {
                        blob = new Blob([buffer], { type: "application/msword" });
                    } else if (fileExtension == "xls") {
                        blob = new Blob([buffer], { type: "application/vnd.ms-excel" });
                    } else if (fileExtension == "pdf") {
                        blob = new Blob([buffer], { type: "application/pdf" });
                    } else if (fileExtension == "zip") {
                        blob = new Blob([buffer], { type: "application/zip, application/x-compressed-zip" });
                    } else if (fileExtension == "bmp") {
                        blob = new Blob([buffer], { type: "image/bmp" });
                    } else if (fileExtension == "dotx") {
                        blob = new Blob([buffer], { type: "application/vnd.openxmlformats-officedocument.wordprocessingml.template" });
                    } else if (fileExtension == "jpg") {
                        blob = new Blob([buffer], { type: "image/jpeg" });
                    } else if (fileExtension == "png") {
                        blob = new Blob([buffer], { type: "image/png" });
                    } else if (fileExtension == "potx") {
                        blob = new Blob([buffer], { type: "application/vnd.openxmlformats-officedocument.presentationml.template" });
                    } else if (fileExtension == "ppsx") {
                        blob = new Blob([buffer], { type: "application/vnd.openxmlformats-officedocument.presentationml.slideshow" });
                    } else if (fileExtension == "pptx") {
                        blob = new Blob([buffer], { type: "application/vnd.openxmlformats-officedocument.presentationml.presentation" });
                    } else if (fileExtension == "tsv") {
                        blob = new Blob([buffer], { type: "text/tab-separated-values" });
                    } else if (fileExtension == "rtf") {
                        blob = new Blob([buffer], { type: "application/rtf" });
                    } else if (fileExtension == "xltx") {
                        blob = new Blob([buffer], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.template" });
                    }
                    var domURL = window.URL || self.URL || self.webkitURL;
                    var url = domURL.createObjectURL(blob),
                      img = new Image;

                    img.onload = function () {
                        domURL.revokeObjectURL(url); // clean up
                        //document.body.appendChild(this);
                        // this = image
                    };
                    img.src = url;
                    reader.readAsArrayBuffer(blob);
                }

                function fetch(url, callback, error) {

                    var xhr = new XMLHttpRequest();
                    try {
                        xhr.open("GET", url);
                        xhr.responseType = "arraybuffer";
                        xhr.onerror = function () {
                            error("Network error")
                        };
                        xhr.onload = function () {
                            if (xhr.status === 200) callback(xhr.response);
                            else error(xhr.statusText);
                        };
                        xhr.send();
                    } catch (err) {
                        error(err.message)
                    }
                }


            } else {
                reader.readAsArrayBuffer(document[indexDocument].DocumentPath);
            }
            //bangkit

            //reader.readAsDataURL(document.DocumentPath);
            return deferred.promise();
        }

        // Add the file to the file collection in the Shared Documents folder.
        function addFileToFolder(arrayBuffer) {

            // Construct the endpoint.
            var fileCollectionEndpoint = String.format(
                    "{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files" +
                    "/add(overwrite=false, url='{2}')",
                serverUrl, serverRelativeUrlToFolder, fileName);

            UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval); //refresh SharePoint token: Rizki 2017-01-31

            // Send the request and return the response.
            // This call returns the SharePoint file.
            return $.ajax({
                url: fileCollectionEndpoint,
                type: "POST",
                data: arrayBuffer,
                processData: false,
                headers: {
                    "accept": "application/json;odata=verbose",
                    "X-RequestDigest": $("#__REQUESTDIGEST").val()
                    //"content-length": arrayBuffer.byteLength
                }
            });
        };

        // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
        function getListItem(fileListItemUri) {

            // Send the request and return the response.
            return $.ajax({
                url: fileListItemUri,
                type: "GET",
                headers: { "accept": "application/json;odata=verbose" }
            });
        };

        // Change the display name and title of the list item.
        function updateListItem(itemMetadata) {
            var body = {
                Title: document[indexDocument].DocumentPath.name,
                //Application_x0020_ID: context.ApplicationID,
                //CIF: context.CIF,
                //Customer_x0020_Name: context.Name,
                //Document_x0020_Type: document[indexDocument].Type.Name,
                //Document_x0020_Purpose: document[indexDocument].Purpose.Name,
                __metadata: {
                    type: itemMetadata.type,
                    FileLeafRef: fileName,
                    Title: fileName
                }
            };

            UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval); //refresh SharePoint token: Rizki 2017-01-31

            // Send the request and return the promise.
            // This call does not return response content from the server.
            return $.ajax({
                url: itemMetadata.uri,
                type: "POST",
                data: ko.toJSON(body),
                headers: {
                    "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                    "content-type": "application/json;odata=verbose",
                    //"content-length": body.length,
                    "IF-MATCH": itemMetadata.etag,
                    "X-HTTP-Method": "MERGE"
                }
            });
        };
    };
    self.LoadDraft = function () {
        var uri = '';
        ar = window.location.hash.split('#');

        if (ar.length < 2) {
            self.IsDraftForm(false);
            return;
        }
        self.IsDraftForm(true);
        uri = api.server + api.url.transactioncif + '/DispatchDraft/' + ar[1];
        var options = {
            url: uri,
            token: self.secretToken
        };
        Helper.Ajax.Get(options, function (data, textStatus, jqXHR) {
            if (jqXHR.status = 200) {
                self.IsLoadDraft(true);
                if (data == null) return;
                self.LoadDraftDM(data);
            } else {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
            }
        }, self.OnError, self.OnAlways);


    };
    self.LoadDraftDM = function (data) {
        viewModel.TransactionMakerDispatchMode().ProductID(ConsProductID.CIFProductIDCons)
        viewModel.TransactionMakerDispatchMode().RequestTypeID(data.RequestTypeID)
        viewModel.TransactionMakerDispatchMode().MaintenanceTypeID(data.MaintenanceTypeID)
        viewModel.TransactionMakerDispatchMode().TransactionID(data.TransactionID)
        viewModel.TransactionMakerDispatchMode().DispatchModeTypeID(data.DispatchModeTypeID)
        viewModel.TransactionMakerDispatchMode().DispatchModeOther(data.DispatchModeOther)
        viewModel.TransactionMakerDispatchMode().Documents(data.Documents)
        if (data.Documents != null && data.Documents.length > 0) {
            ko.utils.arrayForEach(data.Documents, function (item) {
                self.Documents.push(item);
            });
        }
        self.GetParameters();
    };
    function OnSuccessSaveDraft(data, textStatus, jqXHR) {
        if (viewModel.IsCuttOff() == true) {
            viewModel.NotifHeader("Cut Off " + viewModel.strCuttOffTime());
            viewModel.NotifTitle("Attention");
            viewModel.NotifMessage("Transaction has exceeded cut off time, you need to attach email approval to continue cut off transaction. Please open Bring up file menu.");
            $("#modal-form-Notif").modal('show');
        }
        else {
            ShowNotification("Transaction Draft Success", "Transaction draft save attachments", "gritter-success", true);
            window.location = "/home/draft-transactions";
        }
    }
    //--

    //Global Function
    self.GetUserRole = function (userData) {
        var sValue;
        if (userData != undefined && userData.length > 0) {
            $.each(userData, function (index, item) {
                if (Const_RoleName[item.ID].toLowerCase().startsWith("dbs ppu") && Const_RoleName[item.ID].toLowerCase().endsWith("maker")) { //if (item.Name.toLowerCase().startsWith("dbs ppu") && item.Name.toLowerCase().endsWith("maker")) {
                    sValue = item.ID;
                    return sValue;
                }
            });

            //Untuk user non PPU tetapi bisa New Transaction
            $.each(userData, function (index, item) {
                if (Const_RoleName[item.ID].toLowerCase().endsWith("maker")) {
                    sValue = item.ID;
                    return sValue;
                }
            });
        }
        if (sValue == null) {
            sValue = userData[0].ID;
        }
        return sValue;
    };
    self.OnError = function (jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    };
    self.OnAlways = function () {
        if (viewModel != undefined) {
        }
    };
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);
        if (date == '1970/01/01 00:00:00' || date == null) {
            return "";
        }

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return "";
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-17
    };
    self.IsRole = function (name) {
        var result = false;
        $.each($.cookie(api.cookie.spUser).Roles, function (index, item) {
            if (Const_RoleName[item.ID] == name) result = true;
        });
        return result;
    };
    self.AddListItem = function () {
        var body;
        var urlListTransaction = config.sharepoint.DispatchModeRC;
        var Listtitle = self.TransactionMakerDispatchMode().ApplicationID() + " - " + "DUMMYCIF";
        var ListInitGroup = self.GetUserRole(self.SPUser().Roles);
        var ListTransactionID = self.TransactionMakerDispatchMode().TransactionID();
        var ListAppID = self.TransactionMakerDispatchMode().ApplicationID();
        var ListTipe = config.sharepoint.metadata.listDispatchMode;

        body = {
            Title: Listtitle,
            Initiator_x0020_GroupId: ListInitGroup,
            Transaction_x0020_ID: ListTransactionID,
            Application_x0020_ID: ListAppID,
            __metadata: {
                type: ListTipe
            }
        };

        UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval);

        var options = {
            url: config.sharepoint.url.api + "/Lists(guid'" + urlListTransaction + "')/Items",
            data: JSON.stringify(body),
            digest: jQuery("#__REQUESTDIGEST").val()
        };
        Helper.Sharepoint.List.Add(options, function (data, textStatus, jqXHR) {
            var AppIDColl = self.RetIDColl();
            var Cust = "DUMMYCIF";
            self.TransactionMakerDispatchMode().ApplicationID(AppIDColl[0].ApplicationID);
            self.TransactionMakerDispatchMode().TransactionID(AppIDColl[0].TransactionID);
            if (AppIDColl != null) {

                self.ApplicationIDColl([]);
                var appColl = {
                    TransactionID: AppIDColl[0].TransactionID,
                    ApplicationID: AppIDColl[0].ApplicationID,
                    Customer: Cust,
                }
                self.ApplicationIDColl.push(appColl);
                ShowNotification("Submit Transaction Success", "", "gritter-success", true);
                $("#modal-form-applicationID").modal('show');
                window.location = "/home";
            }
            else
                window.location = "/home";
        }, self.OnError, self.OnAlways);
    };
    self.DeleteDraftTr = function (DraftID) {
        $.ajax({
            type: "DELETE",
            url: api.server + api.url.transactioncif + "/Draft/Delete/" + DraftID,
            headers: {
                "Authorization": "Bearer " + self.secretToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status == 200) {
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    };
    self.OnSuccessToken = function (data, textStatus, jqXHR) {
        // store token on browser cookies
        $.cookie(api.cookie.name, data.AccessToken);
        self.secretToken = $.cookie(api.cookie.name);

        // read spuser from cookie
        if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
            spUser = $.cookie(api.cookie.spUser);
            self.SPUser(ko.mapping.toJS(spUser));
        }
    };
    self.secretToken = null;
    self.GetParameters = function () {
        var options = {
            url: api.server + api.url.parameter,
            params: {
                select: "DocType,PurposeDoc"
            },
            token: self.secretToken
        };
        Helper.Ajax.Get(options, function (data, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                var mapping = {
                    'ignore': ["LastModifiedDate", "LastModifiedBy"]
                };
                self.Parameter().DocumentTypes(ko.mapping.toJS(data.DocType, mapping));
                self.Parameter().DocumentPurposes(ko.mapping.toJS(data.PurposeDoc, mapping));
            }
        }, self.OnError, self.OnAlways);
    };
    if ($.cookie(api.cookie.name) == undefined) {
        Helper.Token.Request(self.OnSuccessToken, self.OnError);
    } else {
        // read token from cookie
        self.secretToken = $.cookie(api.cookie.name);
        // read spuser from cookie
        if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
            spUser = $.cookie(api.cookie.spUser);
            self.SPUser(ko.mapping.toJS(spUser));
        }

    }
    function CheckCutOff() {
        var cutOFFParameter = '';
        cutOFFParameter = ConsPARSYS.cifCuttof;
        var options = {
            url: api.server + api.url.parametersystem + "/IsCutOff/" + cutOFFParameter,
            token: self.secretToken
        };

        Helper.Ajax.Get(options, OnSuccessCheckCutOff, OnError, OnAlways);
    }

    function OnSuccessCheckCutOff(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            if (data != null) {
                viewModel.IsCuttOff(data.IsCutOff);
                viewModel.strCuttOffTime(data.CuttofValue);
                if (viewModel.IsDraftForm() == true) {
                    if (viewModel.IsCuttOff() == true) {
                        var isAttachedEmail = CheckEmailAppCIF();
                        if (isAttachedEmail == true) {
                            self.SaveTransaction();
                            viewModel.IsCuttOff(false);
                        }
                        else {
                            ShowNotification("Attention", "You need to attach email approval to continue cut off transaction.", 'gritter-warning', true);
                            return;
                        }
                    }
                    else {
                        self.SaveTransaction();
                        viewModel.IsCuttOff(false);
                    }
                }
                else {
                    if (viewModel.IsCuttOff() == true) {
                        self.TransactionMakerDispatchMode().IsDraft(true);
                        self.TransactionMakerDispatchMode().IsBringupTask(true);
                        self.IsEditable(false);
                        self.SaveAsDraft();//cutt off going to draft(temporary)                  
                        viewModel.IsCuttOff(true);
                    }
                    else {
                        self.SaveTransaction();
                        viewModel.IsCuttOff(false);
                    }
                }

            }
            else
                ShowNotification('Cut Off Empty!!!', jqXHR.responseText, 'gritter-error', true);

        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }
    function OnSuccessSaveDraftDM(data, textStatus, jqXHR) {
        if (viewModel.IsCuttOff() == true) {
            viewModel.NotifHeader("Cut Off " + viewModel.strCuttOffTime());
            viewModel.NotifTitle("Attention");
            viewModel.NotifMessage("Transaction has exceeded cut off time, you need to attach email approval to continue cut off transaction. Please open Bring up file menu.");
            $("#modal-form-Notif").modal('show');
        }
        else {
            ShowNotification("Transaction Draft Success", "Transaction draft save attachments and underlying", "gritter-success", true);
            window.location = "/home/draft-transactions";
        }
    }
    function CheckEmailAppCIF() {
        var docs = viewModel.Documents();
        var EmailApp = config.validate.approval.emailapp;
        var isAttachedEmail = false;
        for (var i = 0; i < docs.length; i++) {
            if (EmailApp.indexOf(docs[i].Type["Name"]) > -1) {
                isAttachedEmail = true;
            }
        }
        return isAttachedEmail;
    }
    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
    function OnAlways() {
        // $box.trigger('reloaded.ace.widget');
        if (viewModel != undefined) {
            // enabling form input controls
            viewModel.IsEditable(true);
        }
    }
    self.NotifOK = function () {
        if (viewModel.IsCuttOff() == true) {
            window.location = "/home/bring-up";
        }
        else
            window.location = "/home";
    }
    self.GetParameters();
    //--
};

var viewModel = new ViewModel();

$(document).ready(function () {
    $box = $('#widget-box');
    var event;
    $box.trigger(event = $.Event('reload.ace.widget'))
    if (event.isDefaultPrevented()) return
    $box.blur();

    // block enter key from user to prevent submitted data.
    $(this).keypress(function (event) {
        var code = event.charCode || event.keyCode;
        if (code == 13) {
            ShowNotification("Page Information", "Enter key is disabled for this form.", "gritter-warning", false);

            return false;
        }
    });

    // Load all templates
    $('script[src][type="text/html"]').each(function () {
        //alert($(this).attr("src"))
        var src = $(this).attr("src");
        if (src != undefined) {
            $(this).load(src);
        }
    });

    // scrollables
    $('.slim-scroll').each(function () {
        var $this = $(this);
        $this.slimscroll({
            height: $this.data('height') || 100,
            railVisible: true,
            alwaysVisible: true,
            color: '#D15B47'
        });
    });

    // tooltip
    $('[data-rel=tooltip]').tooltip();

    $.fn.ForceNumericOnly = function () {
        return this.each(function () {
            $(this).keydown(function (e) {
                var key = e.charCode || e.keyCode || 0;
                return (
                    key == 8 ||
                    key == 9 ||
                    key == 13 ||
                    key == 46 ||
                    key == 110 ||
                    key == 190 ||
                    (key >= 35 && key <= 40) ||
                    (key >= 48 && key <= 57) ||
                    (key >= 96 && key <= 105));
            });

        });

    };
    $(".input-numericonly").ForceNumericOnly();

    $('.btn').each(function () {
        dataBind = $(this).attr('data-bind');
        if (dataBind == "click: SaveAsDraft, disable: !IsEditable(), visible: IsRole('DBS Admin')") {
            $(this).attr('data-bind', "click: SaveAsDraft, disable: !IsEditable(), visible: true")
        }
    });

    /// block enter key from user to prevent submitted data.
    $("input").keypress(function (event) {
        var code = event.charCode || event.keyCode;
        if (code == 13) {
            ShowNotification("Page Information", "Enter key is disabled for this form.", "gritter-warning", false);

            return false;
        }
    });

    $box = $('#widget-box');
    var event;
    $box.trigger(event = $.Event('reload.ace.widget'))
    if (event.isDefaultPrevented()) return
    $box.blur();

    // datepicker
    $('.date-picker').datepicker({
        autoclose: true,
        onSelect: function () {
            this.focus();
        }
    }).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });

    // validation
    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        focusCleanup: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }
    });

    // ace file upload
    $('#document-path').ace_file_input({
        no_file: 'No File ...',
        btn_choose: 'Choose',
        btn_change: 'Change',
        droppable: false,
        //onchange:null,
        thumbnail: false, //| true | large
        //whitelist:'gif|png|jpg|jpeg'
        blacklist: 'exe|dll'
        //onchange:''
        //
    });

    $('#document-path-upload').ace_file_input({
        no_file: 'No File ...',
        btn_choose: 'Choose',
        btn_change: 'Change',
        droppable: false,
        //onchange:null,
        thumbnail: false, //| true | large
        //whitelist:'gif|png|jpg|jpeg'
        blacklist: 'exe|dll'
        //onchange:''
        //
    });

    // Knockout Datepicker custom binding handler
    ko.bindingHandlers.datepicker = {
        init: function (element) {
            $(element).datepicker({ autoclose: true }).next().on(ace.click_event, function () {
                $(this).prev().focus();
            });
        },
        update: function (element) {
            $(element).datepicker("refresh");
        }
    };

    // Knockout custom file handler
    ko.bindingHandlers.file = {
        init: function (element, valueAccessor) {
            $(element).change(function () {
                var file = this.files[0];

                if (ko.isObservable(valueAccessor()))
                    valueAccessor()(file);
            });
        }
    };

    if (viewModel != undefined) {
        ko.dependentObservable(function () {
            var docType = ko.utils.arrayFirst(viewModel.Parameter().DocumentTypes(), function (item) {
                return item.ID == viewModel.Selected().DocumentType();
            });

            if (docType != null) viewModel.DocumentType(docType);

            var docPurpose = ko.utils.arrayFirst(viewModel.Parameter().DocumentPurposes(), function (item) {
                return item.ID == viewModel.Selected().DocumentPurpose();
            });

            if (docPurpose != null) viewModel.DocumentPurpose(docPurpose);

        });
    };

    self.SPUser = ko.observable();
    // Token Validation
    if ($.cookie(api.cookie.name) == undefined) {
        Helper.Token.Request(OnSuccessToken, OnError);
    }
    else {
        // read token from cookie
        viewModel.secretToken = $.cookie(api.cookie.name);

        // read spuser from cookie
        if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
            spUser = $.cookie(api.cookie.spUser);
            self.SPUser(ko.mapping.toJS(spUser));
        }
        viewModel.LoadDraft();
    }

    ko.applyBindings(viewModel);
});