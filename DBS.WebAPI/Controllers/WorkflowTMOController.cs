﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

//=======================================================================================//
///FOR ALL DEVELOPER...
///PLEASE ADD NEW REGION BY YOUR NAME TO KEEP CLEAN !!! DONT BE A DIRTY PROGRAMMER!!!
///ANDIWIJAYA
//=======================================================================================//

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class WorkflowTMOController : ApiController
    {
        private string message = string.Empty;
        /// <summary>
        /// Define repo here
        /// </summary>
        private readonly IWorkflowTMORepository repoTMO = null;
        public WorkflowTMOController()
        {
            this.repoTMO = new WorkflowTMORepository();
        }
        /// <summary>
        /// Add Controller below...
        /// </summary>
        //Basri 01.10.2015
        /// <summary>
        /// Get payment details of transaction by WF Instance ID
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <returns></returns>
        [ResponseType(typeof(TMOMakerDetailModel))]
        [HttpGet]
        [Route("api/WorkflowTMO/{workflowInstanceID}/TMO/Maker/{approverID}")]
        public HttpResponseMessage GetTMOMakerDetails(Guid workflowInstanceID, long approverID)
        {
            TMODetailModel transaction = new TMODetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TMOMakerDetailModel output = new TMOMakerDetailModel();

            try
            {
                // #1 Get TMO Details
                if (!repoTMO.GetTMODetails(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoTMO.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        output.Transaction = transaction;
                        output.Timelines = timelines;

                        return Request.CreateResponse(HttpStatusCode.OK, output);
                    }
                }
            }
            finally
            {
                transaction = null;
                timelines = null;
            }
        }
        /// <summary>
        /// Get payment details of transaction by WF Instance ID
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <returns></returns>
        [ResponseType(typeof(TMOMakerDetailModel))]
        [HttpGet]
        [Route("api/WorkflowTMO/{workflowInstanceID}/TMO/Checker/{approverID}")]
        public HttpResponseMessage GetTMOCheckerDetails(Guid workflowInstanceID, long approverID)
        {
            TMODetailModel transaction = new TMODetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TMOCheckerDetailModel output = new TMOCheckerDetailModel();
            TransactionTMOCheckerDetailModel outputChecker = new TransactionTMOCheckerDetailModel();

            try
            {
                // #1 Get TMO Details
                if (!repoTMO.GetTMODetails(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoTMO.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        if (!repoTMO.GetTMOCheckerDetails(workflowInstanceID, approverID, ref outputChecker, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Transaction = transaction;
                            output.Timelines = timelines;
                            output.Checker = outputChecker.Checker;
                            output.Verify = outputChecker.Verify;

                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {
                transaction = null;
                timelines = null;
            }
        }
        /// <summary>
        /// Save Transaction TMO from TMO Maker
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approver ID from Nintex WF</param>
        /// <param name="tmo">TMO Maker data</param>
        /// <returns></returns>
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowTMO/{workflowInstanceID}/TMO/Maker/{approverID}")]
        public HttpResponseMessage AddTMOMakerDetails(Guid workflowInstanceID, long approverID, TMODetailModel tmo)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoTMO.AddTransactionTMOMaker(workflowInstanceID, approverID, tmo, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "TMO maker data has been saved." });
                }
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowTMO/{workflowInstanceID}/Transaction/Maker/{approverID}")]
        public HttpResponseMessage AddTransactionMakerDetails(Guid workflowInstanceID, long approverID, TransactionTMODetailModel transaction)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoTMO.AddTransactionMaker(workflowInstanceID, approverID, transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction maker data revision has been saved." });
                }
            }
        }
        /// <summary>
        /// Get transaction details for checker by WF Instance ID
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approve ID from Nintex WF</param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionTMOMakerDetailModel))]
        [HttpGet]
        [Route("api/WorkflowTMO/{workflowInstanceID}/Transaction/Maker/{approverID}")]
        public HttpResponseMessage GetTransactionMakerDetails(Guid workflowInstanceID, long approverID)
        {
            TransactionTMODetailModel transaction = new TransactionTMODetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TransactionTMOMakerDetailModel output = new TransactionTMOMakerDetailModel();

            try
            {
                if (!repoTMO.GetTransactionDetails(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoTMO.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        output.Transaction = transaction;
                        output.Timelines = timelines;

                        return Request.CreateResponse(HttpStatusCode.OK, output);
                    }
                }
            }
            finally
            {

                transaction = null;
                timelines = null;
            }
        }
        /// <summary>
        /// Get transaction details for checker by WF Instance ID
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approve ID from Nintex WF</param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionTMOCheckerDetailModel))]
        [HttpGet]
        [Route("api/WorkflowTMO/{workflowInstanceID}/Transaction/Checker/{approverID}")]
        public HttpResponseMessage GetTransactionCheckerDetails(Guid workflowInstanceID, long approverID)
        {
            TransactionTMODetailModel transaction = new TransactionTMODetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TransactionTMOCheckerDetailModel output = new TransactionTMOCheckerDetailModel();
            TransactionTMOCheckerAfterCallbackDetailModel callback = new TransactionTMOCheckerAfterCallbackDetailModel();
            TransactionTMOCallbackDetailModel callbackDetail = new TransactionTMOCallbackDetailModel();
            try
            {
                // #1 Get Transaction Details
                if (!repoTMO.GetTransactionDetails(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoTMO.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        // #3 Get Transaction Checker Details
                        if (!repoTMO.GetTransactionCheckerDetails(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            if (!repoTMO.GetTransactionCallbackDetails(workflowInstanceID, approverID, ref callbackDetail, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                            }
                            else
                            {
                                output.Transaction = transaction;
                                output.Timelines = timelines;
                                output.callbackDetail = callbackDetail;

                                return Request.CreateResponse(HttpStatusCode.OK, output);
                            }

                        }
                    }
                }
            }
            finally
            {

                transaction = null;
                timelines = null;
            }
        }

        /// <summary>
        /// Save approval data from checker
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approver ID from Nintex WF</param>
        /// <param name="transactionChecker">Transaction checker data</param>
        /// <returns></returns>
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowTMO/{workflowInstanceID}/Transaction/Checker/{approverID}")]
        public HttpResponseMessage AddTransactionCheckerDetails(Guid workflowInstanceID, long approverID, TransactionTMOCheckerDetailModel transactionChecker)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoTMO.AddTransactionChecker(workflowInstanceID, approverID, transactionChecker, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been saved." });
                }
            }
        }
        /// <summary>
        /// Get transaction details for Maker After Revise by WF Instance ID
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approve ID from Nintex WF</param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionTMOCheckerDetailModel))]
        [HttpGet]
        [Route("api/WorkflowTMO/{workflowInstanceID}/Transaction/MakerReviseAfterChecker/{approverID}")]
        public HttpResponseMessage GetTransactionMakerAfterReviseDetails(Guid workflowInstanceID, long approverID)
        {
            TransactionTMODetailModel transaction = new TransactionTMODetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TransactionTMOCheckerDetailModel output = new TransactionTMOCheckerDetailModel();

            try
            {
                // #1 Get Transaction Details
                if (!repoTMO.GetTransactionMakerReviseAfterCheckerDetails(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoTMO.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        // #3 Get Transaction Checker Details
                        if (!repoTMO.GetTransactionCheckerDetails(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Transaction = transaction;
                            output.Timelines = timelines;

                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {

                transaction = null;
                timelines = null;
            }
        }
        [ResponseType(typeof(TransactionTMOCheckerDetailModel))]
        [HttpGet]
        [Route("api/WorkflowTMO/{workflowInstanceID}/Transaction/MakerReviseAfterChecker/{approverID}/TMO")]
        public HttpResponseMessage GetTransactionMakerAfterReviseTMODetails(Guid workflowInstanceID, long approverID)
        {
            TransactionTMODetailModel transaction = new TransactionTMODetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TransactionTMOCheckerDetailModel output = new TransactionTMOCheckerDetailModel();

            try
            {
                // #1 Get Transaction Details
                if (!repoTMO.GetTransactionMakerReviseAfterCheckerTMODetails(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoTMO.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        // #3 Get Transaction Checker Details
                        if (!repoTMO.GetTransactionCheckerDetails(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Transaction = transaction;
                            output.Timelines = timelines;

                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {

                transaction = null;
                timelines = null;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="workflowInstanceID"></param>
        /// <param name="approverID"></param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionCallbackDetailModel))]
        [HttpGet]
        [Route("api/WorkflowTMO/{workflowInstanceID}/Transaction/Callback/{approverID}")]
        public HttpResponseMessage GetTransactionCallbackDetails(Guid workflowInstanceID, long approverID)
        {
            TransactionTMODetailModel transaction = new TransactionTMODetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TransactionTMOCallbackDetailModel output = new TransactionTMOCallbackDetailModel();

            try
            {
                // #1 Get Transaction Details
                if (!repoTMO.GetTransactionDetails(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "1" });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoTMO.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "2" });
                    }
                    else
                    {
                        // #3 Get Transaction Checker Details getutc
                        if (!repoTMO.GetTransactionCallbackDetails(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "3" });
                        }
                        else
                        {
                            output.Transaction = transaction;
                            output.Timelines = timelines;

                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {

                transaction = null;
                timelines = null;
            }
        }
        /// <summary>
        /// Save Transaction Caller from PPU Caller
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approver ID from Nintex WF</param>
        /// <param name="data">Payment Maker data</param>
        /// <returns></returns>
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowTMO/{workflowInstanceID}/Transaction/Callback/{approverID}")]
        public HttpResponseMessage AddTransactionCallbackDetails(Guid workflowInstanceID, long approverID, TransactionTMOCallbackDetailModel data)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoTMO.AddTransactionCallback(workflowInstanceID, approverID, data, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "transaction callback data has been saved." });
                }
            }
        }

        /// <summary>
        /// Get transaction details for checker afte caller by WF Instance ID
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approve ID from Nintex WF</param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionTMOCheckerAfterCallbackDetailModel))]
        [HttpGet]
        [Route("api/WorkflowTMO/{workflowInstanceID}/Transaction/Checker/Callback/{approverID}")]
        public HttpResponseMessage GetTransactionCheckCallbackDetails(Guid workflowInstanceID, long approverID)
        {
            TransactionTMODetailModel transaction = new TransactionTMODetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TransactionTMOCheckerAfterCallbackDetailModel output = new TransactionTMOCheckerAfterCallbackDetailModel();

            try
            {
                // #1 Get Transaction Details
                if (!repoTMO.GetTransactionDetails(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoTMO.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        // #3 Get Transaction Checker Details
                        if (!repoTMO.GetTransactionCheckerAfterCallbackDetails(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Transaction = transaction;
                            output.Timelines = timelines;

                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {

                transaction = null;
                timelines = null;
            }
        }
        /// <summary>
        /// Save approval data from checker after callback
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approver ID from Nintex WF</param>
        /// <param name="transactionChecker">Transaction checker data</param>
        /// <returns></returns>
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowTMO/{workflowInstanceID}/Transaction/Checker/Callback/{approverID}")]
        public HttpResponseMessage AddTransactionCheckerCallbackDetails(Guid workflowInstanceID, long approverID, TransactionTMOCheckerDetailModel data)
        {
            // Forward to existing method: same as url "api/WorkflowTMO/{workflowInstanceID}/Transaction/Checker/{approverID}"
            return AddTransactionCheckerDetails(workflowInstanceID, approverID, data);
        }

        /// <summary>
        /// Get transaction details for checker by WF Instance ID
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approve ID from Nintex WF</param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionTMOCheckerDetailModel))]
        [HttpGet]
        [Route("api/WorkflowTMO/{workflowInstanceID}/Transaction/CSO/{approverID}")]
        public HttpResponseMessage GetTransactionCSODetails(Guid workflowInstanceID, long approverID)
        {
            return GetTransactionMakerAfterReviseTMODetails(workflowInstanceID, approverID);
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowTMO/{workflowInstanceID}/Transaction/CSO/{approverID}")]
        public HttpResponseMessage AddTransactionCSODetails(Guid workflowInstanceID, long approverID, TransactionTMODetailModel transaction)
        {
            return AddTransactionMakerDetails(workflowInstanceID, approverID, transaction);
        }
        /// <summary>
        /// Get transaction details for checker afte caller by WF Instance ID
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approve ID from Nintex WF</param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionTMOCheckerAfterCallbackDetailModel))]
        [HttpGet]
        [Route("api/WorkflowTMO/{workflowInstanceID}/Transaction/MakerReviseAfterCaller/{approverID}")]
        public HttpResponseMessage GetTransactionMakerAfterReviseCallerDetails(Guid workflowInstanceID, long approverID)
        {
            TransactionTMODetailModel transaction = new TransactionTMODetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TransactionTMOCheckerAfterCallbackDetailModel output = new TransactionTMOCheckerAfterCallbackDetailModel();

            try
            {
                // #1 Get Transaction Details
                if (!repoTMO.GetTransactionMakerReviseAfterCheckerDetails(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoTMO.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        // #3 Get Transaction Checker Details
                        if (!repoTMO.GetTransactionCheckerAfterCallbackDetails(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Transaction = transaction;
                            output.Timelines = timelines;

                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {

                transaction = null;
                timelines = null;
            }
        }
        [ResponseType(typeof(TransactionTMOCheckerAfterCallbackDetailModel))]
        [HttpGet]
        [Route("api/WorkflowTMO/{workflowInstanceID}/Transaction/MakerReviseAfterCaller/{approverID}/TMO")]
        public HttpResponseMessage GetTransactionMakerAfterReviseCallerTMODetails(Guid workflowInstanceID, long approverID)
        {
            TransactionTMODetailModel transaction = new TransactionTMODetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TransactionTMOCheckerAfterCallbackDetailModel output = new TransactionTMOCheckerAfterCallbackDetailModel();

            try
            {
                // #1 Get Transaction Details
                if (!repoTMO.GetTransactionMakerReviseAfterCheckerTMODetails(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoTMO.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        // #3 Get Transaction Checker Details
                        if (!repoTMO.GetTransactionCheckerAfterCallbackDetails(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Transaction = transaction;
                            output.Timelines = timelines;

                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {

                transaction = null;
                timelines = null;
            }
        }
        //end basri 

        // add Chandra : 20151123
        /// <summary>
        /// Get payment details of transaction by WF Instance ID
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <returns></returns>
        [ResponseType(typeof(TMOMakerDetailModel))]
        [HttpGet]
        [Route("api/WorkflowTMO/{workflowInstanceID}/TrackingTMO/Checker/{approverID}")]
        public HttpResponseMessage GetTrackingTMOCheckerDetails(Guid workflowInstanceID, long approverID)
        {
            TransactionTMOModel transaction = new TransactionTMOModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TMOTrackingCheckerDetailModel output = new TMOTrackingCheckerDetailModel();
            TransactionTMOCheckerDetailModel outputChecker = new TransactionTMOCheckerDetailModel();

            try
            {
                // #1 Get TMO Details
                if (!repoTMO.GetTrackingTMODetails(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoTMO.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        if (!repoTMO.GetTMODealCheckerDetails(workflowInstanceID, approverID, ref outputChecker, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Transaction = transaction;
                            output.Timelines = timelines;
                            output.Checker = outputChecker.Checker;
                            output.Verify = outputChecker.Verify;

                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {
                transaction = null;
                timelines = null;
            }
        }
        /// <summary>
        /// Save approval data from checker
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approver ID from Nintex WF</param>
        /// <param name="transactionTMOChecker">TMO checker data</param>
        /// <returns></returns>
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowTMO/{workflowInstanceID}/TrackingTMO/Checker/{approverID}")]
        public HttpResponseMessage AddTMOCheckerDetails(Guid workflowInstanceID, long approverID, TransactionTMOTrackingCheckerDetailModel transactionChecker)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoTMO.AddTMODealChecker(workflowInstanceID, approverID, transactionChecker, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been saved." });
                }
            }
        }
        [ResponseType(typeof(TMOMakerDetailModel))]
        [HttpGet]
        [Route("api/WorkflowTMO/{workflowInstanceID}/TrackingTMO/Maker/{approverID}")]
        public HttpResponseMessage GetTrackingTMOMakerDetails(Guid workflowInstanceID, long approverID)
        {
            TransactionTMOModel transaction = new TransactionTMOModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TMOTrackingCheckerDetailModel output = new TMOTrackingCheckerDetailModel();
            TransactionTMOCheckerDetailModel outputChecker = new TransactionTMOCheckerDetailModel();

            try
            {
                // #1 Get TMO Details
                if (!repoTMO.GetTrackingTMOMakerDetails(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoTMO.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        if (!repoTMO.GetTMODealCheckerDetails(workflowInstanceID, approverID, ref outputChecker, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Transaction = transaction;
                            output.Timelines = timelines;
                            output.Checker = outputChecker.Checker;
                            output.Verify = outputChecker.Verify;

                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {
                transaction = null;
                timelines = null;
            }
        }
        /// <summary>
        /// Save approval data from Maker
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approver ID from Nintex WF</param>
        /// <param name="transactionMaker">TMO checker data</param>
        /// <returns></returns>
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowTMO/{workflowInstanceID}/TrackingTMO/Maker/{approverID}")]
        public HttpResponseMessage AddTMOMakerDetails(Guid workflowInstanceID, long approverID, TransactionTMOModel transactionMaker)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoTMO.AddTMODealMaker(workflowInstanceID, approverID, transactionMaker, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been saved." });
                }
            }
        }

        /// <summary>
        /// Save approval data from Maker
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approver ID from Nintex WF</param>
        /// <param name="transactionMaker">TMO checker data</param>
        /// <returns></returns>
        /// 
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowTMO/{workflowInstanceID}/TrackingTMO/SubmitMaker/{approverID}")]
        public HttpResponseMessage AddTMOSubmitMakerDetails(Guid workflowInstanceID, long approverID, TransactionTMOModel transactionMaker)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoTMO.AddTMODealSubmitMaker(workflowInstanceID, approverID, transactionMaker, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been saved." });
                }
            }
        }

        /// <summary>
        /// Save approval data from Maker
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approver ID from Nintex WF</param>
        /// <param name="transactionMaker">TMO checker data</param>
        /// <returns></returns>
        /// 
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowTMO/{workflowInstanceID}/TrackingTMO/ReopenMaker/{approverID}")]
        public HttpResponseMessage AddTMOReopenMakerDetails(Guid workflowInstanceID, long approverID, TransactionTMOModel transactionMaker)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoTMO.AddTMODealReopenMaker(workflowInstanceID, approverID, transactionMaker, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been saved." });
                }
            }
        }


        /// <summary>
        /// Save submit approval data from TMO Checker
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approver ID from Nintex WF</param>
        /// <param name="transaction">TMO checker data</param>
        /// <returns></returns>
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowTMO/{workflowInstanceID}/TrackingTMO/CheckerSubmit/{approverID}")]
        public HttpResponseMessage UpdateTMOChecker(Guid workflowInstanceID, long approverID, TransactionTMOTrackingCheckerDetailModel transaction)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoTMO.UpdateCheckerSubmit(workflowInstanceID, approverID, transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been saved." });
                }
            }
        }

        //add by fandi
        //[ResponseType(typeof(object))]
        //[HttpPost]
        //[Route("api/WorkflowTMO/{workflowInstanceID}/SubmiteRevise/Maker/{approverID}")]
        //Remark by arsel, TMOCSODetailModel has not been check in 
        //public HttpResponseMessage SubmitReviseFromCSO(Guid workflowInstanceID, long approverID, TMOCSODetailModel transaction)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        //    }
        //    else
        //    {
        //        if (!repoTMO.UpdateSubmitReviseFromCSOTMO(workflowInstanceID, approverID, transaction, ref message))
        //        {
        //            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
        //        }
        //        else
        //        {
        //            return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction Hass Been Update." });
        //        }
        //    }
        //}
        //end

        /// <summary>
        /// Cancelation approval data from TMO Maker
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approver ID from Nintex WF</param>
        /// <param name="transaction">TMO checker data</param>
        /// <returns></returns>
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowTMO/{workflowInstanceID}/TrackingTMO/CheckerCancelation/")]
        public HttpResponseMessage CancelTMOMaker(Guid workflowInstanceID)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoTMO.UpdateCancelation(workflowInstanceID, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction Maker has been canceled." });
                }
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowTMO/{workflowInstanceID}/TMO/Checker/{approverID}")]
        public HttpResponseMessage AddTransactionCheckerDetailsTMOChecker(Guid workflowInstanceID, long approverID, TMOCheckerDetailModel tmochecker)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoTMO.AddTransactionTMOChecker(workflowInstanceID, approverID, tmochecker, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been saved." });
                }
            }
        }
        /// <param name="disposing"></param>

        [ResponseType(typeof(bool))]
        [HttpGet]
        [Route("api/WorkflowTMO/IsCSO/{userName}")]
        public HttpResponseMessage IsCSO(string userName)
        {
            bool isCso = false;
            try
            {
                if (!repoTMO.IsCSO(userName, ref isCso, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, isCso);
                }
            }
            finally
            {

            }
        }
        protected override void Dispose(bool disposing)
        {
            if (repoTMO != null)
                repoTMO.Dispose();


            base.Dispose(disposing);
        }
    }
}

//=======================================================================================//
///FOR ALL DEVELOPER...
///PLEASE ADD NEW REGION BY YOUR NAME TO KEEP CLEAN !!! DONT BE A DIRTY PROGRAMMER!!!
///ANDIWIJAYA
//=======================================================================================//
