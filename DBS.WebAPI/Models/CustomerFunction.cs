﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Web;
using DBS.Entity;
using DBS.WebAPI.Models;
using DBS.WebAPI.Helpers;

namespace DBS.WebAPI.Models
{
    #region Property
    public class CustomerFunctionModel
    {
        /// <summary>
        /// Customer Indentification File (unique).
        /// </summary>
        public long ID { get; set; }

        public string CIF { get; set; }

        public POAFunctionModel POAFunction { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModifiedDate { get; set; }
    }

    public class CustomerFunctionRow : CustomerFunctionModel
    {
        public int RowID { get; set; }

    }

    public class CustomerFunctionGrid : Grid
    {
        public IList<CustomerFunctionRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class CustomerFunctionFilter : Filter { }
    #endregion

    #region Interface
    public interface ICustomerFunctionRepository : IDisposable
    {
        bool GetCustomerFunctionByID(int id, ref CustomerFunctionModel CustomerFunction, ref string message);
        bool GetCustomerFunction(ref IList<CustomerFunctionModel> CustomerFunctions, int limit, int index, ref string message);
        bool GetCustomerFunction(int page, int size, IList<CustomerFunctionFilter> filters, string sortColumn, string sortOrder, ref CustomerFunctionGrid CustomerFunction, ref string message);
        bool GetCustomerFunction(ref IList<CustomerFunctionModel> CustomerFunctions, ref string message);
        bool GetCustomerFunction(CustomerFunctionFilter filter, ref IList<CustomerFunctionModel> CustomerFunctions, ref string message);
        bool AddCustomerFunction(CustomerFunctionModel CustomerFunction, ref string message);
        bool UpdateCustomerFunction(int id, CustomerFunctionModel CustomerFunction, ref string message);
        bool DeleteCustomerFunction(int id, ref string message);
    }
    #endregion

    #region Repository
    public class CustomerFunctionRepository : ICustomerFunctionRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetCustomerFunctionByID(int id, ref CustomerFunctionModel CustomerFunction, ref string message)
        {
            bool isSuccess = false;

            try
            {
                CustomerFunction = (from a in context.CustomerFunctions
                              where a.IsDeleted.Equals(false) && a.CustomerFunctionID.Equals(id)
                              select new CustomerFunctionModel
                            {
                                ID = a.CustomerFunctionID,
                                CIF = a.CIF,
                                POAFunction = new POAFunctionModel{ ID = a.POAFunction.POAFunctionID, 
                                                                     Name = a.POAFunction.POAFunctionName, 
                                                                     Description = a.POAFunction.POAFunctionDescription},
                                LastModifiedDate = a.CreateDate,
                                LastModifiedBy = a.CreateBy
                            }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCustomerFunction(ref IList<CustomerFunctionModel> CustomerFunctions, ref string message)
        { 
         bool isSuccess = false;

            try
            {

                using (DBSEntities context = new DBSEntities())
                {
                    CustomerFunctions = (from a in context.CustomerFunctions
                                  where a.IsDeleted.Equals(false)
                                  orderby new { a.CustomerFunctionID }
                                  select new CustomerFunctionModel
                                  {
                                      ID = a.CustomerFunctionID,
                                      CIF = a.CIF,
                                      POAFunction = new POAFunctionModel
                                      {
                                          ID = a.POAFunction.POAFunctionID,
                                          Name = a.POAFunction.POAFunctionName,
                                          Description = a.POAFunction.POAFunctionDescription
                                      },
                                      LastModifiedBy = a.CreateBy,
                                      LastModifiedDate = a.CreateDate
                                  }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool GetCustomerFunction(ref IList<CustomerFunctionModel> CustomerFunction, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    CustomerFunction = (from a in context.CustomerFunctions
                                where a.IsDeleted.Equals(false)
                                  orderby new { a.CustomerFunctionID }
                                  select new CustomerFunctionModel
                                {
                                    ID = a.CustomerFunctionID,
                                    CIF = a.CIF,
                                    POAFunction = new POAFunctionModel
                                    {
                                        ID = a.POAFunction.POAFunctionID,
                                        Name = a.POAFunction.POAFunctionName,
                                        Description = a.POAFunction.POAFunctionDescription
                                    },
                                    LastModifiedBy = a.CreateBy,
                                    LastModifiedDate = a.CreateDate
                                }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCustomerFunction(int page, int size, IList<CustomerFunctionFilter> filters, string sortColumn, string sortOrder, ref CustomerFunctionGrid customer, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();
               
                CustomerFunctionModel filter = new CustomerFunctionModel();
                filter.POAFunction = new POAFunctionModel();
                if (filters != null)
                {
                    filter.CIF = (string)filters.Where(a => a.Field.Equals("POAFunctionCIF")).Select(a => a.Value).SingleOrDefault();
                    filter.POAFunction.Name = (string)filters.Where(a => a.Field.Equals("POAFunctionName")).Select(a => a.Value).SingleOrDefault();
                    filter.POAFunction.Description = (string)filters.Where(a => a.Field.Equals("POAFunctionDescription")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.CustomerFunctions
                                let isCIF = !string.IsNullOrEmpty(filter.CIF)
                                let isName = !string.IsNullOrEmpty(filter.POAFunction.Name)
                                let isDesc = !string.IsNullOrEmpty(filter.POAFunction.Description)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) &&
                                    (isCIF ? a.CIF.Contains(filter.CIF) : true) &&
                                    (isName ? a.POAFunction.POAFunctionName.Contains(filter.POAFunction.Name) : true) &&
                                    (isDesc ? a.POAFunction.POAFunctionDescription.Contains(filter.POAFunction.Description) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new CustomerFunctionModel
                                {
                                    ID = a.CustomerFunctionID,
                                    CIF = a.CIF,
                                    POAFunction = new POAFunctionModel
                                    {
                                        ID = a.POAFunction.POAFunctionID,
                                        Name = a.POAFunction.POAFunctionName,
                                        Description = a.POAFunction.POAFunctionDescription
                                    },
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    customer.Page = page;
                    customer.Size = size;
                    customer.Total = data.Count();
                    customer.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    customer.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new CustomerFunctionRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            CIF = a.CIF,
                            POAFunction = a.POAFunction,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCustomerFunction(CustomerFunctionFilter filter, ref IList<CustomerFunctionModel> customer, ref string message)
        {
            throw new NotImplementedException();
        }


        public bool AddCustomerFunction(CustomerFunctionModel CustomerFunction, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (!context.CustomerFunctions.Where(a => a.POAFunctionID.Equals(CustomerFunction.POAFunction.ID) && a.CIF.Equals(CustomerFunction.CIF) && a.IsDeleted.Equals(false)).Any())
                {
                    context.CustomerFunctions.Add(new Entity.CustomerFunction()
                    {
                        CIF = CustomerFunction.CIF,
                        POAFunctionID = CustomerFunction.POAFunction.ID,
                        CustomerFunctionID = CustomerFunction.ID,
                        IsDeleted = false,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    });
                    context.SaveChanges();

                    isSuccess = true;

                }
                else
                {
                    message = string.Format("Customer Function data for CustomerFunction Name {0} is already exist.", CustomerFunction.POAFunction.Name);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateCustomerFunction(int id, CustomerFunctionModel CustomerFunction, ref string message)
        {
            bool isSuccess = false;
            try
            {
                Entity.CustomerFunction data = context.CustomerFunctions.Where(a => a.CustomerFunctionID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.POAFunctionID = CustomerFunction.POAFunction.ID;
                    data.CustomerFunctionID = CustomerFunction.ID;
                    data.UpdateDate = DateTime.UtcNow;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Customer data for Contact ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeleteCustomerFunction(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.CustomerFunction data = context.CustomerFunctions.Where(a => a.CustomerFunctionID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Customer data for Function ID {0} is does not exist.", id.ToString());
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}

       