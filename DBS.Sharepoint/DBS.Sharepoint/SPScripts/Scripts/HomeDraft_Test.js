var accessToken;
var $box;
var $remove = false;
var cifData = '0';
var idrrate = 0;
var treshHold = 0;

var multiViewModel = null;

var _WorkflowInstanceID;
var _ApproverId;
var _SPTaskListItemID;
var _ActivityTitle;
var MasterUnderlyingApproval = "MasterUnderlying";
var CustomerCsoApproval = "CustomerCSO";
var CustomerCallbackApproval = "CustomerCallback";
// ROLES //
var TreeNavigation = function (el) {
    var self = this;

    self.UncheckHide = function () {
        $('#nestable > table > tbody > tr > td > input[id="cbHasAccess"]').each(function () {
            if (!$(this).prop('checked')) $(this).parent().parent().hide();
        });
        return false;
    }

    self.Readonly = false;
    self.parent = el;
    self.DataList = [];
    self.SetPanel = function (el) {
        self.parent = el;
    }

    self.Get = function () {
        result = new Array();
        $('table>tbody>tr>td>input', self.parent).each(function () {
            if ($(this).prop('checked')) {
                result.push({ "ID": $(this).parent().parent().attr('data-id') });
            }
        });
        return result;
    }
    self.Set = function (data) {
        $('table>tbody>tr>td>input', self.parent).each(function () {
            $(this).prop('checked', false);
        });
        $('table>tbody>tr>td>input', self.parent).each(function () {
            var chk = $(this);
            $.each(data, function (index, item) {
                if (item.ID == chk.parent().parent().attr('data-id')) {
                    chk.prop('checked', true);
                }
            });
        });
    }
    self.DoChecked = function () {
        if (self.Readonly) return false;
        var trs = $('table>tbody>tr');
        self.IsChecked(trs, 0, $(this), false, '');
    }
    self.ChildCount = function (trs, parent) {
        var result = 0;
        trs.each(function () {
            if ($(this).attr('data-parentid') == parent) result++;
        });
        return result;
    }
    self.IsChecked = function (trs, parent, chk, root, pad) {
        var result = false;
        var checkboxId = chk.attr('id');
        pad += '   ';
        trs.each(function () {
            if ($(this).attr('data-parentid') == parent) {
                //console.log(pad + '[' + $(this).attr('data-id') + ', ' + $(this).attr('data-parentid') + ']');
                var id = chk.parent().parent().attr('data-id');
                if ($(this).attr('data-parentid') == id) root = true;
                var checked = self.IsChecked(trs, $(this).attr('data-id'), chk, root, pad);
                if (root) {
                    $('td>#' + checkboxId, $(this)).prop('checked', chk.prop('checked'));
                } else {
                    if ($(this).attr('data-id') != id) {
                        if (self.ChildCount(trs, $(this).attr('data-id')) > 0)
                            $('td>#' + checkboxId, $(this)).prop('checked', checked);
                    }
                }
                result = result | $('td>#' + checkboxId, $(this)).prop('checked');
            }
        });
        return result;
    }
    self.Toggle = function () {
        var img = $(this);
        var parentId = $(this).parent().parent().attr('data-id');
        img.attr('class', img.attr('class') == 'icon-plus' ? 'icon-minus' : 'icon-plus');
        $('table>tbody>tr', self.parent).each(function () {
            if ($(this).attr('data-parentid') == parentId) {
                img.attr('class') == 'icon-plus' ? $(this).hide() : $(this).show();
            }
        });
    }
    self.RendRow = function (data, parent, pad) {
        var result = '';
        $.each(data, function (index, item) {
            if (item.ParentID == parent) {
                var subs = self.RendRow(data, item.ID, pad + '&nbsp;&nbsp;&nbsp');
                var icon = '&nbsp;&nbsp;&nbsp;';
                if (subs != '') icon = '<i class="icon-minus"></i>';

                result +=
                '		<tr data-id="' + item.ID + '" data-parentid="' + item.ParentID + '">' +
                '			<td>' + icon + '&nbsp;' + pad + item.Title + '</td>' +
                '			<td><input id="cbHasAccess" type="checkbox"/></td>' +
                '		</tr>';
                result += subs;
            }
        });
        return result;
    }
    self.Rend = function (data) {

        table = self.parent.html(
'<table class="table table-bordered table-hover dataTable">' +
'	<thead>' +
'		<tr>' +
'			<th>Name</th>' +
'			<th>Check</th>' +
'		</tr>' +
'	</thead><tbody>' + self.RendRow(data, '', '') + '</tbody>' +
'</table>'
);
        $('table>tbody>tr>td>#cbHasAccess', self.parent).on('click', self.DoChecked);
        $('table>tbody>tr>td>i', self.parent).on('click', self.Toggle);
    }
    self.Load = function (callback) {
        $.ajax({
            type: "GET",
            url: api.url.roledraftnavigation,
            data: {},
            contentType: "application/json",
            headers: {
                //"Authorization" : "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                self.Rend(data);
                if (callback) callback(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }
    self.IsHasChild = function (data, parentID) {
        result = false;
        $.each(data, function (index, item) {
            if (item.ParentID == parentID) result = true;
        });
        return result;
    }
};

var RoleViewModel = function () {
    var self = this;

    self.IsActive = ko.observable(false);
    // New Data flag
    self.Readonly = ko.observable(true);
    self.IsNewData = ko.observable(false);

    //Declare observable which will be bind with UI
    self.ID = ko.observable("");
    self.MasterID = ko.observable("");
    self.Name = ko.observable("");
    self.Description = ko.observable("");
    self.LastModifiedBy = ko.observable("");
    self.LastModifiedDate = ko.observable("");
    self.IsDeleted = ko.observable(false);
    self.UserCategoryCode = ko.observable("");

    self.treeNavigation = new TreeNavigation();

    self.Load = function (id, callback) {
        self.treeNavigation.SetPanel($('#nestable'));
        self.treeNavigation.Load(function () {
            $.ajax({
                type: "GET",
                url: api.server + "api/RoleDraft" + "/" + id,
                headers: {
                    "Authorization": "Bearer " + accessToken
                },
                success: function (data, textStatus, jqXHR) {
                    // send notification
                    if (jqXHR.status = 200) {
                        //console.log(data);
                        self.Readonly(data.ActionType == null ? false : true);
                        self.treeNavigation.Readonly = data.ActionType == null ? false : true;
                        self.IsNewData(false);
                        self.ID(data.ID);
                        self.MasterID(data.ID);
                        self.Name(data.Name);
                        self.Description(data.Description);
                        self.LastModifiedBy(data.LastModifiedBy);
                        self.LastModifiedDate(data.LastModifiedDate);
                        self.treeNavigation.Set(data.Menus);
                        self.treeNavigation = new TreeNavigation();
                        self.treeNavigation.UncheckHide();
                        self.UserCategoryCode(data.UserCategoryCode);
                        //$('#modal-form-approval').modal('show');
                        if (callback) callback(data);
                    } else
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // send notification
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            });
        });
    }
}

// USERS //
var RankModel = function (id, code, desc) {
    var self = this;
    self.ID = ko.observable(id);
    self.Code = ko.observable(code);
    self.Description = ko.observable(desc);
}

var SegmentModel = function (id, name, desc) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
    self.Description = ko.observable(desc);
}

var CustomerTypeModel = function (id, name) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
}

var UserApprovalModel = function (id, name, beneid, rankid, branchid) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
    self.SegmentID = ko.observable(beneid);
    self.RankID = ko.observable(rankid);
    self.BranchID = ko.observable(branchid);
    self.FullName = ko.observable(id + ' - ' + name);
    self.Email = '';
}

var BranchModel = function (id, name) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
}

var FunctionRoleModel = function (id, name) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
}

var RoleModel = function (id, name, desc) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
    self.Description = ko.observable(desc);
}

var UserCategoryModel = function (code) {
    var self = this;

    self.UserCategoryCode = ko.observable(code);

}

var UserViewModel = function () {
    var self = this;

    //start azam
    //  self.Employee = ko.observable(new EmployeeModel('', '', '', ''));
    //end azam

    self.IsActive = ko.observable(false);
    // New Data flag
    self.Readonly = ko.observable(true);
    self.IsNewData = ko.observable(false);

    //Declare observable which will be bind with UI
    self.EmployeeViewname = ko.observable("");
    self.EmployeeUsername = ko.observable("");
    self.EmployeeName = ko.observable("");
    self.EmployeeEmail = ko.observable("");
    self.Rank = ko.observable(new RankModel('', '', ''));
    self.Segment = ko.observable(new SegmentModel('', '', ''));
    self.Branch = ko.observable(new BranchModel('', ''));
    self.Type = ko.observable(new CustomerTypeModel('', ''));
    self.FunctionRole = ko.observable(new FunctionRoleModel('', ''));
    self.Role = ko.observable(new RoleModel('', '', ''));
    self.LastModifiedBy = ko.observable("");
    self.LastModifiedDate = ko.observable("");
    self.UserCategoryCode = ko.observable(new UserCategoryModel(''));

    //Dropdown
    self.ddlBranch = ko.observableArray([]);
    self.ddlRank = ko.observableArray([]);
    self.ddlSegment = ko.observableArray([]);
    self.ddlFunctionRole = ko.observableArray([]);
    self.ddlUserCategoryCode = ko.observableArray([]);
    //Select
    self.Role = ko.observableArray([]);

    //Data Employee
    self.Employee = ko.observable();

    self.ClearDDL = function () {
        self.Rank(new RankModel('', '', ''));
        self.Segment(new SegmentModel('', '', ''));
        self.Branch(new BranchModel('', ''));
        self.FunctionRole(new FunctionRoleModel('', ''));
        self.UserCategoryCode(new UserCategoryModel(''));
    }


    self.Load = function (id, callback) {

        GetDropdown(function (data) {
            self.ClearDDL();
            $.ajax({
                type: "GET",
                url: api.server + api.url.userapproval + "/Draft/" + id,
                headers: {
                    "Authorization": "Bearer " + accessToken
                },
                success: function (data, textStatus, jqXHR) {
                    // send notification
                    if (jqXHR.status = 200) {
                        self.Employee(ko.toJSON(data));
                        self.EmployeeUsername(data.EmployeeUsername);
                        self.EmployeeName(data.EmployeeName);
                        self.EmployeeEmail(data.EmployeeEmail);
                        self.Rank(new RankModel(data.RankID, data.RankCode, data.RankDescription));
                        self.Segment(new SegmentModel(data.SegmentID, data.SegmentName, data.SegmentDescription));
                        self.Branch(new BranchModel(data.Branch.ID, data.Branch.Name));
                        self.FunctionRole(new FunctionRoleModel(data.FunctionRole.ID, data.FunctionRole.Name));
                        self.UserCategoryCode(new UserCategoryModel(data.UserCategoryCode));

                        var EmployeeViewname = GetViewname(data.EmployeeUsername);
                        self.EmployeeViewname(EmployeeViewname);

                        $.each(data.Roles, function (index, item) {
                            $(':checkbox[value=' + item.ID + ']', $('#user-role-table')).prop('checked', true);
                        });

                        /*$.each(data.Roles,function(index,item){
							!$(':checkbox[value='+item.ID+']',$('#user-role-table')).prop('checked',true);
							if(!$(':checkbox',$('#user-role-table')).prop('checked')) { $(':checkbox[value='+item.ID+']',$('#user-role-table')).parent().parent().hide(); }
							//if(!$(this).prop('checked'))$(this).parent().parent().hide();
						});*/

                        $('#user-role-table > tbody > tr > td > label > input[type="checkbox"]').each(function () {
                            if (!$(this).prop('checked')) $(this).parent().parent().parent().hide();
                        });

                        self.LastModifiedBy(data.LastModifiedBy);
                        self.LastModifiedDate(data.LastModifiedDate);
                        self.treeNavigation = new TreeNavigation();
                        self.treeNavigation.UncheckHide();
                        //$('#modal-form-approval').modal('show');
                        if (callback) callback(data);
                    } else
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // send notification
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            });
        });
    }

    function GetViewname(str) {
        if (str.indexOf("|") != -1)
            result = str.substring(str.lastIndexOf("|") + 1);
        else
            result = str;

        return result;
    }

    function GetDropdown(callback) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.parameter + '?select=BizSegment,Branch,Rank,FunctionRole,UserCategory,Role',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.ddlSegment(data['BizSegment']);
                    self.ddlBranch(data['Branch']);
                    self.ddlRank(data['Rank']);
                    self.ddlFunctionRole(data['FunctionRole']);

                    self.ddlUserCategoryCode(data['UserCategory']);

                    self.Role(data['Role']);
                    if (callback) callback(data);
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }
}

//Underlying Checker Maker Agung
var StatementLetterModel = function (id, name) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
}

var UnderlyingDocumentModel = function (id, name, code) {
    var self = this;
    self.ID = ko.observable(id);
    self.Code = ko.observable(code);
    self.Name = ko.observable(name);
    self.CodeName = ko.observable(code + ' - ' + name);
    self.LastModifiedDate = ko.observable('');
    self.LastModifiedBy = ko.observable("");
}

var DocumentTypeModel = function (id, name) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
    self.LastModifiedDate = ko.observable('');
    self.LastModifiedBy = ko.observable("");
}

var CurrencyModel = function (id, code) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(code);
    //self.Description = ko.observable(desc);
    //self.FullName = ko.observable(code + ' (' + desc + ')');
}

//start azam
var EmployeeModel = function (id, name, username, email) {
    var self = this;
    self.EmployeeID = ko.observable(id);
    self.EmployeeName = ko.observable(name);
    self.EmployeeUserName = ko.observable(username);
    self.EmployeeEmail = ko.observable(email);
}
var POAEmailModel = function (POAEmailProductID, ProductName) {
    var self = this;
    self.POAEmailProductID = ko.observable(POAEmailProductID);
    self.POAEmail = ko.observable();
    self.ProductName = ko.observable(ProductName);
    self.CreateBy = ko.observable();
    self.UpdateBy = ko.observable();
    self.CreateDate = ko.observable();
    self.UpdateDate = ko.observable();
    self.CIF = ko.observable(cifData)

}
var CustomerPOAEmailViewModel = function () {
    var self = this;
    self.Cif = ko.observable();
    self.POAEmailTemp = ko.observableArray([]);
    self.cblPOAEmailProduct = ko.observableArray([]);
    self.IsActive = ko.observable(false);
    self.IsNewDataPOAEmail = ko.observable(false);
    self.Load = function (id, callback) {
        self.GetPOAEmailProduct();
        $.ajax({
            type: "GET",
            url: api.server + "api/WorkflowEntity" + "/" + id,
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                // send notification
                if (jqXHR.status = 200) {
                    var rec = eval('(' + data.Data + ')');

                    self.POAEmailTemp(rec);
                    var length = self.cblPOAEmailProduct().length;
                    for (var i = 0; i < length; i++) {
                        if (self.POAEmailTemp().POAEmail[i]) {
                            $('#POAEmailcheckBox' + self.POAEmailTemp().POAEmail[i].POAEmailProductID).prop('checked', true);
                        }
                        else {
                            $('#POAEmailcheckBox' + '0').prop('checked', false);
                        }
                    }


                } else
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    self.GetPOAEmailProduct = function () {
        var options = {
            url: api.server + api.url.poaemailProduct + "/MasterProduct",
            token: accessToken
        };
        Helper.Ajax.Get(options, OnSuccessGetPOAEmailProduct, OnError, OnAlways);

    }
    function OnSuccessGetPOAEmailProduct(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.cblPOAEmailProduct(data);
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }
}
var CustomerCsoViewModel = function () {
    var self = this;
    self.Cif = ko.observable();
    self.Employee = ko.observable(new EmployeeModel('', '', '', ''));
    self.IsActive = ko.observable(false);
    self.IsNewDataCso = ko.observable(false);



    self.Load = function (id, callback) {
        $.ajax({
            type: "GET",
            url: api.server + "api/WorkflowEntity" + "/" + id,
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                // send notification
                if (jqXHR.status = 200) {
                    //console.log(data);

                    var rec = eval('(' + data.Data + ')');

                    //self.Cif(rec.CIF);
                    // self.EmployeeID(rec.Employee.EmployeeID);
                    //self.EmployeeName(rec.Employee.EmployeeName);
                    // self.EmployeeEmail(rec.Employee.EmployeeEmail);
                    self.Employee(new EmployeeModel(rec.Employee.EmployeeID, rec.Employee.EmployeeName, '', rec.Employee.EmployeeEmail));



                } else
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }
}

//end azam

var CustomerCallbackViewModel = function () {
    var self = this;
    self.CIF = ko.observable();
    self.CustomerName = ko.observable();
    self.ContactName = ko.observable();
    self.ProductName = ko.observable();
    self.Time = ko.observable();
    self.TimeLocal = ko.observable();
    self.IsUTC = ko.observable(false);
    self.Remark = ko.observable();
    self.IsActive = ko.observable(false);

    self.Load = function (id, callback) {
        $.ajax({
            type: "GET",
            url: api.server + "api/WorkflowEntity" + "/" + id,
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                // send notification
                if (jqXHR.status = 200) {
                    
                    var rec = eval('(' + data.Data + ')');
                    self.CIF(rec.CIF);
                    self.CustomerName(rec.CustomerName);
                    self.ContactName(rec.ContactName);
                    self.ProductName(rec.ProductName);
                    self.TimeLocal(Helper.LocalDate(rec.Time));
                    self.Time(rec.Time);
                    self.IsUTC(rec.IsUTC);
                    self.Remark(rec.Remark);
                    self.IsActive = ko.observable(false);
                }
                else
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }
}

var UnderlyingViewModel = function () {
    var self = this;
    //self.ID = ko.observable();
    $(".date-picker").each(function () {
        $(this).datepicker({ autoclose: true }).next().on(ace.click_event, function () {
            $(this).prev().focus();
        });
    });
    self.FormatNumber = function (num) {
        if (ko.isObservable(num)) {
            num = num();
        }
        if (num != null) {
            num = num.toString().replace(/,/g, '');
            num = parseFloat(num).toFixed(2);
            var n = num.toString(), p = n.indexOf('.');
            return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
                return p < 0 || i < p ? ($0 + ',') : $0;
            });
        } else { return 0; }
    }

    self.Branch = ko.observable(new BranchModel('', ''));
    self.BizSegment = ko.observable(new SegmentModel('', '', ''));
    self.Type = ko.observable(new CustomerTypeModel('', ''));
    self.UserApproval = ko.observable(new UserApprovalModel(0, '', 0, 0, 0));

    self.IsCitizen = ko.observable();
    self.IsResident = ko.observable();
    self.IsDormant = ko.observable();
    self.IsFreeze = ko.observable();

    self.IsNewDataUnderlying = ko.observable(false);
    self.IsRoleMaker = ko.observable(false);
    self.IsPermited = ko.observable(false);
    self.CIFJointAccounts = ko.observableArray([{ ID: false, Name: "Single" }, { ID: true, Name: "Join" }]);
    self.IsJointAccount = ko.observable(false);
    self.JointAccountNumbers = ko.observableArray([]);
    self.IsActive = ko.observable(false);
    self.ID_u = ko.observable();
    self.CIF_u = ko.observable("");
    self.StatementLetter_u = ko.observable(new StatementLetterModel('', ''));
    self.UnderlyingDocument_u = ko.observable(new UnderlyingDocumentModel('', ''));
    self.ddlUnderlyingDocument_u = ko.observableArray([]);
    self.DocumentType_u = ko.observable(new DocumentTypeModel('', ''));
    self.ddlDocumentType_u = ko.observableArray([]);
    self.Currency_u = ko.observable(new CurrencyModel('', ''));
    self.ddlCurrency_u = ko.observableArray([]);
    self.Amount_u = ko.observable(0);
    self.formattedAmount_u = ko.computed({
        read: function () {
            var n = self.Amount_u().toString(), p = n.indexOf('.');
            n = n.replace(/,/g, "");
            //var n = value.toString(), p = n.indexOf('.');
            var nVal = n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
                return p < 0 || i < p ? ($0 + ',') : $0;
            });
            self.Amount_u(nVal);
            //return self.Amount_u().toFixed(2);
        },
        write: function (value) {
            // Strip out unwanted characters, parse as float, then write the raw data back to the underlying "price" observable
            // value = parseFloat(value.replace(/[^\.\d]/g, ""));
            var n = value.toString(), p = n.indexOf('.');
            n = n.replace(/,/g, "");
            var nVal = n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
                return p < 0 || i < p ? ($0 + ',') : $0;
            });
            self.Amount_u(nVal); // Write to underlying storage
        },
        owner: self
    });
    self.tmpAccount = ko.observable();
    self.Rate_u = ko.observable(0);
    self.DateOfUnderlying_u = ko.observable("");
    self.ExpiredDate_u = ko.observable("");
    self.AmountUSD_u = ko.observable(0);
    self.ReferenceNumber_u = ko.observable("");
    self.SupplierName_u = ko.observable("");
    self.InvoiceNumber_u = ko.observable("");
    self.IsJointAccount_u = ko.observable(false);
    self.ddlStatementLetter_u = ko.observableArray([]);
    self.DynamicStatementLetter_u = ko.observableArray([]);
    self.AccountNumber_u = ko.observable();
    self.IsTMO = ko.observable(false);
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        if (ko.isObservable(date)) {
            date = date();
        }
        /*
        var localDate = new Date(date);
        if (date == '1970/01/01 00:00:00' || date == null) {
            return "";
        }
        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return ""
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-17
    };
    self.OnChangeStatementLetterUnderlying = function () {
        var SelectedStatementLetterID = $('.statement-letter-id option:selected').val();
        //console.log(SelectedStatementLetterID);
        self.StatementLetter_u().ID(Number(SelectedStatementLetterID.trim()));
        if (self.StatementLetter_u().ID() == CONST_STATEMENT.StatementA_ID) {
            self.Currency_u().ID(1);
            self.Amount_u(treshHold);
            var date = new Date();
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            var day = lastDay.getDate();
            var month = lastDay.getMonth() + 1;
            var year = lastDay.getFullYear();
            if (month > 12) {
                month -= 12;
                year += 1;
            }
            var fixLastDay = year + "/" + month + "/" + day;
            self.ExpiredDate_u(self.LocalDate(fixLastDay, true, false));

            SetDefaultValueStatementA(); // set default value untuk statement A
            GetRateIDRAmount(1); // USD
        } else if (self.StatementLetter_u().ID() == CONST_STATEMENT.AnnualStatement_ID) {
            self.Currency_u().ID(1);
            self.Amount_u(0);
            var date = new Date();
            var lastDay = new Date(date.getFullYear(), 12, 0);
            var day = lastDay.getDate();
            var month = lastDay.getMonth() + 1;
            var year = lastDay.getFullYear();
            if (month > 12) {
                month -= 12;
                year += 1;
            }
            var fixLastDay = year + "/" + month + "/" + day;
            self.ExpiredDate_u(self.LocalDate(fixLastDay, true, false));
            SetDefaultValueStatementA();
        }

        else if (self.StatementLetter_u().ID() == CONST_STATEMENT.StatementB_ID && self.DateOfUnderlying_u() != "" && self.IsNewDataUnderlying()) {
            if (self.LocalDate(self.DateOfUnderlying_u()) != "") {
                var date = new Date(self.DateOfUnderlying_u());
                var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                var day = date.getDate();
                var month = lastDay.getMonth() + 13;
                var year = lastDay.getFullYear();
                if (month > 12) {
                    month -= 12;
                    year += 1;
                }
                var fixLastDay = year + "/" + month + "/" + day;
                if (!moment(fixLastDay, 'YYYY/MM/DD').isValid()) {
                    var LastDay_ = new Date(year, month, 0);
                    day = LastDay_.getDate();
                }
                self.ExpiredDate_u(self.LocalDate(fixLastDay, true, false));
            }
        }
        if (typeof self.Amount_u() != 'number') {
            var value = parseFloat(self.Amount_u().replace(',', '').replace(',', '').replace(',', '').replace(',', ''));
        } else {
            var value = parseFloat(self.Amount_u());
        }
        var res = parseFloat(value) * parseFloat(self.Rate_u()) / parseFloat(idrrate);
        res = Math.round(res * 100) / 100;
        res = isNaN(res) ? 0 : res; //avoid NaN
        self.AmountUSD_u(parseFloat(res).toFixed(2));
    }
    self.OnUnderlyingDocChange = function () {
        var SelectedUnderlyingDocID = $('.underlying-document-id option:selected').val();
        //console.log(SelectedUnderlyingDocID);
        self.UnderlyingDocument_u().ID(Number(SelectedUnderlyingDocID.trim()));
    };

    self.OnTypeOfDocChange = function () {
        var SelectedTypeOfDocID = $('.document-type-id option:selected').val();
        //console.log(SelectedTypeOfDocID);
        self.DocumentType_u().ID(Number(SelectedTypeOfDocID.trim()));
    };
    self.onChangeDateOfUnderlying = function () {
        if (self.StatementLetter_u().ID() == CONST_STATEMENT.StatementA_ID) {
            self.Currency_u().ID(1);
            self.Amount_u(treshHold);
            var date = new Date();
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            var day = lastDay.getDate();
            var month = lastDay.getMonth() + 1;
            var year = lastDay.getFullYear();
            if (month > 12) {
                month -= 12;
                year += 1;
            }
            var fixLastDay = year + "/" + month + "/" + day;
            self.ExpiredDate_u(self.LocalDate(fixLastDay, true, false));

            //SetDefaultValueStatementA(); // set default value untuk statement A
            GetRateIDRAmount(1); // USD
        } else if (self.StatementLetter_u().ID() == CONST_STATEMENT.AnnualStatement_ID) {
            self.Currency_u().ID(1);
            self.Amount_u(0);
            var date = new Date();
            var lastDay = new Date(date.getFullYear(), 12, 0);
            var day = lastDay.getDate();
            var month = lastDay.getMonth() + 1;
            var year = lastDay.getFullYear();
            if (month > 12) {
                month -= 12;
                year += 1;
            }
            var fixLastDay = year + "/" + month + "/" + day;
            self.ExpiredDate_u(self.LocalDate(fixLastDay, true, false));
            //SetDefaultValueStatementA();
        }

        else if (self.StatementLetter_u().ID() == CONST_STATEMENT.StatementB_ID && self.DateOfUnderlying_u() != "" && self.IsNewDataUnderlying()) {
            if (self.LocalDate(self.DateOfUnderlying_u()) != "") {
                var date = new Date(self.DateOfUnderlying_u());
                var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                var day = date.getDate();
                var month = lastDay.getMonth() + 13;
                var year = lastDay.getFullYear();
                if (month > 12) {
                    month -= 12;
                    year += 1;
                }
                var fixLastDay = year + "/" + month + "/" + day;
                if (!moment(fixLastDay, 'YYYY/MM/DD').isValid()) {
                    var LastDay_ = new Date(year, month, 0);
                    day = LastDay_.getDate();
                }
                self.ExpiredDate_u(self.LocalDate(fixLastDay, true, false));
            }
        }
        if (typeof self.Amount_u() != 'number') {
            var value = parseFloat(self.Amount_u().replace(',', '').replace(',', '').replace(',', '').replace(',', ''));
        } else {
            var value = parseFloat(self.Amount_u());
        }
        var res = parseFloat(value) * parseFloat(self.Rate_u()) / parseFloat(idrrate);
        res = Math.round(res * 100) / 100;
        res = isNaN(res) ? 0 : res; //avoid NaN
        self.AmountUSD_u(parseFloat(res).toFixed(2));
    }

    self.OtherUnderlying_u = ko.observable('');
    self.CodeUnderlying = ko.computed({
        read: function () {
            if (self.UnderlyingDocument_u().ID() != undefined && self.UnderlyingDocument_u().ID() != '') {
                var item = ko.utils.arrayFirst(self.ddlUnderlyingDocument_u(), function (x) {
                    return self.UnderlyingDocument_u().ID() == x.ID();
                }
                )
                if (item != null) {
                    return item.Code();
                }
                else {
                    return ''
                }
            } else {
                return ''
            }
        },
        write: function (value) {
            return value;
        }
    });

    self.OnChangeJointAcc = function (obj, event) {
        OnChangeJointAcc(obj);
    }
    self.CheckEmptyAccountNumber = function () {
        var accNumber = multiViewModel.UnderlyingView().CustomerUnderlying().AccountNumber() != null ? multiViewModel.UnderlyingView().CustomerUnderlying().AccountNumber() : null;
        if (accNumber != null) {
            //SetStatementA(true,accNumber);
        }
    }
    function OnChangeJointAcc() {
        if (!CustomerUnderlying.IsJointAccount()) {
            CustomerUnderlying.AccountNumber(null);
            //SetStatementA(false,null);
        }

    }

    var CustomerUnderlying = {
        ID: self.ID_u,
        CIF: self.CIF_u,
        CustomerName: self.CustomerName_u,
        UnderlyingDocument: self.UnderlyingDocument_u,
        DocumentType: self.DocumentType_u,
        Currency: self.Currency_u,
        Amount: self.Amount_u,
        Rate: self.Rate_u,
        AmountUSD: self.AmountUSD_u,
        AvailableAmountUSD: self.AvailableAmount_u,
        AttachmentNo: self.AttachmentNo_u,
        StatementLetter: self.StatementLetter_u,
        IsDeclarationOfException: self.IsDeclarationOfException_u,
        //StartDate: self.StartDate_u,
        //EndDate: self.EndDate_u,
        DateOfUnderlying: self.DateOfUnderlying_u,
        ExpiredDate: self.ExpiredDate_u,
        ReferenceNumber: self.ReferenceNumber_u,
        SupplierName: self.SupplierName_u,
        InvoiceNumber: self.InvoiceNumber_u,
        IsSelectedProforma: self.IsSelectedProforma,
        IsProforma: self.IsProforma_u,
        IsUtilize: self.IsUtilize_u,
        IsJointAccount: self.IsJointAccount_u,
        AccountNumber: self.AccountNumber_u,
        Proformas: self.Proformas,
        IsSelectedBulk: false,
        IsBulkUnderlying: false,
        BulkUnderlyings: self.BulkUnderlyings,
        OtherUnderlying: self.OtherUnderlying_u,
        LastModifiedBy: self.LastModifiedBy_u,
        LastModifiedDate: self.LastModifiedDate_u
    };
    self.CustomerUnderlying = ko.observable(CustomerUnderlying);
    self.Load = function (id, callback) {
        $.ajax({
            type: "GET",
            url: api.server + "api/CustomerUnderlyingDraft" + "/" + id,
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                // send notification
                if (jqXHR.status = 200) {

                    //console.log(data);
                    self.ID_u(data.ID);
                    self.CIF_u(data.CIF);
                    self.StatementLetter_u(new StatementLetterModel(data.StatementLetter.ID, data.StatementLetter.Name));
                    self.UnderlyingDocument_u(new UnderlyingDocumentModel(data.UnderlyingDocument.ID, data.UnderlyingDocument.Name));
                    self.DocumentType_u(new DocumentTypeModel(data.DocumentType.ID, data.DocumentType.Name));
                    self.Currency_u(new CurrencyModel(data.Currency.ID, data.Currency.Code));
                    self.Amount_u(data.Amount);
                    self.Rate_u(data.Rate);
                    self.AmountUSD_u(data.AmountUSD);
                    self.DateOfUnderlying_u(self.LocalDate(data.DateOfUnderlying, true, false));
                    self.ExpiredDate_u(self.LocalDate(data.ExpiredDate, true, false));
                    self.ReferenceNumber_u(data.ReferenceNumber)
                    self.SupplierName_u(data.SupplierName);
                    self.InvoiceNumber_u(data.InvoiceNumber);
                    self.IsJointAccount_u(data.IsJointAccount);
                    self.AccountNumber_u(data.AccountNumber);
                    self.IsTMO(data.IsTMO);
                    if (callback) callback(data);
                    self.tmpAccount(null);
                    self.tmpAccount(data.AccountNumber);
                    cifData = data.CIF;
                    GetDataByCIF(cifData);
                } else
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }
    function GetDataByCIF(sCIF) {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customer + "/" + sCIF,
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetDataCustomer, OnError, OnAlways);
    }
    function OnSuccessGetDataCustomer(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {

            //console.log(data);       	
            self.IsJointAccount(true);
            self.JointAccountNumbers(ko.utils.arrayFilter(data.Accounts, function (item) { return item.IsJointAccount == true }));
            JoinAccountNumber = ko.utils.arrayFilter(data.Accounts, function (item) { return item.IsJointAccount == true });
            // checking havd joint account
            var isJointAcc = ko.utils.arrayFilter(data.Accounts, function (item) {
                return true == item.IsJointAccount;
            });
            if (isJointAcc != null & isJointAcc.length == 0) {
                self.IsJointAccount(false);
            }
            self.IsCitizen(data.IsCitizen);
            self.IsResident(data.IsResident);
            self.IsDormant(data.IsDormant);
            self.IsFreeze(data.IsFreeze);
            self.AccountNumber_u(self.tmpAccount());
        }
        else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }
    self.IsPermited = function (IsPermitedResult) {
        //Ajax call to delete the Customer        
        spUser = $.cookie(api.cookie.spUser);
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/CheckUserPermited/Home",
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    //compare user role to page permission to get checker validation
                    //console.log("spUser Roles : " + ko.toJSON(spUser.Roles));
                    //console.log("spUser Permited : " + data);
                    for (i = 0; i < spUser.Roles.length; i++) {
                        if (Const_RoleName[spUser.Roles[i].ID].endsWith('Maker')) { //if (spUser.Roles[i].Name.endsWith('Maker')) {
                            self.IsRoleMaker(true);
                            return;
                        }


                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }
    self.GetParameters = function () {
        GetParameters();
        GetRateIDR();
        GetTreshold();

    };
    // bind get data IDR RATE
    self.GetRateIDR = function () {
        GetRateIDR();
    }

    self.GetTreshold = function () {
        GetTreshold();
    }

    self.OnCurrencyChange = function () {
        var SelectedCurrencyID = $('.currency-id option:selected').val();
        self.Currency_u().ID(Number(SelectedCurrencyID.trim()));
        var CurrencyID = self.Currency_u().ID();
        //console.log(CurrencyID);    	
        if (CurrencyID != null) {
            var item = ko.utils.arrayFirst(self.ddlCurrency_u(), function (x) { return x.ID() == CurrencyID; });

            if (item != null) {
                self.Currency_u(new CurrencyModel(item.ID, item.Code));
            }
            if (CurrencyID != '' && CurrencyID != undefined) {
                GetRateIDRAmount(CurrencyID);
            } else {
                self.Rate_u(0);
            }
        }
    }

    // get all parameters need
    function GetParameters() {
        // widget reloader function start
        if ($box.css('position') == 'static') { $remove = true; $box.addClass('position-relative'); }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        var options = {
            url: api.server + api.url.parameter,
            params: {
                select: "Branch,BizSegment,CustomerType,POAFunction,UserApproval,statementletter,doctype,underlyingdoc,currency,purposedoc,customer"
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetParameters, OnError, OnAlways);
    }
    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }

    // On Success GetData Parameter for dwopdownlist
    function OnSuccessGetParameters(data, textStatus, jqXHR) {
        if (jqXHR.status == 200) {
            // bind result to observable array

            self.ddlCurrency_u([]);
            self.DynamicStatementLetter_u([]);
            self.ddlUnderlyingDocument_u([]);

            ko.utils.arrayForEach(data['Currency'], function (item) {
                self.ddlCurrency_u.push(new CurrencyModel(item.ID, item.Code));

            });

            // tab customer underlying start
            var underlying = data['UnderltyingDoc'];
            for (var i = 0 ; i < underlying.length; i++) {
                self.ddlUnderlyingDocument_u.push(new UnderlyingDocumentModel(underlying[i].ID, underlying[i].Name, underlying[i].Code));
            }
            self.ddlDocumentType_u(data['DocType']);

            ko.utils.arrayForEach(data['StatementLetter'], function (item) {
                if (item.ID == CONST_STATEMENT.StatementA_ID || item.ID == CONST_STATEMENT.StatementB_ID || item.ID == CONST_STATEMENT.AnnualStatement_ID) {
                    //console.log(item);
                    self.ddlStatementLetter_u.push(item);
                    self.DynamicStatementLetter_u.push(item);
                }
            });
            //self.ddlStatementLetter_u(data['StatementLetter']);
            // tab customer underlying end
            if (self.OnAfterGetParameters) self.OnAfterGetParameters();
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }
    function GetRateIDRAmount(CurrencyID) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.currency + "/CurrencyRate/" + CurrencyID,
            params: {
            },
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status == 200) {
                    self.Rate_u(data.RupiahRate);
                    //if(CurrencyID!=1){ // if not USD
                    var e = document.getElementById("Amount_u");
                    if (e.value != null && typeof e.value == "string") {
                        res = parseFloat(e.value.replace(',', '').replace(',', '').replace(',', '').replace(',', '')).toFixed(2) * data.RupiahRate / idrrate;
                    } else {
                        res = e.value * data.RupiahRate / idrrate;
                    }

                    res = Math.round(res * 100) / 100;
                    self.AmountUSD_u(parseFloat(res).toFixed(2));
                    //}
                }
            }
        });
    }

    function GetRateIDR() {
        var options = {
            url: api.server + api.url.currency + "/CurrencyRate/" + "1",
            params: {
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetRateIDR, OnError, OnAlways);
    }

    function OnSuccessGetRateIDR(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            // bind result to observable array
            idrrate = data.RupiahRate;
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }
    function GetTreshold() {
        $.ajax({
            type: "GET",
            url: api.server + api.url.singlevalueparameter + "/FX_TRANSACTION_TRESHOLD",
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            }, success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    treshHold = data;
                }
            }
        });
    }

    function SetDefaultValueStatementA() {
        var today = Date.now();
        var documentType = ko.utils.arrayFirst(self.ddlDocumentType_u(), function (item) { return item.Name == '-' });
        var underlyingDocument = ko.utils.arrayFirst(self.ddlUnderlyingDocument_u(), function (item) { return item.Code() == 998 });

        if (documentType != null) {
            self.DocumentType_u(new DocumentTypeModel(documentType.ID, documentType.Name));
        }

        if (underlyingDocument != null) {
            self.UnderlyingDocument_u(new UnderlyingDocumentModel(underlyingDocument.ID(), underlyingDocument.Name(), underlyingDocument.Code()));
        }
        self.DateOfUnderlying_u(self.LocalDate(today, true, false));
        self.SupplierName_u('-');
        self.InvoiceNumber_u('-');

    }

    function SetStatementA(isJointAccount, accountNumber) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/GetStatementA/" + cifData + "/" + isJointAccount + "/" + accountNumber,
            data: null, //Convert the Observable Data into JSON
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + accessToken
            }, success: function (data) {
                self.DynamicStatementLetter_u([]);
                if (data.IsStatementA) {
                    self.StatementLetter_u(new StatementLetterModel(CONST_STATEMENT.StatementB_ID, 'Statement B'));
                    ko.utils.arrayForEach(self.ddlStatementLetter_u(), function (item) {
                        if (item.ID != CONST_STATEMENT.StatementA_ID) {
                            self.DynamicStatementLetter_u.push(item);
                        }
                    });
                }
                else {
                    self.DynamicStatementLetter_u(self.ddlStatementLetter_u());
                    //self.IsStatementA(true);
                }
            }
        });
    }

    function setRefID() {
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/GetRefID/" + cifData,
            data: null, //Convert the Observable Data into JSON
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + accessToken
            }, success: function (data) {
                self.ReferenceNumber_u(customerNameData + '-' + data.ID);
                ReferenceNumber = customerNameData + '-' + data.ID;
            }
        });

    }

    self.Read_u = function () {
        Read_u();
    }

    function Read_u() {
        if (typeof self.Amount_u() != 'number') {
            var value = parseFloat(self.Amount_u().replace(',', '').replace(',', '').replace(',', '').replace(',', ''));
        } else {
            var value = parseFloat(self.Amount_u());
        }
        var res = parseFloat(value) * parseFloat(self.Rate_u()) / parseFloat(idrrate);
        res = Math.round(res * 100) / 100;
        res = isNaN(res) ? 0 : res; //avoid NaN
        self.AmountUSD_u(parseFloat(res).toFixed(2));
    }



}

var GridPropertiesModel = function (callback) {
    var self = this;

    self.SortColumn = ko.observable();
    self.SortOrder = ko.observable();
    self.AllowFilter = ko.observable(false);
    self.AvailableSizes = ko.observableArray(config.sizes);
    self.Page = ko.observable(1);
    self.Size = ko.observable(config.sizeDefault);
    self.Total = ko.observable(0);
    self.TotalPages = ko.observable(0);

    self.Callback = function () {
        callback();
    };

    self.OnPageSizeChange = function () {
        self.Page(1);

        self.Callback();
    };

    self.OnPageChange = function () {
        if (self.Page() < 1) {
            self.Page(1);
        } else {
            if (self.Page() > self.TotalPages())
                self.Page(self.TotalPages());
        }

        self.Callback();
    };

    self.NextPage = function () {
        var page = self.Page();
        if (page < self.TotalPages()) {
            self.Page(page + 1);

            self.Callback();
        }
    };

    self.PreviousPage = function () {
        var page = self.Page();
        if (page > 1) {
            self.Page(page - 1);

            self.Callback();
        }
    };

    self.FirstPage = function () {
        self.Page(1);

        self.Callback();
    };

    self.LastPage = function () {
        self.Page(self.TotalPages());

        self.Callback();
    };

    self.Filter = function (data) {
        self.Page(1);

        self.Callback();
    };

    // get sorted column
    self.GetSortedColumn = function (columnName) {
        var sort = "sorting";

        if (self.SortColumn() == columnName) {
            if (self.SortOrder() == "DESC")
                sort = sort + "_asc";
            else
                sort = sort + "_desc";
        }

        return sort;
    };

    // Sorting data
    self.Sorting = function (column) {
        if (self.SortColumn() == column) {
            if (self.SortOrder() == "ASC")
                self.SortOrder("DESC");
            else
                self.SortOrder("ASC");
        } else {
            self.SortOrder("ASC");
        }

        self.SortColumn(column);

        self.Page(1);

        self.Callback();
    };
};

//Rizki, 2016-01-04
//var HomeDraftModel = function (url, form, readonly) {
//    var self = this;

//    self.Form = form;
//    self.Readonly = ko.observable(readonly);;
//    self.url = url;

//    self.FilterWorkflowTaskTitle = ko.observable("");
//    self.FilterWorkflowTaskActivityTitle = ko.observable("");
//    self.FilterWorkflowTaskEntryTime = ko.observable("");
//    self.FilterWorkflowInitiator = ko.observable("");

//    self.Rows = ko.observableArray([]);

//    // grid properties
//    self.GridProperties = ko.observable();
//    self.GridProperties(new GridPropertiesModel(GetData));

//    // set default sorting
//    self.GridProperties().SortColumn("WorkflowTaskEntryTime");
//    self.GridProperties().SortOrder("DESC");

//    // bind clear filters
//    self.ClearFilters = function () {
//        self.FilterWorkflowTaskTitle("");
//        self.FilterWorkflowTaskActivityTitle("");
//        self.FilterWorkflowTaskEntryTime("");
//        self.FilterWorkflowInitiator("");

//        GetData();
//    };

//    // Local Date
//    self.LocalDate = function (date, isDateOnly, isDateLong) {
//        var localDate = new Date(date);

//        if (moment(localDate).isValid()) {
//            if (isDateOnly != undefined || isDateOnly == true) {
//                if (isDateLong)
//                    return moment(localDate).format(config.format.dateLong);
//                else
//                    return moment(localDate).format(config.format.date);
//            } else {
//                return moment(localDate).format(config.format.dateTime);
//            }
//        } else {
//            return "";
//        }
//    };

//    // bind get data function to view
//    self.GetData = function () {
//        GetData();
//    };

//    //Function to Display record to be updated
//    self.GetSelectedRow = function (data) {


//        self.Form.Show(data);

//        /*
//		$.ajax({
//			type: "GET",
//			url: api.server + self.url + "/" + data.ID,
//			headers: {
//				"Authorization" : "Bearer " + accessToken
//			},
//			success: function (data, textStatus, jqXHR) {
//				// send notification
//				if(jqXHR.status = 200){
//					self.Form.Show(data);				
//				}else
//					ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
//			},
//			error: function (jqXHR, textStatus, errorThrown) {
//				// send notification
//				ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
//			}
//		});*/
//    };

//    function GetData() {
//        // widget reloader function start
//        if ($box.css('position') == 'static') {
//            $remove = true;
//            $box.addClass('position-relative');
//        }
//        $box.append(config.spinner);

//        $box.one('reloaded.ace.widget', function () {
//            $box.find('.widget-box-overlay').remove();
//            if ($remove) $box.removeClass('position-relative');
//        });
//        // widget reloader function end

//        // declare options variable for ajax get request
//        var options = {
//            url: api.server + self.url,
//            params: {
//                page: self.GridProperties().Page(),
//                size: self.GridProperties().Size(),
//                sort_column: self.GridProperties().SortColumn(),
//                sort_order: self.GridProperties().SortOrder()
//            },
//            token: accessToken
//        };

//        // get filtered columns
//        var filters = GetFilteredColumns();

//        if (filters.length > 0) {
//            // POST
//            // add request body on POST
//            options.data = ko.toJSON(filters);

//            Helper.Ajax.Post(options, OnSuccessGetData, OnError, OnAlways);
//        } else {
//            // GET
//            Helper.Ajax.Get(options, OnSuccessGetData, OnError, OnAlways);
//        }
//    }

//    function GetFilteredColumns() {
//        // define filter
//        var filters = [];

//        if (self.FilterWorkflowTaskTitle() != "") filters.push({ Field: 'WorkflowTaskTitle', Value: self.FilterWorkflowTaskTitle() });
//        if (self.FilterWorkflowTaskActivityTitle() != "") filters.push({ Field: 'WorkflowTaskActivityTitle', Value: self.FilterWorkflowTaskActivityTitle() });
//        if (self.FilterWorkflowTaskEntryTime() != "") filters.push({ Field: 'WorkflowTaskEntryTime', Value: self.FilterWorkflowTaskEntryTime() });
//        if (self.FilterWorkflowInitiator() != "") filters.push({ Field: 'WorkflowInitiator', Value: self.FilterWorkflowInitiator() });

//        return filters;
//    };

//    // On success GetData callback
//    function OnSuccessGetData(data, textStatus, jqXHR) {
//        if (jqXHR.status = 200) {
//            self.Rows(data.Rows);
//            self.GridProperties().Page(data['Page']);
//            self.GridProperties().Size(data['Size']);
//            self.GridProperties().Total(data['Total']);
//            self.GridProperties().TotalPages(Math.ceil(self.GridProperties().Total() / self.GridProperties().Size()));
//        } else {
//            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
//        }
//    }

//    // On Error callback
//    function OnError(jqXHR, textStatus, errorThrown) {
//        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
//    }

//    // On Always callback
//    function OnAlways() {
//        $box.trigger('reloaded.ace.widget');
//    }
//}

var FormModel = function (parent, cfg) {
    var self = this;
    self.Parent = parent;

    self.Config = cfg;

    self.ActiveTemplate = ko.observable('TaskRoleChecker');
    self.SelectedRow = ko.observable();

    self.IsAuthorizedNintex = ko.observable(false);
    self.IsPendingNintex = ko.observable(false);
    self.MessageNintex = ko.observable();

    // task properties
    self.ApproverId = ko.observable();
    self.SPWebID = ko.observable();
    self.SPSiteID = ko.observable();
    self.SPListID = ko.observable();
    self.SPListItemID = ko.observable();
    self.SPTaskListID = ko.observable();
    self.SPTaskListItemID = ko.observable();
    self.Initiator = ko.observable();
    self.WorkflowInstanceID = ko.observable();
    self.WorkflowID = ko.observable();
    self.WorkflowName = ko.observable();
    self.StartTime = ko.observable();
    self.StateID = ko.observable();
    self.StateDescription = ko.observable();
    self.IsAuthorized = ko.observable();

    self.TaskTypeID = ko.observable();
    self.TaskTypeDescription = ko.observable();
    self.Title = ko.observable();
    self.ActivityTitle = ko.observable();
    self.User = ko.observable();
    self.IsSPGroup = ko.observable();
    self.EntryTime = ko.observable();
    self.ExitTime = ko.observable();
    self.OutcomeID = ko.observable();
    self.OutcomeDescription = ko.observable();
    self.CustomOutcomeID = ko.observable();
    self.CustomOutcomeDescription = ko.observable();
    self.Comments = ko.observable();

    self.UnderlyingID = ko.observable();
    //Declare an ObservableArray for Storing the JSON Response
    self.Tasks = ko.observableArray([]);
    self.Outcomes = ko.observableArray([]);

    self.LocalDate = function (date, isDateOnly, isDateLong) {
        var localDate = new Date(date);

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return "";
        }
    };

    self.LoadForms = function (callback) {
        if (callback) callback();
        return false;
    }

    self.Show = function (row) {
        //console.log(row); 

        self.SelectedRow(row);
        self.WorkflowInstanceID(row.ID);
        self.ApproverId(row.WorkflowApproverID);
        self.SPTaskListID(row.SPTaskListID);
        self.SPTaskListItemID(row.SPTaskListItemID);
        self.ActivityTitle(row.WorkflowTaskActivityTitle);
        if (multiViewModel.Form().SelectedRow().EntityName == MasterUnderlyingApproval || multiViewModel.Form().SelectedRow().EntityName == CustomerCsoApproval ||
            multiViewModel.Form().SelectedRow().EntityName == CustomerCallbackApproval) {
            self.TaskTypeID(row.TaskType);
        }
        else {
            self.TaskTypeID(row.WorkflowTaskType);
        }

        if (multiViewModel.Form().SelectedRow().EntityName == CustomerCsoApproval && multiViewModel.Form().SelectedRow().ActionType == 'ADD') {
            multiViewModel.CustomerCsoView().IsNewDataCso(true);
        }
        else {
            multiViewModel.CustomerCsoView().IsNewDataCso(false);
        }
        if (multiViewModel.Form().SelectedRow().ActionType == 'ADD') {
            multiViewModel.CustomerPOAEmailView().IsNewDataPOAEmail(true);
        } else {
            multiViewModel.CustomerPOAEmailView().IsNewDataPOAEmail(false);
        }
        //end azam

        if (multiViewModel.Form().SelectedRow().EntityName == MasterUnderlyingApproval && multiViewModel.Form().SelectedRow().ActionType == 'Add') {
            multiViewModel.UnderlyingView().IsNewDataUnderlying(true);
        }
        else {
            multiViewModel.UnderlyingView().IsNewDataUnderlying(false);
        }

        self.ActiveTemplate(self.Config[row.WorkflowTaskActivityTitle].template);

        //Hide All Views
        $.each(self.Config, function (index, item) {
            if (index == row.WorkflowTaskActivityTitle) item.view().IsActive(true); else item.view().IsActive(false);
        });

        $('#' + self.Config[row.WorkflowTaskActivityTitle].id).modal('show');

        // show progress bar
        $("#transaction-progress").show();
        // hide transaction data
        $("#transaction-data").hide();

        GetTaskOutcomes(self.SelectedRow().SPTaskListID, self.SelectedRow().SPTaskListItemID);

        // scrollables
        $('.slim-scroll').each(function () {
            var $this = $(this);
            $this.slimscroll({
                height: $this.data('height') || 100,
                //width: $this.data('width') || 100,
                railVisible: true,
                alwaysVisible: true,
                color: '#D15B47'
            });
        });
    }
    self.ApprovalButton = function (name) {
        if (name == 'Continue') {
            return 'Submit';
        } else {
            return name;
        }
    }
    // workflow approval process & get selected outcome to send into custom Nintex REST Api
    self.ApprovalProcess = function (data) {
        var text = String.format("{0}<br/><br/>Do you want to <b>{1}</b> this task?", data.Description, data.Name);
        var outcomes = config.nintex.outcome.uncompleted;
        // while user reject, revise or cancel the task, don't validate the form and the form data will not stored on database

        if (multiViewModel.Form().SelectedRow().EntityName == MasterUnderlyingApproval) {
            if (outcomes.indexOf(data.Name) > -1) {
                bootbox.confirm(text, function (result) {
                    if (result) {
                        // call nintex api

                        CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), data.ID, self.Comments());
                    }
                });
            } else {
                // validation
                var form = $("#aspnetForm");
                form.validate();

                if (form.valid()) {
                    bootbox.confirm(text, function (result) {
                        if (result) {
                            //alert(JSON.stringify(ko.toJS(self.PaymentMaker().Payment.LLDCode)))
                            // store data to db                        
                            SaveApprovalDataUnderlying(self.WorkflowInstanceID(), self.ApproverId(), data.ID);

                        }
                    });
                }
            }

        } else if (multiViewModel.Form().SelectedRow().EntityName == CustomerCsoApproval) {

            if (outcomes.indexOf(data.Name) > -1) {
                bootbox.confirm(text, function (result) {
                    if (result) {
                        // call nintex api

                        CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), data.ID, self.Comments());
                    }
                });
            } else {
                // validation
                var form = $("#aspnetForm");
                form.validate();

                if (form.valid()) {
                    bootbox.confirm(text, function (result) {
                        if (result) {
                            //alert(JSON.stringify(ko.toJS(self.PaymentMaker().Payment.LLDCode)))
                            // store data to db                        
                            SaveApprovalDataUnderlying(self.WorkflowInstanceID(), self.ApproverId(), data.ID);

                        }
                    });
                }
            }

        } else {
            if (outcomes.indexOf(data.Name) > -1) {
                bootbox.confirm(text, function (result) {
                    if (result) {
                        // call nintex api
                        CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), data.ID, self.Comments());
                    }
                });
                /*if(self.Comments() == ""){
					ShowNotification("Task Validation Info", "Please type any comment before revise this task.", "gritter-warning", false);
				}else{
					bootbox.confirm(text, function (result) {
						if (result) {
							// call nintex api
							CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), data.ID, self.Comments());
						}
					});
				}*/
            } else {
                // validation
                var form = $("#aspnetForm");
                form.validate();

                if (form.valid()) {
                    bootbox.confirm(text, function (result) {
                        if (result) {
                            //alert(JSON.stringify(ko.toJS(self.PaymentMaker().Payment.LLDCode)))
                            // store data to db                        
                            SaveApprovalData(self.WorkflowInstanceID(), self.ApproverId(), data.ID);

                            // call nintex api
                            //CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), data.ID, self.Comments());
                        }
                    });
                }
            }
        }
    };

    // get Transaction data
    function GetTransactionData(instanceId, approverId) {
        // show progress bar
        $("#transaction-progress").show();
        // hide transaction data
        $("#transaction-data").hide();

        if (self.Config[self.SelectedRow().WorkflowTaskActivityTitle].load) self.Config[self.SelectedRow().WorkflowTaskActivityTitle].load(self.SelectedRow().ID, function () {

            // hide progress bar
            $("#transaction-progress").hide();
            // show transaction data
            $("#transaction-data").show();
        });
        /*
        var endPointURL;

        switch (self.ActivityTitle()){
            case "Master Role Approval Task" :
                endPointURL = api.server + api.url.workflow.transactionMaker(instanceId, approverId);
                break;
            case "Master User Approval Task" :
                endPointURL = api.server + api.url.workflow.transactionChecker(instanceId, approverId);
                break;
        }

        // declare options variable for ajax get request
        var options = {
            url: endPointURL,
            token: accessToken
        };



        // call ajax
        Helper.Ajax.Get(options, OnSuccessGetTransactionData, OnError, function () {
            // hide progress bar
            $("#transaction-progress").hide();
            // show transaction data
            $("#transaction-data").show();
        });*/
    }

    function SaveApprovalData(instanceId, approverId, outcomeId) {
        CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), outcomeId, self.Comments());
    }

    function CompletingTask(taskListID, taskListItemID, taskTypeID, outcomeID, comments) {

        comments = comments == undefined ? '-' : comments;

        var task = {
            TaskListID: taskListID,
            TaskListItemID: taskListItemID,
            OutcomeID: outcomeID,
            Comment: comments
        };

        var endPointURL;
        switch (taskTypeID) {
            // Request Approval
            case 0: endPointURL = "/_vti_bin/DBSNintex/Services.svc/RespondApprovalTask";
                break;

                // Request Review
            case 1: endPointURL = "/_vti_bin/DBSNintex/Services.svc/RespondReviewTask";
                break;

                // Flexi Task
            case 4: endPointURL = "/_vti_bin/DBSNintex/Services.svc/RespondFlexiTask";
                break;
        }

        var options = {
            url: endPointURL,
            data: ko.toJSON(task)
        };

        Helper.Sharepoint.Nintex.Post(options, OnSuccessCompletingTask, OnError, OnAlways);

    }

    //start azam

    function SaveApprovalCustomerCso(instanceId, approverId, outcomeId) {
        var text = String.format("{0}<br/><br/>Do you want to <b>{1}</b> this task?", data.Description, data.Name);
        var outcomes = config.nintex.outcome.uncompleted;
        bootbox.confirm(text, function (result) {
            if (result) {
                CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), OutcomeID, self.Comments());
            }
        });
    }
    //end azam
    function SaveApprovalDataUnderlying(instanceId, approverId, outcomeId) {
        var endPointURL;
        var body;

        var other = multiViewModel.UnderlyingView().CodeUnderlying() == '999' ? multiViewModel.UnderlyingView().OtherUnderlying_u() : '';
        multiViewModel.UnderlyingView().CustomerUnderlying().OtherUnderlying(other);
        var sAmount = multiViewModel.UnderlyingView().CustomerUnderlying().Amount().toString().replace(/,/g, "");
        sAmount = isNaN(parseFloat(sAmount)) ? 0 : parseFloat(sAmount)
        multiViewModel.UnderlyingView().CustomerUnderlying().Amount(sAmount);

        switch (self.ActivityTitle()) {
            case "Master Underlying Approval Task":
                endPointURL = api.server + api.url.customerunderlying + "/UpdateDraft/" + multiViewModel.UnderlyingView().ID_u();
                body = ko.toJS(multiViewModel.UnderlyingView().CustomerUnderlying());
                break;

        }

        if (endPointURL == null) {
            CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), outcomeId, self.Comments());
        } else {
            // declare options variable for ajax get request
            var options = {
                url: endPointURL,
                token: accessToken,
                data: ko.toJSON(body)
            };

            //Helper.Ajax.Post(options, OnSuccessSaveApprovalData(outcomeId), OnError, OnAlways);
            Helper.Ajax.Put(options, function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), outcomeId, self.Comments());
                }
            }, OnError, OnAlways);
        }
    }

    function OnSuccessGetTransactionData(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            var mapping = {
                //'ignore': ["LastModifiedDate", "LastModifiedBy"]
            };

            switch (self.ActivityTitle()) {
                case "PPU Maker Task":
                    self.TransactionMaker(ko.mapping.toJS(data, mapping));

                    // fill selected id for auto populate field
                    self.Selected().Currency(data.Transaction.Currency.ID);
                    self.Selected().Account(data.Transaction.Account.AccountNumber);
                    self.Selected().Bank(data.Transaction.Bank.ID);
                    break;
                case "PPU Checker Task":
                    self.TransactionChecker(ko.mapping.toJS(data, mapping));
                    break;
                case "PPU Checker After PPU Caller Task":
                    self.TransactionCheckerCallback(ko.mapping.toJS(data, mapping));
                    break;
                case "PPU Caller Task":
                    self.TransactionCallback(ko.mapping.toJS(data, mapping));
                    break;
                case "CBO Maker Task":
                    self.TransactionContact(ko.mapping.toJS(data, mapping));
                    self.TransactionContacts.removeAll;
                    break;
                case "CBO Checker Task":
                    self.TransactionContact(ko.mapping.toJS(data, mapping));
                    break;
                case "Payment Maker Task":
                    self.PaymentMaker(ko.mapping.toJS(data, mapping));

                    // fill selected id for auto populate field
                    self.Selected().Account(data.Payment.Account.AccountNumber);
                    self.Selected().Bank(data.Payment.Bank.ID);
                    break;
                case "Payment Maker Revise Task":
                    self.PaymentMaker(ko.mapping.toJS(data, mapping));

                    // fill selected id for auto populate field
                    self.Selected().Account(data.Payment.Account.AccountNumber);
                    self.Selected().Bank(data.Payment.Bank.ID);
                    break;
                case "Payment Checker Task":
                    self.PaymentChecker(ko.mapping.toJS(data, mapping));
                    break;
            }

            // disable form if this task is not authorized
            if (self.IsPendingNintex()) {
                if (!self.IsAuthorizedNintex()) {
                    DisableForm();
                }
            } else {
                DisableForm();
            }
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    function GetTaskOutcomes(taskListID, taskListItemID) {
        var task = {
            TaskListID: taskListID,
            TaskListItemID: taskListItemID
        };

        var options = {
            url: "/_vti_bin/DBSNintex/Services.svc/GetTaskOutcomesByListID",
            data: ko.toJSON(task)
        };

        Helper.Sharepoint.Nintex.Post(options, OnSuccessTaskOutcomes, OnError, OnAlways);
    }


    function OnSuccessSignal() {
        //ShowNotification("Connection Success", "Connected to signal server", "gritter-success", false);
        /*    var hub = $.connection.task;
            $.connection.hub.url = config.signal.server+ "task";
    
            $.connection.hub.start()
                .done(function () {
                    alert("Connected");
                })
                .fail(function() {
                    alert("Connection failed!");
                });*/
    }

    function OnErrorSignal() {
        ShowNotification("Connection Failed", "Failed connected to signal server", "gritter-error", true);
    }

    function OnReceivedSignal(data) {
        ShowNotification(data.Sender, data.Message, "gritter-info", false);
    }

    //bangkit

    function OnSuccessTaskOutcomes(data, textStatus, jqXHR) {
        if (data.IsSuccess) {

            self.Outcomes(data.Outcomes);
            self.IsPendingNintex(data.IsPending);
            self.IsAuthorizedNintex(data.IsAuthorized);
            self.MessageNintex(data.Message);

            // Get transaction Data
            GetTransactionData(self.WorkflowInstanceID(), self.ApproverId());

            // lock current task only if user had permission
            if (self.IsPendingNintex() == true && self.IsAuthorizedNintex() == true) {

                _WorkflowInstanceID = self.WorkflowInstanceID();
                _ApproverId = self.ApproverId();
                _SPTaskListItemID = self.SPTaskListItemID();
                _ActivityTitle = self.ActivityTitle();

                LockAndUnlockTaskHub("Lock", self.WorkflowInstanceID(), self.ApproverId(), self.SPTaskListItemID(), self.ActivityTitle());
            }
        } else {
            ShowNotification("An error Occured", jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    function OnSuccessCompletingTask(data, textStatus, jqXHR) {
        if (data.IsSuccess) {
            //self.Parent.HomeDraft().GetData(); //Rizki, 2016-01-04

            // send notification
            ShowNotification('Completing Task Success', jqXHR.responseJSON.Message, 'gritter-success', false);

            // close dialog task
            $("#modal-form-master").modal('hide');

            // reload tasks
            //self.Parent.HomeDraft().GetData(); //Rizki, 2016-01-04
        } else {
            ShowNotification('Completing Task Failed', jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }


    //The Object which stored data entered in the observables
    var Role = {
        ID: self.ID,
        MasterID: self.MasterID,
        Name: self.Name,
        Menus: [],
        IsDeleted: self.IsDeleted,
        Description: self.Description,
        LastModifiedBy: self.LastModifiedBy,
        LastModifiedDate: self.LastModifiedDate
    };

    self.save = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            //Ajax call to insert the Roles
            Role.ActionType = 'ADD';
            Role.Menus = self.treeNavigation.Get();

            $.ajax({
                type: "POST",
                url: api.server + api.url.roledraft,
                data: ko.toJSON(Role), //Convert the Observable Data into JSON
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + accessToken
                },
                success: function (data, textStatus, jqXHR) {
                    if (jqXHR.status = 200) {
                        // send notification
                        ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                        // hide current popup window
                        $("#modal-form-master").modal('hide');

                        // refresh data
                        if (self.updateCallback != null) self.updateCallback();
                    } else {
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // send notification
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            });
        }
    };

    self.update = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            // hide current popup window
            $("#modal-form-master").modal('hide');

            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form-master").modal('show');
                } else {
                    //Ajax call to update the Customer
                    Role.ActionType = 'UPDATE';
                    Role.Menus = self.treeNavigation.Get();
                    $.ajax({
                        type: "POST",
                        url: api.server + api.url.roledraft,
                        data: ko.toJSON(Role), //Convert the Observable Data into JSON
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {
                                // send notification
                                ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // hide current popup window
                                $("#modal-form-master").modal('hide');

                                // refresh data
                                if (self.updateCallback != null) self.updateCallback();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });
                    /* CHANGE UPDATE TO ADD FOR WORKFLOW
                    $.ajax({
                        type: "PUT",
                        url: api.server + api.url.roledraft + "/" + Role.ID(),
                        data: ko.toJSON(Role),
                        contentType: "application/json",
                        headers: {
                            "Authorization" : "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if(jqXHR.status = 200){
                                // send notification
                                ShowNotification('Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // hide current popup window
                                $("#modal-form-master").modal('hide');

                                // refresh data
                                GetData();
                            }else{
                                ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });*/
                }
            });
        }
    };

    self.delete = function () {
        // hide current popup window
        $("#modal-form-master").modal('hide');

        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                $("#modal-form-master").modal('show');
            } else {
                //Ajax call to delete the Customer
                Role.ActionType = 'DELETE';
                Role.Menus = self.treeNavigation.Get();
                Role.IsDelete = true;
                $.ajax({
                    type: "POST",
                    url: api.server + api.url.roledraft,
                    data: ko.toJSON(Role), //Convert the Observable Data into JSON
                    contentType: "application/json",
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        if (jqXHR.status = 200) {
                            // send notification
                            ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                            // hide current popup window
                            $("#modal-form-master").modal('hide');

                            // refresh data
                            if (self.updateCallback != null) self.updateCallback();
                        } else {
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
                /* CHANGE DELETE TO ADD FOR WORKFLOW
                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.roledraft + "/" + Role.ID(),
                    headers: {
                        "Authorization" : "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if(jqXHR.status = 200){
                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                            // refresh data
                            GetData();
                        }else
                            ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });*/
            }
        });
    };
}

var MultiViewModel = function () {
    var self = this;

    self.RoleView = ko.observable(new RoleViewModel());
    self.UserView = ko.observable(new UserViewModel());
    self.UnderlyingView = ko.observable(new UnderlyingViewModel());
    //start azam
    self.CustomerCsoView = ko.observable(new CustomerCsoViewModel());
    self.CustomerPOAEmailView = ko.observable(new CustomerPOAEmailViewModel());
    //end azam
    self.CustomerCallbackView = ko.observable(new CustomerCallbackViewModel());

    self.Form = ko.observable(new FormModel(self,
		{
		    "Master Role Approval Task": {
		        template: 'TaskRoleChecker',
		        view: self.RoleView,
		        id: 'modal-form-master',
		        url: "api/RoleDraft",//api.url.roledraft,
		        form: "Approval/RoleChecker.html",
		        width: "600px",
		        load: self.RoleView().Load,
		        loaded: false
		    },
		    "Master User Approval Task": {
		        template: 'TaskUserChecker',
		        view: self.UserView,
		        id: 'modal-form-master',
		        url: api.url.userapprovaldraft,
		        form: "Approval/UserChecker.html",
		        width: "600px",
		        load: self.UserView().Load,
		        loaded: false
		    },
		    "Master Underlying Approval Task": {
		        template: 'TaskUnderlyingChecker',
		        view: self.UnderlyingView,
		        id: 'modal-form-master',
		        url: "api/CustomerUnderlyingDraft",
		        form: "Dialogs/MasterUnderlying.html",
		        width: "900px",
		        load: self.UnderlyingView().Load,
		        loaded: false
		    },
		    "Master CustomerCso Approval Task": {

		        template: 'TaskCustomerCsoChecker',
		        view: self.CustomerCsoView,
		        id: 'modal-form-master',
		        url: "",
		        form: "Dialogs/CustomerCso.html",
		        width: "900px",
		        load: self.CustomerCsoView().Load,
		        loaded: false

		    },
		    "Master Customer POA Email Approval Task": {
		        template: 'TaskCustomerPOAEmailChecker',
		        view: self.CustomerPOAEmailView,
		        id: 'modal-form-master',
		        url: "",
		        form: "Dialogs/ApprovalPOAEmail.html",
		        width: "900px",
		        load: self.CustomerPOAEmailView().Load,
		        loaded: false

		    },
		    "Master Customer Callback Approval Task": {

		        template: 'TaskCustomerCallbackChecker',
		        view: self.CustomerCallbackView,
		        id: 'modal-form-master',
		        url: "",
		        form: "Dialogs/CustomerCallback.html",
		        width: "900px",
		        load: self.CustomerCallbackView().Load,
		        loaded: false

		    },

		}
	));
    //Rizki, 2016-01-04
    //self.HomeDraft = ko.observable(new HomeDraftModel(api.url.homedraft, self.Form(), false));
};

$.holdReady(true);
var intervalRoleName = setInterval(function () {
    if (Object.keys(Const_RoleName).length != 0) {
        $.holdReady(false);
        clearInterval(intervalRoleName);
    }
}, 100);

//$(document).on("RoleNameReady", function() {
$(document).ready(function () {
    //console.log(config);
    // widget loader
    $box = $('#widget-box');
    var event;
    $box.trigger(event = $.Event('reload.ace.widget'))
    if (event.isDefaultPrevented()) return
    $box.blur();

    /// block enter key from user to prevent submitted data.
    $("input").keypress(function (event) {
        var code = event.charCode || event.keyCode;
        if (code == 13) {
            ShowNotification("Page Information", "Enter key is disabled for this form.", "gritter-warning", false);

            return false;
        }
    });




    multiViewModel = new MultiViewModel();
    multiViewModel.Form().LoadForms(function () {
        //bangkit
        //ko.applyBindings(multiViewModel, document.getElementById('widget-box1'));
        ko.applyBindings(multiViewModel, document.getElementById('home-master'));
    });
    //var viewModel = new ViewModel();

    //multiViewModel.RoleDraft().treeNavigation.Load();
    /*
        $('.modal-header').append('<button href="">Navigation</button>').on('click',function(){
            console.log(
                multiViewModel().Form().treeNavigation.Get()
            );
            return false;
        });
    */
    // Token Validation
    if ($.cookie(api.cookie.name) == undefined) {
        Helper.Token.Request(TokenOnSuccess, TokenOnError); //comment azam
    } else {
        // read token from cookie
        accessToken = $.cookie(api.cookie.name);
        multiViewModel.UnderlyingView().IsPermited();
        multiViewModel.UnderlyingView().GetParameters();
        // call spuser function
        //GetCurrentUser(multiViewModel.Role());
        //GetCurrentUser(multiViewModel.RoleDraft());
        //GetCurrentUser(multiViewModel.HomeDraft()); //Rizki, 2016-01-04

        // call get data inside view model	
        //multiViewModel.Role().GetData();
        //multiViewModel.RoleDraft().GetData();
        //multiViewModel.HomeDraft().GetData(); //Rizki, 2016-01-04
        StartTaskHub();
    }

    $("#modal-form-master").on('hidden.bs.modal', function () {
        //alert("close")
        //aridya 20170302 change to multiViewModel
        //LockAndUnlockTaskHub("unlock", viewModel.WorkflowInstanceID(), viewModel.ApproverId(), viewModel.SPTaskListItemID(), viewModel.ActivityTitle());
        LockAndUnlockTaskHub("unlock", multiViewModel.Form().WorkflowInstanceID(), multiViewModel.Form().ApproverId(), multiViewModel.Form().SPTaskListItemID(), multiViewModel.Form().ActivityTitle());
    });

    // datepicker
    $('.date-picker').datepicker({
        autoclose: true,
        onSelect: function () {
            this.focus();
        }
    }).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });

    // Knockout Datepicker custom binding handler
    ko.bindingHandlers.datepicker = {
        init: function (element) {
            $(element).datepicker({ autoclose: true }).next().on(ace.click_event, function () {
                $(this).prev().focus();
            });
        },
        update: function (element) {
            $(element).datepicker("refresh");
        }
    };

    //$("#aspnetForm").validate({onsubmit: false});
    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }

    });
});

// SignalR ----------------------
/*var taskHub = $.hubConnection(config.signal.server+ "hub");
var taskProxy = taskHub.createHubProxy("TaskService");
var IsTaskHubConnected = false;

function StartTaskHub() {
    taskHub.start()
        .done(function () {
            IsTaskHubConnected = true;
        })
        .fail(function() {
            ShowNotification("Hub Connection Failed", "Fail while try connecting to hub server", "gritter-error", true);
        }
    );
}

function LockAndUnlockTaskHub(lockType, instanceId, approverID, taskId, activityTitle){
    if(IsTaskHubConnected) {
        var data = {
            WorkflowInstanceID: instanceId,
            ApproverID: approverID,
            TaskID: taskId,
            ActivityTitle: activityTitle,
            LoginName: spUser.LoginName,
            DisplayName: spUser.DisplayName
        };

        var LockType = lockType.toLowerCase() == "lock" ? "LockTask" : "UnlockTask";

        // try lock current task
        taskProxy.invoke(LockType, data)
            .fail(function (err) {
                ShowNotification("LockType Task Failed", err, "gritter-error", true);
            }
        );
    }else{
        ShowNotification("Task Manager", "There is no active connection to task server manager.", "gritter-error", true);
    }
}*/
/*
// Get SPUser from cookies
function GetCurrentUser(viewModel) {
    //if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
	if ($.cookie(api.cookie.spUser) != undefined) {
        spUser = $.cookie(api.cookie.spUser);

        viewModel.SPUser = spUser;
    }
}

// Token validation On Success function
function TokenOnSuccess(data, textStatus, jqXHR){
    // store token on browser cookies
    $.cookie(api.cookie.name, data.AccessToken);
    accessToken = $.cookie(api.cookie.name);

    // call spuser function
    GetCurrentUser();

    // call get data inside view model
    multiViewModel.RoleDraft().GetParameters();
    multiViewModel.RoleDraft().GetData();
}

// Token validation On Error function
function TokenOnError(jqXHR, textStatus, errorThrown){
    ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
} */
// Token validation On Success function
function TokenOnSuccess(data, textStatus, jqXHR) {
    // store token on browser cookies
    $.cookie(api.cookie.name, data.AccessToken);
    accessToken = $.cookie(api.cookie.name);

    // call spuser function
    //GetCurrentUser();

    // call get data inside view model
    multiViewModel.UnderlyingView().GetParameters();
    multiViewModel.UnderlyingView().IsPermited();
}