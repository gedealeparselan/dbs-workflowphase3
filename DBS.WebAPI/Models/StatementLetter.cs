﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Models
{
    #region Property
    [ModelName("StatementLetter")]
    public class StatementLetterModel
    {
        /// <summary>
        /// Statement Letter ID.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Statement Letter Name.
        /// </summary>
        [MaxLength(50, ErrorMessage = "Statement Letter Name is must be in {1} characters.")]
        public string Name { get; set; }


        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }
    }

    #endregion


    #region Interface
    public interface IStatementLetterRepository : IDisposable
    {
        bool GetStatementLetter(ref IList<StatementLetterModel> StatementLetters, ref string message);
     }
    #endregion

    #region Repository
    public class StatementLetterRepository : IStatementLetterRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        
        public bool GetStatementLetter(ref IList<StatementLetterModel> StatementLetters, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    StatementLetters = (from a in context.StatementLetters
                                    where a.IsDeleted.Equals(false)
                                    orderby new { a.StatementLetterName }
                                    select new StatementLetterModel
                                    {
                                        ID = a.StatementLetterID,
                                        Name = a.StatementLetterName,
                                        LastModifiedBy = a.CreateBy,
                                        LastModifiedDate = a.CreateDate
                                    }).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }


        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}