﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Transactions;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Text.RegularExpressions;

namespace DBS.WebAPI.Models
{
    #region Property
    public class CustomerUnderlyingModel
    {
        public long ID { get; set; }

        public string CIF { get; set; }

        public string CustomerName { get; set; }

        public UnderlyingDocModel UnderlyingDocument { get; set; }

        public DocumentTypeModel DocumentType { get; set; }

        public CurrencyModel Currency { get; set; }

        public StatementLetterModel StatementLetter { get; set; }
        public decimal Amount { get; set; }

        public Nullable<decimal> AmountUSD { get; set; }

        public Nullable<decimal> Rate { get; set; }

        public Nullable<decimal> AvailableAmount { get; set; }

        public string AttachmentNo { get; set; }

        public bool IsDeclarationOfException { get; set; }
        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public DateTime? DateOfUnderlying { get; set; }

        public DateTime? ExpiredDate { get; set; }

        public string ReferenceNumber { get; set; }

        public string SupplierName { get; set; }

        public string InvoiceNumber { get; set; }

        public string OtherUnderlying { get; set; }

        public bool IsSelectedAttach { get; set; }

        public bool IsSelectedProforma { get; set; }

        public bool IsProforma { get; set; }

        public bool? IsSelectedBulk { get; set; }

        public bool? IsBulkUnderlying { get; set; }

        public bool IsUtilize { get; set; }

        public long ParentID { get; set; }
        public bool IsEnable { get; set; }

        public bool? IsAvailable { get; set; }
        public bool? IsExpiredDate { get; set; }
        public bool? IsExpired { get; set; }
        public bool? IsJointAccount { get; set; }
        public string AccountNumber { get; set; }
        public bool? IsTMO { get; set; }
        public DateTime? LastModifiedDate { get; set; }

        public string LastModifiedBy { get; set; }

        public List<MappingUnderlyingModel> Proformas;
        public List<MappingBulkUnderlying> BulkUnderlyings;

        public List<CustomerUnderlyingDetailModel> ProformaDetails { get; set; }
        public List<CustomerUnderlyingDetailModel> BulkDetails { get; set; }
        public decimal UtilizationAmount { get; set; }
        public long TransactionID { get; set; }
        public int StatusShowData { get; set; } //0 : all, 1: single Account, 2: joint account
        public decimal? UtilizeAmountDeal { get; set; }

        public decimal? AvailableAmountDeal { get; set; }

        public DateTime CreatedDate { get; set; }

        public string CreatedBy { get; set; }
    }

    public class CustomerUnderlyingTunningModel
    {
        public long ID { get; set; }

        public string CIF { get; set; }

        public string CustomerName { get; set; }

        public UnderlyingDocModel UnderlyingDocument { get; set; }

        public DocumentTypeModel DocumentType { get; set; }

        public CurrencyModel Currency { get; set; }

        public StatementLetterModel StatementLetter { get; set; }
        public decimal Amount { get; set; }

        public Nullable<decimal> AmountUSD { get; set; }

        public Nullable<decimal> Rate { get; set; }

        public Nullable<decimal> AvailableAmount { get; set; }

        public string AttachmentNo { get; set; }

        public bool IsDeclarationOfException { get; set; }
        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public DateTime? DateOfUnderlying { get; set; }

        public DateTime? ExpiredDate { get; set; }

        public string ReferenceNumber { get; set; }

        public string SupplierName { get; set; }

        public string InvoiceNumber { get; set; }

        public string OtherUnderlying { get; set; }

        public bool IsSelectedAttach { get; set; }

        public bool IsSelectedProforma { get; set; }

        public bool IsProforma { get; set; }

        public bool? IsSelectedBulk { get; set; }

        public bool? IsBulkUnderlying { get; set; }

        public bool IsUtilize { get; set; }

        public long ParentID { get; set; }
        public bool IsEnable { get; set; }

        public bool? IsAvailable { get; set; }
        public bool? IsExpiredDate { get; set; }
        public bool? IsExpired { get; set; }
        public bool? IsJointAccount { get; set; }
        public string AccountNumber { get; set; }
        public bool? IsTMO { get; set; }
        public DateTime? LastModifiedDate { get; set; }

        public string LastModifiedBy { get; set; }

        public List<MappingUnderlyingModel> Proformas;
        public List<MappingBulkUnderlying> BulkUnderlyings;

        public List<CustomerUnderlyingDetailModel> ProformaDetails { get; set; }
        public List<CustomerUnderlyingDetailModel> BulkDetails { get; set; }
        public decimal UtilizationAmount { get; set; }
        public long TransactionID { get; set; }
        public int StatusShowData { get; set; } //0 : all, 1: single Account, 2: joint account
        public decimal? UtilizeAmountDeal { get; set; }

        public decimal? AvailableAmountDeal { get; set; }

        public DateTime CreatedDate { get; set; }

        public string CreatedBy { get; set; }

        public string UnderlyingDocumentName { get; set; }

        public string DocumentTypeName { get; set; }

        public string CurrencyName { get; set; }

        public string StatementLetterName { get; set; }
    }

    public class DataUnderlying
    {
        public long UnderlyingID { get; set; }
    }

    public class CustomerUnderlyingDraftModel
    {
        public string CIF { get; set; }
        public int Amount { get; set; }

        //change by fandi
        //public int Currency { get; set; }
        //public int DocumentType { get; set; }
        //public int AmountUSD { get; set; }
        public string AmountUSD { get; set; }
        public string Currency { get; set; }
        public string DocumentType { get; set; }
        //end 
        public int InvoiceAmount { get; set; }
        public string InvoiceNumber { get; set; }
        //public int Rate { get; set; }
        public string Rate { get; set; }
        //public DateTime DateOfUnderlying2 { get; set; }
        public string DateOfUnderlying2 { get; set; }
        //public DateTime ExpiredDate2 { get; set; }
        public string ExpiredDate2 { get; set; }
        public string ReferenceNumber { get; set; }
        //change by fandi
        //public int StatementLetter { get; set; }
        public string StatementLetter { get; set; }
        public string SupplierName { get; set; }
        //change by fandi
        //public int UnderlyingDocument { get; set; }
        public string UnderlyingDocument { get; set; }
    }

    public class CustomerUnderlyingDetailModel
    {
        public UnderlyingDocModel UnderlyingDocument { get; set; }

        public DocumentTypeModel DocumentType { get; set; }

        public CurrencyModel Currency { get; set; }

        public StatementLetterModel StatementLetter { get; set; }
        public decimal Amount { get; set; }

        public Nullable<decimal> AmountUSD { get; set; }

        public Nullable<decimal> Rate { get; set; }

        public Nullable<decimal> AvailableAmount { get; set; }

        public string AttachmentNo { get; set; }

        public DateTime? DateOfUnderlying { get; set; }

        public DateTime ExpiredDate { get; set; }

        public string ReferenceNumber { get; set; }

        public string SupplierName { get; set; }

        public string InvoiceNumber { get; set; }

        public string OtherUnderlying { get; set; }

    }

    public class UtilizeUnderlyingModel
    {
        public long ID { get; set; }
        public bool Enable { get; set; }
        public decimal USDAmount { get; set; }
    }
    public class MappingUnderlyingModel
    {
        public long ID { get; set; }
        public string CIF { get; set; }
        public long UnderlyingID { get; set; }
        public long ParentID { get; set; }

    }

    public class SelectUtillizeModel
    {
        public long UnderlyingID { get; set; }
        public bool IsEnable { get; set; }
        public decimal AvailableAmount { get; set; }
        public int StatementLetterID { get; set; }
    }

    public class CustomerUnderlyingRow : CustomerUnderlyingModel
    {
        public int RowID { get; set; }
    }

    public class SelectedProformaModel
    {
        public long ID { get; set; }
    }
    public class SelectedBulkModel
    {
        public long ID { get; set; }
        public decimal? Amount { get; set; }
    }
    public class SelectedUtilizeModel
    {
        public long ID { get; set; }
    }


    public class CustomerUnderlyingGrid : Grid
    {
        public IList<CustomerUnderlyingRow> Rows { get; set; }
    }

    public class CustomerUnderlyingHistoryModel
    {
        public long UnderlyingID { get; set; }
        public decimal Amount { get; set; }
        public Nullable<decimal> AmountUSD { get; set; }
        public Nullable<decimal> AvailableAmountUSD { get; set; }
        public DateTime DateOfUnderlying { get; set; }
        public string SupplierName { get; set; }
        public string InvoiceNumber { get; set; }
        public string AccountNumber { get; set; }
        public string IsJointAccount { get; set; }
        public string AttachmentNo { get; set; }
        public string IsExpired { get; set; }
        public string StatementLetterName { get; set; }
        public string UnderlyingDocName { get; set; }
        public string DocTypeName { get; set; }
        public string CurrencyCode { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateBy { get; set; }
    }

    public class CustomerUnderlyingTransactionHistoryModel
    {
        public long UnderlyingID { get; set; }
        public string ApplicationID { get; set; }
        public DateTime? ApplicationDate { get; set; }
        public string ProductCode { get; set; }
        public string CurrencyCode { get; set; }
        public decimal Amount { get; set; }
        public decimal AmountUSD { get; set; }
        public Nullable<decimal> UtilizedAmountInUSD { get; set; }
        public string TransactionStatus { get; set; }
        public DateTime? UpdateDate { get; set; }
    }

    #endregion

    #region Filter
    public class CustomerUnderlyingFilter : Filter
    {
    }
    #endregion

    #region Interface
    public interface ICustomerUnderlyingRepository : IDisposable
    {
        bool AddCustomerUnderlying(CustomerUnderlyingModel customerUnderlying, ref string message);
        bool GetCustomerUnderlyingByID(long id, ref CustomerUnderlyingModel CustomerUnderlying, ref string message);
        #region Tambahan Agung
        bool GetCustomerUnderlyingDraftByID(string id, ref CustomerUnderlyingModel role, ref string message);
        bool GetCustomerUnderlying(ref IList<CustomerUnderlyingModel> roles, ref string message);
        bool GetCustomerUnderlyingDraft(int page, int size, IList<CustomerUnderlyingFilter> filters, string sortColumn, string sortOrder, ref CustomerUnderlyingGrid CustomerUnderlying, ref string message);
        #endregion
        bool GetCustomerUnderlying(IList<CustomerUnderlyingFilter> filters, ref IList<SelectedProformaModel> ID, ref string message);
        bool GetCustomerUnderlying(ref IList<CustomerUnderlyingModel> CustomerUnderlyings, int limit, int index, ref string message);
        bool GetCustomerUnderlyingProforma(int page, int size, IList<CustomerUnderlyingFilter> filters, string sortColumn, string sortOrder, ref CustomerUnderlyingGrid CustomerUnderlyingGrid, ref string message);
        bool GetCustomerBulkUnderlying(int page, int size, IList<CustomerUnderlyingFilter> filters, string sortColumn, string sortOrder, ref CustomerUnderlyingGrid CustomerUnderlyingGrid, ref string message);
        bool GetCustomerBulkUnderlyingJoint(int page, int size, IList<CustomerUnderlyingFilter> filters, string sortColumn, string sortOrder, ref CustomerUnderlyingGrid CustomerUnderlyingGrid, ref string message);
        bool GetCustomerUnderlyingAttach(int page, int size, IList<CustomerUnderlyingFilter> filters, string sortColumn, string sortOrder, ref CustomerUnderlyingGrid CustomerUnderlyingGrid, ref string message);
        bool GetCustomerBulkUnderlying(IList<CustomerUnderlyingFilter> filters, ref IList<SelectedBulkModel> ID, ref string message);
        bool GetCustomerUnderlyingUtilizeID(IList<CustomerUnderlyingFilter> filters, ref IList<SelectedUtilizeModel> ID, ref string message);
        bool GetCustomerUnderlying(int page, int size, IList<CustomerUnderlyingFilter> filters, string sortColumn, string sortOrder, ref CustomerUnderlyingGrid CustomerUnderlying, ref string message);
        bool UpdateCustomerUnderlyingUtilize(IList<TransUnderlyingModel> customerUnderlyings, ref string message);
        bool GetCustomerUnderlying(CustomerUnderlyingFilter filter, ref IList<CustomerUnderlyingModel> CustomerUnderlyings, ref string message);
        bool GetCustomerUnderlying(string key, int limit, ref IList<CustomerUnderlyingModel> CustomerUnderlyings, ref string message);
        bool GetCustomerUnderlyingWithFilterExpiredDate(int page, int size, IList<CustomerUnderlyingFilter> filters, string sortColumn, string sortOrder, ref CustomerUnderlyingGrid CustomerUnderlyingGrid, ref string message);
        bool AddCustomerUnderlyingDraft(CustomerUnderlyingModel CustomerUnderlyingDraft, ref long UnderlyingDraftID, ref string message);
        bool AddCustomerUnderlyingExcelDraft(IList<CustomerUnderlyingDraftModel> CustomerUnderlyingDraft, ref List<DataUnderlying> UnderlyingData, ref string message);
        bool UpdateCustomerUnderlying(long id, CustomerUnderlyingModel customerUnderlying, ref string message);
        bool UpdateCustomerUnderlyingDraft(long id, CustomerUnderlyingModel CustomerUnderlying, ref long UnderlyingDraftID, ref string message);
        bool UpdateCustomerUnderlyingDraftSubmit(long id, CustomerUnderlyingModel CustomerUnderlyingDraft, ref string message);
        bool DeleteCustomerUnderlyingDraft(long id, CustomerUnderlyingModel CustomerUnderlying, ref long UnderlyingDraftID, ref string message);
        bool GetCustomerUnderlyingFile(long id, int page, int size, IList<CustomerUnderlyingFileFilter> filters, string sortColumn, string sortOrder, ref CustomerUnderlyingFileGrid CustomerUnderlyingFile, ref string message);
        bool GetCustomerUnderlyingProforma(long id, int page, int size, IList<CustomerUnderlyingFilter> filters, string sortColumn, string sortOrder, ref CustomerUnderlyingGrid CustomerUnderlyingGrid, ref string message);
        bool GetCustomerUnderlyingAttachByTransactionID(long id, int page, int size, IList<CustomerUnderlyingFilter> filters, string sortColumn, string sortOrder, ref CustomerUnderlyingGrid CustomerUnderlyingGrid, ref string message);
        bool GetCustomerUnderlying(long id, int page, int size, IList<CustomerUnderlyingFilter> filters, string sortColumn, string sortOrder, ref CustomerUnderlyingGrid CustomerUnderlying, ref string message);
        bool GetCustomerUnderlyingByID(long id, int page, int size, IList<CustomerUnderlyingFilter> filters, string sortColumn, string sortOrder, ref CustomerUnderlyingGrid CustomerUnderlying, ref string message);
        bool GetCustomerUnderlyingAttach(IList<long> UnderlyingsID, string cif, ref List<CustomerUnderlyingModel> customerUnderlying, ref string message);
        bool GetCustomerUnderlyingUnionDeal(long id, int page, int size, IList<CustomerUnderlyingFilter> filters, string sortColumn, string sortOrder, ref CustomerUnderlyingGrid CustomerUnderlyingGrid, ref string message);
        bool AddCustomerUnderlying(CustomerUnderlyingModel customerUnderlying, ref SelectUtillizeModel selectUtilize, ref string message);
        bool DeleteCustomerUnderlying(long id, ref string message);
        bool GetUnderlyingHistoryByUnderlyingID(long underlyingID, ref IList<CustomerUnderlyingHistoryModel> CustomerUnderlyingHistory, ref string message);
        bool GetUnderlyingTransactionHistoryByUnderlyingID(long underlyingID, ref IList<CustomerUnderlyingTransactionHistoryModel> CustomerUnderlyingTransactionHistory, ref string message);
    }
    #endregion

    #region Repository
    public class CustomerUnderlyingRepository : ICustomerUnderlyingRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetCustomerUnderlyingByID(long id, ref CustomerUnderlyingModel CustomerUnderlying, ref string message)
        {
            bool isSuccess = false;

            try
            {
                CustomerUnderlying = (from a in context.CustomerUnderlyings
                                      where a.IsDeleted.Equals(false)
                                      select new CustomerUnderlyingModel
                                      {
                                          ID = a.UnderlyingID,
                                          CIF = a.CIF,
                                          UnderlyingDocument = new UnderlyingDocModel
                                          {
                                              ID = a.UnderlyingDocument.UnderlyingDocID,
                                              Code = a.UnderlyingDocument.UnderlyingDocCode,
                                              Name = a.UnderlyingDocument.UnderlyingDocName,
                                              LastModifiedBy = a.UnderlyingDocument.UpdateDate == null ? a.UnderlyingDocument.CreateBy : a.UnderlyingDocument.UpdateBy,
                                              LastModifiedDate = a.UnderlyingDocument.UpdateDate == null ? a.UnderlyingDocument.CreateDate : a.UnderlyingDocument.UpdateDate.Value
                                          },
                                          DocumentType = new DocumentTypeModel
                                          {
                                              ID = a.DocumentType.DocTypeID,
                                              Name = a.DocumentType.DocTypeName,
                                              Description = a.DocumentType.DocTypeDescription,
                                              LastModifiedBy = a.DocumentType.UpdateDate == null ? a.DocumentType.CreateBy : a.DocumentType.UpdateBy,
                                              LastModifiedDate = a.DocumentType.UpdateDate == null ? a.DocumentType.CreateDate : a.DocumentType.UpdateDate.Value
                                          },
                                          Currency = new CurrencyModel
                                          {
                                              ID = a.Currency.CurrencyID,
                                              Code = a.Currency.CurrencyCode,
                                              LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                              LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                          },
                                          StatementLetter = new StatementLetterModel
                                          {
                                              ID = a.StatementLetter.StatementLetterID,
                                              Name = a.StatementLetter.StatementLetterName,
                                              LastModifiedBy = a.StatementLetter.UpdateDate == null ? a.StatementLetter.CreateBy : a.StatementLetter.UpdateBy,
                                              LastModifiedDate = a.StatementLetter.UpdateDate == null ? a.StatementLetter.CreateDate : a.StatementLetter.UpdateDate.Value
                                          },
                                          Amount = a.Amount,
                                          AmountUSD = a.AmountUSD,
                                          AvailableAmount = a.AvailableAmountUSD,
                                          AttachmentNo = a.AttachmentNo,
                                          IsDeclarationOfException = a.IsDeclarationOfExecption,
                                          StartDate = a.StartDate,
                                          EndDate = a.EndDate,
                                          DateOfUnderlying = a.DateOfUnderlying,
                                          ExpiredDate = a.ExpiredDate,
                                          ReferenceNumber = a.ReferenceNumber,
                                          SupplierName = a.SupplierName,
                                          InvoiceNumber = a.InvoiceNumber,
                                          IsUtilize = a.IsUtilize,
                                          IsProforma = a.IsProforma,
                                          IsSelectedProforma = a.IsSelectedProforma,
                                          IsSelectedBulk = a.IsSelectedBulk == null ? false : a.IsSelectedBulk,
                                          IsBulkUnderlying = a.IsBulkUnderlying == null ? false : a.IsBulkUnderlying,
                                          OtherUnderlying = a.OtherUnderlying,
                                          LastModifiedDate = a.CreateDate,
                                          LastModifiedBy = a.CreateBy
                                      }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCustomerUnderlyingDraftByID(string id, ref CustomerUnderlyingModel CustomerUnderlying, ref string message)
        {
            Guid gid = Guid.Empty;
            bool isWorkflow = Guid.TryParse(id, out gid);

            bool isSuccess = false;

            try
            {
                CustomerUnderlying = (from a in context.CustomerUnderlyingDrafts
                                      where a.IsDeleted.Equals(false) && a.WorkflowInstanceID.Value == gid
                                      select new CustomerUnderlyingModel
                                      {
                                          ID = a.UnderlyingID,
                                          CIF = a.CIF,
                                          UnderlyingDocument = new UnderlyingDocModel
                                          {
                                              ID = a.UnderlyingDocument.UnderlyingDocID,
                                              Code = a.UnderlyingDocument.UnderlyingDocCode,
                                              Name = a.UnderlyingDocument.UnderlyingDocName,
                                              LastModifiedBy = a.UnderlyingDocument.UpdateDate == null ? a.UnderlyingDocument.CreateBy : a.UnderlyingDocument.UpdateBy,
                                              LastModifiedDate = a.UnderlyingDocument.UpdateDate == null ? a.UnderlyingDocument.CreateDate : a.UnderlyingDocument.UpdateDate.Value
                                          },
                                          DocumentType = new DocumentTypeModel
                                          {
                                              ID = a.DocumentType.DocTypeID,
                                              Name = a.DocumentType.DocTypeName,
                                              Description = a.DocumentType.DocTypeDescription,
                                              LastModifiedBy = a.DocumentType.UpdateDate == null ? a.DocumentType.CreateBy : a.DocumentType.UpdateBy,
                                              LastModifiedDate = a.DocumentType.UpdateDate == null ? a.DocumentType.CreateDate : a.DocumentType.UpdateDate.Value
                                          },
                                          Currency = new CurrencyModel
                                          {
                                              ID = a.Currency.CurrencyID,
                                              Code = a.Currency.CurrencyCode,
                                              LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                              LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                          },
                                          StatementLetter = new StatementLetterModel
                                          {
                                              ID = a.StatementLetter.StatementLetterID,
                                              Name = a.StatementLetter.StatementLetterName,
                                              LastModifiedBy = a.StatementLetter.UpdateDate == null ? a.StatementLetter.CreateBy : a.StatementLetter.UpdateBy,
                                              LastModifiedDate = a.StatementLetter.UpdateDate == null ? a.StatementLetter.CreateDate : a.StatementLetter.UpdateDate.Value
                                          },
                                          Amount = a.Amount,
                                          AmountUSD = a.AmountUSD,
                                          //Chandra Add
                                          IsJointAccount = a.IsJointAccount,
                                          IsTMO = a.IsTMO,
                                          AccountNumber = a.AccountNumber,
                                          AvailableAmount = a.AvailableAmountUSD,
                                          AttachmentNo = a.AttachmentNo,
                                          IsDeclarationOfException = a.IsDeclarationOfExecption,
                                          Rate = a.Rate,
                                          StartDate = a.StartDate,
                                          EndDate = a.EndDate,
                                          DateOfUnderlying = a.DateOfUnderlying,
                                          ExpiredDate = a.ExpiredDate,
                                          ReferenceNumber = a.ReferenceNumber,
                                          SupplierName = a.SupplierName,
                                          InvoiceNumber = a.InvoiceNumber,
                                          IsUtilize = a.IsUtilize,
                                          IsProforma = a.IsProforma,
                                          IsSelectedProforma = a.IsSelectedProforma,
                                          IsSelectedBulk = a.IsSelectedBulk == null ? false : a.IsSelectedBulk,
                                          IsBulkUnderlying = a.IsBulkUnderlying == null ? false : a.IsBulkUnderlying,
                                          OtherUnderlying = a.OtherUnderlying,
                                          LastModifiedDate = a.CreateDate,
                                          LastModifiedBy = a.CreateBy
                                      }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCustomerUnderlying(ref IList<CustomerUnderlyingModel> CustomerUnderlyings, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    CustomerUnderlyings = (from a in context.CustomerUnderlyings
                                           where a.IsDeleted.Equals(false)
                                           orderby new { a.UnderlyingID, a.DocTypeID }
                                           select new CustomerUnderlyingModel
                                           {
                                               ID = a.UnderlyingID,
                                               CIF = a.CIF,
                                               UnderlyingDocument = new UnderlyingDocModel
                                               {
                                                   ID = a.UnderlyingDocument.UnderlyingDocID,
                                                   Name = a.UnderlyingDocument.UnderlyingDocName,
                                                   Code = a.UnderlyingDocument.UnderlyingDocCode,
                                                   LastModifiedBy = a.UnderlyingDocument.UpdateDate == null ? a.UnderlyingDocument.CreateBy : a.UnderlyingDocument.UpdateBy,
                                                   LastModifiedDate = a.UnderlyingDocument.UpdateDate == null ? a.UnderlyingDocument.CreateDate : a.UnderlyingDocument.UpdateDate.Value
                                               },
                                               DocumentType = new DocumentTypeModel
                                               {
                                                   ID = a.DocumentType.DocTypeID,
                                                   Name = a.DocumentType.DocTypeName,
                                                   Description = a.DocumentType.DocTypeDescription,
                                                   LastModifiedBy = a.DocumentType.UpdateDate == null ? a.DocumentType.CreateBy : a.DocumentType.UpdateBy,
                                                   LastModifiedDate = a.DocumentType.UpdateDate == null ? a.DocumentType.CreateDate : a.DocumentType.UpdateDate.Value
                                               },
                                               Currency = new CurrencyModel
                                               {
                                                   ID = a.Currency.CurrencyID,
                                                   Code = a.Currency.CurrencyCode,
                                                   LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                                   LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                               },
                                               StatementLetter = new StatementLetterModel
                                               {
                                                   ID = a.StatementLetter.StatementLetterID,
                                                   Name = a.StatementLetter.StatementLetterName,
                                                   LastModifiedBy = a.StatementLetter.UpdateDate == null ? a.StatementLetter.CreateBy : a.StatementLetter.UpdateBy,
                                                   LastModifiedDate = a.StatementLetter.UpdateDate == null ? a.StatementLetter.CreateDate : a.StatementLetter.UpdateDate.Value
                                               },
                                               Amount = a.Amount,
                                               AmountUSD = a.AmountUSD,
                                               AvailableAmount = a.AvailableAmountUSD,
                                               AttachmentNo = a.AttachmentNo,
                                               IsDeclarationOfException = a.IsDeclarationOfExecption,
                                               StartDate = a.StartDate,
                                               EndDate = a.EndDate,
                                               DateOfUnderlying = a.DateOfUnderlying,
                                               ExpiredDate = a.ExpiredDate,
                                               ReferenceNumber = a.ReferenceNumber,
                                               SupplierName = a.SupplierName,
                                               IsProforma = a.IsProforma,
                                               IsSelectedProforma = a.IsSelectedProforma,
                                               InvoiceNumber = a.InvoiceNumber,
                                               IsUtilize = a.IsUtilize,
                                               OtherUnderlying = a.OtherUnderlying,
                                               LastModifiedDate = a.CreateDate,
                                               LastModifiedBy = a.CreateBy
                                           }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCustomerUnderlying(IList<CustomerUnderlyingFilter> filters, ref IList<SelectedProformaModel> UnderlyingID, ref string message)
        {
            bool isSuccess = false;

            try
            {

                CustomerUnderlyingModel filter = new CustomerUnderlyingModel();
                if (filters != null)
                {
                    var parentId = filters.Where(a => a.Field.Equals("ParentID")).Select(a => a.Value).SingleOrDefault();
                    filter.CIF = filters.Where(a => a.Field.Equals("CIF")).Select(a => a.Value).SingleOrDefault();
                    filter.ParentID = (parentId != null ? long.Parse((string)parentId) : 0);
                    filter.UnderlyingDocument = new UnderlyingDocModel { Name = (string)filters.Where(a => a.Field.Equals("UnderlyingDocument")).Select(a => a.Value).SingleOrDefault() };
                    filter.DocumentType = new DocumentTypeModel { Name = (string)filters.Where(a => a.Field.Equals("DocumentType")).Select(a => a.Value).SingleOrDefault() };
                }
                using (DBSEntities context = new DBSEntities())
                {
                    UnderlyingID = (from a in context.CustomerUnderlyings
                                    join b in context.MappingUnderlyings.Where(z => z.IsDeleted.Equals(false)) on a.UnderlyingID equals b.UnderlyingID into temp
                                    from x in temp.DefaultIfEmpty()
                                    let isCIF = !string.IsNullOrEmpty(filter.CIF)
                                    let isUnderlyingDocument = !string.IsNullOrEmpty(filter.UnderlyingDocument.Name)
                                    let isDocumentType = !string.IsNullOrEmpty(filter.DocumentType.Name)
                                    where (a.IsSelectedProforma.Equals(true) && x.ParentID.Equals(filter.ParentID)) &&
                                    a.DocumentType.DocTypeName.Contains(filter.DocumentType.Name) && a.UnderlyingID != filter.ParentID &&
                                        (isCIF ? a.CIF.Equals(filter.CIF) : true) &&
                                        (isUnderlyingDocument ? a.UnderlyingDocument.UnderlyingDocName.Contains(filter.UnderlyingDocument.Name) : true) &&
                                        (isDocumentType ? a.DocumentType.DocTypeName.Contains(filter.DocumentType.Name) : true)
                                    select new SelectedProformaModel
                                    {
                                        ID = a.UnderlyingID
                                    }).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool GetCustomerUnderlying(ref IList<CustomerUnderlyingModel> underlying, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    underlying = (from a in context.CustomerUnderlyingDrafts
                                  where a.IsDeleted.Equals(false)
                                  orderby new { a.UnderlyingID, a.DocTypeID }
                                  select new CustomerUnderlyingModel
                                  {
                                      ID = a.UnderlyingID,
                                      CIF = a.CIF,
                                      UnderlyingDocument = new UnderlyingDocModel
                                      {
                                          ID = a.UnderlyingDocument.UnderlyingDocID,
                                          Name = a.UnderlyingDocument.UnderlyingDocName,
                                          Code = a.UnderlyingDocument.UnderlyingDocCode,
                                          LastModifiedBy = a.UnderlyingDocument.UpdateDate == null ? a.UnderlyingDocument.CreateBy : a.UnderlyingDocument.UpdateBy,
                                          LastModifiedDate = a.UnderlyingDocument.UpdateDate == null ? a.UnderlyingDocument.CreateDate : a.UnderlyingDocument.UpdateDate.Value
                                      },
                                      DocumentType = new DocumentTypeModel
                                      {
                                          ID = a.DocumentType.DocTypeID,
                                          Name = a.DocumentType.DocTypeName,
                                          Description = a.DocumentType.DocTypeDescription,
                                          LastModifiedBy = a.DocumentType.UpdateDate == null ? a.DocumentType.CreateBy : a.DocumentType.UpdateBy,
                                          LastModifiedDate = a.DocumentType.UpdateDate == null ? a.DocumentType.CreateDate : a.DocumentType.UpdateDate.Value
                                      },
                                      Currency = new CurrencyModel
                                      {
                                          ID = a.Currency.CurrencyID,
                                          Code = a.Currency.CurrencyCode,
                                          LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                          LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                      },
                                      StatementLetter = new StatementLetterModel
                                      {
                                          ID = a.StatementLetter.StatementLetterID,
                                          Name = a.StatementLetter.StatementLetterName,
                                          LastModifiedBy = a.StatementLetter.UpdateDate == null ? a.StatementLetter.CreateBy : a.StatementLetter.UpdateBy,
                                          LastModifiedDate = a.StatementLetter.UpdateDate == null ? a.StatementLetter.CreateDate : a.StatementLetter.UpdateDate.Value
                                      },
                                      Amount = a.Amount,
                                      AmountUSD = a.AmountUSD,
                                      AvailableAmount = a.AvailableAmountUSD,
                                      AttachmentNo = a.AttachmentNo,
                                      IsDeclarationOfException = a.IsDeclarationOfExecption,
                                      StartDate = a.StartDate,
                                      EndDate = a.EndDate,
                                      DateOfUnderlying = a.DateOfUnderlying,
                                      ExpiredDate = a.ExpiredDate,
                                      ReferenceNumber = a.ReferenceNumber,
                                      SupplierName = a.SupplierName,
                                      IsProforma = a.IsProforma,
                                      IsSelectedProforma = a.IsSelectedProforma,
                                      InvoiceNumber = a.InvoiceNumber,
                                      IsUtilize = a.IsUtilize,
                                      OtherUnderlying = a.OtherUnderlying,
                                      LastModifiedDate = a.CreateDate,
                                      LastModifiedBy = a.CreateBy
                                  }).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCustomerBulkUnderlying(IList<CustomerUnderlyingFilter> filters, ref IList<SelectedBulkModel> UnderlyingID, ref string message)
        {
            bool isSuccess = false;

            try
            {
                CustomerUnderlyingModel filter = new CustomerUnderlyingModel();
                if (filters != null)
                {
                    var parentId = filters.Where(a => a.Field.Equals("ParentID")).Select(a => a.Value).SingleOrDefault();
                    filter.CIF = filters.Where(a => a.Field.Equals("CIF")).Select(a => a.Value).SingleOrDefault();
                    filter.ParentID = (parentId != null ? long.Parse((string)parentId) : 0);
                }
                using (DBSEntities context = new DBSEntities())
                {
                    UnderlyingID = (from a in context.CustomerUnderlyings
                                    join b in context.MappingBulkUnderlyings.Where(z => z.IsDeleted.Equals(false)) on a.UnderlyingID equals b.UnderlyingID into temp
                                    from x in temp.DefaultIfEmpty()
                                    let isCIF = !string.IsNullOrEmpty(filter.CIF)
                                    where (a.IsSelectedBulk.Value.Equals(true) &&
                                        x.ParentID.Equals(filter.ParentID)) &&
                                        a.UnderlyingID != filter.ParentID &&
                                        (isCIF ? a.CIF.Equals(filter.CIF) : true)
                                    select new SelectedBulkModel
                                    {
                                        ID = a.UnderlyingID,
                                        Amount = a.Amount
                                    }).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool GetCustomerUnderlyingUtilizeID(IList<CustomerUnderlyingFilter> filters, ref IList<SelectedUtilizeModel> UnderlyingID, ref string message)
        {
            bool isSuccess = false;

            try
            {

                CustomerUnderlyingModel filter = new CustomerUnderlyingModel();
                filter.UnderlyingDocument = new UnderlyingDocModel();
                filter.DocumentType = new DocumentTypeModel();
                if (filters != null)
                {
                    var parentId = filters.Where(a => a.Field.Equals("ParentID")).Select(a => a.Value).SingleOrDefault();
                    filter.CIF = filters.Where(a => a.Field.Equals("CIF")).Select(a => a.Value).SingleOrDefault();
                }
                using (DBSEntities context = new DBSEntities())
                {
                    UnderlyingID = (from a in context.CustomerUnderlyings
                                    join b in context.MappingUnderlyings.Where(z => z.IsDeleted.Equals(false)) on a.UnderlyingID equals b.UnderlyingID into temp
                                    from x in temp.DefaultIfEmpty()
                                    let isCIF = !string.IsNullOrEmpty(filter.CIF)
                                    where a.IsUtilize.Equals(true) && (isCIF ? a.CIF.Equals(filter.CIF) : true)
                                    select new SelectedUtilizeModel
                                    {
                                        ID = a.UnderlyingID
                                    }).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool GetCustomerUnderlying(int page, int size, IList<CustomerUnderlyingFilter> filters, string sortColumn, string sortOrder, ref CustomerUnderlyingGrid CustomerUnderlyingGrid, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();
                string date = string.Format("{0}-{1}-{2}", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                DateTime currentdate = DateTime.Parse(date);

                CustomerUnderlyingModel filter = new CustomerUnderlyingModel();
                string filterJointAccount = string.Empty;
                string filterIsExpired = string.Empty;
                if (filters != null)
                {
                    var parentId = filters.Where(a => a.Field.Equals("ParentID")).Select(a => a.Value).SingleOrDefault();
                    bool HasSelectBulk = filters.Where(a => a.Field.Equals("IsSelectedBulk")).Select(a => a == null ? false : true).SingleOrDefault();
                    bool isTMO_ = filters.Where(a => a.Field.Equals("IsTMO")).Select(a => a == null ? false : true).SingleOrDefault();
                    filter.ParentID = (parentId != null ? long.Parse((string)parentId) : 0);
                    filter.UnderlyingDocument = new UnderlyingDocModel { Name = (string)filters.Where(a => a.Field.Equals("UnderlyingDocument")).Select(a => a.Value).SingleOrDefault() };
                    filter.DocumentType = new DocumentTypeModel { Name = (string)filters.Where(a => a.Field.Equals("DocumentType")).Select(a => a.Value).SingleOrDefault() };
                    filter.Currency = new CurrencyModel { Code = (string)filters.Where(a => a.Field.Equals("Currency")).Select(a => a == null ? "0" : a.Value).SingleOrDefault() };
                    filter.StatementLetter = new StatementLetterModel { Name = (string)filters.Where(a => a.Field.Equals("StatementLetter")).Select(a => a.Value).SingleOrDefault() };
                    filter.Amount = Convert.ToDecimal((string)filters.Where(a => a.Field.Equals("Amount")).Select(a => a == null ? "0" : a.Value).SingleOrDefault());
                    filter.AvailableAmount = Convert.ToDecimal((string)filters.Where(a => a.Field.Equals("AvailableAmount")).Select(a => a == null ? "0" : a.Value).SingleOrDefault());
                    filter.AttachmentNo = (string)filters.Where(a => a.Field.Equals("AttachmentNo")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("StartDate")).Any())
                    {
                        filter.StartDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("StartDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("EndDate")).Any())
                    {
                        filter.EndDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("EndDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("DateOfUnderlying")).Any())
                    {
                        filter.DateOfUnderlying = DateTime.Parse((string)filters.Where(a => a.Field.Equals("DateOfUnderlying")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("ExpiredDate")).Any())
                    {
                        filter.ExpiredDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("ExpiredDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    filter.CIF = (string)filters.Where(a => a.Field.Equals("CIF")).Select(a => a.Value).SingleOrDefault();
                    filter.ReferenceNumber = (string)filters.Where(a => a.Field.Equals("ReferenceNumber")).Select(a => a.Value).SingleOrDefault();
                    filter.SupplierName = (string)filters.Where(a => a.Field.Equals("SupplierName")).Select(a => a.Value).SingleOrDefault();
                    filter.InvoiceNumber = (string)filters.Where(a => a.Field.Equals("InvoiceNumber")).Select(a => a.Value).SingleOrDefault();
                    filter.IsJointAccount = string.IsNullOrEmpty((string)filters.Where(a => a.Field.Equals("IsJointAccount")).Select(a => a.Value).SingleOrDefault()) ? false : true;
                    if (filter.IsJointAccount.HasValue && filter.IsJointAccount.Value)
                    {
                        filterJointAccount = (string)filters.Where(a => a.Field.Equals("IsJointAccount")).Select(a => a.Value).SingleOrDefault();
                    }
                    filter.IsExpired = string.IsNullOrEmpty((string)filters.Where(a => a.Field.Equals("IsExpired")).Select(a => a.Value).SingleOrDefault()) ? false : true;
                    if (filter.IsExpired.HasValue && filter.IsExpired.Value)
                    {
                        filterIsExpired = (string)filters.Where(a => a.Field.Equals("IsExpired")).Select(a => a.Value).SingleOrDefault();
                    }
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
                    filter.AccountNumber = (string)filters.Where(a => a.Field.Equals("AccountNumber")).Select(a => a.Value).SingleOrDefault();
                    string statusData = (string)filters.Where(a => a.Field.Equals("StatusShowData")).Select(a => a.Value).SingleOrDefault();
                    filter.StatusShowData = statusData != null ? int.Parse(statusData) : 0;
                    if (filters.Where(a => a.Field.Equals("IsAvailable")).SingleOrDefault() != null)
                    {
                        filter.IsAvailable = true;
                    }
                    else
                    {
                        filter.IsAvailable = false;
                    }
                    if (filters.Where(a => a.Field.Equals("IsExpiredDate")).SingleOrDefault() != null)
                    {
                        filter.IsExpiredDate = true;
                        //currentdate = currentdate.AddHours(-7);
                    }
                    else
                    {
                        filter.IsExpiredDate = false;
                    }
                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                    filter.IsSelectedBulk = HasSelectBulk ? true : false;
                    filter.IsTMO = isTMO_ ? true : false;
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var accountNumber = (from j in context.SP_GETJointAccount(filter.CIF)
                                         select new CustomerAccountModel
                                         {
                                             AccountNumber = j.AccountNumber,
                                             Currency = new CurrencyModel
                                             {
                                                 ID = j.CurrencyID.Value,
                                                 Code = j.CurrencyCode,
                                                 Description = j.CurrencyDescription
                                             },
                                             CustomerName = j.CustomerName,
                                             IsJointAccount = j.IsJointAccount
                                         }).ToList();

                    IList<string> JointAccounts = new List<string>();
                    if (accountNumber != null && accountNumber.Count > 0)
                    {
                        JointAccounts = accountNumber.Where(x => x.IsJointAccount.HasValue ? x.IsJointAccount.Value.Equals(true) : false).Select(x => x.AccountNumber).ToList();
                    }
                    IList<CustomerUnderlyingTunningModel> data = (from a in context.SP_GetUnderlying(filter.CIF,
                                                     filter.StatementLetter.Name,
                                                     filter.UnderlyingDocument.Name,
                                                     filter.DocumentType.Name,
                                                     filter.Currency.Code,
                                                     filter.Amount,
                                                     filter.AvailableAmount,
                                                     filter.DateOfUnderlying,
                                                     filter.SupplierName,
                                                     filter.InvoiceNumber,
                                                     filter.AccountNumber,
                                                     filter.AccountNumber,
                                                     filter.AccountNumber,
                                                     filter.AttachmentNo)
                                                                  select new CustomerUnderlyingTunningModel
                                                                  {
                                                                      ID = a.UnderlyingID,
                                                                      CIF = a.CIF,
                                                                      CustomerName = a.CustomerName,
                                                                      Amount = a.Amount,
                                                                      AmountUSD = a.AmountUSD == null ? 0 : a.AmountUSD,
                                                                      AvailableAmount = a.AvailableAmount == null ? 0 : a.AvailableAmount,
                                                                      UtilizeAmountDeal = a.UtilizeAmountDeal == null ? a.AvailableAmount : a.UtilizeAmountDeal,
                                                                      AvailableAmountDeal = a.AvailableAmountDeal == null ? a.AvailableAmount : a.AvailableAmountDeal,
                                                                      Rate = a.Rate,
                                                                      AttachmentNo = a.AttachmentNo,
                                                                      IsDeclarationOfException = a.IsDeclarationOfExecption,
                                                                      StartDate = a.StartDate,
                                                                      EndDate = a.EndDate,
                                                                      DateOfUnderlying = a.DateOfUnderlying,
                                                                      ExpiredDate = a.ExpiredDate,
                                                                      ReferenceNumber = a.ReferenceNumber,
                                                                      SupplierName = a.SupplierName,
                                                                      IsUtilize = a.IsUtilize,
                                                                      IsProforma = a.IsProforma,
                                                                      IsSelectedProforma = a.IsProforma,
                                                                      IsBulkUnderlying = a.IsBulkUnderlying == null ? false : a.IsBulkUnderlying,
                                                                      IsSelectedBulk = a.IsBulkUnderlying == null ? false : a.IsBulkUnderlying,
                                                                      InvoiceNumber = a.InvoiceNumber,
                                                                      OtherUnderlying = "",
                                                                      IsJointAccount = a.IsJointAccount.HasValue ? a.IsJointAccount.Value : false,
                                                                      IsTMO = a.IsTMO.HasValue ? a.IsTMO.Value : false,
                                                                      AccountNumber = a.AccountNumber,
                                                                      IsEnable = false,
                                                                      //IsExpiredDate = DbFunctions.TruncateTime(a.ExpiredDate) <= DbFunctions.TruncateTime(currentdate) ? true : false,
                                                                      IsExpiredDate = a.IsExpired.Equals("Yes") ? true : false,
                                                                      CreatedDate = a.CreateDate,
                                                                      LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                                      LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,
                                                                      UnderlyingDocumentName = a.UnderlyingDocName,
                                                                      DocumentTypeName = a.DocTypeName,
                                                                      CurrencyName = a.CurrencyCode,
                                                                      UnderlyingDocument = new UnderlyingDocModel
                                                                      {
                                                                          ID = a.UnderlyingDocID,
                                                                          Code = a.UnderlyingDocCode,
                                                                          Name = a.UnderlyingDocName,
                                                                          LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                                          LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                                                      },
                                                                      DocumentType = new DocumentTypeModel
                                                                      {
                                                                          ID = a.DocTypeID,
                                                                          Name = a.DocTypeName,
                                                                          Description = a.DocTypeDescription,
                                                                          LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                                          LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                                                      },
                                                                      Currency = new CurrencyModel
                                                                      {
                                                                          ID = a.CurrencyID,
                                                                          Code = a.CurrencyCode,
                                                                          LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                                          LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                                                      },
                                                                      StatementLetter = new StatementLetterModel
                                                                      {
                                                                          ID = a.StatementLetterID,
                                                                          Name = a.StatementLetterName,
                                                                          LastModifiedBy = a.UpdateDateSL == null ? a.CreateBySL : a.UpdateBySL,
                                                                          LastModifiedDate = a.UpdateDateSL == null ? a.CreateDateSL : a.UpdateDateSL.Value
                                                                      }
                                                                  }).ToList();

                    CustomerUnderlyingGrid.Page = page;
                    CustomerUnderlyingGrid.Size = size;
                    CustomerUnderlyingGrid.Total = data.Count();
                    CustomerUnderlyingGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    CustomerUnderlyingGrid.Rows = data.AsEnumerable()
                        .Select((a, i) => new CustomerUnderlyingRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            CIF = a.CIF,
                            CustomerName = a.CustomerName,
                            UnderlyingDocument = a.UnderlyingDocument,
                            DocumentType = a.DocumentType,
                            Currency = a.Currency,
                            AmountUSD = a.AmountUSD.HasValue ? (decimal?)Decimal.Round(a.AmountUSD.Value, 2) : null,
                            Amount = Decimal.Round(a.Amount, 2),
                            Rate = a.Rate,
                            AvailableAmount = a.AvailableAmount.HasValue ? (decimal?)Decimal.Round(a.AvailableAmount.Value, 2) : null,
                            UtilizeAmountDeal = a.UtilizeAmountDeal.HasValue ? (decimal?)Decimal.Round(a.UtilizeAmountDeal.Value, 2) : null,
                            AvailableAmountDeal = a.AvailableAmountDeal.HasValue ? (decimal?)Decimal.Round(a.AvailableAmountDeal.Value, 2) : null,
                            AttachmentNo = a.AttachmentNo,
                            StatementLetter = a.StatementLetter,
                            IsDeclarationOfException = a.IsDeclarationOfException,
                            StartDate = a.StartDate,
                            EndDate = a.EndDate,
                            DateOfUnderlying = a.DateOfUnderlying,
                            ExpiredDate = a.ExpiredDate,
                            ReferenceNumber = a.ReferenceNumber,
                            SupplierName = a.SupplierName,
                            InvoiceNumber = a.InvoiceNumber,
                            IsSelectedProforma = a.IsSelectedProforma,
                            IsProforma = a.IsProforma,
                            IsSelectedBulk = a.IsSelectedBulk,
                            IsBulkUnderlying = a.IsBulkUnderlying,
                            IsUtilize = a.IsUtilize,
                            IsEnable = false,
                            OtherUnderlying = a.OtherUnderlying,
                            IsJointAccount = a.IsJointAccount,
                            IsTMO = a.IsTMO,
                            IsExpiredDate = a.IsExpiredDate,
                            AccountNumber = a.AccountNumber,
                            CreatedBy = a.CreatedBy,
                            CreatedDate = a.CreatedDate,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }


                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCustomerUnderlyingTunning(int page, int size, IList<CustomerUnderlyingFilter> filters, string sortColumn, string sortOrder, ref CustomerUnderlyingGrid CustomerUnderlyingGrid, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();
                string date = string.Format("{0}-{1}-{2}", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                DateTime currentdate = DateTime.Parse(date);

                CustomerUnderlyingModel filter = new CustomerUnderlyingModel();
                string filterJointAccount = string.Empty;
                string filterIsExpired = string.Empty;
                if (filters != null)
                {
                    var parentId = filters.Where(a => a.Field.Equals("ParentID")).Select(a => a.Value).SingleOrDefault();
                    bool HasSelectBulk = filters.Where(a => a.Field.Equals("IsSelectedBulk")).Select(a => a == null ? false : true).SingleOrDefault();
                    bool isTMO_ = filters.Where(a => a.Field.Equals("IsTMO")).Select(a => a == null ? false : true).SingleOrDefault();
                    filter.ParentID = (parentId != null ? long.Parse((string)parentId) : 0);
                    filter.UnderlyingDocument = new UnderlyingDocModel { Name = (string)filters.Where(a => a.Field.Equals("UnderlyingDocument")).Select(a => a.Value).SingleOrDefault() };
                    filter.DocumentType = new DocumentTypeModel { Name = (string)filters.Where(a => a.Field.Equals("DocumentType")).Select(a => a.Value).SingleOrDefault() };
                    filter.Currency = new CurrencyModel { Code = (string)filters.Where(a => a.Field.Equals("Currency")).Select(a => a == null ? "0" : a.Value).SingleOrDefault() };
                    filter.StatementLetter = new StatementLetterModel { Name = (string)filters.Where(a => a.Field.Equals("StatementLetter")).Select(a => a.Value).SingleOrDefault() };
                    filter.Amount = Convert.ToDecimal((string)filters.Where(a => a.Field.Equals("Amount")).Select(a => a == null ? "0" : a.Value).SingleOrDefault());
                    filter.AvailableAmount = Convert.ToDecimal((string)filters.Where(a => a.Field.Equals("AvailableAmount")).Select(a => a == null ? "0" : a.Value).SingleOrDefault());
                    filter.AttachmentNo = (string)filters.Where(a => a.Field.Equals("AttachmentNo")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("StartDate")).Any())
                    {
                        filter.StartDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("StartDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("EndDate")).Any())
                    {
                        filter.EndDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("EndDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("DateOfUnderlying")).Any())
                    {
                        filter.DateOfUnderlying = DateTime.Parse((string)filters.Where(a => a.Field.Equals("DateOfUnderlying")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("ExpiredDate")).Any())
                    {
                        filter.ExpiredDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("ExpiredDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    filter.CIF = (string)filters.Where(a => a.Field.Equals("CIF")).Select(a => a.Value).SingleOrDefault();
                    filter.ReferenceNumber = (string)filters.Where(a => a.Field.Equals("ReferenceNumber")).Select(a => a.Value).SingleOrDefault();
                    filter.SupplierName = (string)filters.Where(a => a.Field.Equals("SupplierName")).Select(a => a.Value).SingleOrDefault();
                    filter.InvoiceNumber = (string)filters.Where(a => a.Field.Equals("InvoiceNumber")).Select(a => a.Value).SingleOrDefault();
                    filter.IsJointAccount = string.IsNullOrEmpty((string)filters.Where(a => a.Field.Equals("IsJointAccount")).Select(a => a.Value).SingleOrDefault()) ? false : true;
                    if (filter.IsJointAccount.HasValue && filter.IsJointAccount.Value)
                    {
                        filterJointAccount = (string)filters.Where(a => a.Field.Equals("IsJointAccount")).Select(a => a.Value).SingleOrDefault();
                    }
                    filter.IsExpired = string.IsNullOrEmpty((string)filters.Where(a => a.Field.Equals("IsExpired")).Select(a => a.Value).SingleOrDefault()) ? false : true;
                    if (filter.IsExpired.HasValue && filter.IsExpired.Value)
                    {
                        filterIsExpired = (string)filters.Where(a => a.Field.Equals("IsExpired")).Select(a => a.Value).SingleOrDefault();
                    }
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
                    filter.AccountNumber = (string)filters.Where(a => a.Field.Equals("AccountNumber")).Select(a => a.Value).SingleOrDefault();
                    string statusData = (string)filters.Where(a => a.Field.Equals("StatusShowData")).Select(a => a.Value).SingleOrDefault();
                    filter.StatusShowData = statusData != null ? int.Parse(statusData) : 0;
                    if (filters.Where(a => a.Field.Equals("IsAvailable")).SingleOrDefault() != null)
                    {
                        filter.IsAvailable = true;
                    }
                    else
                    {
                        filter.IsAvailable = false;
                    }
                    if (filters.Where(a => a.Field.Equals("IsExpiredDate")).SingleOrDefault() != null)
                    {
                        filter.IsExpiredDate = true;
                        //currentdate = currentdate.AddHours(-7);
                    }
                    else
                    {
                        filter.IsExpiredDate = false;
                    }
                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                    filter.IsSelectedBulk = HasSelectBulk ? true : false;
                    filter.IsTMO = isTMO_ ? true : false;
                }
                using (DBSEntities context = new DBSEntities())
                {
                    var accountNumber = (from j in context.SP_GETJointAccount(filter.CIF)
                                         select new CustomerAccountModel
                                         {
                                             AccountNumber = j.AccountNumber,
                                             Currency = new CurrencyModel
                                             {
                                                 ID = j.CurrencyID.Value,
                                                 Code = j.CurrencyCode,
                                                 Description = j.CurrencyDescription
                                             },
                                             CustomerName = j.CustomerName,
                                             IsJointAccount = j.IsJointAccount
                                         }).ToList();

                    IList<string> JointAccounts = new List<string>();
                    if (accountNumber != null && accountNumber.Count > 0)
                    {
                        JointAccounts = accountNumber.Where(x => x.IsJointAccount.HasValue ? x.IsJointAccount.Value.Equals(true) : false).Select(x => x.AccountNumber).ToList();
                    }
                    var data = (from a in context.CustomerUnderlyings.Where(x => x.CIF.Equals(filter.CIF) || (JointAccounts.Count > 0 ? JointAccounts.Contains(x.AccountNumber) : false))
                                let isCIF = !string.IsNullOrEmpty(filter.CIF)
                                let isUnderlyingDocument = !string.IsNullOrEmpty(filter.UnderlyingDocument.Name)
                                let isDocumentType = !string.IsNullOrEmpty(filter.DocumentType.Name)
                                let isCurrency = !string.IsNullOrEmpty(filter.Currency.Code)
                                let isStatementLetter = !string.IsNullOrEmpty(filter.StatementLetter.Name)
                                let isAmount = filter.Amount != null && filter.Amount != 0 ? true : false
                                let isAvailableAmount = filter.AvailableAmount != null && filter.AvailableAmount != 0 ? true : false
                                let isAttachmentNo = !string.IsNullOrEmpty(filter.AttachmentNo)
                                let isStartDate = filter.StartDate.HasValue ? true : false
                                let isEndDate = filter.EndDate.HasValue ? true : false
                                let isDateOfUnderlying = filter.DateOfUnderlying.HasValue ? true : false

                                let isReferenceNumber = !string.IsNullOrEmpty(filter.ReferenceNumber)
                                let isSupplierName = !string.IsNullOrEmpty(filter.SupplierName)
                                let isInvoiceNumber = filter.InvoiceNumber != null ? true : false
                                let isIsJointAccount = filter.IsJointAccount.HasValue ? filter.IsJointAccount.Value : false
                                // let isIsExpiredDate = filter.IsExpiredDate.HasValue ? filter.IsExpiredDate.Value : false 
                                let isExpiredDate = filter.ExpiredDate.HasValue ? true : false
                                let isIsExpired = filter.IsExpired.HasValue ? filter.IsExpired.Value : false
                                let isIsAvailable = filter.IsAvailable == null || filter.IsAvailable == false ? false : true
                                let isIsExpiredDate = filter.IsExpiredDate == null || filter.IsExpiredDate == false ? false : true
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                let isHasSelectBulkk = filter.IsSelectedBulk.Value ? true : false
                                let isTMO = filter.IsTMO.Value ? true : false
                                let isAccountNumber = filter.AccountNumber != null ? true : false
                                let isStatusShow = filter.StatusShowData != null ? true : false
                                where a.IsDeleted.Equals(false) &&
                                    //a.ExpiredDate.Year <= currentdate.Year && a.ExpiredDate.Month <= currentdate.Month && a.ExpiredDate.Day <= currentdate.Day &&
                                    //(isOnlyJointAccount ? isAccountNumber ? a.AccountNumber.Equals(filter.AccountNumber) : isCIF ? a.CIF.Equals(filter.CIF) : true || (isAccountNumber ? a.AccountNumber.Equals(filter.AccountNumber) : true) &&
                                    (isStatusShow ? filter.StatusShowData == 0 ? true : (filter.StatusShowData == 1 ? a.CIF.Equals(filter.CIF) && (a.IsJointAccount.HasValue ? a.IsJointAccount.Value.Equals(false) : true) : filter.StatusShowData == 2 ? a.AccountNumber.Equals(filter.AccountNumber) : true) : true) &&
                                    //(isCIF ? a.CIF.Equals(filter.CIF) : true || (isAccountNumber ? a.AccountNumber.(filter.AccountNumber) : true) &&
                                    (isHasSelectBulkk ? a.IsSelectedBulk.Value.Equals(false) : true) &&
                                    (isTMO ? a.IsTMO.Value.Equals(filter.IsTMO.Value) : true) &&
                                    (isUnderlyingDocument ? a.UnderlyingDocument.UnderlyingDocName.Contains(filter.UnderlyingDocument.Name) : true) &&
                                    (isDocumentType ? a.DocumentType.DocTypeName.Contains(filter.DocumentType.Name) : true) &&
                                    (isCurrency ? a.Currency.CurrencyCode.Contains(filter.Currency.Code) : true) &&
                                    (isStatementLetter ? a.StatementLetter.StatementLetterName.Contains(filter.StatementLetter.Name) : true) &&
                                    (isAmount ? a.Amount.Equals(filter.Amount) : true) &&
                                    (isAvailableAmount ? a.AvailableAmountUSD.Value.Equals(filter.AvailableAmount.Value) : true) &&
                                    (isAttachmentNo ? a.AttachmentNo.Contains(filter.AttachmentNo) : true) &&
                                    (isStartDate ? a.StartDate.Equals(filter.StartDate) : true) &&
                                    (isEndDate ? a.EndDate.Equals(filter.EndDate) : true) &&
                                    (isDateOfUnderlying ? a.DateOfUnderlying == filter.DateOfUnderlying : true) &&
                                    (isExpiredDate ? a.ExpiredDate.Equals(filter.ExpiredDate) : true) &&
                                    (isReferenceNumber ? a.ReferenceNumber.Contains(filter.ReferenceNumber) : true) &&
                                    (isSupplierName ? a.SupplierName.Contains(filter.SupplierName) : true) &&
                                    (isIsAvailable ? (a.AvailableAmountUSD.Value > 0 || a.StatementLetterID == 4) : true) &&
                                    (isIsExpiredDate ? a.ExpiredDate >= currentdate : true) &&
                                    (isInvoiceNumber ? a.InvoiceNumber.Contains(filter.InvoiceNumber) : true) &&
                                    (isIsJointAccount ? (("join").Contains(filterJointAccount.ToLower()) ? a.IsJointAccount.Value == true : (("single").Contains(filterJointAccount.ToLower()) ? a.IsJointAccount.Value == false || a.IsJointAccount.Value == null : a.CIF == "N/A")) : true) &&
                                    (isIsExpired ? (("Yes").Contains(filterIsExpired) ? DbFunctions.TruncateTime(a.ExpiredDate) < DbFunctions.TruncateTime(currentdate) : (("No").Contains(filterIsExpired) ? DbFunctions.TruncateTime(a.ExpiredDate) > DbFunctions.TruncateTime(currentdate) : a.CIF == "N/A")) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)

                                select new CustomerUnderlyingModel
                                {
                                    ID = a.UnderlyingID,
                                    CIF = a.CIF,
                                    CustomerName = a.Customer.CustomerName,
                                    UnderlyingDocument = new UnderlyingDocModel
                                    {
                                        ID = a.UnderlyingDocument.UnderlyingDocID,
                                        Code = a.UnderlyingDocument.UnderlyingDocCode,
                                        Name = a.UnderlyingDocument.UnderlyingDocName,
                                        LastModifiedBy = a.UnderlyingDocument.UpdateDate == null ? a.UnderlyingDocument.CreateBy : a.UnderlyingDocument.UpdateBy,
                                        LastModifiedDate = a.UnderlyingDocument.UpdateDate == null ? a.UnderlyingDocument.CreateDate : a.UnderlyingDocument.UpdateDate.Value
                                    },
                                    DocumentType = new DocumentTypeModel
                                    {
                                        ID = a.DocumentType.DocTypeID,
                                        Name = a.DocumentType.DocTypeName,
                                        Description = a.DocumentType.DocTypeDescription,
                                        LastModifiedBy = a.DocumentType.UpdateDate == null ? a.DocumentType.CreateBy : a.DocumentType.UpdateBy,
                                        LastModifiedDate = a.DocumentType.UpdateDate == null ? a.DocumentType.CreateDate : a.DocumentType.UpdateDate.Value
                                    },
                                    Currency = new CurrencyModel
                                    {
                                        ID = a.Currency.CurrencyID,
                                        Code = a.Currency.CurrencyCode,
                                        LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                        LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                    },
                                    StatementLetter = new StatementLetterModel
                                    {
                                        ID = a.StatementLetter.StatementLetterID,
                                        Name = a.StatementLetter.StatementLetterName,
                                        LastModifiedBy = a.StatementLetter.UpdateDate == null ? a.StatementLetter.CreateBy : a.StatementLetter.UpdateBy,
                                        LastModifiedDate = a.StatementLetter.UpdateDate == null ? a.StatementLetter.CreateDate : a.StatementLetter.UpdateDate.Value
                                    },
                                    Amount = a.Amount,
                                    AmountUSD = a.AmountUSD == null ? 0 : a.AmountUSD,
                                    AvailableAmount = a.AvailableAmountUSD == null ? 0 : a.AvailableAmountUSD,
                                    UtilizeAmountDeal = a.AvailableAmountUSD == null ? 0 : a.AvailableAmountUSD,
                                    AvailableAmountDeal = a.AvailableAmountUSD == null ? 0 : a.AvailableAmountUSD,
                                    Rate = a.Rate,
                                    AttachmentNo = a.AttachmentNo,
                                    IsDeclarationOfException = a.IsDeclarationOfExecption,
                                    StartDate = a.StartDate,
                                    EndDate = a.EndDate,
                                    DateOfUnderlying = a.DateOfUnderlying,
                                    ExpiredDate = a.ExpiredDate,
                                    ReferenceNumber = a.ReferenceNumber,
                                    SupplierName = a.SupplierName,
                                    IsUtilize = a.IsUtilize,
                                    IsProforma = a.IsProforma,
                                    IsSelectedProforma = a.IsSelectedProforma,
                                    IsBulkUnderlying = a.IsBulkUnderlying == null ? false : a.IsBulkUnderlying,
                                    IsSelectedBulk = a.IsSelectedBulk == null ? false : a.IsSelectedBulk,
                                    InvoiceNumber = a.InvoiceNumber,
                                    OtherUnderlying = a.OtherUnderlying,
                                    IsJointAccount = a.IsJointAccount.HasValue ? a.IsJointAccount.Value : false,
                                    IsTMO = a.IsTMO.HasValue ? a.IsTMO.Value : false,
                                    AccountNumber = a.AccountNumber,
                                    IsEnable = false,
                                    IsExpiredDate = DbFunctions.TruncateTime(a.ExpiredDate) <= DbFunctions.TruncateTime(currentdate) ? true : false,
                                    CreatedBy = a.CreateBy,
                                    CreatedDate = a.CreateDate,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    CustomerUnderlyingGrid.Page = page;
                    CustomerUnderlyingGrid.Size = size;
                    CustomerUnderlyingGrid.Total = data.Count();
                    CustomerUnderlyingGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    CustomerUnderlyingGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new CustomerUnderlyingRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            CIF = a.CIF,
                            CustomerName = a.CustomerName,
                            UnderlyingDocument = a.UnderlyingDocument,
                            DocumentType = a.DocumentType,
                            Currency = a.Currency,
                            AmountUSD = a.AmountUSD.HasValue ? (decimal?)Decimal.Round(a.AmountUSD.Value, 2) : null,
                            Amount = Decimal.Round(a.Amount, 2),
                            Rate = a.Rate,
                            AvailableAmount = a.AvailableAmount.HasValue ? (decimal?)Decimal.Round(a.AvailableAmount.Value, 2) : null,
                            UtilizeAmountDeal = a.AvailableAmount.HasValue ? (decimal?)Decimal.Round(a.AvailableAmount.Value, 2) : null,
                            AvailableAmountDeal = a.AvailableAmount.HasValue ? (decimal?)Decimal.Round(a.AvailableAmount.Value, 2) : null,
                            AttachmentNo = a.AttachmentNo,
                            StatementLetter = a.StatementLetter,
                            IsDeclarationOfException = a.IsDeclarationOfException,
                            StartDate = a.StartDate,
                            EndDate = a.EndDate,
                            DateOfUnderlying = a.DateOfUnderlying,
                            ExpiredDate = a.ExpiredDate,
                            ReferenceNumber = a.ReferenceNumber,
                            SupplierName = a.SupplierName,
                            InvoiceNumber = a.InvoiceNumber,
                            IsSelectedProforma = a.IsSelectedProforma,
                            IsProforma = a.IsProforma,
                            IsSelectedBulk = a.IsSelectedBulk,
                            IsBulkUnderlying = a.IsBulkUnderlying,
                            IsUtilize = a.IsUtilize,
                            IsEnable = false,
                            OtherUnderlying = a.OtherUnderlying,
                            IsJointAccount = a.IsJointAccount,
                            IsTMO = a.IsTMO,
                            IsExpiredDate = a.IsExpiredDate,
                            AccountNumber = a.AccountNumber,
                            CreatedBy = a.CreatedBy,
                            CreatedDate = a.CreatedDate,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }


                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCustomerUnderlyingWithFilterExpiredDate(int page, int size, IList<CustomerUnderlyingFilter> filters, string sortColumn, string sortOrder, ref CustomerUnderlyingGrid CustomerUnderlyingGrid, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();
                string date = string.Format("{0}-{1}-{2}", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                DateTime currentdate = DateTime.Parse(date);

                CustomerUnderlyingModel filter = new CustomerUnderlyingModel();
                string filterJointAccount = string.Empty;
                string filterIsExpired = string.Empty;
                if (filters != null)
                {
                    var parentId = filters.Where(a => a.Field.Equals("ParentID")).Select(a => a.Value).SingleOrDefault();
                    bool HasSelectBulk = filters.Where(a => a.Field.Equals("IsSelectedBulk")).Select(a => a == null ? false : true).SingleOrDefault();
                    bool isTMO_ = filters.Where(a => a.Field.Equals("IsTMO")).Select(a => a == null ? false : true).SingleOrDefault();
                    filter.ParentID = (parentId != null ? long.Parse((string)parentId) : 0);
                    filter.UnderlyingDocument = new UnderlyingDocModel { Name = (string)filters.Where(a => a.Field.Equals("UnderlyingDocument")).Select(a => a.Value).SingleOrDefault() };
                    filter.DocumentType = new DocumentTypeModel { Name = (string)filters.Where(a => a.Field.Equals("DocumentType")).Select(a => a.Value).SingleOrDefault() };
                    filter.Currency = new CurrencyModel { Code = (string)filters.Where(a => a.Field.Equals("Currency")).Select(a => a == null ? "0" : a.Value).SingleOrDefault() };
                    filter.StatementLetter = new StatementLetterModel { Name = (string)filters.Where(a => a.Field.Equals("StatementLetter")).Select(a => a.Value).SingleOrDefault() };
                    filter.Amount = Convert.ToDecimal((string)filters.Where(a => a.Field.Equals("Amount")).Select(a => a == null ? "0" : a.Value).SingleOrDefault());
                    filter.AvailableAmount = Convert.ToDecimal((string)filters.Where(a => a.Field.Equals("AvailableAmount")).Select(a => a == null ? "0" : a.Value).SingleOrDefault());
                    filter.AttachmentNo = (string)filters.Where(a => a.Field.Equals("AttachmentNo")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("StartDate")).Any())
                    {
                        filter.StartDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("StartDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("EndDate")).Any())
                    {
                        filter.EndDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("EndDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("DateOfUnderlying")).Any())
                    {
                        filter.DateOfUnderlying = DateTime.Parse((string)filters.Where(a => a.Field.Equals("DateOfUnderlying")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("ExpiredDate")).Any())
                    {
                        filter.ExpiredDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("ExpiredDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    filter.CIF = (string)filters.Where(a => a.Field.Equals("CIF")).Select(a => a.Value).SingleOrDefault();
                    filter.ReferenceNumber = (string)filters.Where(a => a.Field.Equals("ReferenceNumber")).Select(a => a.Value).SingleOrDefault();
                    filter.SupplierName = (string)filters.Where(a => a.Field.Equals("SupplierName")).Select(a => a.Value).SingleOrDefault();
                    filter.InvoiceNumber = (string)filters.Where(a => a.Field.Equals("InvoiceNumber")).Select(a => a.Value).SingleOrDefault();
                    filter.IsJointAccount = string.IsNullOrEmpty((string)filters.Where(a => a.Field.Equals("IsJointAccount")).Select(a => a.Value).SingleOrDefault()) ? false : true;
                    if (filter.IsJointAccount.HasValue && filter.IsJointAccount.Value)
                    {
                        filterJointAccount = (string)filters.Where(a => a.Field.Equals("IsJointAccount")).Select(a => a.Value).SingleOrDefault();
                    }
                    filter.IsExpired = string.IsNullOrEmpty((string)filters.Where(a => a.Field.Equals("IsExpired")).Select(a => a.Value).SingleOrDefault()) ? false : true;
                    if (filter.IsExpired.HasValue && filter.IsExpired.Value)
                    {
                        filterIsExpired = (string)filters.Where(a => a.Field.Equals("IsExpired")).Select(a => a.Value).SingleOrDefault();
                    }
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
                    filter.AccountNumber = (string)filters.Where(a => a.Field.Equals("AccountNumber")).Select(a => a.Value).SingleOrDefault();
                    string statusData = (string)filters.Where(a => a.Field.Equals("StatusShowData")).Select(a => a.Value).SingleOrDefault();
                    filter.StatusShowData = statusData != null ? int.Parse(statusData) : 0;
                    if (filters.Where(a => a.Field.Equals("IsAvailable")).SingleOrDefault() != null)
                    {
                        filter.IsAvailable = true;
                    }
                    else
                    {
                        filter.IsAvailable = false;
                    }
                    if (filters.Where(a => a.Field.Equals("IsExpiredDate")).SingleOrDefault() != null)
                    {
                        filter.IsExpiredDate = true;
                        //currentdate = currentdate.AddHours(-7);
                    }
                    else
                    {
                        filter.IsExpiredDate = false;
                    }
                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                    filter.IsSelectedBulk = HasSelectBulk ? true : false;
                    filter.IsTMO = isTMO_ ? true : false;
                }
                using (DBSEntities context = new DBSEntities())
                {
                    var accountNumber = (from j in context.SP_GETJointAccount(filter.CIF)
                                         select new CustomerAccountModel
                                         {
                                             AccountNumber = j.AccountNumber,
                                             Currency = new CurrencyModel
                                             {
                                                 ID = j.CurrencyID.Value,
                                                 Code = j.CurrencyCode,
                                                 Description = j.CurrencyDescription
                                             },
                                             CustomerName = j.CustomerName,
                                             IsJointAccount = j.IsJointAccount
                                         }).ToList();

                    IList<string> JointAccounts = new List<string>();
                    if (accountNumber != null && accountNumber.Count > 0)
                    {
                        JointAccounts = accountNumber.Where(x => x.IsJointAccount.HasValue ? x.IsJointAccount.Value.Equals(true) : false).Select(x => x.AccountNumber).ToList();
                    }
                    var data = (from a in context.CustomerUnderlyings.Where(x => x.CIF.Equals(filter.CIF) || (JointAccounts.Count > 0 ? JointAccounts.Contains(x.AccountNumber) : false))
                                let isCIF = !string.IsNullOrEmpty(filter.CIF)
                                let isUnderlyingDocument = !string.IsNullOrEmpty(filter.UnderlyingDocument.Name)
                                let isDocumentType = !string.IsNullOrEmpty(filter.DocumentType.Name)
                                let isCurrency = !string.IsNullOrEmpty(filter.Currency.Code)
                                let isStatementLetter = !string.IsNullOrEmpty(filter.StatementLetter.Name)
                                let isAmount = filter.Amount != null && filter.Amount != 0 ? true : false
                                let isAvailableAmount = filter.AvailableAmount != null && filter.AvailableAmount != 0 ? true : false
                                let isAttachmentNo = !string.IsNullOrEmpty(filter.AttachmentNo)
                                let isStartDate = filter.StartDate.HasValue ? true : false
                                let isEndDate = filter.EndDate.HasValue ? true : false
                                let isDateOfUnderlying = filter.DateOfUnderlying.HasValue ? true : false

                                let isReferenceNumber = !string.IsNullOrEmpty(filter.ReferenceNumber)
                                let isSupplierName = !string.IsNullOrEmpty(filter.SupplierName)
                                let isInvoiceNumber = filter.InvoiceNumber != null ? true : false
                                let isIsJointAccount = filter.IsJointAccount.HasValue ? filter.IsJointAccount.Value : false
                                // let isIsExpiredDate = filter.IsExpiredDate.HasValue ? filter.IsExpiredDate.Value : false 
                                let isExpiredDate = filter.ExpiredDate.HasValue ? true : false
                                let isIsExpired = filter.IsExpired.HasValue ? filter.IsExpired.Value : false
                                let isIsAvailable = filter.IsAvailable == null || filter.IsAvailable == false ? false : true
                                let isIsExpiredDate = filter.IsExpiredDate == null || filter.IsExpiredDate == false ? false : true
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                let isHasSelectBulkk = filter.IsSelectedBulk.Value ? true : false
                                let isTMO = filter.IsTMO.Value ? true : false
                                let isAccountNumber = filter.AccountNumber != null ? true : false
                                let isStatusShow = filter.StatusShowData != null ? true : false
                                where a.IsDeleted.Equals(false) &&
                                    a.ExpiredDate.Year >= currentdate.Year && a.ExpiredDate.Month >= currentdate.Month && a.ExpiredDate.Day >= currentdate.Day &&
                                    //(isOnlyJointAccount ? isAccountNumber ? a.AccountNumber.Equals(filter.AccountNumber) : isCIF ? a.CIF.Equals(filter.CIF) : true || (isAccountNumber ? a.AccountNumber.Equals(filter.AccountNumber) : true) &&
                                    (isStatusShow ? filter.StatusShowData == 0 ? true : (filter.StatusShowData == 1 ? a.CIF.Equals(filter.CIF) && (a.IsJointAccount.HasValue ? a.IsJointAccount.Value.Equals(false) : true) : filter.StatusShowData == 2 ? a.AccountNumber.Equals(filter.AccountNumber) : true) : true) &&
                                    //(isCIF ? a.CIF.Equals(filter.CIF) : true || (isAccountNumber ? a.AccountNumber.(filter.AccountNumber) : true) &&
                                    (isHasSelectBulkk ? a.IsSelectedBulk.Value.Equals(false) : true) &&
                                    (isTMO ? a.IsTMO.Value.Equals(filter.IsTMO.Value) : true) &&
                                    (isUnderlyingDocument ? a.UnderlyingDocument.UnderlyingDocName.Contains(filter.UnderlyingDocument.Name) : true) &&
                                    (isDocumentType ? a.DocumentType.DocTypeName.Contains(filter.DocumentType.Name) : true) &&
                                    (isCurrency ? a.Currency.CurrencyCode.Contains(filter.Currency.Code) : true) &&
                                    (isStatementLetter ? a.StatementLetter.StatementLetterName.Contains(filter.StatementLetter.Name) : true) &&
                                    (isAmount ? a.Amount.Equals(filter.Amount) : true) &&
                                    (isAvailableAmount ? a.AvailableAmountUSD.Value.Equals(filter.AvailableAmount.Value) : true) &&
                                    (isAttachmentNo ? a.AttachmentNo.Contains(filter.AttachmentNo) : true) &&
                                    (isStartDate ? a.StartDate.Equals(filter.StartDate) : true) &&
                                    (isEndDate ? a.EndDate.Equals(filter.EndDate) : true) &&
                                    (isDateOfUnderlying ? a.DateOfUnderlying == filter.DateOfUnderlying : true) &&
                                    (isExpiredDate ? a.ExpiredDate.Equals(filter.ExpiredDate) : true) &&
                                    (isReferenceNumber ? a.ReferenceNumber.Contains(filter.ReferenceNumber) : true) &&
                                    (isSupplierName ? a.SupplierName.Contains(filter.SupplierName) : true) &&
                                    (isIsAvailable ? (a.AvailableAmountUSD.Value > 0 || a.StatementLetterID == 4) : true) &&
                                    (isIsExpiredDate ? a.ExpiredDate >= currentdate : true) &&
                                    (isInvoiceNumber ? a.InvoiceNumber.Contains(filter.InvoiceNumber) : true) &&
                                    (isIsJointAccount ? (("Join").Contains(filterJointAccount) ? a.IsJointAccount.Value == true : (("Single").Contains(filterJointAccount) ? a.IsJointAccount.Value == false || a.IsJointAccount.Value == null : a.CIF == "N/A")) : true) &&
                                    (isIsExpired ? (("Yes").Contains(filterIsExpired) ? DbFunctions.TruncateTime(a.ExpiredDate) < DbFunctions.TruncateTime(currentdate) : (("No").Contains(filterIsExpired) ? DbFunctions.TruncateTime(a.ExpiredDate) > DbFunctions.TruncateTime(currentdate) : a.CIF == "N/A")) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)

                                select new CustomerUnderlyingModel
                                {
                                    ID = a.UnderlyingID,
                                    CIF = a.CIF,
                                    CustomerName = a.Customer.CustomerName,
                                    UnderlyingDocument = new UnderlyingDocModel
                                    {
                                        ID = a.UnderlyingDocument.UnderlyingDocID,
                                        Code = a.UnderlyingDocument.UnderlyingDocCode,
                                        Name = a.UnderlyingDocument.UnderlyingDocName,
                                        LastModifiedBy = a.UnderlyingDocument.UpdateDate == null ? a.UnderlyingDocument.CreateBy : a.UnderlyingDocument.UpdateBy,
                                        LastModifiedDate = a.UnderlyingDocument.UpdateDate == null ? a.UnderlyingDocument.CreateDate : a.UnderlyingDocument.UpdateDate.Value
                                    },
                                    DocumentType = new DocumentTypeModel
                                    {
                                        ID = a.DocumentType.DocTypeID,
                                        Name = a.DocumentType.DocTypeName,
                                        Description = a.DocumentType.DocTypeDescription,
                                        LastModifiedBy = a.DocumentType.UpdateDate == null ? a.DocumentType.CreateBy : a.DocumentType.UpdateBy,
                                        LastModifiedDate = a.DocumentType.UpdateDate == null ? a.DocumentType.CreateDate : a.DocumentType.UpdateDate.Value
                                    },
                                    Currency = new CurrencyModel
                                    {
                                        ID = a.Currency.CurrencyID,
                                        Code = a.Currency.CurrencyCode,
                                        LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                        LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                    },
                                    StatementLetter = new StatementLetterModel
                                    {
                                        ID = a.StatementLetter.StatementLetterID,
                                        Name = a.StatementLetter.StatementLetterName,
                                        LastModifiedBy = a.StatementLetter.UpdateDate == null ? a.StatementLetter.CreateBy : a.StatementLetter.UpdateBy,
                                        LastModifiedDate = a.StatementLetter.UpdateDate == null ? a.StatementLetter.CreateDate : a.StatementLetter.UpdateDate.Value
                                    },
                                    Amount = a.Amount,
                                    AmountUSD = a.AmountUSD == null ? 0 : a.AmountUSD,
                                    AvailableAmount = a.AvailableAmountUSD == null ? 0 : a.AvailableAmountUSD,
                                    Rate = a.Rate,
                                    AttachmentNo = a.AttachmentNo,
                                    IsDeclarationOfException = a.IsDeclarationOfExecption,
                                    StartDate = a.StartDate,
                                    EndDate = a.EndDate,
                                    DateOfUnderlying = a.DateOfUnderlying,
                                    ExpiredDate = a.ExpiredDate,
                                    ReferenceNumber = a.ReferenceNumber,
                                    SupplierName = a.SupplierName,
                                    IsUtilize = a.IsUtilize,
                                    IsProforma = a.IsProforma,
                                    IsSelectedProforma = a.IsSelectedProforma,
                                    IsBulkUnderlying = a.IsBulkUnderlying == null ? false : a.IsBulkUnderlying,
                                    IsSelectedBulk = a.IsSelectedBulk == null ? false : a.IsSelectedBulk,
                                    InvoiceNumber = a.InvoiceNumber,
                                    OtherUnderlying = a.OtherUnderlying,
                                    IsJointAccount = a.IsJointAccount.HasValue ? a.IsJointAccount.Value : false,
                                    IsTMO = a.IsTMO.HasValue ? a.IsTMO.Value : false,
                                    AccountNumber = a.AccountNumber,
                                    IsEnable = false,
                                    IsExpiredDate = DbFunctions.TruncateTime(a.ExpiredDate) <= DbFunctions.TruncateTime(currentdate) ? true : false,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    CustomerUnderlyingGrid.Page = page;
                    CustomerUnderlyingGrid.Size = size;
                    CustomerUnderlyingGrid.Total = data.Count();
                    CustomerUnderlyingGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    CustomerUnderlyingGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new CustomerUnderlyingRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            CIF = a.CIF,
                            CustomerName = a.CustomerName,
                            UnderlyingDocument = a.UnderlyingDocument,
                            DocumentType = a.DocumentType,
                            Currency = a.Currency,
                            AmountUSD = a.AmountUSD.HasValue ? (decimal?)Decimal.Round(a.AmountUSD.Value, 2) : null,
                            Amount = Decimal.Round(a.Amount, 2),
                            Rate = a.Rate,
                            AvailableAmount = a.AvailableAmount.HasValue ? (decimal?)Decimal.Round(a.AvailableAmount.Value, 2) : null,
                            AttachmentNo = a.AttachmentNo,
                            StatementLetter = a.StatementLetter,
                            IsDeclarationOfException = a.IsDeclarationOfException,
                            StartDate = a.StartDate,
                            EndDate = a.EndDate,
                            DateOfUnderlying = a.DateOfUnderlying,
                            ExpiredDate = a.ExpiredDate,
                            ReferenceNumber = a.ReferenceNumber,
                            SupplierName = a.SupplierName,
                            InvoiceNumber = a.InvoiceNumber,
                            IsSelectedProforma = a.IsSelectedProforma,
                            IsProforma = a.IsProforma,
                            IsSelectedBulk = a.IsSelectedBulk,
                            IsBulkUnderlying = a.IsBulkUnderlying,
                            IsUtilize = a.IsUtilize,
                            IsEnable = false,
                            OtherUnderlying = a.OtherUnderlying,
                            IsJointAccount = a.IsJointAccount,
                            IsTMO = a.IsTMO,
                            IsExpiredDate = a.IsExpiredDate,
                            AccountNumber = a.AccountNumber,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }


                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        //Draft
        public bool GetCustomerUnderlyingDraft(int page, int size, IList<CustomerUnderlyingFilter> filters, string sortColumn, string sortOrder, ref CustomerUnderlyingGrid CustomerUnderlyingGrid, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();
                string date = string.Format("{0}-{1}-{2}", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                DateTime currentdate = DateTime.Parse(date);

                CustomerUnderlyingModel filter = new CustomerUnderlyingModel();
                string filterJointAccount = string.Empty;

                if (filters != null)
                {
                    var parentId = filters.Where(a => a.Field.Equals("ParentID")).Select(a => a.Value).SingleOrDefault();
                    bool HasSelectBulk = filters.Where(a => a.Field.Equals("IsSelectedBulk")).Select(a => a == null ? false : true).SingleOrDefault();
                    filter.ParentID = (parentId != null ? long.Parse((string)parentId) : 0);
                    filter.UnderlyingDocument = new UnderlyingDocModel { Name = (string)filters.Where(a => a.Field.Equals("UnderlyingDocument")).Select(a => a.Value).SingleOrDefault() };
                    filter.DocumentType = new DocumentTypeModel { Name = (string)filters.Where(a => a.Field.Equals("DocumentType")).Select(a => a.Value).SingleOrDefault() };
                    filter.Currency = new CurrencyModel { Code = (string)filters.Where(a => a.Field.Equals("Currency")).Select(a => a == null ? "0" : a.Value).SingleOrDefault() };
                    filter.StatementLetter = new StatementLetterModel { Name = (string)filters.Where(a => a.Field.Equals("StatementLetter")).Select(a => a.Value).SingleOrDefault() };
                    filter.Amount = Convert.ToDecimal((string)filters.Where(a => a.Field.Equals("Amount")).Select(a => a == null ? "0" : a.Value).SingleOrDefault());
                    filter.AvailableAmount = Convert.ToDecimal((string)filters.Where(a => a.Field.Equals("AvailableAmount")).Select(a => a == null ? "0" : a.Value).SingleOrDefault());
                    filter.AttachmentNo = (string)filters.Where(a => a.Field.Equals("AttachmentNo")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("StartDate")).Any())
                    {
                        filter.StartDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("StartDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("EndDate")).Any())
                    {
                        filter.EndDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("EndDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("DateOfUnderlying")).Any())
                    {
                        filter.DateOfUnderlying = DateTime.Parse((string)filters.Where(a => a.Field.Equals("DateOfUnderlying")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("ExpiredDate")).Any())
                    {
                        filter.ExpiredDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("ExpiredDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    filter.CIF = (string)filters.Where(a => a.Field.Equals("CIF")).Select(a => a.Value).SingleOrDefault();
                    filter.ReferenceNumber = (string)filters.Where(a => a.Field.Equals("ReferenceNumber")).Select(a => a.Value).SingleOrDefault();
                    filter.SupplierName = (string)filters.Where(a => a.Field.Equals("SupplierName")).Select(a => a.Value).SingleOrDefault();
                    filter.InvoiceNumber = (string)filters.Where(a => a.Field.Equals("InvoiceNumber")).Select(a => a.Value).SingleOrDefault();


                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("IsAvailable")).SingleOrDefault() != null)
                    {
                        filter.IsAvailable = true;
                    }
                    else
                    {
                        filter.IsAvailable = false;
                    }
                    if (filters.Where(a => a.Field.Equals("IsExpiredDate")).SingleOrDefault() != null)
                    {
                        filter.IsExpiredDate = true;
                        //currentdate = currentdate.AddHours(-7);
                    }
                    else
                    {
                        filter.IsExpiredDate = false;
                    }
                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                    filter.IsSelectedBulk = HasSelectBulk ? true : false;

                    filter.IsJointAccount = string.IsNullOrEmpty((string)filters.Where(a => a.Field.Equals("IsJointAccount")).Select(a => a.Value).SingleOrDefault()) ? false : true;

                    if (filter.IsJointAccount.HasValue && filter.IsJointAccount.Value)
                    {
                        filterJointAccount = (string)filters.Where(a => a.Field.Equals("IsJointAccount")).Select(a => a.Value).SingleOrDefault();
                    }
                }
                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.CustomerUnderlyingDrafts
                                let isCIF = !string.IsNullOrEmpty(filter.CIF)
                                let isUnderlyingDocument = !string.IsNullOrEmpty(filter.UnderlyingDocument.Name)
                                let isDocumentType = !string.IsNullOrEmpty(filter.DocumentType.Name)
                                let isCurrency = !string.IsNullOrEmpty(filter.Currency.Code)
                                let isStatementLetter = !string.IsNullOrEmpty(filter.StatementLetter.Name)
                                let isAmount = filter.Amount != null && filter.Amount != 0 ? true : false
                                let isAvailableAmount = filter.AvailableAmount != null && filter.AvailableAmount != 0 ? true : false
                                let isAttachmentNo = !string.IsNullOrEmpty(filter.AttachmentNo)
                                let isStartDate = filter.StartDate.HasValue ? true : false
                                let isEndDate = filter.EndDate.HasValue ? true : false
                                let isDateOfUnderlying = filter.DateOfUnderlying.HasValue ? true : false
                                let isExpiredDate = filter.ExpiredDate.HasValue ? true : false
                                let isReferenceNumber = !string.IsNullOrEmpty(filter.ReferenceNumber)
                                let isSupplierName = !string.IsNullOrEmpty(filter.SupplierName)
                                let isInvoiceNumber = filter.InvoiceNumber != null ? true : false
                                let isIsAvailable = filter.IsAvailable == null || filter.IsAvailable == false ? false : true
                                let isIsExpiredDate = filter.IsExpiredDate == null || filter.IsExpiredDate == false ? false : true
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                let isHasSelectBulkk = filter.IsSelectedBulk.Value ? true : false
                                let isJointAccount = filter.IsJointAccount.HasValue ? filter.IsJointAccount.Value : false
                                where a.IsDeleted.Equals(false) &&
                                    (isCIF ? a.CIF.Equals(filter.CIF) : true) &&
                                    (isHasSelectBulkk ? a.IsSelectedBulk.Value.Equals(false) : true) &&
                                    (isUnderlyingDocument ? a.UnderlyingDocument.UnderlyingDocName.Contains(filter.UnderlyingDocument.Name) : true) &&
                                    (isDocumentType ? a.DocumentType.DocTypeName.Contains(filter.DocumentType.Name) : true) &&
                                    (isCurrency ? a.Currency.CurrencyCode.Contains(filter.Currency.Code) : true) &&
                                    (isStatementLetter ? a.StatementLetter.StatementLetterName.Contains(filter.StatementLetter.Name) : true) &&
                                    (isAmount ? a.Amount.Equals(filter.Amount) : true) &&
                                    (isAvailableAmount ? a.AvailableAmountUSD.Value.Equals(filter.AvailableAmount.Value) : true) &&
                                    (isAttachmentNo ? a.AttachmentNo.Contains(filter.AttachmentNo) : true) &&
                                    (isStartDate ? a.StartDate.Equals(filter.StartDate) : true) &&
                                    (isEndDate ? a.EndDate.Equals(filter.EndDate) : true) &&
                                    (isDateOfUnderlying ? a.DateOfUnderlying == filter.DateOfUnderlying : true) &&
                                    (isExpiredDate ? a.ExpiredDate.Equals(filter.ExpiredDate) : true) &&
                                    (isReferenceNumber ? a.ReferenceNumber.Contains(filter.ReferenceNumber) : true) &&
                                    (isSupplierName ? a.SupplierName.Contains(filter.SupplierName) : true) &&
                                    (isIsAvailable ? a.AvailableAmountUSD.Value > 0 : true) &&
                                    (isIsExpiredDate ? a.ExpiredDate >= currentdate : true) &&
                                    (isInvoiceNumber ? a.InvoiceNumber.Contains(filter.InvoiceNumber) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true) &&
                                    (isJointAccount ? (("join").Contains(filterJointAccount.ToLower()) ? a.IsJointAccount.Value == true : (("single").Contains(filterJointAccount.ToLower()) ? a.IsJointAccount.Value == false || a.IsJointAccount.Value == null : a.CIF == "N/A")) : true)

                                select new CustomerUnderlyingModel
                                {
                                    ID = a.UnderlyingID,
                                    CIF = a.CIF,
                                    CustomerName = a.Customer.CustomerName,
                                    UnderlyingDocument = new UnderlyingDocModel
                                    {
                                        ID = a.UnderlyingDocument.UnderlyingDocID,
                                        Code = a.UnderlyingDocument.UnderlyingDocCode,
                                        Name = a.UnderlyingDocument.UnderlyingDocName,
                                        LastModifiedBy = a.UnderlyingDocument.UpdateDate == null ? a.UnderlyingDocument.CreateBy : a.UnderlyingDocument.UpdateBy,
                                        LastModifiedDate = a.UnderlyingDocument.UpdateDate == null ? a.UnderlyingDocument.CreateDate : a.UnderlyingDocument.UpdateDate.Value
                                    },
                                    DocumentType = new DocumentTypeModel
                                    {
                                        ID = a.DocumentType.DocTypeID,
                                        Name = a.DocumentType.DocTypeName,
                                        Description = a.DocumentType.DocTypeDescription,
                                        LastModifiedBy = a.DocumentType.UpdateDate == null ? a.DocumentType.CreateBy : a.DocumentType.UpdateBy,
                                        LastModifiedDate = a.DocumentType.UpdateDate == null ? a.DocumentType.CreateDate : a.DocumentType.UpdateDate.Value
                                    },
                                    Currency = new CurrencyModel
                                    {
                                        ID = a.Currency.CurrencyID,
                                        Code = a.Currency.CurrencyCode,
                                        LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                        LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                    },
                                    StatementLetter = new StatementLetterModel
                                    {
                                        ID = a.StatementLetter.StatementLetterID,
                                        Name = a.StatementLetter.StatementLetterName,
                                        LastModifiedBy = a.StatementLetter.UpdateDate == null ? a.StatementLetter.CreateBy : a.StatementLetter.UpdateBy,
                                        LastModifiedDate = a.StatementLetter.UpdateDate == null ? a.StatementLetter.CreateDate : a.StatementLetter.UpdateDate.Value
                                    },
                                    Amount = a.Amount,
                                    AmountUSD = a.AmountUSD == null ? 0 : a.AmountUSD,
                                    AvailableAmount = a.AvailableAmountUSD == null ? 0 : a.AvailableAmountUSD,
                                    Rate = a.Rate,
                                    AttachmentNo = a.AttachmentNo,
                                    IsDeclarationOfException = a.IsDeclarationOfExecption,
                                    StartDate = a.StartDate,
                                    EndDate = a.EndDate,
                                    DateOfUnderlying = a.DateOfUnderlying,
                                    ExpiredDate = a.ExpiredDate,
                                    ReferenceNumber = a.ReferenceNumber,
                                    SupplierName = a.SupplierName,
                                    IsUtilize = a.IsUtilize,
                                    IsProforma = a.IsProforma,
                                    IsSelectedProforma = a.IsSelectedProforma,
                                    IsBulkUnderlying = a.IsBulkUnderlying == null ? false : a.IsBulkUnderlying,
                                    IsSelectedBulk = a.IsSelectedBulk == null ? false : a.IsSelectedBulk,
                                    IsJointAccount = a.IsJointAccount.HasValue ? a.IsJointAccount.Value : false,
                                    InvoiceNumber = a.InvoiceNumber,
                                    OtherUnderlying = a.OtherUnderlying,
                                    IsEnable = false,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    CustomerUnderlyingGrid.Page = page;
                    CustomerUnderlyingGrid.Size = size;
                    CustomerUnderlyingGrid.Total = data.Count();
                    CustomerUnderlyingGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    CustomerUnderlyingGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new CustomerUnderlyingRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            CIF = a.CIF,
                            CustomerName = a.CustomerName,
                            UnderlyingDocument = a.UnderlyingDocument,
                            DocumentType = a.DocumentType,
                            Currency = a.Currency,
                            AmountUSD = a.AmountUSD.HasValue ? (decimal?)Decimal.Round(a.AmountUSD.Value, 2) : null,
                            Amount = Decimal.Round(a.Amount, 2),
                            Rate = a.Rate,
                            AvailableAmount = a.AvailableAmount.HasValue ? (decimal?)Decimal.Round(a.AvailableAmount.Value, 2) : null,
                            AttachmentNo = a.AttachmentNo,
                            StatementLetter = a.StatementLetter,
                            IsDeclarationOfException = a.IsDeclarationOfException,
                            StartDate = a.StartDate,
                            EndDate = a.EndDate,
                            DateOfUnderlying = a.DateOfUnderlying,
                            ExpiredDate = a.ExpiredDate,
                            ReferenceNumber = a.ReferenceNumber,
                            SupplierName = a.SupplierName,
                            InvoiceNumber = a.InvoiceNumber,
                            IsSelectedProforma = a.IsSelectedProforma,
                            IsProforma = a.IsProforma,
                            IsSelectedBulk = a.IsSelectedBulk,
                            IsBulkUnderlying = a.IsBulkUnderlying,
                            IsJointAccount = a.IsJointAccount,
                            IsUtilize = a.IsUtilize,
                            IsEnable = false,
                            OtherUnderlying = a.OtherUnderlying,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }


                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool GetCustomerUnderlyingProforma(int page, int size, IList<CustomerUnderlyingFilter> filters, string sortColumn, string sortOrder, ref CustomerUnderlyingGrid CustomerUnderlyingGrid, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();
                string filterJointAccount = string.Empty;
                CustomerUnderlyingModel filter = new CustomerUnderlyingModel();
                if (filters != null)
                {
                    var parentId = filters.Where(a => a.Field.Equals("ParentID")).Select(a => a.Value).SingleOrDefault();
                    filter.ParentID = (parentId != null ? long.Parse((string)parentId) : 0);
                    filter.CIF = (string)filters.Where(a => a.Field.Equals("CIF")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("IsSelected")).Any())
                    {
                        filter.IsSelectedProforma = bool.Parse((string)filters.Where(a => a.Field.Equals("IsSelected")).Select(a => a.Value).SingleOrDefault());
                    }
                    filter.UnderlyingDocument = new UnderlyingDocModel { Name = (string)filters.Where(a => a.Field.Equals("UnderlyingDocument")).Select(a => a.Value).SingleOrDefault() };
                    filter.DocumentType = new DocumentTypeModel { Name = (string)filters.Where(a => a.Field.Equals("DocumentType")).Select(a => a.Value).SingleOrDefault() };
                    filter.Currency = new CurrencyModel { Code = (string)filters.Where(a => a.Field.Equals("Currency")).Select(a => a == null ? "0" : a.Value).SingleOrDefault() };
                    filter.StatementLetter = new StatementLetterModel { Name = (string)filters.Where(a => a.Field.Equals("StatementLetter")).Select(a => a.Value).SingleOrDefault() };
                    filter.Amount = Convert.ToDecimal((string)filters.Where(a => a.Field.Equals("Amount")).Select(a => a == null ? "0" : a.Value).SingleOrDefault());
                    filter.AttachmentNo = (string)filters.Where(a => a.Field.Equals("AttachmentNo")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("StartDate")).Any())
                    {
                        filter.StartDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("StartDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("EndDate")).Any())
                    {
                        filter.EndDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("EndDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("DateOfUnderlying")).Any())
                    {
                        filter.DateOfUnderlying = DateTime.Parse((string)filters.Where(a => a.Field.Equals("DateOfUnderlying")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("ExpiredDate")).Any())
                    {
                        filter.ExpiredDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("ExpiredDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    filter.ReferenceNumber = (string)filters.Where(a => a.Field.Equals("ReferenceNumber")).Select(a => a.Value).SingleOrDefault();
                    filter.SupplierName = (string)filters.Where(a => a.Field.Equals("SupplierName")).Select(a => a.Value).SingleOrDefault();
                    filter.InvoiceNumber = (string)filters.Where(a => a.Field.Equals("InvoiceNumber")).Select(a => a.Value).SingleOrDefault();
                    //ady
                    filter.IsJointAccount = string.IsNullOrEmpty((string)filters.Where(a => a.Field.Equals("IsJointAccount")).Select(a => a.Value).SingleOrDefault()) ? false : true;
                    if (filter.IsJointAccount.HasValue && filter.IsJointAccount.Value)
                    {
                        filterJointAccount = (string)filters.Where(a => a.Field.Equals("IsJointAccount")).Select(a => a.Value).SingleOrDefault();
                    }

                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }
                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.CustomerUnderlyings
                                join b in context.MappingUnderlyings.Where(z => z.IsDeleted.Equals(false)) on a.UnderlyingID equals b.UnderlyingID into temp
                                from x in temp.DefaultIfEmpty()
                                let isCIF = !string.IsNullOrEmpty(filter.CIF)
                                let isUnderlyingDocument = !string.IsNullOrEmpty(filter.UnderlyingDocument.Name)
                                let isDocumentType = !string.IsNullOrEmpty(filter.DocumentType.Name)
                                let isCurrency = !string.IsNullOrEmpty(filter.Currency.Code)
                                let isStatementLetter = !string.IsNullOrEmpty(filter.StatementLetter.Name)
                                let isAmount = filter.Amount != null && filter.Amount != 0 ? true : false
                                let isAttachmentNo = !string.IsNullOrEmpty(filter.AttachmentNo)
                                let isStartDate = filter.StartDate.HasValue ? true : false
                                let isEndDate = filter.EndDate.HasValue ? true : false
                                let isDateOfUnderlying = filter.DateOfUnderlying.HasValue ? true : false
                                let isExpiredDate = filter.ExpiredDate.HasValue ? true : false
                                let isReferenceNumber = !string.IsNullOrEmpty(filter.ReferenceNumber)
                                let isSupplierName = !string.IsNullOrEmpty(filter.SupplierName)
                                let isInvoiceNumber = filter.InvoiceNumber != null ? true : false
                                let isIsJointAccount = filter.IsJointAccount.HasValue ? filter.IsJointAccount.Value : false
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where (x.ParentID == null || x.ParentID.Equals(filter.ParentID)) &&
                                a.DocumentType.DocTypeName.Contains(filter.DocumentType.Name) && a.UnderlyingID != filter.ParentID &&
                                    (isCIF ? a.CIF.Equals(filter.CIF) : true) &&
                                    (isUnderlyingDocument ? a.UnderlyingDocument.UnderlyingDocName.Contains(filter.UnderlyingDocument.Name) : true) &&
                                    (isDocumentType ? a.DocumentType.DocTypeName.Contains(filter.DocumentType.Name) : true) &&
                                    (isCurrency ? a.Currency.CurrencyCode.Contains(filter.Currency.Code) : true) &&
                                    (isStatementLetter ? a.StatementLetter.StatementLetterName.Contains(filter.StatementLetter.Name) : true) &&
                                    (isAmount ? a.Amount.Equals(filter.Amount) : true) &&
                                    (isAttachmentNo ? a.AttachmentNo.Contains(filter.AttachmentNo) : true) &&
                                    (isStartDate ? a.StartDate.Equals(filter.StartDate) : true) &&
                                    (isEndDate ? a.EndDate.Equals(filter.EndDate) : true) &&
                                    (isDateOfUnderlying ? a.DateOfUnderlying.Equals(filter.DateOfUnderlying) : true) &&
                                    (isExpiredDate ? a.ExpiredDate.Equals(filter.ExpiredDate) : true) &&
                                    (isReferenceNumber ? a.ReferenceNumber.Contains(filter.ReferenceNumber) : true) &&
                                    (isSupplierName ? a.SupplierName.Contains(filter.SupplierName) : true) &&
                                    (isInvoiceNumber ? a.InvoiceNumber.Contains(filter.InvoiceNumber) : true) &&
                                    (isIsJointAccount ? (("Join").Contains(filterJointAccount) ? a.IsJointAccount.Value == true : (("Single").Contains(filterJointAccount) ? a.IsJointAccount.Value == false || a.IsJointAccount.Value == null : a.CIF == "N/A")) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)

                                select new CustomerUnderlyingModel
                                {
                                    ID = a.UnderlyingID,
                                    CIF = a.CIF,
                                    UnderlyingDocument = new UnderlyingDocModel
                                    {
                                        ID = a.UnderlyingDocument.UnderlyingDocID,
                                        Code = a.UnderlyingDocument.UnderlyingDocCode,
                                        Name = a.UnderlyingDocument.UnderlyingDocName,
                                        LastModifiedBy = a.UnderlyingDocument.UpdateDate == null ? a.UnderlyingDocument.CreateBy : a.UnderlyingDocument.UpdateBy,
                                        LastModifiedDate = a.UnderlyingDocument.UpdateDate == null ? a.UnderlyingDocument.CreateDate : a.UnderlyingDocument.UpdateDate.Value
                                    },
                                    DocumentType = new DocumentTypeModel
                                    {
                                        ID = a.DocumentType.DocTypeID,
                                        Name = a.DocumentType.DocTypeName,
                                        Description = a.DocumentType.DocTypeDescription,
                                        LastModifiedBy = a.DocumentType.UpdateDate == null ? a.DocumentType.CreateBy : a.DocumentType.UpdateBy,
                                        LastModifiedDate = a.DocumentType.UpdateDate == null ? a.DocumentType.CreateDate : a.DocumentType.UpdateDate.Value
                                    },
                                    Currency = new CurrencyModel
                                    {
                                        ID = a.Currency.CurrencyID,
                                        Code = a.Currency.CurrencyCode,
                                        LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                        LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                    },
                                    StatementLetter = new StatementLetterModel
                                    {
                                        ID = a.StatementLetter.StatementLetterID,
                                        Name = a.StatementLetter.StatementLetterName,
                                        LastModifiedBy = a.StatementLetter.UpdateDate == null ? a.StatementLetter.CreateBy : a.StatementLetter.UpdateBy,
                                        LastModifiedDate = a.StatementLetter.UpdateDate == null ? a.StatementLetter.CreateDate : a.StatementLetter.UpdateDate.Value
                                    },
                                    Amount = a.Amount,
                                    AmountUSD = a.AmountUSD,
                                    AttachmentNo = a.AttachmentNo,
                                    IsDeclarationOfException = a.IsDeclarationOfExecption,
                                    StartDate = a.StartDate,
                                    EndDate = a.EndDate,
                                    DateOfUnderlying = a.DateOfUnderlying,
                                    ExpiredDate = a.ExpiredDate,
                                    ReferenceNumber = a.ReferenceNumber,
                                    SupplierName = a.SupplierName,
                                    InvoiceNumber = a.InvoiceNumber,
                                    IsProforma = a.IsProforma,
                                    IsSelectedProforma = a.IsSelectedProforma,
                                    IsBulkUnderlying = a.IsBulkUnderlying == null ? false : a.IsBulkUnderlying,
                                    IsSelectedBulk = a.IsSelectedBulk == null ? false : a.IsSelectedBulk,
                                    IsUtilize = a.IsUtilize,
                                    OtherUnderlying = a.OtherUnderlying,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    CustomerUnderlyingGrid.Page = page;
                    CustomerUnderlyingGrid.Size = size;
                    CustomerUnderlyingGrid.Total = data.Count();
                    CustomerUnderlyingGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    CustomerUnderlyingGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new CustomerUnderlyingRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            CIF = a.CIF,
                            UnderlyingDocument = a.UnderlyingDocument,
                            DocumentType = a.DocumentType,
                            Currency = a.Currency,
                            Amount = a.Amount,
                            AttachmentNo = a.AttachmentNo,
                            StatementLetter = a.StatementLetter,
                            IsDeclarationOfException = a.IsDeclarationOfException,
                            StartDate = a.StartDate,
                            EndDate = a.EndDate,
                            DateOfUnderlying = a.DateOfUnderlying,
                            ExpiredDate = a.ExpiredDate,
                            ReferenceNumber = a.ReferenceNumber,
                            SupplierName = a.SupplierName,
                            IsProforma = a.IsProforma,
                            IsSelectedProforma = a.IsSelectedProforma,
                            IsBulkUnderlying = a.IsBulkUnderlying,
                            IsSelectedBulk = a.IsSelectedBulk,
                            IsUtilize = a.IsUtilize,
                            InvoiceNumber = a.InvoiceNumber,
                            OtherUnderlying = a.OtherUnderlying,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCustomerBulkUnderlying(int page, int size, IList<CustomerUnderlyingFilter> filters, string sortColumn, string sortOrder, ref CustomerUnderlyingGrid CustomerUnderlyingGrid, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();
                CustomerUnderlyingModel filter = new CustomerUnderlyingModel();
                if (filters != null)
                {
                    var parentId = filters.Where(a => a.Field.Equals("ParentID")).Select(a => a.Value).SingleOrDefault();
                    filter.ParentID = (parentId != null ? long.Parse((string)parentId) : 0);
                    filter.CIF = (string)filters.Where(a => a.Field.Equals("CIF")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("IsSelected")).Any())
                    {
                        filter.IsSelectedProforma = bool.Parse((string)filters.Where(a => a.Field.Equals("IsSelected")).Select(a => a.Value).SingleOrDefault());
                    }
                    filter.UnderlyingDocument = new UnderlyingDocModel { Name = (string)filters.Where(a => a.Field.Equals("UnderlyingDocument")).Select(a => a.Value).SingleOrDefault() };
                    filter.DocumentType = new DocumentTypeModel { Name = (string)filters.Where(a => a.Field.Equals("DocumentType")).Select(a => a.Value).SingleOrDefault() };
                    filter.Currency = new CurrencyModel { Code = (string)filters.Where(a => a.Field.Equals("Currency")).Select(a => a == null ? "0" : a.Value).SingleOrDefault() };
                    filter.StatementLetter = new StatementLetterModel { Name = (string)filters.Where(a => a.Field.Equals("StatementLetter")).Select(a => a.Value).SingleOrDefault() };
                    filter.Amount = Convert.ToDecimal((string)filters.Where(a => a.Field.Equals("Amount")).Select(a => a == null ? "0" : a.Value).SingleOrDefault());
                    filter.AttachmentNo = (string)filters.Where(a => a.Field.Equals("AttachmentNo")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("StartDate")).Any())
                    {
                        filter.StartDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("StartDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("EndDate")).Any())
                    {
                        filter.EndDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("EndDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("DateOfUnderlying")).Any())
                    {
                        filter.DateOfUnderlying = DateTime.Parse((string)filters.Where(a => a.Field.Equals("DateOfUnderlying")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("ExpiredDate")).Any())
                    {
                        filter.ExpiredDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("ExpiredDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    filter.ReferenceNumber = (string)filters.Where(a => a.Field.Equals("ReferenceNumber")).Select(a => a.Value).SingleOrDefault();
                    filter.SupplierName = (string)filters.Where(a => a.Field.Equals("SupplierName")).Select(a => a.Value).SingleOrDefault();
                    filter.InvoiceNumber = (string)filters.Where(a => a.Field.Equals("InvoiceNumber")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }
                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.CustomerUnderlyings
                                join b in context.MappingBulkUnderlyings.Where(z => z.IsDeleted.Equals(false)) on a.UnderlyingID equals b.UnderlyingID into temp
                                from x in temp.DefaultIfEmpty()
                                let isCIF = !string.IsNullOrEmpty(filter.CIF)
                                let isUnderlyingDocument = !string.IsNullOrEmpty(filter.UnderlyingDocument.Name)
                                let isDocumentType = !string.IsNullOrEmpty(filter.DocumentType.Name)
                                let isCurrency = !string.IsNullOrEmpty(filter.Currency.Code)
                                let isStatementLetter = !string.IsNullOrEmpty(filter.StatementLetter.Name)
                                let isAmount = filter.Amount != null && filter.Amount != 0 ? true : false
                                let isAttachmentNo = !string.IsNullOrEmpty(filter.AttachmentNo)
                                let isStartDate = filter.StartDate.HasValue ? true : false
                                let isEndDate = filter.EndDate.HasValue ? true : false
                                let isDateOfUnderlying = filter.DateOfUnderlying.HasValue ? true : false
                                let isExpiredDate = filter.ExpiredDate.HasValue ? true : false
                                let isReferenceNumber = !string.IsNullOrEmpty(filter.ReferenceNumber)
                                let isSupplierName = !string.IsNullOrEmpty(filter.SupplierName)
                                let isInvoiceNumber = filter.InvoiceNumber != null ? true : false
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where (x.ParentID == null || x.ParentID.Equals(filter.ParentID)) &&
                                    a.IsBulkUnderlying.Value == false && a.IsDeleted == false &&
                                    a.UnderlyingID != filter.ParentID && a.IsUtilize.Equals(false) &&
                                    a.StatementLetterID == 2 && // statement B
                                    (isCIF ? a.CIF.Equals(filter.CIF) : true) &&
                                    (isUnderlyingDocument ? a.UnderlyingDocument.UnderlyingDocName.Contains(filter.UnderlyingDocument.Name) : true) &&
                                    (isDocumentType ? a.DocumentType.DocTypeName.Contains(filter.DocumentType.Name) : true) &&
                                    (isCurrency ? a.Currency.CurrencyCode.Contains(filter.Currency.Code) : true) &&
                                    (isStatementLetter ? a.StatementLetter.StatementLetterName.Contains(filter.StatementLetter.Name) : true) &&
                                    (isAmount ? a.Amount.Equals(filter.Amount) : true) &&
                                    (isAttachmentNo ? a.AttachmentNo.Contains(filter.AttachmentNo) : true) &&
                                    (isStartDate ? a.StartDate.Equals(filter.StartDate) : true) &&
                                    (isEndDate ? a.EndDate.Equals(filter.EndDate) : true) &&
                                    (isDateOfUnderlying ? a.DateOfUnderlying.Equals(filter.DateOfUnderlying) : true) &&
                                    (isExpiredDate ? a.ExpiredDate.Equals(filter.ExpiredDate) : true) &&
                                    (isReferenceNumber ? a.ReferenceNumber.Contains(filter.ReferenceNumber) : true) &&
                                    (isSupplierName ? a.SupplierName.Contains(filter.SupplierName) : true) &&
                                    (isInvoiceNumber ? a.InvoiceNumber.Contains(filter.InvoiceNumber) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)

                                select new CustomerUnderlyingModel
                                {
                                    ID = a.UnderlyingID,
                                    CIF = a.CIF,
                                    UnderlyingDocument = new UnderlyingDocModel
                                    {
                                        ID = a.UnderlyingDocument.UnderlyingDocID,
                                        Code = a.UnderlyingDocument.UnderlyingDocCode,
                                        Name = a.UnderlyingDocument.UnderlyingDocName,
                                        LastModifiedBy = a.UnderlyingDocument.UpdateDate == null ? a.UnderlyingDocument.CreateBy : a.UnderlyingDocument.UpdateBy,
                                        LastModifiedDate = a.UnderlyingDocument.UpdateDate == null ? a.UnderlyingDocument.CreateDate : a.UnderlyingDocument.UpdateDate.Value
                                    },
                                    DocumentType = new DocumentTypeModel
                                    {
                                        ID = a.DocumentType.DocTypeID,
                                        Name = a.DocumentType.DocTypeName,
                                        Description = a.DocumentType.DocTypeDescription,
                                        LastModifiedBy = a.DocumentType.UpdateDate == null ? a.DocumentType.CreateBy : a.DocumentType.UpdateBy,
                                        LastModifiedDate = a.DocumentType.UpdateDate == null ? a.DocumentType.CreateDate : a.DocumentType.UpdateDate.Value
                                    },
                                    Currency = new CurrencyModel
                                    {
                                        ID = a.Currency.CurrencyID,
                                        Code = a.Currency.CurrencyCode,
                                        LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                        LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                    },
                                    StatementLetter = new StatementLetterModel
                                    {
                                        ID = a.StatementLetter.StatementLetterID,
                                        Name = a.StatementLetter.StatementLetterName,
                                        LastModifiedBy = a.StatementLetter.UpdateDate == null ? a.StatementLetter.CreateBy : a.StatementLetter.UpdateBy,
                                        LastModifiedDate = a.StatementLetter.UpdateDate == null ? a.StatementLetter.CreateDate : a.StatementLetter.UpdateDate.Value
                                    },
                                    Amount = a.Amount,
                                    AmountUSD = a.AmountUSD,
                                    AttachmentNo = a.AttachmentNo,
                                    IsDeclarationOfException = a.IsDeclarationOfExecption,
                                    StartDate = a.StartDate,
                                    EndDate = a.EndDate,
                                    DateOfUnderlying = a.DateOfUnderlying,
                                    ExpiredDate = a.ExpiredDate,
                                    ReferenceNumber = a.ReferenceNumber,
                                    SupplierName = a.SupplierName,
                                    InvoiceNumber = a.InvoiceNumber,
                                    IsProforma = a.IsProforma,
                                    IsSelectedProforma = a.IsSelectedProforma,
                                    IsBulkUnderlying = a.IsBulkUnderlying == null ? false : a.IsBulkUnderlying,
                                    IsSelectedBulk = a.IsSelectedBulk == null ? false : a.IsSelectedBulk,
                                    IsUtilize = a.IsUtilize,
                                    OtherUnderlying = a.OtherUnderlying,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    CustomerUnderlyingGrid.Page = page;
                    CustomerUnderlyingGrid.Size = size;
                    CustomerUnderlyingGrid.Total = data.Count();
                    CustomerUnderlyingGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    CustomerUnderlyingGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new CustomerUnderlyingRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            CIF = a.CIF,
                            UnderlyingDocument = a.UnderlyingDocument,
                            DocumentType = a.DocumentType,
                            Currency = a.Currency,
                            Amount = a.Amount,
                            AttachmentNo = a.AttachmentNo,
                            StatementLetter = a.StatementLetter,
                            IsDeclarationOfException = a.IsDeclarationOfException,
                            StartDate = a.StartDate,
                            EndDate = a.EndDate,
                            DateOfUnderlying = a.DateOfUnderlying,
                            ExpiredDate = a.ExpiredDate,
                            ReferenceNumber = a.ReferenceNumber,
                            SupplierName = a.SupplierName,
                            IsProforma = a.IsProforma,
                            IsSelectedProforma = a.IsSelectedProforma,
                            IsBulkUnderlying = a.IsBulkUnderlying,
                            IsSelectedBulk = a.IsSelectedBulk,
                            IsUtilize = a.IsUtilize,
                            InvoiceNumber = a.InvoiceNumber,
                            OtherUnderlying = a.OtherUnderlying,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCustomerBulkUnderlyingJoint(int page, int size, IList<CustomerUnderlyingFilter> filters, string sortColumn, string sortOrder, ref CustomerUnderlyingGrid CustomerUnderlyingGrid, ref string message)
        {
            //Ambil semua underlying pada CIF dan underlying pada Account joint
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();
                CustomerUnderlyingModel filter = new CustomerUnderlyingModel();
                string[] accountNumbers = { };

                if (filters != null)
                {
                    var parentId = filters.Where(a => a.Field.Equals("ParentID")).Select(a => a.Value).SingleOrDefault();
                    filter.ParentID = (parentId != null ? long.Parse((string)parentId) : 0);
                    filter.CIF = (string)filters.Where(a => a.Field.Equals("CIF")).Select(a => a.Value).SingleOrDefault();

                    if (filter.CIF != null)
                    {
                        accountNumbers = context.CustomerAccountMappings.Where(x => x.AccountNumber.Equals(filter.AccountNumber)).Select(x => x.AccountNumber.ToLower()).ToArray();
                    }

                    if (filters.Where(a => a.Field.Equals("IsSelected")).Any())
                    {
                        filter.IsSelectedProforma = bool.Parse((string)filters.Where(a => a.Field.Equals("IsSelected")).Select(a => a.Value).SingleOrDefault());
                    }
                    filter.UnderlyingDocument = new UnderlyingDocModel { Name = (string)filters.Where(a => a.Field.Equals("UnderlyingDocument")).Select(a => a.Value).SingleOrDefault() };
                    filter.DocumentType = new DocumentTypeModel { Name = (string)filters.Where(a => a.Field.Equals("DocumentType")).Select(a => a.Value).SingleOrDefault() };
                    filter.Currency = new CurrencyModel { Code = (string)filters.Where(a => a.Field.Equals("Currency")).Select(a => a == null ? "0" : a.Value).SingleOrDefault() };
                    filter.StatementLetter = new StatementLetterModel { Name = (string)filters.Where(a => a.Field.Equals("StatementLetter")).Select(a => a.Value).SingleOrDefault() };
                    filter.Amount = Convert.ToDecimal((string)filters.Where(a => a.Field.Equals("Amount")).Select(a => a == null ? "0" : a.Value).SingleOrDefault());
                    filter.AttachmentNo = (string)filters.Where(a => a.Field.Equals("AttachmentNo")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("StartDate")).Any())
                    {
                        filter.StartDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("StartDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("EndDate")).Any())
                    {
                        filter.EndDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("EndDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("DateOfUnderlying")).Any())
                    {
                        filter.DateOfUnderlying = DateTime.Parse((string)filters.Where(a => a.Field.Equals("DateOfUnderlying")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("ExpiredDate")).Any())
                    {
                        filter.ExpiredDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("ExpiredDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    filter.ReferenceNumber = (string)filters.Where(a => a.Field.Equals("ReferenceNumber")).Select(a => a.Value).SingleOrDefault();
                    filter.SupplierName = (string)filters.Where(a => a.Field.Equals("SupplierName")).Select(a => a.Value).SingleOrDefault();
                    filter.InvoiceNumber = (string)filters.Where(a => a.Field.Equals("InvoiceNumber")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }
                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.CustomerUnderlyings
                                join b in context.MappingBulkUnderlyings.Where(z => z.IsDeleted.Equals(false)) on a.UnderlyingID equals b.UnderlyingID into temp
                                from x in temp.DefaultIfEmpty()
                                let isCIF = !string.IsNullOrEmpty(filter.CIF)
                                let isAccountNumber = !string.IsNullOrEmpty(filter.AccountNumber)
                                let isUnderlyingDocument = !string.IsNullOrEmpty(filter.UnderlyingDocument.Name)
                                let isDocumentType = !string.IsNullOrEmpty(filter.DocumentType.Name)
                                let isCurrency = !string.IsNullOrEmpty(filter.Currency.Code)
                                let isStatementLetter = !string.IsNullOrEmpty(filter.StatementLetter.Name)
                                let isAmount = filter.Amount != null && filter.Amount != 0 ? true : false
                                let isAttachmentNo = !string.IsNullOrEmpty(filter.AttachmentNo)
                                let isStartDate = filter.StartDate.HasValue ? true : false
                                let isEndDate = filter.EndDate.HasValue ? true : false
                                let isDateOfUnderlying = filter.DateOfUnderlying.HasValue ? true : false
                                let isExpiredDate = filter.ExpiredDate.HasValue ? true : false
                                let isReferenceNumber = !string.IsNullOrEmpty(filter.ReferenceNumber)
                                let isSupplierName = !string.IsNullOrEmpty(filter.SupplierName)
                                let isInvoiceNumber = filter.InvoiceNumber != null ? true : false
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false

                                where (x.ParentID == null || x.ParentID.Equals(filter.ParentID)) &&
                                    a.IsBulkUnderlying.Value == false && a.IsDeleted == false &&
                                    a.UnderlyingID != filter.ParentID && a.IsUtilize.Equals(false) &&
                                    a.StatementLetterID == 2 && // statement B
                                    a.ExpiredDate.Year >= DateTime.Now.Year && a.ExpiredDate.Month >= DateTime.Now.Month && a.ExpiredDate.Day >= DateTime.Now.Day &&
                                    (isCIF ? a.CIF.Equals(filter.CIF) || accountNumbers.Contains(filter.AccountNumber.ToLower()) : true) &&
                                    (isAccountNumber ? accountNumbers.Contains(filter.AccountNumber.ToLower()) && a.IsJointAccount.HasValue == true : true) &&
                                    (isUnderlyingDocument ? a.UnderlyingDocument.UnderlyingDocName.Contains(filter.UnderlyingDocument.Name) : true) &&
                                    (isDocumentType ? a.DocumentType.DocTypeName.Contains(filter.DocumentType.Name) : true) &&
                                    (isCurrency ? a.Currency.CurrencyCode.Contains(filter.Currency.Code) : true) &&
                                    (isStatementLetter ? a.StatementLetter.StatementLetterName.Contains(filter.StatementLetter.Name) : true) &&
                                    (isAmount ? a.Amount.Equals(filter.Amount) : true) &&
                                    (isAttachmentNo ? a.AttachmentNo.Contains(filter.AttachmentNo) : true) &&
                                    (isStartDate ? a.StartDate.Equals(filter.StartDate) : true) &&
                                    (isEndDate ? a.EndDate.Equals(filter.EndDate) : true) &&
                                    (isDateOfUnderlying ? a.DateOfUnderlying.Equals(filter.DateOfUnderlying) : true) &&
                                    (isExpiredDate ? a.ExpiredDate.Equals(filter.ExpiredDate) : true) &&
                                    (isReferenceNumber ? a.ReferenceNumber.Contains(filter.ReferenceNumber) : true) &&
                                    (isSupplierName ? a.SupplierName.Contains(filter.SupplierName) : true) &&
                                    (isInvoiceNumber ? a.InvoiceNumber.Contains(filter.InvoiceNumber) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)

                                select new CustomerUnderlyingModel
                                {
                                    ID = a.UnderlyingID,
                                    CIF = a.CIF,
                                    UnderlyingDocument = new UnderlyingDocModel
                                    {
                                        ID = a.UnderlyingDocument.UnderlyingDocID,
                                        Code = a.UnderlyingDocument.UnderlyingDocCode,
                                        Name = a.UnderlyingDocument.UnderlyingDocName,
                                        LastModifiedBy = a.UnderlyingDocument.UpdateDate == null ? a.UnderlyingDocument.CreateBy : a.UnderlyingDocument.UpdateBy,
                                        LastModifiedDate = a.UnderlyingDocument.UpdateDate == null ? a.UnderlyingDocument.CreateDate : a.UnderlyingDocument.UpdateDate.Value
                                    },
                                    DocumentType = new DocumentTypeModel
                                    {
                                        ID = a.DocumentType.DocTypeID,
                                        Name = a.DocumentType.DocTypeName,
                                        Description = a.DocumentType.DocTypeDescription,
                                        LastModifiedBy = a.DocumentType.UpdateDate == null ? a.DocumentType.CreateBy : a.DocumentType.UpdateBy,
                                        LastModifiedDate = a.DocumentType.UpdateDate == null ? a.DocumentType.CreateDate : a.DocumentType.UpdateDate.Value
                                    },
                                    Currency = new CurrencyModel
                                    {
                                        ID = a.Currency.CurrencyID,
                                        Code = a.Currency.CurrencyCode,
                                        LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                        LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                    },
                                    StatementLetter = new StatementLetterModel
                                    {
                                        ID = a.StatementLetter.StatementLetterID,
                                        Name = a.StatementLetter.StatementLetterName,
                                        LastModifiedBy = a.StatementLetter.UpdateDate == null ? a.StatementLetter.CreateBy : a.StatementLetter.UpdateBy,
                                        LastModifiedDate = a.StatementLetter.UpdateDate == null ? a.StatementLetter.CreateDate : a.StatementLetter.UpdateDate.Value
                                    },
                                    Amount = a.Amount,
                                    AmountUSD = a.AmountUSD,
                                    AttachmentNo = a.AttachmentNo,
                                    IsDeclarationOfException = a.IsDeclarationOfExecption,
                                    StartDate = a.StartDate,
                                    EndDate = a.EndDate,
                                    DateOfUnderlying = a.DateOfUnderlying,
                                    ExpiredDate = a.ExpiredDate,
                                    ReferenceNumber = a.ReferenceNumber,
                                    SupplierName = a.SupplierName,
                                    InvoiceNumber = a.InvoiceNumber,
                                    IsProforma = a.IsProforma,
                                    IsSelectedProforma = a.IsSelectedProforma,
                                    IsBulkUnderlying = a.IsBulkUnderlying == null ? false : a.IsBulkUnderlying,
                                    IsSelectedBulk = a.IsSelectedBulk == null ? false : a.IsSelectedBulk,
                                    IsUtilize = a.IsUtilize,
                                    OtherUnderlying = a.OtherUnderlying,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,
                                    AccountNumber = a.AccountNumber,
                                    IsJointAccount = a.IsJointAccount,
                                    IsTMO = a.IsTMO == null ? false : a.IsTMO
                                });

                    CustomerUnderlyingGrid.Page = page;
                    CustomerUnderlyingGrid.Size = size;
                    CustomerUnderlyingGrid.Total = data.Count();
                    CustomerUnderlyingGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    CustomerUnderlyingGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new CustomerUnderlyingRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            CIF = a.CIF,
                            UnderlyingDocument = a.UnderlyingDocument,
                            DocumentType = a.DocumentType,
                            Currency = a.Currency,
                            Amount = a.Amount,
                            AttachmentNo = a.AttachmentNo,
                            StatementLetter = a.StatementLetter,
                            IsDeclarationOfException = a.IsDeclarationOfException,
                            StartDate = a.StartDate,
                            EndDate = a.EndDate,
                            DateOfUnderlying = a.DateOfUnderlying,
                            ExpiredDate = a.ExpiredDate,
                            ReferenceNumber = a.ReferenceNumber,
                            SupplierName = a.SupplierName,
                            IsProforma = a.IsProforma,
                            IsSelectedProforma = a.IsSelectedProforma,
                            IsBulkUnderlying = a.IsBulkUnderlying,
                            IsSelectedBulk = a.IsSelectedBulk,
                            IsUtilize = a.IsUtilize,
                            InvoiceNumber = a.InvoiceNumber,
                            OtherUnderlying = a.OtherUnderlying,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate,
                            AccountNumber = a.AccountNumber,
                            IsJointAccount = a.IsJointAccount,
                            IsTMO = a.IsTMO
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCustomerUnderlyingAttach(int page, int size, IList<CustomerUnderlyingFilter> filters, string sortColumn, string sortOrder, ref CustomerUnderlyingGrid CustomerUnderlyingGrid, ref string message)
        {
            bool isSuccess = false;
            DateTime currentDate = DateTime.Now;
            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                CustomerUnderlyingModel filter = new CustomerUnderlyingModel();
                if (filters != null)
                {
                    var parentId = filters.Where(a => a.Field.Equals("ParentID")).Select(a => a.Value).SingleOrDefault();
                    bool isTMO_ = filters.Where(a => a.Field.Equals("IsTMO")).Select(a => a == null ? false : true).SingleOrDefault();
                    filter.CIF = (string)filters.Where(a => a.Field.Equals("CIF")).Select(a => a.Value).SingleOrDefault();
                    filter.ParentID = (parentId != null ? long.Parse((string)parentId) : 0);
                    filter.UnderlyingDocument = new UnderlyingDocModel { Name = (string)filters.Where(a => a.Field.Equals("UnderlyingDocument")).Select(a => a.Value).SingleOrDefault() };
                    filter.DocumentType = new DocumentTypeModel { Name = (string)filters.Where(a => a.Field.Equals("DocumentType")).Select(a => a.Value).SingleOrDefault() };
                    filter.Currency = new CurrencyModel { Code = (string)filters.Where(a => a.Field.Equals("Currency")).Select(a => a == null ? "0" : a.Value).SingleOrDefault() };
                    filter.StatementLetter = new StatementLetterModel { Name = (string)filters.Where(a => a.Field.Equals("StatementLetter")).Select(a => a.Value).SingleOrDefault() };
                    filter.Amount = Convert.ToDecimal((string)filters.Where(a => a.Field.Equals("Amount")).Select(a => a == null ? "0" : a.Value).SingleOrDefault());
                    filter.AvailableAmount = Convert.ToDecimal((string)filters.Where(a => a.Field.Equals("AvailableAmount")).Select(a => a == null ? "0" : a.Value).SingleOrDefault());
                    //filter.AttachmentNo = (string)filters.Where(a => a.Field.Equals("AttachmentNo")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("DateOfUnderlying")).Any())
                    {
                        filter.DateOfUnderlying = DateTime.Parse((string)filters.Where(a => a.Field.Equals("DateOfUnderlying")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("ExpiredDate")).Any())
                    {
                        filter.ExpiredDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("ExpiredDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    filter.ReferenceNumber = (string)filters.Where(a => a.Field.Equals("ReferenceNumber")).Select(a => a.Value).SingleOrDefault();
                    filter.SupplierName = (string)filters.Where(a => a.Field.Equals("SupplierName")).Select(a => a.Value).SingleOrDefault();
                    filter.InvoiceNumber = (string)filters.Where(a => a.Field.Equals("InvoiceNumber")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                    filter.AccountNumber = (string)filters.Where(a => a.Field.Equals("AccountNumber")).Select(a => a.Value).SingleOrDefault();
                    string statusData = (string)filters.Where(a => a.Field.Equals("StatusShowData")).Select(a => a.Value).SingleOrDefault();
                    filter.StatusShowData = statusData != null ? int.Parse(statusData) : 0;
                    filter.IsTMO = isTMO_ ? true : false;

                }
                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.CustomerUnderlyings
                                let isCIF = !string.IsNullOrEmpty(filter.CIF)
                                let isTMO = filter.IsTMO.Value ? true : false
                                let isUnderlyingDocument = !string.IsNullOrEmpty(filter.UnderlyingDocument.Name)
                                let isDocumentType = !string.IsNullOrEmpty(filter.DocumentType.Name)
                                let isCurrency = !string.IsNullOrEmpty(filter.Currency.Code)
                                let isStatementLetter = !string.IsNullOrEmpty(filter.StatementLetter.Name)
                                let isAmount = filter.Amount != null && filter.Amount != 0 ? true : false
                                let isAttachmentNo = !string.IsNullOrEmpty(filter.AttachmentNo)
                                let isDateOfUnderlying = filter.DateOfUnderlying.HasValue ? true : false
                                let isExpiredDate = filter.ExpiredDate.HasValue ? true : false
                                let isReferenceNumber = !string.IsNullOrEmpty(filter.ReferenceNumber)
                                let isSupplierName = !string.IsNullOrEmpty(filter.SupplierName)
                                let isInvoiceNumber = filter.InvoiceNumber != null ? true : false
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                let isAccountNumber = filter.AccountNumber != null ? true : false
                                let isStatusShow = filter.StatusShowData != null ? true : false
                                where a.IsDeleted.Equals(false) && a.IsAttachFile.Equals(false) && DbFunctions.TruncateTime(a.ExpiredDate) >= DbFunctions.TruncateTime(currentDate) &&
                                    //(isCIF ? a.CIF.Equals(filter.CIF) : true) &&
                                    (isTMO ? a.IsTMO.Value.Equals(filter.IsTMO.Value) : true) &&
                                    (isStatusShow ? filter.StatusShowData == 0 ? a.CIF.Equals(filter.CIF) || a.AccountNumber.Equals(filter.AccountNumber) : (filter.StatusShowData == 1 ? a.CIF.Equals(filter.CIF) && (a.IsJointAccount.HasValue ? a.IsJointAccount.Value.Equals(false) : true) : filter.StatusShowData == 2 ? a.AccountNumber.Equals(filter.AccountNumber) : true) : true) &&
                                    (isUnderlyingDocument ? a.UnderlyingDocument.UnderlyingDocName.Contains(filter.UnderlyingDocument.Name) : true) &&
                                    (isDocumentType ? a.DocumentType.DocTypeName.Contains(filter.DocumentType.Name) : true) &&
                                    (isCurrency ? a.Currency.CurrencyCode.Contains(filter.Currency.Code) : true) &&
                                    (isStatementLetter ? a.StatementLetter.StatementLetterName.Contains(filter.StatementLetter.Name) : true) &&
                                    (isAmount ? a.Amount.Equals(filter.Amount) : true) &&
                                    //(isAvailableAmount ? a.AvailableAmountUSD.Value.Equals(filter.AvailableAmount) : true) &&
                                    (isAttachmentNo ? a.AttachmentNo.Contains(filter.AttachmentNo) : true) &&
                                    (isDateOfUnderlying ? a.DateOfUnderlying >= filter.DateOfUnderlying && a.DateOfUnderlying <= filter.DateOfUnderlying : true) &&
                                    (isExpiredDate ? a.ExpiredDate >= filter.ExpiredDate && a.ExpiredDate <= filter.ExpiredDate : true) &&
                                    (isReferenceNumber ? a.ReferenceNumber.Contains(filter.ReferenceNumber) : true) &&
                                    (isSupplierName ? a.SupplierName.Contains(filter.SupplierName) : true) &&
                                    (isInvoiceNumber ? a.InvoiceNumber.Contains(filter.InvoiceNumber) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)

                                select new CustomerUnderlyingModel
                                {
                                    ID = a.UnderlyingID,
                                    CIF = a.CIF,
                                    UnderlyingDocument = new UnderlyingDocModel
                                    {
                                        ID = a.UnderlyingDocument.UnderlyingDocID,
                                        Code = a.UnderlyingDocument.UnderlyingDocCode,
                                        Name = a.UnderlyingDocument.UnderlyingDocName,
                                        LastModifiedBy = a.UnderlyingDocument.UpdateDate == null ? a.UnderlyingDocument.CreateBy : a.UnderlyingDocument.UpdateBy,
                                        LastModifiedDate = a.UnderlyingDocument.UpdateDate == null ? a.UnderlyingDocument.CreateDate : a.UnderlyingDocument.UpdateDate.Value
                                    },
                                    DocumentType = new DocumentTypeModel
                                    {
                                        ID = a.DocumentType.DocTypeID,
                                        Name = a.DocumentType.DocTypeName,
                                        Description = a.DocumentType.DocTypeDescription,
                                        LastModifiedBy = a.DocumentType.UpdateDate == null ? a.DocumentType.CreateBy : a.DocumentType.UpdateBy,
                                        LastModifiedDate = a.DocumentType.UpdateDate == null ? a.DocumentType.CreateDate : a.DocumentType.UpdateDate.Value
                                    },
                                    Currency = new CurrencyModel
                                    {
                                        ID = a.Currency.CurrencyID,
                                        Code = a.Currency.CurrencyCode,
                                        LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                        LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                    },
                                    StatementLetter = new StatementLetterModel
                                    {
                                        ID = a.StatementLetter.StatementLetterID,
                                        Name = a.StatementLetter.StatementLetterName,
                                        LastModifiedBy = a.StatementLetter.UpdateDate == null ? a.StatementLetter.CreateBy : a.StatementLetter.UpdateBy,
                                        LastModifiedDate = a.StatementLetter.UpdateDate == null ? a.StatementLetter.CreateDate : a.StatementLetter.UpdateDate.Value
                                    },
                                    Amount = a.Amount,
                                    AttachmentNo = a.AttachmentNo,
                                    IsDeclarationOfException = a.IsDeclarationOfExecption,
                                    StartDate = a.StartDate,
                                    EndDate = a.EndDate,
                                    AvailableAmount = a.AvailableAmountUSD == null ? 0 : a.AvailableAmountUSD,
                                    DateOfUnderlying = a.DateOfUnderlying,
                                    ExpiredDate = a.ExpiredDate,
                                    ReferenceNumber = a.ReferenceNumber,
                                    SupplierName = a.SupplierName,
                                    IsProforma = a.IsProforma,
                                    IsBulkUnderlying = a.IsBulkUnderlying == null ? false : a.IsBulkUnderlying,
                                    IsSelectedBulk = a.IsSelectedBulk == null ? false : a.IsSelectedBulk,
                                    IsSelectedProforma = a.IsSelectedProforma,
                                    InvoiceNumber = a.InvoiceNumber,
                                    IsTMO = a.IsTMO.HasValue ? a.IsTMO.Value : false,
                                    IsSelectedAttach = a.IsAttachFile,
                                    OtherUnderlying = a.OtherUnderlying,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    CustomerUnderlyingGrid.Page = page;
                    CustomerUnderlyingGrid.Size = size;
                    CustomerUnderlyingGrid.Total = data.Count();
                    CustomerUnderlyingGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    CustomerUnderlyingGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new CustomerUnderlyingRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            CIF = a.CIF,
                            UnderlyingDocument = a.UnderlyingDocument,
                            DocumentType = a.DocumentType,
                            Currency = a.Currency,
                            Amount = a.Amount,
                            AvailableAmount = a.AvailableAmount.HasValue ? (decimal?)Decimal.Round(a.AvailableAmount.Value, 2) : null,
                            AttachmentNo = a.AttachmentNo,
                            StatementLetter = a.StatementLetter,
                            IsDeclarationOfException = a.IsDeclarationOfException,
                            StartDate = a.StartDate,
                            EndDate = a.EndDate,
                            DateOfUnderlying = a.DateOfUnderlying,
                            ExpiredDate = a.ExpiredDate,
                            ReferenceNumber = a.ReferenceNumber,
                            SupplierName = a.SupplierName,
                            IsSelectedProforma = a.IsSelectedProforma,
                            InvoiceNumber = a.InvoiceNumber,
                            IsProforma = a.IsProforma,
                            IsSelectedBulk = a.IsSelectedBulk,
                            IsTMO = a.IsTMO,
                            IsBulkUnderlying = a.IsBulkUnderlying,
                            OtherUnderlying = a.OtherUnderlying,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        /* public bool GetCustomerUnderlying(int page, int size, IList<CustomerUnderlyingFilter> filters, string sortColumn, string sortOrder, ref CustomerUnderlyingGrid CustomerUnderlyingGrid, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                CustomerUnderlyingModel filter = new CustomerUnderlyingModel();
                if (filters!=null )
                {
                    var parentId = filters.Where(a => a.Field.Equals("ParentID")).Select(a => a.Value).SingleOrDefault(); 
                    filter.ParentID = (parentId != null ? long.Parse((string)parentId) : 0);
                    if (filters.Where(a => a.Field.Equals("IsSelected")).Any())
                    {
                        filter.IsSelected = bool.Parse((string)filters.Where(a => a.Field.Equals("IsSelected")).Select(a => a.Value).SingleOrDefault());
                    }
                    filter.UnderlyingDocument = new UnderlyingDocModel { Name = (string)filters.Where(a => a.Field.Equals("UnderlyingDocument")).Select(a => a.Value).SingleOrDefault() };
                    filter.DocumentType = new DocumentTypeModel { DocTypeName = (string)filters.Where(a => a.Field.Equals("DocumentType")).Select(a => a.Value).SingleOrDefault() };
                    filter.Currency = new CurrencyModel { Code = (string)filters.Where(a => a.Field.Equals("Currency")).Select(a => a == null ? "0" : a.Value).SingleOrDefault() };
                    filter.StatementLetter = new StatementLetterModel { Name = (string)filters.Where(a => a.Field.Equals("StatementLetter")).Select(a => a.Value).SingleOrDefault() };
                    filter.Amount = Convert.ToDecimal((string)filters.Where(a => a.Field.Equals("Amount")).Select(a => a == null ? "0" : a.Value).SingleOrDefault());
                    filter.AttachmentNo = (string)filters.Where(a => a.Field.Equals("AttachmentNo")).Select(a => a.Value).SingleOrDefault();
                    if(filters.Where(a => a.Field.Equals("StartDate")).Any()){
                    filter.StartDate =  DateTime.Parse((string)filters.Where(a => a.Field.Equals("StartDate")).Select(a => a.Value).SingleOrDefault()); 
                    }
                    if(filters.Where(a => a.Field.Equals("EndDate")).Any()){
                        filter.EndDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("EndDate")).Select(a => a.Value).SingleOrDefault());                     
                        }
                    if(filters.Where(a => a.Field.Equals("DateOfUnderlying")).Any()){
                        filter.DateOfUnderlying = DateTime.Parse((string)filters.Where(a => a.Field.Equals("DateOfUnderlying")).Select(a => a.Value).SingleOrDefault()); 
                        }
                    if (filters.Where(a => a.Field.Equals("ExpiredDate")).Any())
                    {
                        filter.ExpiredDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("ExpiredDate")).Select(a => a.Value).SingleOrDefault()); 
                        }
                    filter.ReferenceNumber = (string)filters.Where(a => a.Field.Equals("ReferenceNumber")).Select(a => a.Value).SingleOrDefault();
                    filter.SupplierName =(string)filters.Where(a => a.Field.Equals("SupplierName")).Select(a => a.Value).SingleOrDefault();
                    filter.InvoiceNumber = (string)filters.Where(a => a.Field.Equals("InvoiceNumber")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }
                if (filter.IsSelected) // new data
                {
                    NewSelected(page, size, sortColumn, sortOrder, CustomerUnderlyingGrid, skip, orderBy, maxDate, filter);
                }
                else {
                    EditSelected(page, size, sortColumn, sortOrder, CustomerUnderlyingGrid, skip, orderBy, maxDate, filter);
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        } */
        /*
        private static void NewSelected(int page, int size, string sortColumn, string sortOrder, CustomerUnderlyingGrid CustomerUnderlyingGrid, int skip, string orderBy, DateTime maxDate, CustomerUnderlyingModel filter)
        {
            using (DBSEntities context = new DBSEntities())
            {
                var data = (from a in context.CustomerUnderlyings
                            let isCIF = !string.IsNullOrEmpty(filter.CIF)
                            let isUnderlyingDocument = !string.IsNullOrEmpty(filter.UnderlyingDocument.Name)
                            let isSelected = !filter.IsSelected
                            let isDocumentType = !string.IsNullOrEmpty(filter.DocumentType.DocTypeName)
                            let isCurrency = !string.IsNullOrEmpty(filter.Currency.Code)
                            let isStatementLetter = !string.IsNullOrEmpty(filter.StatementLetter.Name)
                            let isAmount = filter.Amount != null && filter.Amount != 0 ? true : false
                            let isAttachmentNo = !string.IsNullOrEmpty(filter.AttachmentNo)
                            let isStartDate = filter.StartDate.HasValue ? true : false
                            let isEndDate = filter.EndDate.HasValue ? true : false
                            let isDateOfUnderlying = filter.DateOfUnderlying.HasValue ? true : false
                            let isExpiredDate = filter.ExpiredDate.HasValue ? true : false
                            let isReferenceNumber = !string.IsNullOrEmpty(filter.ReferenceNumber)
                            let isSupplierName = !string.IsNullOrEmpty(filter.SupplierName)
                            let isInvoiceNumber = filter.InvoiceNumber != null ? true : false
                            let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                            let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                            where a.IsDeleted.Equals(false) &&
                                (isCIF ? a.CIF.Contains(filter.CIF) : true) &&
                                (isSelected ? a.IsSelected.Equals(false) : true) &&
                                (isUnderlyingDocument ? a.UnderlyingDocument.UnderlyingDocName.Contains(filter.UnderlyingDocument.Name) : true) &&
                                (isDocumentType ? a.DocumentType.DocTypeName.Contains(filter.DocumentType.DocTypeName) : true) &&
                                (isCurrency ? a.Currency.CurrencyCode.Contains(filter.Currency.Code) : true) &&
                                (isStatementLetter ? a.StatementLetter.StatementLetterName.Contains(filter.StatementLetter.Name) : true) &&
                                (isAmount ? a.Amount.Equals(filter.Amount) : true) &&
                                (isAttachmentNo ? a.AttachmentNo.Contains(filter.AttachmentNo) : true) &&
                                (isStartDate ? a.StartDate.Equals(filter.StartDate) : true) &&
                                (isEndDate ? a.EndDate.Equals(filter.EndDate) : true) &&
                                (isDateOfUnderlying ? a.DateOfUnderlying.Equals(filter.DateOfUnderlying) : true) &&
                                (isExpiredDate ? a.ExpiredDate.Equals(filter.ExpiredDate) : true) &&
                                (isReferenceNumber ? a.ReferenceNumber.Contains(filter.ReferenceNumber) : true) &&
                                (isSupplierName ? a.SupplierName.Contains(filter.SupplierName) : true) &&
                                (isInvoiceNumber ? a.InvoiceNumber.Contains(filter.InvoiceNumber) : true) &&
                                (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)

                            select new CustomerUnderlyingModel
                            {
                                ID = a.UnderlyingID,
                                CIF = a.CIF,
                                UnderlyingDocument = new UnderlyingDocModel
                                {
                                    ID = a.UnderlyingDocument.UnderlyingDocID,
                                    Code = a.UnderlyingDocument.UnderlyingDocCode,
                                    Name = a.UnderlyingDocument.UnderlyingDocName,
                                    LastModifiedBy = a.UnderlyingDocument.UpdateDate == null ? a.UnderlyingDocument.CreateBy : a.UnderlyingDocument.UpdateBy,
                                    LastModifiedDate = a.UnderlyingDocument.UpdateDate == null ? a.UnderlyingDocument.CreateDate : a.UnderlyingDocument.UpdateDate.Value
                                },
                                DocumentType = new DocumentTypeModel
                                {
                                    ID = a.DocumentType.DocTypeID,
                                    DocTypeName = a.DocumentType.DocTypeName,
                                    DocTypeDescription = a.DocumentType.DocTypeDescription,
                                    LastModifiedBy = a.DocumentType.UpdateDate == null ? a.DocumentType.CreateBy : a.DocumentType.UpdateBy,
                                    LastModifiedDate = a.DocumentType.UpdateDate == null ? a.DocumentType.CreateDate : a.DocumentType.UpdateDate.Value
                                },
                                Currency = new CurrencyModel
                                {
                                    ID = a.Currency.CurrencyID,
                                    Code = a.Currency.CurrencyCode,
                                    LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                    LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                },
                                StatementLetter = new StatementLetterModel
                                {
                                    ID = a.StatementLetter.StatementLetterID,
                                    Name = a.StatementLetter.StatementLetterName,
                                    LastModifiedBy = a.StatementLetter.UpdateDate == null ? a.StatementLetter.CreateBy : a.StatementLetter.UpdateBy,
                                    LastModifiedDate = a.StatementLetter.UpdateDate == null ? a.StatementLetter.CreateDate : a.StatementLetter.UpdateDate.Value
                                },
                                Amount = a.Amount,
                                AttachmentNo = a.AttachmentNo,
                                IsDeclarationOfException = a.IsDeclarationOfExecption,
                                StartDate = a.StartDate,
                                EndDate = a.EndDate,
                                DateOfUnderlying = a.DateOfUnderlying,
                                ExpiredDate = a.ExpiredDate,
                                ReferenceNumber = a.ReferenceNumber,
                                SupplierName = a.SupplierName,
                                IsProforma = a.IsProforma,
                                InvoiceNumber = a.InvoiceNumber,
                                IsSelected = a.IsSelected,
                                OtherUnderlying = a.OtherUnderlying,
                                LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                            });

                CustomerUnderlyingGrid.Page = page;
                CustomerUnderlyingGrid.Size = size;
                CustomerUnderlyingGrid.Total = data.Count();
                CustomerUnderlyingGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                CustomerUnderlyingGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                    .Select((a, i) => new CustomerUnderlyingRow
                    {
                        RowID = i + 1,
                        ID = a.ID,
                        CIF = a.CIF,
                        UnderlyingDocument = a.UnderlyingDocument,
                        DocumentType = a.DocumentType,
                        Currency = a.Currency,
                        Amount = a.Amount,
                        AttachmentNo = a.AttachmentNo,
                        StatementLetter = a.StatementLetter,
                        IsDeclarationOfException = a.IsDeclarationOfException,
                        StartDate = a.StartDate,
                        EndDate = a.EndDate,
                        DateOfUnderlying = a.DateOfUnderlying,
                        ExpiredDate = a.ExpiredDate,
                        ReferenceNumber = a.ReferenceNumber,
                        SupplierName = a.SupplierName,
                        IsProforma = a.IsProforma,
                        InvoiceNumber = a.InvoiceNumber,
                        IsSelected = a.IsSelected,
                        OtherUnderlying = a.OtherUnderlying,
                        LastModifiedBy = a.LastModifiedBy,
                        LastModifiedDate = a.LastModifiedDate
                    })
                    .Skip(skip)
                    .Take(size)
                    .ToList();
            }
        }

        private static void EditSelected(int page, int size, string sortColumn, string sortOrder, CustomerUnderlyingGrid CustomerUnderlyingGrid, int skip, string orderBy, DateTime maxDate, CustomerUnderlyingModel filter)
        {
            using (DBSEntities context = new DBSEntities())
            {
                var data = (from a in context.CustomerUnderlyings
                            join b in context.MappingUnderlyings.Where(z=>z.IsDeleted.Equals(false)) on a.UnderlyingID equals b.UnderlyingID into temp
                            from x in temp.DefaultIfEmpty()
                            let isCIF = !string.IsNullOrEmpty(filter.CIF)
                            let isUnderlyingDocument = !string.IsNullOrEmpty(filter.UnderlyingDocument.Name)
                            let isCurrency = !string.IsNullOrEmpty(filter.Currency.Code)
                            let isStatementLetter = !string.IsNullOrEmpty(filter.StatementLetter.Name)                            
                            let isAmount = filter.Amount != null && filter.Amount != 0 ? true : false
                            let isAttachmentNo = !string.IsNullOrEmpty(filter.AttachmentNo)
                            let isDateOfUnderlying = filter.DateOfUnderlying.HasValue ? true : false
                            let isExpiredDate = filter.ExpiredDate.HasValue ? true : false
                            let isReferenceNumber = !string.IsNullOrEmpty(filter.ReferenceNumber)
                            let isSupplierName = !string.IsNullOrEmpty(filter.SupplierName)
                            where (a.IsSelected.Equals(false) || x.ParentID.Equals(filter.ParentID)) &&
                                a.DocumentType.DocTypeName.Contains(filter.DocumentType.DocTypeName) && a.UnderlyingID != filter.ParentID                                
                                && (isCurrency ? a.Currency.CurrencyCode.Contains(filter.Currency.Code) : true)
                                   && (isUnderlyingDocument ? a.UnderlyingDocument.UnderlyingDocName.Contains(filter.UnderlyingDocument.Name) : true)
                                   && (isStatementLetter ? a.StatementLetter.StatementLetterName.Contains(filter.StatementLetter.Name) : true)
                                   && (isAmount ? a.Amount.Equals(filter.Amount) : true)
                                   && (isAttachmentNo ? a.AttachmentNo.Contains(filter.AttachmentNo) : true)
                                   && (isReferenceNumber ? a.ReferenceNumber.Contains(filter.ReferenceNumber) : true)
                                   && (isSupplierName ? a.SupplierName.Contains(filter.SupplierName) : true)
                                   && (isDateOfUnderlying ? a.DateOfUnderlying.Equals(filter.DateOfUnderlying) : true)
                            select new CustomerUnderlyingModel
                            {
                                ID = a.UnderlyingID,
                                CIF = a.CIF,
                                UnderlyingDocument = new UnderlyingDocModel
                                {
                                    ID = a.UnderlyingDocument.UnderlyingDocID,
                                    Code = a.UnderlyingDocument.UnderlyingDocCode,
                                    Name = a.UnderlyingDocument.UnderlyingDocName,
                                    LastModifiedBy = a.UnderlyingDocument.UpdateDate == null ? a.UnderlyingDocument.CreateBy : a.UnderlyingDocument.UpdateBy,
                                    LastModifiedDate = a.UnderlyingDocument.UpdateDate == null ? a.UnderlyingDocument.CreateDate : a.UnderlyingDocument.UpdateDate.Value
                                },
                                DocumentType = new DocumentTypeModel
                                {
                                    ID = a.DocumentType.DocTypeID,
                                    DocTypeName = a.DocumentType.DocTypeName,
                                    DocTypeDescription = a.DocumentType.DocTypeDescription,
                                    LastModifiedBy = a.DocumentType.UpdateDate == null ? a.DocumentType.CreateBy : a.DocumentType.UpdateBy,
                                    LastModifiedDate = a.DocumentType.UpdateDate == null ? a.DocumentType.CreateDate : a.DocumentType.UpdateDate.Value
                                },
                                Currency = new CurrencyModel
                                {
                                    ID = a.Currency.CurrencyID,
                                    Code = a.Currency.CurrencyCode,
                                    LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                    LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                },
                                StatementLetter = new StatementLetterModel
                                {
                                    ID = a.StatementLetter.StatementLetterID,
                                    Name = a.StatementLetter.StatementLetterName,
                                    LastModifiedBy = a.StatementLetter.UpdateDate == null ? a.StatementLetter.CreateBy : a.StatementLetter.UpdateBy,
                                    LastModifiedDate = a.StatementLetter.UpdateDate == null ? a.StatementLetter.CreateDate : a.StatementLetter.UpdateDate.Value
                                },
                                Amount = a.Amount,
                                AttachmentNo = a.AttachmentNo,
                                IsDeclarationOfException = a.IsDeclarationOfExecption,
                                StartDate = a.StartDate,
                                EndDate = a.EndDate,
                                DateOfUnderlying = a.DateOfUnderlying,
                                ExpiredDate = a.ExpiredDate,
                                ReferenceNumber = a.ReferenceNumber,
                                SupplierName = a.SupplierName,
                                IsProforma = a.IsProforma,
                                InvoiceNumber = a.InvoiceNumber,
                                IsSelected = a.IsSelected,
                                OtherUnderlying = a.OtherUnderlying,
                                LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                            });

                CustomerUnderlyingGrid.Page = page;
                CustomerUnderlyingGrid.Size = size;
                CustomerUnderlyingGrid.Total = data.Count();
                CustomerUnderlyingGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                CustomerUnderlyingGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                    .Select((a, i) => new CustomerUnderlyingRow
                    {
                        RowID = i + 1,
                        ID = a.ID,
                        CIF = a.CIF,
                        UnderlyingDocument = a.UnderlyingDocument,
                        DocumentType = a.DocumentType,
                        Currency = a.Currency,
                        Amount = a.Amount,
                        AttachmentNo = a.AttachmentNo,
                        StatementLetter = a.StatementLetter,
                        IsDeclarationOfException = a.IsDeclarationOfException,
                        StartDate = a.StartDate,
                        EndDate = a.EndDate,
                        DateOfUnderlying = a.DateOfUnderlying,
                        ExpiredDate = a.ExpiredDate,
                        ReferenceNumber = a.ReferenceNumber,
                        SupplierName = a.SupplierName,
                        IsProforma = a.IsProforma,
                        InvoiceNumber = a.InvoiceNumber,
                        IsSelected = a.IsSelected,
                        OtherUnderlying = a.OtherUnderlying,
                        LastModifiedBy = a.LastModifiedBy,
                        LastModifiedDate = a.LastModifiedDate
                    })
                    .Skip(skip)
                    .Take(size)
                    .ToList();
            }
        } */


        public bool GetCustomerUnderlying(CustomerUnderlyingFilter filter, ref IList<CustomerUnderlyingModel> CustomerUnderlyings, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetCustomerUnderlying(string key, int limit, ref IList<CustomerUnderlyingModel> CustomerUnderlyings, ref string message)
        {
            bool isSuccess = false;

            try
            {
                CustomerUnderlyings = (from a in context.CustomerUnderlyings
                                       where a.IsDeleted.Equals(false)
                                       select new CustomerUnderlyingModel
                                       {
                                           CIF = a.CIF,
                                           UnderlyingDocument = new UnderlyingDocModel
                                           {
                                               ID = a.UnderlyingDocument.UnderlyingDocID,
                                               Code = a.UnderlyingDocument.UnderlyingDocCode,
                                               Name = a.UnderlyingDocument.UnderlyingDocName,
                                               LastModifiedBy = a.UnderlyingDocument.UpdateDate == null ? a.UnderlyingDocument.CreateBy : a.UnderlyingDocument.UpdateBy,
                                               LastModifiedDate = a.UnderlyingDocument.UpdateDate == null ? a.UnderlyingDocument.CreateDate : a.UnderlyingDocument.UpdateDate.Value
                                           },
                                           DocumentType = new DocumentTypeModel
                                           {
                                               ID = a.DocumentType.DocTypeID,
                                               Name = a.DocumentType.DocTypeName,
                                               Description = a.DocumentType.DocTypeDescription,
                                               LastModifiedBy = a.DocumentType.UpdateDate == null ? a.DocumentType.CreateBy : a.DocumentType.UpdateBy,
                                               LastModifiedDate = a.DocumentType.UpdateDate == null ? a.DocumentType.CreateDate : a.DocumentType.UpdateDate.Value
                                           },
                                           Currency = new CurrencyModel
                                           {
                                               ID = a.Currency.CurrencyID,
                                               Code = a.Currency.CurrencyCode,
                                               LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                               LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                           },
                                           StatementLetter = new StatementLetterModel
                                           {
                                               ID = a.StatementLetter.StatementLetterID,
                                               Name = a.StatementLetter.StatementLetterName,
                                               LastModifiedBy = a.StatementLetter.UpdateDate == null ? a.StatementLetter.CreateBy : a.StatementLetter.UpdateBy,
                                               LastModifiedDate = a.StatementLetter.UpdateDate == null ? a.StatementLetter.CreateDate : a.StatementLetter.UpdateDate.Value
                                           },
                                           Amount = a.Amount,
                                           AvailableAmount = a.AvailableAmountUSD == null ? 0 : a.AvailableAmountUSD,
                                           AttachmentNo = a.AttachmentNo,
                                           IsDeclarationOfException = a.IsDeclarationOfExecption,
                                           StartDate = a.StartDate,
                                           EndDate = a.EndDate,
                                           DateOfUnderlying = a.DateOfUnderlying,
                                           ExpiredDate = a.ExpiredDate,
                                           ReferenceNumber = a.ReferenceNumber,
                                           SupplierName = a.SupplierName,
                                           IsProforma = a.IsProforma,
                                           IsSelectedProforma = a.IsSelectedProforma,
                                           IsSelectedBulk = a.IsSelectedBulk == null ? false : a.IsSelectedBulk,
                                           IsBulkUnderlying = a.IsBulkUnderlying == null ? false : a.IsBulkUnderlying,
                                           IsUtilize = a.IsUtilize,
                                           InvoiceNumber = a.InvoiceNumber,
                                           OtherUnderlying = a.OtherUnderlying,
                                           LastModifiedDate = a.CreateDate,
                                           LastModifiedBy = a.CreateBy
                                       }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }


        public bool AddCustomerUnderlying(CustomerUnderlyingModel customerUnderlying, ref string message)
        {
            bool isSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    //Add validation double underlying : 2015-04-xx
                    //Changed validation double underlying (exclude statement A) : 2015-05-19 
                    if (customerUnderlying.StatementLetter.ID == 1 || !context.SP_IsDoubleUnderlying(customerUnderlying.CIF,
                                                                                                     customerUnderlying.UnderlyingDocument.ID,
                                                                                                     customerUnderlying.Currency.ID,
                                                                                                     customerUnderlying.Amount,
                                                                                                     customerUnderlying.InvoiceNumber, null).Any())
                    {
                        Entity.CustomerUnderlying result = new Entity.CustomerUnderlying()
                        {

                            CIF = customerUnderlying.CIF,
                            UnderlyingDocID = customerUnderlying.UnderlyingDocument.ID,
                            DocTypeID = customerUnderlying.DocumentType.ID,
                            CurrencyID = customerUnderlying.Currency.ID,
                            Amount = customerUnderlying.Amount,
                            AttachmentNo = "",
                            AmountUSD = customerUnderlying.AmountUSD,
                            AvailableAmountUSD = customerUnderlying.AmountUSD,
                            Rate = customerUnderlying.Rate,
                            StatementLetterID = customerUnderlying.StatementLetter.ID,
                            IsDeclarationOfExecption = customerUnderlying.IsDeclarationOfException,
                            StartDate = customerUnderlying.StartDate,
                            EndDate = customerUnderlying.EndDate,
                            DateOfUnderlying = customerUnderlying.DateOfUnderlying.Value,
                            ExpiredDate = customerUnderlying.ExpiredDate.Value,
                            ReferenceNumber = customerUnderlying.ReferenceNumber,
                            SupplierName = customerUnderlying.SupplierName,
                            InvoiceNumber = customerUnderlying.InvoiceNumber,
                            IsProforma = customerUnderlying.IsProforma,
                            IsSelectedProforma = customerUnderlying.IsSelectedProforma,
                            IsBulkUnderlying = (customerUnderlying.IsBulkUnderlying.Value ? (customerUnderlying.BulkUnderlyings.Count > 0 ? true : false) : false), //saving bulks
                            IsSelectedBulk = customerUnderlying.IsSelectedBulk.HasValue ? customerUnderlying.IsSelectedBulk.Value : false,
                            IsUtilize = customerUnderlying.IsUtilize,
                            OtherUnderlying = customerUnderlying.OtherUnderlying,
                            IsJointAccount = customerUnderlying.IsJointAccount.HasValue ? customerUnderlying.IsJointAccount.Value : false,
                            IsTMO = customerUnderlying.IsTMO.HasValue ? customerUnderlying.IsTMO.Value : false,
                            AccountNumber = customerUnderlying.AccountNumber,
                            IsDeleted = false,
                            CreateDate = DateTime.UtcNow,
                            CreateBy = currentUser.GetCurrentUser().DisplayName

                        };
                        context.CustomerUnderlyings.Add(result);
                        context.SaveChanges();

                        long ParentID = result.UnderlyingID;
                        foreach (MappingUnderlyingModel item in customerUnderlying.Proformas)
                        {

                            if (!context.MappingUnderlyings.Where(a => item.ParentID.Equals(ParentID) && item.UnderlyingID.Equals(a.UnderlyingID)).Any())
                            {
                                context.MappingUnderlyings.Add(new Entity.MappingUnderlying()
                                {
                                    ParentID = ParentID,
                                    UnderlyingID = item.UnderlyingID,
                                    CIF = item.CIF,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().DisplayName
                                });
                                context.SaveChanges();

                                // update selected checkbox
                                Entity.CustomerUnderlying dataHeader = context.CustomerUnderlyings.Where(a => a.UnderlyingID.Equals(item.UnderlyingID) && a.IsDeleted.Equals(false)).SingleOrDefault();
                                if (dataHeader != null)
                                {
                                    dataHeader.IsSelectedProforma = true;
                                    dataHeader.UpdateDate = DateTime.UtcNow;
                                    dataHeader.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                                    context.SaveChanges();
                                }
                            }

                        }

                        foreach (MappingBulkUnderlying item in customerUnderlying.BulkUnderlyings)
                        {

                            if (!context.MappingBulkUnderlyings.Where(a => item.ParentID.Equals(ParentID) && item.UnderlyingID.Equals(a.UnderlyingID)).Any())
                            {
                                context.MappingBulkUnderlyings.Add(new Entity.MappingBulkUnderlying()
                                {
                                    ParentID = ParentID,
                                    UnderlyingID = item.UnderlyingID,
                                    CIF = item.CIF,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().DisplayName
                                });
                                context.SaveChanges();

                                // update selected checkbox
                                Entity.CustomerUnderlying dataHeader = context.CustomerUnderlyings.Where(a => a.UnderlyingID.Equals(item.UnderlyingID) && a.IsDeleted.Equals(false)).SingleOrDefault();
                                if (dataHeader != null)
                                {
                                    dataHeader.IsSelectedBulk = true;
                                    dataHeader.UpdateDate = DateTime.UtcNow;
                                    dataHeader.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                                    context.SaveChanges();
                                }
                            }
                        }
                        isSuccess = true;

                    }
                    else
                    {
                        message = string.Format("Double Underlying, Customer Underlying data for Currency {1}, Amount {2} and Invoice Number {3} is already exist.", customerUnderlying.UnderlyingDocument.Name, customerUnderlying.Currency.Code, customerUnderlying.Amount, customerUnderlying.InvoiceNumber);
                    }
                    ts.Complete();
                }
                catch (Exception ex)
                {
                    ts.Dispose();
                    message = ex.Message;
                }
                return isSuccess;
            }
        }
        public bool AddCustomerUnderlying(CustomerUnderlyingModel customerUnderlying, ref SelectUtillizeModel SelectUtilize, ref string message)
        {
            bool isSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    //Add validation double underlying : 2015-04-xx
                    //Changed validation double underlying (exclude statement A) : 2015-05-19 
                    if (customerUnderlying.StatementLetter.ID == 1 || !context.SP_IsDoubleUnderlying(customerUnderlying.CIF,
                                                                                                     customerUnderlying.UnderlyingDocument.ID,
                                                                                                     customerUnderlying.Currency.ID,
                                                                                                     customerUnderlying.Amount,
                                                                                                     customerUnderlying.InvoiceNumber, null).Any())
                    {
                        Entity.CustomerUnderlying result = new Entity.CustomerUnderlying()
                        {
                            CIF = customerUnderlying.CIF,
                            UnderlyingDocID = customerUnderlying.UnderlyingDocument.ID,
                            DocTypeID = customerUnderlying.DocumentType.ID,
                            CurrencyID = customerUnderlying.Currency.ID,
                            Amount = customerUnderlying.Amount,
                            AttachmentNo = "",
                            AmountUSD = customerUnderlying.AmountUSD,
                            AvailableAmountUSD = customerUnderlying.AmountUSD,
                            Rate = customerUnderlying.Rate,
                            StatementLetterID = customerUnderlying.StatementLetter.ID,
                            IsDeclarationOfExecption = customerUnderlying.IsDeclarationOfException,
                            StartDate = customerUnderlying.StartDate,
                            EndDate = customerUnderlying.EndDate,
                            DateOfUnderlying = customerUnderlying.DateOfUnderlying.Value,
                            ExpiredDate = customerUnderlying.ExpiredDate.Value,
                            ReferenceNumber = customerUnderlying.ReferenceNumber,
                            SupplierName = customerUnderlying.SupplierName,
                            InvoiceNumber = customerUnderlying.InvoiceNumber,
                            IsProforma = customerUnderlying.IsProforma,
                            IsSelectedProforma = customerUnderlying.IsSelectedProforma,
                            IsBulkUnderlying = (customerUnderlying.IsBulkUnderlying.Value ? (customerUnderlying.BulkUnderlyings.Count > 0 ? true : false) : false), //saving bulks
                            IsSelectedBulk = customerUnderlying.IsSelectedBulk.HasValue ? customerUnderlying.IsSelectedBulk.Value : false,
                            IsUtilize = customerUnderlying.IsUtilize,
                            OtherUnderlying = customerUnderlying.OtherUnderlying,
                            IsJointAccount = customerUnderlying.IsJointAccount.HasValue ? customerUnderlying.IsJointAccount.Value : false,
                            IsTMO = customerUnderlying.IsTMO.HasValue ? customerUnderlying.IsTMO.Value : false,
                            AccountNumber = customerUnderlying.AccountNumber,
                            IsDeleted = false,
                            CreateDate = DateTime.UtcNow,
                            CreateBy = currentUser.GetCurrentUser().DisplayName

                        };
                        context.CustomerUnderlyings.Add(result);
                        context.SaveChanges();

                        SelectUtilize = new SelectUtillizeModel
                        {
                            UnderlyingID = result.UnderlyingID,
                            IsEnable = true,
                            AvailableAmount = customerUnderlying.AmountUSD.Value,
                            StatementLetterID = customerUnderlying.StatementLetter.ID
                        };

                        long ParentID = result.UnderlyingID;
                        foreach (MappingUnderlyingModel item in customerUnderlying.Proformas)
                        {

                            if (!context.MappingUnderlyings.Where(a => item.ParentID.Equals(ParentID) && item.UnderlyingID.Equals(a.UnderlyingID)).Any())
                            {
                                context.MappingUnderlyings.Add(new Entity.MappingUnderlying()
                                {
                                    ParentID = ParentID,
                                    UnderlyingID = item.UnderlyingID,
                                    CIF = item.CIF,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().DisplayName
                                });
                                context.SaveChanges();

                                // update selected checkbox
                                Entity.CustomerUnderlying dataHeader = context.CustomerUnderlyings.Where(a => a.UnderlyingID.Equals(item.UnderlyingID) && a.IsDeleted.Equals(false)).SingleOrDefault();
                                if (dataHeader != null)
                                {
                                    dataHeader.IsSelectedProforma = true;
                                    dataHeader.UpdateDate = DateTime.UtcNow;
                                    dataHeader.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                                    context.SaveChanges();
                                }
                            }

                        }

                        foreach (MappingBulkUnderlying item in customerUnderlying.BulkUnderlyings)
                        {

                            if (!context.MappingBulkUnderlyings.Where(a => item.ParentID.Equals(ParentID) && item.UnderlyingID.Equals(a.UnderlyingID)).Any())
                            {
                                context.MappingBulkUnderlyings.Add(new Entity.MappingBulkUnderlying()
                                {
                                    ParentID = ParentID,
                                    UnderlyingID = item.UnderlyingID,
                                    CIF = item.CIF,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().DisplayName
                                });
                                context.SaveChanges();

                                // update selected checkbox
                                Entity.CustomerUnderlying dataHeader = context.CustomerUnderlyings.Where(a => a.UnderlyingID.Equals(item.UnderlyingID) && a.IsDeleted.Equals(false)).SingleOrDefault();
                                if (dataHeader != null)
                                {
                                    dataHeader.IsSelectedBulk = true;
                                    dataHeader.UpdateDate = DateTime.UtcNow;
                                    dataHeader.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                                    context.SaveChanges();
                                }
                            }
                        }
                        isSuccess = true;

                    }
                    else
                    {
                        message = string.Format("Double Underlying, Customer Underlying data for Currency {1}, Amount {2} and Invoice Number {3} is already exist.", customerUnderlying.UnderlyingDocument.Name, customerUnderlying.Currency.Code, customerUnderlying.Amount, customerUnderlying.InvoiceNumber);
                    }
                    ts.Complete();
                }
                catch (Exception ex)
                {
                    ts.Dispose();
                    message = ex.Message;
                }
                return isSuccess;
            }
        }

        private string SkipSimbol(string value)
        {
            Regex rgx = new Regex("[^a-zA-Z0-9 -]");
            value = rgx.Replace(value, "");
            return value;
        }

        public bool DeleteCustomerUnderlying(long id, ref string message)
        {
            bool isSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    SetProformaCustomerUnderlying(id);
                    SetBulkCustomerUnderlying(id);
                    Entity.CustomerUnderlying data = context.CustomerUnderlyings.Where(a => a.UnderlyingID.Equals(id)).SingleOrDefault();

                    if (data != null)
                    {
                        data.IsDeleted = true;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        context.SaveChanges();

                        var detailData = context.MappingUnderlyings.Where(a => a.ParentID.Equals(id)).ToList();

                        detailData.ForEach(a =>
                        {
                            a.IsDeleted = true;
                            a.UpdateDate = DateTime.UtcNow;
                            a.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                        });
                        context.SaveChanges();
                        ts.Complete();
                        isSuccess = true;
                    }
                    else
                    {
                        message = string.Format("Customer Underlying data for Customer Underlying ID {0} is does not exist.", id);
                    }
                }
                catch (Exception ex)
                {
                    ts.Dispose();
                    message = ex.Message;
                }
            }
            return isSuccess;
        }

        #region Draft Add
        public bool AddCustomerUnderlyingDraft(CustomerUnderlyingModel customerUnderlyingdraft, ref long UnderlyingDraftID, ref string message)
        {
            bool isSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    if (customerUnderlyingdraft.StatementLetter.ID == 1 || !context.SP_IsDoubleUnderlying(customerUnderlyingdraft.CIF,
                                                                                                     customerUnderlyingdraft.UnderlyingDocument.ID,
                                                                                                     customerUnderlyingdraft.Currency.ID,
                                                                                                     customerUnderlyingdraft.Amount,
                                                                                                     customerUnderlyingdraft.InvoiceNumber, null).Any())
                    {
                        Entity.CustomerUnderlyingDraft result = new Entity.CustomerUnderlyingDraft()
                        {

                            CIF = customerUnderlyingdraft.CIF,
                            MasterUnderlyingID = 0,
                            UnderlyingDocID = customerUnderlyingdraft.UnderlyingDocument.ID,
                            DocTypeID = customerUnderlyingdraft.DocumentType.ID,
                            CurrencyID = customerUnderlyingdraft.Currency.ID,
                            Amount = customerUnderlyingdraft.Amount,
                            AttachmentNo = "",
                            AmountUSD = customerUnderlyingdraft.AmountUSD,
                            AvailableAmountUSD = customerUnderlyingdraft.AmountUSD,
                            Rate = customerUnderlyingdraft.Rate,
                            StatementLetterID = customerUnderlyingdraft.StatementLetter.ID,
                            IsDeclarationOfExecption = customerUnderlyingdraft.IsDeclarationOfException,
                            StartDate = customerUnderlyingdraft.StartDate,
                            EndDate = customerUnderlyingdraft.EndDate,
                            DateOfUnderlying = customerUnderlyingdraft.DateOfUnderlying.Value,
                            ExpiredDate = customerUnderlyingdraft.ExpiredDate.Value,
                            ReferenceNumber = customerUnderlyingdraft.ReferenceNumber,
                            SupplierName = customerUnderlyingdraft.SupplierName,
                            InvoiceNumber = customerUnderlyingdraft.InvoiceNumber,
                            IsProforma = customerUnderlyingdraft.IsProforma,
                            IsSelectedProforma = customerUnderlyingdraft.IsSelectedProforma,
                            IsBulkUnderlying = (customerUnderlyingdraft.IsBulkUnderlying.Value ? (customerUnderlyingdraft.BulkUnderlyings.Count > 0 ? true : false) : false), //saving bulks
                            IsSelectedBulk = customerUnderlyingdraft.IsSelectedBulk.HasValue ? customerUnderlyingdraft.IsSelectedBulk.Value : false,
                            IsJointAccount = customerUnderlyingdraft.IsJointAccount.HasValue ? customerUnderlyingdraft.IsJointAccount.Value : false,
                            IsTMO = customerUnderlyingdraft.IsTMO.HasValue ? customerUnderlyingdraft.IsTMO.Value : false,
                            AccountNumber = customerUnderlyingdraft.AccountNumber,
                            //IsUtilize = customerUnderlying.IsUtilize,
                            OtherUnderlying = customerUnderlyingdraft.OtherUnderlying,

                            IsDeleted = false,
                            CreateDate = DateTime.UtcNow,
                            CreateBy = currentUser.GetCurrentUser().DisplayName

                        };
                        context.CustomerUnderlyingDrafts.Add(result);
                        context.SaveChanges();

                        UnderlyingDraftID = result.UnderlyingID;

                        long ParentID = result.UnderlyingID;

                        foreach (MappingUnderlyingModel item in customerUnderlyingdraft.Proformas)
                        {

                            if (!context.MappingUnderlyingDrafts.Where(a => item.ParentID.Equals(ParentID) && item.UnderlyingID.Equals(a.UnderlyingID)).Any())
                            {
                                context.MappingUnderlyingDrafts.Add(new Entity.MappingUnderlyingDraft()
                                {
                                    ParentID = ParentID,
                                    UnderlyingID = item.UnderlyingID,
                                    CIF = item.CIF,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().DisplayName
                                });
                                context.SaveChanges();

                                // update selected checkbox
                                Entity.CustomerUnderlyingDraft dataHeader = context.CustomerUnderlyingDrafts.Where(a => a.UnderlyingID.Equals(item.UnderlyingID) && a.IsDeleted.Equals(false)).SingleOrDefault();
                                if (dataHeader != null)
                                {
                                    dataHeader.IsSelectedProforma = true;
                                    dataHeader.UpdateDate = DateTime.UtcNow;
                                    dataHeader.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                                    context.SaveChanges();
                                }
                            }

                        }

                        foreach (MappingBulkUnderlying item in customerUnderlyingdraft.BulkUnderlyings)
                        {

                            if (!context.MappingBulkUnderlyingDrafts.Where(a => item.ParentID.Equals(ParentID) && item.UnderlyingID.Equals(a.UnderlyingID)).Any())
                            {
                                context.MappingBulkUnderlyingDrafts.Add(new Entity.MappingBulkUnderlyingDraft()
                                {
                                    ParentID = ParentID,
                                    UnderlyingID = item.UnderlyingID,
                                    CIF = item.CIF,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().DisplayName
                                });
                                context.SaveChanges();

                                // update selected checkbox
                                Entity.CustomerUnderlyingDraft dataHeader = context.CustomerUnderlyingDrafts.Where(a => a.UnderlyingID.Equals(item.UnderlyingID) && a.IsDeleted.Equals(false)).SingleOrDefault();
                                if (dataHeader != null)
                                {
                                    dataHeader.IsSelectedBulk = true;
                                    dataHeader.UpdateDate = DateTime.UtcNow;
                                    dataHeader.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                                    context.SaveChanges();
                                }
                            }
                        }
                        isSuccess = true;

                    }
                    else
                    {
                        message = string.Format("Double Underlying, Customer Underlying data for Currency {1}, Amount {2} and Invoice Number {3} is already exist.", customerUnderlyingdraft.UnderlyingDocument.Name, customerUnderlyingdraft.Currency.Code, customerUnderlyingdraft.Amount, customerUnderlyingdraft.InvoiceNumber);
                    }
                    ts.Complete();
                }
                catch (Exception ex)
                {
                    ts.Dispose();
                    message = ex.Message;
                }
                return isSuccess;
            }
        }

        public bool UpdateCustomerUnderlyingDraftSubmit(long id, CustomerUnderlyingModel CustomerUnderlyingdraft, ref string message)
        {
            bool IsSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    if (CustomerUnderlyingdraft.StatementLetter.ID == 1 || !context.SP_IsDoubleUnderlying(CustomerUnderlyingdraft.CIF,
                                                                                                     CustomerUnderlyingdraft.UnderlyingDocument.ID,
                                                                                                     CustomerUnderlyingdraft.Currency.ID,
                                                                                                     CustomerUnderlyingdraft.Amount,
                                                                                                     CustomerUnderlyingdraft.InvoiceNumber, id).Any())
                    {
                        Entity.CustomerUnderlyingDraft data = context.CustomerUnderlyingDrafts.Where(x => x.UnderlyingID.Equals(id) && x.IsDeleted.Equals(false)).SingleOrDefault();
                        if (data != null)
                        {
                            data.CIF = CustomerUnderlyingdraft.CIF;
                            data.UnderlyingDocID = CustomerUnderlyingdraft.UnderlyingDocument.ID;
                            data.DocTypeID = CustomerUnderlyingdraft.DocumentType.ID;
                            data.CurrencyID = CustomerUnderlyingdraft.Currency.ID;
                            data.Amount = CustomerUnderlyingdraft.Amount;
                            data.AttachmentNo = "";
                            data.AmountUSD = CustomerUnderlyingdraft.AmountUSD.Value;
                            data.AvailableAmountUSD = CustomerUnderlyingdraft.AmountUSD.Value;
                            data.Rate = CustomerUnderlyingdraft.Rate.Value;
                            data.StatementLetterID = CustomerUnderlyingdraft.StatementLetter.ID;
                            data.IsDeclarationOfExecption = CustomerUnderlyingdraft.IsDeclarationOfException;
                            data.DateOfUnderlying = CustomerUnderlyingdraft.DateOfUnderlying.Value;
                            data.ExpiredDate = CustomerUnderlyingdraft.ExpiredDate.Value;
                            data.ReferenceNumber = CustomerUnderlyingdraft.ReferenceNumber;
                            data.SupplierName = CustomerUnderlyingdraft.SupplierName;
                            data.InvoiceNumber = CustomerUnderlyingdraft.InvoiceNumber;
                            data.IsProforma = CustomerUnderlyingdraft.IsProforma;
                            data.IsSelectedProforma = CustomerUnderlyingdraft.IsSelectedProforma;
                            data.IsBulkUnderlying = (CustomerUnderlyingdraft.IsBulkUnderlying.Value ? (CustomerUnderlyingdraft.BulkUnderlyings.Count > 0 ? true : false) : false); //saving bulks
                            data.IsSelectedBulk = CustomerUnderlyingdraft.IsSelectedBulk.HasValue ? CustomerUnderlyingdraft.IsSelectedBulk.Value : false;
                            data.IsJointAccount = CustomerUnderlyingdraft.IsJointAccount.HasValue ? CustomerUnderlyingdraft.IsJointAccount.Value : false;
                            data.IsTMO = CustomerUnderlyingdraft.IsTMO.HasValue ? CustomerUnderlyingdraft.IsTMO.Value : false;
                            data.AccountNumber = CustomerUnderlyingdraft.AccountNumber;
                            //IsUtilize = customerUnderlying.IsUtilize,
                            data.OtherUnderlying = CustomerUnderlyingdraft.OtherUnderlying;

                            data.IsDeleted = false;
                            data.CreateDate = DateTime.UtcNow;
                            data.CreateBy = currentUser.GetCurrentUser().DisplayName;

                            context.SaveChanges();
                        }
                    }
                    else
                    {
                        message = string.Format("Double Underlying, Customer Underlying data for Underlying Document {0}, Currency {1}, Amount {2} and Invoice Number {3} is already exist.", CustomerUnderlyingdraft.UnderlyingDocument.Name, CustomerUnderlyingdraft.Currency.Code, CustomerUnderlyingdraft.Amount, CustomerUnderlyingdraft.InvoiceNumber);
                    }
                    ts.Complete();
                    IsSuccess = true;
                }
                catch (Exception ex)
                {
                    ts.Dispose();
                    message = ex.Message;
                }
                return IsSuccess;
            }
        }
        public bool AddCustomerUnderlyingExcelDraft(IList<CustomerUnderlyingDraftModel> customerUnderlyingdraft, ref List<DataUnderlying> UnderlyingData, ref string message)
        {
            bool isSuccess = false;
            string cif = "";
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    cif = customerUnderlyingdraft[0].CIF;
                    foreach (CustomerUnderlyingDraftModel item in customerUnderlyingdraft)
                    {
                        //ada by fandi
                        DateTime DateofUnder;
                        DateTime ExpiredDa;
                        DateofUnder = DateTime.ParseExact(item.DateOfUnderlying2, "dd-MM-yyyy", null);
                        ExpiredDa = DateTime.ParseExact(item.ExpiredDate2, "dd-MM-yyyy", null);

                        var CurrID = context.Currencies.Where(a => a.CurrencyCode.Equals(item.Currency)).FirstOrDefault();
                        var DocType = context.DocumentTypes.Where(b => b.DocTypeName.Equals(item.DocumentType)).FirstOrDefault();
                        var UnderLayDoc = context.UnderlyingDocuments.Where(c => c.UnderlyingDocName.Equals(item.UnderlyingDocument)).FirstOrDefault();
                        var StatemrnLetter = context.StatementLetters.Where(d => d.StatementLetterName.Equals(item.StatementLetter)).FirstOrDefault();
                        //end
                        if (cif != null)
                        {
                            Entity.CustomerUnderlyingDraft result = new Entity.CustomerUnderlyingDraft()
                            {
                                CIF = cif,
                                UnderlyingDocID = StatemrnLetter.StatementLetterID != 4 ? UnderLayDoc.UnderlyingDocID : 36,
                                DocTypeID = StatemrnLetter.StatementLetterID != 4 ? DocType.DocTypeID : 1,
                                CurrencyID = StatemrnLetter.StatementLetterID != 4 ? CurrID.CurrencyID : 1,
                                Amount = item.Amount,
                                AttachmentNo = "",
                                AmountUSD = StatemrnLetter.StatementLetterID != 4 ? decimal.Parse(item.AmountUSD) : 0,
                                AvailableAmountUSD = StatemrnLetter.StatementLetterID != 4 ? decimal.Parse(item.AmountUSD) : 0,
                                Rate = StatemrnLetter.StatementLetterID != 4 ? decimal.Parse(item.Rate) : 0,
                                StatementLetterID = StatemrnLetter.StatementLetterID,
                                IsDeclarationOfExecption = false,
                                DateOfUnderlying = DateofUnder,
                                ExpiredDate = ExpiredDa,
                                ReferenceNumber = item.ReferenceNumber,
                                SupplierName = StatemrnLetter.StatementLetterID != 4 ? item.SupplierName : "-",
                                InvoiceNumber = StatemrnLetter.StatementLetterID != 4 ? item.InvoiceNumber : "-",
                                IsProforma = false,
                                IsSelectedProforma = false,
                                IsBulkUnderlying = false,
                                IsSelectedBulk = false,
                                OtherUnderlying = "",
                                IsDeleted = false,
                                CreateDate = DateTime.UtcNow,
                                CreateBy = currentUser.GetCurrentUser().DisplayName

                            };
                            context.CustomerUnderlyingDrafts.Add(result);
                            context.SaveChanges();
                            var dataID = new DataUnderlying();
                            dataID.UnderlyingID = result.UnderlyingID;

                            UnderlyingData.Add(dataID);
                        }
                    }
                    isSuccess = true;

                    ts.Complete();
                }
                catch (Exception ex)
                {
                    ts.Dispose();
                    message = ex.Message;
                }
                return isSuccess;
            }
        }


        public bool UpdateCustomerUnderlying(long id, CustomerUnderlyingModel customerUnderlying, ref string message)
        {
            bool isSuccess = false;
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    //Add validation double underlying : 2015-04-xx
                    //Changed validation double underlying (exclude statement A) : 2015-05-19
                    if (customerUnderlying.StatementLetter.ID == 1 || !context.SP_IsDoubleUnderlying(customerUnderlying.CIF,
                                                                                                     customerUnderlying.UnderlyingDocument.ID,
                                                                                                     customerUnderlying.Currency.ID,
                                                                                                     customerUnderlying.Amount,
                                                                                                     customerUnderlying.InvoiceNumber,
                                                                                                     customerUnderlying.ID).Any())
                    {
                        Entity.CustomerUnderlying data = context.CustomerUnderlyings.Where(a => a.UnderlyingID.Equals(id)).SingleOrDefault();

                        if (data != null)
                        {
                            data.CIF = customerUnderlying.CIF;
                            data.UnderlyingDocID = customerUnderlying.UnderlyingDocument.ID;
                            data.DocTypeID = customerUnderlying.DocumentType.ID;
                            data.CurrencyID = customerUnderlying.Currency.ID;
                            data.Amount = customerUnderlying.Amount;
                            data.AmountUSD = customerUnderlying.AmountUSD;
                            data.Rate = customerUnderlying.Rate;
                            data.AvailableAmountUSD = customerUnderlying.AmountUSD;
                            data.AttachmentNo = customerUnderlying.AttachmentNo;
                            data.StatementLetterID = customerUnderlying.StatementLetter.ID;
                            data.IsDeclarationOfExecption = customerUnderlying.IsDeclarationOfException;
                            data.StartDate = customerUnderlying.StartDate;
                            data.EndDate = customerUnderlying.EndDate;
                            data.DateOfUnderlying = customerUnderlying.DateOfUnderlying.Value;
                            data.ExpiredDate = customerUnderlying.ExpiredDate.Value;
                            data.ReferenceNumber = customerUnderlying.ReferenceNumber;
                            data.SupplierName = customerUnderlying.SupplierName;
                            data.IsProforma = customerUnderlying.IsProforma;
                            data.IsSelectedProforma = customerUnderlying.IsSelectedProforma;
                            data.IsBulkUnderlying = (customerUnderlying.IsBulkUnderlying.Value ? (customerUnderlying.BulkUnderlyings.Count > 0 ? true : false) : false);
                            data.IsSelectedBulk = customerUnderlying.IsSelectedBulk.HasValue ? customerUnderlying.IsSelectedBulk.Value : false;
                            data.InvoiceNumber = customerUnderlying.InvoiceNumber;
                            data.OtherUnderlying = customerUnderlying.OtherUnderlying;
                            data.IsJointAccount = customerUnderlying.IsJointAccount.HasValue ? customerUnderlying.IsJointAccount.Value : false;
                            data.IsTMO = customerUnderlying.IsTMO.HasValue ? customerUnderlying.IsTMO.Value : false;
                            data.AccountNumber = data.AccountNumber;
                            data.UpdateDate = DateTime.UtcNow;
                            data.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                            context.SaveChanges();
                            long underlyingID = data.UnderlyingID;
                            SetProformaCustomerUnderlying(id);
                            DeleteMappingUnderlying(underlyingID);
                            if (customerUnderlying.Proformas.Count > 0)
                            {
                                foreach (MappingUnderlyingModel item in customerUnderlying.Proformas)
                                {
                                    Entity.MappingUnderlying detailItem = context.MappingUnderlyings.Where(a => a.ParentID.Equals(id) && a.UnderlyingID.Equals(item.UnderlyingID)).SingleOrDefault();
                                    if (detailItem == null)
                                    {   //add data mappingunderlying
                                        context.MappingUnderlyings.Add(new Entity.MappingUnderlying()
                                        {
                                            UnderlyingID = item.UnderlyingID,
                                            ParentID = id,
                                            CIF = item.CIF,
                                            IsDeleted = false,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().DisplayName
                                        });
                                        context.SaveChanges();
                                    }
                                    else
                                    {   // if data is exist, data mappingUnderlying set delete = false
                                        detailItem.IsDeleted = false;
                                        detailItem.UpdateDate = DateTime.UtcNow;
                                        detailItem.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                                        context.SaveChanges();
                                    }
                                    // set IsSelectedPerforma = true
                                    Entity.CustomerUnderlying data1 = context.CustomerUnderlyings.Where(a => a.UnderlyingID.Equals(item.UnderlyingID)).SingleOrDefault();
                                    if (data1 != null)
                                    {
                                        data1.IsSelectedProforma = true;
                                        data1.UpdateDate = DateTime.UtcNow;
                                        data1.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                                        context.SaveChanges();
                                    }
                                }
                            }


                            SetBulkCustomerUnderlying(id);
                            DeleteMappingBulkUnderlying(underlyingID);
                            if (customerUnderlying.BulkUnderlyings.Count > 0)
                            {
                                foreach (MappingBulkUnderlying item in customerUnderlying.BulkUnderlyings)
                                {
                                    Entity.MappingBulkUnderlying detailItem = context.MappingBulkUnderlyings.Where(a => a.ParentID.Equals(id) && a.UnderlyingID.Equals(item.UnderlyingID)).SingleOrDefault();
                                    if (detailItem == null)
                                    {   //add data mappingunderlying
                                        context.MappingBulkUnderlyings.Add(new Entity.MappingBulkUnderlying()
                                        {
                                            UnderlyingID = item.UnderlyingID,
                                            ParentID = id,
                                            CIF = item.CIF,
                                            IsDeleted = false,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().DisplayName
                                        });
                                        context.SaveChanges();
                                    }
                                    else
                                    {   // if data is exist, data mappingUnderlying set delete = false
                                        detailItem.IsDeleted = false;
                                        detailItem.UpdateDate = DateTime.UtcNow;
                                        detailItem.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                                        context.SaveChanges();
                                    }
                                    // set IsSelectedPerforma = true
                                    Entity.CustomerUnderlying data1 = context.CustomerUnderlyings.Where(a => a.UnderlyingID.Equals(item.UnderlyingID)).SingleOrDefault();
                                    if (data1 != null)
                                    {
                                        data1.IsSelectedBulk = true;
                                        data1.UpdateDate = DateTime.UtcNow;
                                        data1.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                                        context.SaveChanges();
                                    }
                                }
                            }
                            isSuccess = true;
                        }
                        else
                        {
                            message = string.Format("Customer Underlying data for Customer Underlying ID {0} is does not exist.", id);
                        }

                    }
                    else
                    {
                        message = string.Format("Double Underlying, Customer Underlying data for Currency {1}, Amount {2} and Invoice Number {3} is already exist.", customerUnderlying.UnderlyingDocument.Name, customerUnderlying.Currency.Code, customerUnderlying.Amount, customerUnderlying.InvoiceNumber);
                    }
                }
                catch (Exception ex)
                {
                    ts.Dispose();
                    message = ex.Message;
                }
                ts.Complete();
            }
            return isSuccess;

        }


        #endregion

        public bool UpdateCustomerUnderlyingDraft(long id, CustomerUnderlyingModel customerUnderlyingdraft, ref long UnderlyingDraftID, ref string message)
        {
            bool isSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    if (customerUnderlyingdraft.StatementLetter.ID == 1 || !context.SP_IsDoubleUnderlying(customerUnderlyingdraft.CIF,
                                                                                                     customerUnderlyingdraft.UnderlyingDocument.ID,
                                                                                                     customerUnderlyingdraft.Currency.ID,
                                                                                                     customerUnderlyingdraft.Amount,
                                                                                                     customerUnderlyingdraft.InvoiceNumber, id).Any())
                    {
                        Entity.CustomerUnderlyingDraft result = new Entity.CustomerUnderlyingDraft()
                        {
                            MasterUnderlyingID = id,
                            CIF = customerUnderlyingdraft.CIF,
                            UnderlyingDocID = customerUnderlyingdraft.UnderlyingDocument.ID,
                            DocTypeID = customerUnderlyingdraft.DocumentType.ID,
                            CurrencyID = customerUnderlyingdraft.Currency.ID,
                            Amount = customerUnderlyingdraft.Amount,
                            AttachmentNo = "",
                            AmountUSD = customerUnderlyingdraft.AmountUSD,
                            AvailableAmountUSD = customerUnderlyingdraft.AmountUSD,
                            Rate = customerUnderlyingdraft.Rate,
                            StatementLetterID = customerUnderlyingdraft.StatementLetter.ID,
                            IsDeclarationOfExecption = customerUnderlyingdraft.IsDeclarationOfException,
                            StartDate = customerUnderlyingdraft.StartDate,
                            EndDate = customerUnderlyingdraft.EndDate,
                            DateOfUnderlying = customerUnderlyingdraft.DateOfUnderlying.Value,
                            ExpiredDate = customerUnderlyingdraft.ExpiredDate.Value,
                            ReferenceNumber = customerUnderlyingdraft.ReferenceNumber,
                            SupplierName = customerUnderlyingdraft.SupplierName,
                            InvoiceNumber = customerUnderlyingdraft.InvoiceNumber,
                            IsProforma = customerUnderlyingdraft.IsProforma,
                            IsSelectedProforma = customerUnderlyingdraft.IsSelectedProforma,
                            IsBulkUnderlying = (customerUnderlyingdraft.IsBulkUnderlying.Value ? (customerUnderlyingdraft.BulkUnderlyings.Count > 0 ? true : false) : false), //saving bulks
                            IsSelectedBulk = customerUnderlyingdraft.IsSelectedBulk.HasValue ? customerUnderlyingdraft.IsSelectedBulk.Value : false,
                            //IsUtilize = customerUnderlying.IsUtilize,
                            IsJointAccount = customerUnderlyingdraft.IsJointAccount.HasValue ? customerUnderlyingdraft.IsJointAccount.Value : false,
                            IsTMO = customerUnderlyingdraft.IsTMO.HasValue ? customerUnderlyingdraft.IsTMO.Value : false,
                            AccountNumber = customerUnderlyingdraft.AccountNumber,
                            OtherUnderlying = customerUnderlyingdraft.OtherUnderlying,
                            IsDeleted = false,
                            CreateDate = DateTime.UtcNow,
                            CreateBy = currentUser.GetCurrentUser().DisplayName,
                            //UpdateDate = DateTime.UtcNow,
                            //UpdateBy = currentUser.GetCurrentUser().DisplayName

                        };
                        context.CustomerUnderlyingDrafts.Add(result);
                        context.SaveChanges();

                        Entity.CustomerUnderlying data = context.CustomerUnderlyings.Where(a => a.UnderlyingID == result.MasterUnderlyingID).FirstOrDefault();
                        if (data != null)
                        {
                            data.IsDeleted = true;
                            data.UpdateDate = DateTime.UtcNow;
                            data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                            context.SaveChanges();
                        }

                        UnderlyingDraftID = result.UnderlyingID;
                        long ParentID = result.MasterUnderlyingID.Value;
                        foreach (MappingUnderlyingModel item in customerUnderlyingdraft.Proformas)
                        {

                            if (!context.MappingUnderlyingDrafts.Where(a => item.ParentID.Equals(ParentID) && item.UnderlyingID.Equals(a.UnderlyingID)).Any())
                            {
                                context.MappingUnderlyingDrafts.Add(new Entity.MappingUnderlyingDraft()
                                {
                                    ParentID = ParentID,
                                    UnderlyingID = item.UnderlyingID,
                                    CIF = item.CIF,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().DisplayName,
                                    UpdateDate = DateTime.UtcNow,
                                    UpdateBy = currentUser.GetCurrentUser().DisplayName
                                });
                                context.SaveChanges();

                                // update selected checkbox
                                Entity.CustomerUnderlyingDraft dataHeader = context.CustomerUnderlyingDrafts.Where(a => a.UnderlyingID.Equals(item.UnderlyingID) && a.IsDeleted.Equals(false)).SingleOrDefault();
                                if (dataHeader != null)
                                {
                                    dataHeader.IsSelectedProforma = true;
                                    dataHeader.UpdateDate = DateTime.UtcNow;
                                    dataHeader.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                                    context.SaveChanges();
                                }
                            }
                        }

                        foreach (MappingBulkUnderlying item in customerUnderlyingdraft.BulkUnderlyings)
                        {

                            if (!context.MappingBulkUnderlyingDrafts.Where(a => item.ParentID.Equals(ParentID) && item.UnderlyingID.Equals(a.UnderlyingID)).Any())
                            {
                                context.MappingBulkUnderlyingDrafts.Add(new Entity.MappingBulkUnderlyingDraft()
                                {
                                    ParentID = ParentID,
                                    UnderlyingID = item.UnderlyingID,
                                    CIF = item.CIF,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().DisplayName,
                                    UpdateDate = DateTime.UtcNow,
                                    UpdateBy = currentUser.GetCurrentUser().DisplayName
                                });
                                context.SaveChanges();

                                // update selected checkbox
                                Entity.CustomerUnderlyingDraft dataHeader = context.CustomerUnderlyingDrafts.Where(a => a.UnderlyingID.Equals(item.UnderlyingID) && a.IsDeleted.Equals(false)).SingleOrDefault();
                                if (dataHeader != null)
                                {
                                    dataHeader.IsSelectedBulk = true;
                                    dataHeader.UpdateDate = DateTime.UtcNow;
                                    dataHeader.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                                    context.SaveChanges();
                                }
                            }
                        }
                        isSuccess = true;

                    }
                    else
                    {
                        message = string.Format("Double Underlying, Customer Underlying data for Currency {1}, Amount {2} and Invoice Number {3} is already exist.", customerUnderlyingdraft.UnderlyingDocument.Name, customerUnderlyingdraft.Currency.Code, customerUnderlyingdraft.Amount, customerUnderlyingdraft.InvoiceNumber);
                    }
                    ts.Complete();
                }
                catch (Exception ex)
                {
                    ts.Dispose();
                    message = ex.Message;
                }
                return isSuccess;
            }
        }


        public bool UpdateCustomerUnderlyingUtilize(IList<TransUnderlyingModel> customerUnderlyings, ref string message)
        {
            bool isSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {

                    foreach (TransUnderlyingModel item in customerUnderlyings)
                    {
                        Entity.CustomerUnderlying data = context.CustomerUnderlyings.Where(a => a.UnderlyingID.Equals(item.ID)).SingleOrDefault();
                        if (data != null)
                        {
                            if (item.Enable == true)
                            {
                                data.IsUtilize = !item.Enable;
                                data.UpdateDate = DateTime.UtcNow;
                                data.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                                context.SaveChanges();
                            }
                        }
                        else
                        {
                            message = string.Format("Customer Underlying data for Customer Underlying ID {0} is does not exist.", item.ID);
                        }
                    }
                    ts.Complete();
                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    ts.Dispose();
                    message = ex.Message;
                }
            }
            return isSuccess;

        }

        private void DeleteMappingUnderlying(long parentID)
        {
            try
            {
                var detailData = context.MappingUnderlyings.Where(a => a.ParentID.Equals(parentID)).ToList();
                detailData.ForEach(a =>
                {
                    a.IsDeleted = true;
                    a.UpdateDate = DateTime.UtcNow;
                    a.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                });
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void DeleteMappingBulkUnderlying(long parentID)
        {
            try
            {
                var detailData = context.MappingBulkUnderlyings.Where(a => a.ParentID.Equals(parentID)).ToList();
                detailData.ForEach(a =>
                {
                    a.IsDeleted = true;
                    a.UpdateDate = DateTime.UtcNow;
                    a.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                });

                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SetProformaCustomerUnderlying(long id)
        {
            try
            {
                var mappingData = (from a in context.MappingUnderlyings
                                   join b in context.CustomerUnderlyings on a.ParentID equals b.UnderlyingID
                                   where b.IsDeleted.Equals(false) && a.IsDeleted.Equals(false)
                                   select new { UnderlyingID = a.UnderlyingID }).ToList();
                if (mappingData != null)
                {
                    foreach (var data in mappingData)
                    {
                        var CustomerU = context.CustomerUnderlyings.Where(a => a.UnderlyingID == data.UnderlyingID).SingleOrDefault();
                        CustomerU.IsSelectedProforma = false;
                        CustomerU.UpdateDate = DateTime.UtcNow;
                        CustomerU.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void SetBulkCustomerUnderlying(long id)
        {
            try
            {
                var mappingData = (from a in context.MappingBulkUnderlyings
                                   join b in context.CustomerUnderlyings on a.ParentID equals b.UnderlyingID
                                   where b.IsDeleted.Equals(false) && a.IsDeleted.Equals(false)
                                   select new { UnderlyingID = a.UnderlyingID }).ToList();
                if (mappingData != null)
                {
                    foreach (var data in mappingData)
                    {
                        var CustomerU = context.CustomerUnderlyings.Where(a => a.UnderlyingID == data.UnderlyingID).SingleOrDefault();
                        CustomerU.IsSelectedBulk = false;
                        CustomerU.UpdateDate = DateTime.UtcNow;
                        CustomerU.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteCustomerUnderlyingDraft(long id, CustomerUnderlyingModel customerUnderlyingdraft, ref long UnderlyingDraftID, ref string message)
        {
            bool isSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    if (customerUnderlyingdraft.StatementLetter.ID == 1 || true)
                    {
                        Entity.CustomerUnderlyingDraft result = new Entity.CustomerUnderlyingDraft()
                        {
                            MasterUnderlyingID = id,
                            CIF = customerUnderlyingdraft.CIF,
                            UnderlyingDocID = customerUnderlyingdraft.UnderlyingDocument.ID,
                            DocTypeID = customerUnderlyingdraft.DocumentType.ID,
                            CurrencyID = customerUnderlyingdraft.Currency.ID,
                            Amount = customerUnderlyingdraft.Amount,
                            AttachmentNo = "",
                            AmountUSD = customerUnderlyingdraft.AmountUSD,
                            AvailableAmountUSD = customerUnderlyingdraft.AmountUSD,
                            Rate = customerUnderlyingdraft.Rate,
                            StatementLetterID = customerUnderlyingdraft.StatementLetter.ID,
                            IsDeclarationOfExecption = customerUnderlyingdraft.IsDeclarationOfException,
                            StartDate = customerUnderlyingdraft.StartDate,
                            EndDate = customerUnderlyingdraft.EndDate,
                            DateOfUnderlying = customerUnderlyingdraft.DateOfUnderlying.Value,
                            ExpiredDate = customerUnderlyingdraft.ExpiredDate.Value,
                            ReferenceNumber = customerUnderlyingdraft.ReferenceNumber,
                            SupplierName = customerUnderlyingdraft.SupplierName,
                            InvoiceNumber = customerUnderlyingdraft.InvoiceNumber,
                            IsProforma = customerUnderlyingdraft.IsProforma,
                            IsSelectedProforma = customerUnderlyingdraft.IsSelectedProforma,
                            IsBulkUnderlying = (customerUnderlyingdraft.IsBulkUnderlying.Value ? (customerUnderlyingdraft.BulkUnderlyings.Count > 0 ? true : false) : false), //saving bulks
                            IsSelectedBulk = customerUnderlyingdraft.IsSelectedBulk.HasValue ? customerUnderlyingdraft.IsSelectedBulk.Value : false,
                            //IsUtilize = customerUnderlying.IsUtilize,
                            OtherUnderlying = customerUnderlyingdraft.OtherUnderlying,
                            IsDeleted = false,
                            CreateDate = DateTime.UtcNow,
                            CreateBy = currentUser.GetCurrentUser().DisplayName,
                            UpdateDate = DateTime.UtcNow,
                            UpdateBy = currentUser.GetCurrentUser().DisplayName

                        };
                        context.CustomerUnderlyingDrafts.Add(result);
                        context.SaveChanges();

                        Entity.CustomerUnderlying data = context.CustomerUnderlyings.Where(a => a.UnderlyingID == result.MasterUnderlyingID).FirstOrDefault();
                        if (data != null)
                        {
                            data.IsDeleted = true;
                            data.UpdateDate = DateTime.UtcNow;
                            data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                            context.SaveChanges();
                        }

                        UnderlyingDraftID = result.UnderlyingID;
                        long ParentID = result.MasterUnderlyingID.Value;
                        foreach (MappingUnderlyingModel item in customerUnderlyingdraft.Proformas)
                        {

                            if (!context.MappingUnderlyingDrafts.Where(a => item.ParentID.Equals(ParentID) && item.UnderlyingID.Equals(a.UnderlyingID)).Any())
                            {
                                context.MappingUnderlyingDrafts.Add(new Entity.MappingUnderlyingDraft()
                                {
                                    ParentID = ParentID,
                                    UnderlyingID = item.UnderlyingID,
                                    CIF = item.CIF,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().DisplayName,
                                    UpdateDate = DateTime.UtcNow,
                                    UpdateBy = currentUser.GetCurrentUser().DisplayName
                                });
                                context.SaveChanges();

                                // update selected checkbox
                                Entity.CustomerUnderlyingDraft dataHeader = context.CustomerUnderlyingDrafts.Where(a => a.UnderlyingID.Equals(item.UnderlyingID) && a.IsDeleted.Equals(false)).SingleOrDefault();
                                if (dataHeader != null)
                                {
                                    dataHeader.IsSelectedProforma = true;
                                    dataHeader.UpdateDate = DateTime.UtcNow;
                                    dataHeader.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                                    context.SaveChanges();
                                }
                            }
                        }

                        foreach (MappingBulkUnderlying item in customerUnderlyingdraft.BulkUnderlyings)
                        {

                            if (!context.MappingBulkUnderlyingDrafts.Where(a => item.ParentID.Equals(ParentID) && item.UnderlyingID.Equals(a.UnderlyingID)).Any())
                            {
                                context.MappingBulkUnderlyingDrafts.Add(new Entity.MappingBulkUnderlyingDraft()
                                {
                                    ParentID = ParentID,
                                    UnderlyingID = item.UnderlyingID,
                                    CIF = item.CIF,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().DisplayName,
                                    UpdateDate = DateTime.UtcNow,
                                    UpdateBy = currentUser.GetCurrentUser().DisplayName
                                });
                                context.SaveChanges();

                                // update selected checkbox
                                Entity.CustomerUnderlyingDraft dataHeader = context.CustomerUnderlyingDrafts.Where(a => a.UnderlyingID.Equals(item.UnderlyingID) && a.IsDeleted.Equals(false)).SingleOrDefault();
                                if (dataHeader != null)
                                {
                                    dataHeader.IsSelectedBulk = true;
                                    dataHeader.UpdateDate = DateTime.UtcNow;
                                    dataHeader.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                                    context.SaveChanges();
                                }
                            }
                        }
                        isSuccess = true;
                    }
                    else
                    {
                        message = string.Format("Double Underlying, Customer Underlying data for Underlying Document {0}, Currency {1}, Amount {2} and Invoice Number {3} is already exist.", customerUnderlyingdraft.UnderlyingDocument.Name, customerUnderlyingdraft.Currency.Code, customerUnderlyingdraft.Amount, customerUnderlyingdraft.InvoiceNumber);
                    }
                    ts.Complete();
                }
                catch (Exception ex)
                {
                    ts.Dispose();
                    message = ex.Message;
                }
                return isSuccess;
            }
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        public bool GetCustomerUnderlyingFile(long id, int page, int size, IList<CustomerUnderlyingFileFilter> filters, string sortColumn, string sortOrder, ref CustomerUnderlyingFileGrid customer, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                CustomerUnderlyingFileModel filter = new CustomerUnderlyingFileModel();
                filter.DocumentPurpose = new DocumentPurposeModel { Name = string.Empty };
                if (filters != null)
                {
                    filter.CIF = (string)filters.Where(a => a.Field.Equals("CIF")).Select(a => a.Value).SingleOrDefault();
                    filter.DocumentPurpose.Name = (string)filters.Where(a => a.Field.Equals("DocumentPurpose")).Select(a => a.Value).SingleOrDefault();
                    filter.FileName = (string)filters.Where(a => a.Field.Equals("FileName")).Select(a => a.Value).SingleOrDefault();
                    filter.DocumentRefNumber = (string)filters.Where(a => a.Field.Equals("DocumentRefNumber")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    IEnumerable<CustomerUnderlyingFileModel> data = (from a in context.CustomerUnderlyingFiles
                                                                     join b in context.CustomerUnderlyingMappings on a.UnderlyingFileID equals b.UnderlyingFileID
                                                                     join c in context.CustomerUnderlyings on b.UnderlyingID equals c.UnderlyingID
                                                                     join f in context.TransactionUnderlyings on c.UnderlyingID equals f.UnderlyingID
                                                                     let isCIF = !string.IsNullOrEmpty(filter.CIF)
                                                                     let isDocPurpose = !string.IsNullOrEmpty(filter.DocumentPurpose.Name)
                                                                     let isFileName = !string.IsNullOrEmpty(filter.FileName)
                                                                     let isDocumentRefNumber = !string.IsNullOrEmpty(filter.DocumentRefNumber)
                                                                     let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                                                     let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                                                     where a.IsDeleted.Equals(false) && f.TransactionID.Equals(id)
                                                                     && b.IsDeleted.Equals(false) && c.IsDeleted.Equals(false) &&
                                                                         (isCIF ? c.CIF.Equals(filter.CIF) : true) &&
                                                                         (isDocPurpose ? a.DocumentPurpose.PurposeName.Contains(filter.DocumentPurpose.Name) : true) &&
                                                                         (isFileName ? a.FileName.Contains(filter.FileName) : true) &&
                                                                         (isDocumentRefNumber ? a.DocumentRefNumber.Contains(filter.DocumentRefNumber) : true) &&
                                                                         (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                                                         (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                                                     select new CustomerUnderlyingFileModel
                                                                     {
                                                                         ID = a.UnderlyingFileID,
                                                                         FileName = a.FileName,
                                                                         DocumentPath = a.DocumentPath,
                                                                         DocumentRefNumber = c.ReferenceNumber == null ? "" : c.ReferenceNumber,
                                                                         DocumentPurpose = new DocumentPurposeModel { ID = a.DocumentPurpose.PurposeID, Name = a.DocumentPurpose.PurposeName, Description = a.DocumentPurpose.PurposeDescription },
                                                                         DocumentType = new DocumentTypeModel { ID = a.DocumentType.DocTypeID, Name = a.DocumentType.DocTypeName, Description = a.DocumentType.DocTypeDescription },
                                                                         LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                                         LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                                                     });
                    var groupData = (from a in data.ToList()
                                     group a by a.ID into groupFile
                                     select new CustomerUnderlyingFileModel
                                     {
                                         ID = groupFile.FirstOrDefault().ID,
                                         FileName = groupFile.FirstOrDefault().FileName,
                                         DocumentPath = groupFile.FirstOrDefault().DocumentPath,
                                         DocumentRefNumber = string.Join(", ", (groupFile.Select(x => x.DocumentRefNumber)).ToArray()),
                                         DocumentPurpose = groupFile.FirstOrDefault().DocumentPurpose,
                                         DocumentType = groupFile.FirstOrDefault().DocumentType,
                                         LastModifiedBy = groupFile.FirstOrDefault().LastModifiedBy,
                                         LastModifiedDate = groupFile.FirstOrDefault().LastModifiedDate
                                     }).AsQueryable();
                    customer.Page = page;
                    customer.Size = size;
                    customer.Total = groupData.Count();
                    customer.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    customer.Rows = groupData.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new CustomerUnderlyingFileRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            FileName = a.FileName,
                            DocumentPath = a.DocumentPath,
                            DocumentRefNumber = a.DocumentRefNumber,
                            DocumentPurpose = a.DocumentPurpose,
                            DocumentType = a.DocumentType,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate,
                            CustomerUnderlyingMappings = context.CustomerUnderlyingMappings.Where(e => e.UnderlyingFileID == e.UnderlyingFileID && e.IsDeleted.Equals(false)).Select(e => new CustomerUnderlyingMappingModel
                            {
                                ID = e.UnderlyingMappingID,
                                UnderlyingID = e.UnderlyingID,
                                UnderlyingFileID = e.UnderlyingFileID
                            }).ToList()
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }


        public bool GetCustomerUnderlyingProforma(long id, int page, int size, IList<CustomerUnderlyingFilter> filters, string sortColumn, string sortOrder, ref CustomerUnderlyingGrid CustomerUnderlyingGrid, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                CustomerUnderlyingModel filter = new CustomerUnderlyingModel();
                if (filters != null)
                {
                    var parentId = filters.Where(a => a.Field.Equals("ParentID")).Select(a => a.Value).SingleOrDefault();
                    filter.ParentID = (parentId != null ? long.Parse((string)parentId) : 0);
                    filter.CIF = (string)filters.Where(a => a.Field.Equals("CIF")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("IsSelected")).Any())
                    {
                        filter.IsSelectedProforma = bool.Parse((string)filters.Where(a => a.Field.Equals("IsSelected")).Select(a => a.Value).SingleOrDefault());
                    }
                    filter.UnderlyingDocument = new UnderlyingDocModel { Name = (string)filters.Where(a => a.Field.Equals("UnderlyingDocument")).Select(a => a.Value).SingleOrDefault() };
                    filter.DocumentType = new DocumentTypeModel { Name = (string)filters.Where(a => a.Field.Equals("DocumentType")).Select(a => a.Value).SingleOrDefault() };
                    filter.Currency = new CurrencyModel { Code = (string)filters.Where(a => a.Field.Equals("Currency")).Select(a => a == null ? "0" : a.Value).SingleOrDefault() };
                    filter.StatementLetter = new StatementLetterModel { Name = (string)filters.Where(a => a.Field.Equals("StatementLetter")).Select(a => a.Value).SingleOrDefault() };
                    filter.Amount = Convert.ToDecimal((string)filters.Where(a => a.Field.Equals("Amount")).Select(a => a == null ? "0" : a.Value).SingleOrDefault());
                    filter.AttachmentNo = (string)filters.Where(a => a.Field.Equals("AttachmentNo")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("StartDate")).Any())
                    {
                        filter.StartDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("StartDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("EndDate")).Any())
                    {
                        filter.EndDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("EndDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("DateOfUnderlying")).Any())
                    {
                        filter.DateOfUnderlying = DateTime.Parse((string)filters.Where(a => a.Field.Equals("DateOfUnderlying")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("ExpiredDate")).Any())
                    {
                        filter.ExpiredDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("ExpiredDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    filter.ReferenceNumber = (string)filters.Where(a => a.Field.Equals("ReferenceNumber")).Select(a => a.Value).SingleOrDefault();
                    filter.SupplierName = (string)filters.Where(a => a.Field.Equals("SupplierName")).Select(a => a.Value).SingleOrDefault();
                    filter.InvoiceNumber = (string)filters.Where(a => a.Field.Equals("InvoiceNumber")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }
                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.CustomerUnderlyings
                                join b in context.MappingUnderlyings.Where(z => z.IsDeleted.Equals(false)) on a.UnderlyingID equals b.UnderlyingID into temp
                                join c in context.TransactionUnderlyings on a.UnderlyingID equals c.UnderlyingID
                                from x in temp.DefaultIfEmpty()
                                let isCIF = !string.IsNullOrEmpty(filter.CIF)
                                let isUnderlyingDocument = !string.IsNullOrEmpty(filter.UnderlyingDocument.Name)
                                let isDocumentType = !string.IsNullOrEmpty(filter.DocumentType.Name)
                                let isCurrency = !string.IsNullOrEmpty(filter.Currency.Code)
                                let isStatementLetter = !string.IsNullOrEmpty(filter.StatementLetter.Name)
                                let isAmount = filter.Amount != null && filter.Amount != 0 ? true : false
                                let isAttachmentNo = !string.IsNullOrEmpty(filter.AttachmentNo)
                                let isStartDate = filter.StartDate.HasValue ? true : false
                                let isEndDate = filter.EndDate.HasValue ? true : false
                                let isDateOfUnderlying = filter.DateOfUnderlying.HasValue ? true : false
                                let isExpiredDate = filter.ExpiredDate.HasValue ? true : false
                                let isReferenceNumber = !string.IsNullOrEmpty(filter.ReferenceNumber)
                                let isSupplierName = !string.IsNullOrEmpty(filter.SupplierName)
                                let isInvoiceNumber = filter.InvoiceNumber != null ? true : false
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where (a.IsSelectedProforma.Equals(false) || x.ParentID.Equals(filter.ParentID)) && c.TransactionID.Equals(id) &&
                                a.DocumentType.DocTypeName.Contains(filter.DocumentType.Name) && a.UnderlyingID != filter.ParentID &&
                                    (isCIF ? a.CIF.Equals(filter.CIF) : true) &&
                                    (isUnderlyingDocument ? a.UnderlyingDocument.UnderlyingDocName.Contains(filter.UnderlyingDocument.Name) : true) &&
                                    (isDocumentType ? a.DocumentType.DocTypeName.Contains(filter.DocumentType.Name) : true) &&
                                    (isCurrency ? a.Currency.CurrencyCode.Contains(filter.Currency.Code) : true) &&
                                    (isStatementLetter ? a.StatementLetter.StatementLetterName.Contains(filter.StatementLetter.Name) : true) &&
                                    (isAmount ? a.Amount.Equals(filter.Amount) : true) &&
                                    (isAttachmentNo ? a.AttachmentNo.Contains(filter.AttachmentNo) : true) &&
                                    (isStartDate ? a.StartDate.Equals(filter.StartDate) : true) &&
                                    (isEndDate ? a.EndDate.Equals(filter.EndDate) : true) &&
                                    (isDateOfUnderlying ? a.DateOfUnderlying.Equals(filter.DateOfUnderlying) : true) &&
                                    (isExpiredDate ? a.ExpiredDate.Equals(filter.ExpiredDate) : true) &&
                                    (isReferenceNumber ? a.ReferenceNumber.Contains(filter.ReferenceNumber) : true) &&
                                    (isSupplierName ? a.SupplierName.Contains(filter.SupplierName) : true) &&
                                    (isInvoiceNumber ? a.InvoiceNumber.Contains(filter.InvoiceNumber) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)

                                select new CustomerUnderlyingModel
                                {
                                    ID = a.UnderlyingID,
                                    CIF = a.CIF,
                                    UnderlyingDocument = new UnderlyingDocModel
                                    {
                                        ID = a.UnderlyingDocument.UnderlyingDocID,
                                        Code = a.UnderlyingDocument.UnderlyingDocCode,
                                        Name = a.UnderlyingDocument.UnderlyingDocName,
                                        LastModifiedBy = a.UnderlyingDocument.UpdateDate == null ? a.UnderlyingDocument.CreateBy : a.UnderlyingDocument.UpdateBy,
                                        LastModifiedDate = a.UnderlyingDocument.UpdateDate == null ? a.UnderlyingDocument.CreateDate : a.UnderlyingDocument.UpdateDate.Value
                                    },
                                    DocumentType = new DocumentTypeModel
                                    {
                                        ID = a.DocumentType.DocTypeID,
                                        Name = a.DocumentType.DocTypeName,
                                        Description = a.DocumentType.DocTypeDescription,
                                        LastModifiedBy = a.DocumentType.UpdateDate == null ? a.DocumentType.CreateBy : a.DocumentType.UpdateBy,
                                        LastModifiedDate = a.DocumentType.UpdateDate == null ? a.DocumentType.CreateDate : a.DocumentType.UpdateDate.Value
                                    },
                                    Currency = new CurrencyModel
                                    {
                                        ID = a.Currency.CurrencyID,
                                        Code = a.Currency.CurrencyCode,
                                        LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                        LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                    },
                                    StatementLetter = new StatementLetterModel
                                    {
                                        ID = a.StatementLetter.StatementLetterID,
                                        Name = a.StatementLetter.StatementLetterName,
                                        LastModifiedBy = a.StatementLetter.UpdateDate == null ? a.StatementLetter.CreateBy : a.StatementLetter.UpdateBy,
                                        LastModifiedDate = a.StatementLetter.UpdateDate == null ? a.StatementLetter.CreateDate : a.StatementLetter.UpdateDate.Value
                                    },
                                    Amount = a.Amount,
                                    AvailableAmount = a.AvailableAmountUSD == null ? 0 : a.AvailableAmountUSD,
                                    AttachmentNo = a.AttachmentNo,
                                    IsDeclarationOfException = a.IsDeclarationOfExecption,
                                    StartDate = a.StartDate,
                                    EndDate = a.EndDate,
                                    DateOfUnderlying = a.DateOfUnderlying,
                                    ExpiredDate = a.ExpiredDate,
                                    ReferenceNumber = a.ReferenceNumber,
                                    SupplierName = a.SupplierName,
                                    InvoiceNumber = a.InvoiceNumber,
                                    IsProforma = a.IsProforma,
                                    IsSelectedProforma = a.IsSelectedProforma,
                                    IsSelectedBulk = a.IsSelectedBulk == null ? false : a.IsSelectedBulk,
                                    IsBulkUnderlying = a.IsBulkUnderlying == null ? false : a.IsBulkUnderlying,
                                    IsUtilize = a.IsUtilize,
                                    OtherUnderlying = a.OtherUnderlying,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    CustomerUnderlyingGrid.Page = page;
                    CustomerUnderlyingGrid.Size = size;
                    CustomerUnderlyingGrid.Total = data.Count();
                    CustomerUnderlyingGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    CustomerUnderlyingGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new CustomerUnderlyingRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            CIF = a.CIF,
                            UnderlyingDocument = a.UnderlyingDocument,
                            DocumentType = a.DocumentType,
                            Currency = a.Currency,
                            Amount = a.Amount,
                            AvailableAmount = a.AvailableAmount.HasValue ? (decimal?)Decimal.Round(a.AvailableAmount.Value, 2) : null,
                            AttachmentNo = a.AttachmentNo,
                            StatementLetter = a.StatementLetter,
                            IsDeclarationOfException = a.IsDeclarationOfException,
                            StartDate = a.StartDate,
                            EndDate = a.EndDate,
                            DateOfUnderlying = a.DateOfUnderlying,
                            ExpiredDate = a.ExpiredDate,
                            ReferenceNumber = a.ReferenceNumber,
                            SupplierName = a.SupplierName,
                            IsProforma = a.IsProforma,
                            IsSelectedProforma = a.IsSelectedProforma,
                            IsSelectedBulk = a.IsSelectedBulk,
                            IsBulkUnderlying = a.IsBulkUnderlying,
                            IsUtilize = a.IsUtilize,
                            InvoiceNumber = a.InvoiceNumber,
                            OtherUnderlying = a.OtherUnderlying,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }


        public bool GetCustomerUnderlyingAttachByTransactionID(long id, int page, int size, IList<CustomerUnderlyingFilter> filters, string sortColumn, string sortOrder, ref CustomerUnderlyingGrid CustomerUnderlyingGrid, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                CustomerUnderlyingModel filter = new CustomerUnderlyingModel();
                if (filters != null)
                {
                    var parentId = filters.Where(a => a.Field.Equals("ParentID")).Select(a => a.Value).SingleOrDefault();
                    filter.CIF = (string)filters.Where(a => a.Field.Equals("CIF")).Select(a => a.Value).SingleOrDefault();
                    filter.ParentID = (parentId != null ? long.Parse((string)parentId) : 0);
                    filter.UnderlyingDocument = new UnderlyingDocModel { Name = (string)filters.Where(a => a.Field.Equals("UnderlyingDocument")).Select(a => a.Value).SingleOrDefault() };
                    filter.DocumentType = new DocumentTypeModel { Name = (string)filters.Where(a => a.Field.Equals("DocumentType")).Select(a => a.Value).SingleOrDefault() };
                    filter.Currency = new CurrencyModel { Code = (string)filters.Where(a => a.Field.Equals("Currency")).Select(a => a == null ? "0" : a.Value).SingleOrDefault() };
                    filter.StatementLetter = new StatementLetterModel { Name = (string)filters.Where(a => a.Field.Equals("StatementLetter")).Select(a => a.Value).SingleOrDefault() };
                    filter.Amount = Convert.ToDecimal((string)filters.Where(a => a.Field.Equals("Amount")).Select(a => a == null ? "0" : a.Value).SingleOrDefault());
                    filter.AttachmentNo = (string)filters.Where(a => a.Field.Equals("AttachmentNo")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("StartDate")).Any())
                    {
                        filter.StartDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("StartDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("EndDate")).Any())
                    {
                        filter.EndDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("EndDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("DateOfUnderlying")).Any())
                    {
                        filter.DateOfUnderlying = DateTime.Parse((string)filters.Where(a => a.Field.Equals("DateOfUnderlying")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("ExpiredDate")).Any())
                    {
                        filter.ExpiredDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("ExpiredDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    filter.ReferenceNumber = (string)filters.Where(a => a.Field.Equals("ReferenceNumber")).Select(a => a.Value).SingleOrDefault();
                    filter.SupplierName = (string)filters.Where(a => a.Field.Equals("SupplierName")).Select(a => a.Value).SingleOrDefault();
                    filter.InvoiceNumber = (string)filters.Where(a => a.Field.Equals("InvoiceNumber")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }
                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.CustomerUnderlyings
                                join b in context.TransactionUnderlyings on a.UnderlyingID equals b.UnderlyingID
                                let isCIF = !string.IsNullOrEmpty(filter.CIF)
                                let isUnderlyingDocument = !string.IsNullOrEmpty(filter.UnderlyingDocument.Name)
                                let isDocumentType = !string.IsNullOrEmpty(filter.DocumentType.Name)
                                let isCurrency = !string.IsNullOrEmpty(filter.Currency.Code)
                                let isStatementLetter = !string.IsNullOrEmpty(filter.StatementLetter.Name)
                                let isAmount = filter.Amount != null && filter.Amount != 0 ? true : false
                                let isAttachmentNo = !string.IsNullOrEmpty(filter.AttachmentNo)
                                let isStartDate = filter.StartDate.HasValue ? true : false
                                let isEndDate = filter.EndDate.HasValue ? true : false
                                let isDateOfUnderlying = filter.DateOfUnderlying.HasValue ? true : false
                                let isExpiredDate = filter.ExpiredDate.HasValue ? true : false
                                let isReferenceNumber = !string.IsNullOrEmpty(filter.ReferenceNumber)
                                let isSupplierName = !string.IsNullOrEmpty(filter.SupplierName)
                                let isInvoiceNumber = filter.InvoiceNumber != null ? true : false
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) && a.IsAttachFile.Equals(false) && b.TransactionID.Equals(id) &&
                                    (isCIF ? a.CIF.Equals(filter.CIF) : true) &&
                                    (isUnderlyingDocument ? a.UnderlyingDocument.UnderlyingDocName.Contains(filter.UnderlyingDocument.Name) : true) &&
                                    (isDocumentType ? a.DocumentType.DocTypeName.Contains(filter.DocumentType.Name) : true) &&
                                    (isCurrency ? a.Currency.CurrencyCode.Contains(filter.Currency.Code) : true) &&
                                    (isStatementLetter ? a.StatementLetter.StatementLetterName.Contains(filter.StatementLetter.Name) : true) &&
                                    (isAmount ? a.Amount.Equals(filter.Amount) : true) &&
                                    (isAttachmentNo ? a.AttachmentNo.Contains(filter.AttachmentNo) : true) &&
                                    (isStartDate ? a.StartDate.Equals(filter.StartDate) : true) &&
                                    (isEndDate ? a.EndDate.Equals(filter.EndDate) : true) &&
                                    (isDateOfUnderlying ? a.DateOfUnderlying.Equals(filter.DateOfUnderlying) : true) &&
                                    (isExpiredDate ? a.ExpiredDate.Equals(filter.ExpiredDate) : true) &&
                                    (isReferenceNumber ? a.ReferenceNumber.Contains(filter.ReferenceNumber) : true) &&
                                    (isSupplierName ? a.SupplierName.Contains(filter.SupplierName) : true) &&
                                    (isInvoiceNumber ? a.InvoiceNumber.Contains(filter.InvoiceNumber) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)

                                select new CustomerUnderlyingModel
                                {
                                    ID = a.UnderlyingID,
                                    CIF = a.CIF,
                                    UnderlyingDocument = new UnderlyingDocModel
                                    {
                                        ID = a.UnderlyingDocument.UnderlyingDocID,
                                        Code = a.UnderlyingDocument.UnderlyingDocCode,
                                        Name = a.UnderlyingDocument.UnderlyingDocName,
                                        LastModifiedBy = a.UnderlyingDocument.UpdateDate == null ? a.UnderlyingDocument.CreateBy : a.UnderlyingDocument.UpdateBy,
                                        LastModifiedDate = a.UnderlyingDocument.UpdateDate == null ? a.UnderlyingDocument.CreateDate : a.UnderlyingDocument.UpdateDate.Value
                                    },
                                    DocumentType = new DocumentTypeModel
                                    {
                                        ID = a.DocumentType.DocTypeID,
                                        Name = a.DocumentType.DocTypeName,
                                        Description = a.DocumentType.DocTypeDescription,
                                        LastModifiedBy = a.DocumentType.UpdateDate == null ? a.DocumentType.CreateBy : a.DocumentType.UpdateBy,
                                        LastModifiedDate = a.DocumentType.UpdateDate == null ? a.DocumentType.CreateDate : a.DocumentType.UpdateDate.Value
                                    },
                                    Currency = new CurrencyModel
                                    {
                                        ID = a.Currency.CurrencyID,
                                        Code = a.Currency.CurrencyCode,
                                        LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                        LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                    },
                                    StatementLetter = new StatementLetterModel
                                    {
                                        ID = a.StatementLetter.StatementLetterID,
                                        Name = a.StatementLetter.StatementLetterName,
                                        LastModifiedBy = a.StatementLetter.UpdateDate == null ? a.StatementLetter.CreateBy : a.StatementLetter.UpdateBy,
                                        LastModifiedDate = a.StatementLetter.UpdateDate == null ? a.StatementLetter.CreateDate : a.StatementLetter.UpdateDate.Value
                                    },
                                    Amount = a.Amount,
                                    AvailableAmount = a.AvailableAmountUSD == null ? 0 : a.AvailableAmountUSD,
                                    AttachmentNo = a.AttachmentNo,
                                    IsDeclarationOfException = a.IsDeclarationOfExecption,
                                    StartDate = a.StartDate,
                                    EndDate = a.EndDate,
                                    DateOfUnderlying = a.DateOfUnderlying,
                                    ExpiredDate = a.ExpiredDate,
                                    ReferenceNumber = a.ReferenceNumber,
                                    SupplierName = a.SupplierName,
                                    IsProforma = a.IsProforma,
                                    IsSelectedProforma = a.IsSelectedProforma,
                                    IsBulkUnderlying = a.IsBulkUnderlying == null ? false : a.IsBulkUnderlying,
                                    IsSelectedBulk = a.IsSelectedBulk.HasValue ? a.IsSelectedBulk.Value : false,
                                    InvoiceNumber = a.InvoiceNumber,
                                    IsSelectedAttach = a.IsAttachFile,
                                    OtherUnderlying = a.OtherUnderlying,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    CustomerUnderlyingGrid.Page = page;
                    CustomerUnderlyingGrid.Size = size;
                    CustomerUnderlyingGrid.Total = data.Count();
                    CustomerUnderlyingGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    CustomerUnderlyingGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new CustomerUnderlyingRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            CIF = a.CIF,
                            UnderlyingDocument = a.UnderlyingDocument,
                            DocumentType = a.DocumentType,
                            Currency = a.Currency,
                            Amount = a.Amount,
                            AvailableAmount = a.AvailableAmount.HasValue ? (decimal?)Decimal.Round(a.AvailableAmount.Value, 2) : null,
                            AttachmentNo = a.AttachmentNo,
                            StatementLetter = a.StatementLetter,
                            IsDeclarationOfException = a.IsDeclarationOfException,
                            StartDate = a.StartDate,
                            EndDate = a.EndDate,
                            DateOfUnderlying = a.DateOfUnderlying,
                            ExpiredDate = a.ExpiredDate,
                            ReferenceNumber = a.ReferenceNumber,
                            SupplierName = a.SupplierName,
                            IsSelectedProforma = a.IsSelectedProforma,
                            InvoiceNumber = a.InvoiceNumber,
                            IsProforma = a.IsProforma,
                            IsBulkUnderlying = a.IsBulkUnderlying,
                            IsSelectedBulk = a.IsSelectedBulk,
                            OtherUnderlying = a.OtherUnderlying,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }


        public bool GetCustomerUnderlying(long id, int page, int size, IList<CustomerUnderlyingFilter> filters, string sortColumn, string sortOrder, ref CustomerUnderlyingGrid CustomerUnderlying, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                CustomerUnderlyingModel filter = new CustomerUnderlyingModel();
                if (filters != null)
                {
                    var parentId = filters.Where(a => a.Field.Equals("ParentID")).Select(a => a.Value).SingleOrDefault();
                    filter.ParentID = (parentId != null ? long.Parse((string)parentId) : 0);
                    filter.UnderlyingDocument = new UnderlyingDocModel { Name = (string)filters.Where(a => a.Field.Equals("UnderlyingDocument")).Select(a => a.Value).SingleOrDefault() };
                    filter.DocumentType = new DocumentTypeModel { Name = (string)filters.Where(a => a.Field.Equals("DocumentType")).Select(a => a.Value).SingleOrDefault() };
                    filter.Currency = new CurrencyModel { Code = (string)filters.Where(a => a.Field.Equals("Currency")).Select(a => a == null ? "0" : a.Value).SingleOrDefault() };
                    filter.StatementLetter = new StatementLetterModel { Name = (string)filters.Where(a => a.Field.Equals("StatementLetter")).Select(a => a.Value).SingleOrDefault() };
                    filter.Amount = Convert.ToDecimal((string)filters.Where(a => a.Field.Equals("Amount")).Select(a => a == null ? "0" : a.Value).SingleOrDefault());
                    filter.AttachmentNo = (string)filters.Where(a => a.Field.Equals("AttachmentNo")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("StartDate")).Any())
                    {
                        filter.StartDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("StartDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("EndDate")).Any())
                    {
                        filter.EndDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("EndDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("DateOfUnderlying")).Any())
                    {
                        filter.DateOfUnderlying = DateTime.Parse((string)filters.Where(a => a.Field.Equals("DateOfUnderlying")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("ExpiredDate")).Any())
                    {
                        filter.ExpiredDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("ExpiredDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    filter.CIF = (string)filters.Where(a => a.Field.Equals("CIF")).Select(a => a.Value).SingleOrDefault();
                    filter.ReferenceNumber = (string)filters.Where(a => a.Field.Equals("ReferenceNumber")).Select(a => a.Value).SingleOrDefault();
                    filter.SupplierName = (string)filters.Where(a => a.Field.Equals("SupplierName")).Select(a => a.Value).SingleOrDefault();
                    filter.InvoiceNumber = (string)filters.Where(a => a.Field.Equals("InvoiceNumber")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }
                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.CustomerUnderlyings
                                join b in context.TransactionUnderlyings on a.UnderlyingID equals b.UnderlyingID

                                let isCIF = !string.IsNullOrEmpty(filter.CIF)
                                let isUnderlyingDocument = !string.IsNullOrEmpty(filter.UnderlyingDocument.Name)
                                let isDocumentType = !string.IsNullOrEmpty(filter.DocumentType.Name)
                                let isCurrency = !string.IsNullOrEmpty(filter.Currency.Code)
                                let isStatementLetter = !string.IsNullOrEmpty(filter.StatementLetter.Name)
                                let isAmount = filter.Amount != null && filter.Amount != 0 ? true : false
                                let isAttachmentNo = !string.IsNullOrEmpty(filter.AttachmentNo)
                                let isStartDate = filter.StartDate.HasValue ? true : false
                                let isEndDate = filter.EndDate.HasValue ? true : false
                                let isDateOfUnderlying = filter.DateOfUnderlying.HasValue ? true : false
                                let isExpiredDate = filter.ExpiredDate.HasValue ? true : false
                                let isReferenceNumber = !string.IsNullOrEmpty(filter.ReferenceNumber)
                                let isSupplierName = !string.IsNullOrEmpty(filter.SupplierName)
                                let isInvoiceNumber = filter.InvoiceNumber != null ? true : false
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) && b.TransactionID == id &&
                                    (isCIF ? a.CIF.Equals(filter.CIF) : true) &&
                                    (isUnderlyingDocument ? a.UnderlyingDocument.UnderlyingDocName.Contains(filter.UnderlyingDocument.Name) : true) &&
                                    (isDocumentType ? a.DocumentType.DocTypeName.Contains(filter.DocumentType.Name) : true) &&
                                    (isCurrency ? a.Currency.CurrencyCode.Contains(filter.Currency.Code) : true) &&
                                    (isStatementLetter ? a.StatementLetter.StatementLetterName.Contains(filter.StatementLetter.Name) : true) &&
                                    (isAmount ? a.Amount.Equals(filter.Amount) : true) &&
                                    (isAttachmentNo ? a.AttachmentNo.Contains(filter.AttachmentNo) : true) &&
                                    (isStartDate ? a.StartDate.Equals(filter.StartDate) : true) &&
                                    (isEndDate ? a.EndDate.Equals(filter.EndDate) : true) &&
                                    (isDateOfUnderlying ? a.DateOfUnderlying.Equals(filter.DateOfUnderlying) : true) &&
                                    (isExpiredDate ? a.ExpiredDate.Equals(filter.ExpiredDate) : true) &&
                                    (isReferenceNumber ? a.ReferenceNumber.Contains(filter.ReferenceNumber) : true) &&
                                    (isSupplierName ? a.SupplierName.Contains(filter.SupplierName) : true) &&
                                    (isInvoiceNumber ? a.InvoiceNumber.Contains(filter.InvoiceNumber) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)

                                select new CustomerUnderlyingModel
                                {
                                    ID = a.UnderlyingID,
                                    CIF = a.CIF,
                                    CustomerName = a.Customer.CustomerName,
                                    UtilizationAmount = b.Amount,
                                    UnderlyingDocument = new UnderlyingDocModel
                                    {
                                        ID = a.UnderlyingDocument.UnderlyingDocID,
                                        Code = a.UnderlyingDocument.UnderlyingDocCode,
                                        Name = a.UnderlyingDocument.UnderlyingDocName,
                                        LastModifiedBy = a.UnderlyingDocument.UpdateDate == null ? a.UnderlyingDocument.CreateBy : a.UnderlyingDocument.UpdateBy,
                                        LastModifiedDate = a.UnderlyingDocument.UpdateDate == null ? a.UnderlyingDocument.CreateDate : a.UnderlyingDocument.UpdateDate.Value
                                    },
                                    DocumentType = new DocumentTypeModel
                                    {
                                        ID = a.DocumentType.DocTypeID,
                                        Name = a.DocumentType.DocTypeName,
                                        Description = a.DocumentType.DocTypeDescription,
                                        LastModifiedBy = a.DocumentType.UpdateDate == null ? a.DocumentType.CreateBy : a.DocumentType.UpdateBy,
                                        LastModifiedDate = a.DocumentType.UpdateDate == null ? a.DocumentType.CreateDate : a.DocumentType.UpdateDate.Value
                                    },
                                    Currency = new CurrencyModel
                                    {
                                        ID = a.Currency.CurrencyID,
                                        Code = a.Currency.CurrencyCode,
                                        LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                        LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                    },
                                    StatementLetter = new StatementLetterModel
                                    {
                                        ID = a.StatementLetter.StatementLetterID,
                                        Name = a.StatementLetter.StatementLetterName,
                                        LastModifiedBy = a.StatementLetter.UpdateDate == null ? a.StatementLetter.CreateBy : a.StatementLetter.UpdateBy,
                                        LastModifiedDate = a.StatementLetter.UpdateDate == null ? a.StatementLetter.CreateDate : a.StatementLetter.UpdateDate.Value
                                    },
                                    Amount = a.Amount,
                                    AvailableAmount = a.AvailableAmountUSD == null ? 0 : a.AvailableAmountUSD,
                                    AttachmentNo = a.AttachmentNo,
                                    IsDeclarationOfException = a.IsDeclarationOfExecption,
                                    StartDate = a.StartDate,
                                    EndDate = a.EndDate,
                                    DateOfUnderlying = a.DateOfUnderlying,
                                    ExpiredDate = a.ExpiredDate,
                                    ReferenceNumber = a.ReferenceNumber,
                                    SupplierName = a.SupplierName,
                                    IsUtilize = a.IsUtilize,
                                    IsProforma = true,
                                    IsSelectedProforma = true,
                                    IsSelectedBulk = true,
                                    IsBulkUnderlying = true,
                                    InvoiceNumber = a.InvoiceNumber,
                                    OtherUnderlying = a.OtherUnderlying,
                                    IsJointAccount = a.IsJointAccount.HasValue ? a.IsJointAccount.Value : false,
                                    IsTMO = a.IsTMO.HasValue ? a.IsTMO.Value : false,
                                    AccountNumber = a.AccountNumber,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    CustomerUnderlying.Page = page;
                    CustomerUnderlying.Size = size;
                    CustomerUnderlying.Total = data.Count();
                    CustomerUnderlying.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    CustomerUnderlying.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new CustomerUnderlyingRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            CIF = a.CIF,
                            CustomerName = a.CustomerName,
                            UnderlyingDocument = a.UnderlyingDocument,
                            DocumentType = a.DocumentType,
                            Currency = a.Currency,
                            Amount = a.Amount,
                            AvailableAmount = a.AvailableAmount.HasValue ? (decimal?)Decimal.Round(a.AvailableAmount.Value, 2) : null,
                            AttachmentNo = a.AttachmentNo,
                            StatementLetter = a.StatementLetter,
                            IsDeclarationOfException = a.IsDeclarationOfException,
                            StartDate = a.StartDate,
                            EndDate = a.EndDate,
                            DateOfUnderlying = a.DateOfUnderlying,
                            ExpiredDate = a.ExpiredDate,
                            ReferenceNumber = a.ReferenceNumber,
                            SupplierName = a.SupplierName,
                            InvoiceNumber = a.InvoiceNumber,
                            IsSelectedProforma = a.IsSelectedProforma,
                            IsProforma = a.IsProforma,
                            IsSelectedBulk = a.IsSelectedBulk,
                            IsBulkUnderlying = a.IsBulkUnderlying,
                            IsUtilize = a.IsUtilize,
                            IsEnable = a.IsUtilize,
                            OtherUnderlying = a.OtherUnderlying,
                            IsJointAccount = a.IsJointAccount,
                            IsTMO = a.IsTMO,
                            AccountNumber = a.AccountNumber,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate,
                            UtilizationAmount = a.UtilizationAmount
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool GetCustomerUnderlyingByID(long id, int page, int size, IList<CustomerUnderlyingFilter> filters, string sortColumn, string sortOrder, ref CustomerUnderlyingGrid CustomerUnderlying, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                CustomerUnderlyingModel filter = new CustomerUnderlyingModel();
                if (filters != null)
                {
                    var parentId = filters.Where(a => a.Field.Equals("ParentID")).Select(a => a.Value).SingleOrDefault();
                    filter.ParentID = (parentId != null ? long.Parse((string)parentId) : 0);
                    filter.UnderlyingDocument = new UnderlyingDocModel { Name = (string)filters.Where(a => a.Field.Equals("UnderlyingDocument")).Select(a => a.Value).SingleOrDefault() };
                    filter.DocumentType = new DocumentTypeModel { Name = (string)filters.Where(a => a.Field.Equals("DocumentType")).Select(a => a.Value).SingleOrDefault() };
                    filter.Currency = new CurrencyModel { Code = (string)filters.Where(a => a.Field.Equals("Currency")).Select(a => a == null ? "0" : a.Value).SingleOrDefault() };
                    filter.StatementLetter = new StatementLetterModel { Name = (string)filters.Where(a => a.Field.Equals("StatementLetter")).Select(a => a.Value).SingleOrDefault() };
                    filter.Amount = Convert.ToDecimal((string)filters.Where(a => a.Field.Equals("Amount")).Select(a => a == null ? "0" : a.Value).SingleOrDefault());
                    filter.AttachmentNo = (string)filters.Where(a => a.Field.Equals("AttachmentNo")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("StartDate")).Any())
                    {
                        filter.StartDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("StartDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("EndDate")).Any())
                    {
                        filter.EndDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("EndDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("DateOfUnderlying")).Any())
                    {
                        filter.DateOfUnderlying = DateTime.Parse((string)filters.Where(a => a.Field.Equals("DateOfUnderlying")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("ExpiredDate")).Any())
                    {
                        filter.ExpiredDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("ExpiredDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    filter.CIF = (string)filters.Where(a => a.Field.Equals("CIF")).Select(a => a.Value).SingleOrDefault();
                    filter.ReferenceNumber = (string)filters.Where(a => a.Field.Equals("ReferenceNumber")).Select(a => a.Value).SingleOrDefault();
                    filter.SupplierName = (string)filters.Where(a => a.Field.Equals("SupplierName")).Select(a => a.Value).SingleOrDefault();
                    filter.InvoiceNumber = (string)filters.Where(a => a.Field.Equals("InvoiceNumber")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }
                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.CustomerUnderlyings
                                join b in context.TransactionDealUnderlyings on a.UnderlyingID equals b.UnderlyingID
                                where a.IsDeleted.Equals(false) && b.TransactionDealID == id
                                select new CustomerUnderlyingModel
                                {

                                    ID = a.UnderlyingID,
                                    CIF = a.CIF,
                                    CustomerName = a.Customer.CustomerName,
                                    UtilizationAmount = b.Amount,
                                    UnderlyingDocument = new UnderlyingDocModel
                                    {
                                        ID = a.UnderlyingDocument.UnderlyingDocID,
                                        Code = a.UnderlyingDocument.UnderlyingDocCode,
                                        Name = a.UnderlyingDocument.UnderlyingDocName,
                                        LastModifiedBy = a.UnderlyingDocument.UpdateDate == null ? a.UnderlyingDocument.CreateBy : a.UnderlyingDocument.UpdateBy,
                                        LastModifiedDate = a.UnderlyingDocument.UpdateDate == null ? a.UnderlyingDocument.CreateDate : a.UnderlyingDocument.UpdateDate.Value
                                    },
                                    DocumentType = new DocumentTypeModel
                                    {
                                        ID = a.DocumentType.DocTypeID,
                                        Name = a.DocumentType.DocTypeName,
                                        Description = a.DocumentType.DocTypeDescription,
                                        LastModifiedBy = a.DocumentType.UpdateDate == null ? a.DocumentType.CreateBy : a.DocumentType.UpdateBy,
                                        LastModifiedDate = a.DocumentType.UpdateDate == null ? a.DocumentType.CreateDate : a.DocumentType.UpdateDate.Value
                                    },
                                    Currency = new CurrencyModel
                                    {
                                        ID = a.Currency.CurrencyID,
                                        Code = a.Currency.CurrencyCode,
                                        LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                        LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                    },
                                    StatementLetter = new StatementLetterModel
                                    {
                                        ID = a.StatementLetter.StatementLetterID,
                                        Name = a.StatementLetter.StatementLetterName,
                                        LastModifiedBy = a.StatementLetter.UpdateDate == null ? a.StatementLetter.CreateBy : a.StatementLetter.UpdateBy,
                                        LastModifiedDate = a.StatementLetter.UpdateDate == null ? a.StatementLetter.CreateDate : a.StatementLetter.UpdateDate.Value
                                    },
                                    Amount = a.Amount,
                                    AvailableAmount = a.AvailableAmountUSD == null ? 0 : a.AvailableAmountUSD,
                                    AttachmentNo = a.AttachmentNo,
                                    IsDeclarationOfException = a.IsDeclarationOfExecption,
                                    StartDate = a.StartDate,
                                    EndDate = a.EndDate,
                                    DateOfUnderlying = a.DateOfUnderlying,
                                    ExpiredDate = a.ExpiredDate,
                                    ReferenceNumber = a.ReferenceNumber,
                                    SupplierName = a.SupplierName,
                                    IsUtilize = a.IsUtilize,
                                    IsProforma = true,
                                    IsSelectedProforma = true,
                                    IsBulkUnderlying = true,
                                    IsSelectedBulk = true,
                                    InvoiceNumber = a.InvoiceNumber,
                                    OtherUnderlying = a.OtherUnderlying,
                                    IsJointAccount = a.IsJointAccount.HasValue ? a.IsJointAccount.Value : false,
                                    IsTMO = a.IsTMO.HasValue ? a.IsTMO.Value : false,
                                    AccountNumber = a.AccountNumber,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    CustomerUnderlying.Page = page;
                    CustomerUnderlying.Size = size;
                    CustomerUnderlying.Total = data.Count();
                    CustomerUnderlying.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    CustomerUnderlying.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new CustomerUnderlyingRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            CIF = a.CIF,
                            CustomerName = a.CustomerName,
                            UnderlyingDocument = a.UnderlyingDocument,
                            DocumentType = a.DocumentType,
                            Currency = a.Currency,
                            Amount = a.Amount,
                            AvailableAmount = a.AvailableAmount.HasValue ? (decimal?)Decimal.Round(a.AvailableAmount.Value, 2) : null,
                            AttachmentNo = a.AttachmentNo,
                            StatementLetter = a.StatementLetter,
                            IsDeclarationOfException = a.IsDeclarationOfException,
                            StartDate = a.StartDate,
                            EndDate = a.EndDate,
                            DateOfUnderlying = a.DateOfUnderlying,
                            ExpiredDate = a.ExpiredDate,
                            ReferenceNumber = a.ReferenceNumber,
                            SupplierName = a.SupplierName,
                            InvoiceNumber = a.InvoiceNumber,
                            IsSelectedProforma = a.IsSelectedProforma,
                            IsProforma = a.IsProforma,
                            IsBulkUnderlying = a.IsBulkUnderlying,
                            IsSelectedBulk = a.IsSelectedBulk,
                            IsUtilize = a.IsUtilize,
                            IsEnable = a.IsUtilize,
                            OtherUnderlying = a.OtherUnderlying,
                            IsJointAccount = a.IsJointAccount,
                            IsTMO = a.IsTMO,
                            AccountNumber = a.AccountNumber,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate,
                            UtilizationAmount = a.UtilizationAmount,
                            TransactionID = id
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCustomerUnderlyingAttach(IList<long> UnderlyingsID, string cif, ref List<CustomerUnderlyingModel> customerUnderlying, ref string message)
        {
            bool isSuccess = false;

            try
            {
                DateTime currentDate = DateTime.Now;
                currentDate = currentDate.AddHours(-7);

                using (DBSEntities context = new DBSEntities())
                {
                    customerUnderlying = (from a in context.CustomerUnderlyings
                                          where a.IsDeleted.Equals(false) && a.IsAttachFile.Equals(false) &&
                                          a.CIF == cif &&
                                              //a.ExpiredDate >= currentDate &&
                                           (from x in UnderlyingsID select x).Contains(a.UnderlyingID)
                                          select new CustomerUnderlyingModel
                                          {
                                              ID = a.UnderlyingID,
                                              CIF = a.CIF,
                                              UnderlyingDocument = new UnderlyingDocModel
                                              {
                                                  ID = a.UnderlyingDocument.UnderlyingDocID,
                                                  Code = a.UnderlyingDocument.UnderlyingDocCode,
                                                  Name = a.UnderlyingDocument.UnderlyingDocName,
                                                  LastModifiedBy = a.UnderlyingDocument.UpdateDate == null ? a.UnderlyingDocument.CreateBy : a.UnderlyingDocument.UpdateBy,
                                                  LastModifiedDate = a.UnderlyingDocument.UpdateDate == null ? a.UnderlyingDocument.CreateDate : a.UnderlyingDocument.UpdateDate.Value
                                              },
                                              DocumentType = new DocumentTypeModel
                                              {
                                                  ID = a.DocumentType.DocTypeID,
                                                  Name = a.DocumentType.DocTypeName,
                                                  Description = a.DocumentType.DocTypeDescription,
                                                  LastModifiedBy = a.DocumentType.UpdateDate == null ? a.DocumentType.CreateBy : a.DocumentType.UpdateBy,
                                                  LastModifiedDate = a.DocumentType.UpdateDate == null ? a.DocumentType.CreateDate : a.DocumentType.UpdateDate.Value
                                              },
                                              Currency = new CurrencyModel
                                              {
                                                  ID = a.Currency.CurrencyID,
                                                  Code = a.Currency.CurrencyCode,
                                                  LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                                  LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                              },
                                              StatementLetter = new StatementLetterModel
                                              {
                                                  ID = a.StatementLetter.StatementLetterID,
                                                  Name = a.StatementLetter.StatementLetterName,
                                                  LastModifiedBy = a.StatementLetter.UpdateDate == null ? a.StatementLetter.CreateBy : a.StatementLetter.UpdateBy,
                                                  LastModifiedDate = a.StatementLetter.UpdateDate == null ? a.StatementLetter.CreateDate : a.StatementLetter.UpdateDate.Value
                                              },
                                              Amount = a.Amount,
                                              AvailableAmount = a.AvailableAmountUSD == null ? 0 : a.AvailableAmountUSD,
                                              AttachmentNo = a.AttachmentNo,
                                              IsDeclarationOfException = a.IsDeclarationOfExecption,
                                              StartDate = a.StartDate,
                                              EndDate = a.EndDate,
                                              DateOfUnderlying = a.DateOfUnderlying,
                                              ExpiredDate = a.ExpiredDate,
                                              ReferenceNumber = a.ReferenceNumber,
                                              SupplierName = a.SupplierName,
                                              IsProforma = a.IsProforma,
                                              IsSelectedProforma = a.IsSelectedProforma,
                                              IsSelectedBulk = a.IsSelectedBulk == null ? false : a.IsSelectedBulk,
                                              IsBulkUnderlying = a.IsBulkUnderlying == null ? false : a.IsBulkUnderlying,
                                              InvoiceNumber = a.InvoiceNumber,
                                              IsSelectedAttach = a.IsAttachFile,
                                              OtherUnderlying = a.OtherUnderlying,
                                              IsJointAccount = a.IsJointAccount.HasValue ? a.IsJointAccount.Value : false,
                                              IsTMO = a.IsTMO.HasValue ? a.IsTMO.Value : false,
                                              AccountNumber = a.AccountNumber,
                                              LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                              LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                          }).ToList<CustomerUnderlyingModel>();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCustomerUnderlyingUnionDeal(long id, int page, int size, IList<CustomerUnderlyingFilter> filters, string sortColumn, string sortOrder, ref CustomerUnderlyingGrid CustomerUnderlyingGrid, ref string message)
        {
            bool isSuccess = false;
            DateTime currentdate = DateTime.Now;
            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                CustomerUnderlyingModel filter = new CustomerUnderlyingModel();
                if (filters != null)
                {
                    var parentId = filters.Where(a => a.Field.Equals("ParentID")).Select(a => a.Value).SingleOrDefault();
                    bool isTMO_ = filters.Where(a => a.Field.Equals("IsTMO")).Select(a => a == null ? false : true).SingleOrDefault();
                    filter.ParentID = (parentId != null ? long.Parse((string)parentId) : 0);
                    filter.UnderlyingDocument = new UnderlyingDocModel { Name = (string)filters.Where(a => a.Field.Equals("UnderlyingDocument")).Select(a => a.Value).SingleOrDefault() };
                    filter.DocumentType = new DocumentTypeModel { Name = (string)filters.Where(a => a.Field.Equals("DocumentType")).Select(a => a.Value).SingleOrDefault() };
                    filter.Currency = new CurrencyModel { Code = (string)filters.Where(a => a.Field.Equals("Currency")).Select(a => a == null ? "0" : a.Value).SingleOrDefault() };
                    filter.StatementLetter = new StatementLetterModel { Name = (string)filters.Where(a => a.Field.Equals("StatementLetter")).Select(a => a.Value).SingleOrDefault() };
                    filter.Amount = Convert.ToDecimal((string)filters.Where(a => a.Field.Equals("Amount")).Select(a => a == null ? "0" : a.Value).SingleOrDefault());
                    filter.AttachmentNo = (string)filters.Where(a => a.Field.Equals("AttachmentNo")).Select(a => a.Value).SingleOrDefault();
                    string statusData = (string)filters.Where(a => a.Field.Equals("StatusShowData")).Select(a => a.Value).SingleOrDefault();
                    filter.StatusShowData = statusData != null ? int.Parse(statusData) : 0;
                    if (filters.Where(a => a.Field.Equals("StartDate")).Any())
                    {
                        filter.StartDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("StartDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("EndDate")).Any())
                    {
                        filter.EndDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("EndDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("DateOfUnderlying")).Any())
                    {
                        filter.DateOfUnderlying = DateTime.Parse((string)filters.Where(a => a.Field.Equals("DateOfUnderlying")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("ExpiredDate")).Any())
                    {
                        filter.ExpiredDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("ExpiredDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    filter.CIF = (string)filters.Where(a => a.Field.Equals("CIF")).Select(a => a.Value).SingleOrDefault();
                    filter.ReferenceNumber = (string)filters.Where(a => a.Field.Equals("ReferenceNumber")).Select(a => a.Value).SingleOrDefault();
                    filter.SupplierName = (string)filters.Where(a => a.Field.Equals("SupplierName")).Select(a => a.Value).SingleOrDefault();
                    filter.InvoiceNumber = (string)filters.Where(a => a.Field.Equals("InvoiceNumber")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                    filter.IsTMO = isTMO_ ? true : false;
                }
                var TransactionDealUnderlying = context.TransactionDealUnderlyings.Where(x => x.TransactionDealID == id && x.IsDeleted.Equals(false));
                var data = (from cu in context.CustomerUnderlyings.Where(x => x.CIF == filter.CIF)
                            join trans in TransactionDealUnderlying on cu.UnderlyingID equals trans.UnderlyingID into grp
                            from subTrans in grp.DefaultIfEmpty()
                            let isStatusShow = filter.StatusShowData != null ? true : false
                            let isTMO = filter.IsTMO.Value ? true : false
                            where cu.IsDeleted.Equals(false) && (DbFunctions.TruncateTime(cu.ExpiredDate) >= DbFunctions.TruncateTime(currentdate) || subTrans != null)
                            && ((cu.AvailableAmountUSD > 0 || cu.StatementLetterID == 4) || subTrans != null)
                            && (cu.IsSelectedBulk == null || cu.IsSelectedBulk.Value == false)
                            && (isTMO ? cu.IsTMO.Value.Equals(filter.IsTMO.Value) : true)
                            select new CustomerUnderlyingModel
                            {
                                ID = cu.UnderlyingID,
                                CIF = cu.CIF,
                                CustomerName = cu.Customer.CustomerName,
                                UnderlyingDocument = new UnderlyingDocModel
                                {
                                    ID = cu.UnderlyingDocument.UnderlyingDocID,
                                    Code = cu.UnderlyingDocument.UnderlyingDocCode,
                                    Name = cu.UnderlyingDocument.UnderlyingDocName,
                                    LastModifiedBy = cu.UnderlyingDocument.UpdateDate == null ? cu.UnderlyingDocument.CreateBy : cu.UnderlyingDocument.UpdateBy,
                                    LastModifiedDate = cu.UnderlyingDocument.UpdateDate == null ? cu.UnderlyingDocument.CreateDate : cu.UnderlyingDocument.UpdateDate.Value
                                },
                                DocumentType = new DocumentTypeModel
                                {
                                    ID = cu.DocumentType.DocTypeID,
                                    Name = cu.DocumentType.DocTypeName,
                                    Description = cu.DocumentType.DocTypeDescription,
                                    LastModifiedBy = cu.DocumentType.UpdateDate == null ? cu.DocumentType.CreateBy : cu.DocumentType.UpdateBy,
                                    LastModifiedDate = cu.DocumentType.UpdateDate == null ? cu.DocumentType.CreateDate : cu.DocumentType.UpdateDate.Value
                                },
                                Currency = new CurrencyModel
                                {
                                    ID = cu.Currency.CurrencyID,
                                    Code = cu.Currency.CurrencyCode,
                                    LastModifiedBy = cu.Currency.UpdateDate == null ? cu.Currency.CreateBy : cu.Currency.UpdateBy,
                                    LastModifiedDate = cu.Currency.UpdateDate == null ? cu.Currency.CreateDate : cu.Currency.UpdateDate.Value
                                },
                                StatementLetter = new StatementLetterModel
                                {
                                    ID = cu.StatementLetter.StatementLetterID,
                                    Name = cu.StatementLetter.StatementLetterName,
                                    LastModifiedBy = cu.StatementLetter.UpdateDate == null ? cu.StatementLetter.CreateBy : cu.StatementLetter.UpdateBy,
                                    LastModifiedDate = cu.StatementLetter.UpdateDate == null ? cu.StatementLetter.CreateDate : cu.StatementLetter.UpdateDate.Value
                                },
                                Amount = cu.Amount,
                                //AvailableAmount = cu.AvailableAmountUSD == null ? 0 : cu.AvailableAmountUSD,
                                AvailableAmount = subTrans == null ? (cu.AvailableAmountUSD.HasValue ? cu.AvailableAmountUSD.Value : 0) : cu.AvailableAmountUSD.Value + subTrans.Amount,
                                UtilizeAmountDeal = subTrans.AvailableAmount,
                                AvailableAmountDeal = subTrans.AvailableAmount,
                                AttachmentNo = cu.AttachmentNo,
                                IsDeclarationOfException = cu.IsDeclarationOfExecption,
                                StartDate = cu.StartDate,
                                EndDate = cu.EndDate,
                                DateOfUnderlying = cu.DateOfUnderlying,
                                ExpiredDate = cu.ExpiredDate,
                                ReferenceNumber = cu.ReferenceNumber,
                                SupplierName = cu.SupplierName,
                                IsUtilize = cu.IsUtilize,
                                IsProforma = cu.IsProforma,
                                IsSelectedProforma = cu.IsSelectedProforma,
                                IsSelectedBulk = cu.IsSelectedBulk == null ? false : cu.IsSelectedBulk,
                                IsBulkUnderlying = cu.IsBulkUnderlying == null ? false : cu.IsBulkUnderlying,
                                IsEnable = (subTrans == null ? false : true),
                                InvoiceNumber = cu.InvoiceNumber,
                                OtherUnderlying = cu.OtherUnderlying,
                                IsJointAccount = cu.IsJointAccount.HasValue ? cu.IsJointAccount.Value : false,
                                IsTMO = cu.IsTMO.HasValue ? cu.IsTMO.Value : false,
                                AccountNumber = cu.AccountNumber,
                                LastModifiedBy = cu.UpdateDate == null ? cu.CreateBy : cu.UpdateBy,
                                LastModifiedDate = cu.UpdateDate == null ? cu.CreateDate : cu.UpdateDate.Value
                            });


                CustomerUnderlyingGrid.Page = page;
                CustomerUnderlyingGrid.Size = size;
                CustomerUnderlyingGrid.Total = data.Count();
                CustomerUnderlyingGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                CustomerUnderlyingGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                    .Select((a, i) => new CustomerUnderlyingRow
                    {
                        RowID = i + 1,
                        ID = a.ID,
                        CIF = a.CIF,
                        CustomerName = a.CustomerName,
                        UnderlyingDocument = a.UnderlyingDocument,
                        DocumentType = a.DocumentType,
                        Currency = a.Currency,
                        Amount = a.Amount,
                        AvailableAmount = a.AvailableAmount.HasValue ? (decimal?)Decimal.Round(a.AvailableAmount.Value, 2) : null,
                        UtilizeAmountDeal = a.UtilizeAmountDeal.HasValue ? a.UtilizeAmountDeal : a.AvailableAmount,
                        AvailableAmountDeal = a.AvailableAmountDeal.HasValue ? a.AvailableAmountDeal : a.AvailableAmount,
                        AttachmentNo = a.AttachmentNo,
                        StatementLetter = a.StatementLetter,
                        IsDeclarationOfException = a.IsDeclarationOfException,
                        StartDate = a.StartDate,
                        EndDate = a.EndDate,
                        DateOfUnderlying = a.DateOfUnderlying,
                        ExpiredDate = a.ExpiredDate,
                        ReferenceNumber = a.ReferenceNumber,
                        SupplierName = a.SupplierName,
                        InvoiceNumber = a.InvoiceNumber,
                        IsSelectedProforma = a.IsSelectedProforma,
                        IsProforma = a.IsProforma,
                        IsSelectedBulk = a.IsSelectedBulk,
                        IsBulkUnderlying = a.IsBulkUnderlying,
                        IsUtilize = a.IsUtilize,
                        IsEnable = a.IsUtilize,
                        OtherUnderlying = a.OtherUnderlying,
                        IsJointAccount = a.IsJointAccount,
                        IsTMO = a.IsTMO,
                        AccountNumber = a.AccountNumber,
                        LastModifiedBy = a.LastModifiedBy,
                        LastModifiedDate = a.LastModifiedDate,
                        UtilizationAmount = a.UtilizationAmount
                    })
                    .Skip(skip)
                    .Take(size)
                    .ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool GetUnderlyingHistoryByUnderlyingID(long underlyingID, ref IList<CustomerUnderlyingHistoryModel> CustomerUnderlyingHistory, ref string message)
        {
            bool isSuccess = false;

            try
            {
                context.Transactions.AsNoTracking();

                CustomerUnderlyingHistory = (from a in context.SP_GetUnderlyingHistory(underlyingID)
                                             select new CustomerUnderlyingHistoryModel
                                             {
                                                 UnderlyingID = a.UnderlyingID,
                                                 Amount = a.Amount,
                                                 AmountUSD = a.AmountUSD,
                                                 AvailableAmountUSD = a.AvailableAmountUSD,
                                                 DateOfUnderlying = a.DateOfUnderlying,
                                                 SupplierName = a.SupplierName,
                                                 InvoiceNumber = a.InvoiceNumber,
                                                 AccountNumber = a.AccountNumber,
                                                 IsJointAccount = a.IsJointAccount,
                                                 AttachmentNo = a.AttachmentNo,
                                                 IsExpired = a.IsExpired,
                                                 StatementLetterName = a.StatementLetterName,
                                                 UnderlyingDocName = a.UnderlyingDocName,
                                                 DocTypeName = a.DocTypeName,
                                                 CurrencyCode = a.CurrencyCode,
                                                 UpdateDate = a.UpdateDate,
                                                 UpdateBy = a.UpdateBy
                                             }).Distinct().ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetUnderlyingTransactionHistoryByUnderlyingID(long underlyingID, ref IList<CustomerUnderlyingTransactionHistoryModel> CustomerUnderlyingTransactionHistory, ref string message)
        {
            bool isSuccess = false;

            try
            {
                context.Transactions.AsNoTracking();

                CustomerUnderlyingTransactionHistory = (from a in context.SP_GetUnderlyingTransactionHistory(underlyingID)
                                                        orderby a.UpdateDate descending
                                                        select new CustomerUnderlyingTransactionHistoryModel
                                                        {
                                                            UnderlyingID = a.UnderlyingID,
                                                            ApplicationID = a.ApplicationID,
                                                            ApplicationDate = a.ApplicationDate,
                                                            ProductCode = a.ProductCode,
                                                            CurrencyCode = a.CurrencyCode,
                                                            Amount = a.Amount,
                                                            AmountUSD = a.AmountUSD,
                                                            UtilizedAmountInUSD = a.UtilizedAmountInUSD,
                                                            TransactionStatus = a.TransactionStatus,
                                                            UpdateDate = a.UpdateDate
                                                        }).Distinct().ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
    }
    #endregion
}