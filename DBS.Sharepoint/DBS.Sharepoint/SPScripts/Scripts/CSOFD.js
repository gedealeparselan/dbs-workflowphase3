var accessToken;
var today = Date.now();
var DocumentModels = ko.observable({ FileName: '', DocumentPath: '' });
var SearchByTypes = ko.observableArray([{ ID: 1, Name: "Upload Date" }, { ID: 2, Name: "Rollover Date" }]);
var saveFlag = false;
var SPRole = {
    ID: ko.observable(),
    Name: ko.observable()
};

var SPUser = {
    DisplayName: ko.observable(),
    Email: ko.observable(),
    ID: ko.observable(),
    LoginName: ko.observable(),
    Roles: ko.observableArray([SPRole])
};

var DecodeLogin = function (username) {
    var index = username.lastIndexOf("|");
    var str = username.substring(index + 1);
    return str;
}

var FDCSOModel = {
    ID: ko.observable(),
    ApplicationDate: ko.observable(),
    CreateDate: ko.observable(new Date),
    Documents: ko.observableArray([])
};

var DocumentTypeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var DocumentPurposeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var DocumentModel = {
    ID: ko.observable(),
    Type: ko.observable(DocumentTypeModel),
    Purpose: ko.observable(DocumentPurposeModel),
    FileName: ko.observable(),
    DocumentPath: ko.observable(),
    LastModifiedDate: ko.observable(new Date),
    LastModifiedBy: ko.observable()
};

var Parameter = {
    DocumentTypes: ko.observableArray([DocumentTypeModel]),
    DocumentPurposes: ko.observableArray([DocumentPurposeModel])
};


var ViewModel = function () {
    var self = this;

    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);
        if (date == '1970/01/01 00:00:00' || date == null) {
            return "";
        }

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return "";
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-17
    };

    self.LocalDate2 = function (date, isDateOnly, isDateLong) {
        var localDate = new Date(date);
        if (date == '1970/01/01 00:00:00' || date == null) {
            return "";
        }

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format("DD-MM-YYYY");
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return "";
        }
    };;

    self.IsRole = function (name) {
        var result = false;
        $.each($.cookie(api.cookie.spUser).Roles, function (index, item) {
            if (Const_RoleName[item.ID] == name) result = true; //if (item.Name == name) result = true;
        });
        return result;
    };
    self.SPUser = ko.observable();
    //Document
    self.Documents = ko.observableArray([]);

    // Temporary documents
    self.Documents = ko.observableArray([]);
    self.DocumentPath = ko.observable();
    self.DocumentType = ko.observable();
    self.DocumentPurpose = ko.observable();
    self.DocumentFileName = ko.observable();

    // input form controls
    self.IsEditable = ko.observable(false);

    self.Parameter = ko.observable(Parameter);
    self.InterestSearchBy = ko.observable();

    // Add new document handler
    self.AddDocument = function () {
        var doc = {
            ID: 0,
            Type: self.DocumentType(),
            Purpose: self.DocumentPurpose(),
            FileName: self.DocumentFileName(),
            DocumentPath: self.DocumentPath(),
            LastModifiedDate: new Date(),
            LastModifiedBy: null
        };

        if (doc.Type == null || doc.Purpose == null || doc.DocumentPath == null) {
            alert("Please complete the upload form fields.")
        } else {
            self.Documents.push(doc);

            $('.remove').click();
            // hide upload dialog
            $("#modal-form-upload").modal('hide');
        }

    };
    self.SearchDate = ko.observable(self.LocalDate(new Date, true, false));
    self.EditDocument = function (data) {
        self.DocumentPath(data.DocumentPath);
        self.Selected().DocumentType(data.Type.ID);
        self.Selected().DocumentPurpose(data.Purpose.ID);

        // show upload dialog
        $("#modal-form-upload").modal('show');
    };

    self.RemoveDocument = function (data) {
        //alert(JSON.stringify(data))
        self.Documents.remove(data);
    };

    self.UploadDocument = function () {
        self.DocumentPath(null);
        self.DocumentType(null);
        self.DocumentPurpose(null);
        self.DocumentType(null);
        self.DocumentPurpose(null);
        $("#modal-form-upload").modal('show');
        $('.remove').click();
    }

    self.save_u = function () {
        saveFlag = false;
        var form = $("#aspnetForm");
        form.validate();
        var $container = $("#dataTable");        
        var ht = $container.handsontable('getInstance')
        var data = ht.getData();
        var IsSelected = false;
        var $container_ = $("#dataTable2");
        var data_worksheet_tambahan = $container_.data('handsontable').getData();
        //var data = $container.data('handsontable').getData();
        //console.log(data);
        if (data.length > 0) {
            var selectedCount = 0;                 

            for (var i = 0; i < data.length; i++) {
                var specialFTP = ht.getDataAtRowProp(i, "SpecialFTP");
                var newIntRate = ht.getDataAtRowProp(i, "NewIntRate");
                var intMargin = ht.getDataAtRowProp(i, "InterestMargin");
                var isSelected = ht.getDataAtRowProp(i, "IsSelected");
                var ftpRateItem = ht.getDataAtRowProp(i, "FTPRate");
                var tenorData = ht.getDataAtRowProp(i, "Tenor");

                if (isSelected == true) {
                    selectedCount++;
                }

                if (isSelected === true && specialFTP === true) {
                    if (newIntRate === undefined || newIntRate === "" || newIntRate === null || parseFloat(newIntRate) < 0
                        || intMargin === undefined || intMargin === "" || intMargin === null || parseFloat(intMargin) < 0) {
                        //validasi new int rate dan int margin wajib
                        ShowNotification("Attention", "Interest Margin and New Int Rate are mandatory!", "gritter-warning", true);
                        saveFlag = true;
                        $container.data('handsontable').loadData($container.data('handsontable').getData());
                        $container.data('handsontable').render();
                        return;
                    }
                }
                if (isSelected === true && specialFTP !== true) {
                    if (newIntRate === undefined || newIntRate === "" || newIntRate === null || parseFloat(newIntRate) < 0) {
                        //validasi new int rate wajib
                        ShowNotification("Attention", "New Int Rate is mandatory.", "gritter-warning", true);
                        saveFlag = true;
                        $container.data('handsontable').loadData($container.data('handsontable').getData());
                        $container.data('handsontable').render();
                        return;
                    }
                }
                if (isSelected === true && (tenorData === undefined || tenorData === null || tenorData === "" || tenorData.trim() === "")) {
                    ShowNotification("Attention", "Please Select Tenor First", "gritter-warning", true);
                    saveFlag = true;
                    $container.data('handsontable').loadData($container.data('handsontable').getData());
                    $container.data('handsontable').render();
                    return;
                }
                if (isSelected === true && (newIntRate !== undefined && newIntRate !== null && newIntRate !== "") &&
                    (ftpRateItem !== undefined && ftpRateItem !== null && ftpRateItem !== "")) {
                    var floatFtpRate = parseFloat(ftpRateItem);
                    var floatNewIntRate = parseFloat(newIntRate);
                    if (floatFtpRate < floatNewIntRate) {
                        ShowNotification("Attention", "FTP Rate must be equal or greater than New Int Rate!", "gritter-warning", true);
                        saveFlag = true;
                        $container.data('handsontable').loadData($container.data('handsontable').getData());
                        $container.data('handsontable').render();
                        return;
                    }
                }
            };

            if (selectedCount == 0) {
                ShowNotification("Attention", "No data selected!", "gritter-warning", true);
                return;
            }
        }        

        data_worksheet_tambahan.pop();

        //validasi worksheet tambahan
        if (data_worksheet_tambahan !== undefined && data_worksheet_tambahan !== null && data_worksheet_tambahan.length > 0) {
            var columnName = ["CIF", "CustomerName", "AccountNo", "Currency.Code", "OriginalAmount", "RolloverDate", "MaturityDate",
                    "EmployeeID", "BranchCode", "IntRateCode", "InterestRate", "AcPrefIntCR", "NetIntRate", "InterestMargin",
                    "NewIntRate", "Tenor", "FTPRate", "SpreadRate", "NetIntAmount", "MaturityDateInstruction",
                    "ValueDateInstruction", "Date", "RenewalOption", "RepaymentAccount", "SpecialFTP", "ApprovedBy"];
            for (var i = 0 ; data_worksheet_tambahan.length > i; ++i) {
                var count = 0;
                for (var key in data_worksheet_tambahan[i]) {
                    count++;
                    if (data_worksheet_tambahan[i].hasOwnProperty(key)) {
                        if (key != "ID") {
                            if (data_worksheet_tambahan[i][key] === undefined || data_worksheet_tambahan[i][key] === null || data_worksheet_tambahan[i][key] == "") {
                                ShowNotification('Please insert data to ', key, 'gritter-warning', true);
                                return false;
                            }
                        }
                    }
                }
                if (!(count >= columnName.length)) {
                    ShowNotification('Any cell is blank ', "Please completed your data", 'gritter-warning', true);
                    return;
                }
            }
        }
        var dataGabungan = [];
        if (data != null && data_worksheet_tambahan != null) {
            if (data.length > 0 && data_worksheet_tambahan.length > 0) {
                dataGabungan = data;
                Array.prototype.push.apply(dataGabungan, data_worksheet_tambahan);
                console.log(dataGabungan);
            } else {
                if (data.length > 0) {
                    dataGabungan = data;
                    console.log(dataGabungan);
                } else if (data_worksheet_tambahan.length > 0) {
                    dataGabungan = data_worksheet_tambahan;
                    console.log(dataGabungan);
                }
            }
        } else {
            if (data != null) {
                if (data.length > 0) {
                    dataGabungan = data;
                    console.log(dataGabungan);
                }
            } else if (data_worksheet_tambahan != null) {
                if (data_worksheet_tambahan.length > 0) {
                    dataGabungan = data_worksheet_tambahan;
                    console.log(dataGabungan);
                }
            }
        }
        //cobaGabungan.pop();
        console.log(dataGabungan);


        if (form.valid()) {
            $.ajax({
                type: "POST",
                url: api.server + api.url.transactioncsosave,
                //data: ko.toJSON(GetDataFD()), //Convert the Observable Data into JSON
                data: ko.toJSON(dataGabungan),
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + accessToken
                },
                success: function (data, textStatus, jqXHR) {
                    if (jqXHR.status = 200) {

                        ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);
                        window.location = "/home";
                    } else {
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // send notification
                    //var message = (jqXHR.responseJSON.Message == "Underlying does exist.") ? 'gritter-warning' : 'gritter-error';
                    if (jqXHR.responseJSON.Message.toLowerCase().startsWith("double underlying")) {
                        ShowNotification("", jqXHR.responseJSON.Message, 'gritter-warning', true);
                    } else {
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                }
            });
        }
    };

    self.TransactionFD = ko.observable(FDCSOModel);
    
    self.Search = function () {
        if (self.InterestSearchBy() != null && self.InterestSearchBy() != undefined && self.SearchDate() != null && self.SearchDate() != undefined)
        {
            GetDataFD(self.InterestSearchBy(), moment(self.SearchDate(), "DD-MMM-YYYY").format("YYYY-MM-DD"));
        }
    };
};

var viewModel = new ViewModel();

$.holdReady(true);
var intervalRoleName = setInterval(function () {
    if (Object.keys(Const_RoleName).length != 0) {
        $.holdReady(false);
        clearInterval(intervalRoleName);
    }
}, 100);

//$(document).on("RoleNameReady", function() {
$(document).ready(function () {

    if ($.cookie(api.cookie.name) == undefined) {
        Helper.Token.Request(TokenOnSuccess, TokenOnError);
    } else {
        accessToken = $.cookie(api.cookie.name);
        if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
            spUser = $.cookie(api.cookie.spUser);
            viewModel.SPUser(ko.mapping.toJS(spUser));
        }
    }
    GetParameters();
    GetDataFD(2, moment(Date.now()).format('YYYY-MM-DD'));
    AddDataHot();

    // ace file upload
    $('#document-path').ace_file_input({
        no_file: 'No File ...',
        btn_choose: 'Choose',
        btn_change: 'Change',
        droppable: false,
        //onchange:null,
        thumbnail: false, //| true | large
        //whitelist:'gif|png|jpg|jpeg'
        blacklist: 'exe|dll'
        //onchange:''
        //
    });

    // datepicker
    $('.date-picker').datepicker({ autoclose: true }).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });

    // Knockout custom file handler
    ko.bindingHandlers.file = {
        init: function (element, valueAccessor) {
            $(element).change(function () {
                var file = this.files[0];

                if (ko.isObservable(valueAccessor()))
                    valueAccessor()(file);
            });
        }
    };

    // Knockout Datepicker custom binding handler
    ko.bindingHandlers.datepicker = {
        init: function (element) {
            $(element).datepicker({ autoclose: true }).next().on(ace.click_event, function () {
                $(this).prev().focus();
            });
        },
        update: function (element) {
            $(element).datepicker("refresh");
        }
    };

    ko.applyBindings(viewModel);

});

// Get SPUser from cookies
function GetCurrentUser() {
    //if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
    if ($.cookie(api.cookie.spUser) != undefined) {
        spUser = $.cookie(api.cookie.spUser);

        viewModel.SPUser = spUser;

    }
}
function GetTenorData() {
    var result = [];
    $.ajax({
        async:false,
        type: "GET",
        url: api.server + api.url.gettenordata,
        contentType: "application/json",
        headers: {
            "Authorization": "Bearer " + accessToken
        },
        success: function (data, textStatus, jqXHR) {
            //console.log(data);
            if (jqXHR.status == 200) {
                //console.log(data);
                if (data != null) {
                    result = $.map(data, function (el) { return el; });
                    //return data;
                }
            } else {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    });
    return result;
}

function GetFTPRateData() {
    //console.log(api.server + api.url.getftpratedata);
    var result = [];
    $.ajax({
        async: false,
        type: "GET",
        url: api.server + api.url.getftpratedata,
        contentType: "application/json",
        headers: {
            "Authorization": "Bearer " + accessToken
        },
        success: function (data, textStatus, jqXHR) {
            //console.log(data);
            if (jqXHR.status == 200) {
                //console.log(data);
                if (data != null)
                {
                    result = $.map(data, function (el) { return el; });
                    //console.log(data);
                    //return result;
                }
            } else {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    });
    return result;
}

function GetDataFDInterestMaintenance(data) {
    var descriptionRenderer = function (instance, td, row, col, prop, value, cellProperties) {
        var escaped = Handsontable.helper.stringify(value);
        escaped = strip_tags(escaped, '<em><b><a><button>'); //be sure you only allow certain HTML tags to avoid XSS threats (you should also remove unwanted HTML attributes)
        td.innerHTML = escaped;
        return td;
    };
    var FTPRateData = GetFTPRateData();
    var TenorData = GetTenorData();
    var $container = $("#dataTable");
    if ($container != null) {
        $container.handsontable('destroy');
    }
    
    //var thToYellow = [7];
    var colsToHide = [],
    colsToHide2 = [0],
    tdToYellow = [6, 14, 26],
    colsToOrange = [15];

    function getCustomRenderer() {
        return function (instance, td, row, col, prop, value, cellProperties) {
            var args = arguments;
            Handsontable.renderers.TextRenderer.apply(this, args);
            if (colsToHide.indexOf(col) > -1) {
                //th.hidden = true;
                td.hidden = true;
            } else {
                //th.hidden = false;
                td.hidden = false;
            }
            if (tdToYellow.indexOf(col) > -1) {
                td.style.backgroundColor = 'yellow';
            }
        }
    };
    function getCustomRendererWithValidasiAndColor() {
        return function (instance, td, row, col, prop, value, cellProperties) {
            //Handsontable.renderers.TextRenderer.apply(this, arguments);
            Handsontable.TextCell.renderer.apply(this, arguments);
            var newIntRateData = instance.getDataAtRowProp(row, 'NewIntRate');
            var specialFTPData = instance.getDataAtRowProp(row, 'SpecialFTP');
            var intMarinData = instance.getDataAtRowProp(row, 'InterestMargin');
            var ftpRateData = instance.getDataAtRowProp(row, 'FTPRate');
            var isSelected = instance.getDataAtRowProp(row, 'IsSelected');

            var ftpRate_ = parseFloat(ftpRateData);
            var newIntRate_ = parseFloat(newIntRateData);
            //console.log(isSelected);
            //console.log(specialFTPData);
            //console.log(saveFlag);
            
            //if (ftpRate_ < newIntRate_) {
            //    if (col == 15) {
            //        td.style.background = 'red';
            //    }

            //    if (col == 14) {
            //        td.style.background = 'yellow';
            //    }

            //} else {
                if (colsToHide.indexOf(col) > -1) {
                    //th.hidden = true;
                    td.hidden = true;
                } else {
                    //th.hidden = false;
                    td.hidden = false;
                }
                if (colsToOrange.indexOf(col) > -1) {
                    td.style.background = 'orange';
                }
                if (tdToYellow.indexOf(col) > -1) {
                    td.style.background = 'yellow';
                }
            //}

            //if ((saveFlag == true) && (isSelected == true) && (specialFTPData == true)) {
            //    if (col == 15 || col == 14) {
            //        if (newIntRateData == null || newIntRateData == undefined || newIntRateData == '' || parseFloat(newIntRateData) <= 0 ||
            //            intMarinData == null || intMarinData == undefined || intMarinData == '' || parseFloat(intMarinData) <= 0) {
            //            td.style.background = 'orange';
            //        }
                    
            //    }
            //} else if ((saveFlag == true) && (isSelected == true) && (specialFTPData == false || specialFTPData == null || specialFTPData == undefined)) {
            //    if (col == 14) {
            //        if (newIntRateData == null || newIntRateData == undefined || newIntRateData == '' || parseFloat(newIntRateData) <= 0) {
            //            td.style.background = 'orange';
            //        }
            //    }
            //}

            if ((saveFlag === true) && (isSelected === true) /*&& (specialFTPData == true)*/) {
                    if (ftpRate_ < newIntRate_) {
                        if (col == 15) {
                            //if (newIntRateData == null || newIntRateData == undefined || newIntRateData == '' || parseFloat(newIntRateData) <= 0 ||
                            //    intMarinData == null || intMarinData == undefined || intMarinData == '' || parseFloat(intMarinData) <= 0) {
                            //    td.style.background = 'orange';
                            //}
                            td.style.background = 'red';
                        }
                    }                
            }

            if (specialFTPData == true) {                
                if (col == 15 || col == 14) {
                    //if (newIntRateData == null || newIntRateData == undefined || newIntRateData == '' || parseFloat(newIntRateData) <= 0 ||
                    //    intMarinData == null || intMarinData == undefined || intMarinData == '' || parseFloat(intMarinData) <= 0) {
                    //    td.style.background = 'orange';
                    //}
                    td.style.background = 'orange';
                }
                if ((ftpRate_ < newIntRate_) && saveFlag == true) {
                    if (col == 15) {
                        
                        td.style.background = 'red';
                    }
                }
            }
            //if (specialFTPData == false || specialFTPData == null || specialFTPData == undefined) {
            //    if (col == 14) {
            //        //if (newIntRateData == null || newIntRateData == undefined || newIntRateData == '' || parseFloat(newIntRateData) <= 0 ||
            //        //    intMarinData == null || intMarinData == undefined || intMarinData == '' || parseFloat(intMarinData) <= 0) {
            //        //    td.style.background = 'orange';
            //        //}
            //        td.style.background = 'orange';
            //    }
            //}
        }
    };
    var validatorCustomerRate = function (value, callback) {
        setTimeout(function () {
            var ftp_rate = 27; //All_FTP_Rate[index_row];
            console.log(All_FTP_Rate);
            //console.log(index_row);
            console.log(ftp_rate);
            var CR = parseFloat(value);
            var minRate = parseFloat(MinRate);
            var maxRate = parseFloat(MaxRate);
            console.log(minRate);
            console.log(maxRate);
            if ((CR >= minRate) && (CR <= maxRate) && (CR < ftp_rate)) {
                callback(true);
            } else {
                callback(false);
                alert("NewIntRate must be between " + minRate + " and " + maxRate + ", and less than " + ftp_rate);
            }
        }, 1000);
    };

    $container.handsontable({
        data: data,
        height: 300,
        //fixedColumnsLeft: 3,
        //fixedRowsTop: 1,
        //minSpareCols: 1,
        //minSpareRows: 1,
        //rowHeaders: true,
        //autoWrapRow: true,
        //colHeaders: true,
        afterChange: function (changes,source) {
            if (source == 'loadData') return;//hanya berjalan selain reload pertama kali
            var row;
            var colName;
            var ftpRateValue;

            for (i = 0, len = changes.length; i < len; i++) {//pilih row dan column
                row = changes[i][0];
                colName = changes[i][1];
                //console.log(row);
                //console.log(colName);
            }
            if (colName == 'Tenor') {//event untuk column tenor
                var tenor = hot.getDataAtCell(row, 16);
                var ccy = hot.getDataAtCell(row, 4);

                FTPRateData.forEach(function (item) {
                    if (item.Tenor == tenor) {
                        ftpRateValue = item[ccy];
                    }
                });
                hot.setDataAtCell(row, 17, ftpRateValue);
            }

            if (colName == 'NewIntRate') {//event untuk column NewIntRate
                var ftpRate = hot.getDataAtCell(row, 17);
                var newIntRate = hot.getDataAtCell(row, 15);

                var ftpRate1 = parseFloat(ftpRate);
                var newIntRate1 = parseFloat(newIntRate);
                if (ftpRate1 < newIntRate1) {                    
                    ShowNotification("Attention", "FTP Rate must be equal or greater than New Int Rate!", "gritter-warning", true);
                }
            }

            //if (colName == 'SpecialFTP') {
            //    var specialFTPValue = hot.getDataAtCell(row, 25);
            //    var 
            //}
            
        },
        colHeaders: ["</br></br></br>", "Cust_ID", "Cust_Name", "<center>Account</br>No</center>", "CCY", "<center>OriginalAMount</center>", "<center>Rollover</br>Date</center>", "<center>Maturity</br>Date</center>",
                    "RMName", "<center>Branch</br>Code</center>", "<center>INT_Rate</br>Code</center>", "<center>Interest</br>Rate</center>", "<center>A/C_PREF</br>INT_CR</center>", "<center>NET_INT</br>RATE</center>", "<center>Interest</br>Margin</center>",
                    "<center>NEW_INT</br>RATE</center>","<center>Tenor</center>", "<center>FTP</br>RATE</center>", "<center>SPREAD</br>RATE</center>", "<center>NET_INTEREST</br>AMOUNT</center>", "<center>MATURITYDATE</br>INTRUCTION</center>",
                    "<center>VALUE</br>DATE</br>INTRUCTION</center>", "DATE", "<center>Renewal</br>Option</center>", "<center>Repayment</br>Account</center>", "<center>Special</br>FTP</center>", "<center>Approved</br>By</center>"],
        contextMenu: true,
        columns: [
                {
                    data: 'IsSelected',
                    type: 'checkbox',
                },
            	{
            	    data: 'CIF',
            	    renderer: getCustomRenderer(),
            	    readOnly: true,
            	    allowInvalid: false,
            	},
                {
                    data: 'CustomerName',
                    readOnly: true,
                    renderer: getCustomRenderer(),
                    allowInvalid: false,
                },
                {
                    data: 'AccountNo',
                    readOnly: true,
                    renderer: getCustomRenderer(),
                    allowInvalid: false,
                },
                {
                    data: 'Currency.Code',
                    readOnly: true,
                    renderer: getCustomRenderer(),
                    allowInvalid: false,
                },
                {
                    data: 'OriginalAmount',
                    readOnly: true,
                    type: 'numeric',
                    format: '0,0.00',
                    language: 'en',
                    allowInvalid: false,
                },
                {//6
                    data: 'RolloverDate',
                    type: 'date',
                    dateFormat: 'DD-MMM-YYYY',
                    correctFormat: true,
                    editor: 'date',
                    renderer: getCustomRenderer(),
                    allowInvalid: false,
                },
                {
                    data: 'MaturityDate',
                    type: 'date',
                    readOnly: true,
                    dateFormat: 'DD-MMM-YYYY',
                    correctFormat: true,
                    renderer: getCustomRenderer(),
                    allowInvalid: false,
                },
                {
                    data: 'EmployeeID',
                    readOnly: true,
                    renderer: getCustomRenderer(),
                    allowInvalid: false,
                },
                {
                    data: 'BranchCode',
                    readOnly: true,
                    renderer: getCustomRenderer(),
                    allowInvalid: false,
                },
                {
                    data: 'IntRateCode',
                    readOnly: true,
                    renderer: getCustomRenderer(),
                    allowInvalid: false,
                },
                {
                    data: 'InterestRate',
                    readOnly: true,
                    renderer: getCustomRenderer(),
                    allowInvalid: false,
                },
                {
                    data: 'AcPrefIntCR',
                    readOnly: true,
                    renderer: getCustomRenderer(),
                    allowInvalid: false,
                },
                {
                    data: 'NetIntRate',
                    readOnly: true,
                    renderer: getCustomRenderer(),
                    allowInvalid: false,
                },
                {//14
                    data: 'InterestMargin',
                    editor: 'text',
                    renderer: getCustomRendererWithValidasiAndColor(),
                    allowInvalid: false,
                },
                {//15
                    data: 'NewIntRate',
                    editor: 'text',
                    renderer: getCustomRendererWithValidasiAndColor(),
                    /*validator: function (value, callback) {
                        //console.log(this);
                        //console.log(this.instance.getDataAtCell(this.row, 17));
                        var ftp_rate = this.instance.getDataAtCell(this.row, 17);
                        setTimeout(function () {
                            var CR = parseFloat(value);
                            var FTPRate = parseFloat(ftp_rate);
                            var minRate = parseFloat(MinRate);
                            var maxRate = parseFloat(MaxRate);
                            //console.log(minRate);
                            //console.log(maxRate);
                            if ((CR >= minRate) && (CR <= maxRate) && (CR < FTPRate)) {
                                callback(true);
                            } else {
                                callback(false);
                                alert("NewIntRate must be between " + minRate + " and " + maxRate + ", and less than " + FTPRate);
                            }
                        }, 1000);
                    },*/
                    allowInvalid: false
                },
                {
                    data: 'Tenor',
                    type: 'dropdown',
                    source: TenorData
                },
                {//17
                    data: 'FTPRate',
                    readOnly: true,
                    renderer: getCustomRenderer(),
                    allowInvalid: false,
                },
                {//18
                    data: 'SpreadRate',
                    readOnly: true,
                    renderer: getCustomRenderer(),
                    allowInvalid: false,
                },
                {//19
                    data: 'NetIntAmount',
                    type: 'numeric',
                    format: '0,0.00',
                    language: 'en',
                    readOnly: true,
                    allowInvalid: false,
                },
                {//20
                    data: 'MaturityDateInstruction',
                    type: 'date',
                    dateFormat: 'DD-MMM-YYYY',
                    correctFormat: true,
                    readOnly: true,
                    renderer: getCustomRenderer(),
                    allowInvalid: false,
                },
                {//21
                    data: 'ValueDateInstruction',
                    //type: 'date',
                    //dateFormat: 'DD-MM-YYYY',
                    correctFormat: true,
                    readOnly: true,
                    renderer: getCustomRenderer(),
                    allowInvalid: false,
                },
                {//22
                    data: 'Date',
                    type: 'date',
                    dateFormat: 'DD-MMM-YYYY',
                    correctFormat: true,
                    editor: false,
                    renderer: getCustomRenderer(),
                    allowInvalid: false,
                },
                {//23
                    data: 'RenewalOption',
                    renderer: getCustomRenderer(),
                    allowInvalid: false,
                },
                {//24
                    data: 'RepaymentAccount',
                    renderer: getCustomRenderer(),
                    allowInvalid: false,
                },
                {//25
                    data: 'SpecialFTP',
                    type: 'checkbox'
                },
                {//26
                    data: 'ApprovedBy',
                    editor: 'text',
                    renderer: getCustomRenderer(),
                    allowInvalid: false,
                }
        ]
    });
    var hot = $("#dataTable").handsontable('getInstance');
    $('div.ht_clone_top.handsontable').css('display', 'none');
    $('div.ht_master.handsontable>div.wtHolder').css('width', 1290 + 'px');
    $('div.ht_master.handsontable>div.wtHolder').css('min-height', 300 + 'px');
    $('div.ht_master.handsontable>div.wtHolder').css('max-height', 300 + 'px');
    var handsontable = $container.data('handsontable');
    //console.log(handsontable.getData());
};
function AddDataHot() {
    var $container_ = $("#dataTable2");
    var TenorData = GetTenorData();
    if ($container_ != null) {
        $container_.handsontable('destroy');
    }
    var handsontable = $container_.data('handsontable');
    var emptyValidator = function (value, callback) {
        if (value == "" || value === null || value === undefined) {
            callback(false);
            alert("Can not empty");
        } else {
            callback(true);
        }
    };
    var checkboxRenderer = function (instance, td, row, col, prop, value, cellProperties) {
        //var $td = $(td);
        value = true;
        if (value !== 0 && value !== false)
            value = true;
        else
            value = false;

        // Center the checkbox within the cell
        //$td.addClass('center');

        Handsontable.CheckboxCell.renderer.apply(this, arguments);
    }
    var colsToHide2 = [];
    function getCustomRenderer2() {
        return function (instance, td, row, col, prop, value, cellProperties) {
            var args = arguments;
            if (colsToHide2.indexOf(col) > -1) {
                //th.hidden = true;
                td.hidden = true;
            } else {
                //th.hidden = false;
                td.hidden = false;
            }
            Handsontable.renderers.TextRenderer.apply(this, args);
        }
    };
    var data = [{}];
    var config2 = {
        data: data,
        minRows: 1,
        minSpareCols: 1,
        minSpareRows: 1,
        rowHeaders: true,
        autoWrapRow: true,
        colHeaders: true,
        //currentRowClassName: 'currentRow',
        //currentColClassName: 'currentCol',
        contextMenu: true,
        fixedColumnsLeft: 3,
        colHeaders: ["", "Cust_ID", "Cust_Name", "<center>Account</br>No</center>", "CCY", "<center>OriginalAMount</center>", "<center>Rollover</br>Date</center>", "<center>Maturity</br>Date</center>",
                    "RMName", "<center>Branch</br>Code</center>", "<center>INT_Rate</br>Code</center>", "<center>Interest</br>Rate</center>", "<center>A/C_PREF</br>INT_CR</center>", "<center>NET_INT</br>RATE</center>", "<center>Interest</br>Margin</center>",
                    "<center>NEW_INT</br>RATE</center>","<center>Tenor</center>","<center>FTP</br>RATE</center>", "<center>SPREAD</br>RATE</center>", "<center>NET_INTEREST</br>AMOUNT</center>", "<center>MATURITYDATE</br>INTRUCTION</center>",
                    "<center>VALUE</br>DATE</br>INTRUCTION</center>", "DATE", "<center>Renewal</br>Option</center>", "<center>Repayment</br>Account</center>", "<center>Special</br>FTP</center>", "<center>Approved</br>By</center>"],
        columns: [
                {
                    data: 'ID',
                    renderer: getCustomRenderer2(),
                    readOnly: true
                },
                /*{
                    data: 'IsSelected',
                    type: 'checkbox',
                    checkedTemplate: 1,
                    uncheckedTemplate: 0,
                    label: {
                        position: 'before',
                        value: 'Selected'
                    },
                    //renderer: checkboxRenderer()
                },*/
            	{
            	    data: 'CIF',
            	    renderer: getCustomRenderer2(),
            	    allowInvalid: false,
            	    //validator: emptyValidator
            	},
                {
                    data: 'CustomerName',
                    renderer: getCustomRenderer2(),
                    allowInvalid: false,
                    //validator: emptyValidator
                },
                {
                    data: 'AccountNo',
                    renderer: getCustomRenderer2(),
                    allowInvalid: false,
                    //validator: emptyValidator
                },
                {
                    data: 'Currency.Code',
                    renderer: getCustomRenderer2(),
                    allowInvalid: false,
                    //validator: emptyValidator
                },
                {
                    data: 'OriginalAmount',
                    type: 'numeric',
                    format: '0,0.00',
                    language: 'en',
                    allowInvalid: false,
                    //validator: emptyValidator,
                    renderer: getCustomRenderer2(),
                },
                {//6
                    data: 'RolloverDate',
                    type: 'date',
                    dateFormat: 'DD-MMM-YYYY',
                    correctFormat: true,
                    editor: 'date',//dani
                    renderer: getCustomRenderer2(),//dani
                    allowInvalid: false,
                    //validator: emptyValidator
                },
                {
                    data: 'MaturityDate',
                    type: 'date',
                    dateFormat: 'DD-MMM-YYYY',
                    correctFormat: true,
                    renderer: getCustomRenderer2(),
                    allowInvalid: false,
                    //validator: emptyValidator
                },
                {
                    data: 'EmployeeID',
                    renderer: getCustomRenderer2(),
                    allowInvalid: false,
                    //validator: emptyValidator
                },
                {
                    data: 'BranchCode',
                    renderer: getCustomRenderer2(),
                    allowInvalid: false,
                    //validator: emptyValidator
                },
                {
                    data: 'IntRateCode',
                    renderer: getCustomRenderer2(),
                    allowInvalid: false,
                    //validator: emptyValidator
                },
                {
                    data: 'InterestRate',
                    renderer: getCustomRenderer2(),
                    allowInvalid: false,
                    //validator: emptyValidator
                },
                {
                    data: 'AcPrefIntCR',
                    renderer: getCustomRenderer2(),
                    allowInvalid: false,
                    //validator: emptyValidator
                },
                {
                    data: 'NetIntRate',
                    renderer: getCustomRenderer2(),
                    allowInvalid: false,
                    //validator: emptyValidator
                },
                {//14
                    data: 'InterestMargin',
                    editor: 'text',//dani
                    renderer: getCustomRenderer2(),//dani
                    allowInvalid: false,
                    //validator: emptyValidator
                },
                {//15
                    data: 'NewIntRate',
                    editor: 'text',//dani
                    renderer: getCustomRenderer2(),
                    allowInvalid: false,
                    //validator: emptyValidator
                },
                {
                    data: 'Tenor',
                    type: 'dropdown',
                    source: TenorData
                },
                {
                    data: 'FTPRate',
                    renderer: getCustomRenderer2(),
                    allowInvalid: false,
                    //validator: emptyValidator
                },
                {
                    data: 'SpreadRate',
                    renderer: getCustomRenderer2(),
                    allowInvalid: false,
                    //validator: emptyValidator
                },
                {
                    data: 'NetIntAmount',
                    type: 'numeric',
                    format: '0,0.00',
                    language: 'en',
                    allowInvalid: false,
                    //validator: emptyValidator,
                    renderer: getCustomRenderer2(),
                },
                {
                    data: 'MaturityDateInstruction',
                    type: 'date',
                    dateFormat: 'DD-MMM-YYYY',
                    correctFormat: true,
                    renderer: getCustomRenderer2(),
                    allowInvalid: false,
                    //validator: emptyValidator
                },
                {
                    data: 'ValueDateInstruction',
                    correctFormat: true,
                    renderer: getCustomRenderer2(),
                    allowInvalid: false,
                    //validator: emptyValidator
                },
                {//21
                    data: 'Date',
                    type: 'date',
                    dateFormat: 'DD-MMM-YYYY',
                    correctFormat: true,
                    renderer: getCustomRenderer2(),
                    allowInvalid: false,
                    //validator: emptyValidator
                },
                {
                    data: 'RenewalOption',
                    renderer: getCustomRenderer2(),
                    allowInvalid: false,
                    //validator: emptyValidator
                },
                {
                    data: 'RepaymentAccount',
                    renderer: getCustomRenderer2(),
                    allowInvalid: false,
                    //validator: emptyValidator
                },
                {
                    data: 'SpecialFTP',
                    type: 'checkbox'
                },
                {
                    data: 'ApprovedBy',
                    editor: 'text',
                    renderer: getCustomRenderer2(),
                    allowInvalid: false,
                }
        ]

    };
    $("#dataTable2").handsontable(config2);
}

function GetDataFD(SearchBy, Tanggal) {
    var LogName = DecodeLogin(spUser.LoginName);
    $.ajax({
        type: "GET",
        url: api.server + api.url.transactioncsoget(LogName, SearchBy, Tanggal),
        contentType: "application/json",
        headers: {
            "Authorization": "Bearer " + accessToken
        },
        success: function (data, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                if (data != null) {
                    //console.log(data);
                    if (data.length > 0) {
                        for (var i = 0; i < data.length; i++) {
                            data[i].CreateDate = viewModel.LocalDate(data[i].CreateDate, true, false);
                            data[i].Date = viewModel.LocalDate(data[i].Date, true, false);
                            data[i].MaturityDateInstruction = viewModel.LocalDate(data[i].MaturityDateInstruction, true, false);
                            data[i].RolloverDate = viewModel.LocalDate(data[i].RolloverDate, true, false);
                            data[i].MaturityDate = viewModel.LocalDate(data[i].MaturityDate, true, false);
                            //data[i].ValueDateInstruction = viewModel.LocalDate2(data[i].ValueDateInstruction, true, false);
                        }
                    }
                    GetDataFDInterestMaintenance(data);
                }
            } else {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // send notification
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    });
}

function GetParameters() {
    var options = {
        url: api.server + api.url.parameter,
        params: {
            select: "Product,Currency,Channel,BizSegment,Bank,BankCharge,AgentCharge,FXCompliance,ChargesType,DocType,PurposeDoc,statementletter,underlyingdoc,customer,lld,producttype"
        },
        token: accessToken
    };

    Helper.Ajax.Get(options, OnSuccessGetParameters, OnError, OnAlways);
}

function OnSuccessGetParameters(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {

        var mapping = {
            'ignore': ["LastModifiedDate", "LastModifiedBy"]
        };
        viewModel.Parameter().DocumentTypes(ko.mapping.toJS(data.DocType, mapping));

        var items = ko.utils.arrayFilter(data.PurposeDoc, function (item) {
            return item.ID != 2;
        });
        if (items != null) {
            viewModel.Parameter().DocumentPurposes(ko.mapping.toJS(items, mapping));
        }
    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
}
// AJAX On Alway Callback
function OnAlways() {
    // $box.trigger('reloaded.ace.widget');

    // enabling form input controls
    viewModel.IsEditable(true);
}
// AJAX On Error Callback
function OnError(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}
// Token validation On Success function
function TokenOnSuccess(data, textStatus, jqXHR) {
    // store token on browser cookies
    $.cookie(api.cookie.name, data.AccessToken);
    accessToken = $.cookie(api.cookie.name);
    // call spuser function
    GetCurrentUser(viewModel);
}

// Token validation On Error function
function TokenOnError(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}

function OnReceivedSignal(data) {
    ShowNotification(data.Sender, data.Message, "gritter-info", false);
}

// SignalR ----------------------
var taskHub = $.hubConnection(config.signal.server + "hub");
var taskProxy = taskHub.createHubProxy("TaskService");
var IsTaskHubConnected = false;

function StartTaskHub() {
    taskHub.start()
        .done(function () {

            IsTaskHubConnected = true;
        })
        .fail(function () {
            ShowNotification("Hub Connection Failed", "Fail while try connecting to hub server", "gritter-error", true);
        }
    );
}

// Upload the file
function UploadFile(context, document, callBack) {

    var serverRelativeUrlToFolder = '';
    serverRelativeUrlToFolder = '/Instruction Documents';

    var parts = document.DocumentPath.name.split('.');
    var fileExtension = parts[parts.length - 1];
    var timeStamp = new Date().getTime();
    var fileName = timeStamp + "." + fileExtension; //document.DocumentPath.name;

    // Get the server URL.
    var serverUrl = _spPageContextInfo.webAbsoluteUrl;

    // output variable
    var output;

    // Initiate method calls using jQuery promises.
    // Get the local file as an array buffer.
    var getFile = getFileBuffer();
    getFile.done(function (arrayBuffer) {

        // Add the file to the SharePoint folder.
        var addFile = addFileToFolder(arrayBuffer);
        addFile.done(function (file, status, xhr) {
            output = file.d;

            // Get the list item that corresponds to the uploaded file.
            var getItem = getListItem(file.d.ListItemAllFields.__deferred.uri);
            getItem.done(function (listItem, status, xhr) {

                // Change the display name and title of the list item.
                var changeItem = updateListItem(listItem.d.__metadata);
                changeItem.done(function (data, status, xhr) {
                    //alert('file uploaded and updated');
                    //return output;
                    var newDoc = {
                        ID: 0,
                        Type: document.Type,
                        Purpose: document.Purpose,
                        LastModifiedDate: null,
                        LastModifiedBy: null,
                        DocumentPath: output.ServerRelativeUrl,
                        FileName: document.DocumentPath.name
                    };

                    viewModel.TransactionFD().Documents.push(newDoc);

                    callBack();
                });
                changeItem.fail(OnError);
            });
            getItem.fail(OnError);
        });
        addFile.fail(OnError);
    });
    getFile.fail(OnError);

    // Get the local file as an array buffer.
    function getFileBuffer() {
        var deferred = $.Deferred();
        var reader = new FileReader();
        reader.onloadend = function (e) {
            deferred.resolve(e.target.result);
        }
        reader.onerror = function (e) {
            deferred.reject(e.target.error);
        }

        if (document.DocumentPath.type == Util.spitemdoc) {
            var fileURI = serverUrl + document.DocumentPath.DocPath;
            var ExistFile = $.ajax({
                url: fileURI,
                type: "GET",
                headers: { "accept": "application/json;odata=verbose" }
            });
            return ExistFile;
        }
        //bangkit
        reader.readAsArrayBuffer(document.DocumentPath);
        //reader.readAsDataURL(document.DocumentPath);
        return deferred.promise();
    }

    // Add the file to the file collection in the Shared Documents folder.
    function addFileToFolder(arrayBuffer) {

        // Construct the endpoint.
        var fileCollectionEndpoint = String.format(
                "{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files" +
                "/add(overwrite=false, url='{2}')",
            serverUrl, serverRelativeUrlToFolder, fileName);

        // Send the request and return the response.
        // This call returns the SharePoint file.
        return $.ajax({
            url: fileCollectionEndpoint,
            type: "POST",
            data: arrayBuffer,
            processData: false,
            headers: {
                "accept": "application/json;odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val()
                //"content-length": arrayBuffer.byteLength
            }
        });
    }

    // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
    function getListItem(fileListItemUri) {

        // Send the request and return the response.
        return $.ajax({
            url: fileListItemUri,
            type: "GET",
            headers: { "accept": "application/json;odata=verbose" }
        });
    }

    // Change the display name and title of the list item.
    function updateListItem(itemMetadata) {
        var body = {
            Title: document.DocumentPath.name,
            Application_x0020_ID: context.ApplicationID,
            CIF: context.CIF,
            Customer_x0020_Name: context.Name,
            Document_x0020_Type: document.Type.Name,
            Document_x0020_Purpose: document.Purpose.Name,
            __metadata: {
                type: itemMetadata.type,
                FileLeafRef: fileName,
                Title: fileName
            }
        };

        // Send the request and return the promise.
        // This call does not return response content from the server.
        return $.ajax({
            url: itemMetadata.uri,
            type: "POST",
            data: ko.toJSON(body),
            headers: {
                "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                "content-type": "application/json;odata=verbose",
                //"content-length": body.length,
                "IF-MATCH": itemMetadata.etag,
                "X-HTTP-Method": "MERGE"
            }
        });
    }
}