﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class AdhocController : ApiController
    {
         private string message = string.Empty;
         private readonly IAdhocRepository repository = null;

         public AdhocController()
         {
             this.repository = new AdhocRepository();
         }
         public AdhocController(IAdhocRepository repository)
         {
             this.repository = repository;
         }

         [ResponseType(typeof(AdhocModel))]
         [Route("api/Adhoc/{id}")]
         [HttpGet]
         public HttpResponseMessage Get(int id)
         {
             AdhocModel output = new AdhocModel();

             if (!repository.GetAdhocByID(id, ref output, ref message))
             {
                 return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
             }
             else
             {
                 if (output== null)
                     return Request.CreateResponse(HttpStatusCode.NoContent);
                 else
                     return Request.CreateResponse<AdhocModel>(HttpStatusCode.OK, output);
             }
         }
         [ResponseType(typeof(IList<AdhocModel>))]
         public HttpResponseMessage Get(int? limit, int? index)
         {
             IList<AdhocModel> output = new List<AdhocModel>();

             if (!repository.GetAdhoc(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
             {
                 return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
             }
             else
             {
                 return Request.CreateResponse<IList<AdhocModel>>(HttpStatusCode.OK, output);
             }
         }
         [ResponseType(typeof(AdhocGrid))]
         [AcceptVerbs("GET", "POST")]
         public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<AdhocFilter> filters)
         {
             AdhocGrid output = new AdhocGrid();

             if (!repository.GetAdhoc(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "CIF" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
             {
                 return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
             }
             else
             {
                 return Request.CreateResponse<AdhocGrid>(HttpStatusCode.OK, output);
             }
         }
         [ResponseType(typeof(IList<AdhocModel>))]
         [HttpGet]
         [Route("api/Adhoc/Search")]
         public HttpResponseMessage Search(string query, int? limit)
         {
             IList<AdhocModel> output = new List<AdhocModel>();

             if (!repository.GetAdhoc(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
             {
                 return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
             }
             else
             {
                 return Request.CreateResponse<IList<AdhocModel>>(HttpStatusCode.OK, output);
             }
         }
         [HttpPost]
         [ResponseType(typeof(string))]
         //[Authorize(Roles = "DBS Admin")]
         public HttpResponseMessage Post(AdhocModel adhoc)
         {
             if (!ModelState.IsValid)
             {
                 return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
             }
             else
             {
                 if (!repository.AddAdhoc(adhoc, ref message))
                 {
                     return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                 }
                 else
                 {
                     return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New Adhoc data has been created." });
                 }
             }
         }

         [HttpPut]
         [ResponseType(typeof(string))]
         [Route("api/Adhoc/{id}")]
         //[Authorize(Roles = "DBS Admin")]
         public HttpResponseMessage Put(int id, AdhocModel adhoc)
         {
             if (!ModelState.IsValid)
             {
                 return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
             }
             else
             {
                 if (!repository.UpdateAdhoc(id, adhoc, ref message))
                 {
                     return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                 }
                 else
                 {
                     return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Adhoc data has been updated." });
                 }
             }
         }
         [HttpDelete]
         [Route("api/Adhoc/{id}")]
         [ResponseType(typeof(string))]
         //[Authorize(Roles = "DBS Admin")]
         public HttpResponseMessage Delete(int id)
         {
             if (!ModelState.IsValid)
             {
                 return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
             }
             else
             {
                 if (!repository.DeleteAdhoc(id, ref message))
                 {
                     return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                 }
                 else
                 {
                     return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Adhoc data has been deleted." });
                 }
             }
         }
         protected override void Dispose(bool disposing)
         {
             if (repository != null)
                 repository.Dispose();
             base.Dispose(disposing);
         }

    }
}