﻿var accessToken;
var $box;
var $remove = false;

var productName;
var startDate; var strStartDate;
var endDate; var strEndDate;
var startMonth; var endMonth;
var startDay; var endDay;
var dateNowFromServer;
var clientDateTime;
var isOtherProduct;
var pageTransaction;
var plot = [];
var plotNumber;
var SelectedProductType;
var ProductTypeDetail;
var ProductTypeDetailID;
var ConsProductIDCounter;
//20160913 aridya add untuk payment IPE
var paymentMode = '';
//end aridya


var GridPropertiesModel = function (callback) {
    var self = this;

    self.SortColumn = ko.observable();
    self.SortOrder = ko.observable();
    self.AllowFilter = ko.observable(false);
    self.AvailableSizes = ko.observableArray(config.sizes);
    self.ProductName = ko.observable();
    self.Page = ko.observable(1);
    self.Size = ko.observable(config.sizeDefault);
    self.Total = ko.observable(0);
    self.TotalPages = ko.observable(0);

    self.Callback = function () {
        callback();
    };

    self.OnPageSizeChange = function () {
        self.Page(1);

        self.Callback();
    };

    self.OnPageChange = function () {
        if (self.Page() < 1) {
            self.Page(1);
        } else {
            if (self.Page() > self.TotalPages())
                self.Page(self.TotalPages());
        }

        self.Callback();
    };

    self.NextPage = function () {
        isOtherProduct = false;
        var page = self.Page();
        if (page < self.TotalPages()) {
            self.Page(page + 1);

            self.Callback();
        }
    };

    self.PreviousPage = function () {
        isOtherProduct = false;
        var page = self.Page();
        if (page > 1) {
            self.Page(page - 1);

            self.Callback();
        }
    };

    self.FirstPage = function () {
        self.Page(1);

        self.Callback();
    };

    self.LastPage = function () {
        self.Page(self.TotalPages());

        self.Callback();
    };

    self.Filter = function (data) {
        self.Page(1);

        self.Callback();
    };

    // get sorted column
    self.GetSortedColumn = function (columnName) {
        var sort = "sorting";

        if (self.SortColumn() == columnName) {
            if (self.SortOrder() == "DESC")
                sort = sort + "_asc";
            else
                sort = sort + "_desc";
        }

        return sort;
    };

    // Sorting data
    self.Sorting = function (column) {
        if (self.SortColumn() == column) {
            if (self.SortOrder() == "ASC")
                self.SortOrder("DESC");
            else
                self.SortOrder("ASC");
        } else {
            self.SortOrder("ASC");
        }

        self.SortColumn(column);

        self.Page(1);

        self.Callback();
    };
};
var ProductTypeModel = [{ ID: 0, Name: 'All' },
						{ ID: 1, Name: 'Payment' },
						{ ID: 2, Name: 'TMO' },
						{ ID: 3, Name: 'Loan' },
						{ ID: 4, Name: 'UT' },
						{ ID: 5, Name: 'FD' },
						{ ID: 6, Name: 'Retail CIF' },
                        { ID: 7, Name: 'Payment IPE' }]
var SelectedModel = {
    ProductType: ko.observable()
};
var ViewModel = function () {
    var self = this;

    //Declare Field to Display on grid	
    self.ID = ko.observable();
    self.ApplicationID = ko.observable();
    self.CustomerName = ko.observable();
    self.TransactionStatus = ko.observable();
    self.TAT = ko.observable();
    self.Percentage = ko.observable();

    //Declare Filter for grid
    self.FilterApplicationID = ko.observable("");
    self.FilterCustomerName = ko.observable("");
    self.FilterTransactionStatus = ko.observable("");
    self.FilterTAT = ko.observable();
    self.FilterPercentage = ko.observable();

    // Declare an Observable Array for Storing the JSON Response
    self.Transactions = ko.observableArray([]);

    self.ProductType = ko.observableArray([]);
    self.Selected = ko.observable(SelectedModel);

    self.GridProperties = ko.observable();
    self.GridProperties(new GridPropertiesModel(GetData));


    self.GridProperties().SortColumn("TATTrx");
    self.GridProperties().SortOrder("DESC");

    self.ClearFilters = function () {

        self.FilterApplicationID("");
        self.FilterCustomerName("");
        self.FilterTransactionStatus("");
        self.FilterTAT();
        self.FilterPercentage();


        GetData();

    };
    self.GetData = function (productName, dateNowFromServer, isOtherProduct) {


        var arrProductName = $("#spProductName").text().split(":");
        productName = arrProductName[1].trim();
        isOtherProduct = isOtherProduct;
        if ($("#lblDateFromServer").text() != '') {

            dateNowFromServer = $("#lblDateFromServer").text().trim();
        }

        GetData(productName, dateNowFromServer, isOtherProduct);

    };
    self.OnProductTypeChange = function () {
        SelectedProductType = self.Selected().ProductType();
        ProductTypeDetail = [];
        ConsProductIDCounter = [];
        ProductTypeDetailID = [];

        $.each(ConsProductID, function (item, index) {
            ConsProductIDCounter.push(item);
        });

        for (var i = 0, max = ConsProductIDCounter.length; i <= max; i++) {
            switch (SelectedProductType) {
                case ProductTypeModel[0].ID:
                    ProductTypeDetail.push(ConsProductID.RTGSProductIDCons);
                    ProductTypeDetail.push(ConsProductID.SKNProductIDCons);
                    ProductTypeDetail.push(ConsProductID.OTTProductIDCons);
                    ProductTypeDetail.push(ConsProductID.OverbookingProductIDCons);
                    ProductTypeDetail.push(ConsProductID.TMOProductIDCons);
                    ProductTypeDetail.push(ConsProductID.LoanDisbursmentProductIDCons);
                    ProductTypeDetail.push(ConsProductID.LoanRolloverProductIDCons);
                    ProductTypeDetail.push(ConsProductID.LoanIMProductIDCons);
                    ProductTypeDetail.push(ConsProductID.LoanSettlementProductIDCons);
                    ProductTypeDetail.push(ConsProductID.IDInvestmentProductIDCons);
                    ProductTypeDetail.push(ConsProductID.SavingPlanProductIDCons);
                    ProductTypeDetail.push(ConsProductID.UTOnshoreproductIDCons);
                    ProductTypeDetail.push(ConsProductID.UTOffshoreProductIDCons);
                    ProductTypeDetail.push(ConsProductID.UTCPFProductIDCons);
                    ProductTypeDetail.push(ConsProductID.FDProductIDCons);
                    ProductTypeDetail.push(ConsProductID.CIFProductIDCons);
                    ProductTypeDetail.push(ConsProductID.SKNBulkProductIDCons); //aridya add 20161206 add skn bulk to all
                    paymentMode = '-1'; //20160913 aridya add untuk payment mode
                    break;
                case ProductTypeModel[1].ID:
                    ProductTypeDetail.push(ConsProductID.RTGSProductIDCons);
                    ProductTypeDetail.push(ConsProductID.SKNProductIDCons);
                    ProductTypeDetail.push(ConsProductID.OTTProductIDCons);
                    ProductTypeDetail.push(ConsProductID.OverbookingProductIDCons);
                    //20160913 aridya add untuk payment mode
                    paymentMode = 'BCP2';
                    //end aridya
                    break;
                case ProductTypeModel[2].ID:
                    ProductTypeDetail.push(ConsProductID.TMOProductIDCons);
                    paymentMode = '-1'; //20160913 aridya add untuk payment mode
                    break;
                case ProductTypeModel[3].ID:
                    ProductTypeDetail.push(ConsProductID.LoanDisbursmentProductIDCons);
                    ProductTypeDetail.push(ConsProductID.LoanRolloverProductIDCons);
                    ProductTypeDetail.push(ConsProductID.LoanIMProductIDCons);
                    ProductTypeDetail.push(ConsProductID.LoanSettlementProductIDCons);
                    paymentMode = '-1'; //20160913 aridya add untuk payment mode
                    break;
                case ProductTypeModel[4].ID:
                    ProductTypeDetail.push(ConsProductID.IDInvestmentProductIDCons);
                    ProductTypeDetail.push(ConsProductID.SavingPlanProductIDCons);
                    ProductTypeDetail.push(ConsProductID.UTOnshoreproductIDCons);
                    ProductTypeDetail.push(ConsProductID.UTOffshoreProductIDCons);
                    ProductTypeDetail.push(ConsProductID.UTCPFProductIDCons);
                    paymentMode = '-1'; //20160913 aridya add untuk payment mode
                    break;
                case ProductTypeModel[5].ID:
                    ProductTypeDetail.push(ConsProductID.FDProductIDCons);
                    paymentMode = '-1'; //20160913 aridya add untuk payment mode
                    break;
                case ProductTypeModel[6].ID:
                    ProductTypeDetail.push(ConsProductID.CIFProductIDCons);
                    paymentMode = '-1'; //20160913 aridya add untuk payment mode
                    break;
                //add aridya 20160910 for Payment IPE
                case ProductTypeModel[7].ID:
                    ProductTypeDetail.push(ConsProductID.RTGSProductIDCons);
                    ProductTypeDetail.push(ConsProductID.SKNProductIDCons);
                    ProductTypeDetail.push(ConsProductID.OTTProductIDCons);
                    ProductTypeDetail.push(ConsProductID.OverbookingProductIDCons);
                    ProductTypeDetail.push(ConsProductID.SKNBulkProductIDCons); //aridya add 20161122 add skn bulk to payment ipe
                    //20160913 aridya add untuk payment mode
                    paymentMode = 'IPE';
                    //end aridya
                    break;
                //end add aridya
            }

        }


        $.each(ProductTypeDetail, function (i, el) {
            if ($.inArray(el, ProductTypeDetailID) === -1) ProductTypeDetailID.push(el);
        });

        clearContent();
        getDateFromServer();



    };
    function GetData(productName, dateNowFromServer, isOtherProduct) {
        if (productName == null) {
            var arrProductName = $("#spProductName").text().split(":");
            productName = arrProductName[1].trim();

        }

        if (dateNowFromServer == null) {
            var dateNowFromServer = $("#lblDateFromServer").text().trim();

        }
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }


        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });

        strStartDate = new Date(dateNowFromServer);
        strEndDate = new Date(dateNowFromServer);
        if ((strStartDate.getMonth() + 1).toString().length == 1) {
            startMonth = "0" + (strStartDate.getMonth() + 1).toString();
        } else {
            startMonth = (strStartDate.getMonth() + 1).toString();
        }

        if ((strEndDate.getMonth() + 1).toString().length == 1) {
            endMonth = "0" + (strEndDate.getMonth() + 1).toString();

        } else {
            endMonth = (strEndDate.getMonth() + 1).toString();

        }

        if (strStartDate.getDate().toString().length == 1) {
            startDay = "0" + strStartDate.getDate().toString();
        } else {
            startDay = strStartDate.getDate().toString();
        }

        if (strEndDate.getDate().toString().length == 1) {
            endDay = "0" + strEndDate.getDate().toString();

        } else {
            endDay = strEndDate.getDate().toString();

        }
        if (isOtherProduct) {
            self.GridProperties().Page(1);
        }
        startDate = strStartDate.getFullYear() + "/" + startMonth + "/" + startDay + "T00:00:00.000";
        endDate = strEndDate.getFullYear() + "/" + endMonth + "/" + endDay + "T23:59:59.999";
        //console.log(startDate + " " + endDate);
        var options = {

            url: api.server + api.url.volumematrixtransactions,
            params: {
                productName: productName,
                page: self.GridProperties().Page(),
                size: self.GridProperties().Size(),
                sort_column: self.GridProperties().SortColumn(),
                sort_order: self.GridProperties().SortOrder(),
                startDate: startDate,
                endDate: endDate,
                clientDateTime: clientDateTime,
                paymentMode: paymentMode //20160913 aridya add untuk payment mode
            },
            token: accessToken

        };

        // get filtered columns
        var filters = GetFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST                        
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetData, OnError, OnAlways);

        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetData, OnError, OnAlways);
        }


    }
    function GetFilteredColumns() {
        // define filter
        var filters = [];

        if (self.FilterApplicationID() != "") filters.push({ Field: 'applicationIDTrx', Value: self.FilterApplicationID() });
        if (self.FilterCustomerName() != "") filters.push({ Field: 'customerNameTrx', Value: self.FilterCustomerName() });
        if (self.FilterTransactionStatus() != "") filters.push({ Field: 'transactionStatusTrx', Value: self.FilterTransactionStatus() });
        if (self.FilterTAT() != "") filters.push({ Field: 'TATTrx', Value: self.FilterTAT() });
        if (self.FilterPercentage() != "") filters.push({ Field: 'percentageTrx', Value: self.FilterPercentage() });


        return filters;
    };
    // On success GetData callback
    function OnSuccessGetData(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.Transactions(data.Rows);

            self.GridProperties().Page(data['Page']);
            self.GridProperties().Size(data['Size']);
            self.GridProperties().Total(data['Total']);
            self.GridProperties().TotalPages(Math.ceil(self.GridProperties().Total() / self.GridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    // On Error callback
    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    // On Always callback
    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }

    self.splitStatus = function (data) {
        var item = data.split(')');
        return $.trim(item[1]);
    }

    function clearContent() {
        /*$('#lblInfoPeriod').fadeOut(500);
        $('#chartholder').fadeOut(500);
        $('#tableholder').fadeOut(500);
        $('#chartholder > .row').fadeOut(500);*/
        if (plot) {
            $.each(plot, function (index, item) {
                item.destroy();
            });
        }
        $('#legendholder').hide().fadeOut(500);
        $('#lblInfoPeriod').removeClass('alert alert-info');
        $('#lblDashboardReport').text('');
        $('#lblLastUpdate').text('');
        $('#tableholder > .row').remove();
        
        $('[id*="chartHolder"]').remove();

        $('#chartcluster > .row').unbind();
        $('#chartcluster > .row').html('');

    }
    var counterRefresh = 3600000; //45000; //counter refresh value in milisecond, it's equals to 45 second
    if (counterRefresh == 0) {
        var counter = setInterval(function () {
            clearContent();
            getDateFromServer();
            clearInterval(counter);

        }, 1000);

    } else {
        setInterval(function () {
            clearContent();
            getDateFromServer();
        }, counterRefresh);
    }
    function getDateFromServer() {
        var newDateTime = new Date();
        var newMonth; var newDate;
        var newHour; var newMinute; var newSecond;

        newMonth = newDateTime.getMonth() + 1;
        newMonth = newMonth.toString().length == 2 ? newMonth : "0" + newMonth;
        newDate = newDateTime.getDate();
        newDate = newDate.toString().length == 2 ? newDate : "0" + newDate;
        newHour = newDateTime.getHours();
        newHour = newHour.toString().length == 2 ? newHour : "0" + newHour;
        newMinute = newDateTime.getMinutes();
        newMinute = newMinute.toString().length == 2 ? newMinute : "0" + newMinute;
        newSecond = newDateTime.getSeconds();
        newSecond = newSecond.toString().length == 2 ? newSecond : "0" + newSecond;

        clientDateTime = newDateTime.getFullYear() + "/" + newMonth + "/" + newDate + "T" + newHour + ":" + newMinute + ":" + newSecond;


        var options = {

            url: api.server + api.url.currentdate,
            token: accessToken,
            params: {
                clientDateTime: clientDateTime
            }

        };


        Helper.Ajax.Get(options, OnSuccessGetDateNow, OnError, null);



    }
    function OnSuccessGetDateNow(data, textStatus, jqXHR) {

        if (jqXHR.status = 200) {


            renderDateNow(data);


        }

    }
    function renderDateNow(data) {
        var dateNow; var dateNowLast; var arrDateNow = []; var strDateNowFromClient; var dateNowFromClient;
        if (data.length > 0) {
            for (var i = 0, max = data.length; i < max; i++) {
                if (i == 0) {
                    dateNow = data[i].clientDateTimeNow;

                    arrDateNow = dateNow.indexOf("T") > -1 ? dateNow.split("T") : dateNow.indexOf(" ") > -1 ? dateNow.split(" ") : dateNow.substring(0, 10);
                    dateNowLast = arrDateNow[0];
                    dateNowFromServer = dateNowLast;

                    strDateNowFromClient = new Date(dateNowFromServer);
                    dateNowFromClient = strDateNowFromClient.getFullYear() + "/" + (strDateNowFromClient.getMonth() + 1) + "/" + strDateNowFromClient.getDate() + dateNow.substring(dateNow.toString().indexOf("T"));
                    strStartDate = new Date(dateNowFromServer);
                    strEndDate = new Date(dateNowFromServer);
                    if ((strStartDate.getMonth() + 1).toString().length == 1) {
                        startMonth = "0" + (strStartDate.getMonth() + 1).toString();
                    } else {
                        startMonth = (strStartDate.getMonth() + 1).toString();
                    }

                    if ((strEndDate.getMonth() + 1).toString().length == 1) {
                        endMonth = "0" + (strEndDate.getMonth() + 1).toString();

                    } else {
                        endMonth = (strEndDate.getMonth() + 1).toString();

                    }

                    if (strStartDate.getDate().toString().length == 1) {
                        startDay = "0" + strStartDate.getDate().toString();
                    } else {
                        startDay = strStartDate.getDate().toString();
                    }

                    if (strEndDate.getDate().toString().length == 1) {
                        endDay = "0" + strEndDate.getDate().toString();

                    } else {
                        endDay = strEndDate.getDate().toString();

                    }

                    startDate = strStartDate.getFullYear() + "/" + startMonth + "/" + startDay + "T00:00:00.000";
                    endDate = strEndDate.getFullYear() + "/" + endMonth + "/" + endDay + "T23:59:59.999";
                    $("#lblDateFromServer").text(dateNowFromServer);
                    if (ProductTypeDetailID != undefined || ProductTypeDetailID != null) {
                        renderChart(ProductTypeDetailID, startDate, endDate, dateNow, paymentMode); //20160913 aridya add paymentMode
                    }

                }
            }
        }


    }

    //render chart from jquery plugin (jqplot)
    //20160913 aridya add paymentMode
    function renderChart(ProductTypeDetailID, startDate, endDate, clientDateTime, paymentMode) {

        var url = api.server + api.url.volumematrixdashboardchart;

        var options = {
            params: {
                productTypeDetailID: ProductTypeDetailID,
                startDate: startDate,
                endDate: endDate,
                clientDateTime: clientDateTime,
                paymentMode: paymentMode
            }
        };
        //console.log(options.params.productTypeDetailID);
        if (options.params != null || options.params != undefined) {
            url += DecodeParams(options.params);
        }
        $.ajax({
            async: false,
            url: url,
            type: 'GET',
            contentType: "application/json",
            header: {
                "Authorization": "Bearer " + accessToken,
            },
            success: function (resultChart) {
                //console.log(resultChart);
                var url = api.server + api.url.volumematrixdashboardchannel;
                var options = {
                    params: {
                        productTypeDetailID: ProductTypeDetailID,
                        startDate: startDate,
                        endDate: endDate,
                        clientDateTime: clientDateTime,
                        paymentMode: paymentMode
                    }
                };
                //end add aridya
                if (options.params != null || options.params != undefined) {
                    url += DecodeParams(options.params);
                }


                $.ajax({
                    async: false,
                    url: url,
                    type: "GET",
                    contentType: "application/json",
                    header: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (resultChannel) {
                        //console.log(resultChannel);
                        // Data Modelling //
                        var currentProduct = null;
                        var arr = [];
                        var rowHolder = null;
                        var rowHolderLegend = null;
                        var htlmLegend = null;
                        var chartCount = 0;
                        var chartsData = new Array();

                        if (resultChart.length > 0) {
                            for (var i = 0, max = resultChart.length; i < max; i++) {
                                var product = resultChart[i].productName;
                                var hour = resultChart[i].hour;
                                var qtycompleted = resultChart[i].qtyCompleted;
                                var qtyonprogress = resultChart[i].qtyOnProgress;
                                var qtycancelled = resultChart[i].qtyCancelled;
                                var qtywaitingACK1 = resultChart[i].qtyWaitingACK1;
                                var qtywaitingACK2 = resultChart[i].qtyWaitingACK2;
                                var createdDate = resultChart[i].createdDate;

                                if ((qtycompleted == 0) && (qtyonprogress == 0) && (qtycancelled == 0) && (qtywaitingACK1 == 0) && (qtywaitingACK2 == 0)) {
                                    hour = "";
                                }

                                selChartsData = GetObjectByName(chartsData, product);
                                if (selChartsData == null) {
                                    selChartsData = {
                                        "name": product, "hour": new Array(), "qtycompleted": new Array(), "qtyonprogress": new Array(), "qtycancelled": new Array(), "qtywaitingACK1": new Array(), "qtywaitingACK2": new Array(), "createdDate": new Array(), "xLabel": "Time : "
                                    };
                                    chartsData.push(selChartsData);

                                }


                                selChartsData.hour.push(hour);
                                selChartsData.qtycompleted.push(qtycompleted);
                                selChartsData.qtyonprogress.push(qtyonprogress);
                                selChartsData.qtycancelled.push(qtycancelled);
                                selChartsData.qtywaitingACK1.push(qtywaitingACK1);
                                selChartsData.qtywaitingACK2.push(qtywaitingACK2);
                            }
                        }
                        selChartsData = {
                            "name": "Channel", "hour": new Array(), "qtycompleted": new Array(), "qtyonprogress": new Array(), "qtycancelled": new Array(), "qtywaitingACK1": new Array(), "qtywaitingACK2": new Array(), "createdDate": new Array(), "xLabel": "Channel : "
                        };
                        if (resultChannel.length > 0) {
                            //console.log(resultChannel);
                            $.each(resultChannel, function (index, item) {
                                selChartsData.hour.push(item.channelName);
                                selChartsData.qtycompleted.push(item.qtyCompleted);
                                selChartsData.qtyonprogress.push(item.qtyOnProgress);
                                selChartsData.qtycancelled.push(item.qtyCancelled);
                                selChartsData.qtywaitingACK1.push(item.qtyWaitingACK1);
                                selChartsData.qtywaitingACK2.push(item.qtyWaitingACK2);


                            });
                        } else if (resultChannel.length <= 0) {

                            selChartsData.hour.push('');
                            selChartsData.qtycompleted.push(0);
                            selChartsData.qtyonprogress.push(0);
                            selChartsData.qtycancelled.push(0);
                            selChartsData.qtywaitingACK1.push(0);
                            selChartsData.qtywaitingACK2.push(0);

                        }
                        chartsData.push(selChartsData);

                        // Render View //
                        rowHolder = null;
                        rowHolderLegend = null;

                        $.each(chartsData, function (index, item) {

                            //console.log(item.name + " " + item.qtycompleted + " " + item.qtyonprogress + " " + item.qtycancelled);
                            if (index % 2 == 0) {
                                rowHolder = $('<div class="row"></div>');
                                rowHolder.appendTo("#chartcluster");
                            }
                            var chartHolder = $('<div class="col-xs-6 col-sm-6"><div id="chartHolder' + index + '" style="width:200px; height:270px;"></div></div>');
                            chartHolder.appendTo(rowHolder);

                            holder = $("#chartHolder" + index).attr("id");
                            renderJqPlot(index, item.name, holder, item.qtycompleted, item.qtyonprogress, item.qtywaitingACK1, item.qtywaitingACK2, item.qtycancelled, item.hour, item.xLabel);

                        });
                        plotNumber = chartsData.length - 1;
                        rowHolderLegend = $('<div id="legendholder" class="row"></div>');
                        rowHolderLegendExist = $('#legendholder');
                        if (rowHolderLegendExist.length) {
                        } else {
                            rowHolderLegend.appendTo("#legendcluster");
                        }//
                        htmlLegend = $("<br/>&nbsp;&nbsp;&nbsp;&nbsp<label id='lblLegend'>Legend :</label><br/><div class='col-xs-5 pull-left'><div style='height:150px;'><table class='jqplot-table-legend' style='right: 0px; top: 0px; left:0px;'>" +
										"<tbody><tr class='jqplot-table-legend'>" +
										"<td class='jqplot-table-legend jqplot-table-legend-swatch' style='text-align: center; padding-top: 0px;'>" +
										"<div class='jqplot-table-legend-swatch-outline'>" +
										"<div class='jqplot-table-legend-swatch' style='border-color: rgb(75, 178, 197); background-color: rgb(75, 178, 197);'></div>" +
										"</div></td><td class='jqplot-table-legend jqplot-table-legend-label'>Completed Transactions</td></tr>" +
										"<tr class='jqplot-table-legend'>" +
										"<td class='jqplot-table-legend jqplot-table-legend-swatch' style='text-align: center;'>" +
										"<div class='jqplot-table-legend-swatch-outline'>" +
										"<div class='jqplot-table-legend-swatch' style='border-color: rgb(234, 162, 40); background-color: rgb(234, 162, 40);'></div>" +
										"</div></td><td class='jqplot-table-legend jqplot-table-legend-label' style='padding-top: 0px;'>In Progress Transactions in Work Flow</td></tr>" +
                                        "<tr class='jqplot-table-legend'>" +
										"<td class='jqplot-table-legend jqplot-table-legend-swatch' style='text-align: center;'>" +
										"<div class='jqplot-table-legend-swatch-outline'>" +
										"<div class='jqplot-table-legend-swatch' style='border-color: rgb(153, 255, 102); background-color: rgb(153, 255, 102);'></div>" +
										"</div></td><td class='jqplot-table-legend jqplot-table-legend-label' style='padding-top: 0px;'>In Progress In IPE (Waiting ACK1)</td></tr>" +                                        
                                        "<tr class='jqplot-table-legend'>" +
										"<td class='jqplot-table-legend jqplot-table-legend-swatch' style='text-align: center;'>" +
										"<div class='jqplot-table-legend-swatch-outline'>" +
										"<div class='jqplot-table-legend-swatch' style='border-color: rgb(255, 0, 255); background-color:rgb(255, 0, 255);'></div>" +
										"</div></td><td class='jqplot-table-legend jqplot-table-legend-label' style='padding-top: 0px;'>In Progress In IPE (Waiting ACK2)</td></tr>" +
										"<tr class='jqplot-table-legend'>" +
										"<td class='jqplot-table-legend jqplot-table-legend-swatch' style='text-align: center;'>" +
										"<div class='jqplot-table-legend-swatch-outline'>" +
										"<div class='jqplot-table-legend-swatch' style='border-color: rgb(197, 180, 127); background-color: rgb(197, 180, 127);'></div>" +
										"</div></td><td class='jqplot-table-legend jqplot-table-legend-label' style='padding-top: 0px;'>Cancelled Transactions</td></tr></tbody></table></div></div>");

                        if (rowHolderLegendExist.length) {
                        } else {
                            htmlLegend.appendTo("#legendholder");
                        }

                        renderPeriodLastUpdate(startDate, endDate, clientDateTime, paymentMode); //20160913 aridya add untuk payment mode
                        /*$('#lblInfoPeriod').fadeIn(2000);
                        $('#chartholder').fadeIn(2000);
                        $('#tableholder').fadeIn(2000);
                        $('#chartholder > .row').fadeIn(2000);*/
                        $('#legendholder').show().fadeIn(2000);


                    },
                    error: function () {
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });


            },
            error: function () {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);

            }
        });
    }

    //render chart from jquery plugin (jqplot) (continue...)
    function renderJqPlot(idx, title, placeholder, arraycompleted, arrayonprogress, arraywaitingACK1, arraywaitingACK2, arraycancelled, hour, xlabel) {
        //console.log(title);
        //console.log(placeholder);
        //console.log(array);
        //console.log(hour);

        plot[idx] = $.jqplot(placeholder, [arraycompleted, arrayonprogress, arraywaitingACK1, arraywaitingACK2, arraycancelled], {
            animate: true,
            animateReplot: true,
            stackSeries: true,
            title: title,
            seriesColors: ["#4bb2c5", "#eaa228", "#99ff66", "#ff00ff", "#c5b47f"],
            seriesDefaults: {
                renderer: $.jqplot.BarRenderer,
                rendererOptions: { barWidth: 25, barDirection: 'vertical', animation: { speed: 2500 } },
                pointLabels: { show: true, location: 's' }
            },
            /*legend:{
                show: true,
                location: 'e',
                placement: 'outsideGrid'
            },	*/
            grid: { background: '#006c6c' },
            axesDefaults: {
                pad: 0,
                labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                tickRenderer: $.jqplot.CategoryAxisTickRenderer,
                rendererOptions: {
                    forceTickAt0: true
                }
            },
            axes: {
                xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    tickRenderer: $.jqplot.CategoryAxisTickRenderer,
                    ticks: hour,
                    label: 'Time',

                },
                yaxis: {
                    pad: 1.05,
                    tickOptions: { formatString: '' },
                    label: 'Volume',
                    //renderer: $.jqplot.CategoryAxisRenderer,
                    //labelRenderer: $.jqplot.CanvasAxisLabelRenderer,					        	
                    rendererOptions: {
                        forceTickAt0: true
                    },
                    padMin: 0,
                    Min: 0
                },


            },
            highlighter: {
                show: true,
                tooltipLocation: 'n',
                sizeAdjust: 2,
                tooltipOffset: 9,
                showMarker: false,
                tooltipAxes: 'xy',
                fadeTooltip: true,
                formatString: '<table class="jqplot-highlighter">\
				    				  <tr><td class="xLabelTooltipR">'+ xlabel + '</td><td class="xLabelTooltipL">%s</td></tr>\
				    				  <tr><td class="yLabelTooltipR">Volume : '+ " " + '</td><td class="yLabelTooltipL">%d</td></tr></table>'



            }


        });
        $('.jqplot-highlighter-tooltip').addClass('ui-corner-all');
        hideZeroStackValue();



    }



    //create table product dinamically, according to the summary of product 
    function renderPeriodLastUpdate(startDate, endDate, clientDateTime, paymentMode) { //20160913 aridya add untuk payment mode
        var url = api.server + api.url.volumematrixproduct;
        var options = {
            params: {
                productTypeDetailID: ProductTypeDetailID,
                isTAT: false,
                startDate: startDate,
                endDate: endDate,
                clientDateTime: clientDateTime,
                paymentMode: paymentMode //20160913 aridya add untuk payment mode
            }
        };
        if (options.params != null || options.params != undefined) {
            url += DecodeParams(options.params);
        }

        $.ajax({
            async: false,
            url: url,
            type: "GET",
            contentType: "application/json",
            header: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (resultProduct) {

                var strPeriod; var strPeriodUTC;
                var period;
                var arrProduct = [];
                var arrProductName = [];
                var arrSLATime = [];
                var objTransactionList = new Array();
                var ExceptionalCount = 0;
                if (resultProduct.length > 0) {
                    for (var i = 0, max = resultProduct.length; i < max; i++) {
                        var productName = resultProduct[i].productName;
                        var trxDate = resultProduct[i].trxDate;
                        var actualDate = resultProduct[i].actualDate;
                        var slaTime = resultProduct[i].SLATime;
                        var TAT;
                        if (i == 0) {
                            ExceptionalCount = resultProduct[i].ExceptionalCount;
                        }
                        strPeriod = resultProduct[0].period;


                        //console.log(strPeriod);
                        //console.log(ExceptionalCount);
                        //console.log(productName + " " + trxDate + " " + actualDate);
                        arrProduct.push(productName);

                        objTransaction = GetObjectByName(objTransactionList, productName);
                        if (objTransaction == null) {
                            objTransaction = {
                                "name": productName, "sla": new Array()
                            };
                            objTransactionList.push(objTransaction);

                        }
                        objTransaction.sla.push(slaTime);



                    }


                    for (var i = 0, max = objTransactionList.length; i < max; i++) {
                        //console.log(objTransactionList[i].sla[0]);
                        arrSLATime.push(objTransactionList[i].sla[0]);

                    }
                    renderTableProduct(arrProduct, arrProductName, arrSLATime);

                    $("#lblCumulativeExceptional").text('Cumulative Exceptional Transaction : ' + ExceptionalCount);

                    toggleClassPeriod();
                    period = restructureDate(strPeriod, false); injectDatePeriod(period);
                    period = ""; period = restructureDate(strPeriod, true); injectDateTimePeriod(period);



                }
            },
            error: function () {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }


    //create table product dinamically, according to the summary of product (continue....)
    function renderTableProduct(arrProduct, arrProductName, arrSLATime) {
        var sla2Time;
        for (var i = 0, max = arrProduct.length; i < max; i++) {

            if (arrProductName.indexOf(arrProduct[i]) == -1) {
                arrProductName.push(arrProduct[i]);
            }
        }

        var rowHolderTable = $('<div class="row" id="summTransactions"></div>');

        rowHolderTable.appendTo("#tableholder");

        for (var i = 0, max = arrProductName.length; i < max; i++) {
            sla2Time = parseInt(arrSLATime[i]) * 2;
            //console.log(arrProductName[i]);
            var lblProduct = $('<div class="col-xs-12" style="float:right;left:10px;"><div id="tableholder' + i + '"></div></div>');

            lblProduct.appendTo(rowHolderTable);

            var lblAvgTAT = $('<div class="widget-box"><div class="widget-header"><h5>' + arrProductName[i] + '</h5><div class="widget-toolbar">' +
							   '</div></div><div class="widget-body"><div class="widget-main"><p class="alert alert-info" style="font-size:12px"> AVG TAT </p>' +
							   '<div id="holderAVGTAT' + arrProductName[i].replace(' ', '') + '" style="margin-left:95px; margin-top:-60px;width:90px;height :30px;background-color:#4f99c6;"><label id="lblAVGTAT' + arrProductName[i].replace(' ', '') + '" style="color:#fff;font-size:14; margin-left:10px;margin-top:5px;"></label></div>' +
							   '<p class="alert alert-info" style="font-size:12px"> SLA Indicator <button id="btnProduct' + i + '" type="button" class="btn btn-sm btn-primary pull-right" onclick="$(this).getProductName(this)">View</button></p></div></div></div>' +
							   '<div style="color:#3a87ad;margin-left:108px; margin-top:-60px; width:80px; height:30px; background-color:#5cb85c;"></div>' +
							   '<div style="color:#3a87ad;margin-left:187px; margin-top:-30px; width:80px; height:30px; background-color:#d15b47;"></div>' +
							   '<div id="flagSLA' + arrProductName[i].replace(' ', '') + '" style="color:#3a87ad; margin-top:-30px; width:3px; height:30px; background-color:#404040;"></div>' +
							   '<div style="color:#3a87ad;margin-left:108px; margin-top:-3px; width:80px; height:30px;">0</div>' +
							   '<div style="color:#3a87ad;margin-left:185px; margin-top:-30px; width:80px; height:30px;">' + arrSLATime[i] + '</div>' +
							   '<div id="MaxTime' + arrProductName[i].replace(' ', '') + '" style="color:#3a87ad;margin-left:265px; margin-top:-30px; width:80px; height:30px;">' + sla2Time + ' </div>' +
							   '<div id="flagSLACaret' + arrProductName[i].replace(' ', '') + '" class="icon-caret-down" style="display:block; margin-top:-66px; width:3px;"></div><br/><br/><br/><br/>');

            lblAvgTAT.appendTo("#tableholder" + i);

            $("#flagSLA" + arrProductName[i]).hide();
            $("#flagSLACaret" + arrProductName[i]).hide();

        }


        var lblCumulativeException = $('<div class="col-xs-11" style="height:30px; margin-top:50px; margin-left:20px; padding:1px; background-color:#d15b47; color: #fff"><p style="margin:5px;" id="lblCumulativeExceptional"></p></div>');

        var lblCumulativeExceptionExist = $('#summTransactions > .col-xs-11');
        if (lblCumulativeExceptionExist.length) {
        } else {
            lblCumulativeException.appendTo("#summTransactions");
        }
        renderAVGTAT();

    }
    //insert Average TAT & Cumulative Exceptional Transaction
    function renderAVGTAT() {
        var url = api.server + api.url.volumematrixproduct;

        var options = {
            params: {
                productTypeDetailID: ProductTypeDetailID,
                isTAT: true,
                startDate: startDate,
                endDate: endDate,
                clientDateTime: clientDateTime,
                paymentMode: paymentMode //20160913 aridya add untuk payment mode
            }
        };
        //console.log(options.params.isTAT + " " + options.params.startDate + " " + options.params.endDate + " " + options.params.clientDateTime);
        if (options.params != null || options.params != undefined) {
            url += DecodeParams(options.params);
        }
        $.ajax({
            async: false,
            url: url,
            type: "GET",
            contentType: "application/json",
            header: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (resultTAT) {

                renderAVGTATSuccess(resultTAT);

            },
            error: function () {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });

    }
    // on AJAX call renderAVGTAT function with success result	
    function renderAVGTATSuccess(resultTAT) {
        var arrProduct = [];
        var arrProductName = [];
        var objTATList = new Array();
        //console.log(resultTAT);
        if (resultTAT.length > 0) {
            for (var i = 0, max = resultTAT.length; i < max; i++) {
                var productName = resultTAT[i].productName;
                var TATValue = resultTAT[i].TAT;
                var SLATime = resultTAT[i].SLATime;

                //console.log(productName + " " + TATValue + " " + SLATime);
                objTAT = GetObjectByName(objTATList, productName)
                if (objTAT == null) {
                    objTAT = {
                        "name": productName, "tat": new Array()
                    };
                    objTATList.push(objTAT);

                }
                objTAT.tat.push(TATValue);


            }

            var rawAVGTAT = []; var strAVGTAT = []; var AVGTAT = [];
            var sumTAT; var sumTransaction;
            var hourTAT; var minuteTAT;
            var strHourTAT; var strMinuteTAT;
            var TATMax = []; var strRawHourTAT;
            var arrHourTAT;

            for (var j = 0; j < objTATList.length; j++) {
                sumTAT = 0;
                sumTransaction = 0;
                TATMax[j] = 0;
                for (var k = 0; k < objTATList[j].tat.length; k++) {
                    sumTAT = sumTAT + parseInt(objTATList[j].tat[k]);
                    sumTransaction = sumTransaction + 1;
                    //console.log(objTATList[j].tat[k]);

                    if (TATMax[j] < parseInt(objTATList[j].tat[k], 10))
                        TATMax[j] = parseInt(objTATList[j].tat[k], 10);

                }
                //console.log("Max :" + j + " : " + TATMax[j]);
                //console.log(objTATList[j].name + " " + sumTAT + " " + sumTransaction);				

                rawAVGTAT[j] = sumTAT / sumTransaction;

                //console.log(rawAVGTAT[j]);

                AVGTAT = rawAVGTAT[j] / 3600;


                if (AVGTAT.toString().indexOf(".") > -1 || AVGTAT.toString().indexOf(",") > -1) {
                    var splitter = AVGTAT.toString().indexOf(".") > -1 ? "." : ",";
                    strRawHourTAT = AVGTAT.toString().split(splitter);
                    strMinuteTAT = "0." + strRawHourTAT[1];
                    arrHourTAT = parseInt(strRawHourTAT[0], 10);

                } else {

                    strRawHourTAT = AVGTAT.toString();
                    strMinuteTAT = "0";
                    arrHourTAT = parseInt(strRawHourTAT, 10);
                }
                //console.log(AVGTAT);

                if (strMinuteTAT.indexOf(".") > -1) {

                    arrMinuteTAT = parseFloat(strMinuteTAT.substring(0, 4)) * 60;
                    arrMinuteTAT = Math.floor(arrMinuteTAT);
                    //console.log(arrMinuteTAT.toString());
                    if (arrMinuteTAT.toString().indexOf(".") > -1) {
                        strArrMinuteTAT = arrMinuteTAT.toString().split(".");
                        minuteTAT = parseInt(strArrMinuteTAT[0], 10);
                        //console.log(minuteTAT);
                    } else {
                        strArrMinuteTAT = arrMinuteTAT;
                        minuteTAT = parseInt(strArrMinuteTAT, 10);
                    }
                } else {
                    minuteTAT = 0;

                }

                //console.log(arrHourTAT);
                if (arrHourTAT.toString().length > 0) {
                    hourTAT = arrHourTAT;

                    if ((hourTAT.toString().length > 1) && (hourTAT > 0)) {
                        strHourTAT = hourTAT.toString();
                    } else {
                        strHourTAT = "0" + hourTAT.toString();
                    }

                } else {
                    hourTAT = 0;
                    strHourTAT = "00";

                }

                if (minuteTAT.toString().length > 1) {

                    strMinuteTAT = minuteTAT.toString().substring(0, 2);

                } else {

                    strMinuteTAT = "0" + minuteTAT.toString();
                }

                //console.log(strMinuteTAT);

                strAVGTAT[j] = strHourTAT + ":" + strMinuteTAT + ":00";

                $("#lblAVGTAT" + $.trim(objTATList[j].name.replace(' ', ''))).text(strAVGTAT[j]);


                //remark SLA is flexible according Time Transaction
                var MaxTime = [];
                MaxTime[j] = $("#MaxTime" + $.trim(objTATList[j].name.replace(' ', ''))).text();

                var MarginLeft = [];

                MarginLeft[j] = 160 * ((parseFloat(TATMax[j] / 3600).toFixed(2)) / parseInt(MaxTime[j])) + 105;
                //console.log(TATMax[j]);
                //console.log(MarginLeft[j]);
                $("#flagSLA" + $.trim(objTATList[j].name.replace(' ', ''))).show();
                $("#flagSLA" + $.trim(objTATList[j].name.replace(' ', ''))).css("margin-left", MarginLeft[j] + "px");

                $("#flagSLACaret" + $.trim(objTATList[j].name.replace(' ', ''))).show();
                $("#flagSLACaret" + $.trim(objTATList[j].name.replace(' ', ''))).css("margin-left", MarginLeft[j] - 2 + "px");


                if ((MarginLeft[j] > 265)) {
                    $("#flagSLA" + $.trim(objTATList[j].name.replace(' ', ''))).css("margin-left", 265 + "px");
                    $("#flagSLACaret" + $.trim(objTATList[j].name.replace(' ', ''))).css("margin-left", 265 - 2 + "px");


                }
                if ((MarginLeft[j] < 105) || (MarginLeft[j] < 0)) {
                    $("#flagSLA" + $.trim(objTATList[j].name.replace(' ', ''))).css("margin-left", 105 + "px");
                    $("#flagSLACaret" + $.trim(objTATList[j].name.replace(' ', ''))).css("margin-left", 105 - 2 + "px");


                }


            }

            showFlagAndCaretTAT();


        } else {
            showFlagAndCaretTAT();
        }


    }

    // substract TAT Hour from JSON data
    function substractTAT(trxDate, actualDate) {
        var newTrxDate = new Date(trxDate);
        var newActualDate = new Date(actualDate);
        var strTAT;
        var hour;
        var minute;

        var TAT = Math.abs(newActualDate - newTrxDate);

        TAT = TAT / (60 * 1000);

        TAT = Math.abs(TAT);

        //console.log(TAT);	  	

        arrTAT = TAT.toString();

        arrTAT = arrTAT.split(".");


        TAT = arrTAT[0];

        return TAT;

    }
    //hide stack value for 0 or 0.0 value
    function hideZeroStackValue() {

        $(".jqplot-point-label").each(
               function () {
                   var content = $(this).text();

                   //Get the length of the content;
                   //var contentLength = $.trim( content ).length;
                   if ((content == "0") || (content == "0.0")) {
                       $(this).hide();
                   }
               }
        );

    }
    function showFlagAndCaretTAT() {

        $("[id *= 'lblAVGTAT']").each(

            function () {

                var content = $(this).text();

                //Get the length of the content;
                //var contentLength = $.trim( content ).length;
                if (content == "") {
                    var contentBlank = $(this).attr('id');
                    var productContentBlank = contentBlank.substring(9, contentBlank.length);

                    $("#lblAVGTAT" + productContentBlank).css("display", "block");

                    $("#lblAVGTAT" + productContentBlank).text("-");

                    $("#flagSLA" + productContentBlank).css("display", "block");
                    $("#flagSLACaret" + productContentBlank).css("display", "block");

                    $("#flagSLA" + productContentBlank).css("margin-left", 105 + "px");
                    $("#flagSLACaret" + productContentBlank).css("margin-left", 105 - 2 + "px");

                }
            }
        );


    }

    var GetObjectByName = function (hash, name) {
        result = null;
        $.each(hash, function (index, item) {
            if (item.name == name) result = item;
        });
        return result;
    }
    //change class through add class
    function toggleClassPeriod() {

        $("#lblInfoPeriod").toggleClass("alert alert-info");

    }
    //insert html tag for period (date only)
    function injectDatePeriod(period) {

        $("#lblDashboardReport").html("<b>Dashboard Report</b><br><b>Period</b>" + " <b>" + period + "</b>");

    }

    //insert html tag for period (date time with amp/pm)
    function injectDateTimePeriod(period) {

        $("#lblLastUpdate").html("<b>Last Update :</b><br/>" + "<b>" + period + "</b>");
    }

    // restructur date time from JSON to yyyy/MM/dd (date) or yyyy/MM/dd HH:mm:ss (date time)
    function restructureDate(period, isDateTime) {
        var arrDateTimePeriod = [];
        var arrDatePeriod = []; var arrTimePeriod = []; var ampm = [];
        var arrBuffer = [];
        var year; var hour;
        var month; var minute;
        var day; var second; var ampm;

        var ret;
        if (!isDateTime) {
            arrDateTimePeriod = period.split(" ");
            arrDatePeriod = arrDateTimePeriod[0];
            arrBuffer = arrDatePeriod.split("/");
            year = arrBuffer[2]; month = arrBuffer[0]; day = arrBuffer[1];

            ret = year + "/" + month + "/" + day;
        } else {

            arrDateTimePeriod = period.split(" ");
            arrDatePeriod = arrDateTimePeriod[0];
            arrTimePeriod = arrDateTimePeriod[1];
            ampm = arrDateTimePeriod[2];
            arrBuffer = arrDatePeriod.split("/");
            year = arrBuffer[2]; month = arrBuffer[0]; day = arrBuffer[1];

            arrBuffer = [];
            arrBuffer = arrTimePeriod.split(":");
            hour = arrBuffer[0]; minute = arrBuffer[1]; second = arrBuffer[2];

            ampm = ampm.toLowerCase();
            if (ampm.indexOf("am") > -1 || ampm.indexOf("pm") > -1)
                ampm = ampm.toUpperCase();
            else
                ampm = "";
            ret = year + "/" + month + "/" + day + " " + hour + "." + minute + " " + ampm;
        }
        return ret;
    }

};

var viewModel = new ViewModel();
$(document).ready(function () {
    /* $('#lblInfoPeriod').fadeOut(1000);
     $('#chartholder').fadeOut(1000);
     $('#tableholder').fadeOut(1000);*/
    $box = $('#widget-box');
    var event;
    $box.trigger(event = $.Event('reload.ace.widget'))
    if (event.isDefaultPrevented()) return
    $box.blur();

    var GetObjectByName = function (hash, name) {
        result = null;
        $.each(hash, function (index, item) {
            if (item.name == name) result = item;
        });
        return result;
    }


    if ($.cookie(api.cookie.name) == undefined) {
        Helper.Token.Request(TokenOnSuccess, TokenOnError);


        if ($("#lblDateFromServer").text() == '') {
            getDateFromServer();
            dateNowFromServer = $("#lblDateFromServer").text().trim();
            strStartDate = new Date(dateNowFromServer);
            strEndDate = new Date(dateNowFromServer);
            if ((strStartDate.getMonth() + 1).toString().length == 1) {
                startMonth = "0" + (strStartDate.getMonth() + 1).toString();
            } else {
                startMonth = (strStartDate.getMonth() + 1).toString();
            }

            if ((strEndDate.getMonth() + 1).toString().length == 1) {
                endMonth = "0" + (strEndDate.getMonth() + 1).toString();

            } else {
                endMonth = (strEndDate.getMonth() + 1).toString();

            }

            if (strStartDate.getDate().toString().length == 1) {
                startDay = "0" + strStartDate.getDate().toString();
            } else {
                startDay = strStartDate.getDate().toString();
            }

            if (strEndDate.getDate().toString().length == 1) {
                endDay = "0" + strEndDate.getDate().toString();

            } else {
                endDay = strEndDate.getDate().toString();

            }

            startDate = strStartDate.getFullYear() + "/" + startMonth + "/" + startDay + "T00:00:00.000";
            endDate = strEndDate.getFullYear() + "/" + endMonth + "/" + endDay + "T23:59:59.999";

        }

    } else {
        // read token from cookie
        accessToken = $.cookie(api.cookie.name);

        // call spuser function
        GetCurrentUser(accessToken);
        // call get data inside view model

        if ($("#lblDateFromServer").text() == '') {
            getDateFromServer();
            dateNowFromServer = $("#lblDateFromServer").text().trim();
            strStartDate = new Date(dateNowFromServer);
            strEndDate = new Date(dateNowFromServer);

            if ((strStartDate.getMonth() + 1).toString().length == 1) {
                startMonth = "0" + (strStartDate.getMonth() + 1).toString();
            } else {
                startMonth = (strStartDate.getMonth() + 1).toString();
            }

            if ((strEndDate.getMonth() + 1).toString().length == 1) {
                endMonth = "0" + (strEndDate.getMonth() + 1).toString();

            } else {
                endMonth = (strEndDate.getMonth() + 1).toString();

            }

            if (strStartDate.getDate().toString().length == 1) {
                startDay = "0" + strStartDate.getDate().toString();
            } else {
                startDay = strStartDate.getDate().toString();
            }

            if (strEndDate.getDate().toString().length == 1) {
                endDay = "0" + strEndDate.getDate().toString();

            } else {
                endDay = strEndDate.getDate().toString();

            }


            startDate = strStartDate.getFullYear() + "/" + startMonth + "/" + startDay + "T00:00:00.000";
            endDate = strEndDate.getFullYear() + "/" + endMonth + "/" + endDay + "T23:59:59.999";

        }





    }
    $.fn.ForceNumericOnly = function () {

        return this.each(function () {

            $(this).keydown(function (e) {

                var key = e.charCode || e.keyCode || 0;

                return (

    				key == 8 ||
    				key == 9 ||
    				key == 13 ||
    				key == 46 ||
    				key == 110 ||
    				key == 190 ||
    				(key >= 35 && key <= 40) ||
    				(key >= 48 && key <= 57) ||
    				(key >= 96 && key <= 105));

            });

        });

    };
    function clearContent() {
        /*$('#lblInfoPeriod').fadeOut(500);
        $('#chartholder').fadeOut(500);
        $('#tableholder').fadeOut(500);
        $('#chartholder > .row').fadeOut(500);*/
        if (plot) {
            $.each(plot, function (index, item) {
                item.destroy();
            });
        }
        $('#legendholder').hide().fadeOut(500);
        $('#lblInfoPeriod').removeClass('alert alert-info');
        $('#lblDashboardReport').text('');
        $('#lblLastUpdate').text('');
        $('#tableholder > .row').remove();
        $('[id*="chartHolder"]').remove();

        $('#chartcluster > .row').unbind();
        $('#chartcluster > .row').html('');
        
    }
    var counterRefresh = 3600000; //45000; //counter refresh value in milisecond, it's equals to 45 second
    if (counterRefresh == 0) {
        var counter = setInterval(function () {
            clearContent();
            getDateFromServer();
            clearInterval(counter);

        }, 1000);

    } else {
        setInterval(function () {
            clearContent();
            getDateFromServer();
        }, counterRefresh);
    }
    function getDateFromServer() {
        var newDateTime = new Date();
        var newMonth; var newDate;
        var newHour; var newMinute; var newSecond;

        newMonth = newDateTime.getMonth() + 1;
        newMonth = newMonth.toString().length == 2 ? newMonth : "0" + newMonth;
        newDate = newDateTime.getDate();
        newDate = newDate.toString().length == 2 ? newDate : "0" + newDate;
        newHour = newDateTime.getHours();
        newHour = newHour.toString().length == 2 ? newHour : "0" + newHour;
        newMinute = newDateTime.getMinutes();
        newMinute = newMinute.toString().length == 2 ? newMinute : "0" + newMinute;
        newSecond = newDateTime.getSeconds();
        newSecond = newSecond.toString().length == 2 ? newSecond : "0" + newSecond;

        clientDateTime = newDateTime.getFullYear() + "/" + newMonth + "/" + newDate + "T" + newHour + ":" + newMinute + ":" + newSecond;


        var options = {

            url: api.server + api.url.currentdate,
            token: accessToken,
            params: {
                clientDateTime: clientDateTime
            }

        };


        Helper.Ajax.Get(options, OnSuccessGetDateNow, OnError, null);



    }
    function OnSuccessGetDateNow(data, textStatus, jqXHR) {

        if (jqXHR.status = 200) {


            renderDateNow(data);


        }

    }
    function renderDateNow(data) {
        var dateNow; var dateNowLast; var arrDateNow = []; var strDateNowFromClient; var dateNowFromClient;
        if (data.length > 0) {
            for (var i = 0, max = data.length; i < max; i++) {
                if (i == 0) {
                    dateNow = data[i].clientDateTimeNow;

                    arrDateNow = dateNow.indexOf("T") > -1 ? dateNow.split("T") : dateNow.indexOf(" ") > -1 ? dateNow.split(" ") : dateNow.substring(0, 10);
                    dateNowLast = arrDateNow[0];
                    dateNowFromServer = dateNowLast;

                    strDateNowFromClient = new Date(dateNowFromServer);
                    dateNowFromClient = strDateNowFromClient.getFullYear() + "/" + (strDateNowFromClient.getMonth() + 1) + "/" + strDateNowFromClient.getDate() + dateNow.substring(dateNow.toString().indexOf("T"));
                    strStartDate = new Date(dateNowFromServer);
                    strEndDate = new Date(dateNowFromServer);
                    if ((strStartDate.getMonth() + 1).toString().length == 1) {
                        startMonth = "0" + (strStartDate.getMonth() + 1).toString();
                    } else {
                        startMonth = (strStartDate.getMonth() + 1).toString();
                    }

                    if ((strEndDate.getMonth() + 1).toString().length == 1) {
                        endMonth = "0" + (strEndDate.getMonth() + 1).toString();

                    } else {
                        endMonth = (strEndDate.getMonth() + 1).toString();

                    }

                    if (strStartDate.getDate().toString().length == 1) {
                        startDay = "0" + strStartDate.getDate().toString();
                    } else {
                        startDay = strStartDate.getDate().toString();
                    }

                    if (strEndDate.getDate().toString().length == 1) {
                        endDay = "0" + strEndDate.getDate().toString();

                    } else {
                        endDay = strEndDate.getDate().toString();

                    }

                    startDate = strStartDate.getFullYear() + "/" + startMonth + "/" + startDay + "T00:00:00.000";
                    endDate = strEndDate.getFullYear() + "/" + endMonth + "/" + endDay + "T23:59:59.999";
                    $("#lblDateFromServer").text(dateNowFromServer);
                    if (ProductTypeDetailID != undefined || ProductTypeDetailID != null) {
                        renderChart(ProductTypeDetailID, startDate, endDate, dateNow, paymentMode); //20160913 aridya add paymentMode
                    }

                }
            }
        }


    }

    //render chart from jquery plugin (jqplot)
    //20160913 aridya add paymentMode
    function renderChart(ProductTypeDetailID, startDate, endDate, clientDateTime, paymentMode) {

        var url = api.server + api.url.volumematrixdashboardchart;

        var options = {
            params: {
                productTypeDetailID: ProductTypeDetailID,
                startDate: startDate,
                endDate: endDate,
                clientDateTime: clientDateTime,
                paymentMode: paymentMode
            }
        };
        //console.log(options.params.productTypeDetailID);
        if (options.params != null || options.params != undefined) {
            url += DecodeParams(options.params);
        }
        $.ajax({
            async: false,
            url: url,
            type: 'GET',
            contentType: "application/json",
            header: {
                "Authorization": "Bearer " + accessToken,
            },
            success: function (resultChart) {
                //console.log(resultChart);
                var url = api.server + api.url.volumematrixdashboardchannel;
                var options = {
                    params: {
                        productTypeDetailID: ProductTypeDetailID,
                        startDate: startDate,
                        endDate: endDate,
                        clientDateTime: clientDateTime,
                        paymentMode: paymentMode
                    }
                };
                //end add aridya
                if (options.params != null || options.params != undefined) {
                    url += DecodeParams(options.params);
                }


                $.ajax({
                    async: false,
                    url: url,
                    type: "GET",
                    contentType: "application/json",
                    header: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (resultChannel) {
                        //console.log(resultChannel);
                        // Data Modelling //
                        var currentProduct = null;
                        var arr = [];
                        var rowHolder = null;
                        var rowHolderLegend = null;
                        var htlmLegend = null;
                        var chartCount = 0;
                        var chartsData = new Array();

                        if (resultChart.length > 0) {
                            for (var i = 0, max = resultChart.length; i < max; i++) {
                                var product = resultChart[i].productName;
                                var hour = resultChart[i].hour;
                                var qtycompleted = resultChart[i].qtyCompleted;
                                var qtyonprogress = resultChart[i].qtyOnProgress;
                                var qtycancelled = resultChart[i].qtyCancelled;
                                var qtywaitingACK1 = resultChart[i].qtyWaitingACK1;
                                var qtywaitingACK2 = resultChart[i].qtyWaitingACK2;
                                var createdDate = resultChart[i].createdDate;

                                if ((qtycompleted == 0) && (qtyonprogress == 0) && (qtycancelled == 0) && (qtywaitingACK1 == 0) && (qtywaitingACK2 == 0)) {
                                    hour = "";
                                }

                                selChartsData = GetObjectByName(chartsData, product);
                                if (selChartsData == null) {
                                    selChartsData = {
                                        "name": product, "hour": new Array(), "qtycompleted": new Array(), "qtyonprogress": new Array(), "qtycancelled": new Array(), "qtywaitingACK1": new Array(), "qtywaitingACK2": new Array(), "createdDate": new Array(), "xLabel": "Time : "
                                    };
                                    chartsData.push(selChartsData);

                                }


                                selChartsData.hour.push(hour);
                                selChartsData.qtycompleted.push(qtycompleted);
                                selChartsData.qtyonprogress.push(qtyonprogress);
                                selChartsData.qtycancelled.push(qtycancelled);
                                selChartsData.qtywaitingACK1.push(qtywaitingACK1);
                                selChartsData.qtywaitingACK2.push(qtywaitingACK2);


                            }
                        }
                        selChartsData = {
                            "name": "Channel", "hour": new Array(), "qtycompleted": new Array(), "qtyonprogress": new Array(), "qtycancelled": new Array(), "qtywaitingACK1": new Array(), "qtywaitingACK2": new Array(), "createdDate": new Array(), "xLabel": "Channel : "
                        };
                        if (resultChannel.length > 0) {
                            //console.log(resultChannel);
                            $.each(resultChannel, function (index, item) {
                                selChartsData.hour.push(item.channelName);
                                selChartsData.qtycompleted.push(item.qtyCompleted);
                                selChartsData.qtyonprogress.push(item.qtyOnProgress);
                                selChartsData.qtycancelled.push(item.qtyCancelled);
                                selChartsData.qtywaitingACK1.push(item.qtyWaitingACK1);
                                selChartsData.qtywaitingACK2.push(item.qtyWaitingACK2);


                            });
                        } else if (resultChannel.length <= 0) {

                            selChartsData.hour.push('');
                            selChartsData.qtycompleted.push(0);
                            selChartsData.qtyonprogress.push(0);
                            selChartsData.qtycancelled.push(0);
                            selChartsData.qtywaitingACK1.push(0);
                            selChartsData.qtywaitingACK2.push(0);

                        }
                        chartsData.push(selChartsData);

                        // Render View //
                        rowHolder = null;
                        rowHolderLegend = null;

                        $.each(chartsData, function (index, item) {

                            //console.log(item.name + " " + item.qtycompleted + " " + item.qtyonprogress + " " + item.qtycancelled);
                            if (index % 2 == 0) {
                                rowHolder = $('<div class="row"></div>');
                                rowHolder.appendTo("#chartcluster");
                            }
                            var chartHolder = $('<div class="col-xs-6 col-sm-6"><div id="chartHolder' + index + '" style="width:200px; height:270px;"></div></div>');
                            chartHolder.appendTo(rowHolder);

                            holder = $("#chartHolder" + index).attr("id");
                            renderJqPlot(index, item.name, holder, item.qtycompleted, item.qtyonprogress, item.qtywaitingACK1, item.qtywaitingACK2, item.qtycancelled, item.hour, item.xLabel);

                        });
                        plotNumber = chartsData.length - 1;
                        rowHolderLegend = $('<div id="legendholder" class="row"></div>');
                        rowHolderLegendExist = $('#legendholder');
                        if (rowHolderLegendExist.length) {
                        } else {
                            rowHolderLegend.appendTo("#legendcluster");
                        }//<br/><br/>&nbsp;&nbsp;&nbsp;&nbsp<label id='lblLegend' style='margin-left:40px;'>Legend :</label><br/><div class='col-xs-5 pull-left' style='margin-left:40px;'><div style='height:300px;'><table class='jqplot-table-legend' style='right: 0px; top: 0px; left:0px;'>" +
                        htmlLegend = $("<br/>&nbsp;&nbsp;&nbsp;&nbsp<label id='lblLegend' style='margin-left:40px;'>Legend :</label><br/><div class='col-xs-5 pull-left' style='margin-left:40px;'><div style='height:150px;'><table class='jqplot-table-legend' style='right: 0px; top: 0px; left:0px;'>" +
										"<tbody><tr class='jqplot-table-legend'>" +
										"<td class='jqplot-table-legend jqplot-table-legend-swatch' style='text-align: center; padding-top: 0px;'>" +
										"<div class='jqplot-table-legend-swatch-outline'>" +
										"<div class='jqplot-table-legend-swatch' style='border-color: rgb(75, 178, 197); background-color: rgb(75, 178, 197);'></div>" +
										"</div></td><td class='jqplot-table-legend jqplot-table-legend-label'>Completed Transactions</td></tr>" +
										"<tr class='jqplot-table-legend'>" +
										"<td class='jqplot-table-legend jqplot-table-legend-swatch' style='text-align: center;'>" +
										"<div class='jqplot-table-legend-swatch-outline'>" +
										"<div class='jqplot-table-legend-swatch' style='border-color: rgb(234, 162, 40); background-color: rgb(234, 162, 40);'></div>" +
										"</div></td><td class='jqplot-table-legend jqplot-table-legend-label' style='padding-top: 0px;'>In Progress Transactions in Work Flow</td></tr>" +
                                        "<tr class='jqplot-table-legend'>" +
										"<td class='jqplot-table-legend jqplot-table-legend-swatch' style='text-align: center;'>" +
										"<div class='jqplot-table-legend-swatch-outline'>" +
										"<div class='jqplot-table-legend-swatch' style='border-color: rgb(153, 255, 102); background-color: rgb(153, 255, 102);'></div>" +
										"</div></td><td class='jqplot-table-legend jqplot-table-legend-label' style='padding-top: 0px;'>In Progress In IPE (Waiting ACK1)</td></tr>" +
                                        "<tr class='jqplot-table-legend'>" +
										"<td class='jqplot-table-legend jqplot-table-legend-swatch' style='text-align: center;'>" +
										"<div class='jqplot-table-legend-swatch-outline'>" +
										"<div class='jqplot-table-legend-swatch' style='border-color: rgb(255, 0, 255); background-color:rgb(255, 0, 255);'></div>" +
										"</div></td><td class='jqplot-table-legend jqplot-table-legend-label' style='padding-top: 0px;'>In Progress In IPE (Waiting ACK2)</td></tr>" +
										"<tr class='jqplot-table-legend'>" +
										"<td class='jqplot-table-legend jqplot-table-legend-swatch' style='text-align: center;'>" +
										"<div class='jqplot-table-legend-swatch-outline'>" +
										"<div class='jqplot-table-legend-swatch' style='border-color: rgb(197, 180, 127); background-color: rgb(197, 180, 127);'></div>" +
										"</div></td><td class='jqplot-table-legend jqplot-table-legend-label' style='padding-top: 0px;'>Cancelled Transactions</td></tr></tbody></table></div></div>");

                        if (rowHolderLegendExist.length) {
                        } else {
                            htmlLegend.appendTo("#legendholder");
                        }

                        renderPeriodLastUpdate(startDate, endDate, clientDateTime, paymentMode); //20160913 aridya add untuk payment mode
                        /*$('#lblInfoPeriod').fadeIn(2000);
                        $('#chartholder').fadeIn(2000);
                        $('#tableholder').fadeIn(2000);
                        $('#chartholder > .row').fadeIn(2000);*/
                        $('#legendholder').show().fadeIn(2000);


                    },
                    error: function () {
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });


            },
            error: function () {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);

            }
        });
    }

    //render chart from jquery plugin (jqplot) (continue...)
    function renderJqPlot(idx, title, placeholder, arraycompleted, arrayonprogress, arraywaitingACK1, arraywaitingACK2, arraycancelled, hour, xlabel) {
        //console.log(title);
        //console.log(placeholder);
        //console.log(array);
        //console.log(hour);
        //$('[id*="chartHolder"]').html('');
        plot[idx] = $.jqplot(placeholder, [arraycompleted, arrayonprogress, arraywaitingACK1, arraywaitingACK2, arraycancelled], {
            animate: true,
            animateReplot: true,
            stackSeries: true,
            title: title,
            seriesColors: ["#4bb2c5", "#eaa228", "#99ff66", "#ff00ff", "#c5b47f"],
            seriesDefaults: {
                renderer: $.jqplot.BarRenderer,
                rendererOptions: { barWidth: 25, barDirection: 'vertical', animation: { speed: 2500 } },
                pointLabels: { show: true, location: 's' }
            },
            /*legend:{
                show: true,
                location: 'e',
                placement: 'outsideGrid'
            },	*/
            grid: { background: '#006c6c' },
            axesDefaults: {
                pad: 0,
                labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                tickRenderer: $.jqplot.CategoryAxisTickRenderer,
                rendererOptions: {
                    forceTickAt0: true
                }
            },
            axes: {
                xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    tickRenderer: $.jqplot.CategoryAxisTickRenderer,
                    ticks: hour,
                    label: 'Time',

                },
                yaxis: {
                    pad: 1.05,
                    tickOptions: { formatString: '' },
                    label: 'Volume',
                    //renderer: $.jqplot.CategoryAxisRenderer,
                    //labelRenderer: $.jqplot.CanvasAxisLabelRenderer,					        	
                    rendererOptions: {
                        forceTickAt0: true
                    },
                    padMin: 0,
                    Min: 0
                },


            },
            highlighter: {
                show: true,
                tooltipLocation: 'n',
                sizeAdjust: 2,
                tooltipOffset: 9,
                showMarker: false,
                tooltipAxes: 'xy',
                fadeTooltip: true,
                formatString: '<table class="jqplot-highlighter">\
				    				  <tr><td class="xLabelTooltipR">'+ xlabel + '</td><td class="xLabelTooltipL">%s</td></tr>\
				    				  <tr><td class="yLabelTooltipR">Volume : '+ " " + '</td><td class="yLabelTooltipL">%d</td></tr></table>'



            }


        });
        $('.jqplot-highlighter-tooltip').addClass('ui-corner-all');
        hideZeroStackValue();



    }



    //create table product dinamically, according to the summary of product 
    function renderPeriodLastUpdate(startDate, endDate, clientDateTime, paymentMode) { //20160913 aridya add untuk payment mode
        var url = api.server + api.url.volumematrixproduct;
        var options = {
            params: {
                productTypeDetailID: ProductTypeDetailID,
                isTAT: false,
                startDate: startDate,
                endDate: endDate,
                clientDateTime: clientDateTime,
                paymentMode: paymentMode //20160913 aridya add untuk payment mode
            }
        };
        if (options.params != null || options.params != undefined) {
            url += DecodeParams(options.params);
        }

        $.ajax({
            async: false,
            url: url,
            type: "GET",
            contentType: "application/json",
            header: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (resultProduct) {

                var strPeriod; var strPeriodUTC;
                var period;
                var arrProduct = [];
                var arrProductName = [];
                var arrSLATime = [];
                var objTransactionList = new Array();
                var ExceptionalCount = 0;
                if (resultProduct.length > 0) {
                    for (var i = 0, max = resultProduct.length; i < max; i++) {
                        var productName = resultProduct[i].productName;
                        var trxDate = resultProduct[i].trxDate;
                        var actualDate = resultProduct[i].actualDate;
                        var slaTime = resultProduct[i].SLATime;
                        var TAT;
                        if (i == 0) {
                            ExceptionalCount = resultProduct[i].ExceptionalCount;
                        }
                        strPeriod = resultProduct[0].period;


                        //console.log(strPeriod);
                        //console.log(ExceptionalCount);
                        //console.log(productName + " " + trxDate + " " + actualDate);
                        arrProduct.push(productName);

                        objTransaction = GetObjectByName(objTransactionList, productName);
                        if (objTransaction == null) {
                            objTransaction = {
                                "name": productName, "sla": new Array()
                            };
                            objTransactionList.push(objTransaction);

                        }
                        objTransaction.sla.push(slaTime);



                    }


                    for (var i = 0, max = objTransactionList.length; i < max; i++) {
                        //console.log(objTransactionList[i].sla[0]);
                        arrSLATime.push(objTransactionList[i].sla[0]);

                    }
                    renderTableProduct(arrProduct, arrProductName, arrSLATime);

                    $("#lblCumulativeExceptional").text('Cumulative Exceptional Transaction : ' + ExceptionalCount);

                    toggleClassPeriod();
                    period = restructureDate(strPeriod, false); injectDatePeriod(period);
                    period = ""; period = restructureDate(strPeriod, true); injectDateTimePeriod(period);



                }
            },
            error: function () {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }


    //create table product dinamically, according to the summary of product (continue....)
    function renderTableProduct(arrProduct, arrProductName, arrSLATime) {
        var sla2Time;
        for (var i = 0, max = arrProduct.length; i < max; i++) {

            if (arrProductName.indexOf(arrProduct[i]) == -1) {
                arrProductName.push(arrProduct[i]);
            }
        }

        var rowHolderTable = $('<div class="row" id="summTransactions"></div>');

        rowHolderTable.appendTo("#tableholder");

        for (var i = 0, max = arrProductName.length; i < max; i++) {
            sla2Time = parseInt(arrSLATime[i]) * 2;
            //console.log(arrProductName[i]);
            var lblProduct = $('<div class="col-xs-12" style="float:right;left:10px;"><div id="tableholder' + i + '"></div></div>');

            lblProduct.appendTo(rowHolderTable);

            var lblAvgTAT = $('<div class="widget-box"><div class="widget-header"><h5>' + arrProductName[i] + '</h5><div class="widget-toolbar">' +
							   '</div></div><div class="widget-body"><div class="widget-main"><p class="alert alert-info" style="font-size:12px"> AVG TAT </p>' +
							   '<div id="holderAVGTAT' + arrProductName[i].replace(' ', '') + '" style="margin-left:95px; margin-top:-60px;width:90px;height :30px;background-color:#4f99c6;"><label id="lblAVGTAT' + arrProductName[i].replace(' ', '') + '" style="color:#fff;font-size:14; margin-left:10px;margin-top:5px;"></label></div>' +
							   '<p class="alert alert-info" style="font-size:12px"> SLA Indicator <button id="btnProduct' + i + '" type="button" class="btn btn-sm btn-primary pull-right" onclick="$(this).getProductName(this)">View</button></p></div></div></div>' +
							   '<div style="color:#3a87ad;margin-left:108px; margin-top:-60px; width:80px; height:30px; background-color:#5cb85c;"></div>' +
							   '<div style="color:#3a87ad;margin-left:187px; margin-top:-30px; width:80px; height:30px; background-color:#d15b47;"></div>' +
							   '<div id="flagSLA' + arrProductName[i].replace(' ', '') + '" style="color:#3a87ad; margin-top:-30px; width:3px; height:30px; background-color:#404040;"></div>' +
							   '<div style="color:#3a87ad;margin-left:108px; margin-top:-3px; width:80px; height:30px;">0</div>' +
							   '<div style="color:#3a87ad;margin-left:185px; margin-top:-30px; width:80px; height:30px;">' + arrSLATime[i] + '</div>' +
							   '<div id="MaxTime' + arrProductName[i].replace(' ', '') + '" style="color:#3a87ad;margin-left:265px; margin-top:-30px; width:80px; height:30px;">' + sla2Time + ' </div>' +
							   '<div id="flagSLACaret' + arrProductName[i].replace(' ', '') + '" class="icon-caret-down" style="display:block; margin-top:-66px; width:3px;"></div><br/><br/><br/><br/>');

            lblAvgTAT.appendTo("#tableholder" + i);

            $("#flagSLA" + arrProductName[i]).hide();
            $("#flagSLACaret" + arrProductName[i]).hide();

        }


        var lblCumulativeException = $('<div class="col-xs-11" style="height:30px; margin-top:50px; margin-left:20px; padding:1px; background-color:#d15b47; color: #fff"><p style="margin:5px;" id="lblCumulativeExceptional"></p></div>');

        var lblCumulativeExceptionExist = $('#summTransactions > .col-xs-11');
        if (lblCumulativeExceptionExist.length) {
        } else {
            lblCumulativeException.appendTo("#summTransactions");
        }
        renderAVGTAT();

    }
    //insert Average TAT & Cumulative Exceptional Transaction
    function renderAVGTAT() {
        var url = api.server + api.url.volumematrixproduct;

        var options = {
            params: {
                productTypeDetailID: ProductTypeDetailID,
                isTAT: true,
                startDate: startDate,
                endDate: endDate,
                clientDateTime: clientDateTime,
                paymentMode: paymentMode //20160913 aridya add untuk payment mode
            }
        };
        //console.log(options.params.isTAT + " " + options.params.startDate + " " + options.params.endDate + " " + options.params.clientDateTime);
        if (options.params != null || options.params != undefined) {
            url += DecodeParams(options.params);
        }
        $.ajax({
            async: false,
            url: url,
            type: "GET",
            contentType: "application/json",
            header: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (resultTAT) {

                renderAVGTATSuccess(resultTAT);

            },
            error: function () {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });

    }
    // on AJAX call renderAVGTAT function with success result	
    function renderAVGTATSuccess(resultTAT) {
        var arrProduct = [];
        var arrProductName = [];
        var objTATList = new Array();
        //console.log(resultTAT);
        if (resultTAT.length > 0) {
            for (var i = 0, max = resultTAT.length; i < max; i++) {
                var productName = resultTAT[i].productName;
                var TATValue = resultTAT[i].TAT;
                var SLATime = resultTAT[i].SLATime;

                //console.log(productName + " " + TATValue + " " + SLATime);
                objTAT = GetObjectByName(objTATList, productName)
                if (objTAT == null) {
                    objTAT = {
                        "name": productName, "tat": new Array()
                    };
                    objTATList.push(objTAT);

                }
                objTAT.tat.push(TATValue);


            }

            var rawAVGTAT = []; var strAVGTAT = []; var AVGTAT = [];
            var sumTAT; var sumTransaction;
            var hourTAT; var minuteTAT;
            var strHourTAT; var strMinuteTAT;
            var TATMax = []; var strRawHourTAT;
            var arrHourTAT;

            for (var j = 0; j < objTATList.length; j++) {
                sumTAT = 0;
                sumTransaction = 0;
                TATMax[j] = 0;
                for (var k = 0; k < objTATList[j].tat.length; k++) {
                    sumTAT = sumTAT + parseInt(objTATList[j].tat[k]);
                    sumTransaction = sumTransaction + 1;
                    //console.log(objTATList[j].tat[k]);

                    if (TATMax[j] < parseInt(objTATList[j].tat[k], 10))
                        TATMax[j] = parseInt(objTATList[j].tat[k], 10);

                }
                //console.log("Max :" + j + " : " + TATMax[j]);
                //console.log(objTATList[j].name + " " + sumTAT + " " + sumTransaction);				

                rawAVGTAT[j] = sumTAT / sumTransaction;

                //console.log(rawAVGTAT[j]);

                AVGTAT = rawAVGTAT[j] / 3600;


                if (AVGTAT.toString().indexOf(".") > -1 || AVGTAT.toString().indexOf(",") > -1) {
                    var splitter = AVGTAT.toString().indexOf(".") > -1 ? "." : ",";
                    strRawHourTAT = AVGTAT.toString().split(splitter);
                    strMinuteTAT = "0." + strRawHourTAT[1];
                    arrHourTAT = parseInt(strRawHourTAT[0], 10);

                } else {

                    strRawHourTAT = AVGTAT.toString();
                    strMinuteTAT = "0";
                    arrHourTAT = parseInt(strRawHourTAT, 10);
                }
                //console.log(AVGTAT);

                if (strMinuteTAT.indexOf(".") > -1) {

                    arrMinuteTAT = parseFloat(strMinuteTAT.substring(0, 4)) * 60;
                    arrMinuteTAT = Math.floor(arrMinuteTAT);
                    //console.log(arrMinuteTAT.toString());
                    if (arrMinuteTAT.toString().indexOf(".") > -1) {
                        strArrMinuteTAT = arrMinuteTAT.toString().split(".");
                        minuteTAT = parseInt(strArrMinuteTAT[0], 10);
                        //console.log(minuteTAT);
                    } else {
                        strArrMinuteTAT = arrMinuteTAT;
                        minuteTAT = parseInt(strArrMinuteTAT, 10);
                    }
                } else {
                    minuteTAT = 0;

                }

                //console.log(arrHourTAT);
                if (arrHourTAT.toString().length > 0) {
                    hourTAT = arrHourTAT;

                    if ((hourTAT.toString().length > 1) && (hourTAT > 0)) {
                        strHourTAT = hourTAT.toString();
                    } else {
                        strHourTAT = "0" + hourTAT.toString();
                    }

                } else {
                    hourTAT = 0;
                    strHourTAT = "00";

                }

                if (minuteTAT.toString().length > 1) {

                    strMinuteTAT = minuteTAT.toString().substring(0, 2);

                } else {

                    strMinuteTAT = "0" + minuteTAT.toString();
                }

                //console.log(strMinuteTAT);

                strAVGTAT[j] = strHourTAT + ":" + strMinuteTAT + ":00";

                $("#lblAVGTAT" + $.trim(objTATList[j].name.replace(' ', ''))).text(strAVGTAT[j]);


                //remark SLA is flexible according Time Transaction
                var MaxTime = [];
                MaxTime[j] = $("#MaxTime" + $.trim(objTATList[j].name.replace(' ', ''))).text();

                var MarginLeft = [];

                MarginLeft[j] = 160 * ((parseFloat(TATMax[j] / 3600).toFixed(2)) / parseInt(MaxTime[j])) + 105;
                //console.log(TATMax[j]);
                //console.log(MarginLeft[j]);
                $("#flagSLA" + $.trim(objTATList[j].name.replace(' ', ''))).show();
                $("#flagSLA" + $.trim(objTATList[j].name.replace(' ', ''))).css("margin-left", MarginLeft[j] + "px");

                $("#flagSLACaret" + $.trim(objTATList[j].name.replace(' ', ''))).show();
                $("#flagSLACaret" + $.trim(objTATList[j].name.replace(' ', ''))).css("margin-left", MarginLeft[j] - 2 + "px");


                if ((MarginLeft[j] > 265)) {
                    $("#flagSLA" + $.trim(objTATList[j].name.replace(' ', ''))).css("margin-left", 265 + "px");
                    $("#flagSLACaret" + $.trim(objTATList[j].name.replace(' ', ''))).css("margin-left", 265 - 2 + "px");


                }
                if ((MarginLeft[j] < 105) || (MarginLeft[j] < 0)) {
                    $("#flagSLA" + $.trim(objTATList[j].name.replace(' ', ''))).css("margin-left", 105 + "px");
                    $("#flagSLACaret" + $.trim(objTATList[j].name.replace(' ', ''))).css("margin-left", 105 - 2 + "px");


                }


            }

            showFlagAndCaretTAT();


        } else {
            showFlagAndCaretTAT();
        }


    }

    // substract TAT Hour from JSON data
    function substractTAT(trxDate, actualDate) {
        var newTrxDate = new Date(trxDate);
        var newActualDate = new Date(actualDate);
        var strTAT;
        var hour;
        var minute;

        var TAT = Math.abs(newActualDate - newTrxDate);

        TAT = TAT / (60 * 1000);

        TAT = Math.abs(TAT);

        //console.log(TAT);	  	

        arrTAT = TAT.toString();

        arrTAT = arrTAT.split(".");


        TAT = arrTAT[0];

        return TAT;

    }
    //hide stack value for 0 or 0.0 value
    function hideZeroStackValue() {

        $(".jqplot-point-label").each(
               function () {
                   var content = $(this).text();

                   //Get the length of the content;
                   //var contentLength = $.trim( content ).length;
                   if ((content == "0") || (content == "0.0")) {
                       $(this).hide();
                   }
               }
        );

    }
    function showFlagAndCaretTAT() {

        $("[id *= 'lblAVGTAT']").each(

            function () {

                var content = $(this).text();

                //Get the length of the content;
                //var contentLength = $.trim( content ).length;
                if (content == "") {
                    var contentBlank = $(this).attr('id');
                    var productContentBlank = contentBlank.substring(9, contentBlank.length);

                    $("#lblAVGTAT" + productContentBlank).css("display", "block");

                    $("#lblAVGTAT" + productContentBlank).text("-");

                    $("#flagSLA" + productContentBlank).css("display", "block");
                    $("#flagSLACaret" + productContentBlank).css("display", "block");

                    $("#flagSLA" + productContentBlank).css("margin-left", 105 + "px");
                    $("#flagSLACaret" + productContentBlank).css("margin-left", 105 - 2 + "px");

                }
            }
        );


    }
    //change class through add class
    function toggleClassPeriod() {

        $("#lblInfoPeriod").toggleClass("alert alert-info");

    }
    //insert html tag for period (date only)
    function injectDatePeriod(period) {

        $("#lblDashboardReport").html("<b>Dashboard Report</b><br><b>Period</b>" + " <b>" + period + "</b>");

    }

    //insert html tag for period (date time with amp/pm)
    function injectDateTimePeriod(period) {

        $("#lblLastUpdate").html("<b>Last Update :</b><br/>" + "<b>" + period + "</b>");
    }

    // restructur date time from JSON to yyyy/MM/dd (date) or yyyy/MM/dd HH:mm:ss (date time)
    function restructureDate(period, isDateTime) {
        var arrDateTimePeriod = [];
        var arrDatePeriod = []; var arrTimePeriod = []; var ampm = [];
        var arrBuffer = [];
        var year; var hour;
        var month; var minute;
        var day; var second; var ampm;

        var ret;
        if (!isDateTime) {
            arrDateTimePeriod = period.split(" ");
            arrDatePeriod = arrDateTimePeriod[0];
            arrBuffer = arrDatePeriod.split("/");
            year = arrBuffer[2]; month = arrBuffer[0]; day = arrBuffer[1];

            ret = year + "/" + month + "/" + day;
        } else {

            arrDateTimePeriod = period.split(" ");
            arrDatePeriod = arrDateTimePeriod[0];
            arrTimePeriod = arrDateTimePeriod[1];
            ampm = arrDateTimePeriod[2];
            arrBuffer = arrDatePeriod.split("/");
            year = arrBuffer[2]; month = arrBuffer[0]; day = arrBuffer[1];

            arrBuffer = [];
            arrBuffer = arrTimePeriod.split(":");
            hour = arrBuffer[0]; minute = arrBuffer[1]; second = arrBuffer[2];

            ampm = ampm.toLowerCase();
            if (ampm.indexOf("am") > -1 || ampm.indexOf("pm") > -1)
                ampm = ampm.toUpperCase();
            else
                ampm = "";
            ret = year + "/" + month + "/" + day + " " + hour + "." + minute + " " + ampm;
        }
        return ret;
    }

    // widget loader
    $box = $('#widget-box');
    var event;
    $box.trigger(event = $.Event('reload.ace.widget'))
    if (event.isDefaultPrevented()) return
    $box.blur();

    ko.applyBindings(viewModel);
    var mapping = {
        'ignore': ["LastModifiedDate", "LastModifiedBy"]
    };

    viewModel.ProductType(ko.mapping.toJS(ProductTypeModel, mapping));
    $.fn.getProductName = function (control) {
        var ctrlButton = $(this).attr('id');

        isOtherProduct = true;
        var iterator = ctrlButton.substring(10, ctrlButton.length);
        productName = $("#tableholder" + iterator + " .widget-box .widget-header h5").text().trim();

        $("#modal-form").modal('show');
        $("#spProductName").text('Product : ' + productName);

        //console.log(productName);
        viewModel.GetData(productName, dateNowFromServer, isOtherProduct);

    };

    $(".input-numericOnly").ForceNumericOnly();

    // On Error callback
    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    // On Always callback
    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }


});

// Get SPUser from cookies
function GetCurrentUser(vm) {
    if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
        spUser = $.cookie(api.cookie.spUser);

        vm.SPUser = spUser;
    }
}


// Token validation On Success function
function TokenOnSuccess(data, textStatus, jqXHR) {
    // store token on browser cookies
    $.cookie(api.cookie.name, data.AccessToken);
    accessToken = $.cookie(api.cookie.name);

    // call spuser function
    GetCurrentUser();

    // call get data inside view model
    viewModel.GetParameters();
    viewModel.GetData();

}

// Token validation On Error function
function TokenOnError(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}