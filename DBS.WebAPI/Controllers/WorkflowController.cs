﻿using DBS.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class WorkflowController : ApiController
    {
        private string message = string.Empty;

        private readonly ITransactionRepository repository = null;

        private readonly ITransactionDealRepository deal_repository = null;

        public WorkflowController()
        {

            this.repository = new TransactionRepository();
            this.deal_repository = new TransactionDealRepository();

        }

        public WorkflowController(ITransactionRepository repository, ITransactionDealRepository deal_repository)
        {
            this.repository = repository;
            this.deal_repository = deal_repository;
        }

        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            if (deal_repository != null)
                deal_repository.Dispose();
            base.Dispose(disposing);
        }

        /// <summary>
        /// Get transaction details by WF Instance ID
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionDetailModel))]
        [HttpGet]
        [Route("api/Workflow/{workflowInstanceID}/Transaction")]
        public HttpResponseMessage GetTransactionDetails(Guid workflowInstanceID)
        {
            TransactionDetailModel output = new TransactionDetailModel();

            if (!repository.GetTransactionDetails(workflowInstanceID, ref output, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Save Transaction Payment from Payment Checker
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approver ID from Nintex WF</param>
        /// <returns></returns>
        [ResponseType(typeof(object))]
        [HttpGet]
        [Route("api/Workflow/{workflowInstanceID}/Transaction/Deal/")]
        public HttpResponseMessage GetTransactionDealDetails(Guid workflowInstanceID)
        {
            TransactionDealDetailModel transaction = new TransactionDealDetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TransactionDealCheckerDetailModel output = new TransactionDealCheckerDetailModel();
            //get transaction history by fandi
            IList<TransactionDetailMaker> transactionmaker = new List<TransactionDetailMaker>();
            //end
            if (!deal_repository.GetTransactionDealDetails(workflowInstanceID, ref transaction, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
            }
            else
            {
                if (!repository.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    output.Transaction = transaction;
                    output.Timelines = timelines;
                    //get Transaction History by fandi
                    if (!repository.GetTransactionMaker(transaction.ID, ref transactionmaker, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "5" + message });
                    }
                    else
                    {
                        //getTransaction history by fandi
                        output.TransactionMaker = transactionmaker;
                        //end
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, output);
                }
            }
        }

        /// <summary>
        /// Get Complete Loan Transaction
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionCompleteLoanModel))]
        [HttpGet]
        [Route("api/WorkflowLoan/{workflowInstanceID}/CompleteTransaction/")]
        public HttpResponseMessage GetCompleteTransactionLoanDetails(Guid workflowInstanceID)
        {
            TransactionDisbursementModel Disbursement = new TransactionDisbursementModel();
            IList<TransactionRolloverModel> rollover = new List<TransactionRolloverModel>();
            IList<TransactionInterestMaintenanceModel> im = new List<TransactionInterestMaintenanceModel>();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TransactionLoanModel transaction = new TransactionLoanModel();
            TransactionCompleteLoanModel output = new TransactionCompleteLoanModel();
            IList<TransactionCallbackTimeLoanModel> callback = new List<TransactionCallbackTimeLoanModel>();
            output.Disbursement = Disbursement;
            output.LoanRollover = rollover;
            output.LoanIM = im;
            output.Callbacks = callback;

            if (!repository.GetLoanCompleteDetails(workflowInstanceID, ref transaction, ref Disbursement, ref rollover, ref im, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
            }
            else
            {
                if (!repository.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                if (!repository.GetTransactionCallbackCompleteDetails(workflowInstanceID, ref output, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    //if (!repository.GetTransactionCallbackCompleteDetails(workflowInstanceID, ref output, ref message))
                    //{
                    //    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    //}

                    //else
                    //{
                        output.Transaction = transaction;
                        output.Disbursement = Disbursement;
                        output.LoanRollover = rollover;
                        output.LoanIM = im;
                        output.Timelines = timelines;
                        output.Callbacks = output.Callbacks;

                        return Request.CreateResponse(HttpStatusCode.OK, output);
                    //}
                }
            }
        }

        /// <summary>
        /// Update TransactionDeal After Revise from Unit Checker
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approver ID from Nintex WF</param>
        /// <returns></returns>
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/Workflow/{workflowInstanceID}/Transaction/DealMakerAfterChecker/{approverID}")]
        public HttpResponseMessage TransactionDealMakerAfterRevise(Guid workflowInstanceID, long approverID, TransactionDealDetailModel transaction)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!deal_repository.UpdateTransactionDealAfterRevise(workflowInstanceID, approverID, transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction fx maker data revision has been saved." });
                }
            }
        }



        /// <summary>
        /// Get Transaction Deal Maker after Revise from Payment Checker
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approver ID from Nintex WF</param>
        /// <returns></returns>
        [ResponseType(typeof(object))]
        [HttpGet]
        [Route("api/Workflow/{workflowInstanceID}/TransactionRevise/Deal/")]
        public HttpResponseMessage GetTransactionReviseDealDetails(Guid workflowInstanceID)
        {
            TransactionDealDetailModel transaction = new TransactionDealDetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TransactionDealCheckerDetailModel output = new TransactionDealCheckerDetailModel();

            if (!deal_repository.GetTransactionReviseDealDetails(workflowInstanceID, ref transaction, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
            }
            else
            {
                if (!repository.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    output.Transaction = transaction;
                    output.Timelines = timelines;
                    return Request.CreateResponse(HttpStatusCode.OK, output);
                }
            }
        }

        /// <summary>
        /// Update Transaction Deal Maker after Revise from Payment Checker
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approver ID from Nintex WF</param>
        /// <returns></returns>
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/Workflow/{workflowInstanceID}/TransactionRevise/Deal/")]
        public HttpResponseMessage UpdateTransactionDealRevise(Guid workflowInstanceID, TransactionDealDetailModel transactionDeal)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!deal_repository.UpdateTransactionDealRevise(workflowInstanceID, transactionDeal, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "revise transaction deal complete." });
                }
            }
        }


        [ResponseType(typeof(TransactionMakerDetailModel))]
        [HttpGet]
        [Route("api/Workflow/{workflowInstanceID}/Transaction/Maker/{approverID}")]
        public HttpResponseMessage GetTransactionDetails(Guid workflowInstanceID, long approverID)
        {
            TransactionDetailModel transaction = new TransactionDetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TransactionMakerDetailModel output = new TransactionMakerDetailModel();
            //get transaction history by fandi
            IList<TransactionDetailMaker> transactionmaker = new List<TransactionDetailMaker>();
            //get doubled opened task
            ClientTaskData clienttaskdata = new ClientTaskData();
            //end
            try
            {
                // #1 Get Transaction Details
                if (!repository.GetTransactionDetails(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repository.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        output.Transaction = transaction;
                        output.Timelines = timelines;
                        //get Transaction History by fandi
                        if (!repository.GetTransactionMaker(transaction.ID, ref transactionmaker, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "5" + message });
                        }
                        else
                        {
                            //getTransaction history by fandi
                            output.TransactionMaker = transactionmaker;
                            //end
                        }
                        //add by fandi for check double task opened
                        //ClientTaskData clienttaskdata = null;
                        if (!repository.CheckTask(approverID, ref clienttaskdata, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = " Error Get Data Double Opened" + message });
                        }
                        else
                        {
                            output.ClienTaskData = clienttaskdata;
                        }
                        //end
                        return Request.CreateResponse(HttpStatusCode.OK, output);
                    }
                }
            }
            finally
            {
                transaction = null;
                timelines = null;
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/Workflow/{workflowInstanceID}/Transaction/Maker/{approverID}")]
        public HttpResponseMessage AddTransactionMakerDetails(Guid workflowInstanceID, long approverID, TransactionDetailModel transaction)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddTransactionMaker(workflowInstanceID, approverID, transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction maker data revision has been saved." });
                }
            }
        }

        #region IPE

        [ResponseType(typeof(TransactionCheckerDetailModel))]
        [HttpGet]
        [Route("api/Workflow/{workflowInstanceID}/Transaction/MonitoringIPE/{approverID}")]
        public HttpResponseMessage TransactionMonitoringIPE(Guid workflowInstanceID, long approverID)
        {
            TransactionDetailModel transaction = new TransactionDetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TransactionNormalAllModel output = new TransactionNormalAllModel();
            PaymentDetailModel payment = new PaymentDetailModel();
            TransactionCheckerAfterCallbackDetailModel callback = new TransactionCheckerAfterCallbackDetailModel();
            TransactionCallbackDetailModel callbackDetail = new TransactionCallbackDetailModel();
            TransactionCheckerDetailModel checker = new TransactionCheckerDetailModel();
            IList<TransactionDetailMaker> transactionmaker = new List<TransactionDetailMaker>();
            try
            {
                // #1 Get Transaction Details
                if (!repository.GetTransactionCompletedIPE(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "1" + message });
                }

                // #2 Get Transaction Timeline
                if (!repository.GetTransactionTimelineIPE(workflowInstanceID, ref timelines, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "2" + message });
                }

                if (!repository.GetTransactionCheckerDetailsIPE(workflowInstanceID, approverID, ref checker, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }

                if (!repository.GetNormalPaymentDetailIPE(workflowInstanceID, ref payment, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "4" + message });
                }

                if (!repository.GetTransactionAllNormalCallbackDetails(workflowInstanceID, approverID, ref callback, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "3" + message });
                }

                bool isGettingCallbacks = true;
                if (callback != null)
                {
                    // #3 Get Transaction Checker Details getutc
                    isGettingCallbacks = repository.GetTransactionCallbackDetails(workflowInstanceID, approverID, ref callbackDetail, ref message);
                }

                if (!isGettingCallbacks)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "3" });
                }

                //get Transaction History by fandi
                if (!repository.GetTransactionMaker(transaction.ID, ref transactionmaker, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "5" + message });
                }
                //end
                output.Transaction = transaction;
                output.Checker = checker;
                output.Payment = payment;
                output.Timelines = timelines;
                output.Callback = callback;
                output.callbackDetail = callbackDetail;
                output.TransactionMaker = transactionmaker;
                return Request.CreateResponse(HttpStatusCode.OK, output);
            }
            finally
            {
                transaction = null;
                timelines = null;
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/Workflow/{workflowInstanceID}/Transaction/MakerIPE/{approverID}")]
        public HttpResponseMessage AddTransactionMakerDetailsIPE(Guid workflowInstanceID, long approverID, TransactionDetailModel transaction)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddTransactionMakerIPE(workflowInstanceID, approverID, transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction maker data revision has been saved." });
                }
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/Workflow/{workflowInstanceID}/Transaction/CheckerIPE/{approverID}")]
        public HttpResponseMessage AddTransactionCheckerDetailsIPE(Guid workflowInstanceID, long approverID, TransactionCheckerDetailModel transactionChecker)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddTransactionCheckerDetailsIPE(workflowInstanceID, approverID, transactionChecker, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been saved." });
                }
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/Workflow/{workflowInstanceID}/Transaction/CallbackIPE/{approverID}")]
        public HttpResponseMessage AddTransactionCallbackDetailsIPE(Guid workflowInstanceID, long approverID, TransactionCallbackDetailModel data)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddTransactionCallbackIPE(workflowInstanceID, approverID, data, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "transaction callback data has been saved." });
                }
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/Workflow/{workflowInstanceID}/Transaction/ReactivationDormantIPE/{approverID}")]
        public HttpResponseMessage ReactivationBranchDormantAccountIPE(Guid workflowInstanceID, long approverID, TransactionCheckerDetailModel transactionChecker)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateTransactionDormantIPE(workflowInstanceID, approverID, transactionChecker, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction has been saved." });
                }
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/Workflow/{workflowInstanceID}/Payment/CheckerIPE/Update/{approverID}/{updateType}")]
        public HttpResponseMessage AddPaymentCheckerReviseIPE(Guid workflowInstanceID, long approverID, int updateType, PaymentCheckerDetailModel payment)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdatePaymentCheckerReviseIPE(workflowInstanceID, approverID, updateType, payment, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Payment Checker data has been saved." });
                }
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/Workflow/{workflowInstanceID}/Transaction/DealMakerAfterCheckerIPE/{approverID}")]
        public HttpResponseMessage TransactionDealMakerAfterReviseIPE(Guid workflowInstanceID, long approverID, TransactionDealDetailModel transaction)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!deal_repository.UpdateTransactionDealAfterReviseIPE(workflowInstanceID, approverID, transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction fx maker data revision has been saved." });
                }
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/Workflow/{workflowInstanceID}/Transaction/CheckerIPE/Update/{approverID}")]
        public HttpResponseMessage UpdateTransactionCheckerDetailsIPE(Guid workflowInstanceID, long approverID, TransactionCheckerDetailModel transactionChecker)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateTransactionCheckerDetailsIPE(workflowInstanceID, approverID, transactionChecker, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been updated." });
                }
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/Workflow/{workflowInstanceID}/Transaction/CheckerAfterCallbackIPE/Update/{approverID}")]
        public HttpResponseMessage UpdateTransactionCheckerAfterCallbackDetailsIPE(Guid workflowInstanceID, long approverID, TransactionCheckerAfterCallbackDetailModel transactionChecker)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateTransactionCheckerCallbackDetailsIPE(workflowInstanceID, approverID, transactionChecker, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been updated." });
                }
            }
        }

        [ResponseType(typeof(TransactionCheckerAfterCallbackDetailModel))]
        [HttpGet]
        [Route("api/Workflow/{workflowInstanceID}/Transaction/CheckerIPE/Callback/{approverID}")]
        public HttpResponseMessage GetTransactionCheckCallbackDetailsIPE(Guid workflowInstanceID, long approverID)
        {
            TransactionDetailModel transaction = new TransactionDetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TransactionCheckerAfterCallbackDetailModel output = new TransactionCheckerAfterCallbackDetailModel();
            //get transaction history by fandi
            IList<TransactionDetailMaker> transactionmaker = new List<TransactionDetailMaker>();
            //get doubled opened task
            ClientTaskData clienttaskdata = new ClientTaskData();
            //end
            try
            {
                // #1 Get Transaction Details
                if (!repository.GetTransactionDetailsIPE(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repository.GetTransactionTimelineIPE(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        //output.Timelines = timelines;

                        //return Request.CreateResponse(HttpStatusCode.OK, output);
                        // #3 Get Transaction Checker Details
                        if (!repository.GetTransactionCheckerAfterCallbackDetailsIPE(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Transaction = transaction;
                            output.Timelines = timelines;
                            //get Transaction History by fandi
                            if (!repository.GetTransactionMakerIPE(transaction.ID, ref transactionmaker, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "5" + message });
                            }
                            else
                            {
                                //getTransaction history by fandi
                                output.TransactionMaker = transactionmaker;
                                //end
                            }
                            //add by fandi for check double task opened
                            //ClientTaskData clienttaskdata = null;
                            if (!repository.CheckTaskIPE(approverID,workflowInstanceID, ref clienttaskdata, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = " Error Get Data Double Opened" + message });
                            }
                            else
                            {
                                output.ClienTaskData = clienttaskdata;
                            }
                            //end
                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {
                timelines = null;
            }
        }

        [ResponseType(typeof(TransactionCheckerDetailModel))]
        [HttpGet]
        [Route("api/Workflow/{workflowInstanceID}/Transaction/CheckerIPE/{approverID}")]
        public HttpResponseMessage GetTransactionCheckerDetailsIPE(Guid workflowInstanceID, long approverID)
        {
            TransactionDetailModel transaction = new TransactionDetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TransactionCheckerDetailModel output = new TransactionCheckerDetailModel();
            IList<TransactionDetailMaker> transactionmaker = new List<TransactionDetailMaker>();
            ClientTaskData clienttaskdata = new ClientTaskData();
            try
            {
                // #1 Get Transaction Details
                if (!repository.GetTransactionDetailsIPE(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repository.GetTransactionTimelineIPE(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        // #3 Get Transaction Checker Details
                        if (!repository.GetTransactionCheckerDetailsIPE(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Transaction = transaction;
                            output.Timelines = timelines;
                            if (!repository.GetTransactionMakerIPE(transaction.ID, ref transactionmaker, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "5" + message });
                            }
                            else
                            {
                                output.TransactionMaker = transactionmaker;
                            }
                            if (!repository.CheckTaskIPE(approverID,workflowInstanceID, ref clienttaskdata, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = " Error Get Data Double Opened" + message });
                            }
                            else
                            {
                                output.ClienTaskData = clienttaskdata;
                            }

                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {

                transaction = null;
                timelines = null;
            }
        }

        [ResponseType(typeof(PaymentCheckerDetailModel))]
        [HttpGet]
        [Route("api/Workflow/{workflowInstanceID}/Payment/CheckerIPE/{approverID}")]
        public HttpResponseMessage GetPaymentCheckerDetailsIPE(Guid workflowInstanceID, long approverID)
        {
            PaymentDetailModel payment = new PaymentDetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            PaymentCheckerDetailModel output = new PaymentCheckerDetailModel();
            PaymentMakerDetailModel outputChecker = new PaymentMakerDetailModel();
            IList<TransactionDetailMaker> transactionmaker = new List<TransactionDetailMaker>();
            ClientTaskData clienttaskdata = new ClientTaskData();
            try
            {
                if (!repository.GetPaymentDetailsIPE(workflowInstanceID, ref payment, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    if (!repository.GetTransactionTimelineIPE(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        if (!repository.GetPaymentCheckerDetailsIPE(workflowInstanceID, approverID, ref outputChecker, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Payment = payment;
                            output.Timelines = timelines;
                            output.Verify = outputChecker.Verify;
                            if (!repository.GetTransactionMakerIPE(payment.ID, ref transactionmaker, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "5" + message });
                            }
                            else
                            {
                                output.TransactionMaker = transactionmaker;
                            }
                            if (!repository.CheckTaskIPE(approverID,workflowInstanceID, ref clienttaskdata, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = " Error Get Data Double Opened" + message });
                            }
                            else
                            {
                                output.ClienTaskData = clienttaskdata;
                            }
                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {
                payment = null;
                timelines = null;
            }
        }

        [ResponseType(typeof(TransactionCheckerDetailModel))]
        [HttpGet]
        [Route("api/Workflow/{workflowInstanceID}/Transaction/MakerReviseAfterPaymentIPE/{approverID}")]
        public HttpResponseMessage GetTransactionMakerAfterPaymentReviseDetailsIPE(Guid workflowInstanceID, long approverID)
        {
            TransactionDetailModel transaction = new TransactionDetailModel();
            PaymentDetailModel payment = new PaymentDetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            IList<VerifyModel> verify = new List<VerifyModel>();
            TransactionMakerReviseDetailModel output = new TransactionMakerReviseDetailModel();
            IList<TransactionDetailMaker> transactionmaker = new List<TransactionDetailMaker>();
            ClientTaskData clienttaskdata = new ClientTaskData();
            try
            {
                // #1 Get Transaction Details
                if (!repository.GetTransactionPPUMakerAfterPaymentIPE(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Payment
                    if (!repository.GetPaymentIPE(workflowInstanceID, ref payment, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        // #3 Get Transaction Timeline
                        if (!repository.GetTransactionTimelineIPE(workflowInstanceID, ref timelines, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            // #4 Get Transaction Payment Checker (verify)
                            if (!repository.GetTransactionPaymentCheckerIPE(transaction.ID, ref verify, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                            }
                            else
                            {
                                output.Transaction = transaction;
                                output.Payment = payment;
                                output.Timelines = timelines;
                                output.Verify = verify;

                                if (!repository.GetTransactionMakerIPE(transaction.ID, ref transactionmaker, ref message))
                                {
                                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "5" + message });
                                }
                                else
                                {
                                    output.TransactionMaker = transactionmaker;
                                }
                                if (!repository.CheckTaskIPE(approverID,workflowInstanceID, ref clienttaskdata, ref message))
                                {
                                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = " Error Get Data Double Opened" + message });
                                }
                                else
                                {
                                    output.ClienTaskData = clienttaskdata;
                                }
                                return Request.CreateResponse(HttpStatusCode.OK, output);
                            }
                        }
                    }
                }
            }
            finally
            {
                transaction = null;
                timelines = null;
            }
        }

        [ResponseType(typeof(TransactionCallbackDetailModel))]
        [HttpGet]
        [Route("api/Workflow/{workflowInstanceID}/Transaction/CallbackIPE/{approverID}")]
        public HttpResponseMessage GetTransactionCallbackDetailsIPE(Guid workflowInstanceID, long approverID)
        {
            TransactionDetailModel transaction = new TransactionDetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TransactionCallbackDetailModel output = new TransactionCallbackDetailModel();
            IList<TransactionDetailMaker> transactionmaker = new List<TransactionDetailMaker>();
            ClientTaskData clienttaskdata = new ClientTaskData();
            try
            {
                // #1 Get Transaction Details
                if (!repository.GetTransactionDetailsIPE(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "1" });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repository.GetTransactionTimelineIPE(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "2" });
                    }
                    else
                    {
                        // #3 Get Transaction Checker Details getutc
                        if (!repository.GetTransactionCallbackDetailsIPE(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "3" });
                        }
                        else
                        {
                            if (!repository.GetTransactionMakerIPE(transaction.ID, ref transactionmaker, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "5" + message });
                            }
                            else
                            {
                                output.Transaction = transaction;
                                output.Timelines = timelines;
                                output.TransactionMaker = transactionmaker;
                            }
                            if (!repository.CheckTaskIPE(approverID,workflowInstanceID, ref clienttaskdata, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = " Error Get Data Double Opened" + message });
                            }
                            else
                            {
                                output.ClienTaskData = clienttaskdata;
                            }
                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {

                transaction = null;
                timelines = null;
            }
        }

        [ResponseType(typeof(TransactionCheckerAfterCallbackDetailModel))]
        [HttpGet]
        [Route("api/Workflow/{workflowInstanceID}/Transaction/MakerReviseAfterCallerIPE/{approverID}")]
        public HttpResponseMessage GetTransactionMakerAfterReviseCallerDetailsIPE(Guid workflowInstanceID, long approverID)
        {
            TransactionDetailModel transaction = new TransactionDetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TransactionCheckerAfterCallbackDetailModel output = new TransactionCheckerAfterCallbackDetailModel();
            IList<TransactionDetailMaker> transactionmaker = new List<TransactionDetailMaker>();
            ClientTaskData clienttaskdata = new ClientTaskData();
            try
            {
                // #1 Get Transaction Details
                if (!repository.GetTransactionDetailsIPE(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repository.GetTransactionTimelineIPE(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        // #3 Get Transaction Checker Details
                        if (!repository.GetTransactionCheckerAfterCallbackDetailsIPE(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Transaction = transaction;
                            output.Timelines = timelines;
                            if (!repository.GetTransactionMakerIPE(transaction.ID, ref transactionmaker, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "5" + message });
                            }
                            else
                            {
                                output.TransactionMaker = transactionmaker;
                            }
                            if (!repository.CheckTaskIPE(approverID,workflowInstanceID, ref clienttaskdata, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = " Error Get Data Double Opened" + message });
                            }
                            else
                            {
                                output.ClienTaskData = clienttaskdata;
                            }
                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {

                transaction = null;
                timelines = null;
            }
        }

        [ResponseType(typeof(TransactionContactDetailModel))]
        [HttpGet]
        [Route("api/Workflow/{workflowInstanceID}/Transaction/ContactIPE/{approverID}")]
        public HttpResponseMessage GetTransactionContactDetailsIPE(Guid workflowInstanceID, long approverID)
        {
            TransactionDetailModel transaction = new TransactionDetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            IList<CustomerContactModel> contact = new List<CustomerContactModel>();
            TransactionContactDetailModel output = new TransactionContactDetailModel();
            IList<TransactionDetailMaker> transactionmaker = new List<TransactionDetailMaker>();
            ClientTaskData clienttaskdata = new ClientTaskData();
            try
            {
                // #1 Get Transaction Details
                if (!repository.GetTransactionDetailsIPE(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repository.GetTransactionTimelineIPE(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        // #3 Get Transaction Contact Details
                        if (!repository.GetTransactionContactDetailsIPE(workflowInstanceID, approverID, ref contact, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Contacts = contact;
                            output.Transaction = transaction;
                            output.Timelines = timelines;
                            if (!repository.GetTransactionMakerIPE(transaction.ID, ref transactionmaker, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "5" + message });
                            }
                            else
                            {
                                output.TransactionMaker = transactionmaker;
                            }
                            if (!repository.CheckTaskIPE(approverID,workflowInstanceID, ref clienttaskdata, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = " Error Get Data Double Opened" + message });
                            }
                            else
                            {
                                output.ClienTaskData = clienttaskdata;
                            }
                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {

                transaction = null;
                timelines = null;
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/Workflow/{workflowInstanceID}/Transaction/ContactIPE/{approverID}")]
        public HttpResponseMessage AddTransactionContactDetailsIPE(Guid workflowInstanceID, long approverID, IList<CustomerContactModel> data)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddTransactionContactIPE(workflowInstanceID, approverID, data, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "transaction contact data has been saved." });
                }
            }
        }

        /// <summary>
        /// Get transaction details for Maker After Revise by WF Instance ID
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approve ID from Nintex WF</param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionCheckerDetailModel))]
        [HttpGet]
        [Route("api/Workflow/{workflowInstanceID}/Transaction/MakerReviseAfterCheckerIPE/{approverID}")]
        public HttpResponseMessage GetTransactionMakerAfterReviseDetailsIPE(Guid workflowInstanceID, long approverID)
        {
            TransactionDetailModel transaction = new TransactionDetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TransactionCheckerDetailModel output = new TransactionCheckerDetailModel();
            IList<TransactionDetailMaker> transactionmaker = new List<TransactionDetailMaker>();
            ClientTaskData clienttaskdata = new ClientTaskData();

            try
            {
                // #1 Get Transaction Details
                if (!repository.GetTransactionDetailsIPE(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repository.GetTransactionTimelineIPE(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        // #3 Get Transaction Checker Details
                        if (!repository.GetTransactionCheckerDetailsIPE(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Transaction = transaction;
                            output.Timelines = timelines;
                            if (!repository.GetTransactionMakerIPE(transaction.ID, ref transactionmaker, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "5" + message });
                            }
                            else
                            {
                                output.TransactionMaker = transactionmaker;
                            }
                            if (!repository.CheckTaskIPE(approverID,workflowInstanceID, ref clienttaskdata, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = " Error Get Data Double Opened" + message });
                            }
                            else
                            {
                                output.ClienTaskData = clienttaskdata;
                            }
                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {

                transaction = null;
                timelines = null;
            }
        }

        [ResponseType(typeof(LLDDocumentModel))]
        [HttpGet]
        [Route("api/LLDDocumentAll")]
        public HttpResponseMessage GetAllLldDocument()
        {
            IList<LLDDocumentModel> result = new List<LLDDocumentModel>();
            
                if (!repository.GetAllLLDDocument(ref result,ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    if (result == null)
                        return Request.CreateResponse(HttpStatusCode.NoContent);
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                }
            
        }
        #endregion


        /// <summary>
        /// Get transaction details for checker by WF Instance ID
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approve ID from Nintex WF</param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionCheckerDetailModel))]
        [HttpGet]
        [Route("api/Workflow/{workflowInstanceID}/Transaction/Checker/{approverID}")]
        public HttpResponseMessage GetTransactionCheckerDetails(Guid workflowInstanceID, long approverID)
        {
            TransactionDetailModel transaction = new TransactionDetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TransactionCheckerDetailModel output = new TransactionCheckerDetailModel();
            //get transaction history by fandi
            IList<TransactionDetailMaker> transactionmaker = new List<TransactionDetailMaker>();
            //get doubled opened task
            ClientTaskData clienttaskdata = new ClientTaskData();
            //end

            try
            {
                // #1 Get Transaction Details
                if (!repository.GetTransactionDetails(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repository.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        // #3 Get Transaction Checker Details
                        if (!repository.GetTransactionCheckerDetails(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Transaction = transaction;
                            output.Timelines = timelines;

                            //get Transaction History by fandi
                            if (!repository.GetTransactionMaker(transaction.ID, ref transactionmaker, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "5" + message });
                            }
                            else
                            {
                                //getTransaction history by fandi
                                output.TransactionMaker = transactionmaker;
                                //end
                            }
                            //add by fandi for check double task opened
                            //ClientTaskData clienttaskdata = null;
                            if (!repository.CheckTask(approverID, ref clienttaskdata, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = " Error Get Data Double Opened" + message });
                            }
                            else
                            {
                                output.ClienTaskData = clienttaskdata;
                            }
                            //end
                            //end

                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {

                transaction = null;
                timelines = null;
            }
        }

        /// <summary>
        /// Get transaction details for Maker After Revise by WF Instance ID
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approve ID from Nintex WF</param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionCheckerDetailModel))]
        [HttpGet]
        [Route("api/Workflow/{workflowInstanceID}/Transaction/MakerReviseAfterChecker/{approverID}")]
        public HttpResponseMessage GetTransactionMakerAfterReviseDetails(Guid workflowInstanceID, long approverID)
        {
            TransactionDetailModel transaction = new TransactionDetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TransactionCheckerDetailModel output = new TransactionCheckerDetailModel();
            //get transaction history by fandi
            IList<TransactionDetailMaker> transactionmaker = new List<TransactionDetailMaker>();
            //get doubled opened task
            ClientTaskData clienttaskdata = new ClientTaskData();
            //end
            try
            {
                // #1 Get Transaction Details
                if (!repository.GetTransactionDetailsMaker(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repository.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        // #3 Get Transaction Checker Details
                        if (!repository.GetTransactionCheckerDetails(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Transaction = transaction;
                            output.Timelines = timelines;
                            //get Transaction History by fandi
                            if (!repository.GetTransactionMaker(transaction.ID, ref transactionmaker, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "5" + message });
                            }
                            else
                            {
                                //getTransaction history by fandi
                                output.TransactionMaker = transactionmaker;
                                //end    
                            }
                            //add by fandi for check double task opened
                            //ClientTaskData clienttaskdata = null;
                            if (!repository.CheckTask(approverID, ref clienttaskdata, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = " Error Get Data Double Opened" + message });
                            }
                            else
                            {
                                output.ClienTaskData = clienttaskdata;
                            }
                            //end
                            //end
                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {

                transaction = null;
                timelines = null;
            }
        }

        /// <summary>
        /// Get transaction details for Maker After Payment Maker Revise by WF Instance ID
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approve ID from Nintex WF</param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionCheckerDetailModel))]
        [HttpGet]
        [Route("api/Workflow/{workflowInstanceID}/Transaction/MakerReviseAfterPayment/{approverID}")]
        public HttpResponseMessage GetTransactionMakerAfterPaymentReviseDetails(Guid workflowInstanceID, long approverID)
        {
            TransactionDetailModel transaction = new TransactionDetailModel();
            PaymentDetailModel payment = new PaymentDetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            IList<VerifyModel> verify = new List<VerifyModel>();
            TransactionMakerReviseDetailModel output = new TransactionMakerReviseDetailModel();
            //get transaction history by fandi
            IList<TransactionDetailMaker> transactionmaker = new List<TransactionDetailMaker>();
            //get doubled opened task
            ClientTaskData clienttaskdata = new ClientTaskData();
            //end


            try
            {
                // #1 Get Transaction Details
                if (!repository.GetTransactionPPUMakerAfterPayment(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Payment
                    if (!repository.GetPayment(workflowInstanceID, ref payment, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        // #3 Get Transaction Timeline
                        if (!repository.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            // #4 Get Transaction Payment Checker (verify)
                            if (!repository.GetTransactionPaymentChecker(transaction.ID, ref verify, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                            }
                            else
                            {
                                output.Transaction = transaction;
                                output.Payment = payment;
                                output.Timelines = timelines;
                                output.Verify = verify;
                                //get Transaction History by fandi
                                if (!repository.GetTransactionMaker(transaction.ID, ref transactionmaker, ref message))
                                {
                                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "5" + message });
                                }
                                else
                                {
                                    //getTransaction history by fandi
                                    output.TransactionMaker = transactionmaker;
                                    //end    
                                }
                                //add by fandi for check double task opened
                                //ClientTaskData clienttaskdata = null;
                                if (!repository.CheckTask(approverID, ref clienttaskdata, ref message))
                                {
                                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = " Error Get Data Double Opened" + message });
                                }
                                else
                                {
                                    output.ClienTaskData = clienttaskdata;
                                }
                                //end
                                return Request.CreateResponse(HttpStatusCode.OK, output);
                            }
                        }
                    }
                }
            }
            finally
            {
                transaction = null;
                timelines = null;
            }
        }

        /// <summary>
        /// Get transaction details for checker afte caller by WF Instance ID
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approve ID from Nintex WF</param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionCheckerAfterCallbackDetailModel))]
        [HttpGet]
        [Route("api/Workflow/{workflowInstanceID}/Transaction/MakerReviseAfterCaller/{approverID}")]
        public HttpResponseMessage GetTransactionMakerAfterReviseCallerDetails(Guid workflowInstanceID, long approverID)
        {
            TransactionDetailModel transaction = new TransactionDetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TransactionCheckerAfterCallbackDetailModel output = new TransactionCheckerAfterCallbackDetailModel();
            //get transaction history by fandi
            IList<TransactionDetailMaker> transactionmaker = new List<TransactionDetailMaker>();
            //get doubled opened task
            ClientTaskData clienttaskdata = new ClientTaskData();
            //end
            try
            {
                // #1 Get Transaction Details
                if (!repository.GetTransactionDetailsMaker(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repository.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        // #3 Get Transaction Checker Details
                        if (!repository.GetTransactionCheckerAfterCallbackDetails(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Transaction = transaction;
                            output.Timelines = timelines;
                            //get Transaction History by fandi
                            if (!repository.GetTransactionMaker(transaction.ID, ref transactionmaker, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "5" + message });
                            }
                            else
                            {
                                //getTransaction history by fandi
                                output.TransactionMaker = transactionmaker;
                                //end
                            }
                            //add by fandi for check double task opened
                            //ClientTaskData clienttaskdata = null;
                            if (!repository.CheckTask(approverID, ref clienttaskdata, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = " Error Get Data Double Opened" + message });
                            }
                            else
                            {
                                output.ClienTaskData = clienttaskdata;
                            }
                            //end


                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {

                transaction = null;
                timelines = null;
            }
        }

        /// <summary>
        /// Get transaction details for checker by WF Instance ID
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approve ID from Nintex WF</param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionCheckerDetailModel))]
        [HttpGet]
        [Route("api/Workflow/{workflowInstanceID}/Transaction/Pending/{approverID}")]
        public HttpResponseMessage GetPendingDocumentsDetails(Guid workflowInstanceID, long approverID)
        {
            TransactionDetailModel transaction = new TransactionDetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TransactionCheckerDetailModel output = new TransactionCheckerDetailModel();
            //get transaction history by fandi
            IList<TransactionDetailMaker> transactionmaker = new List<TransactionDetailMaker>();
            //get doubled opened task
            ClientTaskData clienttaskdata = new ClientTaskData();
            //end
            try
            {
                // #1 Get Transaction Details
                if (!repository.GetPendingDocumentsDetails(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    output.Transaction = transaction;
                    //output.Timelines = timelines;
                    //get Transaction History by fandi
                    if (!repository.GetTransactionMaker(transaction.ID, ref transactionmaker, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "5" + message });
                    }
                    else
                    {
                        //getTransaction history by fandi
                        output.TransactionMaker = transactionmaker;
                        //end
                    }
                    //add by fandi for check double task opened
                    //ClientTaskData clienttaskdata = null;
                    if (!repository.CheckTask(approverID, ref clienttaskdata, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = " Error Get Data Double Opened" + message });
                    }
                    else
                    {
                        output.ClienTaskData = clienttaskdata;
                    }
                    //end
                    return Request.CreateResponse(HttpStatusCode.OK, output);
                }
            }
            finally
            {

                transaction = null;
                timelines = null;
            }
        }

        [ResponseType(typeof(TransactionCheckerDetailModel))]
        [HttpGet]
        [Route("api/Workflow/{workflowInstanceID}/Transaction/PendingIPE/{approverID}")]
        public HttpResponseMessage GetPendingDocumentsDetailsIPE(Guid workflowInstanceID, long approverID)
        {
            TransactionDetailModel transaction = new TransactionDetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TransactionCheckerDetailModel output = new TransactionCheckerDetailModel();
            //get transaction history by fandi
            IList<TransactionDetailMaker> transactionmaker = new List<TransactionDetailMaker>();
            //get doubled opened task
            ClientTaskData clienttaskdata = new ClientTaskData();
            //end
            try
            {
                // #1 Get Transaction Details
                if (!repository.GetPendingDocumentsDetailsIPE(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    output.Transaction = transaction;
                    //output.Timelines = timelines;
                    //get Transaction History by fandi
                    if (!repository.GetTransactionMaker(transaction.ID, ref transactionmaker, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "5" + message });
                    }
                    else
                    {
                        //getTransaction history by fandi
                        output.TransactionMaker = transactionmaker;
                        //end
                    }
                    //add by fandi for check double task opened
                    //ClientTaskData clienttaskdata = null;
                    if (!repository.CheckTask(approverID, ref clienttaskdata, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = " Error Get Data Double Opened" + message });
                    }
                    else
                    {
                        output.ClienTaskData = clienttaskdata;
                    }
                    //end
                    return Request.CreateResponse(HttpStatusCode.OK, output);
                }
            }
            finally
            {

                transaction = null;
                timelines = null;
            }
        }

        /// <summary>
        /// Save approval data from checker
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approver ID from Nintex WF</param>
        /// <param name="transactionChecker">Transaction checker data</param>
        /// <returns></returns>
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/Workflow/{workflowInstanceID}/Transaction/Checker/{approverID}")]
        public HttpResponseMessage AddTransactionCheckerDetails(Guid workflowInstanceID, long approverID, TransactionCheckerDetailModel transactionChecker)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddTransactionCheckerDetails(workflowInstanceID, approverID, transactionChecker, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been saved." });
                }
            }
        }

        /// <summary>
        /// Update approval data from checker
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approver ID from Nintex WF</param>
        /// <param name="transactionChecker">Transaction checker data</param>
        /// <returns></returns>
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/Workflow/{workflowInstanceID}/Transaction/Checker/Update/{approverID}")]
        public HttpResponseMessage UpdateTransactionCheckerDetails(Guid workflowInstanceID, long approverID, TransactionCheckerDetailModel transactionChecker)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateTransactionCheckerDetails(workflowInstanceID, approverID, transactionChecker, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been updated." });
                }
            }
        }

        /// <summary>
        /// Get transaction details for checker afte caller by WF Instance ID
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approve ID from Nintex WF</param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionCheckerAfterCallbackDetailModel))]
        [HttpGet]
        [Route("api/Workflow/{workflowInstanceID}/Transaction/Checker/Callback/{approverID}")]
        public HttpResponseMessage GetTransactionCheckCallbackDetails(Guid workflowInstanceID, long approverID)
        {
            TransactionDetailModel transaction = new TransactionDetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TransactionCheckerAfterCallbackDetailModel output = new TransactionCheckerAfterCallbackDetailModel();
            //get transaction history by fandi
            IList<TransactionDetailMaker> transactionmaker = new List<TransactionDetailMaker>();
            //get doubled opened task
            ClientTaskData clienttaskdata = new ClientTaskData();
            //end
            try
            {
                // #1 Get Transaction Details
                if (!repository.GetTransactionCheckerAfterCaller(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repository.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        //output.Timelines = timelines;

                        //return Request.CreateResponse(HttpStatusCode.OK, output);
                        // #3 Get Transaction Checker Details
                        if (!repository.GetTransactionCheckerAfterCallbackDetails(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Transaction = transaction;
                            output.Timelines = timelines;
                            //get Transaction History by fandi
                            if (!repository.GetTransactionMaker(transaction.ID, ref transactionmaker, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "5" + message });
                            }
                            else
                            {
                                //getTransaction history by fandi
                                output.TransactionMaker = transactionmaker;
                                //end
                            }
                            //add by fandi for check double task opened
                            //ClientTaskData clienttaskdata = null;
                            if (!repository.CheckTask(approverID, ref clienttaskdata, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = " Error Get Data Double Opened" + message });
                            }
                            else
                            {
                                output.ClienTaskData = clienttaskdata;
                            }
                            //end
                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }

                #region old code - 2015-12-29
                //// #1 Get Transaction Details
                //if (!repository.GetTransactionDetails(workflowInstanceID, ref transaction, ref message))
                //{
                //    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                //}
                //else
                //{
                //    // #2 Get Transaction Timeline
                //    if (!repository.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                //    {
                //        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                //    }
                //    else
                //    {
                //        // #3 Get Transaction Checker Details
                //        if (!repository.GetTransactionCheckerAfterCallbackDetails(workflowInstanceID, approverID, ref output, ref message))
                //        {
                //            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                //        }
                //        else
                //        {
                //            output.Transaction = transaction;
                //            output.Timelines = timelines;

                //            return Request.CreateResponse(HttpStatusCode.OK, output);
                //        }
                //    }
                //}
                #endregion
            }
            finally
            {
                timelines = null;
            }
        }

        /// <summary>
        /// Save approval data from checker after callback
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approver ID from Nintex WF</param>
        /// <param name="transactionChecker">Transaction checker data</param>
        /// <returns></returns>
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/Workflow/{workflowInstanceID}/Transaction/Checker/Callback/{approverID}")]
        public HttpResponseMessage AddTransactionCheckerCallbackDetails(Guid workflowInstanceID, long approverID, TransactionCheckerDetailModel data)
        {
            // Forward to existing method: same as url "api/Workflow/{workflowInstanceID}/Transaction/Checker/{approverID}"
            return AddTransactionCheckerDetails(workflowInstanceID, approverID, data);
        }


        /// <summary>
        /// Get payment details of transaction by WF Instance ID
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <returns></returns>
        [ResponseType(typeof(PaymentMakerDetailModel))]
        [HttpGet]
        [Route("api/Workflow/{workflowInstanceID}/Payment/Maker/{approverID}")]
        public HttpResponseMessage GetPaymentMakerDetails(Guid workflowInstanceID, long approverID)
        {
            PaymentDetailModel payment = new PaymentDetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            PaymentMakerDetailModel output = new PaymentMakerDetailModel();
            //get transaction history by fandi
            IList<TransactionDetailMaker> transactionmaker = new List<TransactionDetailMaker>();
            //get doubled opened task
            ClientTaskData clienttaskdata = new ClientTaskData();
            //end
            try
            {
                // #1 Get Payment Details
                if (!repository.GetPaymentDetails(workflowInstanceID, ref payment, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repository.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        // #3 Get Transaction Checker Details (Payment Maker Revise Task)
                        if (!repository.GetPaymentCheckerDetails(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Payment = payment;
                            output.Timelines = timelines;
                            //get Transaction History by fandi
                            if (!repository.GetTransactionMaker(payment.ID, ref transactionmaker, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "5" + message });
                            }
                            else
                            {
                                //getTransaction history by fandi
                                output.TransactionMaker = transactionmaker;
                                //end
                            }
                            //add by fandi for check double task opened
                            //ClientTaskData clienttaskdata = null;
                            if (!repository.CheckTask(approverID, ref clienttaskdata, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = " Error Get Data Double Opened" + message });
                            }
                            else
                            {
                                output.ClienTaskData = clienttaskdata;
                            }
                            //end
                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {
                payment = null;
                timelines = null;
            }
        }

        //add udin 20161013 skn bulk ~OFFLINE~
        /// <summary>
        /// Get payment details of transaction by WF Instance ID
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <returns></returns>
        [ResponseType(typeof(PaymentMakerDetailModel))]
        [HttpGet]
        [Route("api/Workflow/{workflowInstanceID}/Payment/MakerIPE/{approverID}")]
        public HttpResponseMessage GetPaymentMakerIPEDetails(Guid workflowInstanceID, long approverID)
        {
            PaymentDetailModel payment = new PaymentDetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            PaymentMakerDetailModel output = new PaymentMakerDetailModel();
            IList<TransactionDetailMaker> transactionmaker = new List<TransactionDetailMaker>();
            ClientTaskData clienttaskdata = new ClientTaskData();
            try
            {
                // #1 Get Payment Details
                if (!repository.GetPaymentMakerDetailsIPE(workflowInstanceID, ref payment, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repository.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        // #3 Get Transaction Checker Details (Payment Maker Revise Task)
                        if (!repository.GetPaymentCheckerDetails(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Payment = payment;
                            output.Timelines = timelines;
                            if (!repository.GetTransactionMaker(payment.ID, ref transactionmaker, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "5" + message });
                            }
                            else
                            {
                                output.TransactionMaker = transactionmaker;
                            }
                            if (!repository.CheckTask(approverID, ref clienttaskdata, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = " Error Get Data Double Opened" + message });
                            }
                            else
                            {
                                output.ClienTaskData = clienttaskdata;
                            }
                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {
                payment = null;
                timelines = null;
            }
        }

        /// <summary>
        /// Save Transaction Payment from Payment Maker
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approver ID from Nintex WF</param>
        /// <param name="payment">Payment Maker data</param>
        /// <returns></returns>
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/Workflow/{workflowInstanceID}/Payment/Maker/{approverID}")]
        public HttpResponseMessage AddPaymentMakerDetails(Guid workflowInstanceID, long approverID, PaymentDetailModel payment)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddTransactionPaymentMaker(workflowInstanceID, approverID, payment, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Payment maker data has been saved." });
                }
            }
        }

        /// <summary>
        /// Revise Transaction Payment from Payment Maker to PPU Maker
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approver ID from Nintex WF</param>
        /// <param name="payment">Payment Maker data</param>
        /// <returns></returns>
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/Workflow/{workflowInstanceID}/Payment/Maker/Revise/{approverID}")]
        public HttpResponseMessage AddPaymentMakerReviseDetails(Guid workflowInstanceID, long approverID, PaymentDetailModel payment)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddTransactionPaymentMakerRevise(workflowInstanceID, approverID, payment, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Payment maker data has been saved." });
                }
            }
        }

        /// <summary>
        /// Payment Checker revise transaction to Payment Maker or PPU Maker
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approver ID from Nintex WF</param>
        /// <param name="updateType">1: revise to Payment Maker; 2: revise to PPU Maker</param>
        /// <param name="payment">Payment Checker data</param>
        /// <returns></returns>
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/Workflow/{workflowInstanceID}/Payment/Checker/Update/{approverID}/{updateType}")]
        public HttpResponseMessage AddPaymentCheckerRevise(Guid workflowInstanceID, long approverID, int updateType, PaymentCheckerDetailModel payment)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdatePaymentCheckerRevise(workflowInstanceID, approverID, updateType, payment, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Payment Checker data has been saved." });
                }
            }
        }

        /// <summary>
        /// Get payment checker details of transaction by WF Instance ID
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approval ID</param>
        /// <returns></returns>
        [ResponseType(typeof(PaymentCheckerDetailModel))]
        [HttpGet]
        [Route("api/Workflow/{workflowInstanceID}/Payment/Checker/{approverID}")]
        public HttpResponseMessage GetPaymentCheckerDetails(Guid workflowInstanceID, long approverID)
        {
            PaymentDetailModel payment = new PaymentDetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            PaymentCheckerDetailModel output = new PaymentCheckerDetailModel();
            PaymentMakerDetailModel outputChecker = new PaymentMakerDetailModel();
            //get transaction history by fandi
            IList<TransactionDetailMaker> transactionmaker = new List<TransactionDetailMaker>();
            //get doubled opened task
            ClientTaskData clienttaskdata = new ClientTaskData();
            //end
            try
            {
                // #1 Get Payment Details
                if (!repository.GetPaymentDetails(workflowInstanceID, ref payment, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repository.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        // #3 Get Payment Checker Details
                        //if (!repository.GetPaymentCheckerDetails(workflowInstanceID, approverID, payment.Product.Name.Equals("OTT"), ref output, ref message))
                        if (!repository.GetPaymentCheckerDetails(workflowInstanceID, approverID, ref outputChecker, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Payment = payment;
                            output.Timelines = timelines;
                            output.Verify = outputChecker.Verify;
                            //get Transaction History by fandi
                            if (!repository.GetTransactionMaker(payment.ID, ref transactionmaker, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "5" + message });
                            }
                            else
                            {
                                //getTransaction history by fandi
                                output.TransactionMaker = transactionmaker;
                                //end
                            }
                            //add by fandi for check double task opened
                            //ClientTaskData clienttaskdata = null;
                            if (!repository.CheckTask(approverID, ref clienttaskdata, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = " Error Get Data Double Opened" + message });
                            }
                            else
                            {
                                output.ClienTaskData = clienttaskdata;
                            }
                            //end
                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {
                payment = null;
                timelines = null;
            }
        }

        /// <summary>
        /// Save Bizunit Checker
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approver ID from Nintex WF</param>
        /// <param name="data">Payment Maker data</param>
        /// <returns></returns>
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/Workflow/{workflowInstanceID}/Bizunit/Checker/{approverID}")]
        public HttpResponseMessage AddPaymentCheckerDetails(Guid workflowInstanceID, long approverID, PaymentCheckerDataModel data)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!deal_repository.AddBizUnitChecker(workflowInstanceID, approverID, data, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Biz Unit Checker data has been saved." });
                }
            }
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="workflowInstanceID"></param>
        /// <param name="approverID"></param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionCallbackDetailModel))]
        [HttpGet]
        [Route("api/Workflow/{workflowInstanceID}/Transaction/Callback/{approverID}")]
        public HttpResponseMessage GetTransactionCallbackDetails(Guid workflowInstanceID, long approverID)
        {
            TransactionDetailModel transaction = new TransactionDetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TransactionCallbackDetailModel output = new TransactionCallbackDetailModel();
            //get transaction history by fandi
            IList<TransactionDetailMaker> transactionmaker = new List<TransactionDetailMaker>();
            //get doubled opened task
            ClientTaskData clienttaskdata = new ClientTaskData();
            //end
            //end 
            try
            {
                // #1 Get Transaction Details
                if (!repository.GetTransactionDetails(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "1" });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repository.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "2" });
                    }
                    else
                    {
                        // #3 Get Transaction Checker Details getutc
                        if (!repository.GetTransactionCallbackDetails(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "3" });
                        }
                        else
                        {
                            //get Transaction History by fandi
                            if (!repository.GetTransactionMaker(transaction.ID, ref transactionmaker, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "5" + message });
                            }
                            else
                            {
                                output.Transaction = transaction;
                                output.Timelines = timelines;
                                output.TransactionMaker = transactionmaker;
                                //return Request.CreateResponse(HttpStatusCode.OK, output);
                            }
                            //end
                            //add by fandi for check double task opened
                            //ClientTaskData clienttaskdata = null;
                            if (!repository.CheckTask(approverID, ref clienttaskdata, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = " Error Get Data Double Opened" + message });
                            }
                            else
                            {
                                output.ClienTaskData = clienttaskdata;
                            }
                            //end
                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {

                transaction = null;
                timelines = null;
            }
        }

        /// <summary>
        /// Save Transaction Caller from PPU Caller
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approver ID from Nintex WF</param>
        /// <param name="data">Payment Maker data</param>
        /// <returns></returns>
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/Workflow/{workflowInstanceID}/Transaction/Callback/{approverID}")]
        public HttpResponseMessage AddTransactionCallbackDetails(Guid workflowInstanceID, long approverID, TransactionCallbackDetailModel data)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddTransactionCallback(workflowInstanceID, approverID, data, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "transaction callback data has been saved." });
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="workflowInstanceID"></param>
        /// <param name="approverID"></param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionContactDetailModel))]
        [HttpGet]
        [Route("api/Workflow/{workflowInstanceID}/Transaction/Contact/{approverID}")]
        public HttpResponseMessage GetTransactionContactDetails(Guid workflowInstanceID, long approverID)
        {
            TransactionDetailModel transaction = new TransactionDetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            IList<CustomerContactModel> contact = new List<CustomerContactModel>();
            TransactionContactDetailModel output = new TransactionContactDetailModel();
            //get transaction history by fandi
            IList<TransactionDetailMaker> transactionmaker = new List<TransactionDetailMaker>();
            //get doubled opened task
            ClientTaskData clienttaskdata = new ClientTaskData();
            //end
            try
            {
                // #1 Get Transaction Details
                if (!repository.GetTransactionDetails(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repository.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        // #3 Get Transaction Contact Details
                        if (!repository.GetTransactionContactDetails(workflowInstanceID, approverID, ref contact, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Contacts = contact;
                            output.Transaction = transaction;
                            output.Timelines = timelines;
                            //get Transaction History by fandi
                            if (!repository.GetTransactionMaker(transaction.ID, ref transactionmaker, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "5" + message });
                            }
                            else
                            {
                                //getTransaction history by fandi
                                output.TransactionMaker = transactionmaker;
                                //end
                            }
                            //add by fandi for check double task opened
                            //ClientTaskData clienttaskdata = null;
                            if (!repository.CheckTask(approverID, ref clienttaskdata, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = " Error Get Data Double Opened" + message });
                            }
                            else
                            {
                                output.ClienTaskData = clienttaskdata;
                            }
                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {

                transaction = null;
                timelines = null;
            }
        }

        /// <summary>
        /// Save Contact from PPU CBO
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approver ID from Nintex WF</param>
        /// <param name="data">Contact data</param>
        /// <returns></returns>
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/Workflow/{workflowInstanceID}/Transaction/Contact/{approverID}")]
        public HttpResponseMessage AddTransactionContactDetails(Guid workflowInstanceID, long approverID, IList<CustomerContactModel> data)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddTransactionContact(workflowInstanceID, approverID, data, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "transaction contact data has been saved." });
                }
            }
        }

        /// <summary>
        /// Get new transaction Status
        /// </summary>
        /// <param name="cif">cif</param>
        /// <param name="productID">productID</param>
        /// <param name="currencyID">currencyID</param>
        /// <param name="amountUSD">amountUSD</param>fv
        /// <param name="applicationDate">applicationDate</param>
        /// <param name="accountNumber">accountNumber</param>
        /// <param name="beneAcc">beneAcc</param>
        /// <param name="beneName">beneName</param>

        /// <returns></returns>
        [ResponseType(typeof(NewTransactionStatusModel))]
        [HttpPost]
        [Route("api/Workflow/Transaction/Check")]
        public HttpResponseMessage GetNewTransactionStatus(DoubleTransactionModel DoubleTransaction)
        {
            NewTransactionStatusModel data = new NewTransactionStatusModel();

            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.GetNewTransactionStatus(DoubleTransaction.CIF, DoubleTransaction.ProductID, DoubleTransaction.CurrencyID, DoubleTransaction.Amount, DoubleTransaction.ApplicationDate, DoubleTransaction.AccountNumber, DoubleTransaction.BeneAccount, DoubleTransaction.BeneName, ref data, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, data);
                }
            }
        }

        /// <summary>
        /// BRANCH Dormant reactivation
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approver ID from Nintex WF</param>
        /// <param name="transactionChecker">Transaction checker data</param>
        /// <returns></returns>
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/Workflow/{workflowInstanceID}/Transaction/ReactivationDormant/{approverID}")]
        public HttpResponseMessage ReactivationBranchDormantAccount(Guid workflowInstanceID, long approverID, TransactionCheckerDetailModel transactionChecker)
        {

            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateTransactionDormant(workflowInstanceID, approverID, transactionChecker, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction has been saved." });
                }
            }
        }

        /// <summary>
        /// Update approval data from checker callback
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approver ID from Nintex WF</param>
        /// <param name="transactionChecker">Transaction checker data</param>
        /// <returns></returns>
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/Workflow/{workflowInstanceID}/Transaction/CheckerAfterCallback/Update/{approverID}")]
        public HttpResponseMessage UpdateTransactionCheckerAfterCallbackDetails(Guid workflowInstanceID, long approverID, TransactionCheckerAfterCallbackDetailModel transactionChecker)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateTransactionCheckerCallbackDetails(workflowInstanceID, approverID, transactionChecker, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been updated." });
                }
            }
        }

        /// <summary>
        /// Get all transaction normal by WF Instance ID
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approve ID from Nintex WF</param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionCheckerDetailModel))]
        [HttpGet]
        [Route("api/Workflow/{workflowInstanceID}/Transaction/AllNormal/{approverID}")]
        public HttpResponseMessage GetTransactionNormalDetails(Guid workflowInstanceID, long approverID)
        {
            TransactionDetailModel transaction = new TransactionDetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TransactionNormalAllModel output = new TransactionNormalAllModel();
            PaymentDetailModel payment = new PaymentDetailModel();
            TransactionCheckerAfterCallbackDetailModel callback = new TransactionCheckerAfterCallbackDetailModel();
            TransactionCallbackDetailModel callbackDetail = new TransactionCallbackDetailModel();
            TransactionCheckerDetailModel checker = new TransactionCheckerDetailModel();
            //get transaction history by fandi
            IList<TransactionDetailMaker> transactionmaker = new List<TransactionDetailMaker>();
            //end
            try
            {
                // #1 Get Transaction Details
                if (!repository.GetTransactionNormalDetails(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "1" + message });
                }

                // #2 Get Transaction Timeline
                if (!repository.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "2" + message });
                }


                if (!repository.GetTransactionCheckerDetails(workflowInstanceID, approverID, ref checker, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }

                if (!repository.GetNormalPaymentDetails(workflowInstanceID, ref payment, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "4" + message });
                }

                if (!repository.GetTransactionAllNormalCallbackDetails(workflowInstanceID, approverID, ref callback, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "3" + message });
                }

                bool isGettingCallbacks = true;
                if (callback != null)
                {
                    // #3 Get Transaction Checker Details getutc
                    isGettingCallbacks = repository.GetTransactionCallbackDetails(workflowInstanceID, approverID, ref callbackDetail, ref message);
                }

                if (!isGettingCallbacks)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "3" });
                }

                //get Transaction History by fandi
                if (!repository.GetTransactionMaker(transaction.ID, ref transactionmaker, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "5" + message });
                }
                //end



                output.Transaction = transaction;
                output.Checker = checker;
                output.Payment = payment;
                output.Timelines = timelines;
                output.Callback = callback;
                output.callbackDetail = callbackDetail;
                //getTransaction history by fandi
                output.TransactionMaker = transactionmaker;
                //end

                return Request.CreateResponse(HttpStatusCode.OK, output);








            }
            finally
            {

                transaction = null;
                timelines = null;
            }
        }

        /// <summary>
        /// Cancel Payment Transaction PPU Maker Approval
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approver ID from Nintex WF</param>
        /// <returns></returns>
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/Workflow/{workflowInstanceID}/Transaction/Cancel/")]
        public HttpResponseMessage CancelPaymentTransaction(Guid workflowInstanceID, TransactionDetailModel transaction)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.CancelTransaction(workflowInstanceID, transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "revise transaction deal complete." });
                }
            }
        }

        [ResponseType(typeof(TransactionCheckerDetailModel))]
        [HttpGet]
        [Route("api/Workflow/{workflowInstanceID}/PaymentCheckerCancellation/{approverID}")]
        public HttpResponseMessage GetCancelPaymentIpe(Guid workflowInstanceID, long approverID)
        {
            TransactionDetailModel transactionDetail = new TransactionDetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TransactionCheckerDetailModel checkerDetail = new TransactionCheckerDetailModel();
            IList<TransactionDetailMaker> transactionMaker = new List<TransactionDetailMaker>();
            ClientTaskData clientTaskData = new ClientTaskData();

            try
            {
                // #1 Get Transaction Details
                if (!repository.GetTransactionDetailsIPE(workflowInstanceID, ref transactionDetail, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repository.GetTransactionTimelineIPE(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        // #3 Get Transaction Checker Details
                        if (!repository.GetTransactionCheckerDetailsIPE(workflowInstanceID, approverID, ref checkerDetail, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            checkerDetail.Transaction = transactionDetail;
                            checkerDetail.Timelines = timelines;
                            if (!repository.GetTransactionMakerIPE(transactionDetail.ID, ref transactionMaker, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "5" + message });
                            }
                            else
                            {
                                checkerDetail.TransactionMaker = transactionMaker;
                            }
                            if (!repository.CheckTaskIPE(approverID,workflowInstanceID, ref clientTaskData, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = " Error Get Data Double Opened" + message });
                            }
                            else
                            {
                                checkerDetail.ClienTaskData = clientTaskData;
                            }

                            return Request.CreateResponse(HttpStatusCode.OK, checkerDetail);
                        }
                    }
                }
            }
            finally
            {

                checkerDetail = null;
                timelines = null;
            }
        }

        /// <summary>
        /// Get Application ID
        /// </summary>
        /// <param name="productID">ID of product</param>
        /// <returns></returns>
        [ResponseType(typeof(object))]
        [Route("api/Transaction/ApplicationIDUtilize/{UnderlyingID}")]
        [HttpGet]
        public HttpResponseMessage GetApplicationIDUnderlying(long UnderlyingID)
        {
            IList<ApplicationIDUtilize> DataUtilize = new List<ApplicationIDUtilize>();
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.GetApplicationIDUtilize(UnderlyingID,ref DataUtilize, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, DataUtilize);
                }
            }
        }

        /// <summary>
        /// Get Application ID
        /// </summary>
        /// <param name="productID">ID of product</param>
        /// <returns></returns>
        [ResponseType(typeof(object))]
        [Route("api/Transaction/UnderlyingUtilize/{cif}")]
        [HttpGet]
        public HttpResponseMessage GetUnderlyingTrans(string cif)
        {
            IList<ApplicationIDUtilize> DataUtilize = new List<ApplicationIDUtilize>();
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.GetUnderlyingUtilize(cif, ref DataUtilize, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, DataUtilize);
                }
            }
        }

        //add aridya 20170108 save double transaction history
        /// <summary>
        /// Save Double Transaction History
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/Workflow/Transaction/SaveDoubleTransactionHistory")]
        public HttpResponseMessage SaveDoubleTransactionHistory(DoubleTransactionHistoryModel doubleTrxHistoryModel)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.SaveDoubleTransactionHistory(doubleTrxHistoryModel, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Double transaction history data has been saved." });
                }
            }
        }

        [ResponseType(typeof(TransactionCheckerDetailModel))]
        [HttpGet]
        [Route("api/Workflow/{workflowInstanceID}/Transaction/ForceCompleteIPE/{approverID}")]
        public HttpResponseMessage GetTransactionForceCompleteIPE(Guid workflowInstanceID, long approverID)
        {
            TransactionCheckerDetailModel output = new TransactionCheckerDetailModel();
            TransactionDetailModel transaction = new TransactionDetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            IList<TransactionCallbackTimeModel> callbacktime = new List<TransactionCallbackTimeModel>();
            IList<TransactionDetailMaker> transactionmaker = new List<TransactionDetailMaker>();
            ClientTaskData clienttaskdata = new ClientTaskData();
            try
            {
                // #1 Get Transaction Details
                if (!repository.GetTransactionDetailsIPE(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    output.Transaction = transaction;

                    // #2 Get Transaction Timeline
                    if (!repository.GetTransactionTimelineIPE(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        output.Timelines = timelines;

                        // #3 Get Callbacktime
                        if (!repository.GetTransactionCallbackTime(transaction.ID, ref callbacktime, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.CallbackTime = callbacktime;

                            //Get transaction history
                            if (!repository.GetTransactionMakerIPE(transaction.ID, ref transactionmaker, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                            }
                            else
                            {
                                output.TransactionMaker = transactionmaker;
                            }

                            if (!repository.CheckTaskIPE(approverID, workflowInstanceID, ref clienttaskdata, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                            }
                            else
                            {
                                output.ClienTaskData = clienttaskdata;
                            }

                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {
                transaction = null;
                timelines = null;
                callbacktime = null;
                transactionmaker = null;
                clienttaskdata = null;
            }
        }

    }
}