﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Web;
using DBS.Entity;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Models
{
    #region Property
    public class RateTypeModel
    {
        /// <summary>
        /// RateType ID.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// RateTypes Name.
        /// </summary>
        [MaxLength(50, ErrorMessage = "Rate Type Name is must be in {1} characters.")]
        public string Name { get; set; }

        /// <summary>
        /// RateType Description.
        /// </summary>
        [MaxLength(255, ErrorMessage = "Maximum length is {1} characters.")]
        public string Description { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }
    }

    public class RateTypeRow : RateTypeModel
    {
        public int RowID { get; set; }

    }

    public class RateTypeGrid : Grid
    {
        public IList<RateTypeRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class RateTypeFilter : Filter { }
    #endregion

    #region Interface
    public interface IRateTypeRepository : IDisposable
    {
        bool GetRateTypeByID(int id, ref RateTypeModel rateType, ref string message);
        bool GetRateType(ref IList<RateTypeModel> rateTypes, int limit, int index, ref string message);
        bool GetRateType(ref IList<RateTypeModel> rateTypes, ref string message);
        bool GetRateType(int page, int size, IList<RateTypeFilter> filters, string sortColumn, string sortOrder, ref RateTypeGrid RateType, ref string message);
        bool GetRateType(RateTypeFilter filter, ref IList<RateTypeModel> rateTypes, ref string message);
        bool GetRateType(string key, int limit, ref IList<RateTypeModel> rateTypes, ref string message);
        bool AddRateType(RateTypeModel rateType, ref string message);
        bool UpdateRateType(int id, RateTypeModel rateType, ref string message);
        bool DeleteRateType(int id, ref string message);
    }
    #endregion

    #region Repository
    public class RateTypeRepository : IRateTypeRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetRateTypeByID(int id, ref RateTypeModel rateType, ref string message)
        {
            bool isSuccess = false;

            try
            {
                rateType = (from a in context.RateTypes
                               where a.IsDeleted.Equals(false) && a.RateTypeID.Equals(id)
                               select new RateTypeModel
                               {
                                   ID = a.RateTypeID,
                                   Name = a.RateTypeName,
                                   Description = a.RateTypeDescription,
                                   LastModifiedDate = a.CreateDate,
                                   LastModifiedBy = a.CreateBy
                               }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetRateType(ref IList<RateTypeModel> rateTypes, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    rateTypes = (from a in context.RateTypes
                                    where a.IsDeleted.Equals(false)
                                    orderby new { a.RateTypeName, a.RateTypeDescription }
                                    select new RateTypeModel
                                    {
                                        ID = a.RateTypeID,
                                        Name = a.RateTypeName,
                                        Description = a.RateTypeDescription,
                                        LastModifiedBy = a.CreateBy,
                                        LastModifiedDate = a.CreateDate
                                    }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetRateType(ref IList<RateTypeModel> rateTypes, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    rateTypes = (from a in context.RateTypes
                                    where a.IsDeleted.Equals(false)
                                    orderby new { a.RateTypeName, a.RateTypeDescription }
                                    select new RateTypeModel
                                    {
                                        ID = a.RateTypeID,
                                        Name = a.RateTypeName,
                                        Description = a.RateTypeDescription,
                                        LastModifiedBy = a.CreateBy,
                                        LastModifiedDate = a.CreateDate
                                    }).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetRateType(int page, int size, IList<RateTypeFilter> filters, string sortColumn, string sortOrder, ref RateTypeGrid rateTypeGrid, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                RateTypeModel filter = new RateTypeModel();
                if (filters != null)
                {
                    filter.Name = (string)filters.Where(a => a.Field.Equals("Name")).Select(a => a.Value).SingleOrDefault();
                    filter.Description = (string)filters.Where(a => a.Field.Equals("Description")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.RateTypes
                                let isRateTypeName = !string.IsNullOrEmpty(filter.Name)
                                let isRateTypeDescription = !string.IsNullOrEmpty(filter.Description)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) &&
                                    (isRateTypeName ? a.RateTypeName.Contains(filter.Name) : true) &&
                                    (isRateTypeDescription ? a.RateTypeDescription.Contains(filter.Description) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new RateTypeModel
                                {
                                    ID = a.RateTypeID,
                                    Name = a.RateTypeName,
                                    Description = a.RateTypeDescription,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    rateTypeGrid.Page = page;
                    rateTypeGrid.Size = size;
                    rateTypeGrid.Total = data.Count();
                    rateTypeGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    rateTypeGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new RateTypeRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            Name = a.Name,
                            Description = a.Description,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetRateType(RateTypeFilter filter, ref IList<RateTypeModel> rateTypes, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetRateType(string key, int limit, ref IList<RateTypeModel> rateTypes, ref string message)
        {
            bool isSuccess = false;

            try
            {
                rateTypes = (from a in context.RateTypes
                                where a.IsDeleted.Equals(false) &&
                                    a.RateTypeName.Contains(key) || a.RateTypeDescription.Contains(key)
                                orderby new { a.RateTypeName, a.RateTypeDescription }
                                select new RateTypeModel
                                {
                                    Name = a.RateTypeName,
                                    Description = a.RateTypeDescription
                                }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddRateType(RateTypeModel rateType, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (!context.RateTypes.Where(a => a.RateTypeName.Equals(rateType.Name) && a.IsDeleted.Equals(false)).Any())
                {
                    context.RateTypes.Add(new Entity.RateType()
                    {
                        RateTypeName = rateType.Name,
                        RateTypeDescription = rateType.Description,
                        IsDeleted = false,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    });

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Rate Type data for Rate Type Name {0} is already exist.", rateType.Name);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateRateType(int id, RateTypeModel rateType, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.RateType data = context.RateTypes.Where(a => a.RateTypeID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    if (context.RateTypes.Where(a => a.RateTypeID != rateType.ID && a.RateTypeName.Equals(rateType.Name) && a.IsDeleted.Equals(false)).Count() >= 1)
                    {
                        message = string.Format("Rate Type Name {0} is exist.", rateType.Name);
                    }
                    else
                    {
                        data.RateTypeName = rateType.Name;
                        data.RateTypeDescription = rateType.Description;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        context.SaveChanges();

                        isSuccess = true;
                    }
                }
                else
                {
                    message = string.Format("Rate Type data for Rate Type ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeleteRateType(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.RateType data = context.RateTypes.Where(a => a.RateTypeID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Rate Type data for Rate Type ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}