﻿using DBS.Entity;
using DBS.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Linq.Dynamic;

namespace DBS.WebAPI.Models
{

    #region Property
    /// <summary>
    /// Information of Human Workflow Tasks on Nintex Workflow
    /// </summary>
    public class SPUserTask
    {
        /// <summary>
        /// Contains basic information about Nintex Workflow task.
        /// </summary>
        public WorkflowContext WorkflowContext { get; set; }

        /// <summary>
        /// Contain all information about current workflow task.
        /// </summary>
        public WorkflowTask CurrentTask { get; set; }

        /// <summary>
        /// Contain all information about latest progress of workflow task.
        /// </summary>
        public WorkflowTask ProgressTask { get; set; }

        /// <summary>
        /// Transaction data
        /// </summary>
        public TransactionTaskModel Transaction { get; set; }
    }

    /*
    public class Workflow
    {
        public long ApproverId { get; set; }
        public Guid SPListID { get; set; }
        public int SPListItemID { get; set; }
        public Guid SPTaskListID { get; set; }
        public int SPTaskListItemID { get; set; }
        public string WorkflowInitiator { get; set; }
        public Guid WorkflowInstanceID { get; set; }
        public string WorkflowName { get; set; }
        public DateTime WorkflowStartTime { get; set; }
        public int WorkflowState { get; set; }
        public string WorkflowStateDescription { get; set; }
        public string WorkflowTaskTitle { get; set; }
        public string WorkflowTaskActivityTitle { get; set; }
        public string WorkflowTaskUser { get; set; }
        public bool WorkflowTaskIsSPGroup { get; set; }
        public DateTime WorkflowTaskEntryTime { get; set; }
        public DateTime WorkflowTaskExitTime { get; set; }
        public int WorkflowOutcome { get; set; }
        public string WorkflowOutcomeDescription { get; set; }
        public int WorkflowCustomOutcome { get; set; }
        public string WorkflowCustomOutcomeDescription { get; set; }
        public string WorkflowComments { get; set; }
        public string WorkflowTaskTitleLatest { get; set; }
        public string WorkflowTaskActivityTitleLatest { get; set; }
        public string WorkflowTaskUserLatest { get; set; }
        public DateTime WorkflowTaskEntryTimeLatest { get; set; }
        public DateTime WorkflowTaskExitTimeLatest { get; set; }
        public int WorkflowOutcomeLatest { get; set; }
        public string WorkflowOutcomeDescriptionLatest { get; set; }
        public int WorkflowCustomOutcomeLatest { get; set; }
        public string WorkflowCustomOutcomeDescriptionLatest { get; set; }
        public string WorkflowCommentsLatest { get; set; }
    }*/

    public class UserTask
    {
        //Workflow Data
        public WorkflowContext WorkflowContext { get; set; }

        //Transaction Data
        public TransactionTaskModel Transaction { get; set; }
    }

    public class UserTaskRow : UserTask
    {
        public int RowID { get; set; }
    }

    public class UserTaskGrid : Grid
    {
        public IList<UserTaskRow> Rows { get; set; }
    }

    public class WorkflowContext
    {
        /// <summary>
        /// 
        /// </summary>
        public long ApproverID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Guid SPWebID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Guid SPSiteID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Guid SPListID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int SPListItemID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Guid SPTaskListID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int SPTaskListItemID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Initiator { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Guid WorkflowInstanceID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Guid WorkflowID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string WorkflowName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? StartTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int StateID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string StateDescription { get; set; }

        /// <summary>
        /// 
        /// </summary>

        public string TransactionStatus { get; set; }

        /// <summary>
        /// Current user have an authorization to completing the task.
        /// true : if the task is assinged to current user or current user groups
        /// false : if the task is not assigned to current user or current user groups
        /// </summary>
        public bool IsAuthorized { get; set; }

    }

    public class WorkflowTask
    {
        /// <summary>
        /// Nintex Task Type
        /// </summary>
        public int TaskTypeID { get; set; }

        /// <summary>
        /// Nintex Task Type Description
        /// </summary>
        public string TaskTypeDescription { get; set; }

        /// <summary>
        /// Nintex task title. Reference from Sharepoint Task List title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Activity title from current nintex task.
        /// </summary>
        public string ActivityTitle { get; set; }

        /// <summary>
        /// Sharepoint User or Group that was assigned for current nintex task.
        /// </summary>
        public string User { get; set; }

        /// <summary>
        /// Current user is a Sharepoint user or Group.
        /// </summary>
        public bool IsSPGroup { get; set; }

        /// <summary>
        /// Time when the task is received to assigned user / group.
        /// </summary>
        public DateTime? EntryTime { get; set; }

        /// <summary>
        ///  Time when the task is completed by assigned user / group.
        /// </summary>
        public DateTime? ExitTime { get; set; }

        /// <summary>
        /// Outcome id of current task.
        /// </summary>
        public int? OutcomeID { get; set; }

        /// <summary>
        /// Outcome Description of current task.
        /// </summary>
        public string OutcomeDescription { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? CustomOutcomeID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CustomOutcomeDescription { get; set; }

        /// <summary>
        /// User comment when completing the task.
        /// </summary>
        public string Comments { get; set; }

        public string Approver { get; set; }
        public string FinalOutcome { get; set; }
    }

    public class WorkflowTaskLatest
    {
        /// <summary>
        /// Nintex Task Type
        /// </summary>
        public int TaskTypeIDLatest { get; set; }

        /// <summary>
        /// Nintex Task Type Description
        /// </summary>
        public string TaskTypeDescriptionLatest { get; set; }

        /// <summary>
        /// Nintex task title. Reference from Sharepoint Task List title.
        /// </summary>
        public string TitleLatest { get; set; }

        /// <summary>
        /// Activity title from current nintex task.
        /// </summary>
        public string ActivityTitleLatest { get; set; }

        /// <summary>
        /// Sharepoint User or Group that was assigned for current nintex task.
        /// </summary>
        public string UserLatest { get; set; }

        /// <summary>
        /// Current user is a Sharepoint user or Group.
        /// </summary>
        public bool IsSPGroupLatest { get; set; }

        /// <summary>
        /// Time when the task is received to assigned user / group.
        /// </summary>
        public DateTime? EntryTimeLatest { get; set; }

        /// <summary>
        ///  Time when the task is completed by assigned user / group.
        /// </summary>
        public DateTime? ExitTimeLatest { get; set; }

        /// <summary>
        /// Outcome id of current task.
        /// </summary>
        public int? OutcomeIDLatest { get; set; }

        /// <summary>
        /// Outcome Description of current task.
        /// </summary>
        public string OutcomeDescriptionLatest { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? CustomOutcomeIDLatest { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CustomOutcomeDescriptionLatest { get; set; }

        /// <summary>
        /// User comment when completing the task.
        /// </summary>
        public string CommentsLatest { get; set; }
    }

    public class SPUserTaskRow : SPUserTask
    {
        public int RowID { get; set; }
    }

    public class NintexTaskRow : V_NintexTasks
    {
        public int RowID { get; set; }
    }

    public class SPUserTaskGrid : Grid
    {
        public IList<SPUserTaskRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class SPUserTaskFilter : Filter { }

    #endregion

    #region Interface
    public interface ISPUserTaskTunningRepository
    {

        #region old function (already delete)
        /*
        bool GetUserTasks(Guid webID, Guid siteID, IList<Guid> workflowIDs, IList<string> state, IList<string> outcome, IList<string> customOutcome, bool showContribute, bool showActiveTask, int? page, int? size, string sortColumn, string sortOrder, IList<SPUserTaskFilter> filters, ref SPUserTaskGrid userTaskGrid, ref string message);
        bool GetUserTasksSP(Guid webID, Guid siteID, IList<Guid> workflowIDs, IList<string> state, IList<string> outcome, IList<string> customOutcome, bool showContribute, bool showActiveTask, int? page, int? size, string sortColumn, string sortOrder, IList<SPUserTaskFilter> filters, ref SPUserTaskGrid userTaskGrid, ref string message);
        bool GetCanceledUserTasks(Guid webID, Guid siteID, IList<Guid> workflowIDs, IList<string> state, IList<string> outcome, IList<string> customOutcome, bool showContribute, bool showActiveTask, int? page, int? size, string sortColumn, string sortOrder, IList<SPUserTaskFilter> filters, ref SPUserTaskGrid userTaskGrid, ref string message);
        bool GetCompletedUserTasks(Guid webID, Guid siteID, IList<Guid> workflowIDs, IList<string> state, IList<string> outcome, IList<string> customOutcome, bool showContribute, bool showActiveTask, int? page, int? size, string sortColumn, string sortOrder, IList<SPUserTaskFilter> filters, ref SPUserTaskGrid userTaskGrid, ref string message);
        bool GetMonitoringUserTasks(Guid webID, Guid siteID, IList<Guid> workflowIDs, IList<string> state, IList<string> outcome, IList<string> customOutcome, bool showContribute, bool showActiveTask, int? page, int? size, string sortColumn, string sortOrder, IList<SPUserTaskFilter> filters, ref SPUserTaskGrid userTaskGrid, ref string message);
        bool GetAllTransactionUserTasks(Guid webID, Guid siteID, IList<Guid> workflowIDs, IList<string> state, IList<string> outcome, IList<string> customOutcome, bool showContribute, bool showActiveTask, int? page, int? size, string sortColumn, string sortOrder, IList<SPUserTaskFilter> filters, ref SPUserTaskGrid userTaskGrid, ref string message);
        */
        #endregion

        #region Home Tuning
        bool GetHomeUserTasks(Guid webID, Guid siteID, IList<Guid> workflowIDs, IList<string> state, IList<string> outcome, IList<string> customOutcome, bool showContribute, bool showActiveTask, int? page, int? size, string sortColumn, string sortOrder, IList<SPUserTaskFilter> filters, ref SPUserTaskGrid userTaskGrid, ref string message);
        bool GetAllTransactionSPUserTasks(Guid webID, Guid siteID, IList<Guid> workflowIDs, IList<string> state, IList<string> outcome, IList<string> customOutcome, bool showContribute, bool showActiveTask, int? page, int? size, string sortColumn, string sortOrder, IList<SPUserTaskFilter> filters, ref SPUserTaskGrid userTaskGrid, ref string message);
        bool GetCanceledSPUserTasks(Guid webID, Guid siteID, IList<Guid> workflowIDs, IList<string> state, IList<string> outcome, IList<string> customOutcome, bool showContribute, bool showActiveTask, int? page, int? size, string sortColumn, string sortOrder, IList<SPUserTaskFilter> filters, ref SPUserTaskGrid userTaskGrid, ref string message);
        bool GetCompletedSPUserTasks(Guid webID, Guid siteID, IList<Guid> workflowIDs, IList<string> state, IList<string> outcome, IList<string> customOutcome, bool showContribute, bool showActiveTask, int? page, int? size, string sortColumn, string sortOrder, IList<SPUserTaskFilter> filters, ref SPUserTaskGrid userTaskGrid, ref string message);
        bool GetMonitoringSPUserTasks(Guid webID, Guid siteID, IList<Guid> workflowIDs, IList<string> state, IList<string> outcome, IList<string> customOutcome, bool showContribute, bool showActiveTask, int? page, int? size, string sortColumn, string sortOrder, IList<SPUserTaskFilter> filters, ref SPUserTaskGrid userTaskGrid, ref string message);

        #endregion
        #region Tunning Loan
        bool GetHomeUserLoanIMTasks(Guid webID, Guid siteID, IList<Guid> workflowIDs, IList<string> state, IList<string> outcome, IList<string> customOutcome, bool showContribute, bool showActiveTask, int? page, int? size, string sortColumn, string sortOrder, IList<SPUserTaskFilter> filters, ref SPUserTaskGrid userTaskGrid, ref string message);
        bool GetHomeUserLoanROTasks(Guid webID, Guid siteID, IList<Guid> workflowIDs, IList<string> state, IList<string> outcome, IList<string> customOutcome, bool showContribute, bool showActiveTask, int? page, int? size, string sortColumn, string sortOrder, IList<SPUserTaskFilter> filters, ref SPUserTaskGrid userTaskGrid, ref string message);
        
        #endregion

        #region Home TMO

        //Basri 05.10.2015
        bool GetHomeTMOUserTasks(Guid webID, Guid siteID, IList<Guid> workflowIDs, IList<string> state, IList<string> outcome, IList<string> customOutcome, bool showContribute, bool showActiveTask, int? page, int? size, string sortColumn, string sortOrder, IList<SPUserTaskFilter> filters, ref SPUserTaskGrid userTaskGrid, ref string message);
        //end basri

        #endregion

        #region Home LOAN
        bool GetHomeLOANTasks(Guid webID, Guid siteID, IList<Guid> workflowIDs, IList<string> state, IList<string> outcome, IList<string> customOutcome, bool showContribute, bool showActiveTask, int? page, int? size, string sortColumn, string sortOrder, IList<SPUserTaskFilter> filters, ref SPUserTaskGrid userTaskGrid, ref string message);
        
        #endregion
    }
    #endregion

    #region Repository
    public class SPUserTaskTunningRepository : ISPUserTaskTunningRepository
    {
        private CurrentUser currentUser = new CurrentUser();

        #region old Function (already delete)
        /*
        public bool GetUserTasks(Guid webID, Guid siteID, IList<Guid> workflowIDs, IList<string> state, IList<string> outcome, IList<string> customOutcome, bool showContribute, bool showActiveTask, int? page, int? size, string sortColumn, string sortOrder, IList<SPUserTaskFilter> filters, ref SPUserTaskGrid userTaskGrid, ref string message)
        {
            bool isSuccess = false;

            using (DBSEntities context = new DBSEntities())
            {
                try
                {
                    int skip = (page.Value - 1) * size.Value;
                    int take = size.Value;

                    SPUserTask filter = new SPUserTask();
                    filter.WorkflowContext = new WorkflowContext();
                    filter.CurrentTask = new WorkflowTask();
                    filter.ProgressTask = new WorkflowTask();
                    filter.Transaction = new TransactionTaskModel();
                    filter.Transaction.IsFXTransaction = null;

                    DateTime maxDateEntryTime = new DateTime();
                    DateTime maxDateExitTime = new DateTime();

                    if (filters != null)
                    {
                        #region Transaction
                        filter.Transaction.ApplicationID = filters.Where(sp => sp.Field.Equals("ApplicationID")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.Customer = filters.Where(sp => sp.Field.Equals("Customer")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.Product = filters.Where(sp => sp.Field.Equals("Product")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.Currency = filters.Where(sp => sp.Field.Equals("Currency")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.DebitAccNumber = (string)filters.Where(sp => sp.Field.Equals("DebitAccNumber")).Select(sp => sp.Value).SingleOrDefault();
                        filter.CurrentTask.ActivityTitle = filters.Where(sp => sp.Field.Equals("ActivityTitle")).Select(sp => sp.Value).SingleOrDefault();
                        filter.WorkflowContext.TransactionStatus = filters.Where(sp => sp.Field.Equals("TransactionStatus")).Select(sp => sp.Value).SingleOrDefault();
                        filter.ProgressTask.Approver = filters.Where(sp => sp.Field.Equals("UserApprover")).Select(sp => sp.Value).SingleOrDefault();
                        
                        string amount = filters.Where(sp => sp.Field.Equals("Amount")).Select(sp => sp.Value).SingleOrDefault();
                        if (!string.IsNullOrEmpty(amount))
                        {
                            decimal valueAmount;
                            bool resultAmount = decimal.TryParse(amount, out valueAmount);
                            if (resultAmount)
                            {
                                filter.Transaction.Amount = valueAmount;
                            }
                            else
                            {
                                userTaskGrid.Page = page.Value;
                                userTaskGrid.Size = size.Value;
                                userTaskGrid.Total = 0;
                                return true;
                            }
                        }

                        string amountUSD = filters.Where(sp => sp.Field.Equals("AmountUSD")).Select(sp => sp.Value).SingleOrDefault();
                        if (!string.IsNullOrEmpty(amountUSD))
                        {
                            filter.Transaction.AmountUSD = Decimal.Parse(amountUSD);
                        }

                        filter.Transaction.IsFXTransactionValue = filters.Where(sp => sp.Field.Equals("IsFXTransaction")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.IsTopUrgentValue = filters.Where(sp => sp.Field.Equals("IsTopUrgent")).Select(sp => sp.Value).SingleOrDefault();

                        filter.CurrentTask.User = filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
                        filter.ProgressTask.User = filters.Where(a => a.Field.Equals("UserApprover")).Select(a => a.Value).SingleOrDefault();

                        if (filters.Where(a => a.Field.Equals("EntryTime")).Any())
                        {
                            filter.CurrentTask.EntryTime = DateTime.Parse((string)filters.Where(a => a.Field.Equals("EntryTime")).Select(a => a.Value).SingleOrDefault());
                            maxDateEntryTime = filter.CurrentTask.EntryTime.Value.AddDays(1);
                        }

                        if (filters.Where(a => a.Field.Equals("ExitTime")).Any())
                        {
                            filter.CurrentTask.ExitTime = DateTime.Parse(filters.Where(a => a.Field.Equals("ExitTime")).Select(a => a.Value).SingleOrDefault());
                            maxDateExitTime = filter.CurrentTask.ExitTime.Value.AddDays(1);
                        }                       
                       
                        #endregion
                    }

                    string userName = currentUser.GetCurrentUser().LoginName;
                    string[] roles = currentUser.GetCurrentUser().Roles.Select(r => r.Name).ToArray();
                    Guid[] workflowID = workflowIDs.ToArray();

                    #region parameter
                    string Username = currentUser.GetCurrentUser().LoginName;
                    string ApplicationID = filter.Transaction.ApplicationID;
                    string CustomerName = filter.Transaction.Customer;
                    string ProductName = filter.Transaction.Product;
                    string CurrencyCode = filter.Transaction.Currency;
                    Decimal? TransactionAmount = filter.Transaction.Amount;
                    string DebitAccNumber = filter.Transaction.DebitAccNumber;
                    string IsFXTransaction = filter.Transaction.IsFXTransactionValue;
                    string Urgency = filter.Transaction.IsTopUrgentValue;
                    string ModifiedBy = filter.ProgressTask.Approver;
                    #endregion

                    var data = (from a in context.SP_GetTaskUser(Username,
                                                                ApplicationID,
                                                                CustomerName,
                                                                ProductName,
                                                                CurrencyCode,
                                                                TransactionAmount,
                                                                DebitAccNumber,
                                                                IsFXTransaction,
                                                                Urgency,
                                                                null, 
                                                                ModifiedBy, 
                                                                null)
                                    let TransactionStatus = a.TransactionStatus                                   
                                    let ExitTime = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                    let isTransactionStatus = !string.IsNullOrEmpty(filter.WorkflowContext.TransactionStatus)                                    
                                    let isExitTime = filter.CurrentTask.EntryTime.HasValue
                                    
                                where 
                                    (isTransactionStatus ? a.TransactionStatus.ToLower().Contains(filter.WorkflowContext.TransactionStatus.ToLower()) : true) &&                                    
                                    (isExitTime ? (a.WorkflowTaskEntryTime >= filter.CurrentTask.EntryTime.Value && a.WorkflowTaskEntryTime <= maxDateEntryTime) : true)
                                 select new
                                {
                                    #region Workflow Context
                                    SPWebID = a.SPWebID,
                                    SPSiteID = a.SPSiteID,
                                    SPListID = a.SPListID,
                                    SPListItemID = a.SPListItemID,
                                    SPTaskListID = a.SPTaskListID,
                                    SPTaskListItemID = a.SPTaskListItemID,
                                    ApproverID = a.WorkflowApproverID,
                                    Initiator = a.WorkflowInitiator,
                                    InitiatorDisplayName = a.UserDisplayNameInitiator,
                                    StartTime = a.WorkflowStartTime,
                                    StateID = a.WorkflowState,
                                    StateDescription = a.WorkflowStateDescription,
                                    TransactionStatus = TransactionStatus,
                                    //StateDescription = a.WorkflowStateDescription.ToLower() == "running" ? a.WorkflowTaskActivityTitle : a.WorkflowStateDescription.ToLower() == "complete" ? ((a.ApplicationID.ToLower().StartsWith("fx") && a.WorkflowOutcomeDescription.ToLower() == "rejected") || a.WorkflowCustomOutcomeDescription.ToLower() == "approve cancellation" ? "Cancelled"  : "Completed") : a.WorkflowStateDescription.Trim(),
                                    WorkflowID = a.WorkflowID,
                                    WorkflowName = a.WorkflowName,
                                    WorkflowInstanceID = a.WorkflowInstanceID,
                                    IsAuthoriaed = a.WorkflowTaskIsSPGroup ? (roles.Contains(a.WorkflowTaskUser) ? true : false) : (a.WorkflowTaskUser.Equals(userName) ? true : false),
                                    #endregion

                                    #region Workflow Task
                                    TaskType = a.WorkflowTaskType,
                                    TaskTypeDescription = a.WorkflowTaskTypeDescription,
                                    Title = a.WorkflowTaskTitle,
                                    ActivityTitle = a.WorkflowTaskActivityTitle,
                                    User = a.WorkflowTaskUser,
                                    UserDisplayName = (a.UserDisplayNameTask != null) ? a.UserDisplayNameTask : "",
                                    IsSPGroup = a.WorkflowTaskIsSPGroup,
                                    EntryTime = a.WorkflowTaskEntryTime,
                                    ExitTime = ExitTime, //a.WorkflowTaskExitTime,
                                    OutcomeID = a.WorkflowOutcome,
                                    OutcomeDescription = a.WorkflowOutcomeDescription,
                                    CustomOutcomeID = a.WorkflowCustomOutcome,
                                    CustomOutcomeDescription = a.WorkflowCustomOutcomeDescription,
                                    Comments = a.WorkflowComments,
                                    Approver = a.WorkflowDisplayName,
                                    #endregion

                                    #region Workflow Task Latest
                                    ActivityTitleLatest = a.WorkflowLastActivityTitle,
                                    CommentsLatest = a.WorkflowLastComments,
                                    CustomOutcomeIDLatest = a.WorkflowLastCustomOutcome,
                                    CustomOutcomeDescriptionLatest = a.WorkflowLastCustomOutcomeDescription,
                                    UserDisplayNameLatest = a.WorkflowLastDisplayName,
                                    OutcomeIDLatest = a.WorkflowLastOutcome,
                                    OutcomeDescriptionLatest = a.WorkflowLastOutcomeDescription,
                                    IsSPGroupLatest = a.WorkflowLastTaskIsSPGroup,
                                    UserLatest = a.WorkflowLastTaskUser,
                                    EntryTimeLatest = a.WorkflowLastTaskEntryTime,
                                    ExitTimeLatest = a.WorkflowLastTaskExitTime,
                                    #endregion

                                    #region Transaction
                                    ID = a.TransactionID,
                                    ApplicationID = a.ApplicationID,
                                    AccountNumber = a.AccountNumber,
                                    Customer = a.CustomerName,
                                    Product = a.ProductName,
                                    Currency = a.CurrencyCode,
                                    Amount = a.Amount,
                                    AmountUSD = a.AmountUSD,
                                    IsTopUrgent = (a.IsTopUrgent == "Top Urgent") ? true : false,
                                    IsFXTransaction = (a.IsFXTransaction == "Yes") ? true : false,
                                    LastModifiedBy =   a.UpdateBy == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,

                                    ProgressApprover = string.IsNullOrEmpty(a.WorkflowLastDisplayName) ? a.UpdateDate == null ? a.CreateBy : a.UpdateBy : a.WorkflowLastDisplayName,                                    
                                    CurrentTaskUser = a.WorkflowTaskIsSPGroup ? a.WorkflowTaskUser : a.UserDisplayNameTask,
                                    ProgressExitTime = a.WorkflowLastTaskExitTime.HasValue ? a.WorkflowLastTaskExitTime : a.UpdateDate == null ? a.CreateDate != null ? a.CreateDate : DateTime.Now : a.UpdateDate.Value
                                    #endregion
                                }).ToList();

                    userTaskGrid.Page = page.Value;
                    userTaskGrid.Size = size.Value;
                    userTaskGrid.Total = data.Count();

                    userTaskGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };

                    sortColumn = sortColumn.Equals("UserDisplayName") ?
                        sortColumn = "CurrentTaskUser" : sortColumn.Equals("Approver") ?
                        sortColumn = "ProgressApprover" : sortColumn.Equals("ExitTime") ?
                        sortColumn = "ProgressExitTime" : sortColumn;

                    string orderBy = string.Format("{0} {1}", sortColumn, sortOrder);


                    userTaskGrid.Rows = data.AsQueryable().OrderBy(orderBy).Select((a, i) => new SPUserTaskRow()
                    {
                        RowID = i + 1,

                        #region Workflow Context
                        WorkflowContext = new WorkflowContext()
                        {
                            SPWebID = a.SPWebID,
                            SPSiteID = a.SPSiteID,
                            SPListID = a.SPListID,
                            SPListItemID = a.SPListItemID,
                            SPTaskListID = a.SPTaskListID.Value,
                            SPTaskListItemID = a.SPTaskListItemID.Value,
                            Initiator = a.InitiatorDisplayName,
                            ApproverID = a.ApproverID,
                            StartTime = a.StartTime,
                            StateID = a.StateID.Value,
                            StateDescription = a.StateDescription,
                            TransactionStatus = a.TransactionStatus,
                            WorkflowInstanceID = a.WorkflowInstanceID,
                            WorkflowID = a.WorkflowID.Value,
                            WorkflowName = a.WorkflowName,
                            IsAuthorized = a.IsAuthoriaed
                        },
                        #endregion

                        #region Workflow Task
                        CurrentTask = new WorkflowTask()
                        {
                            TaskTypeID = a.TaskType.Value,
                            TaskTypeDescription = a.TaskTypeDescription,
                            Title = a.Title,
                            ActivityTitle = a.ActivityTitle,
                            User = a.CurrentTaskUser,
                            IsSPGroup = a.IsSPGroup,
                            EntryTime = a.EntryTime,
                            ExitTime = a.ExitTime,
                            OutcomeID = a.OutcomeID,
                            OutcomeDescription = a.OutcomeDescription,
                            CustomOutcomeID = a.CustomOutcomeID,
                            CustomOutcomeDescription = a.CustomOutcomeDescription,
                            Comments = a.Comments,
                            Approver = a.ProgressApprover
                        },
                        #endregion

                        #region Workflow Task Progress
                        ProgressTask = new WorkflowTask()
                        {
                            ActivityTitle = a.ActivityTitleLatest,
                            User = a.IsSPGroupLatest.HasValue ? (a.IsSPGroupLatest.Value ? a.UserLatest : a.UserDisplayNameLatest) : a.LastModifiedBy,
                            IsSPGroup = a.IsSPGroupLatest.HasValue ? a.IsSPGroupLatest.Value : false,
                            EntryTime = a.EntryTimeLatest.HasValue ? a.EntryTimeLatest : a.LastModifiedDate,
                            ExitTime = a.ProgressExitTime,
                            OutcomeID = a.OutcomeIDLatest,
                            OutcomeDescription = string.IsNullOrEmpty(a.OutcomeDescriptionLatest) ? "Submit Transaction" : a.OutcomeDescriptionLatest,
                            CustomOutcomeID = a.CustomOutcomeIDLatest,
                            CustomOutcomeDescription = a.CustomOutcomeDescriptionLatest,
                            Comments = a.CommentsLatest,
                            Approver = a.ProgressApprover
                        },
                        #endregion

                        #region Transaction
                        Transaction = new TransactionTaskModel()
                        {
                            ID = a.ID,
                            ApplicationID = a.ApplicationID,
                            Customer = a.Customer,
                            Product = a.Product,
                            Currency = a.Currency,
                            Amount = a.Amount,
                            AmountUSD = a.AmountUSD,
                            IsTopUrgent = a.IsTopUrgent,
                            IsFXTransaction = a.IsFXTransaction,
                            TransactionStatus = a.TransactionStatus,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate,
                            DebitAccNumber = a.AccountNumber
                        }
                        #endregion

                    }).Skip(skip).Take(take).ToList();
                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.ToString();
                }
            }

            return isSuccess;
        }

        public bool GetUserTasksSP(Guid webID, Guid siteID, IList<Guid> workflowIDs, IList<string> state, IList<string> outcome, IList<string> customOutcome, bool showContribute, bool showActiveTask, int? page, int? size, string sortColumn, string sortOrder, IList<SPUserTaskFilter> filters, ref SPUserTaskGrid userTaskGrid, ref string message)
        {
            bool isSuccess = false;

            using (DBSEntities context = new DBSEntities())
            {
                try
                {
                    int skip = (page.Value - 1) * size.Value;
                    int take = size.Value;

                    SPUserTask filter = new SPUserTask();
                    filter.WorkflowContext = new WorkflowContext();
                    filter.CurrentTask = new WorkflowTask();
                    filter.ProgressTask = new WorkflowTask();
                    filter.Transaction = new TransactionTaskModel();
                    filter.Transaction.IsFXTransaction = null;

                    DateTime maxDateEntryTime = new DateTime();
                    DateTime maxDateExitTime = new DateTime();

                    if (filters != null)
                    {
                        #region Transaction
                        filter.Transaction.ApplicationID = filters.Where(sp => sp.Field.Equals("ApplicationID")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.Customer = filters.Where(sp => sp.Field.Equals("Customer")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.Product = filters.Where(sp => sp.Field.Equals("Product")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.Currency = filters.Where(sp => sp.Field.Equals("Currency")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.DebitAccNumber = (string)filters.Where(sp => sp.Field.Equals("DebitAccNumber")).Select(sp => sp.Value).SingleOrDefault();
                        filter.CurrentTask.ActivityTitle = filters.Where(sp => sp.Field.Equals("ActivityTitle")).Select(sp => sp.Value).SingleOrDefault();

                        string amount = filters.Where(sp => sp.Field.Equals("Amount")).Select(sp => sp.Value).SingleOrDefault();
                        if (!string.IsNullOrEmpty(amount))
                        {
                            decimal valueAmount;
                            bool resultAmount = decimal.TryParse(amount, out valueAmount);
                            if (resultAmount)
                            {
                                filter.Transaction.Amount = valueAmount;
                            }
                            else
                            {
                                userTaskGrid.Page = page.Value;
                                userTaskGrid.Size = size.Value;
                                userTaskGrid.Total = 0;
                                return true;
                            }
                        }

                        string amountUSD = filters.Where(sp => sp.Field.Equals("AmountUSD")).Select(sp => sp.Value).SingleOrDefault();
                        if (!string.IsNullOrEmpty(amountUSD))
                        {
                            filter.Transaction.AmountUSD = Decimal.Parse(amountUSD);
                        }

                        string fxTransaction = filters.Where(sp => sp.Field.Equals("IsFXTransaction")).Select(sp => sp.Value).SingleOrDefault();
                        if (!string.IsNullOrEmpty(fxTransaction))
                        {
                            bool valueFx;
                            bool resultFx = bool.TryParse(fxTransaction, out valueFx);
                            if (resultFx)
                            {
                                filter.Transaction.IsFXTransaction = valueFx;//bool.Parse(fxTransaction);
                            }
                            else
                            {
                                userTaskGrid.Page = page.Value;
                                userTaskGrid.Size = size.Value;
                                userTaskGrid.Total = 0;
                                return true;
                            }
                        }

                        string topUrgent = filters.Where(sp => sp.Field.Equals("IsTopUrgent")).Select(sp => sp.Value).SingleOrDefault();
                        if (!string.IsNullOrEmpty(topUrgent))
                        {
                            bool valueFx;
                            bool resultFx = bool.TryParse(topUrgent, out valueFx);
                            if (resultFx)
                            {
                                filter.Transaction.IsTopUrgent = valueFx;
                            }
                            else
                            {
                                userTaskGrid.Page = page.Value;
                                userTaskGrid.Size = size.Value;
                                userTaskGrid.Total = 0;
                                return true;

                            }
                        }

                        filter.CurrentTask.User = filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
                        filter.ProgressTask.User = filters.Where(a => a.Field.Equals("UserApprover")).Select(a => a.Value).SingleOrDefault();

                        if (filters.Where(a => a.Field.Equals("EntryTime")).Any())
                        {
                            filter.CurrentTask.EntryTime = DateTime.Parse((string)filters.Where(a => a.Field.Equals("EntryTime")).Select(a => a.Value).SingleOrDefault());
                            maxDateEntryTime = filter.CurrentTask.EntryTime.Value.AddDays(1);
                        }

                        if (filters.Where(a => a.Field.Equals("ExitTime")).Any())
                        {
                            filter.CurrentTask.ExitTime = DateTime.Parse(filters.Where(a => a.Field.Equals("ExitTime")).Select(a => a.Value).SingleOrDefault());
                            maxDateExitTime = filter.CurrentTask.ExitTime.Value.AddDays(1);
                        }
                        #endregion
                    }
                    string orderBy = string.Format("{0} {1}", sortColumn, sortOrder);
                    string userName = currentUser.GetCurrentUser().LoginName;
                    string[] roles = currentUser.GetCurrentUser().Roles.Select(r => r.Name).ToArray();
                    Guid[] workflowID = workflowIDs.ToArray();
                    int countData = (
                            from a in context.V_TaskApprovals

                            let isActivityTitle = !string.IsNullOrEmpty(filter.CurrentTask.ActivityTitle)
                            let isUser = !string.IsNullOrEmpty(filter.CurrentTask.User)
                            let isEntryTime = filter.CurrentTask.EntryTime.HasValue
                            let isExitTime = filter.CurrentTask.ExitTime.HasValue
                            let isUserApprover = !string.IsNullOrEmpty(filter.ProgressTask.User)

                            #region Transaction
                            let isApplicationID = !string.IsNullOrEmpty(filter.Transaction.ApplicationID)
                            let isCustomer = !string.IsNullOrEmpty(filter.Transaction.Customer)
                            let isProduct = !string.IsNullOrEmpty(filter.Transaction.Product)
                            let isCurrency = !string.IsNullOrEmpty(filter.Transaction.Currency)
                            let isAmount = filter.Transaction.Amount.HasValue
                            let isAmountUSD = filter.Transaction.AmountUSD.HasValue
                            let isFXTransaction = filter.Transaction.IsFXTransaction.HasValue
                            let isTopUrgent = filter.Transaction.IsTopUrgent.HasValue
                            let isDebitAccNumber = !string.IsNullOrEmpty(filter.Transaction.DebitAccNumber)
                            #endregion

                            where

                            a.SPWebID.Equals(webID) && a.SPSiteID.Equals(siteID) && // y
                            workflowIDs.Contains(a.WorkflowID.Value) && // y
                            (showContribute ? roles.Any(x => a.WorkflowTaskUserContribute.Contains(x)) || roles.Any(x => a.InitiatorGroup.Contains(x)) || a.WorkflowInitiator.Equals(userName) : true) && // y
                            (!showContribute && showActiveTask ? (a.WorkflowOutcomeDescription.ToLower().Equals("pending"))  // y
                            && (roles.Contains(a.WorkflowTaskUser.ToLower()) || a.WorkflowTaskUser.Equals(userName)) // y
                            && (a.IsCanceled == false) : true) && // y

                            (state.Count() > 0 ? state.Contains(a.WorkflowStateDescription.ToLower()) : true) && // y
                            (customOutcome.Count() > 0 ? (a.WorkflowOutcomeDescription.ToLower().Equals("custom") ? customOutcome.Contains(a.WorkflowCustomOutcomeDescription) : true) : true) &&
                            (isActivityTitle ? a.WorkflowTaskActivityTitle.Contains(filter.CurrentTask.ActivityTitle) : true) &&
                            (isUser ? (a.WorkflowTaskIsSPGroup ? a.WorkflowTaskUser : a.UserDisplayNameTask).Contains(filter.CurrentTask.User) : true) &&
                            (isUserApprover ? (string.IsNullOrEmpty(a.WorkflowLastDisplayName) ? (a.UpdateDate == null ? a.CreateBy : a.UpdateBy) : a.WorkflowLastDisplayName).Contains(filter.ProgressTask.User) : true) &&
                            (isEntryTime ? (a.WorkflowTaskEntryTime >= filter.CurrentTask.EntryTime.Value && a.WorkflowTaskEntryTime <= maxDateEntryTime) : true) &&
                            (isExitTime ? (a.WorkflowTaskExitTime.Value >= filter.CurrentTask.ExitTime.Value && a.WorkflowTaskExitTime.Value <= maxDateExitTime) : true) &&

                            #region Transaction
 (isApplicationID ? a.ApplicationID.ToLower().Contains(filter.Transaction.ApplicationID) : true) &&
                            (isCustomer ? a.CustomerName.ToLower().Contains(filter.Transaction.Customer.ToLower()) : true) &&
                            (isProduct ? a.ProductName.Contains(filter.Transaction.Product) : true) &&
                            (isCurrency ? a.CurrencyCode.Contains(filter.Transaction.Currency) : true) &&
                            (isAmount ? a.Amount == filter.Transaction.Amount.Value : true) &&
                            (isAmountUSD ? a.AmountUSD == filter.Transaction.AmountUSD.Value : true) &&
                            (isFXTransaction ? (a.CurrencyCode != "IDR" && a.DebitCurrencyCode == "IDR").Equals(filter.Transaction.IsFXTransaction.Value) : true) &&
                            (isTopUrgent ? a.IsTopUrgent.Equals(filter.Transaction.IsTopUrgent.Value) : true) &&
                            (isDebitAccNumber ? a.AccountNumber.Contains(filter.Transaction.DebitAccNumber) : true) &&
                            (isActivityTitle ? a.WorkflowTaskActivityTitle.Contains(filter.CurrentTask.ActivityTitle) : true)
                            #endregion

                            select new { a.TransactionID }).Count();

                    var data = (
                            from a in context.V_TaskApprovals

                            let isActivityTitle = !string.IsNullOrEmpty(filter.CurrentTask.ActivityTitle)
                            let isUser = !string.IsNullOrEmpty(filter.CurrentTask.User)
                            let isEntryTime = filter.CurrentTask.EntryTime.HasValue
                            let isExitTime = filter.CurrentTask.ExitTime.HasValue
                            let isUserApprover = !string.IsNullOrEmpty(filter.ProgressTask.User)

                            #region Transaction
                            let isApplicationID = !string.IsNullOrEmpty(filter.Transaction.ApplicationID)
                            let isCustomer = !string.IsNullOrEmpty(filter.Transaction.Customer)
                            let isProduct = !string.IsNullOrEmpty(filter.Transaction.Product)
                            let isCurrency = !string.IsNullOrEmpty(filter.Transaction.Currency)
                            let isAmount = filter.Transaction.Amount.HasValue
                            let isAmountUSD = filter.Transaction.AmountUSD.HasValue
                            let isFXTransaction = filter.Transaction.IsFXTransaction.HasValue
                            let isTopUrgent = filter.Transaction.IsTopUrgent.HasValue
                            let isDebitAccNumber = !string.IsNullOrEmpty(filter.Transaction.DebitAccNumber)
                            #endregion

                            where

                            a.SPWebID.Equals(webID) && a.SPSiteID.Equals(siteID) && // y
                            workflowIDs.Contains(a.WorkflowID.Value) && // y
                            (showContribute ? roles.Any(x => a.WorkflowTaskUserContribute.Contains(x)) || roles.Any(x => a.InitiatorGroup.Contains(x)) || a.WorkflowInitiator.Equals(userName) : true) && // y
                            (!showContribute && showActiveTask ? (a.WorkflowOutcomeDescription.ToLower().Equals("pending"))  // y
                            && (roles.Contains(a.WorkflowTaskUser.ToLower()) || a.WorkflowTaskUser.Equals(userName)) // y
                            && (a.IsCanceled == false) : true) && // y

                            (state.Count() > 0 ? state.Contains(a.WorkflowStateDescription.ToLower()) : true) && // y
                            (customOutcome.Count() > 0 ? (a.WorkflowOutcomeDescription.ToLower().Equals("custom") ? customOutcome.Contains(a.WorkflowCustomOutcomeDescription) : true) : true) &&
                            (isActivityTitle ? a.WorkflowTaskActivityTitle.Contains(filter.CurrentTask.ActivityTitle) : true) &&
                            (isUser ? (a.WorkflowTaskIsSPGroup ? a.WorkflowTaskUser : a.UserDisplayNameTask).Contains(filter.CurrentTask.User) : true) &&
                            (isUserApprover ? (string.IsNullOrEmpty(a.WorkflowLastDisplayName) ? (a.UpdateDate == null ? a.CreateBy : a.UpdateBy) : a.WorkflowLastDisplayName).Contains(filter.ProgressTask.User) : true) &&
                            (isEntryTime ? (a.WorkflowTaskEntryTime >= filter.CurrentTask.EntryTime.Value && a.WorkflowTaskEntryTime <= maxDateEntryTime) : true) &&
                            (isExitTime ? (a.WorkflowTaskExitTime.Value >= filter.CurrentTask.ExitTime.Value && a.WorkflowTaskExitTime.Value <= maxDateExitTime) : true) &&

                            #region Transaction
                            (isApplicationID ? a.ApplicationID.ToLower().Contains(filter.Transaction.ApplicationID) : true) &&
                            (isCustomer ? a.CustomerName.ToLower().Contains(filter.Transaction.Customer.ToLower()) : true) &&
                            (isProduct ? a.ProductName.Contains(filter.Transaction.Product) : true) &&
                            (isCurrency ? a.CurrencyCode.Contains(filter.Transaction.Currency) : true) &&
                            (isAmount ? a.Amount == filter.Transaction.Amount.Value : true) &&
                            (isAmountUSD ? a.AmountUSD == filter.Transaction.AmountUSD.Value : true) &&
                            (isFXTransaction ? (a.CurrencyCode != "IDR" && a.DebitCurrencyCode == "IDR").Equals(filter.Transaction.IsFXTransaction.Value) : true) &&
                            (isTopUrgent ? a.IsTopUrgent.Equals(filter.Transaction.IsTopUrgent.Value) : true) &&
                            (isDebitAccNumber ? a.AccountNumber.Contains(filter.Transaction.DebitAccNumber) : true) &&
                            (isActivityTitle ? a.WorkflowTaskActivityTitle.Contains(filter.CurrentTask.ActivityTitle) : true)
                            #endregion

                            select new
                            {
                                #region Workflow Context
                                SPWebID = a.SPWebID,
                                SPSiteID = a.SPSiteID,
                                SPListID = a.SPListID,
                                SPListItemID = a.SPListItemID,
                                SPTaskListID = a.SPTaskListID,
                                SPTaskListItemID = a.SPTaskListItemID,
                                ApproverID = a.WorkflowApproverID,
                                Initiator = a.WorkflowInitiator,
                                InitiatorDisplayName = a.UserDisplayNameInitiator,
                                StartTime = a.WorkflowStartTime,
                                StateID = a.WorkflowState,
                                StateDescription = a.WorkflowStateDescription,
                                TransactionStatus = a.TransactionStatus,
                                //StateDescription = a.WorkflowStateDescription.ToLower() == "running" ? a.WorkflowTaskActivityTitle : a.WorkflowStateDescription.ToLower() == "complete" ? ((a.ApplicationID.ToLower().StartsWith("fx") && a.WorkflowOutcomeDescription.ToLower() == "rejected") || a.WorkflowCustomOutcomeDescription.ToLower() == "approve cancellation" ? "Cancelled"  : "Completed") : a.WorkflowStateDescription.Trim(),
                                WorkflowID = a.WorkflowID,
                                WorkflowName = a.WorkflowName,
                                WorkflowInstanceID = a.WorkflowInstanceID,
                                IsAuthoriaed = a.WorkflowTaskIsSPGroup ? (roles.Contains(a.WorkflowTaskUser) ? true : false) : (a.WorkflowTaskUser.Equals(userName) ? true : false),
                                #endregion

                                #region Workflow Task
                                TaskType = a.WorkflowTaskType,
                                TaskTypeDescription = a.WorkflowTaskTypeDescription,
                                Title = a.WorkflowTaskTitle,
                                ActivityTitle = a.WorkflowTaskActivityTitle,
                                User = a.WorkflowTaskUser,
                                UserDisplayName = a.UserDisplayNameTask,
                                IsSPGroup = a.WorkflowTaskIsSPGroup,
                                EntryTime = a.WorkflowTaskEntryTime,
                                ExitTime = a.WorkflowTaskExitTime != null ? a.WorkflowTaskExitTime : DateTime.Now,
                                OutcomeID = a.WorkflowOutcome,
                                OutcomeDescription = a.WorkflowOutcomeDescription,
                                CustomOutcomeID = a.WorkflowCustomOutcome,
                                CustomOutcomeDescription = a.WorkflowCustomOutcomeDescription != null ? a.WorkflowCustomOutcomeDescription : "",
                                Comments = a.WorkflowComments,
                                Approver = a.WorkflowDisplayName != null ? a.WorkflowDisplayName : "",
                                #endregion

                                #region Workflow Task Latest
                                ActivityTitleLatest = a.WorkflowLastActivityTitle,
                                CommentsLatest = a.WorkflowLastComments,
                                CustomOutcomeIDLatest = a.WorkflowLastCustomOutcome != null ? a.WorkflowLastCustomOutcome : 0,
                                CustomOutcomeDescriptionLatest = a.WorkflowLastCustomOutcomeDescription != null ? a.WorkflowLastCustomOutcomeDescription : "",
                                UserDisplayNameLatest = a.WorkflowLastDisplayName,
                                OutcomeIDLatest = a.WorkflowLastOutcome,
                                OutcomeDescriptionLatest = a.WorkflowLastOutcomeDescription,
                                IsSPGroupLatest = a.WorkflowLastTaskIsSPGroup,
                                UserLatest = a.WorkflowLastTaskUser,
                                EntryTimeLatest = a.WorkflowLastTaskEntryTime,
                                ExitTimeLatest = a.WorkflowLastTaskExitTime,
                                #endregion

                                #region Transaction
                                ID = a.TransactionID,
                                ApplicationID = a.ApplicationID,
                                AccountNumber = a.AccountNumber,
                                Customer = a.CustomerName,
                                Product = a.ProductName,
                                Currency = a.CurrencyCode,
                                Amount = a.Amount,
                                AmountUSD = a.AmountUSD,
                                IsTopUrgent = a.IsTopUrgent,
                                IsFXTransaction = (a.CurrencyCode != "IDR" && a.DebitCurrencyCode == "IDR") ? true : false,
                                LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,

                                ProgressApprover = string.IsNullOrEmpty(a.WorkflowLastDisplayName) ? a.UpdateDate == null ? a.CreateBy : a.UpdateBy : a.WorkflowLastDisplayName,
                                CurrentTaskUser = a.WorkflowTaskIsSPGroup ? a.WorkflowTaskUser : a.UserDisplayNameTask,
                                ProgressExitTime = a.WorkflowLastTaskExitTime.HasValue ? a.WorkflowLastTaskExitTime : a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                #endregion
                            
                            }).OrderBy(orderBy).Skip(skip).Take(take);

                    userTaskGrid.Page = page.Value;
                    userTaskGrid.Size = size.Value;
                    userTaskGrid.Total = countData;
                    int StartRow = (page.Value * size.Value) - size.Value + 1;
                    userTaskGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };

                    sortColumn = sortColumn.Equals("UserDisplayName") ?
                        sortColumn = "CurrentTaskUser" : sortColumn.Equals("Approver") ?
                        sortColumn = "ProgressApprover" : sortColumn.Equals("ExitTime") ?
                        sortColumn = "ProgressExitTime" : sortColumn;

                  

                    //userTaskGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                    userTaskGrid.Rows = data.AsEnumerable()
                    .Select((a, i) => new SPUserTaskRow()
                    {
                        RowID = StartRow + i,

                        #region Workflow Context
                        WorkflowContext = new WorkflowContext()
                        {
                            SPWebID = a.SPWebID,
                            SPSiteID = a.SPSiteID,
                            SPListID = a.SPListID,
                            SPListItemID = a.SPListItemID,
                            SPTaskListID = a.SPTaskListID.Value,
                            SPTaskListItemID = a.SPTaskListItemID.Value,
                            Initiator = a.InitiatorDisplayName,
                            ApproverID = a.ApproverID,
                            StartTime = a.StartTime,
                            StateID = a.StateID.Value,
                            StateDescription = a.StateDescription,
                            WorkflowInstanceID = a.WorkflowInstanceID,
                            WorkflowID = a.WorkflowID.Value,
                            WorkflowName = a.WorkflowName,
                            IsAuthorized = a.IsAuthoriaed
                        },
                        #endregion

                        #region Workflow Task
                        CurrentTask = new WorkflowTask()
                        {
                            TaskTypeID = a.TaskType.Value,
                            TaskTypeDescription = a.TaskTypeDescription,
                            Title = a.Title,
                            ActivityTitle = a.ActivityTitle,
                            User = a.CurrentTaskUser,
                            IsSPGroup = a.IsSPGroup,
                            EntryTime = a.EntryTime,
                            ExitTime = a.ExitTime,
                            OutcomeID = a.OutcomeID,
                            OutcomeDescription = a.OutcomeDescription,
                            CustomOutcomeID = a.CustomOutcomeID,
                            CustomOutcomeDescription = a.CustomOutcomeDescription,
                            Comments = a.Comments,
                            Approver = a.ProgressApprover
                        },
                        #endregion

                        #region Workflow Task Progress
                        ProgressTask = new WorkflowTask()
                        {
                            ActivityTitle = a.ActivityTitleLatest,
                            User = a.IsSPGroupLatest.HasValue ? (a.IsSPGroupLatest.Value ? a.UserLatest : a.UserDisplayNameLatest) : a.LastModifiedBy,
                            IsSPGroup = a.IsSPGroupLatest.HasValue ? a.IsSPGroupLatest.Value : false,
                            EntryTime = a.EntryTimeLatest.HasValue ? a.EntryTimeLatest : a.LastModifiedDate,
                            ExitTime = a.ProgressExitTime,
                            OutcomeID = a.OutcomeIDLatest,
                            OutcomeDescription = string.IsNullOrEmpty(a.OutcomeDescriptionLatest) ? "Submit Transaction" : a.OutcomeDescriptionLatest,
                            CustomOutcomeID = a.CustomOutcomeIDLatest,
                            CustomOutcomeDescription = a.CustomOutcomeDescriptionLatest,
                            Comments = a.CommentsLatest,
                            Approver = a.ProgressApprover
                        },
                        #endregion

                        #region Transaction
                        Transaction = new TransactionTaskModel()
                        {
                            ID = a.ID,
                            ApplicationID = a.ApplicationID,
                            Customer = a.Customer,
                            Product = a.Product,
                            Currency = a.Currency,
                            Amount = a.Amount,
                            AmountUSD = a.AmountUSD,
                            IsTopUrgent = a.IsTopUrgent,
                            IsFXTransaction = a.IsFXTransaction,
                            TransactionStatus = a.TransactionStatus,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate,
                            DebitAccNumber = a.AccountNumber
                        }
                        #endregion

                     }).ToList();
                    //}).Skip(skip).Take(take).ToList();


                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.InnerException.Message;
                }
            }

            return isSuccess;
        }
        */

        #endregion

        #region Old Code - Rizki 2016-01-06

        //public bool GetHomeUserTasks(Guid webID, Guid siteID, IList<Guid> workflowIDs, IList<string> state, IList<string> outcome, IList<string> customOutcome, bool showContribute, bool showActiveTask, int? page, int? size, string sortColumn, string sortOrder, IList<SPUserTaskFilter> filters, ref SPUserTaskGrid userTaskGrid, ref string message)
        //{
        //    bool isSuccess = false;

        //    using (DBSEntities context = new DBSEntities())
        //    {
        //        try
        //        {
        //            int skip = (page.Value - 1) * size.Value;
        //            int take = size.Value;

        //            SPUserTask filter = new SPUserTask();
        //            filter.WorkflowContext = new WorkflowContext();
        //            filter.CurrentTask = new WorkflowTask();
        //            filter.ProgressTask = new WorkflowTask();
        //            filter.Transaction = new TransactionTaskModel();
        //            filter.Transaction.IsFXTransaction = null;

        //            DateTime maxDateEntryTime = new DateTime();
        //            DateTime maxDateExitTime = new DateTime();

        //            if (filters != null)
        //            {
        //                #region Transaction
        //                filter.Transaction.ApplicationID = filters.Where(sp => sp.Field.Equals("ApplicationID")).Select(sp => sp.Value).SingleOrDefault();
        //                filter.Transaction.Customer = filters.Where(sp => sp.Field.Equals("Customer")).Select(sp => sp.Value).SingleOrDefault();
        //                filter.Transaction.Product = filters.Where(sp => sp.Field.Equals("Product")).Select(sp => sp.Value).SingleOrDefault();
        //                filter.Transaction.Currency = filters.Where(sp => sp.Field.Equals("Currency")).Select(sp => sp.Value).SingleOrDefault();
        //                filter.Transaction.DebitAccNumber = (string)filters.Where(sp => sp.Field.Equals("DebitAccNumber")).Select(sp => sp.Value).SingleOrDefault();
        //                filter.CurrentTask.ActivityTitle = filters.Where(sp => sp.Field.Equals("ActivityTitle")).Select(sp => sp.Value).SingleOrDefault();
        //                filter.WorkflowContext.TransactionStatus = filters.Where(sp => sp.Field.Equals("TransactionStatus")).Select(sp => sp.Value).SingleOrDefault();
        //                filter.ProgressTask.Approver = filters.Where(sp => sp.Field.Equals("UserApprover")).Select(sp => sp.Value).SingleOrDefault();

        //                string amount = filters.Where(sp => sp.Field.Equals("Amount")).Select(sp => sp.Value).SingleOrDefault();
        //                if (!string.IsNullOrEmpty(amount))
        //                {
        //                    decimal valueAmount;
        //                    bool resultAmount = decimal.TryParse(amount, out valueAmount);
        //                    if (resultAmount)
        //                    {
        //                        filter.Transaction.Amount = valueAmount;
        //                    }
        //                    else
        //                    {
        //                        userTaskGrid.Page = page.Value;
        //                        userTaskGrid.Size = size.Value;
        //                        userTaskGrid.Total = 0;
        //                        return true;
        //                    }
        //                }

        //                string amountUSD = filters.Where(sp => sp.Field.Equals("AmountUSD")).Select(sp => sp.Value).SingleOrDefault();
        //                if (!string.IsNullOrEmpty(amountUSD))
        //                {
        //                    filter.Transaction.AmountUSD = Decimal.Parse(amountUSD);
        //                }

        //                filter.Transaction.IsFXTransactionValue = filters.Where(sp => sp.Field.Equals("IsFXTransaction")).Select(sp => sp.Value).SingleOrDefault();
        //                filter.Transaction.IsTopUrgentValue = filters.Where(sp => sp.Field.Equals("IsTopUrgent")).Select(sp => sp.Value).SingleOrDefault();

        //                filter.CurrentTask.User = filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
        //                filter.ProgressTask.User = filters.Where(a => a.Field.Equals("UserApprover")).Select(a => a.Value).SingleOrDefault();

        //                if (filters.Where(a => a.Field.Equals("EntryTime")).Any())
        //                {
        //                    filter.CurrentTask.EntryTime = DateTime.Parse((string)filters.Where(a => a.Field.Equals("EntryTime")).Select(a => a.Value).SingleOrDefault());
        //                    maxDateEntryTime = filter.CurrentTask.EntryTime.Value.AddDays(1);
        //                }

        //                if (filters.Where(a => a.Field.Equals("ExitTime")).Any())
        //                {
        //                    filter.CurrentTask.ExitTime = DateTime.Parse(filters.Where(a => a.Field.Equals("ExitTime")).Select(a => a.Value).SingleOrDefault());
        //                    maxDateExitTime = filter.CurrentTask.ExitTime.Value.AddDays(1);
        //                }

        //                #endregion
        //            }

        //            string userName = currentUser.GetCurrentUser().LoginName;
        //            string[] roles = currentUser.GetCurrentUser().Roles.Select(r => r.Name).ToArray();
        //            Guid[] workflowID = workflowIDs.ToArray();

        //            #region parameter
        //            string Username = currentUser.GetCurrentUser().LoginName;
        //            string ApplicationID = filter.Transaction.ApplicationID;
        //            string CustomerName = filter.Transaction.Customer;
        //            string ProductName = filter.Transaction.Product;
        //            string CurrencyCode = filter.Transaction.Currency;
        //            Decimal? TransactionAmount = filter.Transaction.Amount;
        //            string DebitAccNumber = filter.Transaction.DebitAccNumber;
        //            string IsFXTransaction = filter.Transaction.IsFXTransactionValue;
        //            string Urgency = filter.Transaction.IsTopUrgentValue;
        //            string ModifiedBy = filter.ProgressTask.Approver;
        //            #endregion

        //            var data = (from a in context.SP_GetTaskUser(Username,
        //                                                        ApplicationID,
        //                                                        CustomerName,
        //                                                        ProductName,
        //                                                        CurrencyCode,
        //                                                        TransactionAmount,
        //                                                        DebitAccNumber,
        //                                                        IsFXTransaction,
        //                                                        Urgency,
        //                                                        null,
        //                                                        ModifiedBy,
        //                                                        null)
        //                        let TransactionStatus = a.TransactionStatus
        //                        let ExitTime = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
        //                        let isTransactionStatus = !string.IsNullOrEmpty(filter.WorkflowContext.TransactionStatus)
        //                        let isExitTime = filter.CurrentTask.EntryTime.HasValue

        //                        where
        //                            (isTransactionStatus ? a.TransactionStatus.ToLower().Contains(filter.WorkflowContext.TransactionStatus.ToLower()) : true) &&
        //                            (isExitTime ? (a.WorkflowTaskEntryTime >= filter.CurrentTask.EntryTime.Value && a.WorkflowTaskEntryTime <= maxDateEntryTime) : true)
        //                        select new
        //                        {
        //                            #region Workflow Context
        //                            SPWebID = a.SPWebID,
        //                            SPSiteID = a.SPSiteID,
        //                            SPListID = a.SPListID,
        //                            SPListItemID = a.SPListItemID,
        //                            SPTaskListID = a.SPTaskListID,
        //                            SPTaskListItemID = a.SPTaskListItemID,
        //                            ApproverID = a.WorkflowApproverID,
        //                            Initiator = a.WorkflowInitiator,
        //                            InitiatorDisplayName = a.UserDisplayNameInitiator,
        //                            StartTime = a.WorkflowStartTime,
        //                            StateID = a.WorkflowState,
        //                            StateDescription = a.WorkflowStateDescription,
        //                            TransactionStatus = TransactionStatus,
        //                            WorkflowID = a.WorkflowID,
        //                            WorkflowName = a.WorkflowName,
        //                            WorkflowInstanceID = a.WorkflowInstanceID,
        //                            IsAuthoriaed = a.WorkflowTaskIsSPGroup ? (roles.Contains(a.WorkflowTaskUser) ? true : false) : (a.WorkflowTaskUser.Equals(userName) ? true : false),
        //                            #endregion

        //                            #region Workflow Task
        //                            TaskType = a.WorkflowTaskType,
        //                            TaskTypeDescription = a.WorkflowTaskTypeDescription,
        //                            Title = a.WorkflowTaskTitle,
        //                            ActivityTitle = a.WorkflowTaskActivityTitle,
        //                            User = a.WorkflowTaskUser,
        //                            UserDisplayName = (a.UserDisplayNameTask != null) ? a.UserDisplayNameTask : "",
        //                            IsSPGroup = a.WorkflowTaskIsSPGroup,
        //                            EntryTime = a.WorkflowTaskEntryTime,
        //                            ExitTime = ExitTime, //a.WorkflowTaskExitTime,
        //                            OutcomeID = a.WorkflowOutcome,
        //                            OutcomeDescription = a.WorkflowOutcomeDescription,
        //                            CustomOutcomeID = a.WorkflowCustomOutcome,
        //                            CustomOutcomeDescription = a.WorkflowCustomOutcomeDescription,
        //                            Comments = a.WorkflowComments,
        //                            Approver = a.WorkflowDisplayName,
        //                            #endregion

        //                            #region Workflow Task Latest
        //                            ActivityTitleLatest = a.WorkflowLastActivityTitle,
        //                            CommentsLatest = a.WorkflowLastComments,
        //                            CustomOutcomeIDLatest = a.WorkflowLastCustomOutcome,
        //                            CustomOutcomeDescriptionLatest = a.WorkflowLastCustomOutcomeDescription,
        //                            UserDisplayNameLatest = a.WorkflowLastDisplayName,
        //                            OutcomeIDLatest = a.WorkflowLastOutcome,
        //                            OutcomeDescriptionLatest = a.WorkflowLastOutcomeDescription,
        //                            IsSPGroupLatest = a.WorkflowLastTaskIsSPGroup,
        //                            UserLatest = a.WorkflowLastTaskUser,
        //                            EntryTimeLatest = a.WorkflowLastTaskEntryTime,
        //                            ExitTimeLatest = a.WorkflowLastTaskExitTime,
        //                            #endregion

        //                            #region Transaction
        //                            ID = a.TransactionID,
        //                            ApplicationID = a.ApplicationID,
        //                            AccountNumber = a.AccountNumber,
        //                            Customer = a.CustomerName,
        //                            Product = a.ProductName,
        //                            Currency = a.CurrencyCode,
        //                            Amount = a.Amount,
        //                            AmountUSD = a.AmountUSD,
        //                            IsTopUrgent = a.IsTopUrgent == "Top Urgent Chain" ? 2 : a.IsTopUrgent == "Top Urgent" ? 1 : 0,
        //                            IsFXTransaction = (a.IsFXTransaction == "Yes") ? true : false,
        //                            LastModifiedBy = a.UpdateBy == null ? a.CreateBy : a.UpdateBy,
        //                            LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,

        //                            ProgressApprover = string.IsNullOrEmpty(a.WorkflowLastDisplayName) ? a.UpdateDate == null ? a.CreateBy : a.UpdateBy : a.WorkflowLastDisplayName,
        //                            CurrentTaskUser = a.WorkflowTaskIsSPGroup ? a.WorkflowTaskUser : a.UserDisplayNameTask,
        //                            ProgressExitTime = a.WorkflowLastTaskExitTime.HasValue ? a.WorkflowLastTaskExitTime : a.UpdateDate == null ? a.CreateDate != null ? a.CreateDate : DateTime.Now : a.UpdateDate.Value
        //                            ,
        //                            ProductID = a.ProductID
        //                            #endregion
        //                        }).ToList();

        //            userTaskGrid.Page = page.Value;
        //            userTaskGrid.Size = size.Value;
        //            userTaskGrid.Total = data.Count();

        //            userTaskGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };

        //            sortColumn = sortColumn.Equals("UserDisplayName") ?
        //                sortColumn = "CurrentTaskUser" : sortColumn.Equals("Approver") ?
        //                sortColumn = "ProgressApprover" : sortColumn.Equals("ExitTime") ?
        //                sortColumn = "ProgressExitTime" : sortColumn;

        //            string orderBy = string.Format("IsTopUrgent DESC,{0} {1}", sortColumn, sortOrder);


        //            // userTaskGrid.Rows = data.AsQueryable().OrderBy(orderBy).Select((a, i) => new SPUserTaskRow()
        //            userTaskGrid.Rows = data.AsQueryable().OrderBy(orderBy).Select((a, i) => new SPUserTaskRow()
        //            {
        //                RowID = i + 1,

        //                #region Workflow Context
        //                WorkflowContext = new WorkflowContext()
        //                {
        //                    SPWebID = a.SPWebID,
        //                    SPSiteID = a.SPSiteID,
        //                    SPListID = a.SPListID,
        //                    SPListItemID = a.SPListItemID,
        //                    SPTaskListID = a.SPTaskListID.Value,
        //                    SPTaskListItemID = a.SPTaskListItemID.Value,
        //                    Initiator = a.Initiator, //Add by Fandi
        //                    ApproverID = a.ApproverID,
        //                    StartTime = a.StartTime,
        //                    StateID = a.StateID.Value,
        //                    StateDescription = a.StateDescription,
        //                    TransactionStatus = a.TransactionStatus,
        //                    WorkflowInstanceID = a.WorkflowInstanceID,
        //                    WorkflowID = a.WorkflowID.Value,
        //                    WorkflowName = a.WorkflowName,
        //                    IsAuthorized = a.IsAuthoriaed
        //                },
        //                #endregion

        //                #region Workflow Task
        //                CurrentTask = new WorkflowTask()
        //                {
        //                    TaskTypeID = a.TaskType.Value,
        //                    TaskTypeDescription = a.TaskTypeDescription,
        //                    Title = a.Title,
        //                    ActivityTitle = a.ActivityTitle,
        //                    User = a.CurrentTaskUser,
        //                    IsSPGroup = a.IsSPGroup,
        //                    EntryTime = a.EntryTime,
        //                    ExitTime = a.ExitTime,
        //                    OutcomeID = a.OutcomeID,
        //                    OutcomeDescription = a.OutcomeDescription,
        //                    CustomOutcomeID = a.CustomOutcomeID,
        //                    CustomOutcomeDescription = a.CustomOutcomeDescription,
        //                    Comments = a.Comments,
        //                    Approver = a.ProgressApprover
        //                },
        //                #endregion

        //                #region Workflow Task Progress
        //                ProgressTask = new WorkflowTask()
        //                {
        //                    ActivityTitle = a.ActivityTitleLatest,
        //                    User = a.IsSPGroupLatest.HasValue ? (a.IsSPGroupLatest.Value ? a.UserLatest : a.UserDisplayNameLatest) : a.LastModifiedBy,
        //                    IsSPGroup = a.IsSPGroupLatest.HasValue ? a.IsSPGroupLatest.Value : false,
        //                    EntryTime = a.EntryTimeLatest.HasValue ? a.EntryTimeLatest : a.LastModifiedDate,
        //                    ExitTime = a.ProgressExitTime,
        //                    OutcomeID = a.OutcomeIDLatest,
        //                    OutcomeDescription = string.IsNullOrEmpty(a.OutcomeDescriptionLatest) ? "Submit Transaction" : a.OutcomeDescriptionLatest,
        //                    CustomOutcomeID = a.CustomOutcomeIDLatest,
        //                    CustomOutcomeDescription = a.CustomOutcomeDescriptionLatest,
        //                    Comments = a.CommentsLatest,
        //                    Approver = a.ProgressApprover
        //                },
        //                #endregion

        //                #region Transaction
        //                Transaction = new TransactionTaskModel()
        //                {
        //                    ID = a.ID,
        //                    ApplicationID = a.ApplicationID,
        //                    Customer = a.Customer,
        //                    Product = a.Product,
        //                    Currency = a.Currency,
        //                    Amount = a.Amount,
        //                    AmountUSD = a.AmountUSD,
        //                    IsTopUrgent = a.IsTopUrgent,
        //                    IsFXTransaction = a.IsFXTransaction,
        //                    TransactionStatus = a.TransactionStatus,
        //                    LastModifiedBy = a.LastModifiedBy,
        //                    LastModifiedDate = a.LastModifiedDate,
        //                    DebitAccNumber = a.AccountNumber,
        //                    ProductID = (int)a.ProductID
        //                }
        //                #endregion

        //            }).Skip(skip).Take(take).ToList();
        //            isSuccess = true;
        //        }
        //        catch (Exception ex)
        //        {
        //            message = ex.ToString();
        //        }
        //    }

        //    return isSuccess;
        //}

        //public bool GetCanceledUserTasks(Guid webID, Guid siteID, IList<Guid> workflowIDs, IList<string> state, IList<string> outcome, IList<string> customOutcome, bool showContribute, bool showActiveTask, int? page, int? size, string sortColumn, string sortOrder, IList<SPUserTaskFilter> filters, ref SPUserTaskGrid userTaskGrid, ref string message)
        //{
        //    bool isSuccess = false;

        //    using (DBSEntities context = new DBSEntities())
        //    {
        //        try
        //        {
        //            int skip = (page.Value - 1) * size.Value;
        //            int take = size.Value;

        //            SPUserTask filter = new SPUserTask();
        //            filter.WorkflowContext = new WorkflowContext();
        //            filter.CurrentTask = new WorkflowTask();
        //            filter.ProgressTask = new WorkflowTask();
        //            filter.Transaction = new TransactionTaskModel();
        //            filter.Transaction.IsFXTransaction = null;

        //            DateTime maxDateEntryTime = new DateTime();
        //            DateTime maxDateExitTime = new DateTime();

        //            if (filters != null)
        //            {
        //                #region Transaction
        //                filter.Transaction.ApplicationID = filters.Where(sp => sp.Field.Equals("ApplicationID")).Select(sp => sp.Value).SingleOrDefault();
        //                filter.Transaction.Customer = filters.Where(sp => sp.Field.Equals("Customer")).Select(sp => sp.Value).SingleOrDefault();
        //                filter.Transaction.Product = filters.Where(sp => sp.Field.Equals("Product")).Select(sp => sp.Value).SingleOrDefault();
        //                filter.Transaction.Currency = filters.Where(sp => sp.Field.Equals("Currency")).Select(sp => sp.Value).SingleOrDefault();
        //                filter.Transaction.DebitAccNumber = (string)filters.Where(sp => sp.Field.Equals("DebitAccNumber")).Select(sp => sp.Value).SingleOrDefault();
        //                filter.CurrentTask.ActivityTitle = filters.Where(sp => sp.Field.Equals("ActivityTitle")).Select(sp => sp.Value).SingleOrDefault();
        //                filter.WorkflowContext.TransactionStatus = filters.Where(sp => sp.Field.Equals("TransactionStatus")).Select(sp => sp.Value).SingleOrDefault();
        //                filter.ProgressTask.Approver = filters.Where(sp => sp.Field.Equals("UserApprover")).Select(sp => sp.Value).SingleOrDefault();

        //                string amount = filters.Where(sp => sp.Field.Equals("Amount")).Select(sp => sp.Value).SingleOrDefault();
        //                if (!string.IsNullOrEmpty(amount))
        //                {
        //                    decimal valueAmount;
        //                    bool resultAmount = decimal.TryParse(amount, out valueAmount);
        //                    if (resultAmount)
        //                    {
        //                        filter.Transaction.Amount = valueAmount;
        //                    }
        //                    else
        //                    {
        //                        userTaskGrid.Page = page.Value;
        //                        userTaskGrid.Size = size.Value;
        //                        userTaskGrid.Total = 0;
        //                        return true;
        //                    }
        //                }

        //                string amountUSD = filters.Where(sp => sp.Field.Equals("AmountUSD")).Select(sp => sp.Value).SingleOrDefault();
        //                if (!string.IsNullOrEmpty(amountUSD))
        //                {
        //                    filter.Transaction.AmountUSD = Decimal.Parse(amountUSD);
        //                }

        //                filter.Transaction.IsFXTransactionValue = filters.Where(sp => sp.Field.Equals("IsFXTransaction")).Select(sp => sp.Value).SingleOrDefault();
        //                filter.Transaction.IsTopUrgentValue = filters.Where(sp => sp.Field.Equals("IsTopUrgent")).Select(sp => sp.Value).SingleOrDefault();

        //                filter.CurrentTask.User = filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
        //                filter.ProgressTask.User = filters.Where(a => a.Field.Equals("UserApprover")).Select(a => a.Value).SingleOrDefault();

        //                if (filters.Where(a => a.Field.Equals("EntryTime")).Any())
        //                {
        //                    filter.CurrentTask.EntryTime = DateTime.Parse((string)filters.Where(a => a.Field.Equals("EntryTime")).Select(a => a.Value).SingleOrDefault());
        //                    maxDateEntryTime = filter.CurrentTask.EntryTime.Value.AddDays(1);
        //                }

        //                if (filters.Where(a => a.Field.Equals("ExitTime")).Any())
        //                {
        //                    filter.CurrentTask.ExitTime = DateTime.Parse(filters.Where(a => a.Field.Equals("ExitTime")).Select(a => a.Value).SingleOrDefault());
        //                    maxDateExitTime = filter.CurrentTask.ExitTime.Value.AddDays(1);
        //                }

        //                #endregion
        //            }

        //            string userName = currentUser.GetCurrentUser().LoginName;
        //            string[] roles = currentUser.GetCurrentUser().Roles.Select(r => r.Name).ToArray();
        //            Guid[] workflowID = workflowIDs.ToArray();

        //            #region parameter
        //            string Username = currentUser.GetCurrentUser().LoginName;
        //            string ApplicationID = filter.Transaction.ApplicationID;
        //            string CustomerName = filter.Transaction.Customer;
        //            string ProductName = filter.Transaction.Product;
        //            string CurrencyCode = filter.Transaction.Currency;
        //            Decimal? TransactionAmount = filter.Transaction.Amount;
        //            string DebitAccNumber = filter.Transaction.DebitAccNumber;
        //            string IsFXTransaction = filter.Transaction.IsFXTransactionValue;
        //            string Urgency = filter.Transaction.IsTopUrgentValue;
        //            string ModifiedBy = filter.ProgressTask.Approver;
        //            #endregion

        //            var data = (from a in context.SP_GetTaskUser(Username,
        //                                                        ApplicationID,
        //                                                        CustomerName,
        //                                                        ProductName,
        //                                                        CurrencyCode,
        //                                                        TransactionAmount,
        //                                                        DebitAccNumber,
        //                                                        IsFXTransaction,
        //                                                        Urgency,
        //                                                        null,
        //                                                        ModifiedBy,
        //                                                        null)
        //                        let TransactionStatus = a.TransactionStatus
        //                        let ExitTime = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
        //                        let isTransactionStatus = !string.IsNullOrEmpty(filter.WorkflowContext.TransactionStatus)
        //                        let isExitTime = filter.CurrentTask.EntryTime.HasValue

        //                        where
        //                            (isTransactionStatus ? a.TransactionStatus.ToLower().Contains(filter.WorkflowContext.TransactionStatus.ToLower()) : true) &&
        //                            (isExitTime ? (a.WorkflowTaskEntryTime >= filter.CurrentTask.EntryTime.Value && a.WorkflowTaskEntryTime <= maxDateEntryTime) : true)
        //                        select new
        //                        {
        //                            #region Workflow Context
        //                            SPWebID = a.SPWebID,
        //                            SPSiteID = a.SPSiteID,
        //                            SPListID = a.SPListID,
        //                            SPListItemID = a.SPListItemID,
        //                            SPTaskListID = a.SPTaskListID,
        //                            SPTaskListItemID = a.SPTaskListItemID,
        //                            ApproverID = a.WorkflowApproverID,
        //                            Initiator = a.WorkflowInitiator,
        //                            InitiatorDisplayName = a.UserDisplayNameInitiator,
        //                            StartTime = a.WorkflowStartTime,
        //                            StateID = a.WorkflowState,
        //                            StateDescription = a.WorkflowStateDescription,
        //                            TransactionStatus = TransactionStatus,
        //                            //StateDescription = a.WorkflowStateDescription.ToLower() == "running" ? a.WorkflowTaskActivityTitle : a.WorkflowStateDescription.ToLower() == "complete" ? ((a.ApplicationID.ToLower().StartsWith("fx") && a.WorkflowOutcomeDescription.ToLower() == "rejected") || a.WorkflowCustomOutcomeDescription.ToLower() == "approve cancellation" ? "Cancelled"  : "Completed") : a.WorkflowStateDescription.Trim(),
        //                            WorkflowID = a.WorkflowID,
        //                            WorkflowName = a.WorkflowName,
        //                            WorkflowInstanceID = a.WorkflowInstanceID,
        //                            IsAuthoriaed = a.WorkflowTaskIsSPGroup ? (roles.Contains(a.WorkflowTaskUser) ? true : false) : (a.WorkflowTaskUser.Equals(userName) ? true : false),
        //                            #endregion

        //                            #region Workflow Task
        //                            TaskType = a.WorkflowTaskType,
        //                            TaskTypeDescription = a.WorkflowTaskTypeDescription,
        //                            Title = a.WorkflowTaskTitle,
        //                            ActivityTitle = a.WorkflowTaskActivityTitle,
        //                            User = a.WorkflowTaskUser,
        //                            UserDisplayName = (a.UserDisplayNameTask != null) ? a.UserDisplayNameTask : "",
        //                            IsSPGroup = a.WorkflowTaskIsSPGroup,
        //                            EntryTime = a.WorkflowTaskEntryTime,
        //                            ExitTime = ExitTime, //a.WorkflowTaskExitTime,
        //                            OutcomeID = a.WorkflowOutcome,
        //                            OutcomeDescription = a.WorkflowOutcomeDescription,
        //                            CustomOutcomeID = a.WorkflowCustomOutcome,
        //                            CustomOutcomeDescription = a.WorkflowCustomOutcomeDescription,
        //                            Comments = a.WorkflowComments,
        //                            Approver = a.WorkflowDisplayName,
        //                            #endregion

        //                            #region Workflow Task Latest
        //                            ActivityTitleLatest = a.WorkflowLastActivityTitle,
        //                            CommentsLatest = a.WorkflowLastComments,
        //                            CustomOutcomeIDLatest = a.WorkflowLastCustomOutcome,
        //                            CustomOutcomeDescriptionLatest = a.WorkflowLastCustomOutcomeDescription,
        //                            UserDisplayNameLatest = a.WorkflowLastDisplayName,
        //                            OutcomeIDLatest = a.WorkflowLastOutcome,
        //                            OutcomeDescriptionLatest = a.WorkflowLastOutcomeDescription,
        //                            IsSPGroupLatest = a.WorkflowLastTaskIsSPGroup,
        //                            UserLatest = a.WorkflowLastTaskUser,
        //                            EntryTimeLatest = a.WorkflowLastTaskEntryTime,
        //                            ExitTimeLatest = a.WorkflowLastTaskExitTime,
        //                            #endregion

        //                            #region Transaction
        //                            ID = a.TransactionID,
        //                            ApplicationID = a.ApplicationID,
        //                            AccountNumber = a.AccountNumber,
        //                            Customer = a.CustomerName,
        //                            Product = a.ProductName,
        //                            Currency = a.CurrencyCode,
        //                            Amount = a.Amount,
        //                            AmountUSD = a.AmountUSD,
        //                            IsTopUrgent = a.IsTopUrgent == "Top Urgent Chain" ? 2 : a.IsTopUrgent == "Top Urgent" ? 1 : 0,
        //                            IsFXTransaction = (a.IsFXTransaction == "Yes") ? true : false,
        //                            LastModifiedBy = a.UpdateBy == null ? a.CreateBy : a.UpdateBy,
        //                            LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,

        //                            ProgressApprover = string.IsNullOrEmpty(a.WorkflowLastDisplayName) ? a.UpdateDate == null ? a.CreateBy : a.UpdateBy : a.WorkflowLastDisplayName,
        //                            CurrentTaskUser = a.WorkflowTaskIsSPGroup ? a.WorkflowTaskUser : a.UserDisplayNameTask,
        //                            ProgressExitTime = a.WorkflowLastTaskExitTime.HasValue ? a.WorkflowLastTaskExitTime : a.UpdateDate == null ? a.CreateDate != null ? a.CreateDate : DateTime.Now : a.UpdateDate.Value
        //                            #endregion
        //                        }).ToList();

        //            userTaskGrid.Page = page.Value;
        //            userTaskGrid.Size = size.Value;
        //            userTaskGrid.Total = data.Count();

        //            userTaskGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };

        //            sortColumn = sortColumn.Equals("UserDisplayName") ?
        //                sortColumn = "CurrentTaskUser" : sortColumn.Equals("Approver") ?
        //                sortColumn = "ProgressApprover" : sortColumn.Equals("ExitTime") ?
        //                sortColumn = "ProgressExitTime" : sortColumn;

        //            string orderBy = string.Format("{0} {1}", sortColumn, sortOrder);


        //            userTaskGrid.Rows = data.AsQueryable().OrderBy(orderBy).Select((a, i) => new SPUserTaskRow()
        //            {
        //                RowID = i + 1,

        //                #region Workflow Context
        //                WorkflowContext = new WorkflowContext()
        //                {
        //                    SPWebID = a.SPWebID,
        //                    SPSiteID = a.SPSiteID,
        //                    SPListID = a.SPListID,
        //                    SPListItemID = a.SPListItemID,
        //                    SPTaskListID = a.SPTaskListID.Value,
        //                    SPTaskListItemID = a.SPTaskListItemID.Value,
        //                    Initiator = a.InitiatorDisplayName,
        //                    ApproverID = a.ApproverID,
        //                    StartTime = a.StartTime,
        //                    StateID = a.StateID.Value,
        //                    StateDescription = a.StateDescription,
        //                    TransactionStatus = a.TransactionStatus,
        //                    WorkflowInstanceID = a.WorkflowInstanceID,
        //                    WorkflowID = a.WorkflowID.Value,
        //                    WorkflowName = a.WorkflowName,
        //                    IsAuthorized = a.IsAuthoriaed
        //                },
        //                #endregion

        //                #region Workflow Task
        //                CurrentTask = new WorkflowTask()
        //                {
        //                    TaskTypeID = a.TaskType.Value,
        //                    TaskTypeDescription = a.TaskTypeDescription,
        //                    Title = a.Title,
        //                    ActivityTitle = a.ActivityTitle,
        //                    User = a.CurrentTaskUser,
        //                    IsSPGroup = a.IsSPGroup,
        //                    EntryTime = a.EntryTime,
        //                    ExitTime = a.ExitTime,
        //                    OutcomeID = a.OutcomeID,
        //                    OutcomeDescription = a.OutcomeDescription,
        //                    CustomOutcomeID = a.CustomOutcomeID,
        //                    CustomOutcomeDescription = a.CustomOutcomeDescription,
        //                    Comments = a.Comments,
        //                    Approver = a.ProgressApprover
        //                },
        //                #endregion

        //                #region Workflow Task Progress
        //                ProgressTask = new WorkflowTask()
        //                {
        //                    ActivityTitle = a.ActivityTitleLatest,
        //                    User = a.IsSPGroupLatest.HasValue ? (a.IsSPGroupLatest.Value ? a.UserLatest : a.UserDisplayNameLatest) : a.LastModifiedBy,
        //                    IsSPGroup = a.IsSPGroupLatest.HasValue ? a.IsSPGroupLatest.Value : false,
        //                    EntryTime = a.EntryTimeLatest.HasValue ? a.EntryTimeLatest : a.LastModifiedDate,
        //                    ExitTime = a.ProgressExitTime,
        //                    OutcomeID = a.OutcomeIDLatest,
        //                    OutcomeDescription = string.IsNullOrEmpty(a.OutcomeDescriptionLatest) ? "Submit Transaction" : a.OutcomeDescriptionLatest,
        //                    CustomOutcomeID = a.CustomOutcomeIDLatest,
        //                    CustomOutcomeDescription = a.CustomOutcomeDescriptionLatest,
        //                    Comments = a.CommentsLatest,
        //                    Approver = a.ProgressApprover
        //                },
        //                #endregion

        //                #region Transaction
        //                Transaction = new TransactionTaskModel()
        //                {
        //                    ID = a.ID,
        //                    ApplicationID = a.ApplicationID,
        //                    Customer = a.Customer,
        //                    Product = a.Product,
        //                    Currency = a.Currency,
        //                    Amount = a.Amount,
        //                    AmountUSD = a.AmountUSD,
        //                    IsTopUrgent = a.IsTopUrgent,
        //                    IsFXTransaction = a.IsFXTransaction,
        //                    TransactionStatus = a.TransactionStatus,
        //                    LastModifiedBy = a.LastModifiedBy,
        //                    LastModifiedDate = a.LastModifiedDate,
        //                    DebitAccNumber = a.AccountNumber
        //                }
        //                #endregion

        //            }).Skip(skip).Take(take).ToList();
        //            isSuccess = true;
        //        }
        //        catch (Exception ex)
        //        {
        //            message = ex.ToString();
        //        }
        //    }

        //    return isSuccess;
        //}

        //       public bool GetCompletedUserTasks(Guid webID, Guid siteID, IList<Guid> workflowIDs, IList<string> state, IList<string> outcome, IList<string> customOutcome, bool showContribute, bool showActiveTask, int? page, int? size, string sortColumn, string sortOrder, IList<SPUserTaskFilter> filters, ref SPUserTaskGrid userTaskGrid, ref string message)
        //       {
        //           bool isSuccess = false;

        //           using (DBSEntities context = new DBSEntities())
        //           {
        //               try
        //               {
        //                   int skip = (page.Value - 1) * size.Value;
        //                   int take = size.Value;

        //                   SPUserTask filter = new SPUserTask();
        //                   filter.WorkflowContext = new WorkflowContext();
        //                   filter.CurrentTask = new WorkflowTask();
        //                   filter.ProgressTask = new WorkflowTask();
        //                   filter.Transaction = new TransactionTaskModel();
        //                   filter.Transaction.IsFXTransaction = null;

        //                   DateTime maxDateEntryTime = new DateTime();
        //                   DateTime maxDateExitTime = new DateTime();

        //                   if (filters != null)
        //                   {
        //                       #region Transaction
        //                       filter.Transaction.ApplicationID = filters.Where(sp => sp.Field.Equals("ApplicationID")).Select(sp => sp.Value).SingleOrDefault();
        //                       filter.Transaction.Customer = filters.Where(sp => sp.Field.Equals("Customer")).Select(sp => sp.Value).SingleOrDefault();
        //                       filter.Transaction.Product = filters.Where(sp => sp.Field.Equals("Product")).Select(sp => sp.Value).SingleOrDefault();
        //                       filter.Transaction.Currency = filters.Where(sp => sp.Field.Equals("Currency")).Select(sp => sp.Value).SingleOrDefault();
        //                       filter.Transaction.DebitAccNumber = (string)filters.Where(sp => sp.Field.Equals("DebitAccNumber")).Select(sp => sp.Value).SingleOrDefault();
        //                       filter.CurrentTask.ActivityTitle = filters.Where(sp => sp.Field.Equals("ActivityTitle")).Select(sp => sp.Value).SingleOrDefault();

        //                       string amount = filters.Where(sp => sp.Field.Equals("Amount")).Select(sp => sp.Value).SingleOrDefault();
        //                       if (!string.IsNullOrEmpty(amount))
        //                       {
        //                           decimal valueAmount;
        //                           bool resultAmount = decimal.TryParse(amount, out valueAmount);
        //                           if (resultAmount)
        //                           {
        //                               filter.Transaction.Amount = valueAmount;
        //                           }
        //                           else
        //                           {
        //                               userTaskGrid.Page = page.Value;
        //                               userTaskGrid.Size = size.Value;
        //                               userTaskGrid.Total = 0;
        //                               return true;
        //                           }
        //                       }

        //                       string amountUSD = filters.Where(sp => sp.Field.Equals("AmountUSD")).Select(sp => sp.Value).SingleOrDefault();
        //                       if (!string.IsNullOrEmpty(amountUSD))
        //                       {
        //                           filter.Transaction.AmountUSD = Decimal.Parse(amountUSD);
        //                       }

        //                       string fxTransaction = filters.Where(sp => sp.Field.Equals("IsFXTransaction")).Select(sp => sp.Value).SingleOrDefault();
        //                       if (!string.IsNullOrEmpty(fxTransaction))
        //                       {
        //                           bool valueFx;
        //                           bool resultFx = bool.TryParse(fxTransaction, out valueFx);
        //                           if (resultFx)
        //                           {
        //                               filter.Transaction.IsFXTransaction = valueFx;//bool.Parse(fxTransaction);
        //                           }
        //                           else
        //                           {
        //                               userTaskGrid.Page = page.Value;
        //                               userTaskGrid.Size = size.Value;
        //                               userTaskGrid.Total = 0;
        //                               return true;
        //                           }
        //                       }

        //                       string topUrgent = filters.Where(sp => sp.Field.Equals("IsTopUrgent")).Select(sp => sp.Value).SingleOrDefault();


        //                       filter.CurrentTask.User = filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
        //                       filter.ProgressTask.User = filters.Where(a => a.Field.Equals("UserApprover")).Select(a => a.Value).SingleOrDefault();

        //                       if (filters.Where(a => a.Field.Equals("EntryTime")).Any())
        //                       {
        //                           filter.CurrentTask.EntryTime = DateTime.Parse((string)filters.Where(a => a.Field.Equals("EntryTime")).Select(a => a.Value).SingleOrDefault());
        //                           maxDateEntryTime = filter.CurrentTask.EntryTime.Value.AddDays(1);
        //                       }

        //                       if (filters.Where(a => a.Field.Equals("ExitTime")).Any())
        //                       {
        //                           filter.CurrentTask.ExitTime = DateTime.Parse(filters.Where(a => a.Field.Equals("ExitTime")).Select(a => a.Value).SingleOrDefault());
        //                           maxDateExitTime = filter.CurrentTask.ExitTime.Value.AddDays(1);
        //                       }
        //                       #endregion
        //                   }
        //                   string orderBy = string.Format("{0} {1}", sortColumn, sortOrder);
        //                   string userName = currentUser.GetCurrentUser().LoginName;
        //                   string[] roles = currentUser.GetCurrentUser().Roles.Select(r => r.Name).ToArray();
        //                   Guid[] workflowID = workflowIDs.ToArray();

        //                   var data = (
        //                           from a in context.V_TaskApprovals

        //                           let isActivityTitle = !string.IsNullOrEmpty(filter.CurrentTask.ActivityTitle)
        //                           let isUser = !string.IsNullOrEmpty(filter.CurrentTask.User)
        //                           let isEntryTime = filter.CurrentTask.EntryTime.HasValue
        //                           let isExitTime = filter.CurrentTask.ExitTime.HasValue
        //                           let isUserApprover = !string.IsNullOrEmpty(filter.ProgressTask.User)

        //                           #region Transaction
        //                           let isApplicationID = !string.IsNullOrEmpty(filter.Transaction.ApplicationID)
        //                           let isCustomer = !string.IsNullOrEmpty(filter.Transaction.Customer)
        //                           let isProduct = !string.IsNullOrEmpty(filter.Transaction.Product)
        //                           let isCurrency = !string.IsNullOrEmpty(filter.Transaction.Currency)
        //                           let isAmount = filter.Transaction.Amount.HasValue
        //                           let isAmountUSD = filter.Transaction.AmountUSD.HasValue
        //                           let isFXTransaction = filter.Transaction.IsFXTransaction.HasValue
        //                           let isTopUrgent = filter.Transaction.IsTopUrgent.HasValue
        //                           let isDebitAccNumber = !string.IsNullOrEmpty(filter.Transaction.DebitAccNumber)
        //                           #endregion

        //                           where

        //                           a.SPWebID.Equals(webID) && a.SPSiteID.Equals(siteID) && // y
        //                           workflowIDs.Contains(a.WorkflowID.Value) && // y
        //                           (showContribute ? roles.Any(x => a.WorkflowTaskUserContribute.Contains(x)) || roles.Any(x => a.InitiatorGroup.Contains(x)) || a.WorkflowInitiator.Equals(userName) : true) && // y
        //                           (!showContribute && showActiveTask ? (a.WorkflowOutcomeDescription.ToLower().Equals("pending"))  // y
        //                           && (roles.Contains(a.WorkflowTaskUser.ToLower()) || a.WorkflowTaskUser.Equals(userName)) // y
        //                           && (a.IsCanceled == false) : true) && // y

        //                           (state.Count() > 0 ? state.Contains(a.WorkflowStateDescription.ToLower()) : true) && // y
        //                           (customOutcome.Count() > 0 ? (a.WorkflowOutcomeDescription.ToLower().Equals("custom") ? customOutcome.Contains(a.WorkflowCustomOutcomeDescription) : true) : true) &&
        //                           (isActivityTitle ? a.WorkflowTaskActivityTitle.Contains(filter.CurrentTask.ActivityTitle) : true) &&
        //                           (isUser ? (a.WorkflowTaskIsSPGroup ? a.WorkflowTaskUser : a.UserDisplayNameTask).Contains(filter.CurrentTask.User) : true) &&
        //                           (isUserApprover ? (string.IsNullOrEmpty(a.WorkflowLastDisplayName) ? (a.UpdateDate == null ? a.CreateBy : a.UpdateBy) : a.WorkflowLastDisplayName).Contains(filter.ProgressTask.User) : true) &&
        //                           (isEntryTime ? (a.WorkflowTaskEntryTime >= filter.CurrentTask.EntryTime.Value && a.WorkflowTaskEntryTime <= maxDateEntryTime) : true) &&
        //                           (isExitTime ? (a.WorkflowTaskExitTime.Value >= filter.CurrentTask.ExitTime.Value && a.WorkflowTaskExitTime.Value <= maxDateExitTime) : true) &&

        //                           #region Transaction
        //(isApplicationID ? a.ApplicationID.ToLower().Contains(filter.Transaction.ApplicationID) : true) &&
        //                           (isCustomer ? a.CustomerName.ToLower().Contains(filter.Transaction.Customer.ToLower()) : true) &&
        //                           (isProduct ? a.ProductName.Contains(filter.Transaction.Product) : true) &&
        //                           (isCurrency ? a.CurrencyCode.Contains(filter.Transaction.Currency) : true) &&
        //                           (isAmount ? a.Amount == filter.Transaction.Amount.Value : true) &&
        //                           (isAmountUSD ? a.AmountUSD == filter.Transaction.AmountUSD.Value : true) &&
        //                           (isFXTransaction ? (a.CurrencyCode != "IDR" && a.DebitCurrencyCode == "IDR").Equals(filter.Transaction.IsFXTransaction.Value) : true) &&
        //                           (isTopUrgent ? a.IsTopUrgent.Equals(filter.Transaction.IsTopUrgent.Value) : true) &&
        //                           (isDebitAccNumber ? a.AccountNumber.Contains(filter.Transaction.DebitAccNumber) : true) &&
        //                           (isActivityTitle ? a.WorkflowTaskActivityTitle.Contains(filter.CurrentTask.ActivityTitle) : true)
        //                           #endregion

        //                           select new
        //                           {
        //                               #region Workflow Context
        //                               SPWebID = a.SPWebID,
        //                               SPSiteID = a.SPSiteID,
        //                               SPListID = a.SPListID,
        //                               SPListItemID = a.SPListItemID,
        //                               SPTaskListID = a.SPTaskListID,
        //                               SPTaskListItemID = a.SPTaskListItemID,
        //                               ApproverID = a.WorkflowApproverID,
        //                               Initiator = a.WorkflowInitiator,
        //                               InitiatorDisplayName = a.UserDisplayNameInitiator,
        //                               StartTime = a.WorkflowStartTime,
        //                               StateID = a.WorkflowState,
        //                               StateDescription = a.WorkflowStateDescription,
        //                               TransactionStatus = a.TransactionStatus,
        //                               //StateDescription = a.WorkflowStateDescription.ToLower() == "running" ? a.WorkflowTaskActivityTitle : a.WorkflowStateDescription.ToLower() == "complete" ? ((a.ApplicationID.ToLower().StartsWith("fx") && a.WorkflowOutcomeDescription.ToLower() == "rejected") || a.WorkflowCustomOutcomeDescription.ToLower() == "approve cancellation" ? "Cancelled"  : "Completed") : a.WorkflowStateDescription.Trim(),
        //                               WorkflowID = a.WorkflowID,
        //                               WorkflowName = a.WorkflowName,
        //                               WorkflowInstanceID = a.WorkflowInstanceID,
        //                               IsAuthoriaed = a.WorkflowTaskIsSPGroup ? (roles.Contains(a.WorkflowTaskUser) ? true : false) : (a.WorkflowTaskUser.Equals(userName) ? true : false),
        //                               #endregion

        //                               #region Workflow Task
        //                               TaskType = a.WorkflowTaskType,
        //                               TaskTypeDescription = a.WorkflowTaskTypeDescription,
        //                               Title = a.WorkflowTaskTitle,
        //                               ActivityTitle = a.WorkflowTaskActivityTitle,
        //                               User = a.WorkflowTaskUser,
        //                               UserDisplayName = a.UserDisplayNameTask,
        //                               IsSPGroup = a.WorkflowTaskIsSPGroup,
        //                               EntryTime = a.WorkflowTaskEntryTime,
        //                               ExitTime = a.WorkflowTaskExitTime != null ? a.WorkflowTaskExitTime : DateTime.Now,
        //                               OutcomeID = a.WorkflowOutcome,
        //                               OutcomeDescription = a.WorkflowOutcomeDescription,
        //                               CustomOutcomeID = a.WorkflowCustomOutcome,
        //                               CustomOutcomeDescription = a.WorkflowCustomOutcomeDescription != null ? a.WorkflowCustomOutcomeDescription : "",
        //                               Comments = a.WorkflowComments,
        //                               Approver = a.WorkflowDisplayName != null ? a.WorkflowDisplayName : "",
        //                               #endregion

        //                               #region Workflow Task Latest
        //                               ActivityTitleLatest = a.WorkflowLastActivityTitle,
        //                               CommentsLatest = a.WorkflowLastComments,
        //                               CustomOutcomeIDLatest = a.WorkflowLastCustomOutcome != null ? a.WorkflowLastCustomOutcome : 0,
        //                               CustomOutcomeDescriptionLatest = a.WorkflowLastCustomOutcomeDescription != null ? a.WorkflowLastCustomOutcomeDescription : "",
        //                               UserDisplayNameLatest = a.WorkflowLastDisplayName,
        //                               OutcomeIDLatest = a.WorkflowLastOutcome,
        //                               OutcomeDescriptionLatest = a.WorkflowLastOutcomeDescription,
        //                               IsSPGroupLatest = a.WorkflowLastTaskIsSPGroup,
        //                               UserLatest = a.WorkflowLastTaskUser,
        //                               EntryTimeLatest = a.WorkflowLastTaskEntryTime,
        //                               ExitTimeLatest = a.WorkflowLastTaskExitTime,
        //                               #endregion

        //                               #region Transaction
        //                               ID = a.TransactionID,
        //                               ApplicationID = a.ApplicationID,
        //                               AccountNumber = a.AccountNumber,
        //                               Customer = a.CustomerName,
        //                               Product = a.ProductName,
        //                               Currency = a.CurrencyCode,
        //                               Amount = a.Amount,
        //                               AmountUSD = a.AmountUSD,
        //                               IsTopUrgent = a.IsTopUrgent,
        //                               IsFXTransaction = (a.CurrencyCode != "IDR" && a.DebitCurrencyCode == "IDR") ? true : false,
        //                               LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
        //                               LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,

        //                               ProgressApprover = string.IsNullOrEmpty(a.WorkflowLastDisplayName) ? a.UpdateDate == null ? a.CreateBy : a.UpdateBy : a.WorkflowLastDisplayName,
        //                               CurrentTaskUser = a.WorkflowTaskIsSPGroup ? a.WorkflowTaskUser : a.UserDisplayNameTask,
        //                               ProgressExitTime = a.WorkflowLastTaskExitTime.HasValue ? a.WorkflowLastTaskExitTime : a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
        //                               #endregion

        //                           });

        //                   userTaskGrid.Page = page.Value;
        //                   userTaskGrid.Size = size.Value;
        //                   userTaskGrid.Total = data.Count();
        //                   int StartRow = (page.Value * size.Value) - size.Value + 1;
        //                   userTaskGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };

        //                   sortColumn = sortColumn.Equals("UserDisplayName") ?
        //                       sortColumn = "CurrentTaskUser" : sortColumn.Equals("Approver") ?
        //                       sortColumn = "ProgressApprover" : sortColumn.Equals("ExitTime") ?
        //                       sortColumn = "ProgressExitTime" : sortColumn;



        //                   userTaskGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
        //                   .Select((a, i) => new SPUserTaskRow()
        //                   {
        //                       RowID = StartRow + i,

        //                       #region Workflow Context
        //                       WorkflowContext = new WorkflowContext()
        //                       {
        //                           SPWebID = a.SPWebID,
        //                           SPSiteID = a.SPSiteID,
        //                           SPListID = a.SPListID,
        //                           SPListItemID = a.SPListItemID,
        //                           SPTaskListID = a.SPTaskListID.Value,
        //                           SPTaskListItemID = a.SPTaskListItemID.Value,
        //                           Initiator = a.InitiatorDisplayName,
        //                           ApproverID = a.ApproverID,
        //                           StartTime = a.StartTime,
        //                           StateID = a.StateID.Value,
        //                           StateDescription = a.StateDescription,
        //                           WorkflowInstanceID = a.WorkflowInstanceID,
        //                           WorkflowID = a.WorkflowID.Value,
        //                           WorkflowName = a.WorkflowName,
        //                           IsAuthorized = a.IsAuthoriaed
        //                       },
        //                       #endregion

        //                       #region Workflow Task
        //                       CurrentTask = new WorkflowTask()
        //                       {
        //                           TaskTypeID = a.TaskType.Value,
        //                           TaskTypeDescription = a.TaskTypeDescription,
        //                           Title = a.Title,
        //                           ActivityTitle = a.ActivityTitle,
        //                           User = a.CurrentTaskUser,
        //                           IsSPGroup = a.IsSPGroup,
        //                           EntryTime = a.EntryTime,
        //                           ExitTime = a.ExitTime,
        //                           OutcomeID = a.OutcomeID,
        //                           OutcomeDescription = a.OutcomeDescription,
        //                           CustomOutcomeID = a.CustomOutcomeID,
        //                           CustomOutcomeDescription = a.CustomOutcomeDescription,
        //                           Comments = a.Comments,
        //                           Approver = a.ProgressApprover
        //                       },
        //                       #endregion

        //                       #region Workflow Task Progress
        //                       ProgressTask = new WorkflowTask()
        //                       {
        //                           ActivityTitle = a.ActivityTitleLatest,
        //                           User = a.IsSPGroupLatest.HasValue ? (a.IsSPGroupLatest.Value ? a.UserLatest : a.UserDisplayNameLatest) : a.LastModifiedBy,
        //                           IsSPGroup = a.IsSPGroupLatest.HasValue ? a.IsSPGroupLatest.Value : false,
        //                           EntryTime = a.EntryTimeLatest.HasValue ? a.EntryTimeLatest : a.LastModifiedDate,
        //                           ExitTime = a.ProgressExitTime,
        //                           OutcomeID = a.OutcomeIDLatest,
        //                           OutcomeDescription = string.IsNullOrEmpty(a.OutcomeDescriptionLatest) ? "Submit Transaction" : a.OutcomeDescriptionLatest,
        //                           CustomOutcomeID = a.CustomOutcomeIDLatest,
        //                           CustomOutcomeDescription = a.CustomOutcomeDescriptionLatest,
        //                           Comments = a.CommentsLatest,
        //                           Approver = a.ProgressApprover
        //                       },
        //                       #endregion

        //                       #region Transaction
        //                       Transaction = new TransactionTaskModel()
        //                       {
        //                           ID = a.ID,
        //                           ApplicationID = a.ApplicationID,
        //                           Customer = a.Customer,
        //                           Product = a.Product,
        //                           Currency = a.Currency,
        //                           Amount = a.Amount,
        //                           AmountUSD = a.AmountUSD,
        //                           IsTopUrgent = a.IsTopUrgent,
        //                           IsFXTransaction = a.IsFXTransaction,
        //                           TransactionStatus = a.TransactionStatus,
        //                           LastModifiedBy = a.LastModifiedBy,
        //                           LastModifiedDate = a.LastModifiedDate,
        //                           DebitAccNumber = a.AccountNumber
        //                       }
        //                       #endregion

        //                   }).Skip(skip).Take(take).ToList();


        //                   isSuccess = true;
        //               }
        //               catch (Exception ex)
        //               {
        //                   message = ex.InnerException.Message;
        //               }
        //           }

        //           return isSuccess;
        //       }

        //public bool GetCompletedSPUserTasks(Guid webID, Guid siteID, IList<Guid> workflowIDs, IList<string> state, IList<string> outcome, IList<string> customOutcome, bool showContribute, bool showActiveTask, int? page, int? size, string sortColumn, string sortOrder, IList<SPUserTaskFilter> filters, ref SPUserTaskGrid userTaskGrid, ref string message)
        //{
        //    bool isSuccess = false;

        //    using (DBSEntities context = new DBSEntities())
        //    {
        //        try
        //        {
        //            int skip = (page.Value - 1) * size.Value;
        //            int take = size.Value;

        //            SPUserTask filter = new SPUserTask();
        //            filter.WorkflowContext = new WorkflowContext();
        //            filter.CurrentTask = new WorkflowTask();
        //            filter.ProgressTask = new WorkflowTask();
        //            filter.Transaction = new TransactionTaskModel();
        //            filter.Transaction.IsFXTransaction = null;

        //            DateTime? maxDateEntryTime = new DateTime();
        //            DateTime maxDateExitTime = new DateTime();

        //            if (filters != null)
        //            {
        //                #region Transaction
        //                filter.Transaction.ApplicationID = filters.Where(sp => sp.Field.Equals("ApplicationID")).Select(sp => sp.Value).SingleOrDefault();
        //                filter.Transaction.Customer = filters.Where(sp => sp.Field.Equals("Customer")).Select(sp => sp.Value).SingleOrDefault();
        //                filter.Transaction.Product = filters.Where(sp => sp.Field.Equals("Product")).Select(sp => sp.Value).SingleOrDefault();
        //                filter.Transaction.Currency = filters.Where(sp => sp.Field.Equals("Currency")).Select(sp => sp.Value).SingleOrDefault();
        //                filter.Transaction.DebitAccNumber = (string)filters.Where(sp => sp.Field.Equals("DebitAccNumber")).Select(sp => sp.Value).SingleOrDefault();
        //                filter.CurrentTask.ActivityTitle = filters.Where(sp => sp.Field.Equals("ActivityTitle")).Select(sp => sp.Value).SingleOrDefault();

        //                string amount = filters.Where(sp => sp.Field.Equals("Amount")).Select(sp => sp.Value).SingleOrDefault();
        //                if (!string.IsNullOrEmpty(amount))
        //                {
        //                    decimal valueAmount;
        //                    bool resultAmount = decimal.TryParse(amount, out valueAmount);
        //                    if (resultAmount)
        //                    {
        //                        filter.Transaction.Amount = valueAmount;
        //                    }
        //                    else
        //                    {
        //                        userTaskGrid.Page = page.Value;
        //                        userTaskGrid.Size = size.Value;
        //                        userTaskGrid.Total = 0;
        //                        return true;
        //                    }
        //                }

        //                string amountUSD = filters.Where(sp => sp.Field.Equals("AmountUSD")).Select(sp => sp.Value).SingleOrDefault();
        //                if (!string.IsNullOrEmpty(amountUSD))
        //                {
        //                    filter.Transaction.AmountUSD = Decimal.Parse(amountUSD);
        //                }
        //                filter.Transaction.IsFXTransactionValue = filters.Where(sp => sp.Field.Equals("IsFXTransaction")).Select(sp => sp.Value).SingleOrDefault();
        //                filter.Transaction.IsTopUrgentValue = filters.Where(sp => sp.Field.Equals("IsTopUrgent")).Select(sp => sp.Value).SingleOrDefault();

        //                filter.CurrentTask.User = filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
        //                filter.ProgressTask.User = filters.Where(a => a.Field.Equals("UserApprover")).Select(a => a.Value).SingleOrDefault();
        //                filter.CurrentTask.TaskTypeDescription = filters.Where(a => a.Field.Equals("EntryTime")).Select(a => a.Value).SingleOrDefault();
        //                //if (filters.Where(a => a.Field.Equals("EntryTime")).Any())
        //                //{
        //                //    filter.CurrentTask.EntryTime = DateTime.Parse((string)filters.Where(a => a.Field.Equals("EntryTime")).Select(a => a.Value).SingleOrDefault());
        //                //    maxDateEntryTime = filter.CurrentTask.EntryTime.Value.AddDays(1);
        //                //}

        //                //if (filters.Where(a => a.Field.Equals("ExitTime")).Any())
        //                //{
        //                //    filter.CurrentTask.ExitTime = DateTime.Parse(filters.Where(a => a.Field.Equals("ExitTime")).Select(a => a.Value).SingleOrDefault());
        //                //    maxDateExitTime = filter.CurrentTask.ExitTime.Value.AddDays(1);
        //                //}
        //                #endregion

        //            }


        //            string orderBy = string.Format("{0} {1}", sortColumn, sortOrder);
        //            string[] roles = currentUser.GetCurrentUser().Roles.Select(r => r.Name).ToArray();
        //            Guid[] workflowID = workflowIDs.ToArray();

        //            #region parameter
        //            string userName = currentUser.GetCurrentUser().LoginName;
        //            string ApplicationID = filter.Transaction.ApplicationID;
        //            string CustomerName = filter.Transaction.Customer;
        //            string ProductName = filter.Transaction.Product;
        //            string CurrencyCode = filter.Transaction.Currency;
        //            Decimal? TransactionAmount = filter.Transaction.Amount;
        //            string DebitAccNumber = filter.Transaction.DebitAccNumber;
        //            string IsFXTransaction = filter.Transaction.IsFXTransactionValue;
        //            string Urgency = filter.Transaction.IsTopUrgentValue;
        //            string ModifiedBy = filter.CurrentTask.User;
        //            string TransactionStatus = filter.Transaction.TransactionStatus;
        //            string modifiedDate = filter.CurrentTask.TaskTypeDescription;
        //            #endregion

        //            var data = (
        //                    from a in context.SP_GetTaskCompleted(userName, ApplicationID, CustomerName, ProductName, CurrencyCode, TransactionAmount, DebitAccNumber, IsFXTransaction, Urgency, TransactionStatus, ModifiedBy, modifiedDate)
        //                    select new
        //                    {
        //                        #region Workflow Context
        //                        SPWebID = a.SPWebID,
        //                        SPSiteID = a.SPSiteID,
        //                        SPListID = a.SPListID,
        //                        SPListItemID = a.SPListItemID,
        //                        SPTaskListID = a.SPTaskListID,
        //                        SPTaskListItemID = a.SPTaskListItemID,
        //                        ApproverID = a.WorkflowApproverID,
        //                        Initiator = a.WorkflowInitiator,
        //                        InitiatorDisplayName = a.UserDisplayNameInitiator,
        //                        StartTime = a.WorkflowStartTime,
        //                        StateID = a.WorkflowState,
        //                        StateDescription = a.WorkflowStateDescription,
        //                        //TransactionStatus = a.Transaction,
        //                        WorkflowID = a.WorkflowID,
        //                        WorkflowName = a.WorkflowName,
        //                        WorkflowInstanceID = a.WorkflowInstanceID,
        //                        IsAuthoriaed = a.WorkflowTaskIsSPGroup ? (roles.Contains(a.WorkflowTaskUser) ? true : false) : (a.WorkflowTaskUser.Equals(userName) ? true : false),
        //                        #endregion

        //                        #region Workflow Task
        //                        TaskType = a.WorkflowTaskType,
        //                        TaskTypeDescription = a.WorkflowTaskTypeDescription,
        //                        Title = a.WorkflowTaskTitle,
        //                        ActivityTitle = a.WorkflowTaskActivityTitle,
        //                        User = a.WorkflowTaskUser,
        //                        UserDisplayName = a.UserDisplayNameTask,
        //                        IsSPGroup = a.WorkflowTaskIsSPGroup,
        //                        EntryTime = a.WorkflowTaskEntryTime,
        //                        ExitTime = a.WorkflowTaskExitTime != null ? a.WorkflowTaskExitTime : DateTime.Now,
        //                        OutcomeID = a.WorkflowOutcome,
        //                        OutcomeDescription = a.WorkflowOutcomeDescription,
        //                        CustomOutcomeID = a.WorkflowCustomOutcome,
        //                        CustomOutcomeDescription = a.WorkflowCustomOutcomeDescription != null ? a.WorkflowCustomOutcomeDescription : "",
        //                        Comments = a.WorkflowComments,
        //                        Approver = a.WorkflowDisplayName != null ? a.WorkflowDisplayName : "",
        //                        #endregion

        //                        #region Workflow Task Latest
        //                        ActivityTitleLatest = a.WorkflowLastActivityTitle,
        //                        CommentsLatest = a.WorkflowLastComments,
        //                        CustomOutcomeIDLatest = a.WorkflowLastCustomOutcome != null ? a.WorkflowLastCustomOutcome : 0,
        //                        CustomOutcomeDescriptionLatest = a.WorkflowLastCustomOutcomeDescription != null ? a.WorkflowLastCustomOutcomeDescription : "",
        //                        UserDisplayNameLatest = a.WorkflowLastDisplayName,
        //                        OutcomeIDLatest = a.WorkflowLastOutcome,
        //                        OutcomeDescriptionLatest = a.WorkflowLastOutcomeDescription,
        //                        IsSPGroupLatest = a.WorkflowLastTaskIsSPGroup,
        //                        UserLatest = a.WorkflowLastTaskUser,
        //                        EntryTimeLatest = a.WorkflowLastTaskEntryTime,
        //                        ExitTimeLatest = a.WorkflowLastTaskExitTime,
        //                        #endregion

        //                        #region Transaction
        //                        ID = a.TransactionID,
        //                        ApplicationID = a.ApplicationID,
        //                        AccountNumber = a.AccountNumber,
        //                        Customer = a.CustomerName,
        //                        Product = a.ProductName,
        //                        Currency = a.CurrencyCode,
        //                        Amount = a.Amount,
        //                        AmountUSD = a.AmountUSD,
        //                        IsTopUrgent = a.IsTopUrgent,
        //                        IsFXTransaction = (a.CurrencyCode != "IDR" && a.DebitCurrencyCode == "IDR") ? true : false,
        //                        LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
        //                        LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,

        //                        ProgressApprover = string.IsNullOrEmpty(a.WorkflowLastDisplayName) ? a.UpdateDate == null ? a.CreateBy : a.UpdateBy : a.WorkflowLastDisplayName,
        //                        //CurrentTaskUser = a.WorkflowTaskIsSPGroup ? a.WorkflowTaskUser : a.UserDisplayNameTask,
        //                        CurrentTaskUser = a.WorkflowLastDisplayName,
        //                        ProgressExitTime = a.WorkflowLastTaskExitTime,
        //                        ProductID = a.ProductID
        //                        #endregion

        //                    }).ToList();

        //            userTaskGrid.Page = page.Value;
        //            userTaskGrid.Size = size.Value;
        //            userTaskGrid.Total = data.Count();
        //            int StartRow = (page.Value * size.Value) - size.Value + 1;
        //            userTaskGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };

        //            sortColumn = sortColumn.Equals("UserDisplayName") ?
        //                sortColumn = "CurrentTaskUser" : sortColumn.Equals("Approver") ?
        //                sortColumn = "ProgressApprover" : sortColumn.Equals("ExitTime") ?
        //                sortColumn = "ProgressExitTime" : sortColumn;



        //            userTaskGrid.Rows = data.AsQueryable().OrderBy(orderBy).AsEnumerable()
        //            .Select((a, i) => new SPUserTaskRow()
        //            {
        //                RowID = StartRow + i,

        //                #region Workflow Context
        //                WorkflowContext = new WorkflowContext()
        //                {
        //                    SPWebID = a.SPWebID,
        //                    SPSiteID = a.SPSiteID,
        //                    SPListID = a.SPListID,
        //                    SPListItemID = a.SPListItemID,
        //                    SPTaskListID = a.SPTaskListID.Value,
        //                    SPTaskListItemID = a.SPTaskListItemID.Value,
        //                    Initiator = a.InitiatorDisplayName,
        //                    ApproverID = a.ApproverID,
        //                    StartTime = a.StartTime,
        //                    StateID = a.StateID.Value,
        //                    StateDescription = a.StateDescription,
        //                    WorkflowInstanceID = a.WorkflowInstanceID,
        //                    WorkflowID = a.WorkflowID.Value,
        //                    WorkflowName = a.WorkflowName,
        //                    IsAuthorized = a.IsAuthoriaed
        //                },
        //                #endregion

        //                #region Workflow Task
        //                CurrentTask = new WorkflowTask()
        //                {
        //                    TaskTypeID = a.TaskType.Value,
        //                    TaskTypeDescription = a.TaskTypeDescription,
        //                    Title = a.Title,
        //                    ActivityTitle = a.ActivityTitle,
        //                    User = a.CurrentTaskUser,
        //                    IsSPGroup = a.IsSPGroup,
        //                    EntryTime = a.EntryTime,
        //                    ExitTime = a.ExitTime,
        //                    OutcomeID = a.OutcomeID,
        //                    OutcomeDescription = a.OutcomeDescription,
        //                    CustomOutcomeID = a.CustomOutcomeID,
        //                    CustomOutcomeDescription = a.CustomOutcomeDescription,
        //                    Comments = a.Comments,
        //                    Approver = a.ProgressApprover
        //                },
        //                #endregion

        //                #region Workflow Task Progress
        //                ProgressTask = new WorkflowTask()
        //                {
        //                    ActivityTitle = a.ActivityTitleLatest,
        //                    User = a.IsSPGroupLatest.HasValue ? (a.IsSPGroupLatest.Value ? a.UserLatest : a.UserDisplayNameLatest) : a.LastModifiedBy,
        //                    IsSPGroup = a.IsSPGroupLatest.HasValue ? a.IsSPGroupLatest.Value : false,
        //                    EntryTime = a.EntryTimeLatest.HasValue ? a.EntryTimeLatest : a.LastModifiedDate,
        //                    ExitTime = a.ProgressExitTime,
        //                    OutcomeID = a.OutcomeIDLatest,
        //                    OutcomeDescription = string.IsNullOrEmpty(a.OutcomeDescriptionLatest) ? "Submit Transaction" : a.OutcomeDescriptionLatest,
        //                    CustomOutcomeID = a.CustomOutcomeIDLatest,
        //                    CustomOutcomeDescription = a.CustomOutcomeDescriptionLatest,
        //                    Comments = a.CommentsLatest,
        //                    Approver = a.ProgressApprover
        //                },
        //                #endregion

        //                #region Transaction
        //                Transaction = new TransactionTaskModel()
        //                {
        //                    ID = a.ID,
        //                    ApplicationID = a.ApplicationID,
        //                    Customer = a.Customer,
        //                    Product = a.Product,
        //                    Currency = a.Currency,
        //                    Amount = a.Amount,
        //                    AmountUSD = a.AmountUSD,
        //                    IsTopUrgentValue = a.IsTopUrgent,
        //                    IsFXTransaction = a.IsFXTransaction,
        //                    TransactionStatus = "Completed",
        //                    LastModifiedBy = a.LastModifiedBy,
        //                    LastModifiedDate = a.LastModifiedDate,
        //                    DebitAccNumber = a.AccountNumber,
        //                    ProductID = (int)a.ProductID
        //                }
        //                #endregion

        //            }).Skip(skip).Take(take).ToList();


        //            isSuccess = true;
        //        }
        //        catch (Exception ex)
        //        {
        //            message = ex.InnerException.Message;
        //        }
        //    }

        //    return isSuccess;
        //}

        //public bool GetCanceledSPUserTasks(Guid webID, Guid siteID, IList<Guid> workflowIDs, IList<string> state, IList<string> outcome, IList<string> customOutcome, bool showContribute, bool showActiveTask, int? page, int? size, string sortColumn, string sortOrder, IList<SPUserTaskFilter> filters, ref SPUserTaskGrid userTaskGrid, ref string message)
        //{
        //    bool isSuccess = false;

        //    using (DBSEntities context = new DBSEntities())
        //    {
        //        try
        //        {
        //            int skip = (page.Value - 1) * size.Value;
        //            int take = size.Value;

        //            SPUserTask filter = new SPUserTask();
        //            filter.WorkflowContext = new WorkflowContext();
        //            filter.CurrentTask = new WorkflowTask();
        //            filter.ProgressTask = new WorkflowTask();
        //            filter.Transaction = new TransactionTaskModel();
        //            filter.Transaction.IsFXTransaction = null;

        //            DateTime maxDateEntryTime = new DateTime();
        //            DateTime maxDateExitTime = new DateTime();

        //            if (filters != null)
        //            {
        //                #region Transaction
        //                filter.Transaction.ApplicationID = filters.Where(sp => sp.Field.Equals("ApplicationID")).Select(sp => sp.Value).SingleOrDefault();
        //                filter.Transaction.Customer = filters.Where(sp => sp.Field.Equals("Customer")).Select(sp => sp.Value).SingleOrDefault();
        //                filter.Transaction.Product = filters.Where(sp => sp.Field.Equals("Product")).Select(sp => sp.Value).SingleOrDefault();
        //                filter.Transaction.Currency = filters.Where(sp => sp.Field.Equals("Currency")).Select(sp => sp.Value).SingleOrDefault();
        //                filter.Transaction.DebitAccNumber = (string)filters.Where(sp => sp.Field.Equals("DebitAccNumber")).Select(sp => sp.Value).SingleOrDefault();
        //                filter.Transaction.TransactionStatus = filters.Where(sp => sp.Field.Equals("TransactionStats")).Select(sp => sp.Value).SingleOrDefault();
        //                //filter.WorkflowContext.TransactionStatus = filters.Where(sp => sp.Field.Equals("TransactionStatus")).Select(sp => sp.Value).SingleOrDefault();
        //                filter.ProgressTask.Approver = filters.Where(sp => sp.Field.Equals("UserApprover")).Select(sp => sp.Value).SingleOrDefault();

        //                string amount = filters.Where(sp => sp.Field.Equals("Amount")).Select(sp => sp.Value).SingleOrDefault();
        //                if (!string.IsNullOrEmpty(amount))
        //                {
        //                    decimal valueAmount;
        //                    bool resultAmount = decimal.TryParse(amount, out valueAmount);
        //                    if (resultAmount)
        //                    {
        //                        filter.Transaction.Amount = valueAmount;
        //                    }
        //                    else
        //                    {
        //                        userTaskGrid.Page = page.Value;
        //                        userTaskGrid.Size = size.Value;
        //                        userTaskGrid.Total = 0;
        //                        return true;
        //                    }
        //                }

        //                string amountUSD = filters.Where(sp => sp.Field.Equals("AmountUSD")).Select(sp => sp.Value).SingleOrDefault();
        //                if (!string.IsNullOrEmpty(amountUSD))
        //                {
        //                    filter.Transaction.AmountUSD = Decimal.Parse(amountUSD);
        //                }

        //                filter.Transaction.IsFXTransactionValue = filters.Where(sp => sp.Field.Equals("IsFXTransaction")).Select(sp => sp.Value).SingleOrDefault();
        //                filter.Transaction.IsTopUrgentValue = filters.Where(sp => sp.Field.Equals("IsTopUrgent")).Select(sp => sp.Value).SingleOrDefault();

        //                filter.CurrentTask.User = filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
        //                filter.ProgressTask.User = filters.Where(a => a.Field.Equals("UserApprover")).Select(a => a.Value).SingleOrDefault();
        //                filter.CurrentTask.TaskTypeDescription = filters.Where(a => a.Field.Equals("EntryTime")).Select(a => a.Value).SingleOrDefault();
        //                //if (filters.Where(a => a.Field.Equals("EntryTime")).Any())
        //                //{
        //                //    filter.CurrentTask.EntryTime = DateTime.Parse((string)filters.Where(a => a.Field.Equals("EntryTime")).Select(a => a.Value).SingleOrDefault());
        //                //    maxDateEntryTime = filter.CurrentTask.EntryTime.Value.AddDays(1);
        //                //}

        //                if (filters.Where(a => a.Field.Equals("ExitTime")).Any())
        //                {
        //                    filter.CurrentTask.ExitTime = DateTime.Parse(filters.Where(a => a.Field.Equals("ExitTime")).Select(a => a.Value).SingleOrDefault());
        //                    maxDateExitTime = filter.CurrentTask.ExitTime.Value.AddDays(1);
        //                }

        //                #endregion
        //            }

        //            string userName = currentUser.GetCurrentUser().LoginName;
        //            string[] roles = currentUser.GetCurrentUser().Roles.Select(r => r.Name).ToArray();
        //            Guid[] workflowID = workflowIDs.ToArray();


        //            #region parameter
        //            string Username = currentUser.GetCurrentUser().LoginName;
        //            string ApplicationID = filter.Transaction.ApplicationID;
        //            string CustomerName = filter.Transaction.Customer;
        //            string ProductName = filter.Transaction.Product;
        //            string CurrencyCode = filter.Transaction.Currency;
        //            Decimal? TransactionAmount = filter.Transaction.Amount;
        //            string DebitAccNumber = filter.Transaction.DebitAccNumber;
        //            string IsFXTransaction = filter.Transaction.IsFXTransactionValue;
        //            string Urgency = filter.Transaction.IsTopUrgentValue;
        //            string ModifiedBy = filter.CurrentTask.User;
        //            string TransactionStatus = filter.Transaction.TransactionStatus;
        //            string modifiedDate = filter.CurrentTask.TaskTypeDescription;
        //            #endregion

        //            var data = (
        //                    from a in context.SP_GetTaskCanceled(Username, ApplicationID, CustomerName, ProductName, CurrencyCode, TransactionAmount, DebitAccNumber, IsFXTransaction, Urgency, TransactionStatus, ModifiedBy, modifiedDate)
        //                    select new
        //                    {
        //                        #region Workflow Context
        //                        SPWebID = a.SPWebID,
        //                        SPSiteID = a.SPSiteID,
        //                        SPListID = a.SPListID,
        //                        SPListItemID = a.SPListItemID,
        //                        SPTaskListID = a.SPTaskListID,
        //                        SPTaskListItemID = a.SPTaskListItemID,
        //                        ApproverID = a.WorkflowApproverID,
        //                        Initiator = a.WorkflowInitiator,
        //                        InitiatorDisplayName = a.UserDisplayNameInitiator,
        //                        StartTime = a.WorkflowStartTime,
        //                        StateID = a.WorkflowState,
        //                        StateDescription = a.WorkflowStateDescription,
        //                        TransactionStatus = "Canceled",
        //                        //StateDescription = a.WorkflowStateDescription.ToLower() == "running" ? a.WorkflowTaskActivityTitle : a.WorkflowStateDescription.ToLower() == "complete" ? ((a.ApplicationID.ToLower().StartsWith("fx") && a.WorkflowOutcomeDescription.ToLower() == "rejected") || a.WorkflowCustomOutcomeDescription.ToLower() == "approve cancellation" ? "Cancelled"  : "Completed") : a.WorkflowStateDescription.Trim(),
        //                        WorkflowID = a.WorkflowID,
        //                        WorkflowName = a.WorkflowName,
        //                        WorkflowInstanceID = a.WorkflowInstanceID,
        //                        IsAuthoriaed = a.WorkflowTaskIsSPGroup ? (roles.Contains(a.WorkflowTaskUser) ? true : false) : (a.WorkflowTaskUser.Equals(userName) ? true : false),
        //                        #endregion

        //                        #region Workflow Task
        //                        TaskType = a.WorkflowTaskType,
        //                        TaskTypeDescription = a.WorkflowTaskTypeDescription,
        //                        Title = a.WorkflowTaskTitle,
        //                        ActivityTitle = a.WorkflowTaskActivityTitle,
        //                        User = a.WorkflowTaskUser,
        //                        UserDisplayName = (a.UserDisplayNameTask != null) ? a.UserDisplayNameTask : "",
        //                        IsSPGroup = a.WorkflowTaskIsSPGroup,
        //                        EntryTime = a.WorkflowTaskEntryTime,
        //                        ExitTime = a.WorkflowTaskExitTime,
        //                        OutcomeID = a.WorkflowOutcome,
        //                        OutcomeDescription = a.WorkflowOutcomeDescription,
        //                        CustomOutcomeID = a.WorkflowCustomOutcome,
        //                        CustomOutcomeDescription = a.WorkflowCustomOutcomeDescription,
        //                        Comments = a.WorkflowComments,
        //                        Approver = a.WorkflowDisplayName,
        //                        #endregion

        //                        #region Workflow Task Latest
        //                        ActivityTitleLatest = a.WorkflowLastActivityTitle,
        //                        CommentsLatest = a.WorkflowLastComments,
        //                        CustomOutcomeIDLatest = a.WorkflowLastCustomOutcome,
        //                        CustomOutcomeDescriptionLatest = a.WorkflowLastCustomOutcomeDescription,
        //                        UserDisplayNameLatest = a.WorkflowLastDisplayName,
        //                        OutcomeIDLatest = a.WorkflowLastOutcome,
        //                        OutcomeDescriptionLatest = a.WorkflowLastOutcomeDescription,
        //                        IsSPGroupLatest = a.WorkflowLastTaskIsSPGroup,
        //                        UserLatest = a.WorkflowLastTaskUser,
        //                        EntryTimeLatest = a.WorkflowLastTaskEntryTime,
        //                        ExitTimeLatest = a.WorkflowLastTaskExitTime,
        //                        #endregion

        //                        #region Transaction
        //                        ID = a.TransactionID,
        //                        ApplicationID = a.ApplicationID,
        //                        AccountNumber = a.AccountNumber,
        //                        Customer = a.CustomerName,
        //                        Product = a.ProductName,
        //                        Currency = a.CurrencyCode,
        //                        Amount = a.Amount,
        //                        AmountUSD = a.AmountUSD,
        //                        IsTopUrgent = a.IsTopUrgent == "Top Urgent Chain" ? 2 : a.IsTopUrgent == "Top Urgent" ? 1 : 0,
        //                        IsFXTransaction = (a.IsFXTransaction == "Yes") ? true : false,
        //                        LastModifiedBy = a.UpdateBy == null ? a.CreateBy : a.UpdateBy,
        //                        LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,

        //                        ProgressApprover = string.IsNullOrEmpty(a.WorkflowLastDisplayName) ? a.UpdateDate == null ? a.CreateBy : a.UpdateBy : a.WorkflowLastDisplayName,
        //                        //CurrentTaskUser = a.WorkflowTaskIsSPGroup ? a.WorkflowTaskUser : a.UserDisplayNameTask,
        //                        CurrentTaskUser = a.WorkflowLastDisplayName,
        //                        ProgressExitTime = a.WorkflowLastTaskExitTime//.HasValue ? a.WorkflowLastTaskExitTime : a.UpdateDate == null ? a.CreateDate != null ? a.CreateDate : DateTime.Now : a.UpdateDate.Value
        //                        ,
        //                        ProductID = a.ProductID
        //                        #endregion
        //                    }).ToList();

        //            userTaskGrid.Page = page.Value;
        //            userTaskGrid.Size = size.Value;
        //            userTaskGrid.Total = data.Count();

        //            userTaskGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };

        //            sortColumn = sortColumn.Equals("UserDisplayName") ?
        //                sortColumn = "CurrentTaskUser" : sortColumn.Equals("Approver") ?
        //                sortColumn = "ProgressApprover" : sortColumn.Equals("ExitTime") ?
        //                sortColumn = "ProgressExitTime" : sortColumn;

        //            string orderBy = string.Format("{0} {1}", sortColumn, sortOrder);


        //            userTaskGrid.Rows = data.AsQueryable().OrderBy(orderBy).Select((a, i) => new SPUserTaskRow()
        //            {
        //                RowID = i + 1,

        //                #region Workflow Context
        //                WorkflowContext = new WorkflowContext()
        //                {
        //                    SPWebID = a.SPWebID,
        //                    SPSiteID = a.SPSiteID,
        //                    SPListID = a.SPListID,
        //                    SPListItemID = a.SPListItemID,
        //                    SPTaskListID = a.SPTaskListID.Value,
        //                    SPTaskListItemID = a.SPTaskListItemID.Value,
        //                    Initiator = a.InitiatorDisplayName,
        //                    ApproverID = a.ApproverID,
        //                    StartTime = a.StartTime,
        //                    StateID = a.StateID.Value,
        //                    StateDescription = a.StateDescription,
        //                    TransactionStatus = a.TransactionStatus,
        //                    WorkflowInstanceID = a.WorkflowInstanceID,
        //                    WorkflowID = a.WorkflowID.Value,
        //                    WorkflowName = a.WorkflowName,
        //                    IsAuthorized = a.IsAuthoriaed
        //                },
        //                #endregion

        //                #region Workflow Task
        //                CurrentTask = new WorkflowTask()
        //                {
        //                    TaskTypeID = a.TaskType.Value,
        //                    TaskTypeDescription = a.TaskTypeDescription,
        //                    Title = a.Title,
        //                    ActivityTitle = a.ActivityTitle,
        //                    User = a.CurrentTaskUser,
        //                    IsSPGroup = a.IsSPGroup,
        //                    EntryTime = a.EntryTime,
        //                    ExitTime = a.ExitTime,
        //                    OutcomeID = a.OutcomeID,
        //                    OutcomeDescription = a.OutcomeDescription,
        //                    CustomOutcomeID = a.CustomOutcomeID,
        //                    CustomOutcomeDescription = a.CustomOutcomeDescription,
        //                    Comments = a.Comments,
        //                    Approver = a.ProgressApprover
        //                },
        //                #endregion

        //                #region Workflow Task Progress
        //                ProgressTask = new WorkflowTask()
        //                {
        //                    ActivityTitle = a.ActivityTitleLatest,
        //                    User = a.IsSPGroupLatest.HasValue ? (a.IsSPGroupLatest.Value ? a.UserLatest : a.UserDisplayNameLatest) : a.LastModifiedBy,
        //                    IsSPGroup = a.IsSPGroupLatest.HasValue ? a.IsSPGroupLatest.Value : false,
        //                    EntryTime = a.EntryTimeLatest.HasValue ? a.EntryTimeLatest : a.LastModifiedDate,
        //                    ExitTime = a.ProgressExitTime,
        //                    OutcomeID = a.OutcomeIDLatest,
        //                    OutcomeDescription = string.IsNullOrEmpty(a.OutcomeDescriptionLatest) ? "Submit Transaction" : a.OutcomeDescriptionLatest,
        //                    CustomOutcomeID = a.CustomOutcomeIDLatest,
        //                    CustomOutcomeDescription = a.CustomOutcomeDescriptionLatest,
        //                    Comments = a.CommentsLatest,
        //                    Approver = a.ProgressApprover
        //                },
        //                #endregion

        //                #region Transaction
        //                Transaction = new TransactionTaskModel()
        //                {
        //                    ID = a.ID,
        //                    ApplicationID = a.ApplicationID,
        //                    Customer = a.Customer,
        //                    Product = a.Product,
        //                    Currency = a.Currency,
        //                    Amount = a.Amount,
        //                    AmountUSD = a.AmountUSD,
        //                    IsTopUrgent = a.IsTopUrgent,
        //                    IsFXTransaction = a.IsFXTransaction,
        //                    TransactionStatus = "Canceled", //a.TransactionStatus,
        //                    LastModifiedBy = a.LastModifiedBy,
        //                    LastModifiedDate = a.LastModifiedDate,
        //                    DebitAccNumber = a.AccountNumber,
        //                    ProductID = (int)a.ProductID
        //                }
        //                #endregion

        //            }).Skip(skip).Take(take).ToList();
        //            isSuccess = true;
        //        }
        //        catch (Exception ex)
        //        {
        //            message = ex.ToString();
        //        }
        //    }

        //    return isSuccess;
        //}

        //       public bool GetMonitoringUserTasks(Guid webID, Guid siteID, IList<Guid> workflowIDs, IList<string> state, IList<string> outcome, IList<string> customOutcome, bool showContribute, bool showActiveTask, int? page, int? size, string sortColumn, string sortOrder, IList<SPUserTaskFilter> filters, ref SPUserTaskGrid userTaskGrid, ref string message)
        //       {
        //           bool isSuccess = false;

        //           using (DBSEntities context = new DBSEntities())
        //           {
        //               try
        //               {
        //                   int skip = (page.Value - 1) * size.Value;
        //                   int take = size.Value;

        //                   SPUserTask filter = new SPUserTask();
        //                   filter.WorkflowContext = new WorkflowContext();
        //                   filter.CurrentTask = new WorkflowTask();
        //                   filter.ProgressTask = new WorkflowTask();
        //                   filter.Transaction = new TransactionTaskModel();
        //                   filter.Transaction.IsFXTransaction = null;

        //                   DateTime maxDateEntryTime = new DateTime();
        //                   DateTime maxDateExitTime = new DateTime();

        //                   if (filters != null)
        //                   {
        //                       #region Transaction
        //                       filter.Transaction.ApplicationID = filters.Where(sp => sp.Field.Equals("ApplicationID")).Select(sp => sp.Value).SingleOrDefault();
        //                       filter.Transaction.Customer = filters.Where(sp => sp.Field.Equals("Customer")).Select(sp => sp.Value).SingleOrDefault();
        //                       filter.Transaction.Product = filters.Where(sp => sp.Field.Equals("Product")).Select(sp => sp.Value).SingleOrDefault();
        //                       filter.Transaction.Currency = filters.Where(sp => sp.Field.Equals("Currency")).Select(sp => sp.Value).SingleOrDefault();
        //                       filter.Transaction.DebitAccNumber = (string)filters.Where(sp => sp.Field.Equals("DebitAccNumber")).Select(sp => sp.Value).SingleOrDefault();
        //                       filter.CurrentTask.ActivityTitle = filters.Where(sp => sp.Field.Equals("ActivityTitle")).Select(sp => sp.Value).SingleOrDefault();

        //                       string amount = filters.Where(sp => sp.Field.Equals("Amount")).Select(sp => sp.Value).SingleOrDefault();
        //                       if (!string.IsNullOrEmpty(amount))
        //                       {
        //                           decimal valueAmount;
        //                           bool resultAmount = decimal.TryParse(amount, out valueAmount);
        //                           if (resultAmount)
        //                           {
        //                               filter.Transaction.Amount = valueAmount;
        //                           }
        //                           else
        //                           {
        //                               userTaskGrid.Page = page.Value;
        //                               userTaskGrid.Size = size.Value;
        //                               userTaskGrid.Total = 0;
        //                               return true;
        //                           }
        //                       }

        //                       string amountUSD = filters.Where(sp => sp.Field.Equals("AmountUSD")).Select(sp => sp.Value).SingleOrDefault();
        //                       if (!string.IsNullOrEmpty(amountUSD))
        //                       {
        //                           filter.Transaction.AmountUSD = Decimal.Parse(amountUSD);
        //                       }

        //                       string fxTransaction = filters.Where(sp => sp.Field.Equals("IsFXTransaction")).Select(sp => sp.Value).SingleOrDefault();
        //                       if (!string.IsNullOrEmpty(fxTransaction))
        //                       {
        //                           bool valueFx;
        //                           bool resultFx = bool.TryParse(fxTransaction, out valueFx);
        //                           if (resultFx)
        //                           {
        //                               filter.Transaction.IsFXTransaction = valueFx;//bool.Parse(fxTransaction);
        //                           }
        //                           else
        //                           {
        //                               userTaskGrid.Page = page.Value;
        //                               userTaskGrid.Size = size.Value;
        //                               userTaskGrid.Total = 0;
        //                               return true;
        //                           }
        //                       }

        //                       string topUrgent = filters.Where(sp => sp.Field.Equals("IsTopUrgent")).Select(sp => sp.Value).SingleOrDefault();


        //                       filter.CurrentTask.User = filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
        //                       filter.ProgressTask.User = filters.Where(a => a.Field.Equals("UserApprover")).Select(a => a.Value).SingleOrDefault();

        //                       if (filters.Where(a => a.Field.Equals("EntryTime")).Any())
        //                       {
        //                           filter.CurrentTask.EntryTime = DateTime.Parse((string)filters.Where(a => a.Field.Equals("EntryTime")).Select(a => a.Value).SingleOrDefault());
        //                           maxDateEntryTime = filter.CurrentTask.EntryTime.Value.AddDays(1);
        //                       }

        //                       if (filters.Where(a => a.Field.Equals("ExitTime")).Any())
        //                       {
        //                           filter.CurrentTask.ExitTime = DateTime.Parse(filters.Where(a => a.Field.Equals("ExitTime")).Select(a => a.Value).SingleOrDefault());
        //                           maxDateExitTime = filter.CurrentTask.ExitTime.Value.AddDays(1);
        //                       }
        //                       #endregion
        //                   }
        //                   sortColumn = sortColumn.Equals("UserDisplayName") ?
        //                   sortColumn = "CurrentTaskUser" : sortColumn.Equals("Approver") ?
        //                   sortColumn = "ProgressApprover" :
        //                   (sortColumn.Equals("ExitTime") ? sortColumn = "ProgressExitTime" : sortColumn);
        //                   string orderBy = string.Format("{0} {1}", sortColumn, sortOrder);
        //                   string userName = currentUser.GetCurrentUser().LoginName;
        //                   string[] roles = currentUser.GetCurrentUser().Roles.Select(r => r.Name).ToArray();
        //                   Guid[] workflowID = workflowIDs.ToArray();


        //                   var data = (
        //                           from a in context.V_TaskApprovals

        //                           let isActivityTitle = !string.IsNullOrEmpty(filter.CurrentTask.ActivityTitle)
        //                           let isUser = !string.IsNullOrEmpty(filter.CurrentTask.User)
        //                           let isEntryTime = filter.CurrentTask.EntryTime.HasValue
        //                           let isExitTime = filter.CurrentTask.ExitTime.HasValue
        //                           let isUserApprover = !string.IsNullOrEmpty(filter.ProgressTask.User)

        //                           #region Transaction
        //                           let isApplicationID = !string.IsNullOrEmpty(filter.Transaction.ApplicationID)
        //                           let isCustomer = !string.IsNullOrEmpty(filter.Transaction.Customer)
        //                           let isProduct = !string.IsNullOrEmpty(filter.Transaction.Product)
        //                           let isCurrency = !string.IsNullOrEmpty(filter.Transaction.Currency)
        //                           let isAmount = filter.Transaction.Amount.HasValue
        //                           let isAmountUSD = filter.Transaction.AmountUSD.HasValue
        //                           let isFXTransaction = filter.Transaction.IsFXTransaction.HasValue
        //                           let isTopUrgent = filter.Transaction.IsTopUrgent.HasValue
        //                           let isDebitAccNumber = !string.IsNullOrEmpty(filter.Transaction.DebitAccNumber)
        //                           #endregion

        //                           where

        //                           a.SPWebID.Equals(webID) && a.SPSiteID.Equals(siteID) && // y
        //                           workflowIDs.Contains(a.WorkflowID.Value) && // y
        //                           (showContribute ? roles.Any(x => a.WorkflowTaskUserContribute.Contains(x)) || roles.Any(x => a.InitiatorGroup.Contains(x)) || a.WorkflowInitiator.Equals(userName) : true) && // y
        //                           (!showContribute && showActiveTask ? (a.WorkflowOutcomeDescription.ToLower().Equals("pending"))  // y
        //                           && (roles.Contains(a.WorkflowTaskUser.ToLower()) || a.WorkflowTaskUser.Equals(userName)) // y
        //                           && (a.IsCanceled == false) : true) && // y

        //                           (state.Count() > 0 ? state.Contains(a.WorkflowStateDescription.ToLower()) : true) && // y
        //                           (customOutcome.Count() > 0 ? (a.WorkflowOutcomeDescription.ToLower().Equals("custom") ? customOutcome.Contains(a.WorkflowCustomOutcomeDescription) : true) : true) &&
        //                           (isActivityTitle ? a.WorkflowTaskActivityTitle.Contains(filter.CurrentTask.ActivityTitle) : true) &&
        //                           (isUser ? (a.WorkflowTaskIsSPGroup ? a.WorkflowTaskUser : a.UserDisplayNameTask).Contains(filter.CurrentTask.User) : true) &&
        //                           (isUserApprover ? (string.IsNullOrEmpty(a.WorkflowLastDisplayName) ? (a.UpdateDate == null ? a.CreateBy : a.UpdateBy) : a.WorkflowLastDisplayName).Contains(filter.ProgressTask.User) : true) &&
        //                           (isEntryTime ? (a.WorkflowTaskEntryTime >= filter.CurrentTask.EntryTime.Value && a.WorkflowTaskEntryTime <= maxDateEntryTime) : true) &&
        //                           (isExitTime ? a.ProgressExitTime > filter.CurrentTask.ExitTime.Value && (a.ProgressExitTime < maxDateExitTime) : true) &&
        //                               //(isExitTime ? (a.WorkflowLastTaskExitTime == null ? a.CreateDate : a.WorkflowLastTaskExitTime) > filter.CurrentTask.ExitTime.Value && ((a.WorkflowLastTaskExitTime == null ? a.CreateDate : a.WorkflowLastTaskExitTime) < maxDateExitTime) : true) &&
        //                               //(isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
        //                               //ProgressExitTime = a.WorkflowLastTaskExitTime.HasValue ? a.WorkflowLastTaskExitTime : a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
        //                               //(isExitTime ? (a.WorkflowTaskExitTime.Value >= filter.CurrentTask.ExitTime.Value && a.WorkflowTaskExitTime.Value <= maxDateExitTime) : true) &&

        //                           #region Transaction
        //(isApplicationID ? a.ApplicationID.ToLower().Contains(filter.Transaction.ApplicationID) : true) &&
        //                           (isCustomer ? a.CustomerName.ToLower().Contains(filter.Transaction.Customer.ToLower()) : true) &&
        //                           (isProduct ? a.ProductName.Contains(filter.Transaction.Product) : true) &&
        //                           (isCurrency ? a.CurrencyCode.Contains(filter.Transaction.Currency) : true) &&
        //                           (isAmount ? a.Amount == filter.Transaction.Amount.Value : true) &&
        //                           (isAmountUSD ? a.AmountUSD == filter.Transaction.AmountUSD.Value : true) &&
        //                           (isFXTransaction ? (a.CurrencyCode != "IDR" && a.DebitCurrencyCode == "IDR").Equals(filter.Transaction.IsFXTransaction.Value) : true) &&
        //                           (isTopUrgent ? a.IsTopUrgent.Equals(filter.Transaction.IsTopUrgent.Value) : true) &&
        //                           (isDebitAccNumber ? a.AccountNumber.Contains(filter.Transaction.DebitAccNumber) : true) &&
        //                           (isActivityTitle ? a.WorkflowTaskActivityTitle.Contains(filter.CurrentTask.ActivityTitle) : true)
        //                           #endregion

        //                           select new
        //                           {
        //                               #region Workflow Context
        //                               SPWebID = a.SPWebID,
        //                               SPSiteID = a.SPSiteID,
        //                               SPListID = a.SPListID,
        //                               SPListItemID = a.SPListItemID,
        //                               SPTaskListID = a.SPTaskListID,
        //                               SPTaskListItemID = a.SPTaskListItemID,
        //                               ApproverID = a.WorkflowApproverID,
        //                               Initiator = a.WorkflowInitiator,
        //                               InitiatorDisplayName = a.UserDisplayNameInitiator,
        //                               StartTime = a.WorkflowStartTime,
        //                               StateID = a.WorkflowState,
        //                               StateDescription = a.WorkflowStateDescription,
        //                               TransactionStatus = a.TransactionStatus,
        //                               //StateDescription = a.WorkflowStateDescription.ToLower() == "running" ? a.WorkflowTaskActivityTitle : a.WorkflowStateDescription.ToLower() == "complete" ? ((a.ApplicationID.ToLower().StartsWith("fx") && a.WorkflowOutcomeDescription.ToLower() == "rejected") || a.WorkflowCustomOutcomeDescription.ToLower() == "approve cancellation" ? "Cancelled"  : "Completed") : a.WorkflowStateDescription.Trim(),
        //                               WorkflowID = a.WorkflowID,
        //                               WorkflowName = a.WorkflowName,
        //                               WorkflowInstanceID = a.WorkflowInstanceID,
        //                               IsAuthoriaed = a.WorkflowTaskIsSPGroup ? (roles.Contains(a.WorkflowTaskUser) ? true : false) : (a.WorkflowTaskUser.Equals(userName) ? true : false),
        //                               #endregion

        //                               #region Workflow Task
        //                               TaskType = a.WorkflowTaskType,
        //                               TaskTypeDescription = a.WorkflowTaskTypeDescription,
        //                               Title = a.WorkflowTaskTitle,
        //                               ActivityTitle = a.WorkflowTaskActivityTitle,
        //                               User = a.WorkflowTaskUser,
        //                               UserDisplayName = a.UserDisplayNameTask,
        //                               IsSPGroup = a.WorkflowTaskIsSPGroup,
        //                               EntryTime = a.WorkflowTaskEntryTime,
        //                               ExitTime = a.WorkflowTaskExitTime != null ? a.WorkflowTaskExitTime : DateTime.Now,
        //                               OutcomeID = a.WorkflowOutcome,
        //                               OutcomeDescription = a.WorkflowOutcomeDescription,
        //                               CustomOutcomeID = a.WorkflowCustomOutcome,
        //                               CustomOutcomeDescription = a.WorkflowCustomOutcomeDescription != null ? a.WorkflowCustomOutcomeDescription : "",
        //                               Comments = a.WorkflowComments,
        //                               Approver = a.WorkflowDisplayName != null ? a.WorkflowDisplayName : "",
        //                               #endregion

        //                               #region Workflow Task Latest
        //                               ActivityTitleLatest = a.WorkflowLastActivityTitle,
        //                               CommentsLatest = a.WorkflowLastComments,
        //                               CustomOutcomeIDLatest = a.WorkflowLastCustomOutcome != null ? a.WorkflowLastCustomOutcome : 0,
        //                               CustomOutcomeDescriptionLatest = a.WorkflowLastCustomOutcomeDescription != null ? a.WorkflowLastCustomOutcomeDescription : "",
        //                               UserDisplayNameLatest = a.WorkflowLastDisplayName,
        //                               OutcomeIDLatest = a.WorkflowLastOutcome,
        //                               OutcomeDescriptionLatest = a.WorkflowLastOutcomeDescription,
        //                               IsSPGroupLatest = a.WorkflowLastTaskIsSPGroup,
        //                               UserLatest = a.WorkflowLastTaskUser,
        //                               //EntryTimeLatest = a.WorkflowLastTaskEntryTime,
        //                               EntryTimeLatest = a.WorkflowLastTaskExitTime,
        //                               ExitTimeLatest = a.WorkflowLastTaskExitTime,
        //                               #endregion

        //                               #region Transaction
        //                               ID = a.TransactionID,
        //                               ApplicationID = a.ApplicationID,
        //                               AccountNumber = a.AccountNumber,
        //                               Customer = a.CustomerName,
        //                               Product = a.ProductName,
        //                               Currency = a.CurrencyCode,
        //                               Amount = a.Amount,
        //                               AmountUSD = a.AmountUSD,
        //                               IsTopUrgent = a.IsTopUrgent,
        //                               IsFXTransaction = (a.CurrencyCode != "IDR" && a.DebitCurrencyCode == "IDR") ? true : false,
        //                               LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
        //                               LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,

        //                               ProgressApprover = string.IsNullOrEmpty(a.WorkflowLastDisplayName) ? a.UpdateDate == null ? a.CreateBy : a.UpdateBy : a.WorkflowLastDisplayName,
        //                               //CurrentTaskUser = a.WorkflowTaskIsSPGroup ? a.WorkflowTaskUser : a.UserDisplayNameTask,
        //                               CurrentTaskUser = a.WorkflowLastDisplayName,
        //                               ProgressExitTime = a.ProgressExitTime
        //                               //ProgressExitTime = a.WorkflowLastTaskExitTime.HasValue ? a.WorkflowLastTaskExitTime : a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
        //                               #endregion

        //                           });

        //                   userTaskGrid.Page = page.Value;
        //                   userTaskGrid.Size = size.Value;
        //                   userTaskGrid.Total = data.Count();
        //                   userTaskGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };

        //                   /* sortColumn = sortColumn.Equals("UserDisplayName") ? 
        //                        sortColumn = "CurrentTaskUser" : sortColumn.Equals("Approver") ? 
        //                        sortColumn = "ProgressApprover" : 
        //                        (sortColumn.Equals("ExitTime") ? sortColumn = "ProgressExitTime" : sortColumn); */



        //                   userTaskGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
        //                   .Select((a, i) => new SPUserTaskRow()
        //                   {
        //                       RowID = 1 + i,

        //                       #region Workflow Context
        //                       WorkflowContext = new WorkflowContext()
        //                       {
        //                           SPWebID = a.SPWebID,
        //                           SPSiteID = a.SPSiteID,
        //                           SPListID = a.SPListID,
        //                           SPListItemID = a.SPListItemID,
        //                           SPTaskListID = a.SPTaskListID.Value,
        //                           SPTaskListItemID = a.SPTaskListItemID.Value,
        //                           Initiator = a.InitiatorDisplayName,
        //                           ApproverID = a.ApproverID,
        //                           StartTime = a.StartTime,
        //                           StateID = a.StateID.Value,
        //                           StateDescription = a.StateDescription,
        //                           WorkflowInstanceID = a.WorkflowInstanceID,
        //                           WorkflowID = a.WorkflowID.Value,
        //                           WorkflowName = a.WorkflowName,
        //                           IsAuthorized = a.IsAuthoriaed
        //                       },
        //                       #endregion

        //                       #region Workflow Task
        //                       CurrentTask = new WorkflowTask()
        //                       {
        //                           TaskTypeID = a.TaskType.Value,
        //                           TaskTypeDescription = a.TaskTypeDescription,
        //                           Title = a.Title,
        //                           ActivityTitle = a.ActivityTitle,
        //                           User = a.CurrentTaskUser,
        //                           IsSPGroup = a.IsSPGroup,
        //                           EntryTime = a.EntryTime,
        //                           ExitTime = a.ExitTime,
        //                           OutcomeID = a.OutcomeID,
        //                           OutcomeDescription = a.OutcomeDescription,
        //                           CustomOutcomeID = a.CustomOutcomeID,
        //                           CustomOutcomeDescription = a.CustomOutcomeDescription,
        //                           Comments = a.Comments,
        //                           Approver = a.ProgressApprover
        //                       },
        //                       #endregion

        //                       #region Workflow Task Progress
        //                       ProgressTask = new WorkflowTask()
        //                       {
        //                           ActivityTitle = a.ActivityTitleLatest,
        //                           User = a.IsSPGroupLatest.HasValue ? (a.IsSPGroupLatest.Value ? a.UserLatest : a.UserDisplayNameLatest) : a.LastModifiedBy,
        //                           IsSPGroup = a.IsSPGroupLatest.HasValue ? a.IsSPGroupLatest.Value : false,
        //                           EntryTime = a.EntryTimeLatest.HasValue ? a.EntryTimeLatest : a.LastModifiedDate,
        //                           ExitTime = a.ProgressExitTime,
        //                           OutcomeID = a.OutcomeIDLatest,
        //                           OutcomeDescription = string.IsNullOrEmpty(a.OutcomeDescriptionLatest) ? "Submit Transaction" : a.OutcomeDescriptionLatest,
        //                           CustomOutcomeID = a.CustomOutcomeIDLatest,
        //                           CustomOutcomeDescription = a.CustomOutcomeDescriptionLatest,
        //                           Comments = a.CommentsLatest,
        //                           Approver = a.ProgressApprover
        //                       },
        //                       #endregion

        //                       #region Transaction
        //                       Transaction = new TransactionTaskModel()
        //                       {
        //                           ID = a.ID,
        //                           ApplicationID = a.ApplicationID,
        //                           Customer = a.Customer,
        //                           Product = a.Product,
        //                           Currency = a.Currency,
        //                           Amount = a.Amount,
        //                           AmountUSD = a.AmountUSD,
        //                           IsTopUrgent = a.IsTopUrgent,
        //                           IsFXTransaction = a.IsFXTransaction,
        //                           TransactionStatus = a.TransactionStatus,
        //                           LastModifiedBy = a.LastModifiedBy,
        //                           LastModifiedDate = a.LastModifiedDate,
        //                           DebitAccNumber = a.AccountNumber
        //                       }
        //                       #endregion

        //                   }).Skip(skip).Take(take).ToList();


        //                   isSuccess = true;
        //               }
        //               catch (Exception ex)
        //               {
        //                   message = ex.InnerException.Message;
        //               }
        //           }

        //           return isSuccess;

        //       }

        //       public bool GetAllTransactionUserTasks(Guid webID, Guid siteID, IList<Guid> workflowIDs, IList<string> state, IList<string> outcome, IList<string> customOutcome, bool showContribute, bool showActiveTask, int? page, int? size, string sortColumn, string sortOrder, IList<SPUserTaskFilter> filters, ref SPUserTaskGrid userTaskGrid, ref string message)
        //       {
        //           bool isSuccess = false;

        //           using (DBSEntities context = new DBSEntities())
        //           {
        //               try
        //               {
        //                   int skip = (page.Value - 1) * size.Value;
        //                   int take = size.Value;

        //                   SPUserTask filter = new SPUserTask();
        //                   filter.WorkflowContext = new WorkflowContext();
        //                   filter.CurrentTask = new WorkflowTask();
        //                   filter.ProgressTask = new WorkflowTask();
        //                   filter.Transaction = new TransactionTaskModel();
        //                   filter.Transaction.IsFXTransaction = null;

        //                   DateTime maxDateEntryTime = new DateTime();
        //                   DateTime maxDateExitTime = new DateTime();

        //                   if (filters != null)
        //                   {
        //                       #region Transaction
        //                       filter.Transaction.ApplicationID = filters.Where(sp => sp.Field.Equals("ApplicationID")).Select(sp => sp.Value).SingleOrDefault();
        //                       filter.Transaction.Customer = filters.Where(sp => sp.Field.Equals("Customer")).Select(sp => sp.Value).SingleOrDefault();
        //                       filter.Transaction.Product = filters.Where(sp => sp.Field.Equals("Product")).Select(sp => sp.Value).SingleOrDefault();
        //                       filter.Transaction.Currency = filters.Where(sp => sp.Field.Equals("Currency")).Select(sp => sp.Value).SingleOrDefault();
        //                       filter.Transaction.DebitAccNumber = (string)filters.Where(sp => sp.Field.Equals("DebitAccNumber")).Select(sp => sp.Value).SingleOrDefault();
        //                       filter.CurrentTask.ActivityTitle = filters.Where(sp => sp.Field.Equals("ActivityTitle")).Select(sp => sp.Value).SingleOrDefault();

        //                       string amount = filters.Where(sp => sp.Field.Equals("Amount")).Select(sp => sp.Value).SingleOrDefault();
        //                       if (!string.IsNullOrEmpty(amount))
        //                       {
        //                           decimal valueAmount;
        //                           bool resultAmount = decimal.TryParse(amount, out valueAmount);
        //                           if (resultAmount)
        //                           {
        //                               filter.Transaction.Amount = valueAmount;
        //                           }
        //                           else
        //                           {
        //                               userTaskGrid.Page = page.Value;
        //                               userTaskGrid.Size = size.Value;
        //                               userTaskGrid.Total = 0;
        //                               return true;
        //                           }
        //                       }

        //                       string amountUSD = filters.Where(sp => sp.Field.Equals("AmountUSD")).Select(sp => sp.Value).SingleOrDefault();
        //                       if (!string.IsNullOrEmpty(amountUSD))
        //                       {
        //                           filter.Transaction.AmountUSD = Decimal.Parse(amountUSD);
        //                       }

        //                       string fxTransaction = filters.Where(sp => sp.Field.Equals("IsFXTransaction")).Select(sp => sp.Value).SingleOrDefault();
        //                       if (!string.IsNullOrEmpty(fxTransaction))
        //                       {
        //                           bool valueFx;
        //                           bool resultFx = bool.TryParse(fxTransaction, out valueFx);
        //                           if (resultFx)
        //                           {
        //                               filter.Transaction.IsFXTransaction = valueFx;//bool.Parse(fxTransaction);
        //                           }
        //                           else
        //                           {
        //                               userTaskGrid.Page = page.Value;
        //                               userTaskGrid.Size = size.Value;
        //                               userTaskGrid.Total = 0;
        //                               return true;
        //                           }
        //                       }

        //                       string topUrgent = filters.Where(sp => sp.Field.Equals("IsTopUrgent")).Select(sp => sp.Value).SingleOrDefault();


        //                       filter.CurrentTask.User = filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
        //                       filter.ProgressTask.User = filters.Where(a => a.Field.Equals("UserApprover")).Select(a => a.Value).SingleOrDefault();

        //                       if (filters.Where(a => a.Field.Equals("EntryTime")).Any())
        //                       {
        //                           filter.CurrentTask.EntryTime = DateTime.Parse((string)filters.Where(a => a.Field.Equals("EntryTime")).Select(a => a.Value).SingleOrDefault());
        //                           maxDateEntryTime = filter.CurrentTask.EntryTime.Value.AddDays(1);
        //                       }

        //                       if (filters.Where(a => a.Field.Equals("ExitTime")).Any())
        //                       {
        //                           filter.CurrentTask.ExitTime = DateTime.Parse(filters.Where(a => a.Field.Equals("ExitTime")).Select(a => a.Value).SingleOrDefault());
        //                           maxDateExitTime = filter.CurrentTask.ExitTime.Value.AddDays(1);
        //                       }
        //                       #endregion
        //                   }
        //                   string orderBy = string.Format("{0} {1}", sortColumn, sortOrder);
        //                   string userName = currentUser.GetCurrentUser().LoginName;
        //                   string[] roles = currentUser.GetCurrentUser().Roles.Select(r => r.Name).ToArray();
        //                   Guid[] workflowID = workflowIDs.ToArray();


        //                   var data = (
        //                           from a in context.V_TaskApprovals

        //                           let isActivityTitle = !string.IsNullOrEmpty(filter.CurrentTask.ActivityTitle)
        //                           let isUser = !string.IsNullOrEmpty(filter.CurrentTask.User)
        //                           let isEntryTime = filter.CurrentTask.EntryTime.HasValue
        //                           let isExitTime = filter.CurrentTask.ExitTime.HasValue
        //                           let isUserApprover = !string.IsNullOrEmpty(filter.ProgressTask.User)

        //                           #region Transaction
        //                           let isApplicationID = !string.IsNullOrEmpty(filter.Transaction.ApplicationID)
        //                           let isCustomer = !string.IsNullOrEmpty(filter.Transaction.Customer)
        //                           let isProduct = !string.IsNullOrEmpty(filter.Transaction.Product)
        //                           let isCurrency = !string.IsNullOrEmpty(filter.Transaction.Currency)
        //                           let isAmount = filter.Transaction.Amount.HasValue
        //                           let isAmountUSD = filter.Transaction.AmountUSD.HasValue
        //                           let isFXTransaction = filter.Transaction.IsFXTransaction.HasValue
        //                           let isTopUrgent = filter.Transaction.IsTopUrgent.HasValue
        //                           let isDebitAccNumber = !string.IsNullOrEmpty(filter.Transaction.DebitAccNumber)
        //                           #endregion

        //                           where

        //                           a.SPWebID.Equals(webID) && a.SPSiteID.Equals(siteID) && // y
        //                           workflowIDs.Contains(a.WorkflowID.Value) && // y
        //                           (showContribute ? roles.Any(x => a.WorkflowTaskUserContribute.Contains(x)) || roles.Any(x => a.InitiatorGroup.Contains(x)) || a.WorkflowInitiator.Equals(userName) : true) && // y
        //                           (!showContribute && showActiveTask ? (a.WorkflowOutcomeDescription.ToLower().Equals("pending"))  // y
        //                           && (roles.Contains(a.WorkflowTaskUser.ToLower()) || a.WorkflowTaskUser.Equals(userName)) // y
        //                           && (a.IsCanceled == false) : true) && // y

        //                           (state.Count() > 0 ? state.Contains(a.WorkflowStateDescription.ToLower()) : true) && // y
        //                           (customOutcome.Count() > 0 ? (a.WorkflowOutcomeDescription.ToLower().Equals("custom") ? customOutcome.Contains(a.WorkflowCustomOutcomeDescription) : true) : true) &&
        //                           (isActivityTitle ? a.WorkflowTaskActivityTitle.Contains(filter.CurrentTask.ActivityTitle) : true) &&
        //                           (isUser ? (a.WorkflowTaskIsSPGroup ? a.WorkflowTaskUser : a.UserDisplayNameTask).Contains(filter.CurrentTask.User) : true) &&
        //                           (isUserApprover ? (string.IsNullOrEmpty(a.WorkflowLastDisplayName) ? (a.UpdateDate == null ? a.CreateBy : a.UpdateBy) : a.WorkflowLastDisplayName).Contains(filter.ProgressTask.User) : true) &&
        //                           (isEntryTime ? (a.WorkflowTaskEntryTime >= filter.CurrentTask.EntryTime.Value && a.WorkflowTaskEntryTime <= maxDateEntryTime) : true) &&
        //                           (isExitTime ? (a.WorkflowTaskExitTime.Value >= filter.CurrentTask.ExitTime.Value && a.WorkflowTaskExitTime.Value <= maxDateExitTime) : true) &&

        //                           #region Transaction
        //(isApplicationID ? a.ApplicationID.ToLower().Contains(filter.Transaction.ApplicationID) : true) &&
        //                           (isCustomer ? a.CustomerName.ToLower().Contains(filter.Transaction.Customer.ToLower()) : true) &&
        //                           (isProduct ? a.ProductName.Contains(filter.Transaction.Product) : true) &&
        //                           (isCurrency ? a.CurrencyCode.Contains(filter.Transaction.Currency) : true) &&
        //                           (isAmount ? a.Amount == filter.Transaction.Amount.Value : true) &&
        //                           (isAmountUSD ? a.AmountUSD == filter.Transaction.AmountUSD.Value : true) &&
        //                           (isFXTransaction ? (a.CurrencyCode != "IDR" && a.DebitCurrencyCode == "IDR").Equals(filter.Transaction.IsFXTransaction.Value) : true) &&
        //                           (isTopUrgent ? a.IsTopUrgent.Equals(filter.Transaction.IsTopUrgent.Value) : true) &&
        //                           (isDebitAccNumber ? a.AccountNumber.Contains(filter.Transaction.DebitAccNumber) : true) &&
        //                           (isActivityTitle ? a.WorkflowTaskActivityTitle.Contains(filter.CurrentTask.ActivityTitle) : true)
        //                           #endregion

        //                           select new
        //                           {
        //                               #region Workflow Context
        //                               SPWebID = a.SPWebID,
        //                               SPSiteID = a.SPSiteID,
        //                               SPListID = a.SPListID,
        //                               SPListItemID = a.SPListItemID,
        //                               SPTaskListID = a.SPTaskListID,
        //                               SPTaskListItemID = a.SPTaskListItemID,
        //                               ApproverID = a.WorkflowApproverID,
        //                               Initiator = a.WorkflowInitiator,
        //                               InitiatorDisplayName = a.UserDisplayNameInitiator,
        //                               StartTime = a.WorkflowStartTime,
        //                               StateID = a.WorkflowState,
        //                               StateDescription = a.WorkflowStateDescription,
        //                               TransactionStatus = a.TransactionStatus,
        //                               //StateDescription = a.WorkflowStateDescription.ToLower() == "running" ? a.WorkflowTaskActivityTitle : a.WorkflowStateDescription.ToLower() == "complete" ? ((a.ApplicationID.ToLower().StartsWith("fx") && a.WorkflowOutcomeDescription.ToLower() == "rejected") || a.WorkflowCustomOutcomeDescription.ToLower() == "approve cancellation" ? "Cancelled"  : "Completed") : a.WorkflowStateDescription.Trim(),
        //                               WorkflowID = a.WorkflowID,
        //                               WorkflowName = a.WorkflowName,
        //                               WorkflowInstanceID = a.WorkflowInstanceID,
        //                               IsAuthoriaed = a.WorkflowTaskIsSPGroup ? (roles.Contains(a.WorkflowTaskUser) ? true : false) : (a.WorkflowTaskUser.Equals(userName) ? true : false),
        //                               #endregion

        //                               #region Workflow Task
        //                               TaskType = a.WorkflowTaskType,
        //                               TaskTypeDescription = a.WorkflowTaskTypeDescription,
        //                               Title = a.WorkflowTaskTitle,
        //                               ActivityTitle = a.WorkflowTaskActivityTitle,
        //                               User = a.WorkflowTaskUser,
        //                               UserDisplayName = a.UserDisplayNameTask,
        //                               IsSPGroup = a.WorkflowTaskIsSPGroup,
        //                               EntryTime = a.WorkflowTaskEntryTime,
        //                               ExitTime = a.WorkflowTaskExitTime != null ? a.WorkflowTaskExitTime : DateTime.Now,
        //                               OutcomeID = a.WorkflowOutcome,
        //                               OutcomeDescription = a.WorkflowOutcomeDescription,
        //                               CustomOutcomeID = a.WorkflowCustomOutcome,
        //                               CustomOutcomeDescription = a.WorkflowCustomOutcomeDescription != null ? a.WorkflowCustomOutcomeDescription : "",
        //                               Comments = a.WorkflowComments,
        //                               Approver = a.WorkflowDisplayName != null ? a.WorkflowDisplayName : "",
        //                               #endregion

        //                               #region Workflow Task Latest
        //                               ActivityTitleLatest = a.WorkflowLastActivityTitle,
        //                               CommentsLatest = a.WorkflowLastComments,
        //                               CustomOutcomeIDLatest = a.WorkflowLastCustomOutcome != null ? a.WorkflowLastCustomOutcome : 0,
        //                               CustomOutcomeDescriptionLatest = a.WorkflowLastCustomOutcomeDescription != null ? a.WorkflowLastCustomOutcomeDescription : "",
        //                               UserDisplayNameLatest = a.WorkflowLastDisplayName,
        //                               OutcomeIDLatest = a.WorkflowLastOutcome,
        //                               OutcomeDescriptionLatest = a.WorkflowLastOutcomeDescription,
        //                               IsSPGroupLatest = a.WorkflowLastTaskIsSPGroup,
        //                               UserLatest = a.WorkflowLastTaskUser,
        //                               EntryTimeLatest = a.WorkflowLastTaskEntryTime,
        //                               ExitTimeLatest = a.WorkflowLastTaskExitTime,
        //                               #endregion

        //                               #region Transaction
        //                               ID = a.TransactionID,
        //                               ApplicationID = a.ApplicationID,
        //                               AccountNumber = a.AccountNumber,
        //                               Customer = a.CustomerName,
        //                               Product = a.ProductName,
        //                               Currency = a.CurrencyCode,
        //                               Amount = a.Amount,
        //                               AmountUSD = a.AmountUSD,
        //                               IsTopUrgent = a.IsTopUrgent,
        //                               IsFXTransaction = (a.CurrencyCode != "IDR" && a.DebitCurrencyCode == "IDR") ? true : false,
        //                               LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
        //                               LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,

        //                               ProgressApprover = string.IsNullOrEmpty(a.WorkflowLastDisplayName) ? a.UpdateDate == null ? a.CreateBy : a.UpdateBy : a.WorkflowLastDisplayName,
        //                               CurrentTaskUser = a.WorkflowTaskIsSPGroup ? a.WorkflowTaskUser : a.UserDisplayNameTask,
        //                               ProgressExitTime = a.WorkflowLastTaskExitTime.HasValue ? a.WorkflowLastTaskExitTime : a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
        //                               #endregion

        //                           });

        //                   userTaskGrid.Page = page.Value;
        //                   userTaskGrid.Size = size.Value;
        //                   userTaskGrid.Total = data.Count();
        //                   userTaskGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };

        //                   sortColumn = sortColumn.Equals("UserDisplayName") ?
        //                       sortColumn = "CurrentTaskUser" : sortColumn.Equals("Approver") ?
        //                       sortColumn = "ProgressApprover" : sortColumn.Equals("ExitTime") ?
        //                       sortColumn = "ProgressExitTime" : sortColumn;



        //                   userTaskGrid.Rows = data.OrderBy(orderBy).AsEnumerable()

        //                   .Select((a, i) => new SPUserTaskRow()
        //                   {
        //                       RowID = 1 + i,

        //                       #region Workflow Context
        //                       WorkflowContext = new WorkflowContext()
        //                       {
        //                           SPWebID = a.SPWebID,
        //                           SPSiteID = a.SPSiteID,
        //                           SPListID = a.SPListID,
        //                           SPListItemID = a.SPListItemID,
        //                           SPTaskListID = a.SPTaskListID.Value,
        //                           SPTaskListItemID = a.SPTaskListItemID.Value,
        //                           Initiator = a.InitiatorDisplayName,
        //                           ApproverID = a.ApproverID,
        //                           StartTime = a.StartTime,
        //                           StateID = a.StateID.Value,
        //                           StateDescription = a.StateDescription,
        //                           WorkflowInstanceID = a.WorkflowInstanceID,
        //                           WorkflowID = a.WorkflowID.Value,
        //                           WorkflowName = a.WorkflowName,
        //                           IsAuthorized = a.IsAuthoriaed
        //                       },
        //                       #endregion

        //                       #region Workflow Task
        //                       CurrentTask = new WorkflowTask()
        //                       {
        //                           TaskTypeID = a.TaskType.Value,
        //                           TaskTypeDescription = a.TaskTypeDescription,
        //                           Title = a.Title,
        //                           ActivityTitle = a.ActivityTitle,
        //                           User = a.CurrentTaskUser,
        //                           IsSPGroup = a.IsSPGroup,
        //                           EntryTime = a.EntryTime,
        //                           ExitTime = a.ExitTime,
        //                           OutcomeID = a.OutcomeID,
        //                           OutcomeDescription = a.OutcomeDescription,
        //                           CustomOutcomeID = a.CustomOutcomeID,
        //                           CustomOutcomeDescription = a.CustomOutcomeDescription,
        //                           Comments = a.Comments,
        //                           Approver = a.ProgressApprover
        //                       },
        //                       #endregion

        //                       #region Workflow Task Progress
        //                       ProgressTask = new WorkflowTask()
        //                       {
        //                           ActivityTitle = a.ActivityTitleLatest,
        //                           User = a.IsSPGroupLatest.HasValue ? (a.IsSPGroupLatest.Value ? a.UserLatest : a.UserDisplayNameLatest) : a.LastModifiedBy,
        //                           IsSPGroup = a.IsSPGroupLatest.HasValue ? a.IsSPGroupLatest.Value : false,
        //                           EntryTime = a.EntryTimeLatest.HasValue ? a.EntryTimeLatest : a.LastModifiedDate,
        //                           ExitTime = a.ProgressExitTime,
        //                           OutcomeID = a.OutcomeIDLatest,
        //                           OutcomeDescription = string.IsNullOrEmpty(a.OutcomeDescriptionLatest) ? "Submit Transaction" : a.OutcomeDescriptionLatest,
        //                           CustomOutcomeID = a.CustomOutcomeIDLatest,
        //                           CustomOutcomeDescription = a.CustomOutcomeDescriptionLatest,
        //                           Comments = a.CommentsLatest,
        //                           Approver = a.ProgressApprover
        //                       },
        //                       #endregion

        //                       #region Transaction
        //                       Transaction = new TransactionTaskModel()
        //                       {
        //                           ID = a.ID,
        //                           ApplicationID = a.ApplicationID,
        //                           Customer = a.Customer,
        //                           Product = a.Product,
        //                           Currency = a.Currency,
        //                           Amount = a.Amount,
        //                           AmountUSD = a.AmountUSD,
        //                           IsTopUrgent = a.IsTopUrgent,
        //                           IsFXTransaction = a.IsFXTransaction,
        //                           TransactionStatus = a.TransactionStatus,
        //                           LastModifiedBy = a.LastModifiedBy,
        //                           LastModifiedDate = a.LastModifiedDate,
        //                           DebitAccNumber = a.AccountNumber
        //                       }
        //                       #endregion

        //                   }).Skip(skip).Take(take).ToList();


        //                   isSuccess = true;
        //               }
        //               catch (Exception ex)
        //               {
        //                   message = ex.InnerException.Message;
        //               }
        //           }

        //           return isSuccess;
        //       }

        //public bool GetAllTransactionSPUserTasks(Guid webID, Guid siteID, IList<Guid> workflowIDs, IList<string> state, IList<string> outcome, IList<string> customOutcome, bool showContribute, bool showActiveTask, int? page, int? size, string sortColumn, string sortOrder, IList<SPUserTaskFilter> filters, ref SPUserTaskGrid userTaskGrid, ref string message)
        //{
        //    bool isSuccess = false;

        //    using (DBSEntities context = new DBSEntities())
        //    {
        //        try
        //        {
        //            int skip = (page.Value - 1) * size.Value;
        //            int take = size.Value;

        //            SPUserTask filter = new SPUserTask();
        //            filter.WorkflowContext = new WorkflowContext();
        //            filter.CurrentTask = new WorkflowTask();
        //            filter.ProgressTask = new WorkflowTask();
        //            filter.Transaction = new TransactionTaskModel();
        //            filter.Transaction.IsFXTransaction = null;

        //            DateTime maxDateEntryTime = new DateTime();
        //            DateTime maxDateExitTime = new DateTime();

        //            if (filters != null)
        //            {
        //                #region Transaction
        //                filter.Transaction.ApplicationID = filters.Where(sp => sp.Field.Equals("ApplicationID")).Select(sp => sp.Value).SingleOrDefault();
        //                filter.Transaction.Customer = filters.Where(sp => sp.Field.Equals("Customer")).Select(sp => sp.Value).SingleOrDefault();
        //                filter.Transaction.Product = filters.Where(sp => sp.Field.Equals("Product")).Select(sp => sp.Value).SingleOrDefault();
        //                filter.Transaction.Currency = filters.Where(sp => sp.Field.Equals("Currency")).Select(sp => sp.Value).SingleOrDefault();
        //                filter.Transaction.DebitAccNumber = (string)filters.Where(sp => sp.Field.Equals("DebitAccNumber")).Select(sp => sp.Value).SingleOrDefault();
        //                //filter.CurrentTask.ActivityTitle = filters.Where(sp => sp.Field.Equals("ActivityTitle")).Select(sp => sp.Value).SingleOrDefault();

        //                string amount = filters.Where(sp => sp.Field.Equals("Amount")).Select(sp => sp.Value).SingleOrDefault();
        //                if (!string.IsNullOrEmpty(amount))
        //                {
        //                    decimal valueAmount;
        //                    bool resultAmount = decimal.TryParse(amount, out valueAmount);
        //                    if (resultAmount)
        //                    {
        //                        filter.Transaction.Amount = valueAmount;
        //                    }
        //                    else
        //                    {
        //                        userTaskGrid.Page = page.Value;
        //                        userTaskGrid.Size = size.Value;
        //                        userTaskGrid.Total = 0;
        //                        return true;
        //                    }
        //                }

        //                string amountUSD = filters.Where(sp => sp.Field.Equals("AmountUSD")).Select(sp => sp.Value).SingleOrDefault();
        //                if (!string.IsNullOrEmpty(amountUSD))
        //                {
        //                    filter.Transaction.AmountUSD = Decimal.Parse(amountUSD);
        //                }

        //                filter.Transaction.IsFXTransactionValue = filters.Where(sp => sp.Field.Equals("IsFXTransaction")).Select(sp => sp.Value).SingleOrDefault();
        //                filter.Transaction.IsTopUrgentValue = filters.Where(sp => sp.Field.Equals("IsTopUrgent")).Select(sp => sp.Value).SingleOrDefault();
        //                filter.Transaction.TransactionStatus = filters.Where(sp => sp.Field.Equals("TransactionStatus")).Select(sp => sp.Value).SingleOrDefault();

        //                filter.CurrentTask.User = filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
        //                filter.ProgressTask.User = filters.Where(a => a.Field.Equals("UserApprover")).Select(a => a.Value).SingleOrDefault();
        //                filter.CurrentTask.TaskTypeDescription = filters.Where(a => a.Field.Equals("EntryTime")).Select(a => a.Value).SingleOrDefault();
        //                //if (filters.Where(a => a.Field.Equals("EntryTime")).Any())
        //                //{
        //                //    filter.CurrentTask.EntryTime = DateTime.Parse((string)filters.Where(a => a.Field.Equals("EntryTime")).Select(a => a.Value).SingleOrDefault());
        //                //    maxDateEntryTime = filter.CurrentTask.EntryTime.Value.AddDays(1);
        //                //}

        //                if (filters.Where(a => a.Field.Equals("ExitTime")).Any())
        //                {
        //                    filter.CurrentTask.ExitTime = DateTime.Parse(filters.Where(a => a.Field.Equals("ExitTime")).Select(a => a.Value).SingleOrDefault());
        //                    maxDateExitTime = filter.CurrentTask.ExitTime.Value.AddDays(1);
        //                }
        //                #endregion
        //            }
        //            string orderBy = string.Format("{0} {1}", sortColumn, sortOrder);
        //            string[] roles = currentUser.GetCurrentUser().Roles.Select(r => r.Name).ToArray();
        //            Guid[] workflowID = workflowIDs.ToArray();

        //            #region parameter
        //            string Username = currentUser.GetCurrentUser().LoginName;
        //            string ApplicationID = filter.Transaction.ApplicationID;
        //            string CustomerName = filter.Transaction.Customer;
        //            string ProductName = filter.Transaction.Product;
        //            string CurrencyCode = filter.Transaction.Currency;
        //            Decimal? TransactionAmount = filter.Transaction.Amount;
        //            string DebitAccNumber = filter.Transaction.DebitAccNumber;
        //            string IsFXTransaction = filter.Transaction.IsFXTransactionValue;
        //            string Urgency = filter.Transaction.IsTopUrgentValue;
        //            string ModifiedBy = filter.CurrentTask.User;
        //            string TransactionStatus = filter.Transaction.TransactionStatus;
        //            string modifiedDate = filter.CurrentTask.TaskTypeDescription;
        //            #endregion

        //            var data = (
        //                    from a in context.SP_GetTaskAll(Username, ApplicationID, CustomerName, ProductName, CurrencyCode, TransactionAmount, DebitAccNumber, IsFXTransaction, Urgency, TransactionStatus, ModifiedBy, modifiedDate)
        //                    select new
        //                    {
        //                        #region Workflow Context
        //                        SPWebID = a.SPWebID,
        //                        SPSiteID = a.SPSiteID,
        //                        SPListID = a.SPListID,
        //                        SPListItemID = a.SPListItemID,
        //                        SPTaskListID = a.SPTaskListID,
        //                        SPTaskListItemID = a.SPTaskListItemID,
        //                        ApproverID = a.WorkflowApproverID,
        //                        Initiator = a.WorkflowInitiator,
        //                        InitiatorDisplayName = a.UserDisplayNameInitiator,
        //                        StartTime = a.WorkflowStartTime,
        //                        StateID = a.WorkflowState,
        //                        StateDescription = a.WorkflowStateDescription,
        //                        //StateDescription = a.WorkflowStateDescription.ToLower() == "running" ? a.WorkflowTaskActivityTitle : a.WorkflowStateDescription.ToLower() == "complete" ? ((a.ApplicationID.ToLower().StartsWith("fx") && a.WorkflowOutcomeDescription.ToLower() == "rejected") || a.WorkflowCustomOutcomeDescription.ToLower() == "approve cancellation" ? "Cancelled"  : "Completed") : a.WorkflowStateDescription.Trim(),
        //                        WorkflowID = a.WorkflowID,
        //                        WorkflowName = a.WorkflowName,
        //                        WorkflowInstanceID = a.WorkflowInstanceID,
        //                        IsAuthoriaed = a.WorkflowTaskIsSPGroup ? (roles.Contains(a.WorkflowTaskUser) ? true : false) : (a.WorkflowTaskUser.Equals(Username) ? true : false),
        //                        #endregion

        //                        #region Workflow Task
        //                        TaskType = a.WorkflowTaskType,
        //                        TaskTypeDescription = a.WorkflowTaskTypeDescription,
        //                        Title = a.WorkflowTaskTitle,
        //                        ActivityTitle = a.WorkflowTaskActivityTitle,
        //                        User = a.WorkflowTaskUser,
        //                        UserDisplayName = a.UserDisplayNameTask,
        //                        IsSPGroup = a.WorkflowTaskIsSPGroup,
        //                        //EntryTime = a.WorkflowTaskEntryTime,
        //                        EntryTime = a.WorkflowLastTaskExitTime,
        //                        ExitTime = a.WorkflowTaskExitTime != null ? a.WorkflowTaskExitTime : DateTime.Now,
        //                        OutcomeID = a.WorkflowOutcome,
        //                        OutcomeDescription = a.WorkflowOutcomeDescription,
        //                        CustomOutcomeID = a.WorkflowCustomOutcome,
        //                        CustomOutcomeDescription = a.WorkflowCustomOutcomeDescription != null ? a.WorkflowCustomOutcomeDescription : "",
        //                        Comments = a.WorkflowComments,
        //                        Approver = a.WorkflowDisplayName != null ? a.WorkflowDisplayName : "",
        //                        #endregion

        //                        #region Workflow Task Latest
        //                        ActivityTitleLatest = a.WorkflowLastActivityTitle,
        //                        CommentsLatest = a.WorkflowLastComments,
        //                        CustomOutcomeIDLatest = a.WorkflowLastCustomOutcome != null ? a.WorkflowLastCustomOutcome : 0,
        //                        CustomOutcomeDescriptionLatest = a.WorkflowLastCustomOutcomeDescription != null ? a.WorkflowLastCustomOutcomeDescription : "",
        //                        UserDisplayNameLatest = a.WorkflowLastDisplayName,
        //                        OutcomeIDLatest = a.WorkflowLastOutcome,
        //                        OutcomeDescriptionLatest = a.WorkflowLastOutcomeDescription,
        //                        IsSPGroupLatest = a.WorkflowLastTaskIsSPGroup,
        //                        UserLatest = a.WorkflowLastTaskUser,
        //                        EntryTimeLatest = a.WorkflowLastTaskEntryTime,
        //                        ExitTimeLatest = a.WorkflowLastTaskExitTime,
        //                        #endregion

        //                        #region Transaction
        //                        ID = a.TransactionID,
        //                        ApplicationID = a.ApplicationID,
        //                        AccountNumber = a.AccountNumber,
        //                        Customer = a.CustomerName,
        //                        Product = a.ProductName,
        //                        Currency = a.CurrencyCode,
        //                        Amount = a.Amount,
        //                        AmountUSD = a.AmountUSD,
        //                        IsTopUrgent = a.IsTopUrgent,
        //                        TransactionStatus = a.WorkflowTaskActivityTitle,
        //                        IsFXTransaction = (a.CurrencyCode != "IDR" && a.DebitCurrencyCode == "IDR") ? true : false,
        //                        LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
        //                        LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,

        //                        ProgressApprover = string.IsNullOrEmpty(a.WorkflowLastDisplayName) ? a.UpdateDate == null ? a.CreateBy : a.UpdateBy : a.WorkflowLastDisplayName,
        //                        //CurrentTaskUser = a.WorkflowTaskIsSPGroup ? a.WorkflowTaskUser : a.UserDisplayNameTask,
        //                        CurrentTaskUser = a.WorkflowLastDisplayName,
        //                        //ProgressExitTime = a.WorkflowLastTaskExitTime.HasValue ? a.WorkflowLastTaskExitTime : a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
        //                        ProgressExitTime = a.WorkflowLastTaskExitTime,
        //                        ProductID = a.ProductID
        //                        #endregion

        //                    }).ToList();

        //            userTaskGrid.Page = page.Value;
        //            userTaskGrid.Size = size.Value;
        //            userTaskGrid.Total = data.Count();
        //            userTaskGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };

        //            sortColumn = sortColumn.Equals("UserDisplayName") ?
        //                sortColumn = "CurrentTaskUser" : sortColumn.Equals("Approver") ?
        //                sortColumn = "ProgressApprover" : sortColumn.Equals("ExitTime") ?
        //                sortColumn = "ProgressExitTime" : sortColumn;



        //            //userTaskGrid.Rows = data.OrderBy(orderBy).AsEnumerable().Select((a, i) => new SPUserTaskRow()
        //            userTaskGrid.Rows = data.AsQueryable().OrderBy(orderBy).Select((a, i) => new SPUserTaskRow()
        //            {
        //                RowID = 1 + i,

        //                #region Workflow Context
        //                WorkflowContext = new WorkflowContext()
        //                {
        //                    SPWebID = a.SPWebID,
        //                    SPSiteID = a.SPSiteID,
        //                    SPListID = a.SPListID,
        //                    SPListItemID = a.SPListItemID,
        //                    SPTaskListID = a.SPTaskListID.Value,
        //                    SPTaskListItemID = a.SPTaskListItemID.Value,
        //                    Initiator = a.InitiatorDisplayName,
        //                    ApproverID = a.ApproverID,
        //                    StartTime = a.StartTime,
        //                    StateID = a.StateID.Value,
        //                    StateDescription = a.StateDescription,
        //                    WorkflowInstanceID = a.WorkflowInstanceID,
        //                    WorkflowID = a.WorkflowID.Value,
        //                    WorkflowName = a.WorkflowName,
        //                    IsAuthorized = a.IsAuthoriaed
        //                },
        //                #endregion

        //                #region Workflow Task
        //                CurrentTask = new WorkflowTask()
        //                {
        //                    TaskTypeID = a.TaskType.Value,
        //                    TaskTypeDescription = a.TaskTypeDescription,
        //                    Title = a.Title,
        //                    ActivityTitle = a.ActivityTitle,
        //                    User = a.CurrentTaskUser,
        //                    IsSPGroup = a.IsSPGroup,
        //                    EntryTime = a.EntryTime,
        //                    ExitTime = a.ExitTime,
        //                    OutcomeID = a.OutcomeID,
        //                    OutcomeDescription = a.OutcomeDescription,
        //                    CustomOutcomeID = a.CustomOutcomeID,
        //                    CustomOutcomeDescription = a.CustomOutcomeDescription,
        //                    Comments = a.Comments,
        //                    Approver = a.ProgressApprover
        //                },
        //                #endregion

        //                #region Workflow Task Progress
        //                ProgressTask = new WorkflowTask()
        //                {
        //                    ActivityTitle = a.ActivityTitleLatest,
        //                    User = a.IsSPGroupLatest.HasValue ? (a.IsSPGroupLatest.Value ? a.UserLatest : a.UserDisplayNameLatest) : a.LastModifiedBy,
        //                    IsSPGroup = a.IsSPGroupLatest.HasValue ? a.IsSPGroupLatest.Value : false,
        //                    EntryTime = a.EntryTimeLatest.HasValue ? a.EntryTimeLatest : a.LastModifiedDate,
        //                    ExitTime = a.ProgressExitTime,
        //                    OutcomeID = a.OutcomeIDLatest,
        //                    OutcomeDescription = string.IsNullOrEmpty(a.OutcomeDescriptionLatest) ? "Submit Transaction" : a.OutcomeDescriptionLatest,
        //                    CustomOutcomeID = a.CustomOutcomeIDLatest,
        //                    CustomOutcomeDescription = a.CustomOutcomeDescriptionLatest,
        //                    Comments = a.CommentsLatest,
        //                    Approver = a.ProgressApprover
        //                },
        //                #endregion

        //                #region Transaction
        //                Transaction = new TransactionTaskModel()
        //                {
        //                    ID = a.ID,
        //                    ApplicationID = a.ApplicationID,
        //                    Customer = a.Customer,
        //                    Product = a.Product,
        //                    Currency = a.Currency,
        //                    Amount = a.Amount,
        //                    AmountUSD = a.AmountUSD,
        //                    IsTopUrgentValue = a.IsTopUrgent,// a.isTop,
        //                    IsFXTransaction = a.IsFXTransaction,
        //                    TransactionStatus = a.TransactionStatus,
        //                    LastModifiedBy = a.LastModifiedBy,
        //                    LastModifiedDate = a.LastModifiedDate,
        //                    DebitAccNumber = a.AccountNumber,
        //                    ProductID = (int)a.ProductID
        //                }
        //                #endregion

        //            }).Skip(skip).Take(take).ToList();


        //            isSuccess = true;
        //        }
        //        catch (Exception ex)
        //        {
        //            message = ex.InnerException.Message;
        //        }
        //    }

        //    return isSuccess;
        //}

        #endregion

        #region Home Tuning

        public bool GetHomeUserTasks(Guid webID, Guid siteID, IList<Guid> workflowIDs, IList<string> state, IList<string> outcome, IList<string> customOutcome, bool showContribute, bool showActiveTask, int? page, int? size, string sortColumn, string sortOrder, IList<SPUserTaskFilter> filters, ref SPUserTaskGrid userTaskGrid, ref string message)
        {
            bool isSuccess = false;

            using (DBSEntities context = new DBSEntities())
            {
                try
                {
                    int skip = (page.Value - 1) * size.Value;
                    int take = size.Value;

                    SPUserTask filter = new SPUserTask();
                    filter.WorkflowContext = new WorkflowContext();
                    filter.CurrentTask = new WorkflowTask();
                    filter.ProgressTask = new WorkflowTask();
                    filter.Transaction = new TransactionTaskModel();
                    filter.Transaction.IsFXTransaction = null;

                    if (filters != null)
                    {
                        filter.Transaction.BranchCode = filters.Where(sp => sp.Field.Equals("BranchCode")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.BranchName = filters.Where(sp => sp.Field.Equals("BranchName")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.ApplicationID = filters.Where(sp => sp.Field.Equals("ApplicationID")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.Customer = filters.Where(sp => sp.Field.Equals("Customer")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.Product = filters.Where(sp => sp.Field.Equals("Product")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.Currency = filters.Where(sp => sp.Field.Equals("Currency")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.DebitAccNumber = (string)filters.Where(sp => sp.Field.Equals("DebitAccNumber")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.IsFXTransactionValue = filters.Where(sp => sp.Field.Equals("IsFXTransaction")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.IsTopUrgentValue = filters.Where(sp => sp.Field.Equals("IsTopUrgent")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.TransactionStatus = filters.Where(sp => sp.Field.Equals("TransactionStatus")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.LastModifiedBy = filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                        string amount = filters.Where(sp => sp.Field.Equals("Amount")).Select(sp => sp.Value).SingleOrDefault();
                        if (!string.IsNullOrEmpty(amount))
                        {
                            decimal valueAmount;
                            bool resultAmount = decimal.TryParse(amount, out valueAmount);
                            if (resultAmount)
                            {
                                filter.Transaction.Amount = valueAmount;
                            }
                            else
                            {
                                userTaskGrid.Page = page.Value;
                                userTaskGrid.Size = size.Value;
                                userTaskGrid.Total = 0;
                                return true;
                            }
                        }

                        if (filters.Where(a => a.Field.Equals("EntryTime")).Any())
                        {
                            filter.Transaction.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("EntryTime")).Select(a => a.Value).SingleOrDefault());
                        }
                    }

                    var data = (from a in context.SP_GetTaskUser(currentUser.GetCurrentUser().LoginName,
                                                                filter.Transaction.ApplicationID,
                                                                filter.Transaction.Customer,
                                                                filter.Transaction.Product,
                                                                filter.Transaction.Currency,
                                                                filter.Transaction.Amount,
                                                                filter.Transaction.DebitAccNumber,
                                                                filter.Transaction.IsFXTransactionValue,
                                                                filter.Transaction.IsTopUrgentValue,
                                                                filter.Transaction.TransactionStatus,
                                                                filter.Transaction.BranchCode,
                                                                filter.Transaction.BranchName,
                                                                filter.Transaction.LastModifiedBy,
                                                                filter.Transaction.LastModifiedDate)
                                select new
                                {
                                    #region Workflow Context
                                    SPWebID = a.SPWebID,
                                    SPSiteID = a.SPSiteID,
                                    SPListID = a.SPListID,
                                    SPListItemID = a.SPListItemID,
                                    SPTaskListID = a.SPTaskListID,
                                    SPTaskListItemID = a.SPTaskListItemID,
                                    ApproverID = a.WorkflowApproverID,
                                    Initiator = a.WorkflowInitiator,
                                    WorkflowID = a.WorkflowID,
                                    WorkflowName = a.WorkflowName,
                                    WorkflowInstanceID = a.WorkflowInstanceID,
                                    WorkflowState = a.WorkflowState,
                                    #endregion

                                    #region Workflow Task
                                    TaskType = a.WorkflowTaskType,
                                    ActivityTitle = a.WorkflowTaskActivityTitle,
                                    OutcomeID = a.WorkflowOutcome,
                                    #endregion

                                    #region Transaction
                                    ID = a.TransactionID,
                                    ApplicationID = a.ApplicationID,
                                    AccountNumber = a.AccountNumber,
                                    Customer = a.CustomerName,
                                    Product = a.ProductName,
                                    Currency = a.CurrencyCode,
                                    Amount = a.Amount,
                                    AmountUSD = a.AmountUSD,
                                    IsTopUrgentValue = a.IsTopUrgent,
                                    IsTopUrgent = a.IsTopUrgent == "Top Urgent Chain" ? 2 : a.IsTopUrgent == "Top Urgent" ? 1 : 0,
                                    IsFXTransaction = (a.IsFXTransaction == "Yes") ? true : false,
                                    IsFXTransactionValue = a.IsFXTransaction,
                                    BranchCode = a.BranchCode,
                                    BranchName = a.BranchName,
                                    LastModifiedBy = a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate,
                                    ProductID = a.ProductID,
                                    TransactionStatus = a.TransactionStatus,
                                    CBOOrderNo = a.CBOOrderNo,
                                    Mode = a.Mode
                                    #endregion
                                }).ToList();

                    userTaskGrid.Page = page.Value;
                    userTaskGrid.Size = size.Value;
                    userTaskGrid.Total = data.Count();

                    userTaskGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };

                    string orderBy = string.Format("IsTopUrgent DESC,{0} {1}", sortColumn, sortOrder);

                    #region Set CBO Account order
                    bool isCBO = false;
                    isCBO = IsCBOAccount(currentUser.GetCurrentUser().LoginName);
                    if (isCBO)
                    {
                        orderBy = string.Format("CBOOrderNo ASC,IsTopUrgent DESC,{0} {1}", sortColumn, "ASC");
                    }
                    #endregion

                    userTaskGrid.Rows = data.AsQueryable().OrderBy(orderBy).Select((a, i) => new SPUserTaskRow()
                    {
                        RowID = i + 1,

                        #region Workflow Context
                        WorkflowContext = new WorkflowContext()
                        {
                            SPWebID = a.SPWebID,
                            SPSiteID = a.SPSiteID,
                            SPListID = a.SPListID,
                            SPListItemID = a.SPListItemID,
                            SPTaskListID = a.SPTaskListID.Value,
                            SPTaskListItemID = a.SPTaskListItemID.Value,
                            Initiator = a.Initiator, //Add by Fandi
                            ApproverID = a.ApproverID,
                            WorkflowInstanceID = a.WorkflowInstanceID,
                            WorkflowID = a.WorkflowID.Value,
                            WorkflowName = a.WorkflowName,
                            StateID = a.WorkflowState.Value
                        },
                        #endregion

                        #region Workflow Task
                        CurrentTask = new WorkflowTask()
                        {
                            TaskTypeID = a.TaskType.Value,
                            ActivityTitle = a.ActivityTitle,
                            OutcomeID = a.OutcomeID
                        },
                        #endregion

                        #region Transaction
                        Transaction = new TransactionTaskModel()
                        {
                            ID = a.ID,
                            ApplicationID = a.ApplicationID,
                            Customer = a.Customer,
                            Product = a.Product,
                            Currency = a.Currency,
                            Amount = a.Amount,
                            AmountUSD = a.AmountUSD,
                            IsTopUrgent = a.IsTopUrgent,
                            IsTopUrgentValue = a.IsTopUrgentValue,
                            IsFXTransaction = a.IsFXTransaction,
                            IsFXTransactionValue = a.IsFXTransactionValue,
                            TransactionStatus = a.TransactionStatus,
                            BranchCode = a.BranchCode,
                            BranchName = a.BranchName,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate,
                            DebitAccNumber = a.AccountNumber,
                            ProductID = (int)a.ProductID,
                            Mode = a.Mode
                        }
                        #endregion
                    }).Skip(skip).Take(take).ToList();
                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.ToString();
                }
            }

            return isSuccess;
        }

        public bool GetAllTransactionSPUserTasks(Guid webID, Guid siteID, IList<Guid> workflowIDs, IList<string> state, IList<string> outcome, IList<string> customOutcome, bool showContribute, bool showActiveTask, int? page, int? size, string sortColumn, string sortOrder, IList<SPUserTaskFilter> filters, ref SPUserTaskGrid userTaskGrid, ref string message)
        {
            bool isSuccess = false;

            using (DBSEntities context = new DBSEntities())
            {
                try
                {
                    int skip = (page.Value - 1) * size.Value;
                    int take = size.Value;

                    SPUserTask filter = new SPUserTask();
                    filter.WorkflowContext = new WorkflowContext();
                    filter.CurrentTask = new WorkflowTask();
                    filter.ProgressTask = new WorkflowTask();
                    filter.Transaction = new TransactionTaskModel();
                    filter.Transaction.IsFXTransaction = null;

                    if (filters != null)
                    {
                        filter.Transaction.BranchCode = filters.Where(sp => sp.Field.Equals("BranchCode")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.BranchName = filters.Where(sp => sp.Field.Equals("BranchName")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.ApplicationID = filters.Where(sp => sp.Field.Equals("ApplicationID")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.Customer = filters.Where(sp => sp.Field.Equals("Customer")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.Product = filters.Where(sp => sp.Field.Equals("Product")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.Currency = filters.Where(sp => sp.Field.Equals("Currency")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.DebitAccNumber = (string)filters.Where(sp => sp.Field.Equals("DebitAccNumber")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.IsFXTransactionValue = filters.Where(sp => sp.Field.Equals("IsFXTransaction")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.IsTopUrgentValue = filters.Where(sp => sp.Field.Equals("IsTopUrgent")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.TransactionStatus = filters.Where(sp => sp.Field.Equals("TransactionStatus")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.LastModifiedBy = filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                        string amount = filters.Where(sp => sp.Field.Equals("Amount")).Select(sp => sp.Value).SingleOrDefault();
                        if (!string.IsNullOrEmpty(amount))
                        {
                            decimal valueAmount;
                            bool resultAmount = decimal.TryParse(amount, out valueAmount);
                            if (resultAmount)
                            {
                                filter.Transaction.Amount = valueAmount;
                            }
                            else
                            {
                                userTaskGrid.Page = page.Value;
                                userTaskGrid.Size = size.Value;
                                userTaskGrid.Total = 0;
                                return true;
                            }
                        }

                        if (filters.Where(a => a.Field.Equals("EntryTime")).Any())
                        {
                            filter.Transaction.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("EntryTime")).Select(a => a.Value).SingleOrDefault());
                        }
                    }

                    var data = (from a in context.SP_GetTaskAll(currentUser.GetCurrentUser().LoginName,
                                                                filter.Transaction.ApplicationID,
                                                                filter.Transaction.Customer,
                                                                filter.Transaction.Product,
                                                                filter.Transaction.Currency,
                                                                filter.Transaction.Amount,
                                                                filter.Transaction.DebitAccNumber,
                                                                filter.Transaction.IsFXTransactionValue,
                                                                filter.Transaction.IsTopUrgentValue,
                                                                filter.Transaction.TransactionStatus,
                                                                filter.Transaction.BranchCode,
                                                                filter.Transaction.BranchName,
                                                                filter.Transaction.LastModifiedBy,
                                                                filter.Transaction.LastModifiedDate)
                                select new
                                {
                                    #region Workflow Context
                                    SPWebID = a.SPWebID,
                                    SPSiteID = a.SPSiteID,
                                    SPListID = a.SPListID,
                                    SPListItemID = a.SPListItemID,
                                    SPTaskListID = a.SPTaskListID,
                                    SPTaskListItemID = a.SPTaskListItemID,
                                    ApproverID = a.WorkflowApproverID,
                                    Initiator = a.WorkflowInitiator,
                                    WorkflowID = a.WorkflowID,
                                    WorkflowName = a.WorkflowName,
                                    WorkflowInstanceID = a.WorkflowInstanceID,
                                    WorkflowState = a.WorkflowState,
                                    #endregion

                                    #region Workflow Task
                                    TaskType = a.WorkflowTaskType,
                                    ActivityTitle = a.WorkflowTaskActivityTitle,
                                    OutcomeID = a.WorkflowOutcome,
                                    #endregion

                                    #region Transaction
                                    ID = a.TransactionID,
                                    ApplicationID = a.ApplicationID,
                                    AccountNumber = a.AccountNumber,
                                    Customer = a.CustomerName,
                                    Product = a.ProductName,
                                    Currency = a.CurrencyCode,
                                    Amount = a.Amount,
                                    AmountUSD = a.AmountUSD,
                                    IsTopUrgentValue = a.IsTopUrgent,
                                    IsTopUrgent = a.IsTopUrgent == "Top Urgent Chain" ? 2 : a.IsTopUrgent == "Top Urgent" ? 1 : 0,
                                    IsFXTransaction = (a.IsFXTransaction == "Yes") ? true : false,
                                    IsFXTransactionValue = a.IsFXTransaction,
                                    BranchCode = a.BranchCode,
                                    BranchName = a.BranchName,
                                    LastModifiedBy = a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate,
                                    ProductID = a.ProductID,
                                    TransactionStatus = a.TransactionStatus,
                                    PaymentMode = a.Mode
                                    #endregion
                                }).ToList();

                    userTaskGrid.Page = page.Value;
                    userTaskGrid.Size = size.Value;
                    userTaskGrid.Total = data.Count();

                    userTaskGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };

                    string orderBy = string.Format("{0} {1}", sortColumn, sortOrder);

                    userTaskGrid.Rows = data.AsQueryable().OrderBy(orderBy).Select((a, i) => new SPUserTaskRow()
                    {
                        RowID = i + 1,

                        #region Workflow Context
                        WorkflowContext = new WorkflowContext()
                        {
                            SPWebID = a.SPWebID,
                            SPSiteID = a.SPSiteID,
                            SPListID = a.SPListID,
                            SPListItemID = a.SPListItemID,
                            SPTaskListID = a.SPTaskListID.Value,
                            SPTaskListItemID = a.SPTaskListItemID.Value,
                            Initiator = a.Initiator,
                            ApproverID = a.ApproverID,
                            WorkflowInstanceID = a.WorkflowInstanceID,
                            WorkflowID = a.WorkflowID.Value,
                            WorkflowName = a.WorkflowName,
                            StateID = a.WorkflowState.Value,
                        },
                        #endregion

                        #region Workflow Task
                        CurrentTask = new WorkflowTask()
                        {
                            TaskTypeID = a.TaskType.Value,
                            ActivityTitle = a.ActivityTitle,
                            OutcomeID = a.OutcomeID
                        },
                        #endregion

                        #region Transaction
                        Transaction = new TransactionTaskModel()
                        {
                            ID = a.ID,
                            ApplicationID = a.ApplicationID,
                            Customer = a.Customer,
                            Product = a.Product,
                            Currency = a.Currency,
                            Amount = a.Amount,
                            AmountUSD = a.AmountUSD,
                            IsTopUrgent = a.IsTopUrgent,
                            IsTopUrgentValue = a.IsTopUrgentValue,
                            IsFXTransaction = a.IsFXTransaction,
                            IsFXTransactionValue = a.IsFXTransactionValue,
                            TransactionStatus = a.TransactionStatus,
                            BranchCode = a.BranchCode,
                            BranchName = a.BranchName,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate,
                            DebitAccNumber = a.AccountNumber,
                            ProductID = (int)a.ProductID,
                            Mode = a.PaymentMode,
                            
                        }
                        #endregion

                    }).Skip(skip).Take(take).ToList();


                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.InnerException.Message;
                }
            }

            return isSuccess;
        }

        public bool GetCanceledSPUserTasks(Guid webID, Guid siteID, IList<Guid> workflowIDs, IList<string> state, IList<string> outcome, IList<string> customOutcome, bool showContribute, bool showActiveTask, int? page, int? size, string sortColumn, string sortOrder, IList<SPUserTaskFilter> filters, ref SPUserTaskGrid userTaskGrid, ref string message)
        {
            bool isSuccess = false;

            using (DBSEntities context = new DBSEntities())
            {
                try
                {
                    int skip = (page.Value - 1) * size.Value;
                    int take = size.Value;

                    SPUserTask filter = new SPUserTask();
                    filter.WorkflowContext = new WorkflowContext();
                    filter.CurrentTask = new WorkflowTask();
                    filter.ProgressTask = new WorkflowTask();
                    filter.Transaction = new TransactionTaskModel();
                    filter.Transaction.IsFXTransaction = null;

                    if (filters != null)
                    {
                        filter.Transaction.BranchCode = filters.Where(sp => sp.Field.Equals("BranchCode")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.BranchName = filters.Where(sp => sp.Field.Equals("BranchName")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.ApplicationID = filters.Where(sp => sp.Field.Equals("ApplicationID")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.Customer = filters.Where(sp => sp.Field.Equals("Customer")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.Product = filters.Where(sp => sp.Field.Equals("Product")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.Currency = filters.Where(sp => sp.Field.Equals("Currency")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.DebitAccNumber = (string)filters.Where(sp => sp.Field.Equals("DebitAccNumber")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.IsFXTransactionValue = filters.Where(sp => sp.Field.Equals("IsFXTransaction")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.IsTopUrgentValue = filters.Where(sp => sp.Field.Equals("IsTopUrgent")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.TransactionStatus = filters.Where(sp => sp.Field.Equals("TransactionStatus")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.LastModifiedBy = filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                        string amount = filters.Where(sp => sp.Field.Equals("Amount")).Select(sp => sp.Value).SingleOrDefault();
                        if (!string.IsNullOrEmpty(amount))
                        {
                            decimal valueAmount;
                            bool resultAmount = decimal.TryParse(amount, out valueAmount);
                            if (resultAmount)
                            {
                                filter.Transaction.Amount = valueAmount;
                            }
                            else
                            {
                                userTaskGrid.Page = page.Value;
                                userTaskGrid.Size = size.Value;
                                userTaskGrid.Total = 0;
                                return true;
                            }
                        }

                        if (filters.Where(a => a.Field.Equals("EntryTime")).Any())
                        {
                            filter.Transaction.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("EntryTime")).Select(a => a.Value).SingleOrDefault());
                        }
                    }

                    var data = (
                            from a in context.SP_GetTaskCanceled(currentUser.GetCurrentUser().LoginName,
                                                                  filter.Transaction.ApplicationID,
                                                                  filter.Transaction.Customer,
                                                                  filter.Transaction.Product,
                                                                  filter.Transaction.Currency,
                                                                  filter.Transaction.Amount,
                                                                  filter.Transaction.DebitAccNumber,
                                                                  filter.Transaction.IsFXTransactionValue,
                                                                  filter.Transaction.IsTopUrgentValue,
                                                                  filter.Transaction.TransactionStatus,
                                                                  filter.Transaction.BranchCode,
                                                                  filter.Transaction.BranchName,
                                                                  filter.Transaction.LastModifiedBy,
                                                                  filter.Transaction.LastModifiedDate)
                            select new
                            {
                                #region Workflow Context
                                SPWebID = a.SPWebID,
                                SPSiteID = a.SPSiteID,
                                SPListID = a.SPListID,
                                SPListItemID = a.SPListItemID,
                                SPTaskListID = a.SPTaskListID,
                                SPTaskListItemID = a.SPTaskListItemID,
                                ApproverID = a.WorkflowApproverID,
                                Initiator = a.WorkflowInitiator,
                                WorkflowID = a.WorkflowID,
                                WorkflowName = a.WorkflowName,
                                WorkflowInstanceID = a.WorkflowInstanceID,
                                WorkflowState = a.WorkflowState,
                                #endregion

                                #region Workflow Task
                                TaskType = a.WorkflowTaskType,
                                ActivityTitle = a.WorkflowTaskActivityTitle,
                                OutcomeID = a.WorkflowOutcome,
                                #endregion

                                #region Transaction
                                ID = a.TransactionID,
                                ApplicationID = a.ApplicationID,
                                AccountNumber = a.AccountNumber,
                                Customer = a.CustomerName,
                                Product = a.ProductName,
                                Currency = a.CurrencyCode,
                                Amount = a.Amount,
                                AmountUSD = a.AmountUSD,
                                IsTopUrgentValue = a.IsTopUrgent,
                                IsTopUrgent = a.IsTopUrgent == "Top Urgent Chain" ? 2 : a.IsTopUrgent == "Top Urgent" ? 1 : 0,
                                IsFXTransaction = (a.IsFXTransaction == "Yes") ? true : false,
                                IsFXTransactionValue = a.IsFXTransaction,
                                BranchCode = a.BranchCode,
                                BranchName = a.BranchName,
                                LastModifiedBy = a.UpdateBy,
                                LastModifiedDate = a.UpdateDate,
                                ProductID = a.ProductID,
                                TransactionStatus = a.TransactionStatus,
                                Mode = a.Mode
                                #endregion
                            }).ToList();

                    userTaskGrid.Page = page.Value;
                    userTaskGrid.Size = size.Value;
                    userTaskGrid.Total = data.Count();

                    userTaskGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };

                    string orderBy = string.Format("{0} {1}", sortColumn, sortOrder);

                    userTaskGrid.Rows = data.AsQueryable().OrderBy(orderBy).Select((a, i) => new SPUserTaskRow()
                    {
                        RowID = i + 1,

                        #region Workflow Context
                        WorkflowContext = new WorkflowContext()
                        {
                            SPWebID = a.SPWebID,
                            SPSiteID = a.SPSiteID,
                            SPListID = a.SPListID,
                            SPListItemID = a.SPListItemID,
                            SPTaskListID = a.SPTaskListID.Value,
                            SPTaskListItemID = a.SPTaskListItemID.Value,
                            Initiator = a.Initiator,
                            ApproverID = a.ApproverID,
                            WorkflowInstanceID = a.WorkflowInstanceID,
                            WorkflowID = a.WorkflowID.Value,
                            WorkflowName = a.WorkflowName,
                            StateID = a.WorkflowState.Value
                        },
                        #endregion

                        #region Workflow Task
                        CurrentTask = new WorkflowTask()
                        {
                            TaskTypeID = a.TaskType.Value,
                            ActivityTitle = a.ActivityTitle,
                            OutcomeID = a.OutcomeID
                        },
                        #endregion

                        #region Transaction
                        Transaction = new TransactionTaskModel()
                        {
                            ID = a.ID,
                            ApplicationID = a.ApplicationID,
                            Customer = a.Customer,
                            Product = a.Product,
                            Currency = a.Currency,
                            Amount = a.Amount,
                            AmountUSD = a.AmountUSD,
                            IsTopUrgent = a.IsTopUrgent,
                            IsTopUrgentValue = a.IsTopUrgentValue,
                            IsFXTransaction = a.IsFXTransaction,
                            IsFXTransactionValue = a.IsFXTransactionValue,
                            TransactionStatus = a.TransactionStatus,
                            BranchCode = a.BranchCode,
                            BranchName = a.BranchName,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate,
                            DebitAccNumber = a.AccountNumber,
                            ProductID = (int)a.ProductID,
                            Mode = a.Mode
                        }
                        #endregion
                    }).Skip(skip).Take(take).ToList();
                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.ToString();
                }
            }

            return isSuccess;
        }

        public bool GetCompletedSPUserTasks(Guid webID, Guid siteID, IList<Guid> workflowIDs, IList<string> state, IList<string> outcome, IList<string> customOutcome, bool showContribute, bool showActiveTask, int? page, int? size, string sortColumn, string sortOrder, IList<SPUserTaskFilter> filters, ref SPUserTaskGrid userTaskGrid, ref string message)
        {
            bool isSuccess = false;

            using (DBSEntities context = new DBSEntities())
            {
                try
                {
                    int skip = (page.Value - 1) * size.Value;
                    int take = size.Value;

                    SPUserTask filter = new SPUserTask();
                    filter.WorkflowContext = new WorkflowContext();
                    filter.CurrentTask = new WorkflowTask();
                    filter.ProgressTask = new WorkflowTask();
                    filter.Transaction = new TransactionTaskModel();
                    filter.Transaction.IsFXTransaction = null;

                    if (filters != null)
                    {
                        filter.Transaction.BranchCode = filters.Where(sp => sp.Field.Equals("BranchCode")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.BranchName = filters.Where(sp => sp.Field.Equals("BranchName")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.ApplicationID = filters.Where(sp => sp.Field.Equals("ApplicationID")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.Customer = filters.Where(sp => sp.Field.Equals("Customer")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.Product = filters.Where(sp => sp.Field.Equals("Product")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.Currency = filters.Where(sp => sp.Field.Equals("Currency")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.DebitAccNumber = (string)filters.Where(sp => sp.Field.Equals("DebitAccNumber")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.IsFXTransactionValue = filters.Where(sp => sp.Field.Equals("IsFXTransaction")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.IsTopUrgentValue = filters.Where(sp => sp.Field.Equals("IsTopUrgent")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.TransactionStatus = filters.Where(sp => sp.Field.Equals("TransactionStatus")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.LastModifiedBy = filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                        string amount = filters.Where(sp => sp.Field.Equals("Amount")).Select(sp => sp.Value).SingleOrDefault();
                        if (!string.IsNullOrEmpty(amount))
                        {
                            decimal valueAmount;
                            bool resultAmount = decimal.TryParse(amount, out valueAmount);
                            if (resultAmount)
                            {
                                filter.Transaction.Amount = valueAmount;
                            }
                            else
                            {
                                userTaskGrid.Page = page.Value;
                                userTaskGrid.Size = size.Value;
                                userTaskGrid.Total = 0;
                                return true;
                            }
                        }

                        if (filters.Where(a => a.Field.Equals("EntryTime")).Any())
                        {
                            filter.Transaction.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("EntryTime")).Select(a => a.Value).SingleOrDefault());
                        }
                    }

                    var data = (
                            from a in context.SP_GetTaskCompleted(currentUser.GetCurrentUser().LoginName,
                                                                  filter.Transaction.ApplicationID,
                                                                  filter.Transaction.Customer,
                                                                  filter.Transaction.Product,
                                                                  filter.Transaction.Currency,
                                                                  filter.Transaction.Amount,
                                                                  filter.Transaction.DebitAccNumber,
                                                                  filter.Transaction.IsFXTransactionValue,
                                                                  filter.Transaction.IsTopUrgentValue,
                                                                  filter.Transaction.TransactionStatus,
                                                                  filter.Transaction.BranchCode,
                                                                  filter.Transaction.BranchName,
                                                                  filter.Transaction.LastModifiedBy,
                                                                  filter.Transaction.LastModifiedDate)
                            select new
                            {
                                #region Workflow Context
                                SPWebID = a.SPWebID,
                                SPSiteID = a.SPSiteID,
                                SPListID = a.SPListID,
                                SPListItemID = a.SPListItemID,
                                SPTaskListID = a.SPTaskListID,
                                SPTaskListItemID = a.SPTaskListItemID,
                                ApproverID = a.WorkflowApproverID,
                                Initiator = a.WorkflowInitiator,
                                WorkflowID = a.WorkflowID,
                                WorkflowName = a.WorkflowName,
                                WorkflowInstanceID = a.WorkflowInstanceID,
                                WorkflowState = a.WorkflowState,
                                #endregion

                                #region Workflow Task
                                TaskType = a.WorkflowTaskType,
                                ActivityTitle = a.WorkflowTaskActivityTitle,
                                OutcomeID = a.WorkflowOutcome,
                                #endregion

                                #region Transaction
                                ID = a.TransactionID,
                                ApplicationID = a.ApplicationID,
                                AccountNumber = a.AccountNumber,
                                Customer = a.CustomerName,
                                Product = a.ProductName,
                                Currency = a.CurrencyCode,
                                Amount = a.Amount,
                                AmountUSD = a.AmountUSD,
                                IsTopUrgentValue = a.IsTopUrgent,
                                IsTopUrgent = a.IsTopUrgent == "Top Urgent Chain" ? 2 : a.IsTopUrgent == "Top Urgent" ? 1 : 0,
                                IsFXTransaction = (a.IsFXTransaction == "Yes") ? true : false,
                                IsFXTransactionValue = a.IsFXTransaction,
                                BranchCode = a.BranchCode,
                                BranchName = a.BranchName,
                                LastModifiedBy = a.UpdateBy,
                                LastModifiedDate = a.UpdateDate,
                                ProductID = a.ProductID,
                                TransactionStatus = a.TransactionStatus,
                                Mode = a.Mode
                                #endregion
                            }).ToList();

                    userTaskGrid.Page = page.Value;
                    userTaskGrid.Size = size.Value;
                    userTaskGrid.Total = data.Count();

                    userTaskGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };

                    string orderBy = string.Format("{0} {1}", sortColumn, sortOrder);

                    userTaskGrid.Rows = data.AsQueryable().OrderBy(orderBy).Select((a, i) => new SPUserTaskRow()
                    {
                        RowID = i + 1,

                        #region Workflow Context
                        WorkflowContext = new WorkflowContext()
                        {
                            SPWebID = a.SPWebID,
                            SPSiteID = a.SPSiteID,
                            SPListID = a.SPListID,
                            SPListItemID = a.SPListItemID,
                            SPTaskListID = a.SPTaskListID.Value,
                            SPTaskListItemID = a.SPTaskListItemID.Value,
                            Initiator = a.Initiator,
                            ApproverID = a.ApproverID,
                            WorkflowInstanceID = a.WorkflowInstanceID,
                            WorkflowID = a.WorkflowID.Value,
                            WorkflowName = a.WorkflowName,
                            StateID = a.WorkflowState.Value
                        },
                        #endregion

                        #region Workflow Task
                        CurrentTask = new WorkflowTask()
                        {
                            TaskTypeID = a.TaskType.Value,
                            ActivityTitle = a.ActivityTitle,
                            OutcomeID = a.OutcomeID
                        },
                        #endregion

                        #region Transaction
                        Transaction = new TransactionTaskModel()
                        {
                            ID = a.ID,
                            ApplicationID = a.ApplicationID,
                            Customer = a.Customer,
                            Product = a.Product,
                            Currency = a.Currency,
                            Amount = a.Amount,
                            AmountUSD = a.AmountUSD,
                            IsTopUrgent = a.IsTopUrgent,
                            IsTopUrgentValue = a.IsTopUrgentValue,
                            IsFXTransaction = a.IsFXTransaction,
                            IsFXTransactionValue = a.IsFXTransactionValue,
                            TransactionStatus = a.TransactionStatus,
                            BranchCode = a.BranchCode,
                            BranchName = a.BranchName,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate,
                            DebitAccNumber = a.AccountNumber,
                            ProductID = (int)a.ProductID,
                            Mode = a.Mode
                        }
                        #endregion

                    }).Skip(skip).Take(take).ToList();

                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.InnerException.Message;
                }
            }

            return isSuccess;
        }

        public bool GetMonitoringSPUserTasks(Guid webID, Guid siteID, IList<Guid> workflowIDs, IList<string> state, IList<string> outcome, IList<string> customOutcome, bool showContribute, bool showActiveTask, int? page, int? size, string sortColumn, string sortOrder, IList<SPUserTaskFilter> filters, ref SPUserTaskGrid userTaskGrid, ref string message)
        {
            bool isSuccess = false;

            using (DBSEntities context = new DBSEntities())
            {
                try
                {
                    int skip = (page.Value - 1) * size.Value;
                    int take = size.Value;

                    SPUserTask filter = new SPUserTask();
                    filter.WorkflowContext = new WorkflowContext();
                    filter.CurrentTask = new WorkflowTask();
                    filter.ProgressTask = new WorkflowTask();
                    filter.Transaction = new TransactionTaskModel();
                    filter.Transaction.IsFXTransaction = null;

                    if (filters != null)
                    {
                        filter.Transaction.BranchCode = filters.Where(sp => sp.Field.Equals("BranchCode")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.BranchName = filters.Where(sp => sp.Field.Equals("BranchName")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.ApplicationID = filters.Where(sp => sp.Field.Equals("ApplicationID")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.Customer = filters.Where(sp => sp.Field.Equals("Customer")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.Product = filters.Where(sp => sp.Field.Equals("Product")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.Currency = filters.Where(sp => sp.Field.Equals("Currency")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.DebitAccNumber = (string)filters.Where(sp => sp.Field.Equals("DebitAccNumber")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.IsFXTransactionValue = filters.Where(sp => sp.Field.Equals("IsFXTransaction")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.IsTopUrgentValue = filters.Where(sp => sp.Field.Equals("IsTopUrgent")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.TransactionStatus = filters.Where(sp => sp.Field.Equals("TransactionStatus")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.LastModifiedBy = filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                        string amount = filters.Where(sp => sp.Field.Equals("Amount")).Select(sp => sp.Value).SingleOrDefault();
                        if (!string.IsNullOrEmpty(amount))
                        {
                            decimal valueAmount;
                            bool resultAmount = decimal.TryParse(amount, out valueAmount);
                            if (resultAmount)
                            {
                                filter.Transaction.Amount = valueAmount;
                            }
                            else
                            {
                                userTaskGrid.Page = page.Value;
                                userTaskGrid.Size = size.Value;
                                userTaskGrid.Total = 0;
                                return true;
                            }
                        }

                        if (filters.Where(a => a.Field.Equals("EntryTime")).Any())
                        {
                            filter.Transaction.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("EntryTime")).Select(a => a.Value).SingleOrDefault());
                        }
                    }

                    var data = (
                            from a in context.SP_GetTaskMonitoring(currentUser.GetCurrentUser().LoginName,
                                                                  filter.Transaction.ApplicationID,
                                                                  filter.Transaction.Customer,
                                                                  filter.Transaction.Product,
                                                                  filter.Transaction.Currency,
                                                                  filter.Transaction.Amount,
                                                                  filter.Transaction.DebitAccNumber,
                                                                  filter.Transaction.IsFXTransactionValue,
                                                                  filter.Transaction.IsTopUrgentValue,
                                                                  filter.Transaction.TransactionStatus,
                                                                  filter.Transaction.BranchCode,
                                                                  filter.Transaction.BranchName,
                                                                  filter.Transaction.LastModifiedBy,
                                                                  filter.Transaction.LastModifiedDate)
                            select new
                            {
                                #region Workflow Context
                                SPWebID = a.SPWebID,
                                SPSiteID = a.SPSiteID,
                                SPListID = a.SPListID,
                                SPListItemID = a.SPListItemID,
                                SPTaskListID = a.SPTaskListID,
                                SPTaskListItemID = a.SPTaskListItemID,
                                ApproverID = a.WorkflowApproverID,
                                Initiator = a.WorkflowInitiator,
                                WorkflowID = a.WorkflowID,
                                WorkflowName = a.WorkflowName,
                                WorkflowInstanceID = a.WorkflowInstanceID,
                                WorkflowState = a.WorkflowState,
                                #endregion

                                #region Workflow Task
                                TaskType = a.WorkflowTaskType,
                                ActivityTitle = a.WorkflowTaskActivityTitle,
                                OutcomeID = a.WorkflowOutcome,
                                #endregion

                                #region Transaction
                                ID = a.TransactionID,
                                ApplicationID = a.ApplicationID,
                                AccountNumber = a.AccountNumber,
                                Customer = a.CustomerName,
                                Product = a.ProductName,
                                Currency = a.CurrencyCode,
                                Amount = a.Amount,
                                AmountUSD = a.AmountUSD,
                                IsTopUrgentValue = a.IsTopUrgent,
                                IsTopUrgent = a.IsTopUrgent == "Top Urgent Chain" ? 2 : a.IsTopUrgent == "Top Urgent" ? 1 : 0,
                                IsFXTransaction = (a.IsFXTransaction == "Yes") ? true : false,
                                IsFXTransactionValue = a.IsFXTransaction,
                                BranchCode = a.BranchCode,
                                BranchName = a.BranchName,
                                LastModifiedBy = a.UpdateBy,
                                LastModifiedDate = a.UpdateDate,
                                ProductID = a.ProductID,
                                TransactionStatus = a.TransactionStatus,
                                Mode = a.Mode
                                #endregion
                            }).ToList();

                    userTaskGrid.Page = page.Value;
                    userTaskGrid.Size = size.Value;
                    userTaskGrid.Total = data.Count();
                    userTaskGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };

                    string orderBy = string.Format("{0} {1}", sortColumn, sortOrder);

                    userTaskGrid.Rows = data.AsQueryable().OrderBy(orderBy).Select((a, i) => new SPUserTaskRow()
                    {
                        RowID = i + 1,

                        #region Workflow Context
                        WorkflowContext = new WorkflowContext()
                        {
                            SPWebID = a.SPWebID,
                            SPSiteID = a.SPSiteID,
                            SPListID = a.SPListID,
                            SPListItemID = a.SPListItemID,
                            SPTaskListID = a.SPTaskListID.Value,
                            SPTaskListItemID = a.SPTaskListItemID.Value,
                            Initiator = a.Initiator,
                            ApproverID = a.ApproverID,
                            WorkflowInstanceID = a.WorkflowInstanceID,
                            WorkflowID = a.WorkflowID.Value,
                            WorkflowName = a.WorkflowName,
                            StateID = a.WorkflowState.Value
                        },
                        #endregion

                        #region Workflow Task
                        CurrentTask = new WorkflowTask()
                        {
                            TaskTypeID = a.TaskType.Value,
                            ActivityTitle = a.ActivityTitle,
                            OutcomeID = a.OutcomeID
                        },
                        #endregion

                        #region Transaction
                        Transaction = new TransactionTaskModel()
                        {
                            ID = a.ID,
                            ApplicationID = a.ApplicationID,
                            Customer = a.Customer,
                            Product = a.Product,
                            Currency = a.Currency,
                            Amount = a.Amount,
                            AmountUSD = a.AmountUSD,
                            IsTopUrgent = a.IsTopUrgent,
                            IsTopUrgentValue = a.IsTopUrgentValue,
                            IsFXTransaction = a.IsFXTransaction,
                            IsFXTransactionValue = a.IsFXTransactionValue,
                            TransactionStatus = a.TransactionStatus,
                            BranchCode = a.BranchCode,
                            BranchName = a.BranchName,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate,
                            DebitAccNumber = a.AccountNumber,
                            ProductID = (int)a.ProductID,
                            Mode = a.Mode
                        }
                        #endregion

                    }).Skip(skip).Take(take).ToList();


                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.InnerException.Message;
                }
            }

            return isSuccess;
        }

        public bool IsCBOAccount(string Username)
        {
            bool ret = false;
            RoleRepository Rolerepo = new RoleRepository();
            IList<RoleModel> RoleList = new List<RoleModel>();
            string sMess = string.Empty;
            try
            {
                Rolerepo.GetRoleByUsername(Username, ref  RoleList, ref sMess);
                if (RoleList != null)
                {
                    if (RoleList.Count > 0)
                    {
                        foreach (var rl in RoleList)
                        {
                            if (rl.Name != null)
                            {
                                if (rl.Name.ToUpper().Contains("CBO"))
                                {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                ret = false;
            }

            return ret;
        }
        #endregion

        #region Tunning Loan
        public bool GetHomeUserLoanIMTasks(Guid webID, Guid siteID, IList<Guid> workflowIDs, IList<string> state, IList<string> outcome, IList<string> customOutcome, bool showContribute, bool showActiveTask, int? page, int? size, string sortColumn, string sortOrder, IList<SPUserTaskFilter> filters, ref SPUserTaskGrid userTaskGrid, ref string message)
        {
            bool isSuccess = false;

            using (DBSEntities context = new DBSEntities())
            {
                try
                {
                    int skip = (page.Value - 1) * size.Value;
                    int take = size.Value;

                    SPUserTask filter = new SPUserTask();
                    filter.WorkflowContext = new WorkflowContext();
                    filter.CurrentTask = new WorkflowTask();
                    filter.ProgressTask = new WorkflowTask();
                    filter.Transaction = new TransactionTaskModel();
                    filter.Transaction.IsFXTransaction = null;

                    if (filters != null)
                    {
                        filter.Transaction.BranchCode = filters.Where(sp => sp.Field.Equals("BranchCode")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.BranchName = filters.Where(sp => sp.Field.Equals("BranchName")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.ApplicationID = filters.Where(sp => sp.Field.Equals("ApplicationID")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.Customer = filters.Where(sp => sp.Field.Equals("Customer")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.Product = filters.Where(sp => sp.Field.Equals("Product")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.Currency = filters.Where(sp => sp.Field.Equals("Currency")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.DebitAccNumber = (string)filters.Where(sp => sp.Field.Equals("DebitAccNumber")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.IsFXTransactionValue = filters.Where(sp => sp.Field.Equals("IsFXTransaction")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.IsTopUrgentValue = filters.Where(sp => sp.Field.Equals("IsTopUrgent")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.TransactionStatus = filters.Where(sp => sp.Field.Equals("TransactionStatus")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.LastModifiedBy = filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                        string amount = filters.Where(sp => sp.Field.Equals("Amount")).Select(sp => sp.Value).SingleOrDefault();
                        if (!string.IsNullOrEmpty(amount))
                        {
                            decimal valueAmount;
                            bool resultAmount = decimal.TryParse(amount, out valueAmount);
                            if (resultAmount)
                            {
                                filter.Transaction.Amount = valueAmount;
                            }
                            else
                            {
                                userTaskGrid.Page = page.Value;
                                userTaskGrid.Size = size.Value;
                                userTaskGrid.Total = 0;
                                return true;
                            }
                        }

                        if (filters.Where(a => a.Field.Equals("EntryTime")).Any())
                        {
                            filter.Transaction.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("EntryTime")).Select(a => a.Value).SingleOrDefault());
                        }
                    }

                    var data = (from a in context.SP_GetTaskUserHomeLoanIM(currentUser.GetCurrentUser().LoginName,
                                                                filter.Transaction.ApplicationID,
                                                                filter.Transaction.Customer,
                                                                filter.Transaction.Product,
                                                                filter.Transaction.Currency,
                                                                filter.Transaction.Amount,
                                                                filter.Transaction.DebitAccNumber,
                                                                filter.Transaction.IsFXTransactionValue,
                                                                filter.Transaction.IsTopUrgentValue,
                                                                filter.Transaction.TransactionStatus,
                                                                filter.Transaction.BranchCode,
                                                                filter.Transaction.BranchName,
                                                                filter.Transaction.LastModifiedBy,
                                                                filter.Transaction.LastModifiedDate)
                                select new
                                {
                                    #region Workflow Context
                                    SPWebID = a.SPWebID,
                                    SPSiteID = a.SPSiteID,
                                    SPListID = a.SPListID,
                                    SPListItemID = a.SPListItemID,
                                    SPTaskListID = a.SPTaskListID,
                                    SPTaskListItemID = a.SPTaskListItemID,
                                    ApproverID = a.WorkflowApproverID,
                                    Initiator = a.WorkflowInitiator,
                                    WorkflowID = a.WorkflowID,
                                    WorkflowName = a.WorkflowName,
                                    WorkflowInstanceID = a.WorkflowInstanceID,
                                    WorkflowState = a.WorkflowState,
                                    #endregion

                                    #region Workflow Task
                                    TaskType = a.WorkflowTaskType,
                                    ActivityTitle = a.WorkflowTaskActivityTitle,
                                    OutcomeID = a.WorkflowOutcome,
                                    #endregion

                                    #region Transaction
                                    ID = a.TransactionID,
                                    ApplicationID = a.ApplicationID,
                                    AccountNumber = a.AccountNumber,
                                    Customer = a.CustomerName,
                                    Product = a.ProductName,
                                    Currency = a.CurrencyCode,
                                    Amount = a.Amount,
                                    AmountUSD = a.AmountUSD,
                                    IsTopUrgentValue = a.IsTopUrgent,
                                    IsTopUrgent = a.IsTopUrgent == "Top Urgent Chain" ? 2 : a.IsTopUrgent == "Top Urgent" ? 1 : 0,
                                    IsFXTransaction = (a.IsFXTransaction == "Yes") ? true : false,
                                    IsFXTransactionValue = a.IsFXTransaction,
                                    BranchCode = a.BranchCode,
                                    BranchName = a.BranchName,
                                    LastModifiedBy = a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate,
                                    ProductID = a.ProductID,
                                    TransactionStatus = a.TransactionStatus,
                                    CBOOrderNo = a.CBOOrderNo,
                                    Mode = a.Mode
                                    #endregion
                                }).ToList();

                    userTaskGrid.Page = page.Value;
                    userTaskGrid.Size = size.Value;
                    userTaskGrid.Total = data.Count();

                    userTaskGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };

                    string orderBy = string.Format("IsTopUrgent DESC,{0} {1}", sortColumn, sortOrder);

                    #region Set CBO Account order
                    bool isCBO = false;
                    isCBO = IsCBOAccount(currentUser.GetCurrentUser().LoginName);
                    if (isCBO)
                    {
                        orderBy = string.Format("CBOOrderNo ASC,IsTopUrgent DESC,{0} {1}", sortColumn, "ASC");
                    }
                    #endregion

                    userTaskGrid.Rows = data.AsQueryable().OrderBy(orderBy).Select((a, i) => new SPUserTaskRow()
                    {
                        RowID = i + 1,

                        #region Workflow Context
                        WorkflowContext = new WorkflowContext()
                        {
                            SPWebID = a.SPWebID,
                            SPSiteID = a.SPSiteID,
                            SPListID = a.SPListID,
                            SPListItemID = a.SPListItemID,
                            SPTaskListID = a.SPTaskListID.Value,
                            SPTaskListItemID = a.SPTaskListItemID.Value,
                            Initiator = a.Initiator, //Add by Fandi
                            ApproverID = a.ApproverID,
                            WorkflowInstanceID = a.WorkflowInstanceID,
                            WorkflowID = a.WorkflowID.Value,
                            WorkflowName = a.WorkflowName,
                            StateID = a.WorkflowState.Value
                        },
                        #endregion

                        #region Workflow Task
                        CurrentTask = new WorkflowTask()
                        {
                            TaskTypeID = a.TaskType.Value,
                            ActivityTitle = a.ActivityTitle,
                            OutcomeID = a.OutcomeID
                        },
                        #endregion

                        #region Transaction
                        Transaction = new TransactionTaskModel()
                        {
                            ID = a.ID,
                            ApplicationID = a.ApplicationID,
                            Customer = a.Customer,
                            Product = a.Product,
                            Currency = a.Currency,
                            Amount = a.Amount,
                            AmountUSD = a.AmountUSD,
                            IsTopUrgent = a.IsTopUrgent,
                            IsTopUrgentValue = a.IsTopUrgentValue,
                            IsFXTransaction = a.IsFXTransaction,
                            IsFXTransactionValue = a.IsFXTransactionValue,
                            TransactionStatus = a.TransactionStatus,
                            BranchCode = a.BranchCode,
                            BranchName = a.BranchName,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate,
                            DebitAccNumber = a.AccountNumber,
                            ProductID = (int)a.ProductID,
                            Mode = a.Mode
                        }
                        #endregion
                    }).Skip(skip).Take(take).ToList();
                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.ToString();
                }
            }

            return isSuccess;
        }

        public bool GetHomeUserLoanROTasks(Guid webID, Guid siteID, IList<Guid> workflowIDs, IList<string> state, IList<string> outcome, IList<string> customOutcome, bool showContribute, bool showActiveTask, int? page, int? size, string sortColumn, string sortOrder, IList<SPUserTaskFilter> filters, ref SPUserTaskGrid userTaskGrid, ref string message)
        {
            bool isSuccess = false;

            using (DBSEntities context = new DBSEntities())
            {
                try
                {
                    int skip = (page.Value - 1) * size.Value;
                    int take = size.Value;

                    SPUserTask filter = new SPUserTask();
                    filter.WorkflowContext = new WorkflowContext();
                    filter.CurrentTask = new WorkflowTask();
                    filter.ProgressTask = new WorkflowTask();
                    filter.Transaction = new TransactionTaskModel();
                    filter.Transaction.IsFXTransaction = null;

                    if (filters != null)
                    {
                        filter.Transaction.BranchCode = filters.Where(sp => sp.Field.Equals("BranchCode")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.BranchName = filters.Where(sp => sp.Field.Equals("BranchName")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.ApplicationID = filters.Where(sp => sp.Field.Equals("ApplicationID")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.Customer = filters.Where(sp => sp.Field.Equals("Customer")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.Product = filters.Where(sp => sp.Field.Equals("Product")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.Currency = filters.Where(sp => sp.Field.Equals("Currency")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.DebitAccNumber = (string)filters.Where(sp => sp.Field.Equals("DebitAccNumber")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.IsFXTransactionValue = filters.Where(sp => sp.Field.Equals("IsFXTransaction")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.IsTopUrgentValue = filters.Where(sp => sp.Field.Equals("IsTopUrgent")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.TransactionStatus = filters.Where(sp => sp.Field.Equals("TransactionStatus")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.LastModifiedBy = filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                        string amount = filters.Where(sp => sp.Field.Equals("Amount")).Select(sp => sp.Value).SingleOrDefault();
                        if (!string.IsNullOrEmpty(amount))
                        {
                            decimal valueAmount;
                            bool resultAmount = decimal.TryParse(amount, out valueAmount);
                            if (resultAmount)
                            {
                                filter.Transaction.Amount = valueAmount;
                            }
                            else
                            {
                                userTaskGrid.Page = page.Value;
                                userTaskGrid.Size = size.Value;
                                userTaskGrid.Total = 0;
                                return true;
                            }
                        }

                        if (filters.Where(a => a.Field.Equals("EntryTime")).Any())
                        {
                            filter.Transaction.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("EntryTime")).Select(a => a.Value).SingleOrDefault());
                        }
                    }

                    var data = (from a in context.SP_GetTaskUserHomeLoanRO(currentUser.GetCurrentUser().LoginName,
                                                                filter.Transaction.ApplicationID,
                                                                filter.Transaction.Customer,
                                                                filter.Transaction.Product,
                                                                filter.Transaction.Currency,
                                                                filter.Transaction.Amount,
                                                                filter.Transaction.DebitAccNumber,
                                                                filter.Transaction.IsFXTransactionValue,
                                                                filter.Transaction.IsTopUrgentValue,
                                                                filter.Transaction.TransactionStatus,
                                                                filter.Transaction.BranchCode,
                                                                filter.Transaction.BranchName,
                                                                filter.Transaction.LastModifiedBy,
                                                                filter.Transaction.LastModifiedDate)
                                select new
                                {
                                    #region Workflow Context
                                    SPWebID = a.SPWebID,
                                    SPSiteID = a.SPSiteID,
                                    SPListID = a.SPListID,
                                    SPListItemID = a.SPListItemID,
                                    SPTaskListID = a.SPTaskListID,
                                    SPTaskListItemID = a.SPTaskListItemID,
                                    ApproverID = a.WorkflowApproverID,
                                    Initiator = a.WorkflowInitiator,
                                    WorkflowID = a.WorkflowID,
                                    WorkflowName = a.WorkflowName,
                                    WorkflowInstanceID = a.WorkflowInstanceID,
                                    WorkflowState = a.WorkflowState,
                                    #endregion

                                    #region Workflow Task
                                    TaskType = a.WorkflowTaskType,
                                    ActivityTitle = a.WorkflowTaskActivityTitle,
                                    OutcomeID = a.WorkflowOutcome,
                                    #endregion

                                    #region Transaction
                                    ID = a.TransactionID,
                                    ApplicationID = a.ApplicationID,
                                    AccountNumber = a.AccountNumber,
                                    Customer = a.CustomerName,
                                    Product = a.ProductName,
                                    Currency = a.CurrencyCode,
                                    Amount = a.Amount,
                                    AmountUSD = a.AmountUSD,
                                    IsTopUrgentValue = a.IsTopUrgent,
                                    IsTopUrgent = a.IsTopUrgent == "Top Urgent Chain" ? 2 : a.IsTopUrgent == "Top Urgent" ? 1 : 0,
                                    IsFXTransaction = (a.IsFXTransaction == "Yes") ? true : false,
                                    IsFXTransactionValue = a.IsFXTransaction,
                                    BranchCode = a.BranchCode,
                                    BranchName = a.BranchName,
                                    LastModifiedBy = a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate,
                                    ProductID = a.ProductID,
                                    TransactionStatus = a.TransactionStatus,
                                    CBOOrderNo = a.CBOOrderNo,
                                    Mode = a.Mode
                                    #endregion
                                }).ToList();

                    userTaskGrid.Page = page.Value;
                    userTaskGrid.Size = size.Value;
                    userTaskGrid.Total = data.Count();

                    userTaskGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };

                    string orderBy = string.Format("IsTopUrgent DESC,{0} {1}", sortColumn, sortOrder);

                    #region Set CBO Account order
                    bool isCBO = false;
                    isCBO = IsCBOAccount(currentUser.GetCurrentUser().LoginName);
                    if (isCBO)
                    {
                        orderBy = string.Format("CBOOrderNo ASC,IsTopUrgent DESC,{0} {1}", sortColumn, "ASC");
                    }
                    #endregion

                    userTaskGrid.Rows = data.AsQueryable().OrderBy(orderBy).Select((a, i) => new SPUserTaskRow()
                    {
                        RowID = i + 1,

                        #region Workflow Context
                        WorkflowContext = new WorkflowContext()
                        {
                            SPWebID = a.SPWebID,
                            SPSiteID = a.SPSiteID,
                            SPListID = a.SPListID,
                            SPListItemID = a.SPListItemID,
                            SPTaskListID = a.SPTaskListID.Value,
                            SPTaskListItemID = a.SPTaskListItemID.Value,
                            Initiator = a.Initiator, //Add by Fandi
                            ApproverID = a.ApproverID,
                            WorkflowInstanceID = a.WorkflowInstanceID,
                            WorkflowID = a.WorkflowID.Value,
                            WorkflowName = a.WorkflowName,
                            StateID = a.WorkflowState.Value
                        },
                        #endregion

                        #region Workflow Task
                        CurrentTask = new WorkflowTask()
                        {
                            TaskTypeID = a.TaskType.Value,
                            ActivityTitle = a.ActivityTitle,
                            OutcomeID = a.OutcomeID
                        },
                        #endregion

                        #region Transaction
                        Transaction = new TransactionTaskModel()
                        {
                            ID = a.ID,
                            ApplicationID = a.ApplicationID,
                            Customer = a.Customer,
                            Product = a.Product,
                            Currency = a.Currency,
                            Amount = a.Amount,
                            AmountUSD = a.AmountUSD,
                            IsTopUrgent = a.IsTopUrgent,
                            IsTopUrgentValue = a.IsTopUrgentValue,
                            IsFXTransaction = a.IsFXTransaction,
                            IsFXTransactionValue = a.IsFXTransactionValue,
                            TransactionStatus = a.TransactionStatus,
                            BranchCode = a.BranchCode,
                            BranchName = a.BranchName,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate,
                            DebitAccNumber = a.AccountNumber,
                            ProductID = (int)a.ProductID,
                            Mode = a.Mode
                        }
                        #endregion
                    }).Skip(skip).Take(take).ToList();
                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.ToString();
                }
            }

            return isSuccess;
        }

        #endregion

        #region Home TMO
        //Basri 05.10.2015
        public bool GetHomeTMOUserTasks(Guid webID, Guid siteID, IList<Guid> workflowIDs, IList<string> state, IList<string> outcome, IList<string> customOutcome, bool showContribute, bool showActiveTask, int? page, int? size, string sortColumn, string sortOrder, IList<SPUserTaskFilter> filters, ref SPUserTaskGrid userTaskGrid, ref string message)
        {
            bool isSuccess = false;

            using (DBSEntities context = new DBSEntities())
            {
                try
                {
                    int skip = (page.Value - 1) * size.Value;
                    int take = size.Value;

                    SPUserTask filter = new SPUserTask();
                    filter.WorkflowContext = new WorkflowContext();
                    filter.CurrentTask = new WorkflowTask();
                    filter.ProgressTask = new WorkflowTask();
                    filter.Transaction = new TransactionTaskModel();

                    if (filters != null)
                    {
                        filter.Transaction.ApplicationID = filters.Where(sp => sp.Field.Equals("ApplicationID")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.Customer = filters.Where(sp => sp.Field.Equals("Customer")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.CIF = filters.Where(sp => sp.Field.Equals("CIF")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.Product = filters.Where(sp => sp.Field.Equals("Product")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.Channel = filters.Where(sp => sp.Field.Equals("Channel")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.LastModifiedBy = filters.Where(sp => sp.Field.Equals("UserApprover")).Select(sp => sp.Value).SingleOrDefault();

                        if (filters.Where(a => a.Field.Equals("EntryTime")).Any())
                        {
                            filter.Transaction.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("EntryTime")).Select(a => a.Value).SingleOrDefault());
                        }
                    }

                    var data = (from a in context.SP_GetTaskHomeTMO(currentUser.GetCurrentUser().LoginName,
                                                                filter.Transaction.ApplicationID,
                                                                filter.Transaction.Customer,
                                                                filter.Transaction.Product,
                                                                null,
                                                                null,
                                                                null,
                                                                null,
                                                                null,
                                                                null,
                                                                filter.Transaction.LastModifiedBy,
                                                                filter.Transaction.LastModifiedDate)

                                select new
                                {
                                    #region Workflow Context
                                    SPWebID = a.SPWebID,
                                    SPSiteID = a.SPSiteID,
                                    SPListID = a.SPListID,
                                    SPListItemID = a.SPListItemID,
                                    SPTaskListID = a.SPTaskListID,
                                    SPTaskListItemID = a.SPTaskListItemID,
                                    ApproverID = a.WorkflowApproverID,
                                    Initiator = a.WorkflowInitiator,
                                    WorkflowID = a.WorkflowID,
                                    WorkflowName = a.WorkflowName,
                                    WorkflowInstanceID = a.WorkflowInstanceID,
                                    WorkflowState = a.WorkflowState,
                                    #endregion

                                    #region Workflow Task
                                    TaskType = a.WorkflowTaskType,
                                    ActivityTitle = a.WorkflowTaskActivityTitle,
                                    OutcomeID = a.WorkflowOutcome,
                                    #endregion

                                    #region Transaction
                                    ID = a.TransactionID,
                                    ApplicationID = a.ApplicationID,
                                    AccountNumber = a.AccountNumber,
                                    Customer = a.CustomerName,
                                    CIF = a.CIF,
                                    Product = a.ProductName,
                                    Channel = a.Mode, //Data ChannelName disimpan di Mode
                                    Currency = a.CurrencyCode,
                                    Amount = a.Amount,
                                    AmountUSD = a.AmountUSD,
                                    IsTopUrgent = a.IsTopUrgent == "Top Urgent Chain" ? 2 : a.IsTopUrgent == "Top Urgent" ? 1 : 0,
                                    IsFXTransaction = (a.IsFXTransaction == "Yes") ? true : false,
                                    LastModifiedBy = a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate,
                                    ProductID = a.ProductID
                                    #endregion

                                }).ToList();

                    userTaskGrid.Page = page.Value;
                    userTaskGrid.Size = size.Value;
                    userTaskGrid.Total = data.Count();

                    userTaskGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };

                    string orderBy = string.Format("IsTopUrgent DESC,{0} {1}", sortColumn, sortOrder);

                    userTaskGrid.Rows = data.AsQueryable().OrderBy(orderBy).Select((a, i) => new SPUserTaskRow()
                    {
                        RowID = i + 1,

                        #region Workflow Context
                        WorkflowContext = new WorkflowContext()
                        {
                            SPWebID = a.SPWebID,
                            SPSiteID = a.SPSiteID,
                            SPListID = a.SPListID,
                            SPListItemID = a.SPListItemID,
                            SPTaskListID = a.SPTaskListID.Value,
                            SPTaskListItemID = a.SPTaskListItemID.Value,
                            ApproverID = a.ApproverID,
                            Initiator = a.Initiator,
                            WorkflowInstanceID = a.WorkflowInstanceID,
                            WorkflowID = a.WorkflowID.Value,
                            WorkflowName = a.WorkflowName,
                            StateID = a.WorkflowState.Value
                        },
                        #endregion

                        #region Workflow Task
                        CurrentTask = new WorkflowTask()
                        {
                            TaskTypeID = a.TaskType.Value,
                            ActivityTitle = a.ActivityTitle,
                            OutcomeID = a.OutcomeID
                        },
                        #endregion

                        #region Transaction
                        Transaction = new TransactionTaskModel()
                        {
                            ID = a.ID,
                            ApplicationID = a.ApplicationID,
                            DebitAccNumber = a.AccountNumber,
                            Customer = a.Customer,
                            CIF = a.CIF,
                            Product = a.Product,
                            Channel = a.Channel,
                            Currency = a.Currency,
                            Amount = a.Amount,
                            AmountUSD = a.AmountUSD,
                            IsTopUrgent = a.IsTopUrgent,
                            IsFXTransaction = a.IsFXTransaction,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate,
                            ProductID = (int)a.ProductID
                        }
                        #endregion

                    }).Skip(skip).Take(take).ToList();
                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.ToString();
                }
            }

            return isSuccess;
        }

        //end Basri

        #endregion

        #region Home LOAN
        public bool GetHomeLOANTasks(Guid webID, Guid siteID, IList<Guid> workflowIDs, IList<string> state, IList<string> outcome, IList<string> customOutcome, bool showContribute, bool showActiveTask, int? page, int? size, string sortColumn, string sortOrder, IList<SPUserTaskFilter> filters, ref SPUserTaskGrid userTaskGrid, ref string message)
        {
            bool isSuccess = false;

            using (DBSEntities context = new DBSEntities())
            {
                try
                {
                    int skip = (page.Value - 1) * size.Value;
                    int take = size.Value;

                    SPUserTask filter = new SPUserTask();
                    filter.WorkflowContext = new WorkflowContext();
                    filter.CurrentTask = new WorkflowTask();
                    filter.ProgressTask = new WorkflowTask();
                    filter.Transaction = new TransactionTaskModel();
                    filter.Transaction.IsFXTransaction = null;

                    if (filters != null)
                    {
                        filter.Transaction.BranchCode = filters.Where(sp => sp.Field.Equals("BranchCode")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.BranchName = filters.Where(sp => sp.Field.Equals("BranchName")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.ApplicationID = filters.Where(sp => sp.Field.Equals("ApplicationID")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.Customer = filters.Where(sp => sp.Field.Equals("Customer")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.Product = filters.Where(sp => sp.Field.Equals("Product")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.Currency = filters.Where(sp => sp.Field.Equals("Currency")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.DebitAccNumber = (string)filters.Where(sp => sp.Field.Equals("DebitAccNumber")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.IsFXTransactionValue = filters.Where(sp => sp.Field.Equals("IsFXTransaction")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.IsTopUrgentValue = filters.Where(sp => sp.Field.Equals("IsTopUrgent")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.TransactionStatus = filters.Where(sp => sp.Field.Equals("TransactionStatus")).Select(sp => sp.Value).SingleOrDefault();
                        filter.Transaction.LastModifiedBy = filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                        string amount = filters.Where(sp => sp.Field.Equals("Amount")).Select(sp => sp.Value).SingleOrDefault();
                        if (!string.IsNullOrEmpty(amount))
                        {
                            decimal valueAmount;
                            bool resultAmount = decimal.TryParse(amount, out valueAmount);
                            if (resultAmount)
                            {
                                filter.Transaction.Amount = valueAmount;
                            }
                            else
                            {
                                userTaskGrid.Page = page.Value;
                                userTaskGrid.Size = size.Value;
                                userTaskGrid.Total = 0;
                                return true;
                            }
                        }

                        if (filters.Where(a => a.Field.Equals("EntryTime")).Any())
                        {
                            filter.Transaction.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("EntryTime")).Select(a => a.Value).SingleOrDefault());
                        }
                    }

                    var data = (from a in context.SP_GetTaskHomeLOAN(currentUser.GetCurrentUser().LoginName,
                                                                filter.Transaction.ApplicationID,
                                                                filter.Transaction.Customer,
                                                                filter.Transaction.Product,
                                                                filter.Transaction.Currency,
                                                                filter.Transaction.Amount,
                                                                filter.Transaction.DebitAccNumber,
                                                                filter.Transaction.IsFXTransactionValue,
                                                                filter.Transaction.IsTopUrgentValue,
                                                                filter.Transaction.TransactionStatus,
                                                                filter.Transaction.BranchCode,
                                                                filter.Transaction.BranchName,
                                                                filter.Transaction.LastModifiedBy,
                                                                filter.Transaction.LastModifiedDate)
                                select new
                                {
                                    #region Workflow Context
                                    SPWebID = a.SPWebID,
                                    SPSiteID = a.SPSiteID,
                                    SPListID = a.SPListID,
                                    SPListItemID = a.SPListItemID,
                                    SPTaskListID = a.SPTaskListID,
                                    SPTaskListItemID = a.SPTaskListItemID,
                                    ApproverID = a.WorkflowApproverID,
                                    Initiator = a.WorkflowInitiator,
                                    WorkflowID = a.WorkflowID,
                                    WorkflowName = a.WorkflowName,
                                    WorkflowInstanceID = a.WorkflowInstanceID,
                                    WorkflowState = a.WorkflowState,
                                    #endregion

                                    #region Workflow Task
                                    TaskType = a.WorkflowTaskType,
                                    ActivityTitle = a.WorkflowTaskActivityTitle,
                                    OutcomeID = a.WorkflowOutcome,
                                    #endregion

                                    #region Transaction
                                    ID = a.TransactionID,
                                    ApplicationID = a.ApplicationID,
                                    AccountNumber = a.AccountNumber,
                                    Customer = a.CustomerName,
                                    Product = a.ProductName,
                                    Currency = a.CurrencyCode,
                                    Amount = a.Amount,
                                    AmountUSD = a.AmountUSD,
                                    IsTopUrgentValue = a.IsTopUrgent,
                                    IsTopUrgent = a.IsTopUrgent == "Top Urgent Chain" ? 2 : a.IsTopUrgent == "Top Urgent" ? 1 : 0,
                                    IsFXTransaction = (a.IsFXTransaction == "Yes") ? true : false,
                                    IsFXTransactionValue = a.IsFXTransaction,
                                    BranchCode = a.BranchCode,
                                    BranchName = a.BranchName,
                                    LastModifiedBy = a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate,
                                    ProductID = a.ProductID,
                                    TransactionStatus = a.TransactionStatus,
                                    CBOOrderNo = a.CBOOrderNo
                                    #endregion
                                }).ToList();

                    userTaskGrid.Page = page.Value;
                    userTaskGrid.Size = size.Value;
                    userTaskGrid.Total = data.Count();

                    userTaskGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };

                    string orderBy = string.Format("IsTopUrgent DESC,{0} {1}", sortColumn, sortOrder);

                    #region Set CBO Account order
                    bool isCBO = false;
                    isCBO = IsCBOAccount(currentUser.GetCurrentUser().LoginName);
                    if (isCBO)
                    {
                        orderBy = string.Format("CBOOrderNo ASC,IsTopUrgent DESC,{0} {1}", sortColumn, "ASC");
                    }
                    #endregion

                    userTaskGrid.Rows = data.AsQueryable().OrderBy(orderBy).Select((a, i) => new SPUserTaskRow()
                    {
                        RowID = i + 1,

                        #region Workflow Context
                        WorkflowContext = new WorkflowContext()
                        {
                            SPWebID = a.SPWebID,
                            SPSiteID = a.SPSiteID,
                            SPListID = a.SPListID,
                            SPListItemID = a.SPListItemID,
                            SPTaskListID = a.SPTaskListID.Value,
                            SPTaskListItemID = a.SPTaskListItemID.Value,
                            Initiator = a.Initiator, //Add by Fandi
                            ApproverID = a.ApproverID,
                            WorkflowInstanceID = a.WorkflowInstanceID,
                            WorkflowID = a.WorkflowID.Value,
                            WorkflowName = a.WorkflowName,
                            StateID = a.WorkflowState.Value
                        },
                        #endregion

                        #region Workflow Task
                        CurrentTask = new WorkflowTask()
                        {
                            TaskTypeID = a.TaskType.Value,
                            ActivityTitle = a.ActivityTitle,
                            OutcomeID = a.OutcomeID
                        },
                        #endregion

                        #region Transaction
                        Transaction = new TransactionTaskModel()
                        {
                            ID = a.ID,
                            ApplicationID = a.ApplicationID,
                            Customer = a.Customer,
                            Product = a.Product,
                            Currency = a.Currency,
                            Amount = a.Amount,
                            AmountUSD = a.AmountUSD,
                            IsTopUrgent = a.IsTopUrgent,
                            IsTopUrgentValue = a.IsTopUrgentValue,
                            IsFXTransaction = a.IsFXTransaction,
                            IsFXTransactionValue = a.IsFXTransactionValue,
                            TransactionStatus = a.TransactionStatus,
                            BranchCode = a.BranchCode,
                            BranchName = a.BranchName,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate,
                            DebitAccNumber = a.AccountNumber,
                            ProductID = (int)a.ProductID
                        }
                        #endregion
                    }).Skip(skip).Take(take).ToList();
                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.ToString();
                }
            }

            return isSuccess;
        }

        #endregion

    }
    #endregion

}