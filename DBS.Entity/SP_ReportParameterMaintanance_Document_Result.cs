//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DBS.Entity
{
    using System;
    
    public partial class SP_ReportParameterMaintanance_Document_Result
    {
        public Nullable<long> LogID { get; set; }
        public string TableName { get; set; }
        public string UserName { get; set; }
        public string LogCode { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public string FieldName { get; set; }
        public string Before { get; set; }
        public string After { get; set; }
        public string CIF { get; set; }
        public string StatementLetterName { get; set; }
        public string CurrencyCode { get; set; }
        public Nullable<System.DateTime> DateUnderlying { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public string Attachment { get; set; }
        public string DocTypeName { get; set; }
        public string UnderlyingDocName { get; set; }
        public string SuplierName { get; set; }
    }
}
