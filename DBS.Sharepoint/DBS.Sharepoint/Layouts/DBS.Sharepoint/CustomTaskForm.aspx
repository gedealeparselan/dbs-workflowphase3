﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomTaskForm.aspx.cs" Inherits="DBS.Sharepoint.Layouts.DBS.Sharepoint.CustomTaskForm" DynamicMasterPageFile="~masterurl/default.master" %>

<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">

</asp:Content>

<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <table border="0" cellpadding="2" cellspacing="0" width="100%">
    <tr>
        <td colspan="2">
            <h1><asp:Label ID="HeaderLabel" runat="server"></asp:Label></h1>
            <hr />
        </td>
    </tr>
    <tr>
        <td colspan="2"><asp:PlaceHolder ID="ControlPlaceHolder" runat="server"></asp:PlaceHolder></td>
    </tr>
    <tr>
        <td colspan="2"><hr /></td>
    </tr>
    <tr>
        <td>
            <asp:LinkButton ID="CloseButton" runat="server" OnClick="CloseButton_Click" CausesValidation="false">
                &nbsp;Close            
            </asp:LinkButton>
        </td>
        <td align="right">
            <asp:LinkButton ID="CompleteTaskButton" runat="server" OnClick="CompleteTaskButton_Click" CausesValidation="true">
                &nbsp;Complete Task           
            </asp:LinkButton>
        </td>
    </tr>
    </table>
</asp:Content>

<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
   Custom Task Page
</asp:Content>

<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server" >
    <asp:Literal ID="PageTitleInTitleAreaLiteral" runat="server"></asp:Literal>
</asp:Content>
