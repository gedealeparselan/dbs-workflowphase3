﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Web;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using System.Data.Entity.Validation;
using System.Diagnostics;


namespace DBS.WebAPI.Models
{
    #region property
    [ModelName("MatrixDOAOperator")]
    public class MatrixDOAOperatorModel
    {
        /// <summary>
        /// Exception Handling Operator ID
        /// </summary>
        public int ID { get; set; }



        /// <summary>
        /// Exception Handling Operator Code
        /// </summary>
        public string Code { get; set; }


        /// <summary>
        /// Exception Handling Operator Name
        /// </summary>
        public string Name { get; set; }


        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>

        public string LastModifiedBy { get; set; }
    }

    public class MatrixDOAOperatorRow : MatrixDOAOperatorModel
    {
        public int RowID { get; set; }
    }

    public class MatrixDOAOperatorGrid : Grid
    {
        public IList<MatrixDOAOperatorRow> Rows { get; set; }
    }
    #endregion

    #region Filter

    public class MatrixDOAOperatorFilter : Filter { }
    #endregion

    #region Interface
    public interface IMatrixDOAOperatorRepository : IDisposable
    {
        bool GetMatrixDOAOperator(ref IList<MatrixDOAOperatorModel> MatrixDOAoperator, ref string message);
    }
    #endregion

    #region Repository
    public class MatrixDOAOperatorRepository : IMatrixDOAOperatorRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();
        public bool GetMatrixDOAOperator(ref IList<MatrixDOAOperatorModel> MatrixDOAoperator, ref string message)
        {
            bool isSuccess = false;
            try
            {

                using (DBSEntities context = new DBSEntities())
                {
                    MatrixDOAoperator = (from a in context.GeneralOperators
                                                 orderby new { a.NAME }
                                                 select new MatrixDOAOperatorModel
                                                 {
                                                     ID = a.OperatorID,
                                                     Code = a.Code,
                                                     Name = a.NAME,
                                                     LastModifiedBy = a.CreateBy,
                                                     LastModifiedDate = a.CreateDate
                                                 }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;

        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        currentUser = null;
                        context.Dispose();
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}