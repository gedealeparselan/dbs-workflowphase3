/**
 * Created by Roman Bangkit S on 10/13/2014.
 */


var accessToken;

var ProductModel = {
    ID: ko.observable(0),
    Code: ko.observable(""),
    Name: ko.observable(""),
    WorkflowID: ko.observable(""),
    Workflow: ko.observable("")
};

var SingleValueParameter = function(data) {
    var self = this;

    self.ID = ko.observable(data.ID);
    self.Name = ko.observable(data.Name);
    self.Value = ko.observable(data.Value);
    self.LastModifiedDate = ko.observable(data.LastModifiedDate);
    self.LastModifiedBy = ko.observable(data.LastModifiedBy);
};

var SelectedModel = {
    Product: ko.observable()
};

var ViewModel = function () {
    var self = this;

    //Parameter
    self.ThresholdAmount = ko.observable();
    self.Product = ko.observable();
    self.Products = ko.observableArray([ProductModel]);

    // Options selected value
    self.Selected = ko.observable(SelectedModel);

    var ThresholdAmountData = {
        ID: 0,
        Name: "CALLBACK_AMOUNT_TRESHOLD",
        Value: self.ThresholdAmount,
        LastModifiedDate: new Date(),
        LastModifiedBy: "Unknown"
    }

    var ProductData = {
        ID: 0,
        Name: "CALLBACK_AMOUNT_TRESHOLD_ID",
        Value: self.Product,
        LastModifiedDate: new Date(),
        LastModifiedBy: "Unknown"
    };

    //Body
    self.ThresholdAmountData = ko.observable(new SingleValueParameter(ThresholdAmountData));
    self.ProductData = ko.observable(new SingleValueParameter(ProductData));

    //Get Data
    self.GetThresholdAmount = function() { GetThresholdAmount(); }
    self.GetProductID = function() { GetProductId(); };

    //Get ThresholdAmount
    function GetThresholdAmount() {
        var options = {
            url: api.server + api.url.callbackcriteriaamount,
            params: {
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetThresholdAmount, OnError, OnAlways);
    }

    function OnSuccessGetThresholdAmount(data, textStatus, jqXHR){
        if(jqXHR.status = 200){
            // bind result to observable array
            self.ThresholdAmount(data);
        }else{
            ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }

    //Get ProductID
    function GetProductId() {
        var options = {
            url: api.server + api.url.callbackcriteriaid,
            params: {
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetProductId, OnError, OnAlways);
    }

    function OnSuccessGetProductId(data, textStatus, jqXHR){
        if(jqXHR.status = 200){
            // bind result to observable array
            self.Product(data.Message);
            GetParameters();
        }else{
            ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }

    //Get parameters
    function GetParameters(){
        var options = {
            url: api.server + api.url.parameter,
            params: {
                select: "Product"
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetParameters, OnError, OnAlways);
    }

    function OnSuccessGetParameters(data, textStatus, jqXHR){
        if(jqXHR.status = 200){
            // bind result to observable array
            self.Products(ko.toJS(data.Product));

            //Set Selected Product
            var product = ko.utils.arrayFirst(self.Products(), function (item) { return item.ID == self.Product(); });
            if(product != null) self.Selected().Product(self.Product());
        }else{
            ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }

    //Update ThresholdAmount
    self.UpdateCallBackCriteria = function() { UpdateCallBackCriteria(); }

    function UpdateCallBackCriteria() {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if(form.valid()) {
            var thresholdamount = {
                url: api.server + api.url.singlevalueparameter + "/" + self.ThresholdAmountData().Name(),
                data: ko.toJSON(self.ThresholdAmountData()),
                params: {
                },
                token: accessToken
            };

            Helper.Ajax.Put(thresholdamount, OnSuccessUpdateThresholdAmount, OnError, OnAlways);

            var product = {
                url: api.server + api.url.singlevalueparameter + "/" + self.ProductData().Name(),
                data: ko.toJSON(self.ProductData()),
                params: {
                },
                token: accessToken
            };

            Helper.Ajax.Put(product, OnSuccessUpdateProductID, OnError, OnAlways);
        }
    }

    function OnSuccessUpdateThresholdAmount(data, textStatus, jqXHR){
        if(jqXHR.status = 200){
            // bind result to observable array
            ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseText, 'gritter-success', false);
        }else{
            ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }

    function OnSuccessUpdateProductID(data, textStatus, jqXHR){
        if(jqXHR.status = 200){
            // bind result to observable array
            ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseText, 'gritter-success', false);
        }else{
            ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }

    self.HelperGetSelectedProduct = ko.dependentObservable(function () {
        var product = ko.utils.arrayFirst(self.Products(), function (item) { return item.ID == self.Selected().Product(); });

        if(product != null) { self.Product(product.ID); }
    });
}

$(document).ready(function () {
    // Knockout View Model
    var viewModel = new ViewModel();

    // Knockout Bindings
    ko.applyBindings(viewModel);

    // Token Validation
    if($.cookie(api.cookie.name) == undefined){
        Helper.Token.Request(TokenOnSuccess, TokenOnError);
    }else{
        // read token from cookie
        accessToken = $.cookie(api.cookie.name);

        // call spuser function
        //GetCurrentUser(viewModel);

        // call get data inside view model
        viewModel.GetThresholdAmount();
        viewModel.GetProductID();
    }

    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }
    });
});

function ShowNotification(title, text, className, isSticky){
    $.gritter.add({
        title: title,
        text: text,
        class_name: className,
        sticky: isSticky
    });
}

function OnError(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}

function OnAlways(){
    //$box.trigger('reloaded.ace.widget');
}