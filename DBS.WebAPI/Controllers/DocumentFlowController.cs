﻿using DBS.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class DocumentFlowController : ApiController
    {
        private string message = string.Empty;

        private readonly IDocumentFlowRepository repository = null;
        private readonly IRecipientRepository repoRecipient = null; // david


        public DocumentFlowController()
        {
            this.repository = new DocumentFlowRepository();
            this.repoRecipient = new RepicientRepository();
        }

        public DocumentFlowController(IDocumentFlowRepository repo)
        {
            this.repository = repo;
        }
        public DocumentFlowController(IRecipientRepository repo)
        {
            this.repoRecipient = repo;
        } // david



        [ResponseType(typeof(IList<RecipientModel>))]
        [HttpGet]
        [Route("api/DocumentFlow/Recipient")]
        public HttpResponseMessage GetRecipient()
        {
            IList<RecipientModel> output = new List<RecipientModel>();

            if (!repository.GetRecepient(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<RecipientModel>>(HttpStatusCode.OK, output);
            }
        }

        //[ResponseType(typeof(IList<TransactionTypeDFModel>))]
        //[HttpGet]
        //[Route("api/DocumentFlow/TransactionTypeDF")]
        //public HttpResponseMessage GetTransactionType()
        //{
        //    IList<TransactionTypeDFModel> output = new List<TransactionTypeDFModel>();

        //    if (!repository.GetTransactionType(ref output, ref message))
        //    {
        //        return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
        //    }
        //    else
        //    {
        //        return Request.CreateResponse<IList<TransactionTypeDFModel>>(HttpStatusCode.OK, output);
        //    }
        //}

        [ResponseType(typeof(IList<TransactionDFModel>))]
        [HttpGet]
        [Route("api/DocumentFlow/ClientSearch")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<TransactionDFModel> output = new List<TransactionDFModel>();

            if (!repository.GetTransactionDFByClient(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<TransactionDFModel>>(HttpStatusCode.OK, output);
            }
        }

        [HttpPost]
        [ResponseType(typeof(string))]
        [Route("api/DocumentFlow/SubmitTrans")]
        public HttpResponseMessage Post(DocumentFlowModel data)
        {
            long transactionID = 0;
            string applicationID = string.Empty;
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddTransactionDF(data, ref transactionID, ref  applicationID, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new
                    {
                        ID = transactionID,
                        AppID = applicationID,
                        Message = data.IsDraft ? "Transaction has been saved as draft." : "New Transaction has been submitted."
                    });
                }
            }
        }

        [HttpDelete]
        [Route("api/DocumentFlow/DeleteDraft/{id}")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage DeleteDraft(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteTransactionDraftByID(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Draft." });
                }
            }
        }

        [ResponseType(typeof(TransactionDocumentFlowModel))]
        [HttpGet]
        [Route("api/DocumentFlow/{workflowInstanceID}/TransactionDF/{approverID}")]
        public HttpResponseMessage GetTransactionDF(Guid workflowInstanceID, long approverID)
        {
            DocumentFlowDataModel transaction = new DocumentFlowDataModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TransactionDocumentFlowModel output = new TransactionDocumentFlowModel();

            try
            {
                // #1 Get Transaction Details
                if (!repository.GetTransactionDF(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repository.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        output.Transaction = transaction;
                        output.Timelines = timelines;
                        return Request.CreateResponse(HttpStatusCode.OK, output);
                    }
                }
            }
            finally
            {

                transaction = null;
                timelines = null;
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/DocumentFlow/{workflowInstanceID}/approval/{approverID}")]
        public HttpResponseMessage AddTransactionCheckerDetails(Guid workflowInstanceID, long approverID, TransactionDocumentFlowModel transactionChecker)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddTransactionChecker(workflowInstanceID, approverID, transactionChecker, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been saved." });
                }
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/DocumentFlow/{workflowInstanceID}/Transaction/Maker/Add/{approverID}")]
        public HttpResponseMessage AddTransactionDFMakerDetails(Guid workflowInstanceID, long approverID, DocumentFlowModel transaction)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddTransactionDFMaker(workflowInstanceID, approverID, transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction maker data revision has been saved." });
                }
            }
        }

        [ResponseType(typeof(DocumentFlowDataModel))]
        [Route("api/DocumentFlow/Draft/{id}")]
        [HttpGet]
        public HttpResponseMessage GetDraftDF(Int64 id)
        {
            DocumentFlowDataModel output = new DocumentFlowDataModel();

            if (!repository.GetDocumentFlowDraft(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<DocumentFlowDataModel>(HttpStatusCode.OK, output);
            }
        }

        //start david
        [ResponseType(typeof(RecipientModel))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<RecipientModel> output = new List<RecipientModel>();
            if (!repoRecipient.GetRecipient(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<RecipientModel>>(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(RecipientModel))]
        [AcceptVerbs("GET", "POST")]
        [Route("api/Recipient")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<RecipientFilter> filters)
        {
            RecipientGrid output = new RecipientGrid();
            if (!repoRecipient.GetRecipient(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Code" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<RecipientGrid>(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(IList<RecipientModel>))]
        [HttpGet]
        [Route("api/Recipient/Search")]
        public HttpResponseMessage SearchRecipient(string query, int? limit)
        {
            IList<RecipientModel> output = new List<RecipientModel>();

            if (!repoRecipient.GetRecipient(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<RecipientModel>>(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(RecipientModel))]
        [HttpGet]
        [Route("api/Recipient/{id}")]
        public HttpResponseMessage Get(int id)
        {
            RecipientModel output = new RecipientModel();
            if (!repoRecipient.GetRecipientByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<RecipientModel>(HttpStatusCode.OK, output);
            }
        }

        [HttpPost]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Post(RecipientModel recipient)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoRecipient.AddRecipient(recipient, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New recipient data has been created." });
                }
            }
        }

        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/Recipient/{id}")]
        public HttpResponseMessage Put(int id, RecipientModel recipient)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoRecipient.UpdateRecipient(id, recipient, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Recipient data has been updated." });
                }
            }
        }

        [HttpDelete]
        [Route("api/Recipient/{id}")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoRecipient.DeleteRecipient(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Recipient data has been deleted." });
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
        //end david
    }
}