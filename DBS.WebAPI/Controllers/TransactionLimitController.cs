﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DBS.WebAPI.Models;
using System.Web.Http.Description;

namespace DBS.WebAPI.Controllers
{
    public class TransactionLimitController : ApiController
    {
        
        private string message = string.Empty;

        private readonly ITransactionLimitRepo repository = null;

        public TransactionLimitController()
        {
            this.repository = new TransactionLimitRepository();
        }

         public TransactionLimitController(ITransactionLimitRepo repo)
        {
            this.repository = repo;
        }

   
         /// Get all RejectCode

         [ResponseType(typeof(IList<TransactionLimitModel>))]
         public HttpResponseMessage Get()
         {
             IList<TransactionLimitModel> output = new List<TransactionLimitModel>();

             if (!repository.GetTransactionLimit(ref output, ref message))
             {
                 return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
             }
             else
             {
                 return Request.CreateResponse<IList<TransactionLimitModel>>(HttpStatusCode.OK, output);
             }
         }

       
         /// Get Reject Code with maximum rows will be retreive and select page index to display.
   
         /// <param name="limit">Total rows will be retreive, default is 10.</param>
         /// <param name="index">Select page index, default is 0.</param>
         /// <returns></returns>
         [ResponseType(typeof(IList<TransactionLimitModel>))]
         public HttpResponseMessage Get(int? limit, int? index)
         {
             IList<TransactionLimitModel> output = new List<TransactionLimitModel>();

             if (!repository.GetTransactionLimit(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
             {
                 return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
             }
             else
             {
                 return Request.CreateResponse<IList<TransactionLimitModel>>(HttpStatusCode.OK, output);
             }
         }


         /// <summary>
         /// Get Reject Code for data grid.
         /// </summary>
         /// <param name="limit">Total rows will be retreive, default is 10.</param>
         /// <param name="index">Select page index, default is 0.</param>
         /// <returns></returns>
         [ResponseType(typeof(TransactionLimitModel))]
         [AcceptVerbs("GET", "POST")]
         public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<TransactionLimitFilter> filters)
         {
             TransactionLimitGrid output = new TransactionLimitGrid();

             if (!repository.GetTransactionLimit(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
             {
                 return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
             }
             else
             {
                 return Request.CreateResponse<TransactionLimitGrid>(HttpStatusCode.OK, output);
             }
         }

         /// <summary>
         /// Find Reject Code using criterias.
         /// </summary>
         /// <param name="query">Code or Description.</param>
         /// <param name="limit">Total rows will be retreive, default is 10.</param>
         /// <returns></returns>
         [ResponseType(typeof(IList<FXComplianceModel>))]
         [HttpGet]
         [Route("api/TransactionLimit/Search")]
         public HttpResponseMessage Search(string query, int? limit)
         {
             IList<TransactionLimitModel> output = new List<TransactionLimitModel>();

             if (!repository.GetTransactionLimit(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
             {
                 return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
             }
             else
             {
                 return Request.CreateResponse<IList<TransactionLimitModel>>(HttpStatusCode.OK, output);
             }
         }

         [ResponseType(typeof(IList<TransactionLimitModel>))]
         [HttpGet]
         [Route("api/TransactionLimit/IPE")]
         public HttpResponseMessage GetIPEByProduct(int id)
         {
             IList<TransactionLimitModel> output = new List<TransactionLimitModel>();

             if (!repository.GetTransactionLimitByProduct(ref output, id, ref message))
             {
                 return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
             }
             else
             {
                 return Request.CreateResponse<IList<TransactionLimitModel>>(HttpStatusCode.OK, output);
             }
         }

         /// <summary>
         /// Get FX Compliance by ID.
         /// </summary>
         /// <param name="id">FX Compliance ID.</param>
         /// <returns></returns>
         [ResponseType(typeof(TransactionLimitModel))]
         [Route("api/TransactionLimit/{id}")]
         [HttpGet]
         public HttpResponseMessage Get(int id)
         {
             TransactionLimitModel output = new TransactionLimitModel();

             if (!repository.GetTransactionLimitByID(id, ref output, ref message))
             {
                 return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
             }
             else
             {
                 if (output == null)
                     return Request.CreateResponse(HttpStatusCode.NoContent);
                 else
                     return Request.CreateResponse<TransactionLimitModel>(HttpStatusCode.OK, output);
             }
         }


         /// <summary>
         /// Create a new reject Code.
         /// </summary>
         /// <param name="FXCompliance">FX Compliance data.</param>
         /// <returns></returns>
         [HttpPost]
         [ResponseType(typeof(string))]
         [Authorize(Roles = "DBS Admin")]
         public HttpResponseMessage Post(TransactionLimitModel TransactionLimit)
         {
             if (!ModelState.IsValid)
             {
                 return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
             }
             else
             {
                 if (!repository.AddTransactionLimit(TransactionLimit, ref message))
                 {
                     return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                 }
                 else
                 {
                     return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New Transaction Limit data has been created." });
                 }
             }
         }

         /// <summary>
         /// Modify existing Transaction Limit.
         /// </summary>
         /// <param name="id">ID of Transaction Limit to be modify.</param>
         /// <param name="FXCompliance">TransactionLimit data to be updated.</param>
         /// <returns></returns>
         [HttpPut]
         [ResponseType(typeof(string))]
         [Route("api/TransactionLimit/{id}")]
         [Authorize(Roles = "DBS Admin")]
         public HttpResponseMessage Put(int id, TransactionLimitModel TransactionLimit)
         {
             if (!ModelState.IsValid)
             {
                 return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
             }
             else
             {
                 if (!repository.UpdateTransactionLimit(id, TransactionLimit, ref message))
                 {
                     return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                 }
                 else
                 {
                     return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction Limit data has been updated." });
                 }
             }
         }

         /// <summary>
         /// Remove exisiting Transaction Limit.
         /// </summary>
         /// <param name="id">ID of Reject Code to be deleted.</param>
         /// <returns></returns>
         [HttpDelete]
         [Route("api/TransactionLimit/{id}")]
         [ResponseType(typeof(string))]
         public HttpResponseMessage Delete(int id)
         {
             if (!ModelState.IsValid)
             {
                 return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
             }
             else
             {
                 if (!repository.DeleteTransactionLimit(id, ref message))
                 {
                     return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                 }
                 else
                 {
                     return Request.CreateResponse(HttpStatusCode.OK, new { Message = "TransactionLimit data has been deleted." });
                 }
             }
         }


         protected override void Dispose(bool disposing)
         {
             if (repository != null)
                 repository.Dispose();
             base.Dispose(disposing);
         }
    }
}
