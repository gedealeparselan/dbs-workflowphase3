﻿/**
 * Created by azam komaru 08/25/2016.
 */

function Comma(Num) {
    Num += '';
    Num = Num.replace(',', ''); Num = Num.replace(',', ''); Num = Num.replace(',', '');
    Num = Num.replace(',', ''); Num = Num.replace(',', ''); Num = Num.replace(',', '');
    x = Num.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1))
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    return x1 + x2;
}


var ProductModel = function (id, name) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
}
var CCYModel = function (id, name) {
    var self = this;
    self.ID = ko.observable(id);
    self.Code = ko.observable(name);
}
var ViewModel = function () {
    //Make the self as 'this' reference
    var self = this;


    self.Readonly = ko.observable(false);
    self.IsWorkflow = ko.observable(false);
    //Declare observable which will be bind with UI
    self.ID = ko.observable("");
    self.Product = ko.observable(new ProductModel('', ''));
    self.MinAmount = ko.observable(0);
    self.MaxAmount = ko.observable(0);
    self.Currency = ko.observable(new CCYModel('', ''));
    self.LastModifiedBy = ko.observable("");
    self.LastModifiedDate = ko.observable("");

    self.CheckedMaxAmaount = ko.observable(false);

    // filter
    self.FilterProduct = ko.observable("");
    self.FilterMinAmount = ko.observable("");
    self.FilterMaxAmount = ko.observable("");
    self.FilterModifiedBy = ko.observable("");
    self.FilterModifiedDate = ko.observable("");

    self.ddlCurrency = ko.observableArray([]);
    self.ddlProduct = ko.observableArray([]);

    self.IsRoleMaker = ko.observable(false);
    self.IsPermited = ko.observable(false);

    // New Data flag
    self.IsNewData = ko.observable(false);

    // Declare an ObservableArray for Storing the JSON Response
    self.TransactionLimits = ko.observableArray([]);

    // grid properties
    self.GridProperties = ko.observable();
    self.GridProperties(new GridPropertiesModel(GetData));

    // set default sorting
    self.GridProperties().SortColumn("Product");
    self.GridProperties().SortOrder("ASC");

    // bind clear filters
    self.ClearFilters = function () {
        self.FilterProduct("");
        self.FilterMinAmount("");
        self.FilterMaxAmount("");
        self.FilterModifiedBy("");
        self.FilterModifiedDate("");

        GetData();
    };


    self.OnChangeProduct = function () {
        var ProductName = $('#Product option:selected').text();
        console.log(ProductName);
        self.Product().Name(ProductName);
    };

    self.OnChangeCurrency = function () {
        var CurrencyCode = $('#Currency option:selected').text();
        console.log(CurrencyCode);
        self.Currency().Code(CurrencyCode);

    };


    // bind get data function to view
    self.GetData = function () {
        GetData();
    };


    //bind get dropdown function to view
    self.GetDropdown = function () {
        GetDropdown();
    };

    self.SetCekBox = ko.computed(function () {
        if (self.CheckedMaxAmaount()) {
            self.MaxAmount('');
            self.MaxAmount(0);
        }
    });


    //The Object which stored data entered in the observables
    var TransactionLimit = {
        ID: self.ID,
        Product: self.Product,
        MinAmount: self.MinAmount,
        MaxAmount: self.MaxAmount,
        Currency: self.Currency,
        Unlimited: self.CheckedMaxAmaount,
        LastModifiedBy: self.LastModifiedBy,
        LastModifiedDate: self.LastModifiedDate
    };

    self.IsPermited = function (IsPermitedResult) {
        //Ajax call to delete the Customer
        spUser = $.cookie(api.cookie.spUser);

        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/CheckUserPermited/" + _spFriendlyUrlPageContextInfo.title,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    //compare user role to page permission to get checker validation
                    for (i = 0; i < spUser.Roles.length; i++) {
                        for (j = 0; j < data.length; j++) {
                            if (Const_RoleName[spUser.Roles[i].ID] == data[j]) { //if (spUser.Roles[i].Name == data[j]) {
                                if (Const_RoleName[spUser.Roles[i].ID].endsWith('Maker')) { //if (spUser.Roles[i].Name.endsWith('Maker')) {
                                    self.IsRoleMaker(true);
                                    return;
                                }
                                else {
                                    self.IsRoleMaker(false);
                                }
                            }
                        }
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    function GetDropdown() {
        $.ajax({
            async: false,
            type: "GET",
            url: api.server + api.url.parameter + '?select=Currency,Product',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status == 200) {
                    if (data['Currency'] != null, data['Product'] != null) {
                        self.ddlCurrency(data['Currency']);
                        self.ddlProduct(data['Product']);
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }

        });
    };

    self.save = function () {

        var textMinAmount = document.getElementById('MinAmount').value;
        var textMaxAmount = document.getElementById('MaxAmount').value;

        var Resultmin = textMinAmount.replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '');
        var Resultmaxn = textMaxAmount.replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '');

        self.MinAmount(Resultmin);
        self.MaxAmount(Resultmaxn);
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {

                    //Ajax call to insert the transactionlimit
                    $.ajax({
                        type: "POST",
                        url: api.server + api.url.transactionlimit,
                        data: ko.toJSON(TransactionLimit), //Convert the Observable Data into JSON
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {
                                // send notification
                                ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // hide current popup window
                                $("#modal-form").modal('hide');

                                // refresh data
                                GetData();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });
                }
            });
        }
    };

    self.update = function () {
        var textMinAmount = document.getElementById('MinAmount').value;
        var textMaxAmount = document.getElementById('MaxAmount').value;

        var Resultmin = textMinAmount.replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '');
        var Resultmaxn = textMaxAmount.replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '');

        self.MinAmount(Resultmin);
        self.MaxAmount(Resultmaxn);
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            // hide current popup window
            // $("#modal-form").modal('hide');

            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {
                    //Ajax call to update the Customer
                    $.ajax({
                        type: "PUT",
                        url: api.server + api.url.transactionlimit + "/" + TransactionLimit.ID(),
                        data: ko.toJSON(TransactionLimit),
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {
                                // send notification
                                ShowNotification('Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // hide current popup window
                                $("#modal-form").modal('hide');

                                // refresh data
                                GetData();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });
                }
            });
        }
    };

    self.delete = function () {
        // hide current popup window
        //$("#modal-form").modal('hide');
        var textMinAmount = document.getElementById('MinAmount').value;
        var textMaxAmount = document.getElementById('MaxAmount').value;

        var Resultmin = textMinAmount.replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '');
        var Resultmaxn = textMaxAmount.replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '');

        self.MinAmount(Resultmin);
        self.MaxAmount(Resultmaxn);

        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                $("#modal-form").modal('show');
            } else {
                //Ajax call to delete the Customer
                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.transactionlimit + "/" + TransactionLimit.ID(),
                    data: ko.toJSON(TransactionLimit),
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if (jqXHR.status = 200) {
                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                            // refresh data
                            GetData();
                        } else
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
            }
        });
    };

    // Local Date
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return "";
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-18
    };

    //Function to Display record to be updated
    self.GetSelectedRow = function (data) {
        self.IsNewData(false);

        if (self.IsWorkflow()) $("#modal-form-workflow").modal('show');
        else $("#modal-form").modal('show');

        self.ID(data.ID);
        //self.ProductID(data.Product);
        //self.CurrencyID(data.Currency);
        self.Product(new ProductModel(data.Product.ID, data.Product.Name));
        self.Currency(new CCYModel(data.Currency.ID, data.Currency.Code));
        self.MinAmount(formatNumber(data.MinAmount));
        self.MaxAmount(formatNumber(data.MaxAmount));
        self.CheckedMaxAmaount(data.Unlimited);
        self.LastModifiedBy(data.LastModifiedBy);
        self.LastModifiedDate(data.LastModifiedDate);
    };

    //insert new
    self.NewData = function () {
        // flag as new Data
        self.IsNewData(true);
        self.Readonly(false);
        // bind empty data
        self.ID(0);
        //self.ProductID('');  
        self.CheckedMaxAmaount(false);
        self.MinAmount(0);
        self.MaxAmount(0);
        //self.CurrencyID('');
        self.Product(new ProductModel('', ''));
        self.Currency(new CCYModel('', ''));
    };

    //untuk kebutuhan format number
    $('#MinAmount').keypress(function () {
        console.log("Masuk");


    });

    //end formate number





    //Function to Read All TransactionLimit
    function GetData() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.transactionlimit,
            params: {
                page: self.GridProperties().Page(),
                size: self.GridProperties().Size(),
                sort_column: self.GridProperties().SortColumn(),
                sort_order: self.GridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetData, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetData, OnError, OnAlways);
        }
        console.log("data" + options);
        console.log("data" + OnSuccessGetData);
    }

    // Get filtered columns value
    function GetFilteredColumns() {
        // define filter
        var filters = [];

        if (self.FilterProduct() != "") filters.push({ Field: 'Product', Value: self.FilterProduct() });
        if (self.FilterMinAmount() != "") filters.push({ Field: 'MinAmount', Value: self.FilterMinAmount() });
        if (self.FilterMaxAmount() != "") filters.push({ Field: 'MaxAmount', Value: self.FilterMaxAmount() });
        if (self.FilterModifiedBy() != "") filters.push({ Field: 'LastModifiedBy', Value: self.FilterModifiedBy() });
        if (self.FilterModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.FilterModifiedDate() });

        return filters;
    };

    // On success GetData callback
    function OnSuccessGetData(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.TransactionLimits(data.Rows);
            console.log(data);
            console.log(data.Rows);

            self.GridProperties().Page(data['Page']);
            self.GridProperties().Size(data['Size']);
            self.GridProperties().Total(data['Total']);
            self.GridProperties().TotalPages(Math.ceil(self.GridProperties().Total() / self.GridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    // On Error callback
    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    // On Always callback
    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }
    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }

    });
};