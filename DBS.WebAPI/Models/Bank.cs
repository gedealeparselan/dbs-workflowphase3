﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using System.Data.Entity.SqlServer;

namespace DBS.WebAPI.Models
{
    #region Property
    [ModelName("Bank")]
    public class BankModel
    {
        /// <summary>
        /// Bank ID.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Bank Code.
        /// </summary>
        // [MaxLength(50, ErrorMessage = "Bene Segment Name is must be in {1} characters.")]
        public string Code { get; set; }

        /// <summary>
        /// Branch Code.
        /// </summary>
        //[MaxLength(255, ErrorMessage = "Maximum length is {1} characters.")]
        public string BranchCode { get; set; }

        /// <summary>
        /// Swift Code.
        /// </summary>
        //[MaxLength(255, ErrorMessage = "Maximum length is {1} characters.")]
        public string SwiftCode { get; set; }

        /// <summary>
        /// Bank A/C.
        /// </summary>
        //[MaxLength(255, ErrorMessage = "Maximum length is {1} characters.")]
        public string BankAccount { get; set; }

        /// <summary>
        /// Bank Description.
        /// </summary>
        //[MaxLength(255, ErrorMessage = "Maximum length is {1} characters.")]
        public string Description { get; set; }

        /// <summary>
        /// Common Name.
        /// </summary>
        //[MaxLength(255, ErrorMessage = "Maximum length is {1} characters.")]
        public string CommonName { get; set; }

        /// <summary>
        /// Currency.
        /// </summary>
        //[MaxLength(255, ErrorMessage = "Maximum length is {1} characters.")]
        public string Currency { get; set; }

        /// <summary>
        /// Currency ID
        /// </summary>        
        public string CurrencyID { get; set; }

        /// <summary>
        /// PGSL.
        /// </summary>
        //[MaxLength(255, ErrorMessage = "Maximum length is {1} characters.")]
        public string PGSL { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }
        public bool IsDelete { get; set; }

        public IList<BranchModel> IBranchBank { get; set; }
        public BranchModel BranchBank { get; set; }

        public bool? IsRTGS { get; set; }
        public bool? IsSKN { get; set; }
        public bool? IsOTT { get; set; }
        public bool? IsOVB { get; set; }
    }

    #region IPE
    public class BankBranchModel
    {
        /// <summary>
        /// Bank ID.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Bank Code.
        /// </summary>
        // [MaxLength(50, ErrorMessage = "Bene Segment Name is must be in {1} characters.")]
        public string Code { get; set; }

        /// <summary>
        /// Branch Code.
        /// </summary>
        //[MaxLength(255, ErrorMessage = "Maximum length is {1} characters.")]
        public string BranchCode { get; set; }

        /// <summary>
        /// Swift Code.
        /// </summary>
        //[MaxLength(255, ErrorMessage = "Maximum length is {1} characters.")]
        public string SwiftCode { get; set; }

        /// <summary>
        /// Bank A/C.
        /// </summary>
        //[MaxLength(255, ErrorMessage = "Maximum length is {1} characters.")]
        public string BankAccount { get; set; }

        /// <summary>
        /// Bank Description.
        /// </summary>
        //[MaxLength(255, ErrorMessage = "Maximum length is {1} characters.")]
        public string Description { get; set; }

        /// <summary>
        /// Common Name.
        /// </summary>
        //[MaxLength(255, ErrorMessage = "Maximum length is {1} characters.")]
        public string CommonName { get; set; }

        /// <summary>
        /// Currency.
        /// </summary>
        //[MaxLength(255, ErrorMessage = "Maximum length is {1} characters.")]
        public string Currency { get; set; }

        /// <summary>
        /// Currency ID
        /// </summary>        
        public string CurrencyID { get; set; }

        /// <summary>
        /// PGSL.
        /// </summary>
        //[MaxLength(255, ErrorMessage = "Maximum length is {1} characters.")]
        public string PGSL { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }
        public bool IsDelete { get; set; }
        public IList<BranchModel> IBranchBank { get; set; }
        public BranchModel BranchBank { get; set; }
        //public CurrencyModel CRY { get; set; }
        public string CityCode { get; set; }
        public bool? IsRTGS { get; set; }
        public bool? IsSKN { get; set; }
        public bool? IsOTT { get; set; }
        public bool? IsOVB { get; set; }
    }
    #endregion

    public class BankRow : BankModel
    {
        public int RowID { get; set; }
    }


    public class BankGrid : Grid
    {
        public IList<BankRow> Rows { get; set; }
    }


    #endregion

    #region Filter
    public class BankFilter : Filter { }
    #endregion

    #region Interface
    public interface IBankRepository : IDisposable
    {
        bool GetBankByID(int id, ref BankModel banks, ref string message);
        bool GetBank(ref IList<BankModel> banks, int limit, int index, ref string message);
        bool GetBank(ref IList<BankModel> banks, ref string message);
        bool GetBank(int page, int size, IList<BankFilter> filters, string sortColumn, string sortOrder, ref BankGrid banks, ref string message);

        bool GetBank(BankFilter filter, ref IList<BankModel> banks, ref string message);
        bool GetBank(string key, int limit, ref IList<BankModel> banks, ref string message);
        bool GetBank(string key, int limit, ref IList<BankBranchModel> bankbranch, ref string message);
        #region IPE
        bool GetBank(string key, int limit, string name, ref IList<BankBranchModel> bankbranch, ref string message);
        bool GetBank(int key, ref IList<BankBranchModel> bankbranch, ref string message);
        bool AddBank(BankModel banks, ref string message);
        #endregion
        bool UpdateBank(int id, BankModel banks, ref string message);
        bool DeleteBank(int id, ref string message);

    }
    #endregion

    #region Repository
    public class BankRepository : IBankRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetBankByID(int id, ref BankModel bank, ref string message)
        {
            bool isSuccess = false;

            try
            {
                bank = (from a in context.Banks
                        where a.IsDeleted.Equals(false) && a.BankID.Equals(id)
                        select new BankModel
                        {
                            ID = a.BankID,
                            Code = a.BankCode,
                            BranchCode = a.BranchCode,
                            SwiftCode = a.SwiftCode,
                            BankAccount = a.BankAccount,
                            Description = a.BankDescription,
                            CommonName = a.CommonName,
                            Currency = a.Currency,
                            PGSL = a.PGSL,
                            LastModifiedDate = a.CreateDate,
                            LastModifiedBy = a.CreateBy,
                            //aridya 20161005 get IBranchBank
                            IBranchBank = (from ab in context.Branches
                                           join b in context.Banks on ab.BankID equals b.BankID
                                           join c in context.Cities on ab.CityID equals c.CityID
                                           where b.BankID == a.BankID
                                           select new BranchModel
                                           {
                                               ID = ab.BranchID,
                                               CityID = ab.CityID,
                                               Code = ab.BranchCode,
                                               Name = ab.CommonName,
                                               CityDescription = c.Description,
                                               CityCode = c.CityCode
                                           }).ToList()
                            //end add aridya
                        }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetBank(ref IList<BankModel> banks, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    banks = (from a in context.Banks
                             where a.IsDeleted.Equals(false)
                             orderby new { a.BankCode, a.BranchCode, a.SwiftCode, a.BankAccount, a.BankDescription, a.CommonName, a.Currency, a.PGSL }
                             select new BankModel
                             {
                                 ID = a.BankID,
                                 Code = a.BankCode,
                                 BranchCode = a.BranchCode,
                                 SwiftCode = a.SwiftCode,
                                 BankAccount = a.BankAccount,
                                 Description = a.BankDescription,
                                 CommonName = a.CommonName,
                                 Currency = a.Currency,
                                 PGSL = a.PGSL,
                                 IsRTGS = a.IsRTGS,
                                 IsSKN = a.IsSKN,
                                 IsOVB = a.IsOVB,
                                 IsOTT = a.IsOTT,
                                 LastModifiedBy = a.CreateBy,
                                 LastModifiedDate = a.CreateDate
                             }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetBank(ref IList<BankModel> banks, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    banks = (from a in context.Banks
                             where a.IsDeleted.Equals(false)
                             orderby new { a.BankDescription, a.BankCode }
                             select new BankModel
                             {
                                 ID = a.BankID,
                                 Code = a.BankCode,
                                 BranchCode = a.BranchCode,
                                 SwiftCode = a.SwiftCode,
                                 BankAccount = a.BankAccount,
                                 Description = a.BankDescription,
                                 CommonName = a.CommonName,
                                 Currency = a.Currency,
                                 PGSL = a.PGSL,
                                 LastModifiedBy = a.CreateBy,
                                 LastModifiedDate = a.CreateDate
                             }).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;

        }

        public bool GetBank(int page, int size, IList<BankFilter> filters, string sortColumn, string sortOrder, ref BankGrid BankGrid, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                BankModel filter = new BankModel();
                //filter.Currency = new CurrencyModel { Code = string.Empty };

                if (filters != null)
                {
                    filter.Code = (string)filters.Where(a => a.Field.Equals("Code")).Select(a => a.Value).SingleOrDefault();
                    filter.BranchCode = (string)filters.Where(a => a.Field.Equals("BranchCode")).Select(a => a.Value).SingleOrDefault();
                    filter.SwiftCode = (string)filters.Where(a => a.Field.Equals("SwiftCode")).Select(a => a.Value).SingleOrDefault();
                    filter.BankAccount = (string)filters.Where(a => a.Field.Equals("BankAccount")).Select(a => a.Value).SingleOrDefault();
                    filter.Description = (string)filters.Where(a => a.Field.Equals("Description")).Select(a => a.Value).SingleOrDefault();
                    filter.CommonName = (string)filters.Where(a => a.Field.Equals("CommonName")).Select(a => a.Value).SingleOrDefault();
                    filter.Currency = (string)filters.Where(a => a.Field.Equals("Currency")).Select(a => a.Value).SingleOrDefault();
                    filter.PGSL = (string)filters.Where(a => a.Field.Equals("PGSL")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.Banks
                                join b in context.Currencies on new { Con = a.Currency } equals new { Con = SqlFunctions.StringConvert((decimal)b.CurrencyID).Trim() } into temp
                                from b in temp.DefaultIfEmpty()
                                let isBankCode = !string.IsNullOrEmpty(filter.Code)
                                let isBranchCode = !string.IsNullOrEmpty(filter.BranchCode)
                                let isSwiftCode = !string.IsNullOrEmpty(filter.SwiftCode)
                                let isBankAccount = !string.IsNullOrEmpty(filter.BankAccount)
                                let isBankDescription = !string.IsNullOrEmpty(filter.Description)
                                let isCommonName = !string.IsNullOrEmpty(filter.CommonName)
                                let isCurrency = !string.IsNullOrEmpty(filter.Currency)
                                let isPGSL = !string.IsNullOrEmpty(filter.PGSL)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) &&
                                    (isBankCode ? a.BankCode.Contains(filter.Code) : true) &&
                                    (isBranchCode ? a.BranchCode.Contains(filter.BranchCode) : true) &&
                                    (isSwiftCode ? a.SwiftCode.Contains(filter.SwiftCode) : true) &&
                                    (isBankAccount ? a.BankAccount.Contains(filter.BankAccount) : true) &&
                                    (isBankDescription ? a.BankDescription.Contains(filter.Description) : true) &&
                                    (isCommonName ? a.CommonName.Contains(filter.CommonName) : true) &&
                                    (isCurrency ? b.CurrencyCode.Contains(filter.Currency) : true) &&
                                    (isPGSL ? a.PGSL.Contains(filter.PGSL) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new BankModel
                                {
                                    ID = a.BankID,
                                    Code = a.BankCode,
                                    Description = a.BankDescription,
                                    BranchCode = a.BranchCode,
                                    SwiftCode = a.SwiftCode,
                                    BankAccount = a.BankAccount,
                                    CommonName = a.CommonName,
                                    Currency = b.CurrencyCode,
                                    CurrencyID = a.Currency,
                                    PGSL = a.PGSL,
                                    IsRTGS = a.IsRTGS ?? false,
                                    IsSKN = a.IsSKN ?? false,
                                    IsOTT = a.IsOTT ?? false,
                                    IsOVB = a.IsOVB ?? false,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    BankGrid.Page = page;
                    BankGrid.Size = size;
                    BankGrid.Total = data.Count();
                    BankGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    BankGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new BankRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            Code = a.Code,
                            BranchCode = a.BranchCode,
                            SwiftCode = a.SwiftCode,
                            BankAccount = a.BankAccount,
                            Description = a.Description,
                            CommonName = a.CommonName,
                            Currency = a.Currency,
                            CurrencyID = a.CurrencyID,
                            PGSL = a.PGSL,
                            IsRTGS = a.IsRTGS ?? false,
                            IsSKN = a.IsSKN ?? false,
                            IsOTT = a.IsOTT ?? false,
                            IsOVB = a.IsOVB ?? false,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetBank(BankFilter filter, ref IList<BankModel> banks, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetBank(string key, int limit, ref IList<BankModel> banks, ref string message)
        {
            bool isSuccess = false;

            try
            {
                banks = (from a in context.Banks
                         where (a.BankCode.Contains(key) ||
                              a.BranchCode.Contains(key) || a.SwiftCode.Contains(key) || a.BankAccount.Contains(key)
                             || a.BankDescription.Contains(key) || a.CommonName.Contains(key)
                             || a.Currency.Contains(key) || a.PGSL.Contains(key)) && a.IsDeleted.Equals(false)
                         orderby new { a.BankCode, a.BranchCode, a.SwiftCode, a.BankAccount, a.BankDescription, a.CommonName, a.Currency, a.PGSL }
                         select new BankModel
                         {
                             ID = a.BankID,
                             Code = a.BankCode,
                             BranchCode = a.BranchCode,
                             SwiftCode = a.SwiftCode,
                             BankAccount = a.BankAccount,
                             Description = a.BankDescription,
                             CommonName = a.CommonName,
                             Currency = a.Currency,
                             PGSL = a.PGSL,

                             IsRTGS = a.IsRTGS ?? false,
                             IsSKN = a.IsSKN ?? false,
                             IsOTT = a.IsOTT ?? false,
                             IsOVB = a.IsOVB ?? false,
                             IsDelete = a.IsDeleted

                         }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddBank(BankModel bank, ref string message)
        {
            bool isSuccess = false;

            if (bank != null)
            {
                try
                {
                    //if (!context.Banks.Where(a => a.BankCode.Equals(bank.Code) && a.IsDeleted.Equals(false)).Any())
                    //{

                        Entity.Bank data = new Entity.Bank()
                        {
                            BankCode = bank.Code,
                            BranchCode = bank.BranchCode,
                            SwiftCode = bank.SwiftCode,
                            BankAccount = bank.BankAccount,
                            BankDescription = bank.Description,
                            CommonName = bank.CommonName,
                            Currency = bank.Currency,
                            PGSL = bank.PGSL,
                            IsRTGS = bank.IsRTGS ?? false,
                            IsSKN = bank.IsSKN ?? false,
                            IsOTT = bank.IsOTT ?? false,
                            IsOVB = bank.IsOVB ?? false,
                            IsDeleted = false,
                            CreateDate = DateTime.UtcNow,
                            CreateBy = currentUser.GetCurrentUser().DisplayName
                        };
                        context.Banks.Add(data);
                        context.SaveChanges();

                        isSuccess = true;
                    //}
                    //else
                    //{
                    //    message = string.Format("Bank data for Bank Code {0} is already exist.", bank.Code);
                    //}
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
            }
            return isSuccess;
        }

        public bool UpdateBank(int id, BankModel bank, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.Bank data = context.Banks.Where(a => a.BankID.Equals(id)).SingleOrDefault();

                if (data != null)
                {                   

                        data.BankCode = bank.Code;
                        data.BranchCode = bank.BranchCode;
                        data.BankDescription = bank.Description;
                        data.SwiftCode = bank.SwiftCode;
                        data.BankAccount = bank.BankAccount;
                        data.BankDescription = bank.Description;
                        data.CommonName = bank.CommonName;
                        data.Currency = bank.Currency;
                        data.PGSL = bank.PGSL;
                        data.IsRTGS = bank.IsRTGS ?? false;
                        data.IsSKN = bank.IsSKN ?? false;
                        data.IsOTT = bank.IsOTT ?? false;
                        data.IsOVB = bank.IsOVB ?? false;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        context.SaveChanges();

                        isSuccess = true;
                    
                }
                else
                {
                    message = string.Format("Bank data for Bank ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeleteBank(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.Bank data = context.Banks.Where(a => a.BankID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Bank data for Bank ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public bool GetBank(string key, int limit, ref IList<BankBranchModel> bankbranch, ref string message)
        {
            bool isSuccess = false;

            try
            {
                bankbranch = (from a in context.Banks
                              //join b in context.Branches on a.BankID equals b.CityID
                              where (a.BankCode.Contains(key) ||
                                   a.BranchCode.Contains(key) || a.SwiftCode.Contains(key) || a.BankAccount.Contains(key)
                                  || a.BankDescription.Contains(key) || a.CommonName.Contains(key)
                                  || a.Currency.Contains(key) || a.PGSL.Contains(key)) && a.IsDeleted.Equals(false)
                              orderby new { a.BankCode, a.BranchCode, a.SwiftCode, a.BankAccount, a.BankDescription, a.CommonName, a.Currency, a.PGSL }
                              select new BankBranchModel
                              {
                                  ID = a.BankID,
                                  Code = a.BankCode,
                                  BranchCode = a.BranchCode,
                                  SwiftCode = a.SwiftCode,
                                  BankAccount = a.BankAccount,
                                  Description = a.BankDescription,
                                  CommonName = a.CommonName,
                                  Currency = a.Currency,
                                  PGSL = a.PGSL,
                                  IsDelete = a.IsDeleted
                                  ,
                                  //Mark henggar query to heavy.....!!
                                  //BranchBank = new BranchModel { }
                                  //BranchBank = (from ab in context.Branches
                                  //                  where ab.CityID == ab.Bank.BankID 
                                  //                  select new BranchModel 
                                  //                  {
                                  //                      ID = ab.BranchID,
                                  //                      Code = ab.BranchCode,
                                  //                      Name = ab.CommonName
                                  //                  }).Take(limit).ToList().FirstOrDefault(),
                                  //IBranchBank = (from ab in context.Branches
                                  //               join b in context.Banks on ab.BankID equals b.BankID
                                  //               join c in context.Cities on ab.CityID equals c.CityID
                                  //               where b.BankID == a.BankID
                                  //               select new BranchModel
                                  //               {
                                  //                   ID = ab.BranchID,
                                  //                   CityID = ab.CityID,
                                  //                   Code = ab.BranchCode,
                                  //                   Name = ab.CommonName,
                                  //                   CityDescription = c.Description,
                                  //                   CityCode = c.CityCode
                                  //               }).Take(limit).ToList(),
                              }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        #region IPE
        public bool GetBank(string key, int limit, string name, ref IList<BankBranchModel> bankbranch, ref string message)
        {
            bool isSuccess = false;
            try
            {

                //bankbranch = (from a in context.Banks
                //              where (a.BankCode.Contains(key) ||
                //                   a.BranchCode.Contains(key) || a.SwiftCode.Contains(key) || a.BankAccount.Contains(key)
                //                  || a.BankDescription.Contains(key) || a.CommonName.Contains(key)
                //                  || a.Currency.Contains(key) || a.PGSL.Contains(key)) && a.IsDeleted.Equals(false)
                //              orderby new { a.BankCode, a.BranchCode, a.SwiftCode, a.BankAccount, a.BankDescription, a.CommonName, a.Currency, a.PGSL }
                //              select new BankBranchModel
                //              {
                //                  ID = a.BankID,
                //                  Code = a.BankCode,
                //                  BranchCode = a.BranchCode,
                //                  SwiftCode = a.SwiftCode,
                //                  BankAccount = a.BankAccount,
                //                  Description = a.BankDescription,
                //                  CommonName = a.CommonName,
                //                  Currency = a.Currency,
                //                  PGSL = a.PGSL,
                //                  IsDelete = a.IsDeleted,
                //                  IsOTT = a.IsOTT.Value,
                //                  IsOVB = a.IsOVB.Value,
                //                  IsRTGS = a.IsRTGS.Value,
                //                  IsSKN = a.IsSKN.Value
                //                  ,
                //                  //Mark henggar query to heavy.....!!
                //                  //BranchBank = new BranchModel { }
                //                  //BranchBank = (from ab in context.Branches
                //                  //                  where ab.CityID == ab.Bank.BankID 
                //                  //                  select new BranchModel 
                //                  //                  {
                //                  //                      ID = ab.BranchID,
                //                  //                      Code = ab.BranchCode,
                //                  //                      Name = ab.CommonName
                //                  //                  }).Take(limit).ToList().FirstOrDefault(),
                //                  //IBranchBank = (from ab in context.Branches
                //                  //               join b in context.Banks on ab.BankID equals b.BankID
                //                  //               join c in context.Cities on ab.CityID equals c.CityID
                //                  //               where b.BankID == a.BankID
                //                  //               select new BranchModel
                //                  //               {
                //                  //                   ID = ab.BranchID,
                //                  //                   CityID = ab.CityID,
                //                  //                   Code = ab.BranchCode,
                //                  //                   Name = ab.CommonName,
                //                  //                   CityDescription = c.Description,
                //                  //                   CityCode = c.CityCode
                //                  //               }).Take(limit).ToList(),
                //                  //IBranchBank = null
                //              }).ToList(); //.Take(limit)

                switch (name)
                {
                    case "OVB":
                        //bankbranch = bankbranch.Where(a => a.IsOVB.HasValue ? a.IsOVB.Value : false ).ToList();
                        bankbranch = (from a in context.Banks
                              where (a.BankCode.Contains(key) ||
                                   a.BranchCode.Contains(key) || a.SwiftCode.Contains(key) || a.BankAccount.Contains(key)
                                  || a.BankDescription.Contains(key) || a.CommonName.Contains(key)
                                  || a.Currency.Contains(key) || a.PGSL.Contains(key)) && a.IsDeleted.Equals(false) && a.IsOVB.Value.Equals(true)
                              orderby new { a.BankCode, a.BranchCode, a.SwiftCode, a.BankAccount, a.BankDescription, a.CommonName, a.Currency, a.PGSL }
                              select new BankBranchModel
                              {
                                  ID = a.BankID,
                                  Code = a.BankCode,
                                  BranchCode = a.BranchCode,
                                  SwiftCode = a.SwiftCode,
                                  BankAccount = a.BankAccount,
                                  Description = a.BankDescription,
                                  CommonName = a.CommonName,
                                  Currency = a.Currency,
                                  PGSL = a.PGSL,
                                  IsDelete = a.IsDeleted,
                                  IsOTT = a.IsOTT.Value,
                                  IsOVB = a.IsOVB.Value,
                                  IsRTGS = a.IsRTGS.Value,
                                  IsSKN = a.IsSKN.Value
                              }).Take(20).ToList(); 
                        break;
                    case "RTGS":
                        //bankbranch = bankbranch.Where(a => a.IsRTGS.HasValue ? a.IsRTGS.Value : false).ToList();
                        bankbranch = (from a in context.Banks
                                      where (a.BankCode.Contains(key) ||
                                           a.BranchCode.Contains(key) || a.SwiftCode.Contains(key) || a.BankAccount.Contains(key)
                                          || a.BankDescription.Contains(key) || a.CommonName.Contains(key)
                                          || a.Currency.Contains(key) || a.PGSL.Contains(key)) && a.IsDeleted.Equals(false) && a.IsRTGS.Value.Equals(true)
                                      orderby new { a.BankCode, a.BranchCode, a.SwiftCode, a.BankAccount, a.BankDescription, a.CommonName, a.Currency, a.PGSL }
                                      select new BankBranchModel
                                      {
                                          ID = a.BankID,
                                          Code = a.BankCode,
                                          BranchCode = a.BranchCode,
                                          SwiftCode = a.SwiftCode,
                                          BankAccount = a.BankAccount,
                                          Description = a.BankDescription,
                                          CommonName = a.CommonName,
                                          Currency = a.Currency,
                                          PGSL = a.PGSL,
                                          IsDelete = a.IsDeleted,
                                          IsOTT = a.IsOTT.Value,
                                          IsOVB = a.IsOVB.Value,
                                          IsRTGS = a.IsRTGS.Value,
                                          IsSKN = a.IsSKN.Value
                                      }).Take(20).ToList(); 
                        break;
                    case "OTT":
                        //bankbranch = bankbranch.Where(a => a.IsOTT.HasValue ? a.IsOTT.Value : false).ToList();
                        bankbranch = (from a in context.Banks
                                      where (a.BankCode.Contains(key) ||
                                           a.BranchCode.Contains(key) || a.SwiftCode.Contains(key) || a.BankAccount.Contains(key)
                                          || a.BankDescription.Contains(key) || a.CommonName.Contains(key)
                                          || a.Currency.Contains(key) || a.PGSL.Contains(key)) && a.IsDeleted.Equals(false) && a.IsOTT.Value.Equals(true)
                                      orderby new { a.BankCode, a.BranchCode, a.SwiftCode, a.BankAccount, a.BankDescription, a.CommonName, a.Currency, a.PGSL }
                                      select new BankBranchModel
                                      {
                                          ID = a.BankID,
                                          Code = a.BankCode,
                                          BranchCode = a.BranchCode,
                                          SwiftCode = a.SwiftCode,
                                          BankAccount = a.BankAccount,
                                          Description = a.BankDescription,
                                          CommonName = a.CommonName,
                                          Currency = a.Currency,
                                          PGSL = a.PGSL,
                                          IsDelete = a.IsDeleted,
                                          IsOTT = a.IsOTT.Value,
                                          IsOVB = a.IsOVB.Value,
                                          IsRTGS = a.IsRTGS.Value,
                                          IsSKN = a.IsSKN.Value
                                      }).Take(20).ToList(); 
                        break;
                    case "SKN":
                        //bankbranch = bankbranch.Where(a => a.IsSKN.HasValue ? a.IsRTGS.Value : false).ToList();
                        bankbranch = (from a in context.Banks
                                      where (a.BankCode.Contains(key) ||
                                           a.BranchCode.Contains(key) || a.SwiftCode.Contains(key) || a.BankAccount.Contains(key)
                                          || a.BankDescription.Contains(key) || a.CommonName.Contains(key)
                                          || a.Currency.Contains(key) || a.PGSL.Contains(key)) && a.IsDeleted.Equals(false) && a.IsSKN.Value.Equals(true)
                                      orderby new { a.BankCode, a.BranchCode, a.SwiftCode, a.BankAccount, a.BankDescription, a.CommonName, a.Currency, a.PGSL }
                                      select new BankBranchModel
                                      {
                                          ID = a.BankID,
                                          Code = a.BankCode,
                                          BranchCode = a.BranchCode,
                                          SwiftCode = a.SwiftCode,
                                          BankAccount = a.BankAccount,
                                          Description = a.BankDescription,
                                          CommonName = a.CommonName,
                                          Currency = a.Currency,
                                          PGSL = a.PGSL,
                                          IsDelete = a.IsDeleted,
                                          IsOTT = a.IsOTT.Value,
                                          IsOVB = a.IsOVB.Value,
                                          IsRTGS = a.IsRTGS.Value,
                                          IsSKN = a.IsSKN.Value
                                      }).Take(20).ToList(); 
                        break;
                    //case (int)DBSProductID.SKNProductIDCons:
                    //    bankbranch = bankbranch.Where(a => a.IsSKN.Equals(true)).ToList();
                    //    break;
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetBank(int key, ref IList<BankBranchModel> bankbranch, ref string message)
        {
            bool isSuccess = false;
            try
            {
                bankbranch = (from a in context.Banks
                              //join b in context.Branches on a.BankID equals b.CityID
                              where a.BankID.Equals(key) && a.IsDeleted.Equals(false)
                              orderby new { a.BankCode, a.BranchCode, a.SwiftCode, a.BankAccount, a.BankDescription, a.CommonName, a.Currency, a.PGSL }
                              select new BankBranchModel
                              {
                                  ID = a.BankID,
                                  Code = a.BankCode,
                                  BranchCode = a.BranchCode,
                                  SwiftCode = a.SwiftCode,
                                  BankAccount = a.BankAccount,
                                  Description = a.BankDescription,
                                  CommonName = a.CommonName,
                                  Currency = a.Currency,
                                  PGSL = a.PGSL,
                                  IsDelete = a.IsDeleted,
                                  IBranchBank = (from ab in context.Branches
                                                 join b in context.Banks on ab.BankID equals b.BankID
                                                 join c in context.Cities on ab.CityID equals c.CityID
                                                 where b.BankID == a.BankID
                                                 select new BranchModel
                                                 {
                                                     ID = ab.BranchID,
                                                     CityID = ab.CityID,
                                                     Code = ab.BranchCode,
                                                     Name = ab.CommonName,
                                                     CityDescription = c.Description,
                                                     CityCode = c.CityCode
                                                 }).Take(20).ToList(),
                              }).Take(20).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        #endregion
    }
    #endregion
}