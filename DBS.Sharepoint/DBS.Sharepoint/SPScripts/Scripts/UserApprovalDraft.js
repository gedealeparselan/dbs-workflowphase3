var accessToken;
var $box;
var $remove = false;

var GridPropertiesModel = function (callback) {
    var self = this;

    self.SortColumn = ko.observable();
    self.SortOrder = ko.observable();
    self.AllowFilter = ko.observable(false);
    self.AvailableSizes = ko.observableArray(config.sizes);
    self.Page = ko.observable(1);
    self.Size = ko.observable(config.sizeDefault);
    self.Total = ko.observable(0);
    self.TotalPages = ko.observable(0);

    self.Callback = function () {
        callback();
    };

    self.OnPageSizeChange = function () {
        self.Page(1);

        self.Callback();
    };

    self.OnPageChange = function () {
        if (self.Page() < 1) {
            self.Page(1);
        } else {
            if (self.Page() > self.TotalPages())
                self.Page(self.TotalPages());
        }

        self.Callback();
    };

    self.NextPage = function () {
        var page = self.Page();
        if (page < self.TotalPages()) {
            self.Page(page + 1);

            self.Callback();
        }
    };

    self.PreviousPage = function () {
        var page = self.Page();
        if (page > 1) {
            self.Page(page - 1);

            self.Callback();
        }
    };

    self.FirstPage = function () {
        self.Page(1);

        self.Callback();
    };

    self.LastPage = function () {
        self.Page(self.TotalPages());

        self.Callback();
    };

    self.Filter = function (data) {
        self.Page(1);

        self.Callback();
    };

    // get sorted column
    self.GetSortedColumn = function (columnName) {
        var sort = "sorting";

        if (self.SortColumn() == columnName) {
            if (self.SortOrder() == "DESC")
                sort = sort + "_asc";
            else
                sort = sort + "_desc";
        }

        return sort;
    };

    // Sorting data
    self.Sorting = function (column) {
        if (self.SortColumn() == column) {
            if (self.SortOrder() == "ASC")
                self.SortOrder("DESC");
            else
                self.SortOrder("ASC");
        } else {
            self.SortOrder("ASC");
        }

        self.SortColumn(column);

        self.Page(1);

        self.Callback();
    };
};

var RankModel = function(id,code,desc)
{
    var self = this;
    self.ID = ko.observable(id);
    self.Code = ko.observable(code);
    self.Description = ko.observable(desc);
}

var SegmentModel = function(id,name,desc)
{
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
    self.Description = ko.observable(desc);
}

var BranchModel = function(id,name)
{
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
}

var RoleModel = function(id,name,desc)
{
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
    self.Description = ko.observable(desc);
}

var ViewModel = function (url) {
    //Make the self as 'this' reference
    var self = this;

	//Declare observable which will be bind with UI
    //Declare observable which will be bind with UI 
    self.ID = ko.observable("");
    self.Name = ko.observable("");
    self.Email = ko.observable("");
    self.Rank = ko.observable(new RankModel('','',''));
    self.Segment = ko.observable(new SegmentModel('','',''));
    self.Branch = ko.observable(new BranchModel('',''));
	self.Role = ko.observable(new RoleModel('','',''));
    self.LastModifiedBy = ko.observable("");
    self.LastModifiedDate = ko.observable("");

    //Dropdown
    self.ddlBranch = ko.observableArray([]);
    self.ddlRank = ko.observableArray([]);
    self.ddlSegment = ko.observableArray([]);
	self.ddlRole = ko.observableArray([]);

    // filter
    self.FilterID= ko.observable("");
    self.FilterName = ko.observable("");
    self.FilterEmail = ko.observable("");
    self.FilterRank = ko.observable("");
    self.FilterSegment = ko.observable("");
    self.FilterBranch = ko.observable("");
	self.FilterRole = ko.observable("");
    self.FilterModifiedBy = ko.observable("");
    self.FilterModifiedDate = ko.observable("");


    // New Data flag
    self.IsNewData = ko.observable(false);

    // Declare an ObservableArray for Storing the JSON Response
    self.UserApprovals = ko.observableArray([]);

    // grid properties
    self.GridProperties = ko.observable();
    self.GridProperties(new GridPropertiesModel(GetData));

    // set default sorting
    self.GridProperties().SortColumn("Name");
    self.GridProperties().SortOrder("ASC");

    // bind clear filters
    self.ClearFilters = function () {
        self.FilterID("");
        self.FilterName("");
        self.FilterEmail("");
        self.FilterRank("");
        self.FilterSegment("");
        self.FilterBranch("");
		self.FilterRole("");
        self.FilterModifiedBy("");
        self.FilterModifiedDate("");

        GetData();
    };


    // bind get data function to view
    self.GetData = function () {
        GetData();
    };
    self.GetDropdown = function () {
        GetDropdown();
    };
    //The Object which stored data entered in the observables
    var UserApproval = {
        ID: self.ID,
        Name: self.Name,
        Rank: self.Rank,
        Segment: self.Segment,
        Branch: self.Branch,
		Role: self.Role,
        Email: self.Email,
        LastModifiedBy: self.LastModifiedBy,
        LastModifiedDate: self.LastModifiedDate
    };
	
	self.save = function (event, ui) {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if(form.valid()){
		
			$("#modal-form-master").modal('hide');

			bootbox.confirm("Are you sure?", function(result) {
				if(!result) {
					$("#modal-form-master").modal('show');
				}else{
		
					//Ajax call to insert the UserApprovals
					$.ajax({
						type: "POST",
						url: api.server + api.url.userapprovaldraft,
						data: ko.toJSON(UserApproval), //Convert the Observable Data into JSON
						contentType: "application/json",
						headers: {
							"Authorization" : "Bearer " + accessToken
						},
						success: function (data, textStatus, jqXHR) {
							if(jqXHR.status = 200){
								//insert to sharepoint list
								//AddListItem(ui.currentTarget.children[0].id, UserApproval.ID());
								
								// send notification
								ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

								// hide current popup window
								$("#modal-form-master").modal('hide');

								// refresh data
								GetData();
							}else{
								ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
							}
						},
						error: function (jqXHR, textStatus, errorThrown) {
							// send notification
							ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
						}
					});
				}
			});
		}
    };

    self.update = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if(form.valid()){
            // hide current popup window
            $("#modal-form-master").modal('hide');

            bootbox.confirm("Are you sure?", function(result) {
                if(!result) {
                    $("#modal-form-master").modal('show');
                }else{
                    //Ajax call to update the Customer
                    $.ajax({
                        type: "PUT",
                        url: api.server + url + "/" + UserApproval.ID(),
                        data: ko.toJSON(UserApproval),
                        contentType: "application/json",
                        headers: {
                            "Authorization" : "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if(jqXHR.status = 200){
                                // send notification
                                ShowNotification('Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // hide current popup window
                                $("#modal-form-master").modal('hide');

                                // refresh data
                                GetData();
                            }else{
                                ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });
                }
            });
        }
    };

    self.delete = function () {
        // hide current popup window
        $("#modal-form-master").modal('hide');

        bootbox.confirm("Are you sure want to delete?", function(result) {
            if(!result) {
                $("#modal-form-master").modal('show');
            }else{
                //Ajax call to delete the Customer
                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.userapprovaldraft + "/" + UserApproval.ID(),
                    data: ko.toJSON(UserApproval),
                    headers: {
                        "Authorization" : "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if(jqXHR.status = 200){
                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                            // refresh data
                            GetData();
                        }else
                            ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
            }
        });
    };
	
	//add to list sharepoint
	function AddListItem(ActionType, EmployeeID) {
		var body = {
			Title: 'User Approval (' + ActionType + ') ',// + new Date,
			ActionType: ActionType,
			EmployeeID: EmployeeID,
			__metadata: {
				type: config.sharepoint.metadata.list
			}
		};

		var options = {
			url: config.sharepoint.url.api + "/Lists(guid'" + config.sharepoint.listIdMaster + "')/Items",
			data: JSON.stringify(body),
			digest: jQuery("#__REQUESTDIGEST").val()
		};

		Helper.Sharepoint.List.Add(options, OnSuccessAddListItem, OnError, OnAlways);
	}
	
	function OnSuccessAddListItem(data, textStatus, jqXHR) {
		// clear transaction model
		//viewModel.TransactionModel(null);

		// send notification
		//ShowNotification('Submit Success', 'New transaction has been submitted at '+ viewModel.LocalDate(data.d.Created), 'gritter-success', false);

		// redirect to all transaction
		//window.location = "/home/all-transaction";
		//bangkit
		//window.location = "/home";
	}

    // Local Date
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return "";
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-18
    };

    //Function to Display record to be updated
    self.GetSelectedRow = function (data, flag) {
        self.IsNewData(false);
		
		if (flag == 'master') {
			$("#modal-form-master").modal('show');
		} else {
			$("#modal-form-approval").modal('show');
		}

        self.ID(data.ID);
        self.Name(data.Name);
        self.Email(data.Email);
        self.Rank(new RankModel(data.Rank.ID,data.Rank.Code,data.Rank.Description));
        self.Segment(new SegmentModel(data.Segment.ID,data.Segment.Name,data.Segment.Description));
        self.Branch(new BranchModel(data.Branch.ID,data.Branch.Name));
		self.Role(new RoleModel(data.Role.ID,data.Role.Name,data.Role.Description));
        self.LastModifiedBy(data.LastModifiedBy);
        self.LastModifiedDate(data.LastModifiedDate);
    };

    //insert new
    self.NewData = function () {
        // flag as new Data
        self.IsNewData(true);
        self.ID('');
        self.Name('');
        self.Email('');
        self.Rank(new RankModel('','','',''));
        self.Segment(new SegmentModel('','',''));
        self.Branch(new BranchModel('',''));
		self.Role(new RoleModel('','',''));
		var inp = $('#id').get(0);
		inp.removeAttribute('disabled');
    };

    // Function to get Biz Segment for Dropdownlist
    function GetDropdown() {
        $.ajax({
            type: "GET",
            url: api.server + api.url.parameter+'?select=BizSegment,Branch,Rank,Role',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization" : "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if(jqXHR.status = 200){
                    self.ddlSegment(data['BizSegment']);
                    self.ddlBranch(data['Branch']);
                    self.ddlRank(data['Rank']);
					self.ddlRole(data['Role']);
                }else{
                    ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    //Function to Read All Employee
    function GetData() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + url,
            params: {
                page: self.GridProperties().Page(),
                size: self.GridProperties().Size(),
                sort_column: self.GridProperties().SortColumn(),
                sort_order: self.GridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetFilteredColumns();

        if(filters.length > 0){
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetData, OnError, OnAlways);
        }else{
            // GET
            Helper.Ajax.Get(options, OnSuccessGetData, OnError, OnAlways);
        }
    }


    // Get filtered columns value
    function GetFilteredColumns(){
        // define filter
        var filters = [];
        if (self.FilterID() != "") filters.push({ Field: 'ID', Value: self.FilterID() });
        if (self.FilterName() != "") filters.push({ Field: 'Name', Value: self.FilterName() });
        if (self.FilterEmail() != "") filters.push({ Field: 'Email', Value: self.FilterEmail() });
        if (self.FilterRank() != "") filters.push({ Field: 'Rank', Value: self.FilterRank() });
        if (self.FilterSegment() != "") filters.push({ Field: 'Segment', Value: self.FilterSegment() });
        if (self.FilterBranch() != "") filters.push({ Field: 'Branch', Value: self.FilterBranch() });
		if (self.FilterRole() != "") filters.push({ Field: 'Role', Value: self.FilterRole() });
        if (self.FilterModifiedBy() != "") filters.push({ Field: 'LastModifiedBy', Value: self.FilterModifiedBy() });
        if (self.FilterModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.FilterModifiedDate() });

        return filters;
    };

    // On success GetData callback
    function OnSuccessGetData(data, textStatus, jqXHR){
        if(jqXHR.status = 200){
            self.UserApprovals(data.Rows);

            self.GridProperties().Page(data['Page']);
            self.GridProperties().Size(data['Size']);
            self.GridProperties().Total(data['Total']);
            self.GridProperties().TotalPages(Math.ceil(self.GridProperties().Total() / self.GridProperties().Size()));
        }else{
            ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    // On Error callback
    function OnError(jqXHR, textStatus, errorThrown){
        ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    // On Always callback
    function OnAlways(){
        $box.trigger('reloaded.ace.widget');
    }
};

var MultiViewModel = function() {
	var self = this;
	
	self.ViewModelMaster = ko.observable(new ViewModel(api.url.userapproval));
	self.ViewModelApproval = ko.observable(new ViewModel(api.url.userapprovaldraft));
}

$(document).ready(function () {
    // widget loader
    $box = $('.widget-box');
    var event;
    $box.trigger(event = $.Event('reload.ace.widget'))
    if (event.isDefaultPrevented()) return
    $box.blur();

    /// block enter key from user to prevent submitted data.
    $("input").keypress(function (event) {
        var code = event.charCode || event.keyCode;
        if (code == 13) {
            ShowNotification("Page Information", "Enter key is disabled for this form.", "gritter-warning", false);

            return false;
        }
    });

    // scrollables
    $('.slim-scroll').each(function () {
        var $this = $(this);
        $this.slimscroll({
            height: $this.data('height') || 100,
            //width: $this.data('width') || 100,
            railVisible: true,
            alwaysVisible: true,
            color: '#D15B47'
        });
    });

    var viewModel = new MultiViewModel();
    ko.applyBindings(viewModel);
	
	$("#id").autocomplete({
        source: function (request, response) {
            // declare options variable for ajax get request
            var options = {
                url: api.server + api.url.helper + "/GetUserAD",
                data: {
                    username: request.term,
                    limit: 20
                },
                token: accessToken
            };

            // exec ajax request
            Helper.Ajax.AutoComplete(options, response, OnSuccessAutoComplete, TokenOnError);
        },
        minLength: 2,
        select: function (event, ui) {
            // set customer data model
            if(ui.item.data != undefined || ui.item.data != null) {
                viewModel.ViewModelMaster().ID(ui.item.data.ID);
				viewModel.ViewModelMaster().Name(ui.item.data.Name);
				viewModel.ViewModelMaster().Email(ui.item.data.Email);
				$('#ID').val(ui.item.data.ID);
				$('#Name').val(ui.item.data.Name);
				$('#Email').val(ui.item.data.Email);
				
				var inp = $('#id').get(0);
				inp.setAttribute('disabled' , 'disabled');
            }else{
			
			}   
        }
    });
	
	/*var tag_input = $('#id');
	if(! ( /msie\s*(8|7|6)/.test(navigator.userAgent.toLowerCase())) ) 
	{
		tag_input.tag(
		  {
			placeholder:tag_input.attr('placeholder'),
			//enable typeahead by specifying the source array
			//source: ace.variable_US_STATES,//defined in ace.js >> ace.enable_search_ahead
		  }
		);
	}
	else {
		//display a textarea for old IE, because it doesn't support this plugin or another one I tried!
		tag_input.after('<textarea id="'+tag_input.attr('id')+'" name="'+tag_input.attr('name')+'" rows="3">'+tag_input.val()+'</textarea>').remove();
		//$('#form-field-tags').autosize({append: "\n"});
	}*/

    // Token Validation
    if($.cookie(api.cookie.name) == undefined){
        Helper.Token.Request(TokenOnSuccess, TokenOnError);
    }else{
        // read token from cookie
        accessToken = $.cookie(api.cookie.name);

        // call spuser function
        GetCurrentUser(viewModel.ViewModelMaster());

        // call get data inside view model
        viewModel.ViewModelMaster().GetData();
        viewModel.ViewModelMaster().GetDropdown();
		viewModel.ViewModelApproval().GetData();
        viewModel.ViewModelApproval().GetDropdown();
    }

    $('.date-picker').datepicker({autoclose: true}).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });

    //$("#aspnetForm").validate({onsubmit: false});
    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }

    });
});

// Get SPUser from cookies
function GetCurrentUser(vm) {
    if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
        spUser = $.cookie(api.cookie.spUser);

        vm.SPUser = spUser;
    }
}


// Token validation On Success function
function TokenOnSuccess(data, textStatus, jqXHR){
    // store token on browser cookies
    $.cookie(api.cookie.name, data.AccessToken);
    accessToken = $.cookie(api.cookie.name);

    // call spuser function
    GetCurrentUser();

    // call get data inside view model
    viewModel.ViewModelMaster().GetParameters();
    viewModel.ViewModelMaster().GetData();
	viewModel.ViewModelApproval().GetParameters();
    viewModel.ViewModelApproval().GetData();
}

// Token validation On Error function
function TokenOnError(jqXHR, textStatus, errorThrown){
    ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}

// Autocomplete
function OnSuccessAutoComplete(response, data, textStatus, jqXHR) {
    response($.map(data, function (item) {
            return {
                // default autocomplete object
                label: item.UserName + " - " + item.DisplayName,
                value: item.UserName,

                // custom object binding
                data: {
                    ID: item.UserName,
                    Name: item.DisplayName,
                    Email: item.Email
                }
            }
        })
    );
}