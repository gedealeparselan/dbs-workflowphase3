﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using System.Net.Http;
using System.Web.Script.Serialization;
using System.Transactions;
using System.Data.Entity;

//=======================================================================================//
///FOR ALL DEVELOPER...
///PLEASE ADD NEW REGION BY YOUR NAME TO KEEP CLEAN !!! DONT BE A DIRTY PROGRAMMER!!!
///ANDIWIJAYA
//=======================================================================================//

namespace DBS.WebAPI.Models
{
    #region Property
    // Basri 01.10.2015 : Add TMO Detail Model   
    public class TransactionTMOMakerDetailModel
    {
        public TransactionTMODetailModel Transaction { get; set; }
        public IList<TransactionTimelineModel> Timelines { get; set; }
    }
    public class TransactionTMOCheckerDetailModel
    {
        public TransactionTMODetailModel Transaction { get; set; }
        public IList<TransactionTimelineModel> Timelines { get; set; }
        public TransactionTMOCheckerDataModel Checker { get; set; }
        public IList<VerifyModel> Verify { get; set; }
        public TransactionTMOCallbackDetailModel callbackDetail { get; set; }
    }
    public class TransactionTMOTrackingCheckerDetailModel
    {
        public TransactionTMOModel Transaction { get; set; }
        public IList<TransactionTimelineModel> Timelines { get; set; }
        public TransactionTMOCheckerDataModel Checker { get; set; }
        public IList<VerifyModel> Verify { get; set; }
    }
    public class TransactionTMOCheckerAfterCallbackDetailModel
    {
        public TransactionTMODetailModel Transaction { get; set; }
        public TransactionTMOCallbackModel Callback { get; set; }
        public IList<TransactionTimelineModel> Timelines { get; set; }
        public TransactionTMOCheckerDataModel Checker { get; set; }
        public IList<VerifyModel> Verify { get; set; }
    }
    public class TransactionTMOCallbackDetailModel
    {
        public TransactionTMODetailModel Transaction { get; set; }
        public IList<TransactionTimelineModel> Timelines { get; set; }
        public IList<TransactionTMOCallbackTimeModel> Callbacks { get; set; }
        public IList<VerifyModel> Verify { get; set; }
    }

    public class TMOCheckerDetailModel
    {
        public TMODetailModel Transaction { get; set; }
        public TransactionTMOCheckerDataModel Checker { get; set; }
        public IList<TransactionTimelineModel> Timelines { get; set; }
        public IList<VerifyModel> Verify { get; set; }
    }
    // add chandra : 20151123
    public class TMOTrackingCheckerDetailModel
    {
        public TransactionTMOModel Transaction { get; set; }
        public TransactionTMOCheckerDataModel Checker { get; set; }
        public IList<TransactionTimelineModel> Timelines { get; set; }
        public IList<VerifyModel> Verify { get; set; }
    }

    //add by fandi
    public class TMOCSODetailModel
    {
        public TransactionDetailModel Transaction { get; set; }
        public TransactionTMOCheckerDataModel Checker { get; set; }
        public IList<TransactionTimelineModel> Timelines { get; set; }
        public IList<VerifyModel> Verify { get; set; }

    }
    public class TransactionTMOCallbackTimeModel
    {
        public long ID { get; set; }
        public long ApproverID { get; set; }
        public CustomerContactModel Contact { get; set; }
        public DateTime Time { get; set; }
        public bool? IsUTC { get; set; }
        public string Remark { get; set; }

    }
    public class CSOTMOModel
    {
        public TransactionTMOMakerDetailModel CSOTMO { get; set; }
    }
    public class TransactionTMOCheckerDataModel
    {
        public long ID { get; set; }


        public string Others { get; set; }

        /// <summary>
        /// Signature Verification
        /// </summary>
        public bool IsSignatureVerified { get; set; }

        /// <summary>
        /// Dormant Account
        /// </summary>
        public bool IsDormantAccount { get; set; }

        /// <summary>
        /// Freeze Account
        /// </summary>
        public bool IsFreezeAccount { get; set; }

        public bool IsCallbackRequired { get; set; }



        public bool IsLOIAvailable { get; set; }



        public bool IsStatementLetterCopy { get; set; }
        /// <summary>
        /// LLD Details
        /// </summary>
        public LLDModel LLD { get; set; }
    }
    public class TMODetailModel
    {
        /// <summary>
        /// Workflow Instance ID
        /// </summary>
        public Guid WorkflowInstanceID { get; set; }

        /// <summary>
        /// Transaction ID.
        /// </summary>
        public long ID { get; set; }

        /// <summary>
        /// Application ID
        /// </summary>
        public string ApplicationID { get; set; }


        /// <summary>
        /// Customer Data Details
        /// </summary>
        public CustomerModel Customer { get; set; }


        /// <summary>
        /// Product Details
        /// </summary>
        public ProductModel Product { get; set; }

        /// <summary>
        /// Channel Details
        /// </summary>
        public ChannelModel Channel { get; set; }

        public CurrencyModel Currency { get; set; }

        public decimal Amount { get; set; }
        public string DealNumber { get; set; }
        public DateTime? ValueDate { get; set; }

        /// <summary>
        /// Remakrs
        /// </summary>
        public string Remarks { get; set; }


        /// <summary>
        /// Create Date
        /// </summary>
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }

        /// <summary>
        /// List Transaction Document Details
        /// </summary>
        public IList<TransactionDocumentModel> Documents { get; set; }

        public long? ApproverID { get; set; }


    }

    public class TransactionTMODetailModel
    {
        /// <summary>
        /// Workflow Instance ID
        /// </summary>
        public Guid? WorkflowInstanceID { get; set; }

        /// <summary>
        /// Transaction ID.
        /// </summary>
        public long ID { get; set; }

        /// <summary>
        /// Application ID
        /// </summary>
        public string ApplicationID { get; set; }

        /// <summary>
        /// Customer Data Details
        /// </summary>
        public CustomerModel Customer { get; set; }

        /// <summary>
        /// Product Details
        /// </summary>
        public ProductModel Product { get; set; }

        /// <summary>
        /// Channel Details
        /// </summary>
        public ChannelModel Channel { get; set; }

        /// <summary>
        /// Signature Verification
        /// </summary>
        public bool IsSignatureVerified { get; set; }

        /// <summary>
        /// Dormant Account
        /// </summary>
        public bool IsDormantAccount { get; set; }

        /// <summary>
        /// Freeze Account
        /// </summary>
        public bool IsFreezeAccount { get; set; }

        /// basri 30-09-2015
        /// <summary>
        /// Callback Required
        /// </summary>
        public bool IsCallbackRequired { get; set; }

        /// <summary>
        /// Letter of Indemnity Available
        /// </summary>
        public bool IsLOIAvailable { get; set; }

        /// <summary>
        /// Others
        /// </summary>
        public string Others { get; set; }
        /// <summary>
        /// SwapTransactionID
        /// </summary>
        public long? SwapTransactionID { get; set; }

        /// <summary>
        /// Create Date
        /// </summary>
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }

        /// <summary>
        /// Is Draft
        /// </summary>
        public bool IsDraft { get; set; }

        /// <summary>
        /// List Transaction Document Details
        /// </summary>
        public IList<TransactionDocumentModel> Documents { get; set; }

        /// <summary>
        /// Remarks
        /// </summary>
        public string Remarks { get; set; }

        /// <summary>
        /// Transaction Create By
        /// </summary>
        public string CreateBy { get; set; }

        /// <summary>
        /// Flag Document Completeness
        /// </summary>
        public bool IsDocumentComplete { get; set; }

        /// <summary>
        /// Approver ID
        /// </summary>
        public long? ApproverID { get; set; }

        /// <summary>
        /// List Transaction All Document reference to underlying by cif
        /// </summary>
        public IList<ReviseMakerDocumentModel> ReviseMakerDocuments { get; set; }

        /// <summary>
        /// Amount
        /// </summary>
        public decimal Amount { get; set; }
        public string DealNumber { get; set; }
        public DateTime? ValueDate { get; set; }
        public CurrencyModel Currency { get; set; }
    }
    public class TransactionTMOCallbackModel
    {
        public long? ApproverID { get; set; }
        public long ID { get; set; }

        /// <summary>
        /// Product Details
        /// </summary>
        public ProductModel Product { get; set; }

        /// <summary>
        /// Channel Details
        /// </summary>
        public ChannelModel Channel { get; set; }

        /// <summary>
        /// Others information
        /// </summary>
        public string Others { get; set; }

        /// <summary>
        /// Remarks
        /// </summary>
        public string Remarks { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }

        public bool IsSignatureVerified { get; set; }

        public bool IsDormantAccount { get; set; }

        public bool IsFreezeAccount { get; set; }

        public bool IsLOIAvailable { get; set; }

    }
    public class TMOMakerDetailModel
    {
        public TMODetailModel Transaction { get; set; }
        public IList<TransactionTimelineModel> Timelines { get; set; }
    }
    //end basri
    #endregion
    #region Interface
    public interface IWorkflowTMORepository : IDisposable
    {
        //Basri 01.10.2015
        bool GetTransactionMakerReviseAfterCheckerDetails(Guid instanceID, ref TransactionTMODetailModel transaction, ref string message);
        bool GetTransactionMakerReviseAfterCheckerTMODetails(Guid instanceID, ref TransactionTMODetailModel transaction, ref string message);
        bool GetTransactionCheckerDetails(Guid workflowInstanceID, long approverID, ref TransactionTMOCheckerDetailModel output, ref string message);
        bool GetTransactionDetails(Guid instanceID, ref TransactionTMODetailModel transaction, ref string message);
        bool GetTMODetails(Guid workflowInstanceId, ref TMODetailModel output, ref string message);
        bool AddTransactionTMOMaker(Guid workflowInstanceID, long approverID, TMODetailModel tmo, ref string message);
        bool AddTransactionMaker(Guid workflowInstanceID, long approverID, TransactionTMODetailModel transaction, ref string message);
        bool AddTransactionChecker(Guid workflowInstanceID, long approverID, TransactionTMOCheckerDetailModel data, ref string message);
        bool AddTransactionTMOChecker(Guid workflowInstanceID, long approverID, TMOCheckerDetailModel data, ref string message);
        bool AddTransactionCallback(Guid workflowInstanceID, long approverID, TransactionTMOCallbackDetailModel data, ref string message);
        bool GetTransactionTimeline(Guid workflowInstanceID, ref IList<TransactionTimelineModel> timelines, ref string message);
        bool GetTransactionCallbackDetails(Guid workflowInstanceID, long approverID, ref TransactionTMOCallbackDetailModel output, ref string message);
        bool GetTransactionCheckerAfterCallbackDetails(Guid workflowInstanceID, long approverID, ref TransactionTMOCheckerAfterCallbackDetailModel output, ref string message);
        bool GetTMOCheckerDetails(Guid workflowInstanceID, long approverID, ref TransactionTMOCheckerDetailModel output, ref string message);
        //end basri
        //chandra
        bool GetTrackingTMODetails(Guid workflowInstanceId, ref TransactionTMOModel output, ref string message);
        bool GetTMODealCheckerDetails(Guid workflowInstanceID, long approverID, ref TransactionTMOCheckerDetailModel output, ref string message);
        bool AddTMODealChecker(Guid workflowInstanceID, long approverID, TransactionTMOTrackingCheckerDetailModel data, ref string message);
        bool AddTMODealMaker(Guid workflowInstanceID, long approverID, TransactionTMOModel data, ref string message);
        bool AddTMODealSubmitMaker(Guid workflowInstanceID, long approverID, TransactionTMOModel data, ref string message);
        bool AddTMODealReopenMaker(Guid workflowInstanceID, long approverID, TransactionTMOModel data, ref string message);
        bool GetTrackingTMOMakerDetails(Guid workflowInstanceId, ref TransactionTMOModel output, ref string message);
        bool UpdateCheckerSubmit(Guid workflowInstanceID, long approverID, TransactionTMOTrackingCheckerDetailModel data, ref string message);
        bool UpdateCancelation(Guid workflowInstanceID, ref string message);
        //add by fandi
        bool UpdateSubmitReviseFromCSOTMO(Guid workflowInstanceID, long approverID, TMOCSODetailModel data, ref string message);
        //end

        #region Dani
        bool IsCSO(string userName, ref bool IsCSO, ref string message);
        #endregion
    }
    #endregion
    #region Repository
    public class WorkflowTMORepository : IWorkflowTMORepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        /// <summary>
        /// Add method below...
        /// </summary>
        public bool GetTransactionTimeline(Guid workflowInstanceID, ref IList<TransactionTimelineModel> timelines, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                //Remark Rizki 2015-04-21, ganti dari View ke SP             
                timelines = (from a in context.SP_GetNintexTaskTimeline(workflowInstanceID)
                             select new TransactionTimelineModel
                             {
                                 ApproverID = a.WorkflowApproverID,
                                 Activity = a.ActivityTitle,
                                 Time = a.ActivityTime,
                                 UserOrGroup = a.UserOrGroup,
                                 IsGroup = a.IsSPGroup.Value,
                                 Outcome = a.Outcome,
                                 DisplayName = a.DisplayName,
                                 Comment = a.Comment
                             }).ToList();

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }
        public bool GetTransactionMakerReviseAfterCheckerDetails(Guid instanceID, ref TransactionTMODetailModel transaction, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                context.Transactions.AsNoTracking();

                TransactionTMODetailModel datatransaction = (from a in context.Transactions
                                                             where a.WorkflowInstanceID.Value == instanceID
                                                             select new TransactionTMODetailModel
                                                             {
                                                                 ID = a.TransactionID,
                                                                 WorkflowInstanceID = a.WorkflowInstanceID.Value,
                                                                 ApplicationID = a.ApplicationID,
                                                                 ApproverID = a.ApproverID != null ? a.ApproverID : 0,
                                                                 //added by dani 11-3-2016
                                                                 Currency = new CurrencyModel()
                                                                 {
                                                                     ID = (a.Currency != null && a.Currency.CurrencyID > 0) ? a.Currency.CurrencyID : 0,
                                                                     Code = (a.Currency != null && !string.IsNullOrEmpty(a.Currency.CurrencyCode)) ? a.Currency.CurrencyCode : "-",
                                                                     Description = (a.Currency != null && !string.IsNullOrEmpty(a.Currency.CurrencyDescription)) ? a.Currency.CurrencyDescription : "-",
                                                                     CodeDescription = (a.Currency != null && !string.IsNullOrEmpty(a.Currency.CurrencyCode) && !string.IsNullOrEmpty(a.Currency.CurrencyDescription)) ? "(" + a.Currency.CurrencyCode + ")  " + a.Currency.CurrencyDescription : "-"
                                                                 },
                                                                 //end dani
                                                                 Product = new ProductModel()
                                                                 {
                                                                     ID = a.Product.ProductID,
                                                                     Code = a.Product.ProductCode,
                                                                     Name = a.Product.ProductName,
                                                                     WorkflowID = a.Product.ProductWorkflow.WorkflowID,
                                                                     Workflow = a.Product.ProductWorkflow.WorkflowName,
                                                                     LastModifiedBy = a.Product.UpdateDate == null ? a.Product.CreateBy : a.Product.UpdateBy,
                                                                     LastModifiedDate = a.Product.UpdateDate == null ? a.Product.CreateDate : a.Product.UpdateDate.Value
                                                                 },
                                                                 Channel = new ChannelModel()
                                                                 {
                                                                     ID = a.Channel.ChannelID,
                                                                     Name = a.Channel.ChannelName,
                                                                     LastModifiedBy = a.Channel.UpdateDate == null ? a.Channel.CreateBy : a.Channel.UpdateBy,
                                                                     LastModifiedDate = a.Channel.UpdateDate == null ? a.Channel.CreateDate : a.Channel.UpdateDate.Value
                                                                 },
                                                                 IsSignatureVerified = a.IsSignatureVerified,
                                                                 IsFreezeAccount = a.IsFrezeAccount,
                                                                 IsDormantAccount = a.IsDormantAccount,
                                                                 IsDraft = a.IsDraft,
                                                                 Others = a.Others,
                                                                 Documents = (from p in a.TransactionDocuments

                                                                              join c in context.CustomerUnderlyingFiles
                                                                              on p.DocumentPath equals c.DocumentPath into zi
                                                                              from c in zi.DefaultIfEmpty()

                                                                              where p.IsDeleted.Equals(false)
                                                                              select new TransactionDocumentModel()
                                                                              {
                                                                                  ID = c.UnderlyingFileID != null ? c.UnderlyingFileID : p.TransactionDocumentID,
                                                                                  Type = new DocumentTypeModel()
                                                                                  {
                                                                                      ID = p.DocumentType.DocTypeID,
                                                                                      Name = p.DocumentType.DocTypeName,
                                                                                      Description = p.DocumentType.DocTypeDescription,
                                                                                      LastModifiedBy = p.DocumentType.UpdateDate == null ? p.DocumentType.CreateBy : p.DocumentType.UpdateBy,
                                                                                      LastModifiedDate = p.DocumentType.UpdateDate == null ? p.DocumentType.CreateDate : p.DocumentType.UpdateDate.Value
                                                                                  },
                                                                                  Purpose = new DocumentPurposeModel()
                                                                                  {
                                                                                      ID = p.DocumentPurpose.PurposeID,
                                                                                      Name = p.DocumentPurpose.PurposeName,
                                                                                      Description = p.DocumentPurpose.PurposeDescription,
                                                                                      LastModifiedBy = p.DocumentPurpose.UpdateDate == null ? p.DocumentPurpose.CreateBy : p.DocumentPurpose.UpdateBy,
                                                                                      LastModifiedDate = p.DocumentPurpose.UpdateDate == null ? p.DocumentPurpose.CreateDate : p.DocumentPurpose.UpdateDate.Value
                                                                                  },
                                                                                  FileName = p.Filename,
                                                                                  DocumentPath = p.DocumentPath,
                                                                                  IsDormant = p.IsDormant.Value,
                                                                                  LastModifiedBy = p.UpdateDate == null ? p.CreateBy : p.UpdateBy,
                                                                                  LastModifiedDate = p.UpdateDate == null ? p.CreateDate : p.UpdateDate.Value

                                                                              }).ToList(),
                                                                 ReviseMakerDocuments = (from cu in a.Customer.CustomerUnderlyings
                                                                                         join cm in context.CustomerUnderlyingMappings on cu.UnderlyingID equals cm.UnderlyingID
                                                                                         join cf in context.CustomerUnderlyingFiles on cm.UnderlyingFileID equals cf.UnderlyingFileID
                                                                                         where cu.IsDeleted.Equals(false) && cm.IsDeleted.Equals(false) && cf.IsDeleted.Equals(false)
                                                                                         select new ReviseMakerDocumentModel()
                                                                                         {
                                                                                             UnderlyingID = cu.UnderlyingID,
                                                                                             ID = cf.UnderlyingFileID,
                                                                                             Type = new DocumentTypeModel()
                                                                                             {
                                                                                                 ID = cf.DocumentType.DocTypeID,
                                                                                                 Name = cf.DocumentType.DocTypeName,
                                                                                                 Description = cf.DocumentType.DocTypeDescription,
                                                                                                 LastModifiedBy = cf.DocumentType.UpdateDate == null ? cf.DocumentType.CreateBy : cf.DocumentType.UpdateBy,
                                                                                                 LastModifiedDate = cf.DocumentType.UpdateDate == null ? cf.DocumentType.CreateDate : cf.DocumentType.UpdateDate.Value
                                                                                             },
                                                                                             Purpose = new DocumentPurposeModel()
                                                                                             {
                                                                                                 ID = cf.DocumentPurpose.PurposeID,
                                                                                                 Name = cf.DocumentPurpose.PurposeName,
                                                                                                 Description = cf.DocumentPurpose.PurposeDescription,
                                                                                                 LastModifiedBy = cf.DocumentPurpose.UpdateDate == null ? cf.DocumentPurpose.CreateBy : cf.DocumentPurpose.UpdateBy,
                                                                                                 LastModifiedDate = cf.DocumentPurpose.UpdateDate == null ? cf.DocumentPurpose.CreateDate : cf.DocumentPurpose.UpdateDate.Value
                                                                                             },
                                                                                             FileName = cf.FileName,
                                                                                             DocumentPath = cf.DocumentPath,
                                                                                             IsDormant = false,//cf.IsDormant.Value,
                                                                                             LastModifiedBy = cf.UpdateDate == null ? cf.CreateBy : cf.UpdateBy,
                                                                                             LastModifiedDate = cf.UpdateDate == null ? cf.CreateDate : cf.UpdateDate.Value
                                                                                         }).Concat(from p in a.TransactionDocuments
                                                                                                   where p.PurposeID != 2 && p.IsDeleted.Equals(false)
                                                                                                   select new ReviseMakerDocumentModel()
                                                                                                   {
                                                                                                       UnderlyingID = 0,
                                                                                                       ID = p.TransactionDocumentID,
                                                                                                       Type = new DocumentTypeModel()
                                                                                                       {
                                                                                                           ID = p.DocumentType.DocTypeID,
                                                                                                           Name = p.DocumentType.DocTypeName,
                                                                                                           Description = p.DocumentType.DocTypeDescription,
                                                                                                           LastModifiedBy = p.DocumentType.UpdateDate == null ? p.DocumentType.CreateBy : p.DocumentType.UpdateBy,
                                                                                                           LastModifiedDate = p.DocumentType.UpdateDate == null ? p.DocumentType.CreateDate : p.DocumentType.UpdateDate.Value
                                                                                                       },
                                                                                                       Purpose = new DocumentPurposeModel()
                                                                                                       {
                                                                                                           ID = p.DocumentPurpose.PurposeID,
                                                                                                           Name = p.DocumentPurpose.PurposeName,
                                                                                                           Description = p.DocumentPurpose.PurposeDescription,
                                                                                                           LastModifiedBy = p.DocumentPurpose.UpdateDate == null ? p.DocumentPurpose.CreateBy : p.DocumentPurpose.UpdateBy,
                                                                                                           LastModifiedDate = p.DocumentPurpose.UpdateDate == null ? p.DocumentPurpose.CreateDate : p.DocumentPurpose.UpdateDate.Value
                                                                                                       },
                                                                                                       FileName = p.Filename,
                                                                                                       DocumentPath = p.DocumentPath,
                                                                                                       IsDormant = false,//p.IsDormant.Value,
                                                                                                       LastModifiedBy = p.UpdateDate == null ? p.CreateBy : p.UpdateBy,
                                                                                                       LastModifiedDate = p.UpdateDate == null ? p.CreateDate : p.UpdateDate.Value
                                                                                                   }).ToList(),
                                                                 CreateDate = a.CreateDate,
                                                                 LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                                 LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,
                                                                 IsDocumentComplete = a.IsDocumentComplete, //modify by dani 7-6-2016
                                                                 Remarks = a.Remarks,
                                                                 Amount = a.Amount > 0 ? a.Amount : 0,
                                                                 DealNumber = string.IsNullOrEmpty(a.DealNumber) ? "-" : a.DealNumber,
                                                                 ValueDate = a.ValueDate ?? null,
                                                             }).SingleOrDefault();



                // fill payment data
                if (datatransaction != null)
                {
                    string cif = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(instanceID)).Select(a => a.Customer.CIF).FirstOrDefault();
                    ICustomerRepository customerRepo = new CustomerRepository();
                    CustomerModel customer = new CustomerModel();
                    // changed, chandra : 2015.03.23
                    if (customerRepo.GetCustomerByTransactionReviseMaker(datatransaction.ID, ref customer, ref message))
                    {
                        datatransaction.Customer = customer;
                    }


                    customerRepo.Dispose();
                    customer = null;


                    // get lld code & info from last checker
                    var checker = (from a in context.Transactions
                                   where a.WorkflowInstanceID.Value.Equals(instanceID)
                                   select a.TransactionCheckerDatas
                                    .OrderByDescending(b => b.ApproverID)
                                    .FirstOrDefault()
                                   ).SingleOrDefault();

                    if (checker != null)
                    {
                        datatransaction.Others = checker.Others;

                        datatransaction.IsSignatureVerified = checker.IsSignatureVerified;
                        datatransaction.IsDormantAccount = checker.IsDormantAccount;
                        datatransaction.IsFreezeAccount = checker.IsFreezeAccount;
                    }

                }
                //===========================================

                TransactionTMODetailModel dataTMO = (from a in context.TransactionTMOes
                                                     where a.TransactionID.Equals(datatransaction.ID)
                                                     select new TransactionTMODetailModel
                                                     {
                                                         ID = a.TransactionID,
                                                         WorkflowInstanceID = datatransaction.WorkflowInstanceID.Value,
                                                         ApplicationID = datatransaction.ApplicationID,
                                                         ApproverID = a.ApproverID != null ? a.ApproverID : 0,
                                                         Product = new ProductModel()
                                                         {
                                                             ID = datatransaction.Product.ID,
                                                             Code = datatransaction.Product.Code,
                                                             Name = datatransaction.Product.Name,
                                                             WorkflowID = datatransaction.Product.WorkflowID,
                                                             Workflow = datatransaction.Product.Workflow,
                                                             LastModifiedBy = datatransaction.Product.LastModifiedBy,
                                                             LastModifiedDate = datatransaction.Product.LastModifiedDate
                                                         },
                                                         Channel = new ChannelModel()
                                                         {
                                                             ID = datatransaction.Channel.ID,
                                                             Name = datatransaction.Channel.Name,
                                                             LastModifiedBy = datatransaction.Channel.LastModifiedBy,
                                                             LastModifiedDate = datatransaction.Channel.LastModifiedDate
                                                         },
                                                         IsSignatureVerified = datatransaction.IsSignatureVerified,
                                                         IsFreezeAccount = datatransaction.IsFreezeAccount,
                                                         IsDormantAccount = datatransaction.IsDormantAccount,
                                                         IsDraft = datatransaction.IsDraft,
                                                         Others = datatransaction.Others,
                                                         CreateDate = a.CreateDate,
                                                         LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                         LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,
                                                         Remarks = a.Remarks,
                                                         ValueDate = datatransaction.ValueDate,
                                                         Amount = datatransaction.Amount,
                                                         DealNumber = datatransaction.DealNumber,
                                                         Currency = new CurrencyModel()
                                                         {
                                                             ID = datatransaction.Currency.ID,
                                                             Code = datatransaction.Currency.Code,
                                                             Description = datatransaction.Currency.Description,
                                                             CodeDescription = datatransaction.Currency.CodeDescription
                                                         }
                                                     }).OrderByDescending(a => a.ID).FirstOrDefault();


                if (dataTMO != null)
                {
                    dataTMO.Customer = datatransaction.Customer;
                    dataTMO.Documents = datatransaction.Documents;
                    dataTMO.ReviseMakerDocuments = datatransaction.ReviseMakerDocuments;

                    if (dataTMO.LastModifiedDate > datatransaction.LastModifiedDate)
                    {
                        transaction = dataTMO;
                    }
                    else
                    {
                        transaction = datatransaction;
                    }
                }
                else
                {
                    transaction = datatransaction;
                }

                IsSuccess = true;

            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }

            return IsSuccess;
        }
        public bool GetTransactionMakerReviseAfterCheckerTMODetails(Guid instanceID, ref TransactionTMODetailModel transaction, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                context.Transactions.AsNoTracking();

                TransactionTMODetailModel datatransaction = (from a in context.Transactions
                                                             where a.WorkflowInstanceID.Value == instanceID
                                                             select new TransactionTMODetailModel
                                                             {
                                                                 ID = a.TransactionID,
                                                                 WorkflowInstanceID = a.WorkflowInstanceID.Value,
                                                                 ApplicationID = a.ApplicationID,
                                                                 ApproverID = a.ApproverID != null ? a.ApproverID : 0,
                                                                 //added by dani 11-3-2016
                                                                 Currency = new CurrencyModel()
                                                                 {
                                                                     ID = (a.Currency != null && a.Currency.CurrencyID > 0) ? a.Currency.CurrencyID : 0,
                                                                     Code = (a.Currency != null && !string.IsNullOrEmpty(a.Currency.CurrencyCode)) ? a.Currency.CurrencyCode : "-",
                                                                     Description = (a.Currency != null && !string.IsNullOrEmpty(a.Currency.CurrencyDescription)) ? a.Currency.CurrencyDescription : "-",
                                                                     CodeDescription = (a.Currency != null && !string.IsNullOrEmpty(a.Currency.CurrencyCode) && !string.IsNullOrEmpty(a.Currency.CurrencyDescription)) ? "(" + a.Currency.CurrencyCode + ")  " + a.Currency.CurrencyDescription : "-"
                                                                 },
                                                                 //end dani
                                                                 Product = new ProductModel()
                                                                 {
                                                                     ID = a.Product.ProductID,
                                                                     Code = a.Product.ProductCode,
                                                                     Name = a.Product.ProductName,
                                                                     WorkflowID = a.Product.ProductWorkflow.WorkflowID,
                                                                     Workflow = a.Product.ProductWorkflow.WorkflowName,
                                                                     LastModifiedBy = a.Product.UpdateDate == null ? a.Product.CreateBy : a.Product.UpdateBy,
                                                                     LastModifiedDate = a.Product.UpdateDate == null ? a.Product.CreateDate : a.Product.UpdateDate.Value
                                                                 },
                                                                 Channel = new ChannelModel()
                                                                 {
                                                                     ID = a.Channel.ChannelID,
                                                                     Name = a.Channel.ChannelName,
                                                                     LastModifiedBy = a.Channel.UpdateDate == null ? a.Channel.CreateBy : a.Channel.UpdateBy,
                                                                     LastModifiedDate = a.Channel.UpdateDate == null ? a.Channel.CreateDate : a.Channel.UpdateDate.Value
                                                                 },
                                                                 IsSignatureVerified = a.IsSignatureVerified,
                                                                 IsFreezeAccount = a.IsFrezeAccount,
                                                                 IsDormantAccount = a.IsDormantAccount,
                                                                 IsDraft = a.IsDraft,
                                                                 Others = a.Others,
                                                                 Documents = (from p in a.TransactionDocuments

                                                                              join c in context.CustomerUnderlyingFiles
                                                                              on p.DocumentPath equals c.DocumentPath into zi
                                                                              from c in zi.DefaultIfEmpty()

                                                                              where p.IsDeleted.Equals(false)
                                                                              select new TransactionDocumentModel()
                                                                              {
                                                                                  ID = c.UnderlyingFileID != null ? c.UnderlyingFileID : p.TransactionDocumentID,
                                                                                  Type = new DocumentTypeModel()
                                                                                  {
                                                                                      ID = p.DocumentType.DocTypeID,
                                                                                      Name = p.DocumentType.DocTypeName,
                                                                                      Description = p.DocumentType.DocTypeDescription,
                                                                                      LastModifiedBy = p.DocumentType.UpdateDate == null ? p.DocumentType.CreateBy : p.DocumentType.UpdateBy,
                                                                                      LastModifiedDate = p.DocumentType.UpdateDate == null ? p.DocumentType.CreateDate : p.DocumentType.UpdateDate.Value
                                                                                  },
                                                                                  Purpose = new DocumentPurposeModel()
                                                                                  {
                                                                                      ID = p.DocumentPurpose.PurposeID,
                                                                                      Name = p.DocumentPurpose.PurposeName,
                                                                                      Description = p.DocumentPurpose.PurposeDescription,
                                                                                      LastModifiedBy = p.DocumentPurpose.UpdateDate == null ? p.DocumentPurpose.CreateBy : p.DocumentPurpose.UpdateBy,
                                                                                      LastModifiedDate = p.DocumentPurpose.UpdateDate == null ? p.DocumentPurpose.CreateDate : p.DocumentPurpose.UpdateDate.Value
                                                                                  },
                                                                                  FileName = p.Filename,
                                                                                  DocumentPath = p.DocumentPath,
                                                                                  IsDormant = p.IsDormant.Value,
                                                                                  LastModifiedBy = p.UpdateDate == null ? p.CreateBy : p.UpdateBy,
                                                                                  LastModifiedDate = p.UpdateDate == null ? p.CreateDate : p.UpdateDate.Value

                                                                              }).ToList(),
                                                                 ReviseMakerDocuments = (from p in a.TransactionDocuments
                                                                                         where p.IsDeleted.Equals(false)
                                                                                         select new ReviseMakerDocumentModel()
                                                                                         {
                                                                                             UnderlyingID = 0,
                                                                                             ID = p.TransactionDocumentID,
                                                                                             Type = new DocumentTypeModel()
                                                                                             {
                                                                                                 ID = p.DocumentType.DocTypeID,
                                                                                                 Name = p.DocumentType.DocTypeName,
                                                                                                 Description = p.DocumentType.DocTypeDescription,
                                                                                                 LastModifiedBy = p.DocumentType.UpdateDate == null ? p.DocumentType.CreateBy : p.DocumentType.UpdateBy,
                                                                                                 LastModifiedDate = p.DocumentType.UpdateDate == null ? p.DocumentType.CreateDate : p.DocumentType.UpdateDate.Value
                                                                                             },
                                                                                             Purpose = new DocumentPurposeModel()
                                                                                             {
                                                                                                 ID = p.DocumentPurpose.PurposeID,
                                                                                                 Name = p.DocumentPurpose.PurposeName,
                                                                                                 Description = p.DocumentPurpose.PurposeDescription,
                                                                                                 LastModifiedBy = p.DocumentPurpose.UpdateDate == null ? p.DocumentPurpose.CreateBy : p.DocumentPurpose.UpdateBy,
                                                                                                 LastModifiedDate = p.DocumentPurpose.UpdateDate == null ? p.DocumentPurpose.CreateDate : p.DocumentPurpose.UpdateDate.Value
                                                                                             },
                                                                                             FileName = p.Filename,
                                                                                             DocumentPath = p.DocumentPath,
                                                                                             IsDormant = false,//p.IsDormant.Value,
                                                                                             LastModifiedBy = p.UpdateDate == null ? p.CreateBy : p.UpdateBy,
                                                                                             LastModifiedDate = p.UpdateDate == null ? p.CreateDate : p.UpdateDate.Value
                                                                                         }).ToList(),
                                                                 CreateDate = a.CreateDate,
                                                                 LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                                 LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,
                                                                 IsDocumentComplete = a.IsDocumentComplete,//edit by dani 7-6-2016 //false,
                                                                 Remarks = a.Remarks,
                                                                 Amount = a.Amount > 0 ? a.Amount : 0,
                                                                 DealNumber = string.IsNullOrEmpty(a.DealNumber) ? "-" : a.DealNumber,
                                                                 ValueDate = a.ValueDate ?? null,
                                                             }).SingleOrDefault();



                // fill payment data
                if (datatransaction != null)
                {
                    string cif = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(instanceID)).Select(a => a.Customer.CIF).FirstOrDefault();
                    ICustomerRepository customerRepo = new CustomerRepository();
                    CustomerModel customer = new CustomerModel();
                    // changed, chandra : 2015.03.23
                    if (customerRepo.GetCustomerByTransactionReviseMaker(datatransaction.ID, ref customer, ref message))
                    {
                        datatransaction.Customer = customer;
                    }


                    customerRepo.Dispose();
                    customer = null;


                    // get lld code & info from last checker
                    var checker = (from a in context.Transactions
                                   where a.WorkflowInstanceID.Value.Equals(instanceID)
                                   select a.TransactionCheckerDatas
                                    .OrderByDescending(b => b.ApproverID)
                                    .FirstOrDefault()
                                   ).SingleOrDefault();

                    if (checker != null)
                    {
                        datatransaction.Others = checker.Others;

                        datatransaction.IsSignatureVerified = checker.IsSignatureVerified;
                        datatransaction.IsDormantAccount = checker.IsDormantAccount;
                        datatransaction.IsFreezeAccount = checker.IsFreezeAccount;
                    }

                }
                //===========================================

                TransactionTMODetailModel dataTMO = (from a in context.TransactionTMOes
                                                     where a.TransactionID.Equals(datatransaction.ID)
                                                     select new TransactionTMODetailModel
                                                     {
                                                         ID = a.TransactionID,
                                                         WorkflowInstanceID = datatransaction.WorkflowInstanceID.Value,
                                                         ApplicationID = datatransaction.ApplicationID,
                                                         ApproverID = a.ApproverID != null ? a.ApproverID : 0,
                                                         Product = new ProductModel()
                                                         {
                                                             ID = datatransaction.Product.ID,
                                                             Code = datatransaction.Product.Code,
                                                             Name = datatransaction.Product.Name,
                                                             WorkflowID = datatransaction.Product.WorkflowID,
                                                             Workflow = datatransaction.Product.Workflow,
                                                             LastModifiedBy = datatransaction.Product.LastModifiedBy,
                                                             LastModifiedDate = datatransaction.Product.LastModifiedDate
                                                         },
                                                         Channel = new ChannelModel()
                                                         {
                                                             ID = datatransaction.Channel.ID,
                                                             Name = datatransaction.Channel.Name,
                                                             LastModifiedBy = datatransaction.Channel.LastModifiedBy,
                                                             LastModifiedDate = datatransaction.Channel.LastModifiedDate
                                                         },
                                                         IsSignatureVerified = datatransaction.IsSignatureVerified,
                                                         IsFreezeAccount = datatransaction.IsFreezeAccount,
                                                         IsDormantAccount = datatransaction.IsDormantAccount,
                                                         IsDraft = datatransaction.IsDraft,
                                                         Others = datatransaction.Others,
                                                         CreateDate = a.CreateDate,
                                                         LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                         LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,
                                                         Remarks = a.Remarks,
                                                         ValueDate = datatransaction.ValueDate,
                                                         Amount = datatransaction.Amount,
                                                         DealNumber = datatransaction.DealNumber,
                                                         Currency = new CurrencyModel()
                                                         {
                                                             ID = datatransaction.Currency.ID,
                                                             Code = datatransaction.Currency.Code,
                                                             Description = datatransaction.Currency.Description,
                                                             CodeDescription = datatransaction.Currency.CodeDescription
                                                         }
                                                     }).OrderByDescending(a => a.ID).FirstOrDefault();


                if (dataTMO != null)
                {
                    dataTMO.Customer = datatransaction.Customer;
                    dataTMO.Documents = datatransaction.Documents;
                    dataTMO.ReviseMakerDocuments = datatransaction.ReviseMakerDocuments;

                    if (dataTMO.LastModifiedDate > datatransaction.LastModifiedDate)
                    {
                        transaction = dataTMO;
                    }
                    else
                    {
                        transaction = datatransaction;
                    }
                }
                else
                {
                    transaction = datatransaction;
                }

                IsSuccess = true;

            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }

            return IsSuccess;
        }
        public bool GetTransactionCheckerDetails(Guid workflowInstanceID, long approverID, ref TransactionTMOCheckerDetailModel output, ref string message)
        {
            bool IsSuccess = false;

            var transactionID = context.Transactions
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.TransactionID)
                        .SingleOrDefault();

            try
            {
                output = (from a in context.Transactions.AsNoTracking()
                          where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                          select new TransactionTMOCheckerDetailModel
                          {
                              Checker = a.TransactionCheckerDatas
                                .Where(x => x.TransactionID.Equals(transactionID))
                                  .OrderByDescending(x => x.TransactionCheckerDataID)
                                .Select(x => new TransactionTMOCheckerDataModel()
                                {
                                    ID = x.TransactionCheckerDataID,
                                    Others = x.Others,

                                    IsSignatureVerified = x.IsSignatureVerified,
                                    IsDormantAccount = x.IsDormantAccount,
                                    IsFreezeAccount = x.IsFreezeAccount,
                                    IsCallbackRequired = x.IsCallbackRequired,
                                    IsLOIAvailable = x.IsLOIAvailable,
                                    IsStatementLetterCopy = x.IsStatementLetterCopy

                                }).FirstOrDefault(),
                              Verify = a.TransactionCheckers
                                .Where(x => x.TransactionID.Equals(transactionID) && x.IsDeleted == false)
                                .Select(x => new VerifyModel()
                                {
                                    ID = x.TransactionColumnID,
                                    Name = x.TransactionColumn.ColumnName,
                                    IsVerified = x.IsVerified
                                }).ToList()
                          }).SingleOrDefault();

                // replace output while Checker & Verify is empty
                if (output != null)
                {
                    // replace Checker while empty
                    if (output.Checker == null)
                    {
                        var trans = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID)).SingleOrDefault();

                        output.Checker = new TransactionTMOCheckerDataModel()
                        {
                            ID = 0,
                            Others = trans.Others,

                        };
                    }

                    // replace verify list while empty
                    if (!output.Verify.Any())
                    {
                        output.Verify = context.TransactionColumns
                            .Where(x => x.IsPPUCheckerTMO.Value.Equals(true) && x.IsDeleted.Equals(false))
                            .Select(x => new VerifyModel()
                            {
                                ID = x.TransactionColumnID,
                                Name = x.ColumnName,
                                IsVerified = false
                            })
                            .ToList();
                    }
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }
        public bool GetTransactionDetails(Guid instanceID, ref TransactionTMODetailModel transaction, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                context.Transactions.AsNoTracking();

                TransactionTMODetailModel datatransaction = (from a in context.Transactions
                                                             where a.WorkflowInstanceID.Value.Equals(instanceID)
                                                             select new TransactionTMODetailModel
                                                             {
                                                                 ID = a.TransactionID,
                                                                 WorkflowInstanceID = a.WorkflowInstanceID.Value,
                                                                 ApplicationID = a.ApplicationID,
                                                                 ApproverID = a.ApproverID != null ? a.ApproverID : 0,
                                                                 Product = new ProductModel()
                                                                 {
                                                                     ID = a.Product.ProductID,
                                                                     Code = a.Product.ProductCode,
                                                                     Name = a.Product.ProductName,
                                                                     WorkflowID = a.Product.ProductWorkflow.WorkflowID,
                                                                     Workflow = a.Product.ProductWorkflow.WorkflowName,
                                                                     LastModifiedBy = a.Product.UpdateDate == null ? a.Product.CreateBy : a.Product.UpdateBy,
                                                                     LastModifiedDate = a.Product.UpdateDate == null ? a.Product.CreateDate : a.Product.UpdateDate.Value
                                                                 },
                                                                 Channel = new ChannelModel()
                                                                 {
                                                                     ID = a.Channel.ChannelID,
                                                                     Name = a.Channel.ChannelName,
                                                                     LastModifiedBy = a.Channel.UpdateDate == null ? a.Channel.CreateBy : a.Channel.UpdateBy,
                                                                     LastModifiedDate = a.Channel.UpdateDate == null ? a.Channel.CreateDate : a.Channel.UpdateDate.Value
                                                                 },

                                                                 IsSignatureVerified = a.IsSignatureVerified,
                                                                 IsFreezeAccount = a.IsFrezeAccount,
                                                                 IsDormantAccount = a.IsDormantAccount,
                                                                 IsDraft = a.IsDraft,
                                                                 Others = a.Others,

                                                                 Documents = a.TransactionDocuments.Where(x => x.IsDeleted.Equals(false)).Select(x => new TransactionDocumentModel()
                                                                 {
                                                                     ID = x.TransactionDocumentID,
                                                                     Type = new DocumentTypeModel()
                                                                     {
                                                                         ID = x.DocumentType.DocTypeID,
                                                                         Name = x.DocumentType.DocTypeName,
                                                                         Description = x.DocumentType.DocTypeDescription,
                                                                         LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                                                         LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                                                     },
                                                                     Purpose = new DocumentPurposeModel()
                                                                     {
                                                                         ID = x.DocumentPurpose.PurposeID,
                                                                         Name = x.DocumentPurpose.PurposeName,
                                                                         Description = x.DocumentPurpose.PurposeDescription,
                                                                         LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                                                         LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                                                     },
                                                                     FileName = x.Filename,
                                                                     DocumentPath = x.DocumentPath,
                                                                     LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                                                     LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value,
                                                                     IsDormant = x.IsDormant == null ? false : x.IsDormant
                                                                 }).ToList(),

                                                                 //Tambah Agung
                                                                 Currency = new CurrencyModel
                                                                 {
                                                                     ID = (a.Currency != null && a.Currency.CurrencyID > 0) ? a.Currency.CurrencyID : 0,
                                                                     Code = (a.Currency != null && !string.IsNullOrEmpty(a.Currency.CurrencyCode)) ? a.Currency.CurrencyCode : "-",
                                                                     Description = (a.Currency != null && !string.IsNullOrEmpty(a.Currency.CurrencyDescription)) ? a.Currency.CurrencyDescription : "-",
                                                                     CodeDescription = (a.Currency != null && !string.IsNullOrEmpty(a.Currency.CurrencyCode) && !string.IsNullOrEmpty(a.Currency.CurrencyDescription)) ? "(" + a.Currency.CurrencyCode + ")  " + a.Currency.CurrencyDescription : "-"
                                                                 },
                                                                 Amount = a.Amount > 0 ? a.Amount : 0,
                                                                 DealNumber = string.IsNullOrEmpty(a.DealNumber) ? "-" : a.DealNumber,
                                                                 ValueDate = a.ValueDate,
                                                                 //End
                                                                 CreateDate = a.CreateDate,
                                                                 LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                                 LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,
                                                                 Remarks = string.IsNullOrEmpty(a.Remarks) ? "-" : a.Remarks,
                                                                 IsDocumentComplete = a.IsDocumentComplete
                                                             }).SingleOrDefault();

                // fill payment data
                if (datatransaction != null)
                {
                    //datatransaction.IsOtherBeneBank = datatransaction.Bank.Code != "999" ? false : true;
                    string cif = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(instanceID)).Select(a => a.Customer.CIF).FirstOrDefault();
                    ICustomerRepository customerRepo = new CustomerRepository();
                    CustomerModel customer = new CustomerModel();
                    // changed, chandra : 2015.03.23
                    if (customerRepo.GetCustomerByTransaction(datatransaction.ID, ref customer, ref message))
                    {
                        datatransaction.Customer = customer;
                    }

                    long IDTrns = datatransaction.ID;
                    var checker = context.TransactionCheckerDatas.Where(x => x.TransactionID.Equals(IDTrns)).Select(x => x).OrderByDescending(b => b.ApproverID).FirstOrDefault();
                    if (checker != null)
                    {

                        datatransaction.Others = checker.Others;
                        datatransaction.IsSignatureVerified = checker.IsSignatureVerified;
                        //basri 30-09-2015
                        datatransaction.IsDocumentComplete = datatransaction.IsDocumentComplete;
                        datatransaction.IsCallbackRequired = checker.IsCallbackRequired;
                        datatransaction.IsLOIAvailable = checker.IsLOIAvailable;
                        //end basri
                        datatransaction.IsDormantAccount = checker.IsDormantAccount;
                        datatransaction.IsFreezeAccount = checker.IsFreezeAccount;
                    }

                }

                //======================================
                TransactionTMODetailModel dataTMO = (from a in context.TransactionTMOes
                                                     where a.TransactionID.Equals(datatransaction.ID)
                                                     select new TransactionTMODetailModel
                                                     {
                                                         ID = a.TransactionID,
                                                         WorkflowInstanceID = datatransaction.WorkflowInstanceID.Value,
                                                         ApplicationID = datatransaction.ApplicationID,
                                                         ApproverID = a.ApproverID != null ? a.ApproverID : 0,
                                                         Product = new ProductModel()
                                                         {
                                                             ID = datatransaction.Product.ID,
                                                             Code = datatransaction.Product.Code,
                                                             Name = datatransaction.Product.Name,
                                                             WorkflowID = datatransaction.Product.WorkflowID,
                                                             Workflow = datatransaction.Product.Workflow,
                                                             LastModifiedBy = datatransaction.Product.LastModifiedBy,
                                                             LastModifiedDate = datatransaction.Product.LastModifiedDate
                                                         },
                                                         Channel = new ChannelModel()
                                                         {
                                                             ID = datatransaction.Channel.ID,
                                                             Name = datatransaction.Channel.Name,
                                                             LastModifiedBy = datatransaction.Channel.LastModifiedBy,
                                                             LastModifiedDate = datatransaction.Channel.LastModifiedDate

                                                         },
                                                         IsSignatureVerified = datatransaction.IsSignatureVerified,
                                                         IsFreezeAccount = datatransaction.IsFreezeAccount,
                                                         IsDormantAccount = datatransaction.IsDormantAccount,
                                                         IsDraft = datatransaction.IsDraft,
                                                         IsDocumentComplete = datatransaction.IsDocumentComplete,
                                                         Others = datatransaction.Others,
                                                         //Tambah Agung
                                                         Currency = new CurrencyModel
                                                         {
                                                             ID = datatransaction.Currency.ID,
                                                             Code = datatransaction.Currency.Code,
                                                             Description = datatransaction.Currency.Description,
                                                             CodeDescription = datatransaction.Currency.CodeDescription
                                                         },
                                                         Amount = datatransaction.Amount > 0 ? datatransaction.Amount : 0,
                                                         DealNumber = string.IsNullOrEmpty(datatransaction.DealNumber) ? "-" : datatransaction.DealNumber,
                                                         ValueDate = datatransaction.ValueDate,
                                                         Remarks = datatransaction.Remarks,
                                                         //End
                                                         CreateDate = a.CreateDate,
                                                         LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                         LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,

                                                     }).OrderByDescending(a => a.ID).FirstOrDefault();

                if (dataTMO != null)
                {
                    string cif = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(instanceID)).Select(a => a.Customer.CIF).FirstOrDefault();
                    ICustomerRepository customerRepo = new CustomerRepository();
                    CustomerModel customerpy = new CustomerModel();
                    if (customerRepo.GetCustomerByTransaction(dataTMO.ID, ref customerpy, ref message))
                    {
                        dataTMO.Customer = customerpy;
                    }

                    customerRepo.Dispose();
                    customerpy = null;



                    IList<TransactionDocumentModel> DocumentsTMO = new List<TransactionDocumentModel>();
                    if (datatransaction.Documents.Count > 0)
                    {
                        dataTMO.Documents = datatransaction.Documents.Select(x => new TransactionDocumentModel()
                        {
                            ID = x.ID,
                            Type = new DocumentTypeModel()
                            {
                                ID = x.Type.ID,
                                Name = x.Type.Name,
                                Description = x.Type.Description,
                                LastModifiedBy = x.LastModifiedBy,
                                LastModifiedDate = x.LastModifiedDate
                            },
                            Purpose = new DocumentPurposeModel()
                            {
                                ID = x.Purpose.ID,
                                Name = x.Purpose.Name,
                                Description = x.Purpose.Description,
                                LastModifiedBy = x.LastModifiedBy,
                                LastModifiedDate = x.LastModifiedDate
                            },
                            FileName = x.FileName,
                            DocumentPath = x.DocumentPath,
                            LastModifiedBy = x.LastModifiedBy,
                            LastModifiedDate = x.LastModifiedDate
                        }).ToList();
                    }
                    else
                    {
                        dataTMO.Documents = new List<TransactionDocumentModel>();
                    }

                    if (datatransaction.Currency != null)
                    {
                        dataTMO.Currency = new CurrencyModel()
                        {
                            Code = datatransaction.Currency.Code,
                            Description = datatransaction.Currency.Description,
                            CodeDescription = datatransaction.Currency.CodeDescription,
                            ID = datatransaction.Currency.ID
                        };
                    }
                    else
                    {
                        dataTMO.Currency = new CurrencyModel()
                        {
                            Code = "",
                            Description = "",
                            CodeDescription = ""
                        };
                    }

                    dataTMO.Amount = datatransaction.Amount > 0 ? datatransaction.Amount : 0;
                    dataTMO.DealNumber = string.IsNullOrEmpty(datatransaction.DealNumber) ? "" : datatransaction.DealNumber;
                    dataTMO.ValueDate = datatransaction.ValueDate != null ? datatransaction.ValueDate.Value : DateTime.UtcNow;
                    dataTMO.Remarks = string.IsNullOrEmpty(datatransaction.Remarks) ? "" : datatransaction.Remarks;
                }

                if (dataTMO != null)
                {
                    if (dataTMO.LastModifiedDate > datatransaction.LastModifiedDate)
                    {
                        transaction = dataTMO;
                    }
                    else
                    {
                        transaction = datatransaction;
                    }
                }
                else
                {
                    transaction = datatransaction;
                }



                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }

            return IsSuccess;
        }
        public bool AddTransactionChecker(Guid workflowInstanceID, long approverID, TransactionTMOCheckerDetailModel data, ref string message)
        {
            bool IsSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    var transactionID = context.Transactions
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.TransactionID)
                        .SingleOrDefault();

                    if (transactionID > 0)
                    {
                        Entity.Transaction data_t = context.Transactions.Where(a => a.TransactionID.Equals(transactionID)).SingleOrDefault();
                        if (data_t != null)
                        {
                            data_t.IsDocumentComplete = data.Transaction.IsDocumentComplete;
                            context.SaveChanges();
                        }

                        var checker = context.TransactionCheckers
                           .Where(a => a.TransactionID.Equals(transactionID) && a.IsDeleted == false)
                           .ToList();

                        var checkerData = context.TransactionCheckerDatas
                            .Where(a => a.TransactionID.Equals(transactionID))
                            .SingleOrDefault();

                        #region Transaction Checker Data
                        if (checkerData != null)
                        {


                            checkerData.Others = data.Checker.Others;
                            checkerData.IsSignatureVerified = data.Checker.IsSignatureVerified;
                            checkerData.IsDormantAccount = data.Checker.IsDormantAccount;
                            checkerData.IsFreezeAccount = data.Checker.IsFreezeAccount;
                            checkerData.IsCallbackRequired = data.Checker.IsCallbackRequired;
                            checkerData.IsLOIAvailable = data.Checker.IsLOIAvailable;
                            checkerData.UpdateDate = DateTime.UtcNow;
                            checkerData.UpdateBy = currentUser.GetCurrentUser().LoginName;

                            context.SaveChanges();
                        }
                        else
                        {

                            context.TransactionCheckerDatas.Add(new TransactionCheckerData()
                            {
                                ApproverID = approverID,
                                TransactionID = transactionID,
                                Others = data.Checker.Others,
                                IsSignatureVerified = data.Checker.IsSignatureVerified,
                                IsDormantAccount = data.Checker.IsDormantAccount,
                                IsFreezeAccount = data.Checker.IsFreezeAccount,
                                IsCallbackRequired = data.Checker.IsCallbackRequired,
                                IsLOIAvailable = data.Checker.IsLOIAvailable,
                                CreatedDate = DateTime.UtcNow,
                                CreatedBy = currentUser.GetCurrentUser().LoginName
                            });

                            context.SaveChanges();

                        }
                        #endregion

                        #region Transaction Checker
                        if (checker.Count > 0)
                        {
                            var checkerdata = context.TransactionCheckers.Where(a => a.TransactionID.Equals(transactionID)).ToList();
                            foreach (var item in checkerdata)
                            {
                                context.TransactionCheckers.Remove(item);
                            }
                            context.SaveChanges();

                            foreach (var item in data.Verify)
                            {
                                context.TransactionCheckers.Add(new TransactionChecker()
                                {
                                    ApproverID = approverID,
                                    TransactionID = transactionID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                });

                                context.SaveChanges();
                            }
                        }
                        else
                        {
                            foreach (var item in data.Verify)
                            {
                                context.TransactionCheckers.Add(new TransactionChecker()
                                {
                                    ApproverID = approverID,
                                    TransactionID = transactionID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                });

                                context.SaveChanges();
                            }
                        }
                        #endregion

                        checker = null;
                        checkerData = null;
                    }

                    ts.Complete();

                    IsSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }

            return IsSuccess;
        }
        public bool AddTransactionTMOChecker(Guid workflowInstanceID, long approverID, TMOCheckerDetailModel data, ref string message)
        {
            bool IsSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {

                    ts.Complete();

                    IsSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }

            return IsSuccess;
        }
        //Basri 01.10.2015
        #region TMO-Basri
        public bool GetTMODetails(Guid workflowInstanceID, ref TMODetailModel output, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                context.Transactions.AsNoTracking();
                // get main transaction data

                output = (from a in context.Transactions
                          where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                          select new TMODetailModel
                          {
                              ID = a.TransactionID,
                              WorkflowInstanceID = a.WorkflowInstanceID.Value,
                              ApplicationID = a.ApplicationID,
                              ApproverID = a.ApproverID != null ? a.ApproverID : 0,
                              Product = new ProductModel()
                              {
                                  ID = a.Product.ProductID,
                                  Code = a.Product.ProductCode,
                                  Name = a.Product.ProductName,
                                  WorkflowID = a.Product.ProductWorkflow.WorkflowID,
                                  Workflow = a.Product.ProductWorkflow.WorkflowName,
                                  LastModifiedBy = a.Product.UpdateDate == null ? a.Product.CreateBy : a.Product.UpdateBy,
                                  LastModifiedDate = a.Product.UpdateDate == null ? a.Product.CreateDate : a.Product.UpdateDate.Value
                              },
                              Channel = new ChannelModel()
                              {
                                  ID = a.Channel.ChannelID,
                                  Name = a.Channel.ChannelName,
                                  LastModifiedBy = a.Channel.UpdateDate == null ? a.Channel.CreateBy : a.Channel.UpdateBy,
                                  LastModifiedDate = a.Channel.UpdateDate == null ? a.Channel.CreateDate : a.Channel.UpdateDate.Value
                              },
                              Documents = a.TransactionDocuments.Where(x => x.IsDeleted.Equals(false)).Select(x => new TransactionDocumentModel()
                              {
                                  ID = x.TransactionDocumentID,
                                  Type = new DocumentTypeModel()
                                  {
                                      ID = x.DocumentType.DocTypeID,
                                      Name = x.DocumentType.DocTypeName,
                                      Description = x.DocumentType.DocTypeDescription,
                                      LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                      LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                  },
                                  Purpose = new DocumentPurposeModel()
                                  {
                                      ID = x.DocumentPurpose.PurposeID,
                                      Name = x.DocumentPurpose.PurposeName,
                                      Description = x.DocumentPurpose.PurposeDescription,
                                      LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                      LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                  },
                                  FileName = x.Filename,
                                  DocumentPath = x.DocumentPath,
                                  LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                  LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                              }).ToList(),
                              //15-2-2016 dani
                              Currency = new CurrencyModel
                              {
                                  ID = (a.Currency != null && a.Currency.CurrencyID > 0) ? a.Currency.CurrencyID : 0,
                                  Code = (a.Currency != null && !string.IsNullOrEmpty(a.Currency.CurrencyCode)) ? a.Currency.CurrencyCode : "-",
                                  Description = (a.Currency != null && !string.IsNullOrEmpty(a.Currency.CurrencyDescription)) ? a.Currency.CurrencyDescription : "-",
                                  CodeDescription = (a.Currency != null && !string.IsNullOrEmpty(a.Currency.CurrencyCode) && !string.IsNullOrEmpty(a.Currency.CurrencyDescription)) ? "(" + a.Currency.CurrencyCode + ")  " + a.Currency.CurrencyDescription : "-"
                              },
                              Amount = a.Amount > 0 ? a.Amount : 0,
                              DealNumber = string.IsNullOrEmpty(a.DealNumber) ? "-" : a.DealNumber,
                              ValueDate = a.ValueDate,
                              //15-2-2016 end dani
                              Remarks = string.IsNullOrEmpty(a.Remarks) ? "-" : a.Remarks,
                              CreateDate = a.CreateDate,
                              LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                              LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                          }).SingleOrDefault();

                if (output != null)
                {
                    string cif = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID)).Select(a => a.Customer.CIF).FirstOrDefault();
                    ICustomerRepository customerRepo = new CustomerRepository();
                    CustomerModel customer = new CustomerModel();
                    long TransactionID = output.ID;

                    bool isUnderlying = context.TransactionUnderlyings.Any(x => x.TransactionID.Equals(TransactionID));
                    if (isUnderlying)
                    {
                        if (customerRepo.GetCustomerByTransaction(TransactionID, ref customer, ref message))
                        {
                            output.Customer = customer;
                        }
                    }
                    else
                    {
                        if (customerRepo.GetCustomerByCIF(cif, ref customer, ref message))
                        {
                            output.Customer = customer;
                        }
                    }
                    customerRepo.Dispose();
                    customer = null;
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }

        public bool AddTransactionMaker(Guid workflowInstanceID, long approverID, TransactionTMODetailModel transaction, ref string message)
        {
            bool isSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    var data = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID)).SingleOrDefault();
                    if (data != null)
                    {
                        data.CIF = transaction.Customer.CIF;
                        data.ApproverID = approverID;
                        data.ChannelID = transaction.Channel.ID;
                        data.Remarks = transaction.Remarks;
                        data.CurrencyID = transaction.Currency.ID;
                        data.Amount = transaction.Amount;
                        data.ValueDate = transaction.ValueDate;
                        data.DealNumber = transaction.DealNumber;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().LoginName;

                        context.SaveChanges();

                        if (transaction.Documents != null)
                        {
                            var existingDocuments = (from x in context.TransactionDocuments
                                                     where x.TransactionID == data.TransactionID && x.IsDeleted == false
                                                     select x);
                            if (existingDocuments != null && existingDocuments.ToList().Count > 0)
                            {
                                foreach (var item in existingDocuments.ToList()) // remove All transaction document
                                {
                                    //if (item.PurposeID != 2)
                                    {
                                        context.TransactionDocuments.Remove(item);
                                    }
                                }
                                context.SaveChanges();

                                foreach (var item in transaction.Documents) //Add All Transaction document
                                {
                                    //if (item.Purpose.ID != 2)
                                    {
                                        Entity.TransactionDocument document = new Entity.TransactionDocument()
                                        {
                                            TransactionID = data.TransactionID,
                                            DocTypeID = item.Type.ID,
                                            PurposeID = item.Purpose.ID,
                                            Filename = item.FileName,
                                            IsDeleted = false,
                                            DocumentPath = item.DocumentPath,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().LoginName
                                        };

                                        context.TransactionDocuments.Add(document);
                                    }
                                    context.SaveChanges();
                                }
                            }
                            else
                            {
                                foreach (var item in transaction.Documents)
                                {
                                    // if (item.Purpose.ID != 2) Add All transaction Document
                                    {
                                        Entity.TransactionDocument document = new Entity.TransactionDocument()
                                        {
                                            TransactionID = data.TransactionID,
                                            DocTypeID = item.Type.ID,
                                            PurposeID = item.Purpose.ID,
                                            Filename = item.FileName,
                                            IsDeleted = false,
                                            DocumentPath = item.DocumentPath,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().LoginName
                                        };
                                        context.TransactionDocuments.Add(document);
                                    }
                                    context.SaveChanges();
                                }
                            }
                        }

                    }
                    ts.Complete();
                    isSuccess = true;
                }

                catch (Exception ex)
                {
                    ts.Dispose();
                    message = ex.Message;
                }
            }

            return isSuccess;
        }
        public bool AddTransactionTMOMaker(Guid workflowInstanceID, long approverID, TMODetailModel tmo, ref string message)
        {
            bool isSuccess = false;

            //feedback

            try
            {

                Entity.TransactionTMO data = context.TransactionTMOes.Where(a => a.TransactionID.Equals(tmo.ID)).SingleOrDefault();

                if (data != null)
                {
                    //Update                    
                    data.ApproverID = approverID;
                    data.ChannelID = tmo.Channel.ID;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().LoginName;
                    data.Remarks = tmo.Remarks;

                }
                else
                {
                    // Insert
                    DBS.Entity.TransactionTMO dataTMO = new DBS.Entity.TransactionTMO()
                    {
                        TransactionID = tmo.ID,
                        ApproverID = approverID,
                        ChannelID = tmo.Channel.ID,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().LoginName,
                        Remarks = tmo.Remarks

                    };

                    context.TransactionTMOes.Add(dataTMO);

                }
                context.SaveChanges();

                isSuccess = true;

            }
            catch (Exception ex)
            {
                message = ex.InnerException.ToString();
            }

            return isSuccess;
        }

        public bool GetTransactionCallbackDetails(Guid workflowInstanceID, long approverID, ref TransactionTMOCallbackDetailModel output, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                var transactionID = context.Transactions
                       .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                       .Select(a => a.TransactionID)
                       .SingleOrDefault();

                var callbackID = context.TransactionCallbacks
                    .Where(a => a.IsUTC == false && a.TransactionID.Equals(transactionID))
                    .OrderByDescending(a => a.TransactionCallbackID)
                    .Select(a => a.TransactionCallbackID)
                    .FirstOrDefault();

                output = (from a in context.Transactions.AsNoTracking()
                          where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                          select new TransactionTMOCallbackDetailModel
                          {
                              Callbacks = (from b in context.TransactionCallbacks
                                           where b.TransactionID.Equals(transactionID)
                                           select new TransactionTMOCallbackTimeModel
                                           {
                                               ID = b.TransactionCallbackID,
                                               ApproverID = b.ApproverID,
                                               Contact = b.TransactionCallbackContacts.Select(y => new CustomerContactModel()
                                               {
                                                   ID = y.CustomerContact.ContactID,
                                                   Name = y.CustomerContact.ContactName,
                                                   PhoneNumber = y.CustomerContact.PhoneNumber,
                                                   DateOfBirth = y.CustomerContact.DateOfBirth,
                                                   IDNumber = y.CustomerContact.IdentificationNumber,
                                               }).FirstOrDefault(),
                                               Time = b.Time,
                                               IsUTC = b.IsUTC,
                                               Remark = b.Remark
                                           }).ToList(),
                              Verify = a.TransactionCallbackCheckers
                                .Select(z => new VerifyModel()
                                {
                                    ID = z.TransactionColumnID,
                                    Name = z.TransactionColumn.ColumnName,
                                    IsVerified = z.IsVerified
                                }).ToList()
                          }).SingleOrDefault();

                // replace verify list while empty
                if (output != null)
                {

                    output.Verify = (from a in context.TransactionColumns
                                     where a.IsDeleted.Equals(false) && a.IsCallBack.Equals(true)
                                     select new VerifyModel()
                                     {
                                         ID = a.TransactionColumnID,
                                         Name = a.ColumnName,
                                         IsVerified = false
                                     }).ToList();


                    var documentCompleteness = (from a in context.TransactionCheckers
                                                join b in context.TransactionColumns
                                                on a.TransactionColumnID equals b.TransactionColumnID
                                                where a.TransactionID.Equals(transactionID) && b.ColumnName.Equals("Document Completeness") && a.IsDeleted == false && b.IsDeleted == false
                                                select new VerifyModel()
                                                {
                                                    ID = a.TransactionColumnID,
                                                    Name = b.ColumnName,
                                                    IsVerified = a.IsVerified
                                                }).FirstOrDefault();

                    if (documentCompleteness != null)
                    {
                        output.Verify.Add(documentCompleteness);
                    }
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }

        public bool AddTransactionCallback(Guid workflowInstanceID, long approverID, TransactionTMOCallbackDetailModel data, ref string message)
        {
            bool IsSuccess = false;

            //feedbackcaller

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {

                    // insert trasaction TMO callback data
                    DBS.Entity.TransactionCallbackData dataCallback = new DBS.Entity.TransactionCallbackData()
                    {
                        TransactionID = data.Transaction.ID,
                        ApproverID = approverID,
                        ChannelID = data.Transaction.Channel.ID,
                        IsDormantAccount = data.Transaction.IsDormantAccount,
                        IsFreezeAccount = data.Transaction.IsFreezeAccount,
                        IsSignatureVerified = data.Transaction.IsSignatureVerified,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().LoginName,
                    };
                    //hardcode for prevent error while saving, due to different model between payment product and TMO product
                    dataCallback.Amount = 0;
                    dataCallback.AmountUSD = 0;
                    dataCallback.Rate = 0;
                    dataCallback.ApplicationDate = DateTime.UtcNow;
                    dataCallback.CurrencyID = 1;
                    dataCallback.DebitCurrencyID = 1;

                    context.TransactionCallbackDatas.Add(dataCallback);
                    context.SaveChanges();

                    // insert callback
                    foreach (var item in data.Callbacks)
                    {
                        if (item.Time.Hour > 0)
                        {
                            // Insert transaction callback
                            TransactionCallback callback = new TransactionCallback()
                            {
                                ApproverID = approverID,
                                TransactionID = data.Transaction.ID,
                                Time = item.Time.ToUniversalTime(),
                                IsUTC = item.IsUTC.Value,
                                Remark = item.Remark,
                                CreateDate = DateTime.UtcNow,
                                CreateBy = currentUser.GetCurrentUser().LoginName
                            };

                            context.TransactionCallbacks.Add(callback);
                            context.SaveChanges();

                            // Insert transaction callback contact
                            if (item.Contact != null)
                            {
                                context.TransactionCallbackContacts.Add(new TransactionCallbackContact()
                                {
                                    TransactionCallbackID = callback.TransactionCallbackID,
                                    ContactID = item.Contact.ID,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                });
                            }
                            context.SaveChanges();

                            callback = null;
                        }
                    }

                    ts.Complete();

                    IsSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }

            return IsSuccess;
        }

        public bool GetTransactionCheckerAfterCallbackDetails(Guid workflowInstanceID, long approverID, ref TransactionTMOCheckerAfterCallbackDetailModel output, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                var transactionID = context.Transactions
                       .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                       .Select(a => a.TransactionID)
                       .SingleOrDefault();

                output = (from a in context.Transactions.AsNoTracking()
                          where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                          select new TransactionTMOCheckerAfterCallbackDetailModel
                          {
                              Callback = a.TransactionCallbackDatas
                                .OrderByDescending(x => x.ApproverID)
                                .Select(x => new TransactionTMOCallbackModel()
                                {
                                    ID = x.TransactionCallbackDataID,
                                    Product = new ProductModel()
                                    {
                                        ID = a.Product.ProductID,
                                        Code = a.Product.ProductCode,
                                        Name = a.Product.ProductName,
                                        WorkflowID = a.Product.ProductWorkflow.WorkflowID,
                                        Workflow = a.Product.ProductWorkflow.WorkflowName,
                                        LastModifiedBy = a.Product.UpdateDate == null ? a.Product.CreateBy : a.Product.UpdateBy,
                                        LastModifiedDate = a.Product.UpdateDate == null ? a.Product.CreateDate : a.Product.UpdateDate.Value
                                    },
                                    Channel = new ChannelModel()
                                    {
                                        ID = x.Channel.ChannelID,
                                        Name = x.Channel.ChannelName,
                                        LastModifiedBy = x.Channel.UpdateDate == null ? x.Channel.CreateBy : x.Channel.UpdateBy,
                                        LastModifiedDate = x.Channel.UpdateDate == null ? x.Channel.CreateDate : x.Channel.UpdateDate.Value
                                    },
                                    Remarks = a.Remarks,
                                    IsSignatureVerified = x.IsSignatureVerified,
                                    IsDormantAccount = x.IsDormantAccount,
                                    IsFreezeAccount = x.IsFreezeAccount,
                                    Others = x.Other,
                                    LastModifiedBy = a.Channel.UpdateDate == null ? a.Channel.CreateBy : a.Channel.UpdateBy,
                                    LastModifiedDate = a.Channel.UpdateDate == null ? a.Channel.CreateDate : a.Channel.UpdateDate.Value
                                }).FirstOrDefault(),
                              Checker = a.TransactionCheckerDatas
                                .Where(x => x.TransactionID.Equals(transactionID))
                                .Select(x => new TransactionTMOCheckerDataModel()
                                {
                                    ID = x.TransactionCheckerDataID,
                                    Others = x.Others,
                                    IsSignatureVerified = x.IsSignatureVerified,
                                    IsDormantAccount = x.IsDormantAccount,
                                    IsFreezeAccount = x.IsFreezeAccount,
                                    IsLOIAvailable = x.IsLOIAvailable,
                                    IsCallbackRequired = x.IsCallbackRequired
                                }).FirstOrDefault(),
                              Verify = a.TransactionCallbackCheckers
                                .Where(x => x.IsDeleted == false)
                                .Select(x => new VerifyModel()
                                {
                                    ID = x.TransactionColumnID,
                                    Name = x.TransactionColumn.ColumnName,
                                    IsVerified = x.IsVerified
                                }).ToList()
                          }).SingleOrDefault();

                // replace output while Checker & Verify is empty
                if (output != null)
                {

                    // replace checker data while empty with data from caller
                    if (output.Checker == null)
                    {
                        output.Checker = new TransactionTMOCheckerDataModel()
                        {
                            ID = 0,
                            Others = output.Callback.Others,
                            IsSignatureVerified = output.Callback.IsSignatureVerified,
                            IsDormantAccount = output.Callback.IsDormantAccount,
                            IsFreezeAccount = output.Callback.IsFreezeAccount,
                            IsLOIAvailable = output.Checker.IsLOIAvailable

                        };
                    }

                    // replace verify list while empty
                    if (!output.Verify.Any())
                    {
                        output.Verify = context.TransactionColumns
                            .Where(x => x.IsPPUCheckerAfterCallbackTMO.Value.Equals(true) && x.IsDeleted.Equals(false))
                            .Select(x => new VerifyModel()
                            {
                                ID = x.TransactionColumnID,
                                Name = x.ColumnName,
                                IsVerified = false
                            })
                            .ToList();
                    }

                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }

        public bool GetTMOCheckerDetails(Guid workflowInstanceID, long approverID, ref TransactionTMOCheckerDetailModel output, ref string message)
        {
            bool IsSuccess = false;

            var transactionID = context.Transactions
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.TransactionID)
                        .SingleOrDefault();

            try
            {
                output = (from a in context.Transactions.AsNoTracking()
                          where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                          select new TransactionTMOCheckerDetailModel
                          {
                              Checker = a.TransactionCheckerDatas
                                .Where(x => x.TransactionID.Equals(transactionID))
                                  .OrderByDescending(x => x.TransactionCheckerDataID)
                                .Select(x => new TransactionTMOCheckerDataModel()
                                {
                                    ID = x.TransactionCheckerDataID,
                                    Others = x.Others,
                                    IsSignatureVerified = x.IsSignatureVerified,
                                    IsDormantAccount = x.IsDormantAccount,
                                    IsFreezeAccount = x.IsFreezeAccount,
                                    IsCallbackRequired = x.IsCallbackRequired,
                                    IsLOIAvailable = x.IsLOIAvailable,
                                    IsStatementLetterCopy = x.IsStatementLetterCopy
                                }).FirstOrDefault(),
                              Verify = context.TransactionColumns
                                .Where(x => x.IsPPUCheckerTMO.Value.Equals(true) && x.IsDeleted.Equals(false))
                                .Select(x => new VerifyModel()
                                {
                                    ID = x.TransactionColumnID,
                                    Name = x.ColumnName,
                                    IsVerified = false
                                })
                                .ToList()
                          }).SingleOrDefault();

                // replace output while Checker & Verify is empty
                if (output != null)
                {
                    // replace Checker while empty
                    if (output.Checker == null)
                    {
                        var trans = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID)).SingleOrDefault();

                        output.Checker = new TransactionTMOCheckerDataModel()
                        {
                            ID = 0,
                            Others = trans.Others
                        };
                    }
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }
        #endregion TMO-Basri
        //end basri
        // add Chandra : 20151123
        public bool GetTrackingTMODetails(Guid workflowInstanceID, ref TransactionTMOModel data, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                // context.Transactions.AsNoTracking();
                data = (from a in context.TransactionDeals
                        where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                        select new TransactionTMOModel
                        {
                            ID = a.TransactionDealID,
                            ApplicationID = a.ApplicationID,
                            CIF = a.CIF,
                            TradeDate = a.TradeDate,
                            ValueDate = a.ValueDate,
                            TZReference = a.TZRef,
                            OtherUnderlying = a.OtherUnderlying,
                            Rate = a.Rate,
                            StatementLetter = new StatementLetterModel()
                            {
                                ID = a.StatementLetter.StatementLetterID,
                                Name = a.StatementLetter.StatementLetterName
                            },
                            ProductType = new ProductTypeModel()
                            {
                                ID = a.ProductType.ProductTypeID,
                                Code = a.ProductType.ProductTypeCode,
                                Description = a.ProductType.ProductTypeDesc,
                                IsFlowValas = a.ProductType.IsFlowValas
                            },
                            BuyAmount = a.BuyAmount,
                            SellAmount = a.SellAmount,
                            AmountUSD = a.AmountUSD,
                            TransactionStatus = a.TransactionStatus,
                            IsUnderlyingCompleted = a.IsUnderlyingCompleted,
                            SubmissionDateSL = a.ActualSubmissionDateSL,
                            SubmissionDateInstruction = a.ActualSubmissionDateInstruction,
                            SubmissionDateUnderlying = a.ActualSubmissionDateUnderlying,
                            UnderlyingCodeID = a.UnderlyingCodeID,
                            AccountNumber = a.AccountNumber,
                            IsResident = a.IsResident.HasValue ? a.IsResident.Value : false,
                            IsOtherAccountNumber = a.IsOtherAccountNumber != null ? a.IsOtherAccountNumber : false,
                            IsJointAccount = a.IsJointAccount.HasValue ? a.IsJointAccount.Value : false,
                            Remarks = a.Remarks,
                            NB = a.NB,
                            SwapType = a.SwapType,
                            SwapTZDealNo = a.SwapTZDealNo,
                            IsNettingTransaction = a.IsNettingTransaction.HasValue ? a.IsNettingTransaction : false,
                            Documents = a.TransactionDealDocuments.Where(x => x.IsDeleted.Equals(false)).Select(x => new TransactionDealDocumentModel()
                            {
                                ID = x.TransactionDocumentID,
                                Type = new DocumentTypeModel()
                                {
                                    ID = x.DocumentType.DocTypeID,
                                    Name = x.DocumentType.DocTypeName,
                                    Description = x.DocumentType.DocTypeDescription,
                                    LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                    LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                },
                                Purpose = new DocumentPurposeModel()
                                {
                                    ID = x.DocumentPurpose.PurposeID,
                                    Name = x.DocumentPurpose.PurposeName,
                                    Description = x.DocumentPurpose.PurposeDescription,
                                    LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                    LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                },
                                FileName = x.Filename,
                                DocumentPath = x.DocumentPath,
                                LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                            }).ToList(),
                            CreateDate = a.CreatedDate,
                            LastModifiedBy = a.UpdateDate == null ? a.CreatedBy : a.UpdateBy,
                            LastModifiedDate = a.UpdateDate == null ? a.CreatedDate : a.UpdateDate.Value,
                            IsResubmit = a.IsResubmit


                        }).SingleOrDefault();

                if (data != null)
                {
                    var deal = context.TransactionDeals.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID)).Select(a => a).FirstOrDefault();
                    var TransactionDealID = deal.TransactionDealID;
                    var cif = deal.CIF;
                    string accountNumber = deal.AccountNumber;
                    ICustomerRepository customerRepo = new CustomerRepository();
                    CustomerModel customer = new CustomerModel();
                    if (deal.TransactionStatus != null) // view transaction, hanya menampilkan transaction underlying
                    {
                        bool isUnderlying = context.TransactionDealUnderlyings.Any(x => x.TransactionDealID.Equals(TransactionDealID));


                        if (isUnderlying)
                        {
                            if (customerRepo.GetCustomerByTransactionFXDealChecker(TransactionDealID, ref customer, ref message))
                            {
                                data.Customer = customer;
                            }
                        }
                        else
                        {

                            long isTransUnderlying = context.Transactions.Where(x => x.TZNumber.Equals(deal.TZRef)).Select(x => x.TransactionID).FirstOrDefault();

                            if (isTransUnderlying > 0)
                            {
                                if (customerRepo.GetCustomerByTransactionDealChecker(0, isTransUnderlying, ref customer, ref message))
                                {
                                    data.Customer = customer;
                                }
                                data.Documents = (from p in context.TransactionDocuments
                                                  where p.IsDeleted.Equals(false) && p.TransactionID.Equals(isTransUnderlying)
                                                  select new TransactionDealDocumentModel()
                                                  {
                                                      ID = p.TransactionDocumentID,
                                                      Type = new DocumentTypeModel()
                                                      {
                                                          ID = p.DocumentType.DocTypeID,
                                                          Name = p.DocumentType.DocTypeName,
                                                          Description = p.DocumentType.DocTypeDescription,
                                                          LastModifiedBy = p.DocumentType.UpdateDate == null ? p.DocumentType.CreateBy : p.DocumentType.UpdateBy,
                                                          LastModifiedDate = p.DocumentType.UpdateDate == null ? p.DocumentType.CreateDate : p.DocumentType.UpdateDate.Value
                                                      },
                                                      Purpose = new DocumentPurposeModel()
                                                      {
                                                          ID = p.DocumentPurpose.PurposeID,
                                                          Name = p.DocumentPurpose.PurposeName,
                                                          Description = p.DocumentPurpose.PurposeDescription,
                                                          LastModifiedBy = p.DocumentPurpose.UpdateDate == null ? p.DocumentPurpose.CreateBy : p.DocumentPurpose.UpdateBy,
                                                          LastModifiedDate = p.DocumentPurpose.UpdateDate == null ? p.DocumentPurpose.CreateDate : p.DocumentPurpose.UpdateDate.Value
                                                      },
                                                      FileName = p.Filename,
                                                      DocumentPath = p.DocumentPath,
                                                      LastModifiedBy = p.UpdateDate == null ? p.CreateBy : p.UpdateBy,
                                                      LastModifiedDate = p.UpdateDate == null ? p.CreateDate : p.UpdateDate.Value
                                                  }).ToList();
                            }
                        }
                    }
                    else
                    {   // Menampilkan transaction underlying dan Customer Underlying
                        if (customerRepo.GetCustomerByTransactionReviseDealMaker(TransactionDealID, ref customer, ref message))
                        {
                            data.Customer = customer;
                        }
                    }
                    //tbo tmo fx
                    data.DocumentTBO = (from tbo in context.TBOTMOTransactions
                                        join tDeal in context.TransactionDeals on tbo.TransactionDealsID equals tDeal.TransactionDealID
                                        join cus in context.Customers on tDeal.CIF equals cus.CIF
                                        where tDeal.TransactionDealID == TransactionDealID
                                        select new DocumentTBO()
                                        {
                                            TBOTMOTransactionID = tbo.TBOTMOTransactionID,
                                            TBOSLAID = tbo.TBOSLAID.Value,
                                            TBOTMOID = tbo.TBOTMODocumentID.Value,
                                            TBOTMOName = tbo.TBOTMODocumentName,
                                            ExpectedDateTBO = tbo.TBOTMOExpectedReceivedDate.Value,
                                            RemaksTBO = tbo.RemaksTBO,
                                            TransactionDealID = tDeal.TransactionDealID,
                                            CustomerName = cus.CustomerName,
                                            CIF = cus.CIF,
                                            IsChecklistTBO = tbo.IsChecklistTBO
                                        }).ToList();
                    //end
                    data.ReviseDealDocuments = (from cu in context.CustomerUnderlyings
                                                join cm in context.CustomerUnderlyingMappings on cu.UnderlyingID equals cm.UnderlyingID
                                                join cf in context.CustomerUnderlyingFiles on cm.UnderlyingFileID equals cf.UnderlyingFileID
                                                where cu.IsDeleted.Equals(false) && cm.IsDeleted.Equals(false) && cf.IsDeleted.Equals(false) &&
                                                (cu.CIF.Equals(cif))
                                                select new ReviseDealDocumentModel()
                                                {
                                                    UnderlyingID = cu.UnderlyingID,
                                                    ID = cf.UnderlyingFileID,
                                                    Type = new DocumentTypeModel()
                                                    {
                                                        ID = cf.DocumentType.DocTypeID,
                                                        Name = cf.DocumentType.DocTypeName,
                                                        Description = cf.DocumentType.DocTypeDescription,
                                                        LastModifiedBy = cf.DocumentType.UpdateDate == null ? cf.DocumentType.CreateBy : cf.DocumentType.UpdateBy,
                                                        LastModifiedDate = cf.DocumentType.UpdateDate == null ? cf.DocumentType.CreateDate : cf.DocumentType.UpdateDate.Value
                                                    },
                                                    Purpose = new DocumentPurposeModel()
                                                    {
                                                        ID = cf.DocumentPurpose.PurposeID,
                                                        Name = cf.DocumentPurpose.PurposeName,
                                                        Description = cf.DocumentPurpose.PurposeDescription,
                                                        LastModifiedBy = cf.DocumentPurpose.UpdateDate == null ? cf.DocumentPurpose.CreateBy : cf.DocumentPurpose.UpdateBy,
                                                        LastModifiedDate = cf.DocumentPurpose.UpdateDate == null ? cf.DocumentPurpose.CreateDate : cf.DocumentPurpose.UpdateDate.Value
                                                    },
                                                    FileName = cf.FileName,
                                                    DocumentPath = cf.DocumentPath,
                                                    IsDormant = false,//cf.IsDormant.Value,
                                                    LastModifiedBy = cf.UpdateDate == null ? cf.CreateBy : cf.UpdateBy,
                                                    LastModifiedDate = cf.UpdateDate == null ? cf.CreateDate : cf.UpdateDate.Value
                                                }).Concat(from p in context.TransactionDealDocuments.Where(x => x.TransactionDealID.Equals(TransactionDealID))
                                                          where p.PurposeID != 2 && p.IsDeleted.Equals(false)
                                                          select new ReviseDealDocumentModel()
                                                          {
                                                              UnderlyingID = 0,
                                                              ID = p.TransactionDocumentID,
                                                              Type = new DocumentTypeModel()
                                                              {
                                                                  ID = p.DocumentType.DocTypeID,
                                                                  Name = p.DocumentType.DocTypeName,
                                                                  Description = p.DocumentType.DocTypeDescription,
                                                                  LastModifiedBy = p.DocumentType.UpdateDate == null ? p.DocumentType.CreateBy : p.DocumentType.UpdateBy,
                                                                  LastModifiedDate = p.DocumentType.UpdateDate == null ? p.DocumentType.CreateDate : p.DocumentType.UpdateDate.Value
                                                              },
                                                              Purpose = new DocumentPurposeModel()
                                                              {
                                                                  ID = p.DocumentPurpose.PurposeID,
                                                                  Name = p.DocumentPurpose.PurposeName,
                                                                  Description = p.DocumentPurpose.PurposeDescription,
                                                                  LastModifiedBy = p.DocumentPurpose.UpdateDate == null ? p.DocumentPurpose.CreateBy : p.DocumentPurpose.UpdateBy,
                                                                  LastModifiedDate = p.DocumentPurpose.UpdateDate == null ? p.DocumentPurpose.CreateDate : p.DocumentPurpose.UpdateDate.Value
                                                              },
                                                              FileName = p.Filename,
                                                              DocumentPath = p.DocumentPath,
                                                              IsDormant = false,//p.IsDormant.Value,
                                                              LastModifiedBy = p.UpdateDate == null ? p.CreateBy : p.UpdateBy,
                                                              LastModifiedDate = p.UpdateDate == null ? p.CreateDate : p.UpdateDate.Value
                                                          }).ToList();
                    if (deal.IsOtherAccountNumber != null && deal.IsOtherAccountNumber == true)
                    {
                        CustomerAccountModel Account = new CustomerAccountModel();
                        data.Account = Account;
                        data.OtherAccountNumber = deal.OtherAccountNumber;
                    }
                    else
                    {
                        data.Account = (from j in context.SP_GETJointAccount(cif)
                                        where j.AccountNumber.Equals(accountNumber)
                                        select new CustomerAccountModel
                                        {
                                            AccountNumber = j.AccountNumber,
                                            Currency = new CurrencyModel
                                            {
                                                ID = j.CurrencyID.Value,
                                                Code = j.CurrencyCode,
                                                Description = j.CurrencyDescription
                                            },
                                            CustomerName = j.CustomerName,
                                            IsJointAccount = j.IsJointAccount
                                        }).FirstOrDefault();
                        data.AccountNumber = deal.AccountNumber;

                    }

                    CurrencyModel AccountCurrency = new CurrencyModel();
                    AccountCurrency.ID = deal.DebitCurrency.CurrencyID;
                    AccountCurrency.Code = deal.DebitCurrency.CurrencyCode;
                    AccountCurrency.Description = deal.DebitCurrency.CurrencyDescription;

                    data.DebitCurrency = AccountCurrency;
                    data.AccountCurrencyCode = AccountCurrency.Code;
                    if (deal.CurrencySell != null)
                    {
                        data.SellCurrency = new CurrencyModel()
                        {
                            ID = deal.CurrencySell.CurrencyID,
                            Code = deal.CurrencySell.CurrencyCode,
                            CodeDescription = deal.CurrencySell.CurrencyDescription,
                            Description = deal.CurrencySell.CurrencyDescription,
                            LastModifiedBy = deal.CurrencySell.UpdateDate == null ? deal.CurrencySell.CreateBy : deal.CurrencySell.UpdateBy,
                            LastModifiedDate = deal.CurrencySell.UpdateDate == null ? deal.CurrencySell.CreateDate : deal.CurrencySell.UpdateDate.Value
                        };
                    }

                    if (deal.CurrencyBuy != null)
                    {
                        data.BuyCurrency = new CurrencyModel()
                        {
                            ID = deal.CurrencyBuy.CurrencyID,
                            Code = deal.CurrencyBuy.CurrencyCode,
                            CodeDescription = deal.CurrencyBuy.CurrencyDescription,
                            Description = deal.CurrencyBuy.CurrencyDescription,
                            LastModifiedBy = deal.CurrencyBuy.UpdateDate == null ? deal.CurrencyBuy.CreateBy : deal.CurrencyBuy.UpdateBy,
                            LastModifiedDate = deal.CurrencyBuy.UpdateDate == null ? deal.CurrencyBuy.CreateDate : deal.CurrencyBuy.UpdateDate.Value
                        };
                    }

                    customerRepo.Dispose();
                    customer = null;

                }
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }

        public bool GetTMODealCheckerDetails(Guid workflowInstanceID, long approverID, ref TransactionTMOCheckerDetailModel output, ref string message)
        {
            bool IsSuccess = false;

            var transactionDealID = context.TransactionDeals
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.TransactionDealID)
                        .SingleOrDefault();

            try
            {
                output = (from a in context.TransactionDeals.AsNoTracking()
                          where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                          select new TransactionTMOCheckerDetailModel
                          {
                              Verify = a.TransactionDealCheckers
                                .Where(x => x.TransactionDealID.Equals(transactionDealID) && x.IsDeleted == false)
                                .Select(x => new VerifyModel()
                                {
                                    ID = x.TransactionColumnID,
                                    Name = x.TransactionColumn.ColumnName,
                                    IsVerified = x.IsVerified
                                }).ToList()
                          }).SingleOrDefault();

                // replace output while Checker & Verify is empty
                if (output != null)
                {

                    if (!output.Verify.Any())
                    {
                        output.Verify = context.TransactionColumns
                            .Where(x => x.IsTMOCheckerFX.Value.Equals(true) && x.IsDeleted.Equals(false))
                            .Select(x => new VerifyModel()
                            {
                                ID = x.TransactionColumnID,
                                Name = x.ColumnName,
                                IsVerified = false
                            })
                            .ToList();
                    }
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }
        public bool AddTMODealChecker(Guid workflowInstanceID, long approverID, TransactionTMOTrackingCheckerDetailModel data, ref string message)
        {
            bool IsSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    var transactionDealID = context.TransactionDeals
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.TransactionDealID)
                        .SingleOrDefault();

                    if (transactionDealID > 0)
                    {
                        /*Entity.TransactionDeal data_t = context.TransactionDeals.Where(a => a.TransactionDealID.Equals(transactionDealID)).SingleOrDefault();
                        if (data_t != null)
                        {
                            data_t.IsDocumentComplete = data.Transaction.IsDocumentComplete;
                            context.SaveChanges();
                        } */

                        var checker = context.TransactionDealCheckers
                           .Where(a => a.TransactionDealID.Equals(transactionDealID) && a.IsDeleted == false)
                           .ToList();

                        /* var checkerData = context.TransactionCheckerDatas
                             .Where(a => a.TransactionID.Equals(transactionID))
                             .SingleOrDefault(); */

                        #region Transaction Checker
                        if (checker.Count > 0)
                        {
                            var checkerdata = context.TransactionDealCheckers.Where(a => a.TransactionDealID.Equals(transactionDealID)).ToList();
                            foreach (var item in checkerdata)
                            {
                                context.TransactionDealCheckers.Remove(item);
                            }
                            context.SaveChanges();

                            foreach (var item in data.Verify)
                            {
                                context.TransactionDealCheckers.Add(new TransactionDealChecker()
                                {
                                    ApproverID = (int)approverID,
                                    TransactionDealID = transactionDealID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                });

                                context.SaveChanges();
                            }
                        }
                        else
                        {
                            foreach (var item in data.Verify)
                            {
                                context.TransactionDealCheckers.Add(new TransactionDealChecker()
                                {
                                    ApproverID = (int)approverID,
                                    TransactionDealID = transactionDealID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                });

                                context.SaveChanges();
                            }
                        }
                        #endregion

                        checker = null;
                    }

                    // rollback Underlying 
                    Entity.TransactionDeal TMOData = context.TransactionDeals.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID)).SingleOrDefault();
                    if (TMOData != null)
                    {
                        long transactionID = TMOData.TransactionDealID;
                        bool isSuccess = RoolbackDealCustomerUnderlying(transactionID, ref message);
                        //bool isSuccess2 = DeleteTransDealDocuments(transactionID, ref message);
                        //if (!isSuccess || !isSuccess2)
                        //{
                        //    ts.Dispose();
                        //}
                        if (!isSuccess)
                        {
                            ts.Dispose();
                        }
                        //if (TMOData.SwapType != null & TMOData.NB != null)
                        //{
                        //    long SwapTransactionID = data.Transaction.SwapTransactionID.Value;
                        //    bool isSwapSuccess = RoolbackTransDealUnderlying(SwapTransactionID, ref message); // rollback only TransactionDealUnderlying for Swap
                        //    //bool isSwapSuccess2 = DeleteTransDealDocuments(SwapTransactionID, ref message);
                        //    //if (!isSwapSuccess || !isSwapSuccess2)
                        //    //{
                        //    //    ts.Dispose();
                        //    //}
                        //    if (!isSwapSuccess)
                        //    {
                        //        ts.Dispose();
                        //    }
                        //}

                    }

                    ts.Complete();

                    IsSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }

            return IsSuccess;
        }
        public bool UpdateCheckerSubmit(Guid workflowInstanceID, long approverID, TransactionTMOTrackingCheckerDetailModel data, ref string message)
        {
            bool IsSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    var transactionDealID = context.TransactionDeals
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.TransactionDealID)
                        .SingleOrDefault();

                    if (transactionDealID > 0)
                    {
                        var checker = context.TransactionDealCheckers
                           .Where(a => a.TransactionDealID.Equals(transactionDealID) && a.IsDeleted == false)
                           .ToList();

                        #region Transaction Checker
                        if (checker.Count > 0)
                        {
                            var checkerdata = context.TransactionDealCheckers.Where(a => a.TransactionDealID.Equals(transactionDealID)).ToList();
                            foreach (var item in checkerdata)
                            {
                                context.TransactionDealCheckers.Remove(item);
                            }
                            context.SaveChanges();

                            foreach (var item in data.Verify)
                            {
                                context.TransactionDealCheckers.Add(new TransactionDealChecker()
                                {
                                    ApproverID = approverID,
                                    TransactionDealID = transactionDealID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                });

                                context.SaveChanges();
                            }
                        }
                        else
                        {
                            foreach (var item in data.Verify)
                            {
                                context.TransactionDealCheckers.Add(new TransactionDealChecker()
                                {
                                    ApproverID = approverID,
                                    TransactionDealID = transactionDealID,
                                    TransactionColumnID = item.ID,
                                    IsVerified = item.IsVerified,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                });

                                context.SaveChanges();
                            }
                        }
                        #endregion
                    }
                    ts.Complete();
                    IsSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }
            return IsSuccess;
        }
        public bool UpdateSubmitReviseFromCSOTMO(Guid workflowInstanceID, long approverID, TMOCSODetailModel data, ref string message)
        {
            bool IsSuccess = false;
            using (System.Transactions.TransactionScope ts = new System.Transactions.TransactionScope())
            {
                try
                {
                    Entity.Transaction DataTransaction = context.Transactions.Where(d => d.TransactionID.Equals(data.Transaction.ID)).SingleOrDefault();
                    if (DataTransaction != null)
                    {
                        #region Unused field but Not Null
                        DataTransaction.IsSignatureVerified = false;
                        DataTransaction.IsDormantAccount = false;
                        DataTransaction.IsFrezeAccount = false;
                        DataTransaction.BeneName = "-";
                        DataTransaction.BankID = 1;
                        DataTransaction.IsResident = false;
                        DataTransaction.IsCitizen = false;
                        DataTransaction.AmountUSD = 0.00M;
                        DataTransaction.Rate = 0.00M;
                        DataTransaction.DebitCurrencyID = 1;
                        DataTransaction.BizSegmentID = 1;
                        DataTransaction.ChannelID = 1;
                        #endregion
                        DataTransaction.CIF = data.Transaction.Customer.CIF;
                        DataTransaction.ProductID = data.Transaction.Product.ID;
                        DataTransaction.TransactionID = data.Transaction.ID;
                        DataTransaction.ApplicationID = data.Transaction.ApplicationID;
                        DataTransaction.IsDraft = data.Transaction.IsDraft;
                        DataTransaction.IsNewCustomer = data.Transaction.IsNewCustomer;
                        //DataTransaction.CreateDate = DateTime.UtcNow;
                        //DataTransaction.CreateBy = currentUser.GetCurrentUser().LoginName;
                        DataTransaction.CurrencyID = (data.Transaction.Currency.ID != null && data.Transaction.Currency.ID != 0 ? data.Transaction.Currency.ID : 13);
                        DataTransaction.Amount = 0;
                        DataTransaction.IsTopUrgent = 0;
                        DataTransaction.IsDocumentComplete = false;
                        DataTransaction.StateID = (int)StateID.OnProgress;
                        DataTransaction.DealNumber = data.Transaction.DealNumber;
                        //DataTransaction.StaffID = data.Transaction.StaffID;
                        //DataTransaction.AccountNumber = (data.Transaction.AccountNumber == null ? null : data.Transaction.AccountNumber);
                        //DataTransaction.IsChangeRM = data.Transaction.IsChangeRM;
                        //DataTransaction.IsSegment = data.Transaction.IsSegment;
                        //DataTransaction.IsSolID = data.Transaction.IsSolID;

                        context.SaveChanges();

                    }
                    ts.Complete();
                    IsSuccess = true;
                }
                catch (Exception ex)
                {

                    message = ex.Message;
                    ts.Dispose(); ;
                }

            }
            return IsSuccess;
        }
        public bool UpdateCancelation(Guid workflowInstanceID, ref string message)
        {
            bool IsSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    var transactionDealID = context.TransactionDeals
                        .Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID))
                        .Select(a => a.TransactionDealID)
                        .SingleOrDefault();

                    // rollback Underlying 
                    Entity.TransactionDeal TMOData = context.TransactionDeals.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID)).SingleOrDefault();
                    if (TMOData != null)
                    {
                        long transactionID = TMOData.TransactionDealID;
                        IsSuccess = RoolbackDealCustomerUnderlying(transactionID, ref message);
                        if (!IsSuccess)
                        {
                            ts.Dispose();
                        }
                    }

                    ts.Complete();
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }

            return IsSuccess;
        }
        public bool RoolbackDealCustomerUnderlying(long IdTransactionDeal, ref string message)
        {
            bool isSuccess = false;

            try
            {
                List<Entity.TransactionDealUnderlying> items = context.TransactionDealUnderlyings.Where(a => a.TransactionDealID.Equals(IdTransactionDeal) && a.IsDeleted.Equals(false)).ToList();
                if (items != null && items.Count > 0)
                {
                    foreach (Entity.TransactionDealUnderlying data in items)
                    {
                        // set update customer underlying
                        var dataCustomerUnderlying = context.CustomerUnderlyings.Where(a => a.UnderlyingID.Equals(data.UnderlyingID)).SingleOrDefault();
                        dataCustomerUnderlying.AvailableAmountUSD += data.Amount;
                        dataCustomerUnderlying.IsUtilize = dataCustomerUnderlying.AmountUSD == dataCustomerUnderlying.AvailableAmountUSD ? false : true;
                        dataCustomerUnderlying.UpdateDate = DateTime.UtcNow;
                        dataCustomerUnderlying.UpdateBy = currentUser.GetCurrentUser().LoginName;
                        context.SaveChanges();
                        // set update transaction Deal underlying
                        var dataTransactionDealUnderlying = context.TransactionDealUnderlyings.Where(a => a.TransactionDealUnderlyingID.Equals(data.TransactionDealUnderlyingID)).SingleOrDefault();
                        dataTransactionDealUnderlying.IsDeleted = true;
                        dataTransactionDealUnderlying.UpdateDate = DateTime.UtcNow;
                        dataTransactionDealUnderlying.UpdateBy = currentUser.GetCurrentUser().LoginName;
                        context.SaveChanges();
                    }
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {

                message = ex.Message;
            }
            return isSuccess;
        }

        public bool RoolbackCustomerUnderlyingTMO(long IdTransaction, ref string message)
        {
            bool isSuccess = false;

            try
            {
                List<Entity.TransactionUnderlying> items = context.TransactionUnderlyings.Where(a => a.TransactionID.Equals(IdTransaction) && a.IsDeleted.Equals(false)).ToList();
                if (items != null && items.Count > 0)
                {
                    foreach (Entity.TransactionUnderlying data in items)
                    {
                        // set update customer underlying
                        var dataCustomerUnderlying = context.CustomerUnderlyings.Where(a => a.UnderlyingID.Equals(data.UnderlyingID)).SingleOrDefault();
                        dataCustomerUnderlying.AvailableAmountUSD += data.Amount;
                        dataCustomerUnderlying.IsUtilize = dataCustomerUnderlying.AmountUSD == dataCustomerUnderlying.AvailableAmountUSD ? false : true;
                        dataCustomerUnderlying.UpdateDate = DateTime.UtcNow;
                        dataCustomerUnderlying.UpdateBy = currentUser.GetCurrentUser().LoginName;
                        context.SaveChanges();
                        // set update transaction Deal underlying
                        var dataTransactionDealUnderlying = context.TransactionUnderlyings.Where(a => a.TransactionUnderlyingID.Equals(data.TransactionUnderlyingID)).SingleOrDefault();
                        dataTransactionDealUnderlying.IsDeleted = true;
                        dataTransactionDealUnderlying.UpdateDate = DateTime.UtcNow;
                        dataTransactionDealUnderlying.UpdateBy = currentUser.GetCurrentUser().LoginName;

                        Entity.TransactionUnderlyingHistory dataTransactionUnderlyingHistory = context.TransactionUnderlyingHistories.Where(a => a.TransactionUnderlyingID.Equals(data.TransactionUnderlyingID) && a.IsDeleted.Value.Equals(false)).SingleOrDefault();
                        if (dataTransactionUnderlyingHistory != null)
                        {
                            dataTransactionUnderlyingHistory.IsDeleted = true;
                        }

                        context.SaveChanges();
                    }
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {

                message = ex.Message;
            }
            return isSuccess;
        }
        public bool RoolbackTransDealUnderlying(long IdTransactionDeal, ref string message)
        {
            bool isSuccess = false;

            try
            {
                List<Entity.TransactionDealUnderlying> items = context.TransactionDealUnderlyings.Where(a => a.TransactionDealID.Equals(IdTransactionDeal) && a.IsDeleted.Equals(false)).ToList();
                if (items != null && items.Count > 0)
                {
                    foreach (Entity.TransactionDealUnderlying data in items)
                    {
                        // set update transaction Deal underlying
                        var dataTransactionDealUnderlying = context.TransactionDealUnderlyings.Where(a => a.TransactionDealUnderlyingID.Equals(data.TransactionDealUnderlyingID)).SingleOrDefault();
                        dataTransactionDealUnderlying.IsDeleted = true;
                        dataTransactionDealUnderlying.UpdateDate = DateTime.UtcNow;
                        dataTransactionDealUnderlying.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                        context.SaveChanges();
                    }
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {

                message = ex.Message;
            }
            return isSuccess;
        }
        public bool DeleteTransDealDocuments(long IdTransactionDeal, ref string message)
        {
            bool isSuccess = false;

            try
            {
                List<Entity.TransactionDealDocument> items = context.TransactionDealDocuments.Where(a => a.TransactionDealID.Equals(IdTransactionDeal) && a.IsDeleted.Equals(false)).ToList();
                if (items != null && items.Count > 0)
                {
                    foreach (Entity.TransactionDealDocument data in items)
                    {
                        if (data.PurposeID == 2) // delete document underlying
                        {
                            // set update transaction Deal underlying
                            var dataTransactionDealDocument = context.TransactionDealDocuments.Where(a => a.TransactionDocumentID.Equals(data.TransactionDocumentID)).SingleOrDefault();
                            dataTransactionDealDocument.IsDeleted = true;
                            dataTransactionDealDocument.UpdateDate = DateTime.UtcNow;
                            dataTransactionDealDocument.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                            context.SaveChanges();
                        }
                    }
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {

                message = ex.Message;
            }
            return isSuccess;
        }
        public bool AddTMODealMaker(Guid workflowInstanceID, long approverID, TransactionTMOModel transaction, ref string message)
        {
            bool IsSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    var TMOdata = context.TransactionDeals.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID)).SingleOrDefault();
                    if (TMOdata != null)
                    {
                        TMOdata.IsUnderlyingCompleted = transaction.IsUnderlyingCompleted;
                        TMOdata.ActualSubmissionDateSL = transaction.SubmissionDateSL;
                        TMOdata.ActualSubmissionDateInstruction = transaction.SubmissionDateInstruction;
                        TMOdata.ActualSubmissionDateUnderlying = transaction.SubmissionDateUnderlying;
                        TMOdata.Remarks = transaction.Remarks;
                        TMOdata.UnderlyingCodeID = transaction.UnderlyingCodeID;
                        TMOdata.ActualSubmissionDateUnd = transaction.IsUnderlyingCompleted.HasValue ? (transaction.IsUnderlyingCompleted.Value ? DateTime.Now : default(DateTime?)) : default(DateTime?); ;
                        TMOdata.UpdateDate = DateTime.UtcNow;
                        TMOdata.UpdateBy = currentUser.GetCurrentUser().LoginName;
                        context.SaveChanges();

                        #region Tambah Agung
                        bool isUnderlying = context.TransactionDealUnderlyings.Any(x => x.TransactionDealID.Equals(TMOdata.TransactionDealID));

                        if (isUnderlying)
                        {
                            RoolbackDealCustomerUnderlying(TMOdata.TransactionDealID, ref message);

                            #region Tambah Agung
                            decimal AmountTransaction = transaction.BuyAmount.Value;
                            bool mencukupi = false;
                            foreach (TransDetailUnderlyingModel underlyingItem in transaction.Underlyings)
                            {
                                if (mencukupi != true)
                                {
                                    Entity.CustomerUnderlying customerUnderlying = context.CustomerUnderlyings.Where(a => a.UnderlyingID.Equals(underlyingItem.ID)).SingleOrDefault();
                                    if (customerUnderlying != null)
                                    {
                                        decimal tempAvailableAmount = (decimal)customerUnderlying.AvailableAmountUSD;
                                        customerUnderlying.IsUtilize = true;
                                        customerUnderlying.UpdateDate = DateTime.UtcNow;
                                        customerUnderlying.AvailableAmountUSD = tempAvailableAmount - (AmountTransaction > underlyingItem.USDAmount ? (underlyingItem.USDAmount * customerUnderlying.Rate.Value) / Convert.ToDecimal((customerUnderlying.Amount * customerUnderlying.Rate.Value / customerUnderlying.AmountUSD.Value).ToString("#.##")) : (AmountTransaction * customerUnderlying.Rate.Value) / (customerUnderlying.Amount * customerUnderlying.Rate.Value / customerUnderlying.AmountUSD.Value));
                                        customerUnderlying.UpdateBy = currentUser.GetCurrentUser().LoginName;
                                        context.SaveChanges();

                                        Entity.TransactionDealUnderlying Underlying = new Entity.TransactionDealUnderlying()
                                        {
                                            TransactionDealID = TMOdata.TransactionDealID,
                                            UnderlyingID = underlyingItem.ID,
                                            Amount = AmountTransaction > underlyingItem.USDAmount ? (underlyingItem.USDAmount * customerUnderlying.Rate.Value) / Convert.ToDecimal((customerUnderlying.Amount * customerUnderlying.Rate.Value / customerUnderlying.AmountUSD.Value).ToString("#.##")) : (AmountTransaction * customerUnderlying.Rate.Value) / Convert.ToDecimal((customerUnderlying.Amount * customerUnderlying.Rate.Value / customerUnderlying.AmountUSD.Value).ToString("#.##")),
                                            AvailableAmount = AmountTransaction > underlyingItem.USDAmount ? underlyingItem.USDAmount : AmountTransaction,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().LoginName
                                        };
                                        context.TransactionDealUnderlyings.Add(Underlying);
                                        context.SaveChanges();


                                        if (AmountTransaction > underlyingItem.USDAmount)
                                        {
                                            AmountTransaction = AmountTransaction - underlyingItem.USDAmount;
                                        }
                                        else
                                        {
                                            mencukupi = true;
                                        }
                                    }
                                }
                            }
                            #endregion

                            var existingDocuments = (from x in context.TransactionDealDocuments
                                                     where x.TransactionDealID == TMOdata.TransactionDealID && x.IsDeleted == false
                                                     select x);
                            if (existingDocuments != null && existingDocuments.ToList().Count > 0)
                            {
                                foreach (var item in existingDocuments.ToList())
                                {
                                    context.TransactionDealDocuments.Remove(item);
                                }
                                context.SaveChanges();
                            }

                            if (transaction.Documents.Count > 0)
                            {
                                foreach (var item in transaction.Documents)
                                {
                                    Entity.TransactionDealDocument document = new Entity.TransactionDealDocument()
                                    {
                                        TransactionDealID = TMOdata.TransactionDealID,
                                        DocTypeID = item.Type.ID,
                                        PurposeID = item.Purpose.ID,
                                        Filename = item.FileName,
                                        DocumentPath = item.DocumentPath,
                                        CreateDate = DateTime.UtcNow,
                                        CreateBy = currentUser.GetCurrentUser().LoginName
                                    };

                                    context.TransactionDealDocuments.Add(document);
                                }
                                context.SaveChanges();
                            }
                        }
                        else
                        {
                            long isTransUnderlying = context.Transactions.Where(x => x.TZNumber.Equals(TMOdata.TZRef)).Select(x => x.TransactionID).FirstOrDefault();

                            RoolbackCustomerUnderlyingTMO(isTransUnderlying, ref message);

                            #region Tambah Agung
                            decimal AmountTransaction = transaction.AmountUSD;
                            bool mencukupi = false;
                            foreach (TransDetailUnderlyingModel underlyingItem in transaction.Underlyings)
                            {
                                if (mencukupi != true)
                                {
                                    Entity.CustomerUnderlying customerUnderlying = context.CustomerUnderlyings.Where(a => a.UnderlyingID.Equals(underlyingItem.ID)).SingleOrDefault();
                                    if (customerUnderlying != null)
                                    {
                                        decimal tempAvailableAmount = (decimal)customerUnderlying.AvailableAmountUSD;

                                        customerUnderlying.IsUtilize = true;
                                        customerUnderlying.UpdateDate = DateTime.UtcNow;
                                        customerUnderlying.AvailableAmountUSD = tempAvailableAmount - (AmountTransaction > underlyingItem.USDAmount ? underlyingItem.UtilizeAmountDeal.Value : AmountTransaction);
                                        customerUnderlying.UpdateBy = currentUser.GetCurrentUser().LoginName;
                                        context.SaveChanges();

                                        Entity.TransactionUnderlying Underlying = new Entity.TransactionUnderlying()
                                        {
                                            TransactionID = isTransUnderlying,
                                            UnderlyingID = underlyingItem.ID,
                                            Amount = AmountTransaction > underlyingItem.USDAmount ? underlyingItem.USDAmount : AmountTransaction,
                                            AvailableAmount = 0,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().LoginName
                                        };
                                        context.TransactionUnderlyings.Add(Underlying);
                                        context.SaveChanges();

                                        Entity.TransactionUnderlyingHistory vTransactionUnderlyingHistory = new Entity.TransactionUnderlyingHistory()
                                        {
                                            TransactionUnderlyingID = Underlying.TransactionUnderlyingID,
                                            Amount = AmountTransaction > underlyingItem.USDAmount ? underlyingItem.USDAmount : AmountTransaction,
                                            IsDeleted = false,
                                            CreatedDate = DateTime.UtcNow,
                                            CreatedBy = currentUser.GetCurrentUser().DisplayName
                                        };
                                        context.TransactionUnderlyingHistories.Add(vTransactionUnderlyingHistory);
                                        context.SaveChanges();

                                        if (AmountTransaction > underlyingItem.USDAmount)
                                        {
                                            AmountTransaction = AmountTransaction - underlyingItem.USDAmount;
                                        }
                                        else
                                        {
                                            mencukupi = true;
                                        }
                                    }
                                }
                            }
                            #endregion

                            var existingDocuments = (from x in context.TransactionDocuments
                                                     where x.TransactionID == isTransUnderlying && x.IsDeleted == false
                                                     select x);
                            if (existingDocuments != null && existingDocuments.ToList().Count > 0)
                            {
                                foreach (var item in existingDocuments.ToList())
                                {
                                    context.TransactionDocuments.Remove(item);
                                }
                                context.SaveChanges();
                            }

                            if (transaction.Documents.Count > 0)
                            {
                                foreach (var item in transaction.Documents)
                                {
                                    Entity.TransactionDocument document = new Entity.TransactionDocument()
                                    {
                                        TransactionID = isTransUnderlying,
                                        DocTypeID = item.Type.ID,
                                        PurposeID = item.Purpose.ID,
                                        Filename = item.FileName,
                                        DocumentPath = item.DocumentPath,
                                        CreateDate = DateTime.UtcNow,
                                        CreateBy = currentUser.GetCurrentUser().LoginName
                                    };

                                    context.TransactionDocuments.Add(document);
                                }
                                context.SaveChanges();
                            }
                        }
                        #endregion

                        //if (transaction.Documents != null)
                        //{
                        //    var existingDocuments = (from x in context.TransactionDealDocuments
                        //                             where x.TransactionDealID == TMOdata.TransactionDealID && x.IsDeleted == false
                        //                             select x);
                        //    if (TMOdata.NB != null && TMOdata.SwapType != null)
                        //    {
                        //        var existingSwapDocuments = (from x in context.TransactionDealDocuments
                        //                                     where x.TransactionDealID == transaction.SwapTransactionID.Value && x.IsDeleted == false
                        //                                     select x);
                        //        if (existingSwapDocuments != null && existingSwapDocuments.ToList().Count > 0)
                        //        {
                        //            foreach (var item in existingSwapDocuments.ToList()) // remove All transaction document
                        //            {
                        //                context.TransactionDealDocuments.Remove(item);
                        //            }
                        //            context.SaveChanges();
                        //        }
                        //    }
                        //    if (existingDocuments != null && existingDocuments.ToList().Count > 0)
                        //    {
                        //        foreach (var item in existingDocuments.ToList()) // remove All transaction document
                        //        {
                        //            context.TransactionDealDocuments.Remove(item);
                        //        }
                        //        context.SaveChanges();

                        //        foreach (var item in transaction.Documents) //Add All Transaction document
                        //        {
                        //            //if (item.Purpose.ID != 2)
                        //            // {
                        //            Entity.TransactionDealDocument document = new Entity.TransactionDealDocument()
                        //            {
                        //                TransactionDealID = TMOdata.TransactionDealID,
                        //                DocTypeID = item.Type.ID,
                        //                PurposeID = item.Purpose.ID,
                        //                Filename = item.FileName,
                        //                IsDeleted = false,
                        //                DocumentPath = item.DocumentPath,
                        //                CreateDate = DateTime.UtcNow,
                        //                CreateBy = currentUser.GetCurrentUser().LoginName
                        //            };
                        //            context.TransactionDealDocuments.Add(document);

                        //            if (TMOdata.NB != null && TMOdata.SwapType != null)
                        //            {
                        //                Entity.TransactionDealDocument swapDocument = new Entity.TransactionDealDocument()
                        //                {
                        //                    TransactionDealID = transaction.SwapTransactionID.Value,
                        //                    DocTypeID = item.Type.ID,
                        //                    PurposeID = item.Purpose.ID,
                        //                    Filename = item.FileName,
                        //                    IsDeleted = false,
                        //                    DocumentPath = item.DocumentPath,
                        //                    CreateDate = DateTime.UtcNow,
                        //                    CreateBy = currentUser.GetCurrentUser().DisplayName
                        //                };
                        //                context.TransactionDealDocuments.Add(swapDocument);
                        //            }
                        //            // }
                        //            context.SaveChanges();
                        //        }
                        //    }
                        //    else
                        //    {
                        //        foreach (var item in transaction.Documents)
                        //        {
                        //            //if (item.Purpose.ID != 2) Add All transaction Document
                        //            //{
                        //            Entity.TransactionDealDocument document = new Entity.TransactionDealDocument()
                        //            {
                        //                TransactionDealID = TMOdata.TransactionDealID,
                        //                DocTypeID = item.Type.ID,
                        //                PurposeID = item.Purpose.ID,
                        //                Filename = item.FileName,
                        //                IsDeleted = false,
                        //                DocumentPath = item.DocumentPath,
                        //                CreateDate = DateTime.UtcNow,
                        //                CreateBy = currentUser.GetCurrentUser().LoginName
                        //            };
                        //            context.TransactionDealDocuments.Add(document);
                        //            //}
                        //            if (TMOdata.NB != null && TMOdata.SwapType != null) // Swap Transaction
                        //            {
                        //                Entity.TransactionDealDocument swapDocument = new Entity.TransactionDealDocument()
                        //                {
                        //                    TransactionDealID = transaction.SwapTransactionID.Value,
                        //                    DocTypeID = item.Type.ID,
                        //                    PurposeID = item.Purpose.ID,
                        //                    Filename = item.FileName,
                        //                    IsDeleted = false,
                        //                    DocumentPath = item.DocumentPath,
                        //                    CreateDate = DateTime.UtcNow,
                        //                    CreateBy = currentUser.GetCurrentUser().DisplayName
                        //                };
                        //                context.TransactionDealDocuments.Add(swapDocument);
                        //            }

                        //            context.SaveChanges();
                        //        }
                        //    }
                        //}

                        //if (transaction.Underlyings != null && transaction.Underlyings.Count > 0)
                        //{
                        //    long transactionID = transaction.ID;
                        //    #region Tambah Agung
                        //    decimal AmountTransaction = transaction.BuyAmount.Value;

                        //    foreach (TransDetailUnderlyingModel underlyingItem in transaction.Underlyings)
                        //    {
                        //        Entity.CustomerUnderlying customerUnderlying = context.CustomerUnderlyings.Where(a => a.UnderlyingID.Equals(underlyingItem.ID)).SingleOrDefault();
                        //        if (customerUnderlying != null)
                        //        {
                        //            decimal tempAvailableAmount = (decimal)customerUnderlying.AvailableAmountUSD;
                        //            customerUnderlying.IsUtilize = true;
                        //            customerUnderlying.UpdateDate = DateTime.UtcNow;
                        //            customerUnderlying.AvailableAmountUSD = tempAvailableAmount - (AmountTransaction > underlyingItem.USDAmount ? (underlyingItem.USDAmount * customerUnderlying.Rate.Value) / Convert.ToDecimal((customerUnderlying.Amount * customerUnderlying.Rate.Value / customerUnderlying.AmountUSD.Value).ToString("#.##")) : (AmountTransaction * customerUnderlying.Rate.Value) / (customerUnderlying.Amount * customerUnderlying.Rate.Value / customerUnderlying.AmountUSD.Value));
                        //            customerUnderlying.UpdateBy = currentUser.GetCurrentUser().LoginName;
                        //            context.SaveChanges();

                        //            Entity.TransactionDealUnderlying Underlying = new Entity.TransactionDealUnderlying()
                        //            {
                        //                TransactionDealID = transactionID,
                        //                UnderlyingID = underlyingItem.ID,
                        //                Amount = AmountTransaction > underlyingItem.USDAmount ? (underlyingItem.USDAmount * customerUnderlying.Rate.Value) / Convert.ToDecimal((customerUnderlying.Amount * customerUnderlying.Rate.Value / customerUnderlying.AmountUSD.Value).ToString("#.##")) : (AmountTransaction * customerUnderlying.Rate.Value) / Convert.ToDecimal((customerUnderlying.Amount * customerUnderlying.Rate.Value / customerUnderlying.AmountUSD.Value).ToString("#.##")),
                        //                AvailableAmount = AmountTransaction > underlyingItem.USDAmount ? underlyingItem.USDAmount : AmountTransaction,
                        //                CreateDate = DateTime.UtcNow,
                        //                CreateBy = currentUser.GetCurrentUser().LoginName
                        //            };
                        //            context.TransactionDealUnderlyings.Add(Underlying);
                        //            context.SaveChanges();
                        //        }
                        //        if (AmountTransaction > underlyingItem.USDAmount)
                        //        {
                        //            AmountTransaction = AmountTransaction - underlyingItem.USDAmount;
                        //        }
                        //    }

                        //    #endregion

                        //}
                    }

                    ts.Complete();

                    IsSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }

            return IsSuccess;
        }

        public bool AddTMODealSubmitMaker(Guid workflowInstanceID, long approverID, TransactionTMOModel transaction, ref string message)
        {
            bool IsSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    var TMOdata = context.TransactionDeals.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID)).SingleOrDefault();
                    if (TMOdata != null)
                    {
                        TMOdata.IsUnderlyingCompleted = transaction.IsUnderlyingCompleted;
                        TMOdata.ActualSubmissionDateSL = transaction.SubmissionDateSL;
                        TMOdata.ActualSubmissionDateInstruction = transaction.SubmissionDateInstruction;
                        TMOdata.ActualSubmissionDateUnderlying = transaction.SubmissionDateUnderlying;
                        TMOdata.Remarks = transaction.Remarks;
                        TMOdata.ActualSubmissionDateUnd = transaction.IsUnderlyingCompleted.HasValue ? (transaction.IsUnderlyingCompleted.Value ? DateTime.Now : default(DateTime?)) : default(DateTime?); ;
                        TMOdata.UpdateDate = DateTime.UtcNow;
                        TMOdata.UpdateBy = currentUser.GetCurrentUser().LoginName;
                        context.SaveChanges();

                        if (transaction.Documents != null)
                        {
                            var existingDocuments = (from x in context.TransactionDealDocuments
                                                     where x.TransactionDealID == TMOdata.TransactionDealID && x.IsDeleted == false
                                                     select x);
                            if (TMOdata.NB != null && TMOdata.SwapType != null)
                            {
                                var existingSwapDocuments = (from x in context.TransactionDealDocuments
                                                             where x.TransactionDealID == transaction.SwapTransactionID.Value && x.IsDeleted == false
                                                             select x);
                                if (existingSwapDocuments != null && existingSwapDocuments.ToList().Count > 0)
                                {
                                    foreach (var item in existingSwapDocuments.ToList()) // remove All transaction document
                                    {
                                        context.TransactionDealDocuments.Remove(item);
                                    }
                                    context.SaveChanges();
                                }
                            }
                            if (existingDocuments != null && existingDocuments.ToList().Count > 0)
                            {
                                foreach (var item in existingDocuments.ToList())
                                {
                                    context.TransactionDealDocuments.Remove(item);
                                }
                                context.SaveChanges();

                                foreach (var item in transaction.Documents)
                                {
                                    Entity.TransactionDealDocument document = new Entity.TransactionDealDocument()
                                    {
                                        TransactionDealID = TMOdata.TransactionDealID,
                                        DocTypeID = item.Type.ID,
                                        PurposeID = item.Purpose.ID,
                                        Filename = item.FileName,
                                        IsDeleted = false,
                                        DocumentPath = item.DocumentPath,
                                        CreateDate = DateTime.UtcNow,
                                        CreateBy = currentUser.GetCurrentUser().LoginName
                                    };
                                    context.TransactionDealDocuments.Add(document);

                                    if (TMOdata.NB != null && TMOdata.SwapType != null)
                                    {
                                        Entity.TransactionDealDocument swapDocument = new Entity.TransactionDealDocument()
                                        {
                                            TransactionDealID = transaction.SwapTransactionID.Value,
                                            DocTypeID = item.Type.ID,
                                            PurposeID = item.Purpose.ID,
                                            Filename = item.FileName,
                                            IsDeleted = false,
                                            DocumentPath = item.DocumentPath,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().DisplayName
                                        };
                                        context.TransactionDealDocuments.Add(swapDocument);
                                    }
                                    context.SaveChanges();
                                }
                            }
                            else
                            {
                                foreach (var item in transaction.Documents)
                                {
                                    Entity.TransactionDealDocument document = new Entity.TransactionDealDocument()
                                    {
                                        TransactionDealID = TMOdata.TransactionDealID,
                                        DocTypeID = item.Type.ID,
                                        PurposeID = item.Purpose.ID,
                                        Filename = item.FileName,
                                        IsDeleted = false,
                                        DocumentPath = item.DocumentPath,
                                        CreateDate = DateTime.UtcNow,
                                        CreateBy = currentUser.GetCurrentUser().LoginName
                                    };
                                    context.TransactionDealDocuments.Add(document);

                                    if (TMOdata.NB != null && TMOdata.SwapType != null)
                                    {
                                        Entity.TransactionDealDocument swapDocument = new Entity.TransactionDealDocument()
                                        {
                                            TransactionDealID = transaction.SwapTransactionID.Value,
                                            DocTypeID = item.Type.ID,
                                            PurposeID = item.Purpose.ID,
                                            Filename = item.FileName,
                                            IsDeleted = false,
                                            DocumentPath = item.DocumentPath,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().DisplayName
                                        };
                                        context.TransactionDealDocuments.Add(swapDocument);
                                    }

                                    context.SaveChanges();
                                }
                            }
                        }

                    }

                    ts.Complete();

                    IsSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }

            return IsSuccess;
        }

        public bool AddTMODealReopenMaker(Guid workflowInstanceID, long approverID, TransactionTMOModel transaction, ref string message)
        {
            bool IsSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    Entity.TransactionDeal TMOdata = context.TransactionDeals.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID)).SingleOrDefault();
                    if (TMOdata != null)
                    {
                        TMOdata.UnderlyingCodeID = transaction.UnderlyingCodeID;
                        TMOdata.UpdateDate = DateTime.UtcNow;
                        TMOdata.UpdateBy = currentUser.GetCurrentUser().LoginName;
                        context.SaveChanges();


                        #region Tambah Agung
                        bool isUnderlying = context.TransactionDealUnderlyings.Any(x => x.TransactionDealID.Equals(TMOdata.TransactionDealID));

                        if (isUnderlying)
                        {
                            RoolbackDealCustomerUnderlying(TMOdata.TransactionDealID, ref message);

                            #region Tambah Agung
                            decimal AmountTransaction = transaction.BuyAmount.Value;
                            bool mencukupi = false;
                            foreach (TransDetailUnderlyingModel underlyingItem in transaction.Underlyings)
                            {
                                if (mencukupi != true)
                                {
                                    Entity.CustomerUnderlying customerUnderlying = context.CustomerUnderlyings.Where(a => a.UnderlyingID.Equals(underlyingItem.ID)).SingleOrDefault();
                                    if (customerUnderlying != null)
                                    {
                                        decimal tempAvailableAmount = (decimal)customerUnderlying.AvailableAmountUSD;
                                        customerUnderlying.IsUtilize = true;
                                        customerUnderlying.UpdateDate = DateTime.UtcNow;
                                        customerUnderlying.AvailableAmountUSD = tempAvailableAmount - (AmountTransaction > underlyingItem.USDAmount ? (underlyingItem.USDAmount * customerUnderlying.Rate.Value) / Convert.ToDecimal((customerUnderlying.Amount * customerUnderlying.Rate.Value / customerUnderlying.AmountUSD.Value).ToString("#.##")) : (AmountTransaction * customerUnderlying.Rate.Value) / (customerUnderlying.Amount * customerUnderlying.Rate.Value / customerUnderlying.AmountUSD.Value));
                                        customerUnderlying.UpdateBy = currentUser.GetCurrentUser().LoginName;
                                        context.SaveChanges();

                                        Entity.TransactionDealUnderlying Underlying = new Entity.TransactionDealUnderlying()
                                        {
                                            TransactionDealID = TMOdata.TransactionDealID,
                                            UnderlyingID = underlyingItem.ID,
                                            Amount = AmountTransaction > underlyingItem.USDAmount ? (underlyingItem.USDAmount * customerUnderlying.Rate.Value) / Convert.ToDecimal((customerUnderlying.Amount * customerUnderlying.Rate.Value / customerUnderlying.AmountUSD.Value).ToString("#.##")) : (AmountTransaction * customerUnderlying.Rate.Value) / Convert.ToDecimal((customerUnderlying.Amount * customerUnderlying.Rate.Value / customerUnderlying.AmountUSD.Value).ToString("#.##")),
                                            AvailableAmount = AmountTransaction > underlyingItem.USDAmount ? underlyingItem.USDAmount : AmountTransaction,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().LoginName
                                        };
                                        context.TransactionDealUnderlyings.Add(Underlying);
                                        context.SaveChanges();


                                        if (AmountTransaction > underlyingItem.USDAmount)
                                        {
                                            AmountTransaction = AmountTransaction - underlyingItem.USDAmount;
                                        }
                                        else
                                        {
                                            mencukupi = true;
                                        }
                                    }
                                }
                            }
                            #endregion

                            var existingDocuments = (from x in context.TransactionDealDocuments
                                                     where x.TransactionDealID == TMOdata.TransactionDealID && x.IsDeleted == false
                                                     select x);
                            if (existingDocuments != null && existingDocuments.ToList().Count > 0)
                            {
                                foreach (var item in existingDocuments.ToList())
                                {
                                    context.TransactionDealDocuments.Remove(item);
                                }
                                context.SaveChanges();
                            }

                            if (transaction.Documents.Count > 0)
                            {
                                foreach (var item in transaction.Documents)
                                {
                                    Entity.TransactionDealDocument document = new Entity.TransactionDealDocument()
                                    {
                                        TransactionDealID = TMOdata.TransactionDealID,
                                        DocTypeID = item.Type.ID,
                                        PurposeID = item.Purpose.ID,
                                        Filename = item.FileName,
                                        DocumentPath = item.DocumentPath,
                                        CreateDate = DateTime.UtcNow,
                                        CreateBy = currentUser.GetCurrentUser().LoginName
                                    };

                                    context.TransactionDealDocuments.Add(document);
                                }
                                context.SaveChanges();
                            }
                        }
                        else
                        {
                            long isTransUnderlying = context.Transactions.Where(x => x.TZNumber.Equals(TMOdata.TZRef)).Select(x => x.TransactionID).FirstOrDefault();

                            RoolbackCustomerUnderlyingTMO(isTransUnderlying, ref message);

                            #region Tambah Agung
                            decimal AmountTransaction = transaction.AmountUSD;
                            bool mencukupi = false;
                            foreach (TransDetailUnderlyingModel underlyingItem in transaction.Underlyings)
                            {
                                if (mencukupi != true)
                                {
                                    Entity.CustomerUnderlying customerUnderlying = context.CustomerUnderlyings.Where(a => a.UnderlyingID.Equals(underlyingItem.ID)).SingleOrDefault();
                                    if (customerUnderlying != null)
                                    {
                                        decimal tempAvailableAmount = (decimal)customerUnderlying.AvailableAmountUSD;

                                        customerUnderlying.IsUtilize = true;
                                        customerUnderlying.UpdateDate = DateTime.UtcNow;
                                        customerUnderlying.AvailableAmountUSD = tempAvailableAmount - (AmountTransaction > underlyingItem.USDAmount ? underlyingItem.UtilizeAmountDeal.Value : AmountTransaction);
                                        customerUnderlying.UpdateBy = currentUser.GetCurrentUser().LoginName;
                                        context.SaveChanges();

                                        Entity.TransactionUnderlying Underlying = new Entity.TransactionUnderlying()
                                        {
                                            TransactionID = isTransUnderlying,
                                            UnderlyingID = underlyingItem.ID,
                                            Amount = AmountTransaction > underlyingItem.USDAmount ? underlyingItem.USDAmount : AmountTransaction,
                                            AvailableAmount = 0,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().LoginName
                                        };
                                        context.TransactionUnderlyings.Add(Underlying);
                                        context.SaveChanges();

                                        Entity.TransactionUnderlyingHistory vTransactionUnderlyingHistory = new Entity.TransactionUnderlyingHistory()
                                        {
                                            TransactionUnderlyingID = Underlying.TransactionUnderlyingID,
                                            Amount = AmountTransaction > underlyingItem.USDAmount ? underlyingItem.USDAmount : AmountTransaction,
                                            IsDeleted = false,
                                            CreatedDate = DateTime.UtcNow,
                                            CreatedBy = currentUser.GetCurrentUser().DisplayName
                                        };
                                        context.TransactionUnderlyingHistories.Add(vTransactionUnderlyingHistory);
                                        context.SaveChanges();

                                        if (AmountTransaction > underlyingItem.USDAmount)
                                        {
                                            AmountTransaction = AmountTransaction - underlyingItem.USDAmount;
                                        }
                                        else
                                        {
                                            mencukupi = true;
                                        }
                                    }
                                }
                            }
                            #endregion

                            var existingDocuments = (from x in context.TransactionDocuments
                                                     where x.TransactionID == isTransUnderlying && x.IsDeleted == false
                                                     select x);
                            if (existingDocuments != null && existingDocuments.ToList().Count > 0)
                            {
                                foreach (var item in existingDocuments.ToList())
                                {
                                    context.TransactionDocuments.Remove(item);
                                }
                                context.SaveChanges();
                            }

                            if (transaction.Documents.Count > 0)
                            {
                                foreach (var item in transaction.Documents)
                                {
                                    Entity.TransactionDocument document = new Entity.TransactionDocument()
                                    {
                                        TransactionID = isTransUnderlying,
                                        DocTypeID = item.Type.ID,
                                        PurposeID = item.Purpose.ID,
                                        Filename = item.FileName,
                                        DocumentPath = item.DocumentPath,
                                        CreateDate = DateTime.UtcNow,
                                        CreateBy = currentUser.GetCurrentUser().LoginName
                                    };

                                    context.TransactionDocuments.Add(document);
                                }
                                context.SaveChanges();
                            }
                        }
                        #endregion


                        //if (transaction.Documents != null)
                        //{
                        //    var existingDocuments = (from x in context.TransactionDealDocuments
                        //                             where x.TransactionDealID == TMOdata.TransactionDealID && x.IsDeleted == false
                        //                             select x);
                        //    if (TMOdata.NB != null && TMOdata.SwapType != null)
                        //    {
                        //        var existingSwapDocuments = (from x in context.TransactionDealDocuments
                        //                                     where x.TransactionDealID == transaction.SwapTransactionID.Value && x.IsDeleted == false
                        //                                     select x);
                        //        if (existingSwapDocuments != null && existingSwapDocuments.ToList().Count > 0)
                        //        {
                        //            foreach (var item in existingSwapDocuments.ToList()) // remove All transaction document
                        //            {
                        //                context.TransactionDealDocuments.Remove(item);
                        //            }
                        //            context.SaveChanges();
                        //        }
                        //    }
                        //    if (existingDocuments != null && existingDocuments.ToList().Count > 0)
                        //    {
                        //        foreach (var item in existingDocuments.ToList())
                        //        {
                        //            context.TransactionDealDocuments.Remove(item);
                        //        }
                        //        context.SaveChanges();

                        //        foreach (var item in transaction.Documents)
                        //        {
                        //            Entity.TransactionDealDocument document = new Entity.TransactionDealDocument()
                        //            {
                        //                TransactionDealID = TMOdata.TransactionDealID,
                        //                DocTypeID = item.Type.ID,
                        //                PurposeID = item.Purpose.ID,
                        //                Filename = item.FileName,
                        //                IsDeleted = false,
                        //                DocumentPath = item.DocumentPath,
                        //                CreateDate = DateTime.UtcNow,
                        //                CreateBy = currentUser.GetCurrentUser().LoginName
                        //            };
                        //            context.TransactionDealDocuments.Add(document);

                        //            if (TMOdata.NB != null && TMOdata.SwapType != null)
                        //            {
                        //                Entity.TransactionDealDocument swapDocument = new Entity.TransactionDealDocument()
                        //                {
                        //                    TransactionDealID = transaction.SwapTransactionID.Value,
                        //                    DocTypeID = item.Type.ID,
                        //                    PurposeID = item.Purpose.ID,
                        //                    Filename = item.FileName,
                        //                    IsDeleted = false,
                        //                    DocumentPath = item.DocumentPath,
                        //                    CreateDate = DateTime.UtcNow,
                        //                    CreateBy = currentUser.GetCurrentUser().DisplayName
                        //                };
                        //                context.TransactionDealDocuments.Add(swapDocument);
                        //            }
                        //            context.SaveChanges();
                        //        }
                        //    }
                        //    else
                        //    {
                        //        foreach (var item in transaction.Documents)
                        //        {
                        //            Entity.TransactionDealDocument document = new Entity.TransactionDealDocument()
                        //            {
                        //                TransactionDealID = TMOdata.TransactionDealID,
                        //                DocTypeID = item.Type.ID,
                        //                PurposeID = item.Purpose.ID,
                        //                Filename = item.FileName,
                        //                IsDeleted = false,
                        //                DocumentPath = item.DocumentPath,
                        //                CreateDate = DateTime.UtcNow,
                        //                CreateBy = currentUser.GetCurrentUser().LoginName
                        //            };
                        //            context.TransactionDealDocuments.Add(document);

                        //            if (TMOdata.NB != null && TMOdata.SwapType != null)
                        //            {
                        //                Entity.TransactionDealDocument swapDocument = new Entity.TransactionDealDocument()
                        //                {
                        //                    TransactionDealID = transaction.SwapTransactionID.Value,
                        //                    DocTypeID = item.Type.ID,
                        //                    PurposeID = item.Purpose.ID,
                        //                    Filename = item.FileName,
                        //                    IsDeleted = false,
                        //                    DocumentPath = item.DocumentPath,
                        //                    CreateDate = DateTime.UtcNow,
                        //                    CreateBy = currentUser.GetCurrentUser().DisplayName
                        //                };
                        //                context.TransactionDealDocuments.Add(swapDocument);
                        //            }

                        //            context.SaveChanges();
                        //        }
                        //    }
                        //}

                    }
                    ts.Complete();

                    IsSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }

            return IsSuccess;
        }

        public bool GetTrackingTMOMakerDetails(Guid workflowInstanceID, ref TransactionTMOModel data, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                // context.Transactions.AsNoTracking();
                data = (from a in context.TransactionDeals
                        where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                        select new TransactionTMOModel
                        {
                            ID = a.TransactionDealID,
                            ApplicationID = a.ApplicationID,
                            CIF = a.CIF,
                            TradeDate = a.TradeDate,
                            ValueDate = a.ValueDate,
                            TZReference = a.TZRef,
                            OtherUnderlying = a.OtherUnderlying,
                            Rate = a.Rate,
                            StatementLetter = new StatementLetterModel()
                            {
                                ID = a.StatementLetter.StatementLetterID,
                                Name = a.StatementLetter.StatementLetterName
                            },
                            ProductType = new ProductTypeModel()
                            {
                                ID = a.ProductType.ProductTypeID,
                                Code = a.ProductType.ProductTypeCode,
                                Description = a.ProductType.ProductTypeDesc,
                                IsFlowValas = a.ProductType.IsFlowValas
                            },
                            BuyAmount = a.BuyAmount,
                            SellAmount = a.SellAmount,
                            AmountUSD = a.AmountUSD,
                            TransactionStatus = a.TransactionStatus,
                            IsUnderlyingCompleted = a.IsUnderlyingCompleted,
                            SubmissionDateSL = a.ActualSubmissionDateSL,
                            SubmissionDateInstruction = a.ActualSubmissionDateInstruction,
                            SubmissionDateUnderlying = a.ActualSubmissionDateUnderlying,
                            UnderlyingCodeID = a.UnderlyingCodeID,
                            AccountNumber = a.AccountNumber,
                            IsResident = a.IsResident.HasValue ? a.IsResident.Value : false,
                            IsOtherAccountNumber = a.IsOtherAccountNumber != null ? a.IsOtherAccountNumber : false,
                            IsJointAccount = a.IsJointAccount.HasValue ? a.IsJointAccount.Value : false,
                            Remarks = a.Remarks,
                            NB = a.NB,
                            SwapType = a.SwapType,
                            SwapTZDealNo = a.SwapTZDealNo,
                            IsNettingTransaction = a.IsNettingTransaction.HasValue ? a.IsNettingTransaction : false,
                            Documents = a.TransactionDealDocuments.Where(x => x.IsDeleted.Equals(false)).Select(x => new TransactionDealDocumentModel()
                            {
                                ID = x.TransactionDocumentID,
                                Type = new DocumentTypeModel()
                                {
                                    ID = x.DocumentType.DocTypeID,
                                    Name = x.DocumentType.DocTypeName,
                                    Description = x.DocumentType.DocTypeDescription,
                                    LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                    LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                },
                                Purpose = new DocumentPurposeModel()
                                {
                                    ID = x.DocumentPurpose.PurposeID,
                                    Name = x.DocumentPurpose.PurposeName,
                                    Description = x.DocumentPurpose.PurposeDescription,
                                    LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                    LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                },
                                FileName = x.Filename,
                                DocumentPath = x.DocumentPath,
                                LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                            }).ToList(),
                            ReviseDealDocuments = (from cu in a.Customer.CustomerUnderlyings
                                                   join cm in context.CustomerUnderlyingMappings on cu.UnderlyingID equals cm.UnderlyingID
                                                   join cf in context.CustomerUnderlyingFiles on cm.UnderlyingFileID equals cf.UnderlyingFileID
                                                   join trans in context.TransactionDealUnderlyings.Where(x => x.TransactionDealID == a.TransactionDealID && x.IsDeleted == false) on cu.UnderlyingID equals trans.UnderlyingID into grp
                                                   from subTrans in grp.DefaultIfEmpty()
                                                   where cu.IsDeleted.Equals(false) && cm.IsDeleted.Equals(false) && cf.IsDeleted.Equals(false) && (DbFunctions.TruncateTime(cu.ExpiredDate) >= DbFunctions.TruncateTime(DateTime.Now) || subTrans != null)
                                                   select new ReviseDealDocumentModel()
                                                   {
                                                       UnderlyingID = cu.UnderlyingID,
                                                       ID = cf.UnderlyingFileID,
                                                       Type = new DocumentTypeModel()
                                                       {
                                                           ID = cf.DocumentType.DocTypeID,
                                                           Name = cf.DocumentType.DocTypeName,
                                                           Description = cf.DocumentType.DocTypeDescription,
                                                           LastModifiedBy = cf.DocumentType.UpdateDate == null ? cf.DocumentType.CreateBy : cf.DocumentType.UpdateBy,
                                                           LastModifiedDate = cf.DocumentType.UpdateDate == null ? cf.DocumentType.CreateDate : cf.DocumentType.UpdateDate.Value
                                                       },
                                                       Purpose = new DocumentPurposeModel()
                                                       {
                                                           ID = cf.DocumentPurpose.PurposeID,
                                                           Name = cf.DocumentPurpose.PurposeName,
                                                           Description = cf.DocumentPurpose.PurposeDescription,
                                                           LastModifiedBy = cf.DocumentPurpose.UpdateDate == null ? cf.DocumentPurpose.CreateBy : cf.DocumentPurpose.UpdateBy,
                                                           LastModifiedDate = cf.DocumentPurpose.UpdateDate == null ? cf.DocumentPurpose.CreateDate : cf.DocumentPurpose.UpdateDate.Value
                                                       },
                                                       FileName = cf.FileName,
                                                       DocumentPath = cf.DocumentPath,
                                                       IsDormant = false,//cf.IsDormant.Value,
                                                       LastModifiedBy = cf.UpdateDate == null ? cf.CreateBy : cf.UpdateBy,
                                                       LastModifiedDate = cf.UpdateDate == null ? cf.CreateDate : cf.UpdateDate.Value
                                                   }).Concat(from p in a.TransactionDealDocuments
                                                             where p.PurposeID != 2 && p.IsDeleted.Equals(false)
                                                             select new ReviseDealDocumentModel()
                                                             {
                                                                 UnderlyingID = 0,
                                                                 ID = p.TransactionDocumentID,
                                                                 Type = new DocumentTypeModel()
                                                                 {
                                                                     ID = p.DocumentType.DocTypeID,
                                                                     Name = p.DocumentType.DocTypeName,
                                                                     Description = p.DocumentType.DocTypeDescription,
                                                                     LastModifiedBy = p.DocumentType.UpdateDate == null ? p.DocumentType.CreateBy : p.DocumentType.UpdateBy,
                                                                     LastModifiedDate = p.DocumentType.UpdateDate == null ? p.DocumentType.CreateDate : p.DocumentType.UpdateDate.Value
                                                                 },
                                                                 Purpose = new DocumentPurposeModel()
                                                                 {
                                                                     ID = p.DocumentPurpose.PurposeID,
                                                                     Name = p.DocumentPurpose.PurposeName,
                                                                     Description = p.DocumentPurpose.PurposeDescription,
                                                                     LastModifiedBy = p.DocumentPurpose.UpdateDate == null ? p.DocumentPurpose.CreateBy : p.DocumentPurpose.UpdateBy,
                                                                     LastModifiedDate = p.DocumentPurpose.UpdateDate == null ? p.DocumentPurpose.CreateDate : p.DocumentPurpose.UpdateDate.Value
                                                                 },
                                                                 FileName = p.Filename,
                                                                 DocumentPath = p.DocumentPath,
                                                                 IsDormant = false,//p.IsDormant.Value,
                                                                 LastModifiedBy = p.UpdateDate == null ? p.CreateBy : p.UpdateBy,
                                                                 LastModifiedDate = p.UpdateDate == null ? p.CreateDate : p.UpdateDate.Value
                                                             }).ToList(),
                            CreateDate = a.CreatedDate,
                            LastModifiedBy = a.UpdateDate == null ? a.CreatedBy : a.UpdateBy,
                            LastModifiedDate = a.UpdateDate == null ? a.CreatedDate : a.UpdateDate.Value,
                        }).SingleOrDefault();

                if (data != null)
                {
                    var deal = context.TransactionDeals.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID)).Select(a => a).FirstOrDefault();
                    var TransactionDealID = deal.TransactionDealID;
                    var cif = deal.CIF;
                    string accountNumber = deal.AccountNumber;
                    ICustomerRepository customerRepo = new CustomerRepository();
                    CustomerModel customer = new CustomerModel();
                    if (deal.TransactionStatus != null) // view transaction, hanya menampilkan transaction underlying
                    {
                        bool isUnderlying = context.TransactionDealUnderlyings.Any(x => x.TransactionDealID.Equals(TransactionDealID));

                        if (isUnderlying)
                        {
                            if (customerRepo.GetCustomerByTransactionDeal(TransactionDealID, ref customer, ref message))
                            {
                                data.Customer = customer;
                            }
                        }
                        else
                        {
                            long isTransUnderlying = context.Transactions.Where(x => x.TZNumber.Equals(deal.TZRef)).Select(x => x.TransactionID).FirstOrDefault();

                            if (isTransUnderlying > 0)
                            {
                                if (customerRepo.GetCustomerByTransactionDealChecker(0, isTransUnderlying, ref customer, ref message))
                                {
                                    data.Customer = customer;
                                }
                                data.Documents = (from p in context.TransactionDocuments
                                                  where p.IsDeleted.Equals(false) && p.TransactionID.Equals(isTransUnderlying)
                                                  select new TransactionDealDocumentModel()
                                                  {
                                                      ID = p.TransactionDocumentID,
                                                      Type = new DocumentTypeModel()
                                                      {
                                                          ID = p.DocumentType.DocTypeID,
                                                          Name = p.DocumentType.DocTypeName,
                                                          Description = p.DocumentType.DocTypeDescription,
                                                          LastModifiedBy = p.DocumentType.UpdateDate == null ? p.DocumentType.CreateBy : p.DocumentType.UpdateBy,
                                                          LastModifiedDate = p.DocumentType.UpdateDate == null ? p.DocumentType.CreateDate : p.DocumentType.UpdateDate.Value
                                                      },
                                                      Purpose = new DocumentPurposeModel()
                                                      {
                                                          ID = p.DocumentPurpose.PurposeID,
                                                          Name = p.DocumentPurpose.PurposeName,
                                                          Description = p.DocumentPurpose.PurposeDescription,
                                                          LastModifiedBy = p.DocumentPurpose.UpdateDate == null ? p.DocumentPurpose.CreateBy : p.DocumentPurpose.UpdateBy,
                                                          LastModifiedDate = p.DocumentPurpose.UpdateDate == null ? p.DocumentPurpose.CreateDate : p.DocumentPurpose.UpdateDate.Value
                                                      },
                                                      FileName = p.Filename,
                                                      DocumentPath = p.DocumentPath,
                                                      LastModifiedBy = p.UpdateDate == null ? p.CreateBy : p.UpdateBy,
                                                      LastModifiedDate = p.UpdateDate == null ? p.CreateDate : p.UpdateDate.Value
                                                  }).ToList();
                            }
                        }
                    }
                    else
                    {   // Menampilkan transaction underlying dan Customer Underlying
                        if (customerRepo.GetCustomerByTransactionReviseDealMaker(TransactionDealID, ref customer, ref message))
                        {
                            data.Customer = customer;
                        }
                    }

                    if (deal.IsOtherAccountNumber != null && deal.IsOtherAccountNumber == true)
                    {
                        CustomerAccountModel Account = new CustomerAccountModel();
                        data.Account = Account;
                        data.OtherAccountNumber = deal.OtherAccountNumber;
                    }
                    else
                    {
                        data.Account = (from j in context.SP_GETJointAccount(cif)
                                        where j.AccountNumber.Equals(accountNumber)
                                        select new CustomerAccountModel
                                        {
                                            AccountNumber = j.AccountNumber,
                                            Currency = new CurrencyModel
                                            {
                                                ID = j.CurrencyID.Value,
                                                Code = j.CurrencyCode,
                                                Description = j.CurrencyDescription
                                            },
                                            CustomerName = j.CustomerName,
                                            IsJointAccount = j.IsJointAccount
                                        }).FirstOrDefault();
                        data.AccountNumber = deal.AccountNumber;

                    }

                    CurrencyModel AccountCurrency = new CurrencyModel();
                    AccountCurrency.ID = deal.DebitCurrency.CurrencyID;
                    AccountCurrency.Code = deal.DebitCurrency.CurrencyCode;
                    AccountCurrency.Description = deal.DebitCurrency.CurrencyDescription;

                    data.DebitCurrency = AccountCurrency;
                    data.AccountCurrencyCode = AccountCurrency.Code;


                    if (deal.CurrencySell != null)
                    {
                        data.SellCurrency = new CurrencyModel()
                        {
                            ID = deal.CurrencySell.CurrencyID,
                            Code = deal.CurrencySell.CurrencyCode,
                            CodeDescription = deal.CurrencySell.CurrencyDescription,
                            Description = deal.CurrencySell.CurrencyDescription,
                            LastModifiedBy = deal.CurrencySell.UpdateDate == null ? deal.CurrencySell.CreateBy : deal.CurrencySell.UpdateBy,
                            LastModifiedDate = deal.CurrencySell.UpdateDate == null ? deal.CurrencySell.CreateDate : deal.CurrencySell.UpdateDate.Value
                        };
                    }

                    if (deal.CurrencyBuy != null)
                    {
                        data.BuyCurrency = new CurrencyModel()
                        {
                            ID = deal.CurrencyBuy.CurrencyID,
                            Code = deal.CurrencyBuy.CurrencyCode,
                            CodeDescription = deal.CurrencyBuy.CurrencyDescription,
                            Description = deal.CurrencyBuy.CurrencyDescription,
                            LastModifiedBy = deal.CurrencyBuy.UpdateDate == null ? deal.CurrencyBuy.CreateBy : deal.CurrencyBuy.UpdateBy,
                            LastModifiedDate = deal.CurrencyBuy.UpdateDate == null ? deal.CurrencyBuy.CreateDate : deal.CurrencyBuy.UpdateDate.Value
                        };
                    }

                    customerRepo.Dispose();
                    customer = null;

                }
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }

        #region Dani
        public bool IsCSO(string userName, ref bool IsCSO, ref string message)
        {
            bool IsSuccess = false;
            IsCSO = false;
            string newUsername = "i:0#.f|dbsmembership|" + userName.Trim();
            try
            {
                IsCSO = IsCSOUser(newUsername);
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                IsCSO = false;
                message = ex.Message;
            }
            return IsSuccess;
        }
        public bool IsCSOUser(string Username)
        {
            bool ret = false;
            RoleRepository Rolerepo = new RoleRepository();
            IList<RoleModel> RoleList = new List<RoleModel>();
            string sMess = string.Empty;
            try
            {
                Rolerepo.GetRoleByUsername(Username, ref  RoleList, ref sMess);
                if (RoleList != null)
                {
                    if (RoleList.Count > 0)
                    {
                        foreach (var rl in RoleList)
                        {
                            if (rl.Name != null)
                            {
                                if (rl.Name.ToUpper().Contains("CSO"))
                                {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                ret = false;
            }

            return ret;
        }
        #endregion

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}

//=======================================================================================//
///FOR ALL DEVELOPER...
///PLEASE ADD NEW REGION BY YOUR NAME TO KEEP CLEAN !!! DONT BE A DIRTY PROGRAMMER!!!
///ANDIWIJAYA
//=======================================================================================//