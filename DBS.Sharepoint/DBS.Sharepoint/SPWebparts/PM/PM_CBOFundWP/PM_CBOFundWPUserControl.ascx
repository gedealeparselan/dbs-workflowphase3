﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PM_CBOFundWPUserControl.ascx.cs" Inherits="DBS.Sharepoint.SPWebparts.PM.PM_CBOFundWP.PM_CBOFundWPUserControl" %>

<h1 class="header smaller lighter dark">CBO Fund</h1>

<!-- widget box start -->
<div id="widget-box" class="widget-box">
	<!-- widget header start -->
	<div class="widget-header widget-hea1der-small header-color-dark">
		<h6>CBO Fund Table</h6>

		<div class="widget-toolbar">
			<label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
				<i class="blue icon-filter"></i>
				<input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: GridProperties().AllowFilter" />
				<span class="lbl"></span>
			</label>
			<a href="#" data-bind="click: GetData" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
				<i class="blue icon-refresh"></i>
			</a>
		</div>
	</div>
	<!-- widget header end -->

	<!-- widget body start -->
	<div class="widget-body">
		<!-- widget main start -->
		<div class="widget-main padding-0">
			<!-- widget slim control start -->
			<div class="slim-scroll" data-height="400">
				<!-- widget content start -->
				<div class="content">

					<!-- table responsive start -->
					<div class="table-responsive">
						<div class="dataTables_wrapper" role="grid">
							<table id="CBOFund-table" class="table table-striped table-bordered table-hover dataTable">
                                <thead>
                                    <tr data-bind="with: GridProperties">
                                        <th style="width:50px">No.</th>
                                        <th data-bind="click: function () { Sorting('FundCode'); }, css: GetSortedColumn('FundCode')">Fund Code</th>
                                        <th data-bind="click: function () { Sorting('FundName'); }, css: GetSortedColumn('FundName')">Fund Name</th>
                                        <th data-bind="click: function () { Sorting('OffOnShore'); }, css: GetSortedColumn('OffOnShore')">Offshore/Onshore</th>
                                        <th data-bind="click: function () { Sorting('AccountNo'); }, css: GetSortedColumn('AccountNo')">Account No</th>
                                        <th data-bind="click: function () { Sorting('LastModifiedBy'); }, css: GetSortedColumn('LastModifiedBy')">Modified By</th>
                                        <th data-bind="click: function () { Sorting('LastModifiedDate'); }, css: GetSortedColumn('LastModifiedDate')">Modified Date</th>
                                    </tr>
                                </thead>
                                <thead data-bind="visible: GridProperties().AllowFilter" class="table-filter">
                                    <tr>
                                        <th class="clear-filter">
                                            <a href="#" data-bind="click: ClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                <i class="green icon-trash"></i>
                                            </a>
                                        </th>
                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterFundCode, event: { change: GridProperties().Filter }" /></th>
                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterFundName, event: { change: GridProperties().Filter }" /></th>
                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterOffOnshore, event: { change: GridProperties().Filter }" /></th>
                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterAccountNo, event: { change: GridProperties().Filter }" /></th>
                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterModifiedBy, event: { change: GridProperties().Filter }" /></th>
                                        <th><input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.FilterModifiedDate, event: { change: GridProperties().Filter }"/></th>
                                    </tr>
                                </thead>
                                <tbody data-bind="foreach: Funds, visible:Funds().length > 0">
                                    <tr data-bind="click: $root.GetSelectedRow" data-toggle="modal">
                                        <td><span data-bind="text: RowID"></span></td>
                                        <td><span data-bind="text: FundCode"></span></td>
                                        <td><span data-bind="text: FundName"></span></td>     
                                        <td><span data-bind="text: OffOnshore.Description"></span></td>
                                        <td><span data-bind="text: AccountNo"></span></td>
                                        <td><span data-bind="text: LastModifiedBy"></span></td>
                                   	    <td><span data-bind="text: $root.LocalDate(LastModifiedDate)"></span></td>              
                                    </tr>
                                </tbody>
                                <tbody data-bind="visible: Funds().length == 0">
                                    <tr>
                                        <td colspan="7" class="text-center">
                                            No entries available.
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
						</div>
					</div>
					<!-- table responsive end -->
				</div>
				<!-- widget content end -->
			</div>
			<!-- widget slim control end -->

			<!-- widget footer start -->
			<div class="widget-toolbox padding-8 clearfix">
				<div class="row" data-bind="with: GridProperties">
					<!-- pagination size start -->
					<div class="col-sm-6">
						<div class="dataTables_paginate paging_bootstrap pull-left">
							Showing <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
							rows of <span data-bind="text: Total"></span>
							entries
						</div>
					</div>
					<!-- pagination size end -->

					<!-- pagination page jump start -->
					<div class="col-sm-3">
						<div class="dataTables_paginate paging_bootstrap">
							Page <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width:50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page"/>
							of <span data-bind="text: TotalPages"></span>
						</div>
					</div>
					<!-- pagination page jump end -->

					<!-- pagination navigation start -->
					<div class="col-sm-3">
						<div class="dataTables_paginate paging_bootstrap">
							<ul class="pagination">
								<li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
									<a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
										<i class="icon-double-angle-left"></i>
									</a>
								</li>
								<li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
									<a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
										<i class="icon-angle-left"></i>
									</a>
								</li>
								<li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
									<a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
										<i class="icon-angle-right"></i>
									</a>
								</li>
								<li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
									<a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
										<i class="icon-double-angle-right"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
					<!-- pagination navigation end -->

				</div>
			</div>
			<!-- widget footer end -->

		</div>
		<!-- widget main end -->
	</div>
	<!-- widget body end -->
</div>
<!-- widget box end -->
                
<div data-bind="if: $root.IsRoleMaker()">
	<h4 class="pink">
		<i class="icon-hand-right green"></i>
		<a href="#modal-form" role="button" class="blue" data-toggle="modal" data-bind="click: NewData">
		New CBO Fund</a>
	</h4>
</div>


<!-- modal form start -->
<div id="modal-form" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content" >
			<!-- modal header start -->
			<div class="modal-header">
                <h4 class="blue bigger">
                    <span data-bind="if: $root.IsNewData()">Create a CBO Fund Parameter</span>
                    <span data-bind="if: !$root.IsNewData()">Modify a CBO Fund Parameter</span>
                </h4>
            </div>
			<!-- modal header end -->

			<!-- modal body start -->
            
            
			<div class="modal-body overflow-visible">
                <!-- modal body form start -->
                <div id="customer-form" class="form-horizontal" role="form">
                    <div class="space-4"></div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="offonshore">
                            Offshore/Onshore
                        </label>
                        <div class="col-sm-9">
                            <div class="clearfix">
                                 <select id="offonshore" name="offonshore" data-bind="value: OffOnshore().ID, options: ddlOffOnshore, optionsText: 'Description', optionsValue: 'ID', optionsCaption: 'Please Select...', disable: $root.Readonly" class="valid col-xs-10 col-sm-11" data-rule-required="true"></select>
                                 <label class="control-label bolder text-danger" for="offonshore" data-bind="disable: $root.Readonly, visible: $root.Readonly() == false">*</label>
                            </div>
                        </div>
                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="Fundcode">
                            Fund Code
                        </label>

                        <div class="col-sm-9">
                            <div class="clearfix">
                                <input type="text" id="Fundcode" name="Fundcode" data-bind="value: FundCode, disable: $root.Readonly" class="col-xs-10 col-sm-5" data-rule-required="true" data-rule-maxlength="100" />
                                <label class="control-label bolder text-danger" for="Fundcode" data-bind="disable: $root.Readonly, visible: $root.Readonly() == false">*</label>                                
                            </div>
                        </div>
                    </div>
                    <div class="space-4"></div>

                    <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="Fundname">
                    Fund Name
                </label>
                <div class="col-sm-9">
                    <div class="clearfix">
                        <input type="text" name="Fundname" id="Fundname" data-bind="value: FundName, disable: $root.Readonly" data-rule-required="true" class="col-xs-10 col-sm-11" data-rule-maxlength="500" />
                        <label class="control-label bolder text-danger" for="Fundname" data-bind="disable: $root.Readonly, visible: $root.Readonly() == false">*</label>                        
                    </div>
                </div>
            </div>

                    <div class="space-4"></div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="Accountno">
                            Account No
                        </label>
                        <div class="col-sm-9">
                            <div class="clearfix">
                                <input type="text" id="Accountno" name="Accountno" data-bind="value: AccountNo, disable: $root.Readonly" class="col-xs-10 col-sm-11" data-rule-maxlength="100" />                        
                            </div>
                        </div>
                    </div>
                    
                    <div class="space-4"></div>
                                <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="checkbox">
                </label>
                <div class="col-sm-9">
                    <div class="clearfix">
                        <input id="Switching" name="Switching" type="checkbox" data-bind="checked: IsSwitching" />
                        <input id="SavingPlan" name="SavingPlan" type="checkbox" data-bind="checked: IsSaving" />
                    </div>
                </div>
            </div>

            
                    <div class="space-4"></div>
                    <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="Savingplandate">
                    Saving Plan Date
                </label>
                <div class="col-sm-9">
                    <div class="clearfix input-group">
                        <input type="text" id="Savingplandate" name="Savingplandate" class="form-control date-picker" autocomplete="off" data-date-format="dd-M-yyyy" data-rule-value="true" data-bind="value: $root.LocalDate(SavingPlanDate), disable: $root.Readonly, enable: IsSaving" />
                        <span class="input-group-addon">
                            <i class="icon-calendar bigger-110"></i>
                        </span>
                        <!--<label class="control-label bolder text-danger starremove starremovefirefox" style="position:absolute;" for="application-date">*</label>-->
                    </div>
                </div>
            </div>                    
                </div>
                <!-- modal body form end -->
			</div>
			<!-- modal body end -->

			<!-- modal footer start -->
			<div class="modal-footer">
				<button class="btn btn-sm btn-primary" data-bind="click: $root.save, visible: $root.IsNewData() && $root.IsRoleMaker()">
                    <i class="icon-save"></i>
                    Save
                </button>
                <button class="btn btn-sm btn-primary" data-bind="click: $root.update, visible: !$root.IsNewData() && $root.IsRoleMaker()">
                    <i class="icon-edit"></i>
                    Update
                </button>
                <button class="btn btn-sm btn-warning" data-bind="click: $root.delete, visible: !$root.IsNewData() && $root.IsRoleMaker()">
                    <i class="icon-trash"></i>
                    Delete
                </button>
                <button class="btn btn-sm" data-dismiss="modal">
                    <i class="icon-remove"></i>
                    Close
                </button>
			</div>
			<!-- modal footer end -->

		</div>
	</div>
</div>
<!-- modal form end -->

<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/additional-methods.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.slimscroll.min.js"></script>