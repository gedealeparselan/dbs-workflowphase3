var accessToken;
var $box;
var $remove = false;

var TreeNavigation = function (el) {
    var self = this;

    self.UncheckHide = function () {
        $('#nestable > table > tbody > tr > td > input[id="cbHasAccess"]').each(function () {
            if (!$(this).prop('checked')) $(this).parent().parent().hide();
        });
        return false;
    }

    //self.Readonly = false;
    self.parent = el;
    self.DataList = [];
    self.Get = function () {
        result = new Array();
        $('table>tbody>tr>td>input', self.parent).each(function () {
            if ($(this).prop('checked')) {
                result.push({ "ID": $(this).parent().parent().attr('data-id') });
            }
        });
        return result;
    }

    self.Set = function (data) {
        $('table>tbody>tr>td>input', self.parent).each(function () {
            $(this).prop('checked', false);
        });
        $('table>tbody>tr>td>input', self.parent).each(function () {
            var chk = $(this);
            $.each(data, function (index, item) {
                if (item.ID == chk.parent().parent().attr('data-id')) {
                    chk.prop('checked', true);
                }
            });
        });
    }

    self.DoChecked = function () {
        if (self.Readonly) return false;
        var trs = $('table>tbody>tr');
        self.IsChecked(trs, 0, $(this), false, '');
    }
    self.ChildCount = function (trs, parent) {
        var result = 0;
        trs.each(function () {
            if ($(this).attr('data-parentid') == parent) result++;
        });
        return result;
    }
    self.IsChecked = function (trs, parent, chk, root, pad) {
        var result = false;
        var checkboxId = chk.attr('id');
        pad += '   ';

        trs.each(function (index) {
            if ($(this).attr('data-parentid') == parent) {
                //console.log(pad + '[' + $(this).attr('data-id') + ', ' + $(this).attr('data-parentid') + ']');
                var id = chk.parent().parent().attr('data-id');
                var pid = chk.parent().parent().attr('data-parentid');
                var parentException = $(this).attr('class');
                var childrenid; var parentid;
                if ($(this).attr('data-parentid') == id) {
                    if (parentException != 'parentException') {
                        root = true;

                    }
                }

                var checked = self.IsChecked(trs, $(this).attr('data-id'), chk, root, pad);
                if (root) {
                    $('td>#' + checkboxId, $(this)).prop('checked', chk.prop('checked'));
                } else {
                    childrenid = $(this).attr('data-parentid');
                    parentid = $('tr[data-id="' + childrenid + '"]');

                    if ($(this).attr('data-id') != id) {
                        if (parentException != 'parentException') {
                            if (self.ChildCount(trs, $(this).attr('data-id')) > 0)
                                $('td>#' + checkboxId, $(this)).prop('checked', checked);
                            if ($(this).attr('data-parentid') == id) {
                                if (chk.prop('checked') == false)
                                    $('td>#' + checkboxId, $(this)).prop('checked', chk.prop('checked'));


                            }
                        } else {
                            if ($(this).attr('data-id') == pid) {
                                if (self.ChildCount(trs, $(this).attr('data-id')) > 0)
                                    $('td>#' + checkboxId, $(this)).prop('checked', checked);

                            }
                            if ($(this).attr('data-parentid') == id) {
                                if (chk.prop('checked') == false)
                                    $('td>#' + checkboxId, $(this)).prop('checked', chk.prop('checked'));

                            }


                        }
                    }

                }
                result = result | $('td>#' + checkboxId, $(this)).prop('checked');
            }
        });
        return result;

    }
    self.Toggle = function () {
        var img = $(this);
        var parentId = $(this).parent().parent().attr('data-id');
        img.attr('class', img.attr('class') == 'icon-plus' ? 'icon-minus' : 'icon-plus');
        $('table>tbody>tr', self.parent).each(function () {
            if ($(this).attr('data-parentid') == parentId) {
                img.attr('class') == 'icon-plus' ? $(this).hide() : $(this).show();
            }
        });
    }
    self.RendRow = function (data, parent, pad) {
        var result = '';
        $.each(data, function (index, item) {
            if (item.ParentID == parent) {
                var subs = self.RendRow(data, item.ID, pad + '&nbsp;&nbsp;&nbsp');
                var icon = '&nbsp;&nbsp;&nbsp;';
                if (subs != '') icon = '<i class="icon-minus"></i>';

                result +=
                '		<tr data-id="' + item.ID + '" data-parentid="' + item.ParentID + '">' +
                '			<td>' + icon + '&nbsp;' + pad + item.Title + '</td>' +
                '			<td><input id="cbHasAccess" type="checkbox"/></td>' +
                '		</tr>';
                result += subs;
            }
        });
        return result;
    }

    self.Rend = function (data) {

        table = self.parent.append(
				'<table class="table table-bordered table-hover dataTable">' +
				'	<thead>' +
				'		<tr>' +
				'			<th>Name</th>' +
				'			<th>Check</th>' +
				'		</tr>' +
				'	</thead><tbody>' + self.RendRow(data, '', '') + '</tbody>' +
				'</table>'
				);
        $('table>tbody>tr>td>#cbHasAccess', self.parent).on('click', self.DoChecked);
        $('table>tbody>tr>td>i', self.parent).on('click', self.Toggle);
    }
    self.Load = function () {
        $.ajax({
            type: "GET",
            url: api.url.roledraftnavigation,
            data: {},
            contentType: "application/json",
            headers: {
                //"Authorization" : "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                self.Rend(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    self.IsHasChild = function (data, parentID) {
        result = false;
        $.each(data, function (index, item) {
            if (item.ParentID == parentID) result = true;
        });
        return result;
    }
};

var GridPropertiesModel = function (callback) {
    var self = this;

    self.SortColumn = ko.observable();
    self.SortOrder = ko.observable();
    self.AllowFilter = ko.observable(false);
    self.AvailableSizes = ko.observableArray(config.sizes);
    self.Page = ko.observable(1);
    self.Size = ko.observable(config.sizeDefault);
    self.Total = ko.observable(0);
    self.TotalPages = ko.observable(0);

    self.Callback = function () {
        callback();
    };

    self.OnPageSizeChange = function () {
        self.Page(1);

        self.Callback();
    };

    self.OnPageChange = function () {
        if (self.Page() < 1) {
            self.Page(1);
        } else {
            if (self.Page() > self.TotalPages())
                self.Page(self.TotalPages());
        }

        self.Callback();
    };

    self.NextPage = function () {
        var page = self.Page();
        if (page < self.TotalPages()) {
            self.Page(page + 1);

            self.Callback();
        }
    };

    self.PreviousPage = function () {
        var page = self.Page();
        if (page > 1) {
            self.Page(page - 1);

            self.Callback();
        }
    };

    self.FirstPage = function () {
        self.Page(1);

        self.Callback();
    };

    self.LastPage = function () {
        self.Page(self.TotalPages());

        self.Callback();
    };

    self.Filter = function (data) {
        self.Page(1);

        self.Callback();
    };

    // get sorted column
    self.GetSortedColumn = function (columnName) {
        var sort = "sorting";

        if (self.SortColumn() == columnName) {
            if (self.SortOrder() == "DESC")
                sort = sort + "_asc";
            else
                sort = sort + "_desc";
        }

        return sort;
    };

    // Sorting data
    self.Sorting = function (column) {
        if (self.SortColumn() == column) {
            if (self.SortOrder() == "ASC")
                self.SortOrder("DESC");
            else
                self.SortOrder("ASC");
        } else {
            self.SortOrder("ASC");
        }

        self.SortColumn(column);

        self.Page(1);

        self.Callback();
    };
};

var ViewModel = function (url, form, readonly) {
    //Make the self as 'this' reference
    var self = this;

    self.Form = form;

    self.Readonly = ko.observable(readonly);
    self.url = url;



    // filter
    self.FilterName = ko.observable("");
    self.FilterDescription = ko.observable("");
    self.FilterAction = ko.observable("");
    self.FilterModifiedBy = ko.observable("");
    self.FilterModifiedDate = ko.observable("");

    // Declare an ObservableArray for Storing the JSON Response
    self.Roles = ko.observableArray([]);

    // Draft
    self.RoleDrafts = ko.observableArray([]);

    // grid properties
    self.GridProperties = ko.observable();
    self.GridProperties(new GridPropertiesModel(GetData));

    // set default sorting
    self.GridProperties().SortColumn("Name");
    self.GridProperties().SortOrder("ASC");

    // bind clear filters
    self.ClearFilters = function () {
        self.FilterName("");
        self.FilterDescription("");
        self.FilterAction("");
        self.FilterModifiedBy("");
        self.FilterModifiedDate("");

        GetData();
    };

    // get is data drafted
    self.SetColorStatus = function (ID) {
        var isSelected = ko.utils.arrayFirst(self.RoleDrafts(), function (selectedData) {
            return selectedData.MasterID == ID;
        });

        if (isSelected) {
            return "danger";
        }
    };

    // bind get data function to view
    self.GetData = function () {
        GetData();
    };

    // Local Date
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return "";
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-18
    };

    //Function to Display record to be updated
    self.GetSelectedRow = function (data) {
        $.ajax({
            type: "GET",
            url: api.server + self.url + "/" + data.ID,
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                // send notification
                if (jqXHR.status = 200) {
                    self.Form.Show(data);
                } else
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
        flagHomeAndFXPage();
    };
    function flagHomeAndFXPage() {
        var menuCounter = $('#nestable > table > tbody > tr > td').length;
        var menuHierarchy = '';
        var isHomeOrFXMenu = false; var isEQID = false;
        var parentid = '';
        var childrenid = '';

        for (var i = 0; i <= menuCounter; i++) {
            menuHierarchy = $('#nestable > table > tbody > tr > td:eq(' + i + ')').text().trim().toLowerCase();
            isHomeOrFXMenu = menuHierarchy == 'home' || menuHierarchy == 'fx tracking' ? true : false;
            if (isHomeOrFXMenu) {

                parentid = $('#nestable > table > tbody > tr > td:eq(' + i + ')').parent().attr('data-id');
                $('#nestable > table > tbody > tr > td:eq(' + i + ')').parent().addClass('parentException');

                for (var j = 0; j <= menuCounter; j++) {

                    childrenid = $('#nestable > table > tbody > tr:eq(' + j + ')').attr('data-parentid');
                    isEQID = parentid == childrenid ? true : false;

                    if (isEQID) {
                        $('#nestable > table > tbody > tr:eq(' + j + ')').addClass('parentException');
                    }
                }
            }
        }

    }
    self.CopyFromExistingRole = function () {
        CopyFromExistingRole();
    }

    function CopyFromExistingRole() {
        var RoleID = $("#ExistingRole option:selected").val();
        $.ajax({
            type: "GET",
            url: api.server + self.url + "/" + RoleID,
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.Form.ShowCopyExistingRole(data);
                } else
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);


            },
            error: function (jqXHR, textStatus, errorThrown) {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);


            }

        });

    }
    //Function to Read All Customers
    function GetData() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        //get draft
        var optionsDraft = {
            url: api.server + 'api/RoleDraft',
            token: accessToken
        };

        Helper.Ajax.Get(optionsDraft, OnSuccessGetDraft, OnError, OnAlways);

        // declare options variable for ajax get request
        var options = {
            url: api.server + self.url,
            params: {
                page: self.GridProperties().Page(),
                size: self.GridProperties().Size(),
                sort_column: self.GridProperties().SortColumn(),
                sort_order: self.GridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetData, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetData, OnError, OnAlways);
        }
    }

    //Function to validation dynamic field

    function IsvalidField() { /*
     $("[name^=days]").each(function () {
     $(this).rules('add', {
     required: true,
     maxlength: 2,
     number: true,
     messages: {
     required: "Please provide a valid Day.",
     maxlength: "Please provide maximum 2 number valid day value.",
     number: "Please provide number"
     }
     });
     }); */
        return true;
    }

    // Get filtered columns value
    function GetFilteredColumns() {
        // define filter
        var filters = [];

        if (self.FilterName() != "") filters.push({ Field: 'Name', Value: self.FilterName() });
        if (self.FilterDescription() != "") filters.push({ Field: 'Description', Value: self.FilterDescription() });
        if (self.FilterAction() != "") filters.push({ Field: 'Action', Value: self.FilterAction() });
        if (self.FilterModifiedBy() != "") filters.push({ Field: 'LastModifiedBy', Value: self.FilterModifiedBy() });
        if (self.FilterModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.FilterModifiedDate() });

        return filters;
    };

    // On success GetData callback
    function OnSuccessGetData(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.Roles(data.Rows);

            self.GridProperties().Page(data['Page']);
            self.GridProperties().Size(data['Size']);
            self.GridProperties().Total(data['Total']);
            self.GridProperties().TotalPages(Math.ceil(self.GridProperties().Total() / self.GridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    // On success GetData callback
    function OnSuccessGetDraft(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.RoleDrafts(data);
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    // On Error callback
    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    // On Always callback
    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }
};
var ExistingRoleModel = function (id, name) {

    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);

}
var FormModel = function () {
    var self = this;

    self.updateCallback = function () { };

    self.SetUpdateCallback = function (callback) {
        self.updateCallback = callback;
    };

    self.treeNavigation = new TreeNavigation($('#nestable'));
    self.treeNavigation.Load();

    // New Data flag
    self.Readonly = ko.observable(false);
    self.IsNewData = ko.observable(false);

    self.IsRoleMaker = ko.observable(false);

    //Declare observable which will be bind with UI
    self.ID = ko.observable("");
    self.MasterID = ko.observable("");
    self.Name = ko.observable("");
    self.IsNewRole = ko.observable(true);
    self.ExistingRole = ko.observable(new ExistingRoleModel('', ''));
    self.ddlExistingRole = ko.observableArray([]);
    self.Description = ko.observable("");
    self.LastModifiedBy = ko.observable("");
    self.LastModifiedDate = ko.observable("");
    self.IsDeleted = ko.observable(false);

    //insert new
    self.NewData = function () {
        // flag as new Data
        self.IsNewData(true);
        self.Readonly(false);
        self.IsNewRole(true);
        self.ddlExistingRole(new ExistingRoleModel('', ''));
        self.GetDropdown();
        // bind empty data
        self.ID(0);
        self.MasterID(0);
        self.Name('');
        self.Description('');
        self.treeNavigation.Set([]);
        self.treeNavigation.Readonly = false;
        flagHomeAndFXPage();
    };
    function flagHomeAndFXPage() {
        var menuCounter = $('#nestable > table > tbody > tr > td').length;
        var menuHierarchy = '';
        var isHomeOrFXMenu = false; var isEQID = false;
        var parentid = '';
        var childrenid = '';


        for (var i = 0; i <= menuCounter; i++) {
            menuHierarchy = $('#nestable > table > tbody > tr > td:eq(' + i + ')').text().trim().toLowerCase();
            isHomeOrFXMenu = menuHierarchy == 'home' || menuHierarchy == 'fx tracking' ? true : false;
            if (isHomeOrFXMenu) {

                parentid = $('#nestable > table > tbody > tr > td:eq(' + i + ')').parent().attr('data-id');
                $('#nestable > table > tbody > tr > td:eq(' + i + ')').parent().addClass('parentException');

                for (var j = 0; j <= menuCounter; j++) {
                    childrenid = $('#nestable > table > tbody > tr:eq(' + j + ')').attr('data-parentid');
                    isEQID = parentid == childrenid ? true : false;

                    if (isEQID) {
                        $('#nestable > table > tbody > tr:eq(' + j + ')').addClass('parentException');

                    }
                }
            }
        }
    }
    self.Show = function (data) {
        self.Readonly(data.ActionType == null ? false : true);
        self.treeNavigation.Readonly = data.ActionType == null ? false : true;
        self.IsNewData(false);
        self.ID(data.ID);
        self.MasterID(data.ID);
        self.Name(data.Name);
        self.Description(data.Description);
        self.LastModifiedBy(data.LastModifiedBy);
        self.LastModifiedDate(data.LastModifiedDate);
        self.treeNavigation.Set(data.Menus);
        $("#modal-form").modal('show');
    }
    self.ShowCopyExistingRole = function (data) {
        self.Readonly(false);
        self.treeNavigation.Readonly = true;
        self.IsNewData(true);
        self.IsNewRole(false);
        self.treeNavigation.Set(data.Menus);
        $("#modal-form").modal('show');

    }
    self.ReleaseReadonlyRole = function (data) {
        self.Readonly(false);
        self.treeNavigation.Readonly = false;
        self.IsNewData(true);
        self.IsNewRole(true);
        self.treeNavigation.Set('');
        $("#modal-form").modal('show');


    }
    self.SetReadonlyRole = function (data) {
        self.Readonly(false);
        self.treeNavigation.Readonly = true;
        self.IsNewData(true);
        self.IsNewRole(false);
        self.treeNavigation.Set('');
        $("#modal-form").modal('show');


    }

    //The Object which stored data entered in the observables
    var Role = {
        ID: self.ID,
        MasterID: self.MasterID,
        Name: self.Name,
        Menus: [],
        Role: self.Roles,
        IsDeleted: self.IsDeleted,
        Description: self.Description,
        LastModifiedBy: self.LastModifiedBy,
        LastModifiedDate: self.LastModifiedDate
    };

    self.GetDropdown = function () {

        GetDropdown();

    };

    function GetDropdown() {

        var url = api.server + api.url.parameter + '?select=Role';


        $.ajax({
            type: "GET",
            url: url,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },

            success: function (dataValue, textStatus, jqXHR) {
                if (jqXHR.status = 200) {

                    if (dataValue['Role'] != null) {
                        //console.log(dataValue['Role']);	

                        self.ddlExistingRole(dataValue['Role']);
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);

                }
            },

            error: function (jqXHR, textStatus, errorThrown) {

                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }

        });
    }

    self.save = function (event, ui) {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        bootbox.confirm("Are you sure?", function (result) {

            if (!result) {

                $("#modal-form").modal('show');

            } else {

                if (form.valid()) {
                    //Ajax call to insert the Roles
                    Role.ActionType = 'ADD';
                    Role.Menus = self.treeNavigation.Get();
                    console.log(Role);
                    $.ajax({
                        type: "POST",
                        url: api.server + api.url.roledraft,
                        data: ko.toJSON(Role), //Convert the Observable Data into JSON
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {
                                // send notification
                                if (data.RoleID != 0) {
                                    ShowNotification('Create Request', 'Request to Add Role has been created', 'gritter-success', false);

                                    // add to list
                                    AddListItem(ui.currentTarget.children[0].id, data.RoleID);
                                } else {
                                    ShowNotification('Warning', jqXHR.responseJSON.Message, 'gritter-warning', true);
                                }

                                // hide current popup window
                                $("#modal-form").modal('hide');

                                // refresh data
                                if (self.updateCallback != null) self.updateCallback();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });
                }

            }
        });
    };

    self.updatedata = function (event, ui) {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        bootbox.confirm("Are you sure?", function (result) {

            if (!result) {

                $("#modal-form").modal('show');

            } else {

                if (form.valid()) {
                    //Ajax call to insert the Roles
                    Role.ActionType = ui.currentTarget.children[0].id;
                    Role.Menus = self.treeNavigation.Get();
                    console.log(Role);
                    $.ajax({
                        type: "POST",
                        url: api.server + api.url.roledraft + '/Update',
                        data: ko.toJSON(Role), //Convert the Observable Data into JSON
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {
                                // send notification
                                if (data.RoleID != 0) {

                                    switch (ui.currentTarget.children[0].id) {
                                        case "UPDATE":
                                            ShowNotification('Create Request', 'Request to Update Role has been created', 'gritter-success', false);
                                            break;
                                        case "DELETE":
                                            ShowNotification('Create Request', 'Request to Delete Role has been created', 'gritter-success', false);
                                            break;
                                    }

                                    // add to list
                                    AddListItem(ui.currentTarget.children[0].id, data.RoleID);

                                } else {
                                    ShowNotification('Warning', jqXHR.responseJSON.Message, 'gritter-warning', true);
                                }

                                // hide current popup window
                                $("#modal-form").modal('hide');

                                // refresh data
                                if (self.updateCallback != null) self.updateCallback();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });
                }

            }
        });
    };

    function AddListItem(ActionType, RoleID) {
        var body = {
            Title: RoleID.toString(),
            ActionType: ActionType,
            //EmployeeID: EmployeeID,
            __metadata: {
                type: config.sharepoint.metadata.listMasterRole
            }
        };

        var options = {
            url: config.sharepoint.url.api + "/Lists(guid'" + config.sharepoint.listIdMasterRole + "')/Items",
            data: JSON.stringify(body),
            digest: jQuery("#__REQUESTDIGEST").val()
        };

        Helper.Sharepoint.List.Add(options, OnSuccessAddListItem, OnError, OnAlways);
    }

    function OnSuccessAddListItem(data, textStatus, jqXHR) {
        // clear transaction model
        //viewModel.TransactionModel(null);

        // send notification
        //ShowNotification('Submit Success', 'New transaction has been submitted at '+ viewModel.LocalDate(data.d.Created), 'gritter-success', false);

        // redirect to all transaction
        //window.location = "/home/all-transaction";
        //bangkit
        //window.location = "/home";
    }

    self.update = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            // hide current popup window
            $("#modal-form").modal('hide');

            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {
                    //Ajax call to update the Customer
                    Role.ActionType = 'UPDATE';
                    Role.Menus = self.treeNavigation.Get();
                    $.ajax({
                        type: "POST",
                        url: api.server + api.url.roledraft,
                        data: ko.toJSON(Role), //Convert the Observable Data into JSON
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {
                                // send notification
                                ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // hide current popup window
                                $("#modal-form").modal('hide');

                                // refresh data
                                if (self.updateCallback != null) self.updateCallback();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });
                    /* CHANGE UPDATE TO ADD FOR WORKFLOW
                    $.ajax({
                        type: "PUT",
                        url: api.server + api.url.roledraft + "/" + Role.ID(),
                        data: ko.toJSON(Role),
                        contentType: "application/json",
                        headers: {
                            "Authorization" : "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if(jqXHR.status = 200){
                                // send notification
                                ShowNotification('Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // hide current popup window
                                $("#modal-form").modal('hide');

                                // refresh data
                                GetData();
                            }else{
                                ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });*/
                }
            });
        }
    };

    self.delete = function () {
        // hide current popup window
        $("#modal-form").modal('hide');

        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                $("#modal-form").modal('show');
            } else {
                //Ajax call to delete the Customer
                Role.ActionType = 'DELETE';
                Role.Menus = self.treeNavigation.Get();
                Role.IsDelete = true;
                $.ajax({
                    type: "POST",
                    url: api.server + api.url.roledraft,
                    data: ko.toJSON(Role), //Convert the Observable Data into JSON
                    contentType: "application/json",
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        if (jqXHR.status = 200) {
                            // send notification
                            ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                            // hide current popup window
                            $("#modal-form").modal('hide');

                            // refresh data
                            if (self.updateCallback != null) self.updateCallback();
                        } else {
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
                /* CHANGE DELETE TO ADD FOR WORKFLOW
                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.roledraft + "/" + Role.ID(),
                    headers: {
                        "Authorization" : "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if(jqXHR.status = 200){
                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                            // refresh data
                            GetData();
                        }else
                            ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });*/
            }
        });
    };

    // On Error callback
    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    // On Always callback
    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }
}

var MultiViewModel = function () {
    var self = this;
    self.Form = ko.observable(new FormModel());
    self.Role = ko.observable(new ViewModel(api.url.role, self.Form(), false));
    self.RoleDraft = ko.observable(new ViewModel(api.url.roledraft, self.Form(), true));
    self.Form().SetUpdateCallback(function () {
        self.Role().GetData();
        self.RoleDraft().GetData();
    });
};

$.holdReady(true);
var intervalRoleName = setInterval(function () {
    if (Object.keys(Const_RoleName).length != 0) {
        $.holdReady(false);
        clearInterval(intervalRoleName);
    }
}, 100);

//$(document).on("RoleNameReady", function() {
$(document).ready(function () {
    // widget loader
    $box = $('#widget-box');
    var event;
    $box.trigger(event = $.Event('reload.ace.widget'))
    if (event.isDefaultPrevented()) return
    $box.blur();

    /// block enter key from user to prevent submitted data.
    $("input").keypress(function (event) {
        var code = event.charCode || event.keyCode;
        if (code == 13) {
            ShowNotification("Page Information", "Enter key is disabled for this form.", "gritter-warning", false);

            return false;
        }
    });

    // scrollables
    $('.slim-scroll').each(function () {
        var $this = $(this);
        $this.slimscroll({
            height: $this.data('height') || 100,
            //width: $this.data('width') || 100,
            railVisible: true,
            alwaysVisible: true,
            color: '#D15B47'
        });
    });


    var multiViewModel = new MultiViewModel();

    //var viewModel = new ViewModel();
    ko.applyBindings(multiViewModel);
    //multiViewModel.RoleDraft().treeNavigation.Load();
    /*
        $('.modal-header').append('<button href="">Navigation</button>').on('click',function(){
            console.log(
                multiViewModel().Form().treeNavigation.Get()
            );
            return false;
        });
    */
    // Token Validation
    if ($.cookie(api.cookie.name) == undefined) {
        Helper.Token.Request(TokenOnSuccess, TokenOnError);
    } else {
        // read token from cookie
        accessToken = $.cookie(api.cookie.name);

        // call spuser function
        GetCurrentUser(multiViewModel.Role());
        GetCurrentUser(multiViewModel.RoleDraft());

        // call get data inside view model	
        multiViewModel.Role().GetData();
        multiViewModel.RoleDraft().GetData();

        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/CheckUserPermited/" + _spFriendlyUrlPageContextInfo.title,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    console.log("spUser Roles : " + ko.toJSON(multiViewModel.Role().SPUser.Roles));
                    console.log("spUser Permited : " + data);
                    //compare user role to page permission to get checker validation
                    for (i = 0; i < multiViewModel.Role().SPUser.Roles.length; i++) {
                        for (j = 0; j < data.length; j++) {
                            if (Const_RoleName[multiViewModel.Role().SPUser.Roles[i].ID] == data[j]) { //if (multiViewModel.Role().SPUser.Roles[i].Name == data[j]) {
                                if (Const_RoleName[multiViewModel.Role().SPUser.Roles[i].ID].endsWith('Maker')) { //if (multiViewModel.Role().SPUser.Roles[i].Name.endsWith('Maker')) {
                                    multiViewModel.Form().IsRoleMaker(true);
                                    return;
                                }
                                else {
                                    multiViewModel.Form().IsRoleMaker(false);
                                }
                            }
                        }
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    $('#IsNewRole').click(function () {
        if ($('#IsNewRole').is(':checked')) {
            multiViewModel.Form().ReleaseReadonlyRole();

        } else {

            multiViewModel.Form().SetReadonlyRole();

        }

    });





    $('.date-picker').datepicker({ autoclose: true }).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });

    //$("#aspnetForm").validate({onsubmit: false});
    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }

    });
});

// Get SPUser from cookies
function GetCurrentUser(vm) {
    if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
        spUser = $.cookie(api.cookie.spUser);

        vm.SPUser = spUser;


    }
}


// Token validation On Success function
function TokenOnSuccess(data, textStatus, jqXHR) {
    // store token on browser cookies
    $.cookie(api.cookie.name, data.AccessToken);
    accessToken = $.cookie(api.cookie.name);

    // call spuser function
    GetCurrentUser();

    // call get data inside view model
    multiViewModel.RoleDraft().GetParameters();
    multiViewModel.RoleDraft().GetData();
}

// Token validation On Error function
function TokenOnError(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}