﻿using DBS.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class NewTransactionLoanController : ApiController
    {
        private string message = string.Empty;
        private readonly INewTransactionLoanRepository repository = null;
        public NewTransactionLoanController()
        {
            this.repository = new NewTransactionLoanRepository();
        }
        public NewTransactionLoanController(INewTransactionLoanRepository repo)
        {
            this.repository = repo;
        }

        [HttpPost]
        [ResponseType(typeof(string))]
        [Route("api/NewTransactionLoan")]
        public HttpResponseMessage PostLoan(TransactionDetailModel transaction)
        {
            long transactionID = 0;
            string ApplicationID = string.Empty;
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddTransactionLoan(transaction, ref transactionID, ref ApplicationID, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new
                    {
                        ID = transactionID,
                        AppID = ApplicationID,
                        Message = transaction.IsDraft ? "Transaction has been saved as draft." : "New Transaction has been submitted."
                    });
                }
            }
        }

        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/NewTransactionLoan/Draft/{id}")]
        public HttpResponseMessage PutDraft(long id, TransactionDetailModel transaction)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateTransactionDraft(id, transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction data has been updated." });
                }
            }
        }

        [ResponseType(typeof(TransactionDisbursementModel))]
        [HttpGet]
        [Route("api/NewTransactionLoan/Draft/{id}")]
        public HttpResponseMessage GetTransactionLoanDisbursement(Int64 id)
        {
            TransactionDraftLoanModel output = new TransactionDraftLoanModel();
            if (!repository.GetTransactionDraftByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<TransactionDraftLoanModel>(HttpStatusCode.OK, output);
            }
        }

        [HttpDelete]
        [Route("api/NewTransactionLoan/Draft/Delete/{id}")]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage DeleteDraft(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteTransactionDraftByID(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Draft." });
                }
            }
        }
    }
}
