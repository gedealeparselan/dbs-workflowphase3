﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace DBS.Sharepoint
{
    #region Interfaces

    [ServiceContract]
    public interface IUserValidationServices
    {

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string UserAD(UserData userData);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string UserUnlockReleaseCCU(UserData userData);
    }

    #endregion

    #region Properties    

    [DataContract]
    public class UserData
    {
        [DataMember]
        public string UserID { get; set; }
        [DataMember]
        public string Password { get; set; }

    }    

    #endregion

}
