﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;

namespace DBS.WebAPI.Controllers
{
    //[Authorize]
    public class VolumeMatrixTransactionsController : ApiController
    {
        private string message = string.Empty;
        private readonly IVolumeMatrixTransactionsRepository repositoryTransactions = null;

        public VolumeMatrixTransactionsController()
        {
            this.repositoryTransactions = new VolumeMatrixTransactionsRepository();
        }
        public VolumeMatrixTransactionsController(IVolumeMatrixTransactionsRepository repositoryTransactions)
        {
            this.repositoryTransactions = repositoryTransactions;
        }


        [ResponseType(typeof(VolumeMatrixTransactionsGrid))]
        [Route("api/VolumeMatrixTransactions")]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string productName, DateTime startDate, DateTime endDate, DateTime clientDateTime, string sort_column, string sort_order, IList<VolumeMatrixTransactionsFilter> filters, string paymentMode)
        {
            VolumeMatrixTransactionsGrid output = new VolumeMatrixTransactionsGrid();
            if (!repositoryTransactions.GetVolumeMatrixTransactions(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, productName, startDate, endDate, clientDateTime, filters, string.IsNullOrEmpty(sort_column) ? "CustomerName" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, paymentMode, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<VolumeMatrixTransactionsGrid>(HttpStatusCode.OK, output);
            }
        }
    }
}
