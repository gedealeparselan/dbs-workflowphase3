﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class ParameterController : ApiController
    {
        private string message = string.Empty;
        //for cif
        private readonly IWorkflowCIFRepository repoworkflowcif = null;
        //end
        private readonly IMainDataCustomerCenterModelRepository repoMainDataCustomerCenter = null;
        private readonly IMaintenanceTypeRepository repoMaintenanceType = null;
        private readonly ICBOMaintenancePriorityOrderModelRepository repoCBOMaintenancePriorityOrderName = null;
        private readonly ICBOMaintenancePriorityOrderModelRepository repoCBOMaintenancePriorityOrder = null;
        private readonly IFundingMemoLTRepository repoFundingMemo = null;
        private readonly ITransactionTypeRepository repoTransactionType = null;
        private readonly ITransactionSubTypeRepository repoTransactionSubType = null;
        private readonly IProductRepository repoProduct = null;
        private readonly ICurrencyRepository repoCurrency = null;
        private readonly IRankRepository repoRank = null;
        private readonly IBranchRepository repoBranch = null;
        private readonly IRoleRepository repoRole = null;
        private readonly IBizSegmentRepository repoBizSegment = null;
        private readonly IBeneSegmentRepository repoBeneSegment = null;
        private readonly ICustomerTypeRepository repoCustomerType = null;
        private readonly IUnderlyingDocRepository repoUnderlyingDoc = null;
        private readonly IChannelRepository repoChannel = null;
        private readonly IAuditFirmRepository repoAuditFirm = null;
        private readonly IBankRepository repoBank = null;
        private readonly IGeneralParameterRepository repoGeneralParam = null;
        private readonly INostroRepository repoNostro = null;
        private readonly ISundryRepository repoSundry = null;
        private readonly ICBOFundRepository repoCBOFund = null;
        private readonly IParameterSystemRepository repoParamSystem = null;
        private readonly IChargesTypeRepository repoChargesType = null;
        private readonly IAgeingReportRepository repoAgeingReport = null;
        private readonly IPOAFunctionRepository repoPOAFunction = null;
        private readonly IEmployeeModelRepository repoUserApproval = null;
        private readonly IExceptionHandlingRepository repoExceptionHandling = null;
        private readonly ISLASettingRepository repoSLASetting = null;
        private readonly IRateTypeRepository repoRateType = null;
        private readonly IDocumentPurposeRepository repoPurposeDoc = null;
        private readonly IFXComplianceRepository repoFXCompliance = null;
        private readonly IMatrixRepository repoMatrix = null;
        private readonly IDocumentTypeRepository repoDocType = null;
        private readonly IStatementLetterRepository repoStatementLetter = null;
        private readonly IProductTypeRepository repoProductType = null;
        private readonly ILLDRepository repoLLD = null;
        private readonly ILLDDocumentRepository repoLLDDocument = null; //add by adi
        private readonly IMiddlewareParameterSystemRepository repoMiddleware = null;
        private readonly IRegionRepository repoRegion = null;
        private readonly IFunctionRoleRepository repoFunctionRole = null;
        private readonly IExceptionHandlingParameterRepository repoExceptionHandlingParameter = null;
        private readonly IExceptionHandlingOperatorRepository repoExceptionHandlingOperator = null;
        private readonly IMatrixFieldRepository repoMatrixField = null;
        private readonly IMurexMappingRepository repoMurexMapping = null;//murex by dani
        private readonly IFindingEmailTime repoFindingEmailTime = null;
        private readonly IPriorityProductModelInterface repoPriorityProduct = null; //Priority henggar
        private readonly IProductTypeThresholdMapping repoProductTypeThresholdMapping = null;
        private readonly IMatrixDOARepository repoDOA = null;
        private readonly IMatrixDOAParameterRepository repoDOAParam = null;
        private readonly IMatrixDOAOperatorRepository repoDOAOperator = null;
        private readonly IBeneficiaryBusinesRepository repoBeneficiaryBusines = null;
        #region IPE
        private readonly ICityModel repoCity = null;
        private readonly IBranchBankTes repoBranchBankTes = null;
        //private readonly IPaymentModeRepository repoPaymentMode = null;
        private readonly IRejectCodeRepo repoRejectCode = null;
        private readonly IUserCategoryRepository repoUserCategory = null;
        private readonly ICurrencyRepository repoChargingAccountCurrency = null;
        private readonly IBeneficiaryCountryRepository repoBeneficiaryCountry = null;
        private readonly ITransactionRelationshipRepository repoTransactionRelationship = null;
        private readonly IPOAEmailModel repoPOAEmailProduct = null; // add by david
        private readonly IRecipientRepository repoRecipient = null; // add by david
        private readonly ITBODocsSLARepository repoTBODocsSLA = null; // add by david
        private readonly IFDTransferRepository repoFDTransfer = null;
        private readonly ITypeOfTransactionRepository repoTypeOfTransaction = null;
        private readonly ITBOModelInterface repoTBOTransaction = null;
        #endregion
        public ParameterController()
        {
            this.repoworkflowcif = new WorkflowCIFRepository();
            this.repoMainDataCustomerCenter = new MainDataCustomerCenterRepository();
            this.repoMaintenanceType = new MaintenanceTypeRepository();
            this.repoCBOMaintenancePriorityOrderName = new CBOMaintenanceOrderRepository();
            this.repoCBOMaintenancePriorityOrder = new CBOMaintenanceOrderRepository();
            this.repoFundingMemo = new FundingMemoLTRepository();
            this.repoTransactionType = new TransactionTypeRepository();
            this.repoTransactionSubType = new TransactionSubTypeRepository();
            this.repoProduct = new ProductRepository();
            this.repoCurrency = new CurrencyRepository();
            this.repoRank = new RankRepository();
            this.repoBranch = new BranchRepository();
            this.repoRole = new RoleRepository();
            this.repoBizSegment = new BizSegmentRepository();
            this.repoBeneSegment = new BeneSegmentRepository();
            this.repoCustomerType = new CustomerTypeRepository();
            this.repoUnderlyingDoc = new UnderlyingDocRepository();
            this.repoChannel = new ChannelRepository();
            this.repoAuditFirm = new AuditFirmRepository();
            this.repoGeneralParam = new GeneralParameterRepository();
            this.repoMainDataCustomerCenter = new MainDataCustomerCenterRepository();
            this.repoBank = new BankRepository();
            this.repoNostro = new NostroRepository();
            this.repoSundry = new SundryRepository();
            this.repoCBOFund = new CBOFundRepository();
            this.repoParamSystem = new ParameterSystemRepository();
            this.repoChargesType = new ChargesTypeRepository();
            this.repoAgeingReport = new AgeingReportRepository();
            this.repoPOAFunction = new POAFunctionRepository();
            this.repoUserApproval = new EmployeeModelRepository();
            this.repoExceptionHandling = new ExceptionHandlingRepository();
            this.repoSLASetting = new SLASettingRepository();
            this.repoRateType = new RateTypeRepository();
            this.repoPurposeDoc = new PurposeDocsRepository();
            this.repoFXCompliance = new FXComplianceRepository();
            this.repoMatrix = new MatrixRepository();
            this.repoDocType = new DocumentTypeRepository();
            this.repoStatementLetter = new StatementLetterRepository();
            this.repoProductType = new ProductTypeRepository();
            this.repoLLD = new LLDRepository();
            this.repoLLDDocument = new LDDDocumentRepository();//add by adi
            this.repoMiddleware = new MiddlewareParameterSystemRepository();
            this.repoRegion = new RegionRepository();
            this.repoFunctionRole = new FunctionRoleRepository();
            this.repoExceptionHandlingParameter = new ExceptionHandlingParameterRepository();
            this.repoExceptionHandlingOperator = new ExceptionHandlingOperatorRepository();
            this.repoMatrixField = new MatrixFieldRepository();
            this.repoMurexMapping = new MurexMappingRepository();//murex by dani
            this.repoFindingEmailTime = new FindingEmailTimeRepository();
            this.repoPriorityProduct = new PriorityProductModelRepository(); //priority by henggar
            this.repoProductTypeThresholdMapping = new ProductTypeThresholdModelRepository();//by adi
            this.repoDOA = new MatrixDOARepository();
            this.repoDOAParam = new MatrixDOAParameterRepository();
            this.repoDOAOperator = new MatrixDOAOperatorRepository();
            this.repoBeneficiaryBusines = new BeneficiaryBusinesRepository();
            #region IPE
            this.repoCity = new CityRepository();
            this.repoBranchBankTes = new BranchBankTesRepository();
            //this.repoPaymentMode = new PaymentModeRepository();
            this.repoRejectCode = new RejectCodeRepository();
            this.repoUserCategory = new UserCategoryRepository();
            this.repoChargingAccountCurrency = new CurrencyRepository();
            this.repoBeneficiaryCountry = new BeneficiaryCountryRepository();
            this.repoTransactionRelationship = new TransactionRelationshipRepository();
            this.repoPOAEmailProduct = new POAEmailProductRepository(); // add by david
            this.repoRecipient = new RepicientRepository(); // add by david 
            this.repoTBODocsSLA = new TBODocsSLARepository(); // add by david
            this.repoFDTransfer = new FDTransferRepository();
            this.repoTypeOfTransaction = new TypeOfTransactionRepository();
            this.repoTBOTransaction = new TBOModelRepository();
            #endregion
        }


        /// <summary>
        /// Get Parameter by Name.
        /// </summary>
        /// <param name="select">Parameter Name.</param>
        /// <returns></returns>
        public HttpResponseMessage Get(string select = "CustomerAccount,TransactionSubType,CIFSUMBERDANA,CIFJENISIDENTITAS,CIFASSETBERSIH,CIFPENDPATANBULANAN,CIFPENGHASILANTAMBAHAN,CIFPEKERJAAN,CIFTUJUANPEMBUKAANREKENING,CIFPERKIRAANDANAMASUK,CIFPERKIRAANDANAKELUAR,CIFPERKIRAANTRANSAKSIKELUAR,MaintenanceType,TransactionType,InformationType,MaintenanceCC,Product,Currency,Rank,Branch,Role,BizSegment,BeneSegment,CustomerType,CategoryID,Channel,Bank,AuditFirm,MainDataCustomerCenter,MaintenanceTypeNames,MaintenanceTypeNamePrioritys,Nostro,Sundry,CBOFund,UnderlyingDoc,ChargesType,AgeingReport,POAFunction,UserApproval,ExceptionHandling,SLASetting,RateType,PurposeDoc,FXCompliance,DocType,Matrix,StatementLetter,ProductType,FunctionRole,Region,ExceptionHandlingParameter,ExceptionHandlingOperator,MatrixField,EscalationCustomerAvailable,EscalationCustomer,LoanParameterSystem,MurexMapping,FindingEmailTime,PriorityProduct,ProductTypeThresholdMapping, MatrixDOAParameter, MatrixDOAOperator,BeneficiaryBusines,City,BranchBankTes,PaymentMode, RejectCode, UserCategory, ChargingAccountCurrency, beneficiarycountry, transactionrelationship,LLDDocument,MiddlewareParameterSystem,POAEmailProduct,Recipient,TBODocsSLA,FDTransfer,TypeOfTransaction,TBOTransaction")//murex by dani
        {
            IDictionary<string, object> output = new Dictionary<string, object>();
            string messages = string.Empty;
            if (!string.IsNullOrEmpty(select))
            {
                string[] val = select.Split(',');
                foreach (string str in val)
                {

                    switch (str.ToLower())
                    {
                        case "beneficiarycountry":
                            IList<BeneficiaryCountryModel> BeneficiaryCountryModel = new List<BeneficiaryCountryModel>();
                            if (!repoBeneficiaryCountry.GetBeneficiaryCountry(ref BeneficiaryCountryModel, ref message))
                            {
                                messages += string.Concat("Error User BeneficiaryCountry: ", message);
                            }
                            else
                            {
                                output.Add("BeneficiaryCountry", BeneficiaryCountryModel);
                            }
                            BeneficiaryCountryModel = null;
                            break;

                        case "transactionrelationship":
                            IList<TransactionRelationshipModel> TransactionRelationshipModel = new List<TransactionRelationshipModel>();
                            if (!repoTransactionRelationship.GetTransactionRelationship(ref TransactionRelationshipModel, ref message))
                            {
                                messages += string.Concat("Error User TransactionRelationship: ", message);
                            }
                            else
                            {
                                output.Add("TransactionRelationship", TransactionRelationshipModel);
                            }
                            TransactionRelationshipModel = null;
                            break;
                        case "cifsumberdana":
                            IList<CIFSUMBERDANA> CIFSUMBERDANA = new List<CIFSUMBERDANA>();
                            if (!repoworkflowcif.GetCIFSUMBERDANA(ref CIFSUMBERDANA, ref message))
                            {
                                message += string.Concat("Error CIFSUMBERDANA : ", message);
                            }
                            else
                            {
                                output.Add("CIFSUMBERDANA", CIFSUMBERDANA);
                            }
                            CIFSUMBERDANA = null;
                            break;
                        case "cifjenisidentitas":
                            IList<CIFJENISIDENTITAS> CIFJENISIDENTITAS = new List<CIFJENISIDENTITAS>();
                            if (!repoworkflowcif.GetCIFJENISIDENTITAS(ref CIFJENISIDENTITAS, ref message))
                            {
                                messages += string.Concat("Error CIFJENISIDENTITAS : ", message);
                            }
                            else
                            {
                                output.Add("CIFJENISIDENTITAS", CIFJENISIDENTITAS);
                            }
                            CIFJENISIDENTITAS = null;
                            break;
                        case "cifassetbersih":
                            IList<CIFASSETBERSIH> CIFASSETBERSIH = new List<CIFASSETBERSIH>();
                            if (!repoworkflowcif.GetCIFASSETBERSIH(ref CIFASSETBERSIH, ref message))
                            {
                                messages += string.Concat("Error CIFASSETBERSIH : ", message);
                            }
                            else
                            {
                                output.Add("CIFASSETBERSIH", CIFASSETBERSIH);
                            }
                            CIFASSETBERSIH = null;
                            break;
                        case "cifpendpatanbulanan":
                            IList<CIFPENDPATANBULANAN> CIFPENDPATANBULANAN = new List<CIFPENDPATANBULANAN>();
                            if (!repoworkflowcif.GetCIFPENDPATANBULANAN(ref CIFPENDPATANBULANAN, ref message))
                            {
                                messages += string.Concat("Error CIFPENDPATANBULANAN : ", message);
                            }
                            else
                            {
                                output.Add("CIFPENDPATANBULANAN", CIFPENDPATANBULANAN);
                            }
                            CIFPENDPATANBULANAN = null;
                            break;
                        case "cifpenghasilantambahan":
                            IList<CIFPENGHASILANTAMBAHAN> CIFPENGHASILANTAMBAHAN = new List<CIFPENGHASILANTAMBAHAN>();
                            if (!repoworkflowcif.GetCIFPENGHASILANTAMBAHAN(ref CIFPENGHASILANTAMBAHAN, ref message))
                            {
                                messages += string.Concat("Error CIFPENGHASILANTAMBAHAN : ", message);
                            }
                            else
                            {
                                output.Add("CIFPENGHASILANTAMBAHAN", CIFPENGHASILANTAMBAHAN);
                            }
                            CIFPENGHASILANTAMBAHAN = null;
                            break;
                        case "cifpekerjaan":
                            IList<CIFPEKERJAAN> CIFPEKERJAAN = new List<CIFPEKERJAAN>();
                            if (!repoworkflowcif.GetCIFPEKERJAAN(ref CIFPEKERJAAN, ref message))
                            {
                                messages += string.Concat("Error CIFPEKERJAAN : ", message);
                            }
                            else
                            {
                                output.Add("CIFPEKERJAAN", CIFPEKERJAAN);
                            }
                            CIFPENGHASILANTAMBAHAN = null;
                            break;
                        case "ciftujuanpembukaanrekening":
                            IList<CIFTUJUANPEMBUKAANREKENING> CIFTUJUANPEMBUKAANREKENING = new List<CIFTUJUANPEMBUKAANREKENING>();
                            if (!repoworkflowcif.GetCIFTUJUANPEMBUKAANREKENING(ref CIFTUJUANPEMBUKAANREKENING, ref message))
                            {
                                messages += string.Concat("Error CIFTUJUANPEMBUKAANREKENING : ", message);
                            }
                            else
                            {
                                output.Add("CIFTUJUANPEMBUKAANREKENING", CIFTUJUANPEMBUKAANREKENING);
                            }
                            CIFTUJUANPEMBUKAANREKENING = null;
                            break;
                        case "cifperkiraandanamasuk":
                            IList<CIFPERKIRAANDANAMASUK> CIFPERKIRAANDANAMASUK = new List<CIFPERKIRAANDANAMASUK>();
                            if (!repoworkflowcif.GetCIFPERKIRAANDANAMASUK(ref CIFPERKIRAANDANAMASUK, ref message))
                            {
                                messages += string.Concat("Error CIFPERKIRAANDANAMASUK : ", message);
                            }
                            else
                            {
                                output.Add("CIFPERKIRAANDANAMASUK", CIFPERKIRAANDANAMASUK);
                            }
                            CIFPERKIRAANDANAMASUK = null;
                            break;
                        case "cifperkiraandanakeluar":
                            IList<CIFPERKIRAANDANAKELUAR> CIFPERKIRAANDANAKELUAR = new List<CIFPERKIRAANDANAKELUAR>();
                            if (!repoworkflowcif.GetCIFPERKIRAANDANAKELUAR(ref CIFPERKIRAANDANAKELUAR, ref message))
                            {
                                messages += string.Concat("Error CIFPERKIRAANDANAKELUAR : ", message);
                            }
                            else
                            {
                                output.Add("CIFPERKIRAANDANAKELUAR", CIFPERKIRAANDANAKELUAR);
                            }
                            CIFPERKIRAANDANAKELUAR = null;
                            break;
                        case "cifperkiraantransaksikeluar":
                            IList<CIFPERKIRAANTRANSAKSIKELUAR> CIFPERKIRAANTRANSAKSIKELUAR = new List<CIFPERKIRAANTRANSAKSIKELUAR>();
                            if (!repoworkflowcif.GetCIFPERKIRAANTRANSAKSIKELUAR(ref CIFPERKIRAANTRANSAKSIKELUAR, ref message))
                            {
                                messages += string.Concat("Error CIFPERKIRAANTRANSAKSIKELUAR : ", message);
                            }
                            else
                            {
                                output.Add("CIFPERKIRAANTRANSAKSIKELUAR", CIFPERKIRAANTRANSAKSIKELUAR);
                            }
                            CIFPERKIRAANTRANSAKSIKELUAR = null;
                            break;
                        case "frequency":
                            IList<PeggingFrequency> frequency = new List<PeggingFrequency>();
                            if (!repoFundingMemo.GetRepricingFrequency(ref frequency, ref message))
                            {
                                messages += string.Concat("Error frequency : ", message);
                            }
                            else
                            {
                                output.Add("Frequency", frequency);
                            }
                            frequency = null;
                            break;
                        case "interest":
                            IList<Interest> interest = new List<Interest>();
                            if (!repoFundingMemo.GetInterestCode(ref interest, ref message))
                            {
                                messages += string.Concat("Error LoanType : ", message);
                            }
                            else
                            {
                                output.Add("Interest", interest);
                            }
                            interest = null;
                            break;
                        case "repricingplan":
                            IList<RepricingPlan> repricingplan = new List<RepricingPlan>();
                            if (!repoFundingMemo.GetRepricing(ref repricingplan, ref message))
                            {
                                messages += string.Concat("Error LoanType : ", message);
                            }
                            else
                            {
                                output.Add("RepricingPlan", repricingplan);
                            }
                            interest = null;
                            break;
                        case "loantype":
                            IList<ParameterSystemModelFunding> loantype = new List<ParameterSystemModelFunding>();
                            if (!repoFundingMemo.GetLoanType(ref loantype, ref message))
                            {
                                messages += string.Concat("Error LoanType : ", message);
                            }
                            else
                            {
                                output.Add("LoanID", loantype);
                            }
                            loantype = null;
                            break;
                        case "programtype":
                            IList<ProgramType> programtype = new List<ProgramType>();
                            if (!repoFundingMemo.GetProgramType(ref programtype, ref message))
                            {
                                messages += string.Concat("Error ProgramType : ", message);
                            }
                            else
                            {
                                output.Add("ProgramType", programtype);
                            }
                            programtype = null;
                            break;
                        case "schemecode":
                            IList<FinacleScheme> schemecode = new List<FinacleScheme>();
                            if (!repoFundingMemo.GetSchemeCode(ref schemecode, ref message))
                            {
                                messages += string.Concat("Error SchemeCode : ", message);
                            }
                            else
                            {
                                output.Add("FinacleScheme", schemecode);
                            }
                            programtype = null;
                            break;
                        case "amountdisburse":
                            IList<CurrencyModel> amountdisburse = new List<CurrencyModel>();
                            if (!repoCurrency.GetCurrency(ref amountdisburse, ref message))
                            {
                                messages += string.Concat("Error amountdisburse : ", message);
                            }
                            else
                            {
                                output.Add("AmountDisburseID", amountdisburse);
                            }
                            amountdisburse = null;
                            break;
                        case "chargingaccountcurrency":
                            IList<CurrencyModel> chargingaccountcurrency = new List<CurrencyModel>();
                            if (!repoChargingAccountCurrency.GetCurrency(ref chargingaccountcurrency, ref message))
                            {
                                messages += string.Concat("Error amountdisburse : ", message);
                            }
                            else
                            {
                                output.Add("AmountDisburseID", chargingaccountcurrency);
                            }
                            chargingaccountcurrency = null;
                            break;
                        case "sanctionlimitcurrid":
                            IList<CurrencyModel> sanctionlimitcurr = new List<CurrencyModel>();
                            if (!repoCurrency.GetCurrency(ref sanctionlimitcurr, ref message))
                            {
                                messages += string.Concat("Error sanctionlimitcurr : ", message);
                            }
                            else
                            {
                                output.Add("SanctionLimitCurrID", sanctionlimitcurr);
                            }
                            sanctionlimitcurr = null;
                            break;
                        case "utilization":
                            IList<CurrencyModel> utilization = new List<CurrencyModel>();
                            if (!repoCurrency.GetCurrency(ref utilization, ref message))
                            {
                                messages += string.Concat("Error utilization : ", message);
                            }
                            else
                            {
                                output.Add("utilization", utilization);
                            }
                            utilization = null;
                            break;
                        case "outstanding":
                            IList<CurrencyModel> outstanding = new List<CurrencyModel>();
                            if (!repoCurrency.GetCurrency(ref outstanding, ref message))
                            {
                                messages += string.Concat("Error outstanding : ", message);
                            }
                            else
                            {
                                output.Add("Outstanding", outstanding);
                            }
                            outstanding = null;
                            break;
                        case "maintenancecc":
                            IList<MainDataCustomerCenterModel> maintenanceCC = new List<MainDataCustomerCenterModel>();
                            if (!repoMainDataCustomerCenter.GetMainDataCustomerCenterMaintenanceCC(ref maintenanceCC, ref message))
                            {
                                messages += string.Concat("Error Bene Segment : ", message);
                            }
                            else
                            {
                                output.Add("MaintenanceCC", maintenanceCC);
                            }
                            maintenanceCC = null;
                            break;
                        case "maintenancetype":
                            IList<MaintenanceTypeModel> maintenancetype = new List<MaintenanceTypeModel>();
                            if (!repoMaintenanceType.GetMaintenanceType(ref maintenancetype, ref message))
                            {
                                messages += string.Concat("Error Maintenance Type : ", message);
                            }
                            else
                            {
                                output.Add("MaintenanceType", maintenancetype);
                            }
                            maintenancetype = null;
                            break;
                        case "informationtype":
                            IList<MainDataCustomerCenterModel> informationtype = new List<MainDataCustomerCenterModel>();
                            if (!repoMainDataCustomerCenter.GetMainDataCustomerCenter(ref informationtype, ref message))
                            {
                                messages += string.Concat("Error Bene Segment : ", message);
                            }
                            else
                            {
                                output.Add("InformationType", informationtype);
                            }
                            informationtype = null;
                            break;
                        case "maintenancetypenames":
                            IList<CBOMaintenancePriorityOrderModel> maintenancetypenames = new List<CBOMaintenancePriorityOrderModel>();
                            if (!repoCBOMaintenancePriorityOrder.GetCBOMaintenaceType(ref maintenancetypenames, ref message))
                            {
                                messages += string.Concat("Error Bene Segment : ", message);
                            }
                            else
                            {
                                output.Add("MaintenanceTypeNames", maintenancetypenames);
                            }
                            maintenancetypenames = null;
                            break;
                        case "maintenancetypenameprioritys":
                            IList<CBOMaintenancePriorityOrderModel> maintenancetypenameprioritys = new List<CBOMaintenancePriorityOrderModel>();
                            if (!repoCBOMaintenancePriorityOrderName.GetCBOMaintenaceTypePriority(ref maintenancetypenameprioritys, ref message))
                            {
                                messages += string.Concat("Error Bene Segment : ", message);
                            }
                            else
                            {
                                output.Add("MaintenanceTypeNamePrioritys", maintenancetypenameprioritys);
                            }
                            maintenancetypenameprioritys = null;
                            break;
                        case "transactiontype":
                            IList<TransactionTypeModel> transactiontypes = new List<TransactionTypeModel>();
                            if (!repoTransactionType.GetTransactionType(ref transactiontypes, ref message))
                            {
                                messages += string.Concat("Error TransactionType : ", message);
                            }
                            else
                            {
                                output.Add("TransactionType", transactiontypes);
                            }
                            transactiontypes = null;
                            break;
                        case "requesttypecif":
                            IList<TransactionTypeParameterModel> requesttypecif = new List<TransactionTypeParameterModel>();
                            int ProductID = (int)DBSProductID.CIFProductIDCons;
                            if (!repoTransactionType.GetTransactionTypeByProductID(ProductID, ref requesttypecif, ref message))
                            {
                                messages += string.Concat("Error Request Type : ", message);
                            }
                            else
                            {
                                output.Add("RequestType", requesttypecif);
                            }
                            transactiontypes = null;
                            break;
                        case "categoryid":
                            IList<BizSegmentModel> categoryid = new List<BizSegmentModel>();
                            if (!repoBizSegment.GetBizSegment(ref categoryid, ref message))
                            {
                                messages += string.Concat("Error Biz Segment : ", message);
                            }
                            else
                            {
                                output.Add("CategoryID", categoryid);
                            }
                            categoryid = null;
                            break;
                        case "benesegment":
                            IList<BeneSegmentModel> beneSegments = new List<BeneSegmentModel>();
                            if (!repoBeneSegment.GetBeneSegment(ref beneSegments, ref message))
                            {
                                messages += string.Concat("Error Bene Segment : ", message);
                            }
                            else
                            {
                                output.Add("BeneSegment", beneSegments);
                            }
                            beneSegments = null;
                            break;
                        case "currency":
                            IList<CurrencyModel> currencies = new List<CurrencyModel>();
                            if (!repoCurrency.GetCurrency(ref currencies, ref message))
                            {
                                messages += string.Concat("Error Currency : ", message);
                            }
                            else
                            {
                                output.Add("Currency", currencies);
                            }
                            currencies = null;
                            break;
                        case "product":
                            IList<ProductModel> products = new List<ProductModel>();
                            if (!repoProduct.GetProduct(ref products, ref message))
                            {
                                messages += string.Concat("Error Bene Segment : ", message);
                            }
                            else
                            {
                                output.Add("Product", products);
                            }
                            products = null;
                            break;
                        case "role":
                            IList<RoleModel> roles = new List<RoleModel>();
                            if (!repoRole.GetRole(ref roles, ref message))
                            {
                                messages += string.Concat("Error Role : ", message);
                            }
                            else
                            {
                                output.Add("Role", roles);
                            }
                            roles = null;
                            break;
                        case "rank":

                            IList<RankModel> ranks = new List<RankModel>();
                            if (!repoRank.GetRank(ref ranks, ref message))
                            {
                                messages += string.Concat("Error Rank : ", message);
                            }
                            else
                            {
                                output.Add("Rank", ranks);
                            }
                            ranks = null;
                            break;
                        case "branch":

                            IList<BranchModel> branchs = new List<BranchModel>();
                            if (!repoBranch.GetBranch(ref branchs, ref message))
                            {
                                messages += string.Concat("Error Branch : ", message);
                            }
                            else
                            {
                                output.Add("Branch", branchs);
                            }
                            branchs = null;
                            break;
                        case "customertype":

                            IList<CustomerTypeModel> customerType = new List<CustomerTypeModel>();
                            if (!repoCustomerType.GetCustomerType(ref customerType, ref message))
                            {
                                messages += string.Concat("Error Customer Type : ", message);
                            }
                            else
                            {
                                output.Add("CustomerType", customerType);
                            }
                            customerType = null;
                            break;

                        case "bizsegment":
                            IList<BizSegmentModel> bizsegment = new List<BizSegmentModel>();
                            if (!repoBizSegment.GetBizSegment(ref bizsegment, ref message))
                            {
                                messages += string.Concat("Error Biz Segment : ", message);
                            }
                            else
                            {
                                output.Add("BizSegment", bizsegment);
                            }
                            bizsegment = null;
                            break;
                        case "channel":
                            IList<ChannelModel> channel = new List<ChannelModel>();
                            if (!repoChannel.GetChannel(ref channel, ref message))
                            {
                                messages += string.Concat("Error Channel : ", message);
                            }
                            else
                            {
                                output.Add("Channel", channel);
                            }
                            channel = null;
                            break;
                        case "auditfirm":
                            IList<AuditFirmModel> auditFirm = new List<AuditFirmModel>();
                            if (!repoAuditFirm.GetAuditFirm(ref auditFirm, ref message))
                            {
                                messages += string.Concat("Error Audit Firm : ", message);
                            }
                            else
                            {
                                output.Add("AuditFirm", auditFirm);
                            }
                            auditFirm = null;
                            break;
                        //case "maindatacustomercenter":
                        //    IList<MainDataCustomerCenterModel> mainDataCustomerCenter = new List<MainDataCustomerCenterModel>();
                        //    if (!repoMainDataCustomerCenter.GetInformationType(ref mainDataCustomerCenter, ref message))
                        //    {
                        //        messages += string.Concat("Error Main Data Customer Center : ", message);
                        //    }
                        //    else
                        //    {
                        //        output.Add("MainDataCustomerCenter", mainDataCustomerCenter);
                        //    }
                        //    mainDataCustomerCenter = null;
                        //    break;
                        case "bank":
                            IList<BankModel> bank = new List<BankModel>();
                            if (!repoBank.GetBank(ref bank, ref message))
                            {
                                messages += string.Concat("Error Bank : ", message);
                            }
                            else
                            {
                                output.Add("Bank", bank);
                            }
                            bank = null;
                            break;
                        case "branchbank":
                            IList<BranchBankTesModel> BranchBankModel = new List<BranchBankTesModel>();
                            if (!repoBranchBankTes.GetBranchBank(ref BranchBankModel, ref message))
                            {
                                message += string.Concat("Eror Branch Bank : ", message);
                            }
                            else
                            {
                                output.Add("Branch Bank", BranchBankModel);
                            }
                            break;
                        case "nostro":
                            IList<NostroModel> nostro = new List<NostroModel>();
                            if (!repoNostro.GetNostro(ref nostro, ref message))
                            {
                                messages += string.Concat("Error Nostro : ", message);
                            }
                            else
                            {
                                output.Add("Nostro", nostro);
                            }
                            nostro = null;
                            break;
                        case "sundry":
                            IList<SundryModel> sundry = new List<SundryModel>();
                            if (!repoSundry.GetSundry(ref sundry, ref message))
                            {
                                messages += string.Concat("Error Sundry : ", message);
                            }
                            else
                            {
                                output.Add("Sundry", sundry);
                            }
                            sundry = null;
                            break;
                        case "cbofund":
                            IList<CBOFundModel> cbofund = new List<CBOFundModel>();
                            if (!repoCBOFund.GetCBOFund(ref cbofund, ref message))
                            {
                                messages += string.Concat("Error CBO Fund : ", message);
                            }
                            else
                            {
                                output.Add("CBO Fund", cbofund);
                            }
                            cbofund = null;
                            break;
                        case "FindingEmailTime":
                            IList<FindingEmailTimeModel> findingEmail = new List<FindingEmailTimeModel>();
                            if (!repoFindingEmailTime.GetFindingEmailTime(ref findingEmail, ref message))
                            {
                                message += string.Concat("Error Finding Email Time : ", message);
                            }
                            else
                            {
                                output.Add("Finding Email Time :", findingEmail);
                            }
                            findingEmail = null;
                            break;
                        case "ProductTypeThresholdMapping":
                            IList<ThresholdGroupModel> ThresholdGroup = new List<ThresholdGroupModel>();
                            if (!repoProductTypeThresholdMapping.GetProductTypeThresholdMapping(ref ThresholdGroup, ref message))
                            {
                                message += string.Concat("Product Type Threshold Mapping : ", message);
                            }
                            else
                            {
                                output.Add("Product Type Threshold Mapping :", ThresholdGroup);
                            }
                            ThresholdGroup = null;
                            break;
                        /*case "parametersystem":
                            IList<ParameterSystemModel> param = new List<ParameterSystemModel>();
                            //string modul = Request.QueryString["MName"];
                            string modul = string.Empty;
                            if(!repoParamSystem.GetParamSystem(ref param, ref message, modul)) {
                                messages += string.Concat("Error Parameter System : ", message);
                            }
                            else
                            {
                                output.Add("Parameter System", param);
                            }
                            param = null;
                            break;*/
                        case "underlyingdoc":
                            IList<UnderlyingDocModel> underlyingDocs = new List<UnderlyingDocModel>();
                            if (!repoUnderlyingDoc.GetUnderlyingDoc(ref underlyingDocs, ref message))
                            {
                                messages += string.Concat("Error Underlying Docs : ", message);
                            }
                            else
                            {
                                output.Add("UnderltyingDoc", underlyingDocs);
                            }
                            underlyingDocs = null;
                            break;
                        case "chargestype":
                            IList<ChargesTypeModel> chargesTypes = new List<ChargesTypeModel>();
                            if (!repoChargesType.GetChargesType(ref chargesTypes, ref message))
                            {
                                messages += string.Concat("Error Charges Type : ", message);
                            }
                            else
                            {
                                output.Add("ChargesType", chargesTypes);
                            }
                            chargesTypes = null;
                            break;
                        case "ageingreport":
                            IList<AgeingReportModel> ageingReports = new List<AgeingReportModel>();
                            if (!repoAgeingReport.GetAgeingReport(ref ageingReports, ref message))
                            {
                                messages += string.Concat("Error Ageing Report : ", message);
                            }
                            else
                            {
                                output.Add("AgeingReport", ageingReports);
                            }
                            ageingReports = null;
                            break;
                        case "poafunction":
                            IList<POAFunctionModel> pOAFunctions = new List<POAFunctionModel>();
                            if (!repoPOAFunction.GetPOAFunction(ref pOAFunctions, ref message))
                            {
                                messages += string.Concat("Error POA Function : ", message);
                            }
                            else
                            {
                                output.Add("POAFunction", pOAFunctions);
                            }
                            pOAFunctions = null;
                            break;
                        case "userapproval":
                            IList<EmployeeModel> userApproval = new List<EmployeeModel>();
                            if (!repoUserApproval.GetUserApproval(ref userApproval, ref message))
                            {
                                messages += string.Concat("Error User Approval : ", message);
                            }
                            else
                            {
                                output.Add("UserApproval", userApproval);
                            }
                            userApproval = null;
                            break;
                        case "exceptionhandling":
                            IList<ExceptionHandlingModel> exceptionHandlings = new List<ExceptionHandlingModel>();
                            if (!repoExceptionHandling.GetExceptionHandling(ref exceptionHandlings, ref message))
                            {
                                messages += string.Concat("Error Charges Type : ", message);
                            }
                            else
                            {
                                output.Add("ExceptionHandling", exceptionHandlings);
                            }
                            exceptionHandlings = null;
                            break;
                        case "slasetting":
                            IList<SLASettingModel> sLASettings = new List<SLASettingModel>();
                            if (!repoSLASetting.GetSLASetting(ref sLASettings, ref message))
                            {
                                messages += string.Concat("Error SLA Setting : ", message);
                            }
                            else
                            {
                                output.Add("SLASetting", sLASettings);
                            }
                            sLASettings = null;
                            break;
                        case "ratetype":
                            IList<RateTypeModel> rateTypes = new List<RateTypeModel>();
                            if (!repoRateType.GetRateType(ref rateTypes, ref message))
                            {
                                messages += string.Concat("Error Rate Type : ", message);
                            }
                            else
                            {
                                output.Add("RateType", rateTypes);
                            }
                            rateTypes = null;
                            break;
                        case "purposedoc":
                            IList<DocumentPurposeModel> purposeDocs = new List<DocumentPurposeModel>();
                            if (!repoPurposeDoc.GetPurposeDocs(ref purposeDocs, ref message))
                            {
                                messages += string.Concat("Error Purpose Docs : ", message);
                            }
                            else
                            {
                                output.Add("PurposeDoc", purposeDocs);
                            }
                            purposeDocs = null;
                            break;
                        case "fxcompliance":
                            IList<FXComplianceModel> fXCompliance = new List<FXComplianceModel>();
                            if (!repoFXCompliance.GetFXCompliance(ref fXCompliance, ref message))
                            {
                                messages += string.Concat("Error FX Compliance Code : ", message);
                            }
                            else
                            {
                                output.Add("FXCompliance", fXCompliance);
                            }
                            fXCompliance = null;
                            break;
                        case "matrix":
                            IList<MatrixModel> matrix = new List<MatrixModel>();
                            if (!repoMatrix.GetMatrix(ref matrix, ref message))
                            {
                                messages += string.Concat("Error Matrix : ", message);
                            }
                            else
                            {
                                output.Add("Matrix", matrix);
                            }
                            matrix = null;
                            break;
                        case "doctype":
                            IList<DocumentTypeModel> docType = new List<DocumentTypeModel>();
                            if (!repoDocType.GetDocType(ref docType, ref message))
                            {
                                messages += string.Concat("Error Document Type : ", message);
                            }
                            else
                            {
                                output.Add("DocType", docType);
                            }
                            docType = null;
                            break;
                        case "statementletter":
                            IList<StatementLetterModel> statementLetter = new List<StatementLetterModel>();
                            if (!repoStatementLetter.GetStatementLetter(ref statementLetter, ref message))
                            {
                                messages += string.Concat("Error Statement Letter : ", message);
                            }
                            else
                            {
                                output.Add("StatementLetter", statementLetter);
                            }
                            statementLetter = null;
                            break;
                        case "lld":
                            IList<LLDModel> lld = new List<LLDModel>();
                            if (!repoLLD.GetLLD(ref lld, ref message))
                            {
                                messages += string.Concat("Error Statement Letter : ", message);
                            }
                            else
                            {
                                output.Add("LLD", lld);
                            }
                            statementLetter = null;
                            break;
                        case "llddocument": //add by adi
                            IList<LLDDocumentModel> LLDDocument = new List<LLDDocumentModel>();
                            if (!repoLLDDocument.GetLLDDocument(ref LLDDocument, ref message))
                            {
                                messages += string.Concat("Error LLD Document : ", message);
                            }
                            else
                            {
                                output.Add("LLDDocument", LLDDocument);
                            }
                            LLDDocument = null;
                            break;
                        case "tbotransaction":
                            IList<DocumentTypeTBOModel> TBO = new List<DocumentTypeTBOModel>();
                            if (!repoTBOTransaction.GetTBO(ref TBO, ref message))
                            {
                                messages += string.Concat("Error TBO Transaction : ", message);
                            }
                            else
                            {
                                output.Add("TBOTransaction", TBO);
                            }
                            TBO = null;
                            break;
                        case "middlewareparametersystem": //add by adi
                            IList<MiddlewareParameterSystemModel> MiddlewareParameterSystem = new List<MiddlewareParameterSystemModel>();
                            if (!repoMiddleware.GetMiddlewareParameterSystem(ref MiddlewareParameterSystem, ref message))
                            {
                                messages += string.Concat("Error Middleware Parameter System : ", message);
                            }
                            else
                            {
                                output.Add("Middleware Parameter System", MiddlewareParameterSystem);
                            }
                            MiddlewareParameterSystem = null;
                            break;

                        case "producttype":
                            IList<ProductTypeModel> productType = new List<ProductTypeModel>();
                            if (!repoProductType.GetProductType(ref productType, ref message))
                            {
                                messages += string.Concat("Error Product Type : ", message);
                            }
                            else
                            {
                                output.Add("ProductType", productType);
                            }
                            productType = null;
                            break;
                        case "region":
                            IList<RegionModel> region = new List<RegionModel>();
                            if (!repoRegion.GetRegion(ref region, ref message))
                            {
                                messages += string.Concat("Error Region : ", message);
                            }
                            else
                            {
                                output.Add("Region", region);
                            }
                            region = null;
                            break;
                        case "functionrole":
                            IList<FunctionRoleModel> functionrole = new List<FunctionRoleModel>();
                            if (!repoFunctionRole.GetFunctionRole(ref functionrole, ref message))
                            {
                                messages += string.Concat("Error Function Roles : ", message);
                            }
                            else
                            {
                                output.Add("FunctionRole", functionrole);
                            }
                            functionrole = null;
                            break;
                        case "exceptionhandlingparameter":
                            IList<ExceptionHandlingParameterModel> exceptionhandlingparameter = new List<ExceptionHandlingParameterModel>();
                            if (!repoExceptionHandlingParameter.GetExceptionHandlingParameter(ref exceptionhandlingparameter, ref message))
                            {
                                messages += string.Concat("Error Exception Handling Parameter : ", message);
                            }
                            else
                            {
                                output.Add("ExceptionHandlingParameter", exceptionhandlingparameter);
                            }
                            exceptionhandlingparameter = null;
                            break;
                        case "exceptionhandlingoperator":
                            IList<ExceptionHandlingOperatorModel> exceptionhandlingoperator = new List<ExceptionHandlingOperatorModel>();
                            if (!repoExceptionHandlingOperator.GetExceptionHandlingOperator(ref exceptionhandlingoperator, ref message))
                            {
                                messages += string.Concat("Error Exception Handling Operator : ", message);
                            }
                            else
                            {
                                output.Add("ExceptionHandlingOperator", exceptionhandlingoperator);
                            }
                            exceptionhandlingoperator = null;
                            break;
                        case "matrixfield":
                            IList<MatrixFieldModel> matrixfield = new List<MatrixFieldModel>();
                            if (!repoMatrixField.GetMatrixField(ref matrixfield, ref message))
                            {
                                messages += string.Concat("Error Matrix Field : ", message);
                            }
                            else
                            {
                                output.Add("MatrixField", matrixfield);
                            }
                            matrixfield = null;
                            break;
                        case "loanparametersystem":
                            IList<GeneralParameterModel> loanParSys = new List<GeneralParameterModel>();
                            if (!repoGeneralParam.GetAllLoanParameterSystem(ref loanParSys, ref message))
                            {
                                message += string.Concat("Error Loan Parameter System : ", message);
                            }
                            else
                            {
                                output.Add("LOAN_CUSTOMER_CATEGORY", loanParSys.Where(p => p.StringKey == "LOAN_CUSTOMER_CATEGORY"));
                                output.Add("LOAN_TYPE", loanParSys.Where(p => p.StringKey == "LOAN_TYPE"));
                                output.Add("LOAN_PROGRAM_TYPE", loanParSys.Where(p => p.StringKey == "LOAN_PROGRAM_TYPE"));
                                output.Add("LOAN_SCHEME_CODE", loanParSys.Where(p => p.StringKey == "LOAN_SCHEME_CODE"));
                                output.Add("LOAN_INTEREST_RATE_CODE", loanParSys.Where(p => p.StringKey == "LOAN_INTEREST_RATE_CODE"));
                                output.Add("LOAN_REPRICING_PLAN", loanParSys.Where(p => p.StringKey == "LOAN_REPRICING_PLAN"));
                                output.Add("LOAN_PRINCIPAL_FREQ", loanParSys.Where(p => p.StringKey == "LOAN_PRINCIPAL_FREQ"));
                                output.Add("LOAN_INTEREST_FREQ", loanParSys.Where(p => p.StringKey == "LOAN_INTEREST_FREQ"));
                                output.Add("LOAN_PEGGING_FREQ", loanParSys.Where(p => p.StringKey == "LOAN_PEGGING_FREQ"));
                                output.Add("LOAN_MAINTENANCE_TYPE", loanParSys.Where(p => p.StringKey == "LOAN_MAINTENANCE_TYPE"));
                            }
                            loanParSys = null;
                            break;
                        //murex by dani
                        case "murexmapping":
                            IList<MurexMappingModel> murex = new List<MurexMappingModel>();
                            if (!repoMurexMapping.GetMurexAll(ref murex, ref message))
                            {
                                messages += string.Concat("Error Murex : ", message);
                            }
                            else
                            {
                                output.Add("Murex", murex);
                            }
                            murex = null;
                            break;
                        //murex by dani end

                        //henggar
                        case "priorityproduct":
                            IList<PriorityProductModel> priority = new List<PriorityProductModel>();
                            if (!repoPriorityProduct.GetPriority(ref priority, ref message))
                            {
                                message += string.Concat("Eror Priority:", message);
                            }
                            else
                            {
                                output.Add("Priority", priority);
                            }
                            priority = null;
                            break;
                        //end henggar

                        case "transactionsubtype":
                            IList<TransactionSubTypeModel> transactionsubtypes = new List<TransactionSubTypeModel>();
                            if (!repoTransactionSubType.GetTransactionSubType(ref transactionsubtypes, ref message))
                            {
                                messages += string.Concat("Error Transaction Sub Type : ", message);
                            }
                            else
                            {
                                output.Add("TransactionSubType", transactionsubtypes);
                            }
                            transactionsubtypes = null;
                            break;
                        case "matrixdoaparameter":
                            IList<MatrixDOAParameterModel> matrixParamDOA = new List<MatrixDOAParameterModel>();
                            if (!repoDOAParam.GetMatrixDOAParameter(ref matrixParamDOA, ref message))
                            {
                                messages += string.Concat("Error Matrix DOA parameter: ", message);
                            }
                            else
                            {
                                output.Add("MatrixDOAParameter", matrixParamDOA);
                            }
                            matrixParamDOA = null;
                            break;
                        case "matrixdoaoperator":
                            IList<MatrixDOAOperatorModel> matrixOperatorDOA = new List<MatrixDOAOperatorModel>();
                            if (!repoDOAOperator.GetMatrixDOAOperator(ref matrixOperatorDOA, ref message))
                            {
                                messages += string.Concat("Error Matrix DOA Operator: ", message);
                            }
                            else
                            {
                                output.Add("MatrixDOAOperator", matrixOperatorDOA);
                            }
                            matrixOperatorDOA = null;
                            break;
                        case "City":
                            IList<CityModel> cityModel = new List<CityModel>();
                            if (!repoCity.GetCity(ref cityModel, ref message))
                            {
                                message += string.Concat("Eror City : ", message);
                            }
                            else
                            {
                                output.Add("City", cityModel);
                            }
                            break;

                        case "beneficiarybusines":
                            IList<BeneficiaryBusinesModel> beneficiaryBusines = new List<BeneficiaryBusinesModel>();
                            if (!repoBeneficiaryBusines.GetBeneficiaryBusines(ref beneficiaryBusines, ref message))
                            {
                                messages += string.Concat("Error Beneficiary Busines : ", message);
                            }
                            else
                            {
                                output.Add("BeneficiaryBusines", beneficiaryBusines);
                            }
                            beneficiaryBusines = null;
                            break;
                        case "rejectcode":
                            IList<RejectCodeModel> rejectCode = new List<RejectCodeModel>();
                            if (!repoRejectCode.GetRejectCode(ref rejectCode, ref message))
                            {
                                messages += string.Concat("Error Reject Code Type: ", message);
                            }
                            else
                            {
                                output.Add("RejectCode", rejectCode);
                            }
                            rejectCode = null;
                            break;
                        case "usercategory":
                            IList<UserCategoryModel> usercategory = new List<UserCategoryModel>();
                            if (!repoUserCategory.GetUserCategory(ref usercategory, ref message))
                            {
                                messages += string.Concat("Error User Category: ", message);
                            }
                            else
                            {
                                output.Add("UserCategory", usercategory);
                            }
                            usercategory = null;
                            break;

                        // start David
                        case "POAEmailProduct":
                            IList<POAEmailModel> poaemailproduct = new List<POAEmailModel>();
                            if (!repoPOAEmailProduct.GetPOAEmailProduct(ref poaemailproduct, ref message))
                            {
                                message += string.Concat("Eror POAEmailProduct : ", message);
                            }
                            else
                            {
                                output.Add("POAEmailProduct", poaemailproduct);
                            }
                            break;

                        case "Recipient":
                            IList<RecipientModel> recipient = new List<RecipientModel>();
                            if (!repoRecipient.GetRecipient(ref recipient, ref message))
                            {
                                messages += string.Concat("Error Recipient : ", message);
                            }
                            else
                            {
                                output.Add("recipient", recipient);
                            }
                            recipient = null;
                            break;

                        case "TBODocsSLA":
                            IList<TBODocsSLAModel> tbodocssla = new List<TBODocsSLAModel>();
                            if (!repoTBODocsSLA.GetTBODocsSLA(ref tbodocssla, ref message))
                            {
                                messages += string.Concat("Error TBODocsSLA : ", message);
                            }
                            else
                            {
                                output.Add("tbodocssla", tbodocssla);
                            }
                            tbodocssla = null;
                            break;

                        case "fdtransfer":
                            IList<FDTransferModel> fdtransfer = new List<FDTransferModel>();
                            if (!repoFDTransfer.GetFDTransfer(ref fdtransfer, ref message))
                            {
                                messages += string.Concat("Error FDTransfer : ", message);
                            }
                            else
                            {
                                output.Add("FDTransfer", fdtransfer);
                            }
                            fdtransfer = null;
                            break;

                        case "TypeOfTransaction":
                            IList<TypeOfTransactionModel> typeoftransaction = new List<TypeOfTransactionModel>();
                            if (!repoTypeOfTransaction.GetTypeOfTransaction(ref typeoftransaction, ref message))
                            {
                                messages += string.Concat("Error TypeOfTransaction : ", message);
                            }
                            else
                            {
                                output.Add("TypeOfTransaction", typeoftransaction);
                            }
                            typeoftransaction = null;
                            break;
                        // end David

                        default:
                            break;
                    }
                }
            }


            if (output == null)
                return Request.CreateResponse(HttpStatusCode.NoContent);
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, output);
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (repoMainDataCustomerCenter != null)
                repoMainDataCustomerCenter.Dispose();
            if (repoMaintenanceType != null)
                repoMaintenanceType.Dispose();
            if (repoTransactionType != null)
                repoTransactionType.Dispose();
            if (repoTransactionSubType != null)
                if (repoProduct != null)
                    repoProduct.Dispose();
            if (repoBeneSegment != null)
                repoBeneSegment.Dispose();
            if (repoCurrency != null)
                repoCurrency.Dispose();
            if (repoRole != null)
                repoRole.Dispose();
            if (repoRank != null)
                repoRank.Dispose();
            if (repoBranch != null)
                repoBranch.Dispose();
            if (repoCustomerType != null)
                repoCustomerType.Dispose();
            if (repoBizSegment != null)
                repoBizSegment.Dispose();
            if (repoUnderlyingDoc != null)
                repoUnderlyingDoc.Dispose();
            if (repoChannel != null)
                repoChannel.Dispose();
            if (repoAuditFirm != null)
                repoAuditFirm.Dispose();
            //if (repoMainDataCustomerCenter != null)
            //    repoMainDataCustomerCenter.Dispose();
            if (repoBank != null)
                repoBank.Dispose();
            if (repoSundry != null)
                repoSundry.Dispose();
            if (repoCBOFund != null)
                repoCBOFund.Dispose();
            if (repoNostro != null)
                repoNostro.Dispose();
            if (repoFindingEmailTime != null)
                repoFindingEmailTime.Dispose();
            if (repoProductTypeThresholdMapping != null)
                repoProductTypeThresholdMapping.Dispose();
            if (repoParamSystem != null)
                repoParamSystem.Dispose();
            if (repoChargesType != null)
                repoChargesType.Dispose();
            if (repoAgeingReport != null)
                repoAgeingReport.Dispose();
            if (repoPOAFunction != null)
                repoPOAFunction.Dispose();
            if (repoUserApproval != null)
                repoUserApproval.Dispose();
            if (repoSLASetting != null)
                repoSLASetting.Dispose();
            if (repoRateType != null)
                repoRateType.Dispose();
            if (repoPurposeDoc != null)
                repoPurposeDoc.Dispose();
            if (repoFXCompliance != null)
                repoFXCompliance.Dispose();
            if (repoDocType != null)
                repoDocType.Dispose();
            if (repoDocType != null)
                repoDocType.Dispose();
            if (repoStatementLetter != null)
                repoStatementLetter.Dispose();
            if (repoProductType != null)
                repoProductType.Dispose();
            if (repoLLD != null)
                repoLLD.Dispose();
            if (repoLLDDocument != null) //add By Adi
                repoLLDDocument.Dispose();
            if (repoFunctionRole != null)
                repoFunctionRole.Dispose();
            if (repoExceptionHandling != null)
                repoExceptionHandling.Dispose();
            if (repoExceptionHandlingOperator != null)
                repoExceptionHandlingOperator.Dispose();
            if (repoExceptionHandlingParameter != null)
                repoExceptionHandlingParameter.Dispose();
            if (repoRegion != null)
                repoRegion.Dispose();
            if (repoMatrixField != null)
                repoMatrixField.Dispose();
            //murex by dani
            if (repoMurexMapping != null)
                repoMurexMapping.Dispose();
            //murex by dani end
            //henggar
            if (repoPriorityProduct != null)
                repoPriorityProduct.Dispose();
            //end henggar
            if (repoCity != null)
                repoCity.Dispose();
            if (repoBranchBankTes != null)
                repoBranchBankTes.Dispose();

            if (repoBeneficiaryBusines != null)
            {
                repoBeneficiaryBusines.Dispose();
            }
            //if (repoPaymentMode != null)
            //{
            //    repoPaymentMode.Dispose();
            //}
            if (repoRejectCode != null)
            {
                repoRejectCode.Dispose();
            }
            if (repoUserCategory != null)
            {
                repoUserCategory.Dispose();
            }
            if (repoBeneficiaryCountry != null)
            {
                repoBeneficiaryCountry.Dispose();
            }
            if (repoTransactionRelationship != null)
            {
                repoTransactionRelationship.Dispose();
            }
            //start david
            if (repoPOAEmailProduct != null)
            {
                repoPOAEmailProduct.Dispose();
            }
            if (repoRecipient != null)
                repoRecipient.Dispose();

            if (repoTBODocsSLA != null)
                repoTBODocsSLA.Dispose();
            if (repoFDTransfer != null)
                repoFDTransfer.Dispose();
            if (repoTypeOfTransaction != null)
                repoTypeOfTransaction.Dispose();
            //end david 
            base.Dispose(disposing);
        }
    }

}