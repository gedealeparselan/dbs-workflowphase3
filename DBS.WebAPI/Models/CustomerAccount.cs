﻿using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;

namespace DBS.WebAPI.Models
{
    #region Property
    [ModelName("CustomerAccount")]
    public class CustomerAccountModel
    {

        /// <summary>
        /// CustomerAccount Number.
        /// </summary>
        public string AccountNumber { get; set; }


        /// <summary>
        /// Currency.
        /// </summary>
        public CurrencyModel Currency { get; set; }
        /// <summary>
        /// Is Joint Account
        /// </summary>
        public bool? IsJointAccount { get; set; }
        /// <summary>
        /// Customer Name Concated
        /// </summary>
        public string CustomerName { get; set; }
        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }

        public bool? IsAddTblFreezeAccount { get; set; }
        public bool? IsAddDormantAccount { get; set; }
        public bool? IsFreezeAccount { get; set; }
        public string CIF { get; set; }
    }

    public class CustomerAccountRow : CustomerAccountModel
    {
        public int RowID { get; set; }

    }
    public class CustomerAccountFixModel : CustomerAccountModel
    {
        public string CIF { get; set; }
    }

    public class CustomerAccountGrid : Grid
    {
        public IList<CustomerAccountRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class CustomerAccountFilter : Filter { }
    #endregion

    #region Interface
    public interface ICustomerAccountRepository : IDisposable
    {
       bool GetCustomerAccountByID(string cif, string accountNumber, ref CustomerAccountModel customerAccount, ref string message);

       bool GetCustomerAccount(string cif, int page, int size, IList<CustomerAccountFilter> filters, string sortColumn, string sortOrder, ref CustomerAccountGrid customerAccount, ref string message);

       bool GetCustomerAccountMapping(string cif, int page, int size, IList<CustomerAccountFilter> filters, string sortColumn, string sortOrder, ref CustomerAccountGrid customerAccount, ref string message);

       // bool GetCustomerAccount(string cif, ref IList<CustomerAccountModel> customerAccounts, ref string message);
        bool GetCustomerAccountMapping(string cif, ref IList<CustomerAccountModel> customerAccounts, ref string message);
        bool GetCustomerJointAccount(string cif, ref IList<CustomerAccountModel> customerAccounts, ref string message);

        bool AddCustomerAccount(string cif, CustomerAccountModel customerAccount, ref string message);
        bool UpdateCustomerAccount(string cif, CustomerAccountModel customerAccount, ref string message);
        bool DeleteCustomerAccount(string cif, string accountNumber, ref string message);
        bool GetCustomerAccountByCIF(string cif, ref List<CustomerAccountModel> customerAccount, ref string message);
    }

  
    #endregion


    #region Repository
    public class CustomerAccountRepository : ICustomerAccountRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetCustomerAccountByID(string cif, string accountNumber, ref CustomerAccountModel customerAccount, ref string message)
        {
            bool isSuccess = false;

            try
            {
                customerAccount = (from a in context.CustomerAccounts
                                   where a.IsDeleted.Equals(false) && a.CIF.Equals(cif) && a.AccountNumber.Equals(accountNumber)
                                   select new CustomerAccountModel
                                   {
                                       AccountNumber = a.AccountNumber,
                                       Currency = new CurrencyModel()
                                       {
                                           ID = a.Currency.CurrencyID,
                                           Code = a.Currency.CurrencyCode,
                                           Description = a.Currency.CurrencyDescription,
                                           LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                           LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                       },
                                       IsJointAccount = a.IsJointAccount.HasValue ? a.IsJointAccount.Value:false,
                                       LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                       LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                   }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCustomerAccount(string cif, int page, int size, IList<CustomerAccountFilter> filters, string sortColumn, string sortOrder, ref CustomerAccountGrid customerAccount, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                CustomerAccountModel filter = new CustomerAccountModel();
                filter.Currency = new CurrencyModel();
                if (filters != null)
                {
                    filter.AccountNumber = (string)filters.Where(a => a.Field.Equals("AccountNumber")).Select(a => a.Value).SingleOrDefault();
                    filter.Currency.Code = (string)filters.Where(a => a.Field.Equals("CurrencyCode")).Select(a => a.Value).SingleOrDefault();
                    filter.Currency.Description = (string)filters.Where(a => a.Field.Equals("CurrencyDesc")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.CustomerAccounts
                                let isAccountNumber = !string.IsNullOrEmpty(filter.AccountNumber)
                                let isCurrencyCode = !string.IsNullOrEmpty(filter.Currency.Code)
                                let isCurrencyDesc = !string.IsNullOrEmpty(filter.Currency.Description)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) && a.CIF.Equals(cif) &&
                                    (isAccountNumber ? a.AccountNumber.Contains(filter.AccountNumber) : true) &&
                                    (isCurrencyCode ? a.Currency.CurrencyCode.Contains(filter.Currency.Code) : true) &&
                                    (isCurrencyDesc ? a.Currency.CurrencyDescription.Contains(filter.Currency.Description) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new CustomerAccountModel
                                {
                                    AccountNumber = a.AccountNumber,
                                    Currency = new CurrencyModel
                                    {
                                        ID = a.Currency.CurrencyID,
                                        Code = a.Currency.CurrencyCode,
                                        Description = a.Currency.CurrencyDescription
                                    },
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    customerAccount.Page = page;
                    customerAccount.Size = size;
                    customerAccount.Total = data.Count();
                    customerAccount.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    customerAccount.Rows = data.OrderBy(orderBy).AsEnumerable().Select((a, i) => new CustomerAccountRow()
                    //customerAccount.Rows = data.AsEnumerable()
                    //.Select((a, i) => new CustomerAccountRow
                    {
                        RowID = i + 1,
                        AccountNumber = a.AccountNumber,
                        Currency = a.Currency,
                        LastModifiedBy = a.LastModifiedBy,
                        LastModifiedDate = a.LastModifiedDate
                    })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetCustomerAccount(string cif, ref IList<CustomerAccountModel> customerAccount, ref string message)
        {
            bool isSuccess = false;

            try
            {
                customerAccount = (from a in context.CustomerAccounts
                                   where a.IsDeleted.Equals(false) && a.CIF.Equals(cif)
                                   orderby a.AccountNumber
                                   select new CustomerAccountModel
                                   {
                                       AccountNumber = a.AccountNumber,
                                       Currency = new CurrencyModel()
                                       {
                                           ID = a.Currency.CurrencyID,
                                           Code = a.Currency.CurrencyCode,
                                           Description = a.Currency.CurrencyDescription,
                                           LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                           LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                       },
                                       LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                       LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                   }).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddCustomerAccount(string cif, CustomerAccountModel customerAccount, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (!context.CustomerAccounts.Where(a => a.AccountNumber.Equals(customerAccount.AccountNumber) && a.IsDeleted.Equals(false)).Any())
                {
                    context.CustomerAccounts.Add(new CustomerAccount()
                    {
                        CIF = cif,
                        AccountNumber = customerAccount.AccountNumber,
                        CurrencyID = customerAccount.Currency.ID,
                        IsDeleted = false,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    });

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Customer Account {0} is already exist.", customerAccount.AccountNumber);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool UpdateCustomerAccount(string cif, CustomerAccountModel customerAccount, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.CustomerAccount data = context.CustomerAccounts.Where(a => a.CIF.Equals(cif) && a.AccountNumber.Equals(customerAccount.AccountNumber) && a.IsDeleted.Equals(false)).SingleOrDefault();

                if (data != null)
                {
                    /*       if (context.CustomerAccounts.Where(a => a.AccountNumber != customerAccount.AccountNumber && a.CIF.Equals(customerAccount.CIF) && a.IsDeleted.Equals(false)).Count() >= 1)
                           {
                               message = string.Format("Customer Account {0} is exist.", customerAccount.AccountNumber);
                           }
                           else
                           { */
                    data.AccountNumber = customerAccount.AccountNumber;
                    data.CurrencyID = customerAccount.Currency.ID;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                    //}
                }
                else
                {
                    message = string.Format("Customer Account {0} is does not exist.", customerAccount.AccountNumber);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool DeleteCustomerAccount(string cif, string accountNumber, ref string message)
        {
            bool isSuccess = false;

            try
            {
                CustomerAccount data = context.CustomerAccounts.Where(a => a.CIF.Equals(cif) && a.AccountNumber.Equals(accountNumber)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Customer Account {0} is does not exist.", accountNumber);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool GetCustomerJointAccount(string cif, ref IList<CustomerAccountModel> customerAccount, ref string message)
        {
            bool isSuccess = false;

            try
            {
                customerAccount = (from a in context.CustomerAccountMappings
                                   where a.IsDeleted.Equals(false) && a.CIF.Equals(cif) && (a.RelationType.Equals("M") || (a.RelationType.Equals("J")))
                                   orderby a.AccountNumber
                                   select new CustomerAccountModel
                                   {
                                       AccountNumber = a.AccountNumber,
                                       Currency = context.Currencies.Join(context.CustomerAccounts.Where(x => x.AccountNumber.Equals(a.AccountNumber)), cur => cur.CurrencyID, acc => acc.CurrencyID, (cur, acc) => new CurrencyModel()
                                       {
                                           ID = cur.CurrencyID,
                                           Code = cur.CurrencyCode,
                                           Description = cur.CurrencyDescription,
                                           LastModifiedBy = cur.UpdateDate == null ? cur.CreateBy : cur.UpdateBy,
                                           LastModifiedDate = cur.UpdateDate == null ? cur.CreateDate : cur.UpdateDate.Value
                                       }).SingleOrDefault(),
                                       IsJointAccount = true,
                                       LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                       LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                   }).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool GetCustomerAccountByCIF(string cif, ref List<CustomerAccountModel> customerAccount, ref string message)
        {

            bool isSuccess = false;

            try
            {
                customerAccount = (from a in context.SP_GETJointAccount(cif)
                                   select new CustomerAccountModel
                                   {
                                       AccountNumber = a.AccountNumber,
                                       Currency = new CurrencyModel
                                       {
                                           ID = a.CurrencyID.Value,
                                           Code = a.CurrencyCode,
                                           Description = a.CurrencyDescription,
                                       },
                                       CustomerName = a.CustomerName,
                                       IsJointAccount = a.IsJointAccount.HasValue?a.IsJointAccount.Value:false,
                                       IsAddTblFreezeAccount = false,
                                       IsAddDormantAccount = false,
                                       IsFreezeAccount = false,
                                   }).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        public bool GetCustomerAccountMapping(string cif, int page, int size, IList<CustomerAccountFilter> filters, string sortColumn, string sortOrder, ref CustomerAccountGrid customerAccount, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();
                string filterJointAccount = string.Empty;
                CustomerAccountModel filter = new CustomerAccountModel();
                filter.Currency = new CurrencyModel();
                if (filters != null)
                {
                    filter.AccountNumber = (string)filters.Where(a => a.Field.Equals("AccountNumber")).Select(a => a.Value).SingleOrDefault();
                    filter.Currency.Code = (string)filters.Where(a => a.Field.Equals("CurrencyCode")).Select(a => a.Value).SingleOrDefault();
                    filter.Currency.Description = (string)filters.Where(a => a.Field.Equals("CurrencyDesc")).Select(a => a.Value).SingleOrDefault();
                    filter.IsJointAccount = string.IsNullOrEmpty((string)filters.Where(a => a.Field.Equals("IsJointAccount")).Select(a => a.Value).SingleOrDefault())? false:true;
                    if(filter.IsJointAccount.HasValue && filter.IsJointAccount.Value){
                    filterJointAccount = (string)filters.Where(a => a.Field.Equals("IsJointAccount")).Select(a => a.Value).SingleOrDefault();
                    }
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                   
                    var data = (from a in context.CustomerAccountMappings
                                join b in context.CustomerAccounts on a.AccountNumber equals b.AccountNumber
                                join c in context.Currencies on b.CurrencyID equals c.CurrencyID
                                let isAccountNumber = !string.IsNullOrEmpty(filter.AccountNumber)
                                let isCurrencyCode = !string.IsNullOrEmpty(filter.Currency.Code)
                                let isCurrencyDesc = !string.IsNullOrEmpty(filter.Currency.Description)
                                let isIsJointAccount = filter.IsJointAccount.HasValue?filter.IsJointAccount.Value:false
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) && a.CIF.Equals(cif) &&
                                    (isAccountNumber ? a.AccountNumber.Contains(filter.AccountNumber) : true) &&
                                    (isCurrencyCode ? c.CurrencyCode.Contains(filter.Currency.Code) : true) &&
                                    (isCurrencyDesc ? c.CurrencyDescription.Contains(filter.Currency.Description) : true) &&
                                    (isIsJointAccount ? (("Join").Contains(filterJointAccount) ? b.IsJointAccount.Value == true : (("Single").Contains(filterJointAccount) ? b.IsJointAccount.Value == false ||b.IsJointAccount.Value == null : a.CIF=="N/A")) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new CustomerAccountModel
                                {
                                    AccountNumber = a.AccountNumber,
                                    Currency = new CurrencyModel
                                    {
                                        ID = c.CurrencyID,
                                        Code = c.CurrencyCode,
                                        Description = c.CurrencyDescription
                                    },
                                    IsJointAccount = b.IsJointAccount.HasValue?b.IsJointAccount.Value:false,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    customerAccount.Page = page;
                    customerAccount.Size = size;
                    customerAccount.Total = data.Count();
                    customerAccount.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    customerAccount.Rows = data.OrderBy(orderBy).AsEnumerable().Select((a, i) => new CustomerAccountRow()
                    //customerAccount.Rows = data.AsEnumerable()
                    //.Select((a, i) => new CustomerAccountRow
                    {
                        RowID = i + 1,
                        AccountNumber = a.AccountNumber,
                        Currency = a.Currency,
                        IsJointAccount=a.IsJointAccount,
                        LastModifiedBy = a.LastModifiedBy,
                        LastModifiedDate = a.LastModifiedDate
                    })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }


        public bool GetCustomerAccountMapping(string cif, ref IList<CustomerAccountModel> customerAccounts, ref string message)
        {
            bool isSuccess = false;

            try
            {
                customerAccounts = (from a in context.CustomerAccountMappings
                                    join b in context.CustomerAccounts on a.AccountNumber equals b.AccountNumber
                                    join c in context.Currencies on b.CurrencyID equals c.CurrencyID
                                   where a.IsDeleted.Equals(false) && a.CIF.Equals(cif)
                                   orderby a.AccountNumber
                                   select new CustomerAccountModel
                                   {
                                       AccountNumber = a.AccountNumber,
                                       Currency = new CurrencyModel()
                                       {
                                           ID = b.Currency.CurrencyID,
                                           Code = c.CurrencyCode,
                                           Description = c.CurrencyDescription,
                                           LastModifiedBy = c.UpdateDate == null ? c.CreateBy :c.UpdateBy,
                                           LastModifiedDate = c.UpdateDate == null ? c.CreateDate : c.UpdateDate.Value
                                       },
                                       IsJointAccount = b.IsJointAccount.HasValue ? b.IsJointAccount.Value : false,
                                       LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                       LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                   }).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
    }

  
    #endregion
}