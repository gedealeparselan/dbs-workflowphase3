﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Web;
using DBS.Entity;
using DBS.WebAPI.Models;
using DBS.WebAPI.Helpers;

namespace DBS.WebAPI.Models
{
    #region Model
    public class CustomerPhoneNumberModel
    {
        public string ActionType { get; set; }
        public string CIF { get; set; }
        public string CityCode { get; set; }
        public string ContactType { get; set; }
        public string CountryCode { get; set; }
        public string CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public long CustomerPhoneNumberID { get; set; }
        public string PhoneNumber { get; set; }
        public long RetailCIFCBOContactID { get; set; }
        public long? RetailCIFCBOID { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
    public class CustomerContact2Model
    {
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public string PhoneNumber { get; set; }

        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
    public class CustomerPhoneNumberModelFilter
    {
        public string ActionType { get; set; }
        public string CIF { get; set; }
        public string CityCode { get; set; }
        public string ContactType { get; set; }
        public string CountryCode { get; set; }
        public string CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public string PhoneNumber { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
    public class CustomerPhoneNumberGrid : Grid
    {
        public IList<CustomerPhoneNumberRow> Rows { get; set; }
    }
    public class CustomerPhoneNumberRow : CustomerPhoneNumberModelFilter
    {
        public int RowID { get; set; }
    }
    #endregion

    #region Filter
    public class CustomerPhoneNumberFilter : Filter
    {
        //public string Field { get; set; }
        //public string Value { get; set; }
    }
    #endregion

    public interface ICustomerPhoneNumberRepository : IDisposable
    {
        bool GetCustomerPhoneNumberGrid(string CIF, int page, int size, IList<CustomerPhoneNumberFilter> filters, string sortColumn, string sortOrder, ref CustomerPhoneNumberGrid CustomerPhoneNumber, ref string message);
    }

    public class CustomerPhoneNumberRepository : ICustomerPhoneNumberRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();
        public bool GetCustomerPhoneNumberGrid(string CIF, int page, int size, IList<CustomerPhoneNumberFilter> filters, string sortColumn, string sortOrder, ref CustomerPhoneNumberGrid CustomerPhoneNumber, ref string message)
        {
            bool isSuccess = false;
            try
            {
                int skip = (page - 1) * size;
                string orderBy = string.Format("UpdateDate DESC,{0} {1}", sortColumn, sortOrder);
                CustomerPhoneNumberModelFilter filter = new CustomerPhoneNumberModelFilter();

                if (filters != null)
                {
                    filter.ContactType = filters.Where(a => a.Field.Equals("ContactType")).Select(a => a.Value).SingleOrDefault();
                    filter.CountryCode = filters.Where(a => a.Field.Equals("CountryCode")).Select(a => a.Value).SingleOrDefault();
                    filter.CityCode = filters.Where(a => a.Field.Equals("CityCode")).Select(a => a.Value).SingleOrDefault();
                    filter.PhoneNumber = filters.Where(a => a.Field.Equals("PhoneNumber")).Select(a => a.Value).SingleOrDefault();
                    filter.UpdateBy = filters.Where(a => a.Field.Equals("UpdateBy")).Select(a => a.Value).SingleOrDefault();
                    filter.CreateBy = filters.Where(a => a.Field.Equals("CreateBy")).Select(a => a.Value).SingleOrDefault();
                    filter.UpdateDate = DateTime.Parse(filters.Where(a => a.Field.Equals("UpdateDate")).Select(a => a.Value).SingleOrDefault());
                }
                List<CustomerPhoneNumberModelFilter> PhoneNumberColected = new List<CustomerPhoneNumberModelFilter>();
                var CustomerContactPhonNumberOld = context.CustomerContacts.Where(a => a.CIF == CIF && a.PhoneNumber != null && a.PhoneNumber != "").ToList();

                foreach (var item in CustomerContactPhonNumberOld)
                {

                    if (item.PhoneNumber != null)
                    {
                        CustomerPhoneNumberModelFilter ex = new CustomerPhoneNumberModelFilter();
                        ex.PhoneNumber = item.PhoneNumber;
                        ex.UpdateBy = item.UpdateBy;
                        ex.UpdateDate = item.UpdateDate;
                        ex.CreateBy = item.CreateBy;
                        PhoneNumberColected.Add(ex);
                    }
                }
                List<CustomerPhoneNumber> CPNTemporary = context.CustomerPhoneNumbers.Where(a => a.CIF == CIF).ToList();
                foreach (var item in CPNTemporary)
                {
                    CustomerPhoneNumberModelFilter ex = new CustomerPhoneNumberModelFilter();
                    ex.ContactType = item.ContactType;
                    ex.CountryCode = item.CountryCode;
                    ex.CityCode = item.CityCode;
                    ex.PhoneNumber = item.PhoneNumber;
                    ex.UpdateBy = item.UpdateBy;
                    ex.UpdateDate = item.UpdateDate;
                    ex.CreateBy = item.CreateBy;
                    PhoneNumberColected.Add(ex);
                }

                CustomerPhoneNumber.Page = page;
                CustomerPhoneNumber.Size = size;
                CustomerPhoneNumber.Total = PhoneNumberColected.Count();
                CustomerPhoneNumber.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                CustomerPhoneNumber.Rows = PhoneNumberColected.AsQueryable().OrderBy(orderBy).Select((a, i) => new CustomerPhoneNumberRow
                {
                    RowID = i + 1,
                    ContactType = a.ContactType,
                    CountryCode = a.CountryCode == null || a.CountryCode == "" ? "" : a.CountryCode,
                    CityCode = a.CityCode == null || a.CityCode == "" ? "" : a.CityCode,
                    PhoneNumber = a.PhoneNumber == null || a.PhoneNumber == "" ? "" : a.PhoneNumber,
                    UpdateBy = context.Employees.Where(x => x.EmployeeUsername == a.UpdateBy).Select(x => x.EmployeeName).FirstOrDefault(),
                    UpdateDate = a.UpdateDate,
                    CreateBy = context.Employees.Where(x => x.EmployeeUsername == a.CreateBy).Select(x => x.EmployeeName).FirstOrDefault()
                })
                .Skip(skip)
                .Take(size)
                .Distinct()
                .ToList();

                isSuccess = true;
            }

            catch (Exception e)
            {
                message = e.Message;
            }

            return isSuccess;
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                        context.Dispose();
                }
            }
            this.disposed = true;
        }

        void IDisposable.Dispose()
        {

            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}