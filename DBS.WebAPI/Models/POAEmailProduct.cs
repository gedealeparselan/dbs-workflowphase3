﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using System.Linq.Dynamic;


namespace DBS.WebAPI.Models
{
    #region property

    public class POAEmailModel
    {
        public int POAEmailProductID { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public bool? Show { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateBy { get; set; }
    }

    public class POAEmailModelRow : POAEmailModel
    {
        public int RowID { get; set; }
    }
    public class POAEmailModelGrid : Grid
    {
        public IList<POAEmailModelRow> Rows { get; set; }
    }
    #endregion

    #region Filter
    public class POAEmailModelFilter : Filter { }
    #endregion

    #region interface
    public interface IPOAEmailModel : IDisposable
    {
        bool GetPOAEmailProductByID(int ID, ref POAEmailModel POAEmailProduct, ref string message);
        bool GetPOAEmailProduct(ref IList<POAEmailModel> POAEmailProduct, ref string message);
 //       bool GetPOAEmailProduct(string key, int limit, ref IList<ProvinsiModel> provinsi, ref string message);
        bool GetPOAEmailProduct(int page, int size, IList<POAEmailModelFilter> filters, string sortColumn, string sortOrder, ref POAEmailModelGrid POAEmailProduct, ref string message);
        bool GetPOAEmailProduct(string key, int limit, ref IList<POAEmailModel> banks, ref string message);
        bool AddPOAEmailProduct(POAEmailModel POAEmailProduct, ref string message);

        bool UpdatePOAEmailProduct(int ID, ref POAEmailModel POAEmailProduct, ref string message);

        bool DeletePOAEmailProduct(int id, ref string message);
    }
    #endregion

    #region Repository
    public class POAEmailProductRepository : IPOAEmailModel
    {

        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();
        public bool GetPOAEmailProductByID(int ID, ref POAEmailModel POAEmailProduct, ref string message)
        {
            bool isSuccess = false;

            try
            {
                POAEmailProduct = (from a in context.POAEmailProducts
                        //join b in context.Branches on a.CityCode equals b.CityCode
                        // where a.IsDeleted.Equals(false) && a.CityID.Equals(ID)
                        select new POAEmailModel
                        {
                            POAEmailProductID = a.POAEmailProductID,
                            ProductName = a.ProductName,
                            Description = a.Description,
                            Show = a.Show,
                            UpdateBy = a.UpdateBy,
                            UpdateDate = a.UpdateDate
                        }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetPOAEmailProduct(ref IList<POAEmailModel> POAEmailProduct, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    POAEmailProduct = (from a in context.POAEmailProducts

                            where a.IsDeleted.Equals(false)
                            orderby new { a.POAEmailProductID , a.ProductName, a.Description,a.Show, a.IsDeleted, a.UpdateBy , a.UpdateDate }
                            select new POAEmailModel
                            {
                                POAEmailProductID = a.POAEmailProductID,
                                ProductName = a.ProductName,
                                Description = a.Description,
                                Show = a.Show,
                                UpdateBy = a.UpdateBy,
                                UpdateDate = a.UpdateDate
                            }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

      //public bool GetPOAEmailProduct(ref IList<ProvinsiModel> POAEmailProduct, int limit, int index, ref string message)
      //  {
      //      bool isSuuces = false;
      //      try
      //      {
      //          int skip = limit * index;
      //          using (DBSEntities context = new DBSEntities())
      //          {
      //              POAEmailProduct = (from a in context.Provinces

      //                                 select new ProvinsiModel
      //                                 {
      //                                     ProvinceID = a.ProvinceID,
      //                                     ProvinceCode = a.ProvinceCode,
      //                                     Description = a.Description,

      //                                 }).Skip(skip).Take(limit).ToList();
      //          }
      //      }
      //      catch (Exception ex)
      //      {
      //          message = ex.Message;
      //      }
      //      return isSuuces;
      //  }

        public bool GetPOAEmailProduct(int page, int size, IList<POAEmailModelFilter> filters, string sortColumn, string sortOrder, ref POAEmailModelGrid POAEmailProduct, ref string message)
        {
            bool isSucces = false;
            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                POAEmailModel filter = new POAEmailModel();
                if (filters != null)
                {

                    filter.ProductName = filters.Where(a => a.Field.Equals("ProductName")).Select(a => a.Value).SingleOrDefault();
                    filter.Description = filters.Where(a => a.Field.Equals("Description")).Select(a => a.Value).SingleOrDefault();
                    //filter.Show = filters.Where(a => a.Field.Equals("Show")).Select(a => a.Value).SingleOrDefault();     TODO


                    filter.UpdateBy = filters.Where(a => a.Field.Equals("UpdateBy")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("UpdateDate")).Any())
                    {
                        filter.UpdateDate = DateTime.Parse(filters.Where(a => a.Field.Equals("UpdateDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.UpdateDate.Value.AddDays(1);
                    }
                }
                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.POAEmailProducts
                                let isProductName = !string.IsNullOrEmpty(filter.ProductName)
                                let isDescription = !string.IsNullOrEmpty(filter.Description)
                                //let isShow = !string.IsNullOrEmpty(filter.Show)   TODO
                                let isCreateBy = !string.IsNullOrEmpty(filter.UpdateBy)
                                let isCreateDate = filter.UpdateDate.HasValue ? true : false

                                where a.IsDeleted == false &&


                                 (isProductName ? a.ProductName.Contains(filter.ProductName) : true) &&
                                 (isDescription ? a.Description.Contains(filter.Description) : true) &&  
                                 (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.UpdateBy) : a.UpdateBy.Contains(filter.UpdateBy)) : true) &&
                                 (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.UpdateDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)


                                select new POAEmailModel
                                {
                                    POAEmailProductID = a.POAEmailProductID,
                                    ProductName = a.ProductName,
                                    Description = a.Description,
                                    Show = a.Show,
                                    UpdateBy = a.UpdateBy,
                                    UpdateDate = a.UpdateDate
                                });

                    POAEmailProduct.Page = page;
                    POAEmailProduct.Size = size;
                    POAEmailProduct.Total = data.Count();
                    POAEmailProduct.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    POAEmailProduct.Rows = data.OrderBy(orderBy).AsEnumerable().Select((a, i) => new POAEmailModelRow
                    {
                        RowID = i + 1,
                        POAEmailProductID = a.POAEmailProductID,
                        ProductName = a.ProductName,
                        Description = a.Description,
                        Show = a.Show,
                        UpdateBy = a.UpdateBy,
                        UpdateDate = a.UpdateDate,
                    }).Skip(skip).Take(size).ToList();
                }
                isSucces = true;
            }
            catch (Exception ex)
            {
                message = ex.Message + "; " + ex.InnerException.Message;
            }
            return isSucces;
        }

        public bool AddPOAEmailProduct(POAEmailModel POAEmailProduct, ref string message)
        {
            //throw new NotImplementedException();
            bool isSuccess = false;

            try
            {
                if (!context.POAEmailProducts.Where(a => a.ProductName == POAEmailProduct.ProductName && a.IsDeleted == false).Any())
                {
                    POAEmailProduct item = new POAEmailProduct();

                    item.ProductName = POAEmailProduct.ProductName;
                    item.Description = POAEmailProduct.Description;
                    item.Show = POAEmailProduct.Show;
                    item.IsDeleted = false;
                    item.CreateDate = DateTime.UtcNow;
                    item.CreateBy = currentUser.GetCurrentUser().DisplayName;
                                        context.POAEmailProducts.Add(item);
                    context.SaveChanges();
                    isSuccess = true;


                }
                else
                {
                    message = string.Format("POA Email Product  {0} is already exist.", POAEmailProduct.ProductName);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;

        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        currentUser = null;
                        context.Dispose();
                    }
                }
            }
            this.disposed = true;
        }


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        public bool UpdatePOAEmailProduct(int ID, ref POAEmailModel POAEmailProduct, ref string message)
        {
            Console.WriteLine(currentUser.GetCurrentUser().DisplayName);
            bool isSucces = false;
            try
            {
                Entity.POAEmailProduct data = context.POAEmailProducts.Where(a => a.POAEmailProductID.Equals(ID)).SingleOrDefault();
                if (data != null)
                {

                    data.ProductName = POAEmailProduct.ProductName;
                    data.Description = POAEmailProduct.Description;
                    data.Show = POAEmailProduct.Show;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                    data.UpdateDate = DateTime.UtcNow;
                    context.SaveChanges();
                    isSucces = true;
                }
                else
                {
                    message = string.Format("Data POAEmailProduct {0} is does not exist.", ID);
                }
                isSucces = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSucces;
        }


        public bool DeletePOAEmailProduct(int id, ref string message)
        {
            bool isSucces = false;
            try
            {
                Entity.POAEmailProduct data = context.POAEmailProducts.Where(a => a.POAEmailProductID.Equals(id)).SingleOrDefault();
                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                    context.SaveChanges();
                    isSucces = true;
                }
                else
                {
                    message = string.Format("POAEmailProduct data for FindingEmailTimeID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSucces;
        }


        //public bool GetPOAEmailProduct(string key, int limit, ref IList<ProvinsiModel> provinsi, ref string message)
        //{
        //    bool isSuccess = false;
        //    try
        //    {
        //        provinsi = (from a in context.Provinces
        //                    where a.Description.Contains(key)
        //                    select new ProvinsiModel
        //                    {
        //                        ProvinceID = a.ProvinceID,
        //                        Description = a.Description,
        //                        ProvinceCode = a.ProvinceCode
        //                    }).Take(limit).ToList();

        //        isSuccess = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        message = ex.Message;
        //    }

        //    return isSuccess;
        //}


        public bool GetPOAEmailProduct(string key, int limit, ref IList<POAEmailModel> banks, ref string message)
        {
            string param = key.Trim().ToLower();
            bool isSuccess = false;
            try
            {
                banks = (from a in context.POAEmailProducts
                         where a.ProductName.Trim().ToLower().Contains(param)
                         || a.Description.Trim().ToLower().Contains(param) 
                         //&& a.IsDeleted.Equals(false)
                         //orderby new {a.CityCode, a.Description}

                         select new POAEmailModel
                         {
                             POAEmailProductID = a.POAEmailProductID,
                             ProductName = a.ProductName,
                             Description = a.Description,
                             Show = a.Show,
                         }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
    }
    #endregion
}