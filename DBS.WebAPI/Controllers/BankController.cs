﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class BankController : ApiController
    {
        private string message = string.Empty;

        private readonly IBankRepository repository = null;

        public BankController()
        {
            this.repository = new BankRepository();
        }

        public BankController(IBankRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all currencies.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<BankModel>))]
        public HttpResponseMessage Get()
        {
            IList<BankModel> output = new List<BankModel>();

            if (!repository.GetBank(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<BankModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get currencies with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<BankModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<BankModel> output = new List<BankModel>();

            if (!repository.GetBank(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<BankModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get currencies for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(BankGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<BankFilter> filters)
        {
            BankGrid output = new BankGrid();

            if (!repository.GetBank(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Code" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<BankGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find currencies using criterias Code or Description.
        /// </summary>
        /// <param name="query">Code or Description.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<BankModel>))]
        [HttpGet]
        [Route("api/Bank/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<BankModel> output = new List<BankModel>();

            if (!repository.GetBank(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<BankModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find currencies using criterias Code or Description.
        /// </summary>
        /// <param name="query">Code or Description.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<BankBranchModel>))]
        [HttpGet]
        [Route("api/Bank/SearchDetail")]
        public HttpResponseMessage SearchDetail(string query, int? limit)
        {
            IList<BankBranchModel> output = new List<BankBranchModel>();

            if (!repository.GetBank(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<BankBranchModel>>(HttpStatusCode.OK, output);
            }
        }

        #region IPE
        /// <summary>
        /// Find currencies using criterias Code or Description.
        /// </summary>
        /// <param name="query">Code or Description.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<BankBranchModel>))]
        [HttpGet]
        [Route("api/Bank/SearchDetailIPE")]
        public HttpResponseMessage SearchDetail(string query, int? limit, string name)
        {
            IList<BankBranchModel> output = new List<BankBranchModel>();

            if (!repository.GetBank(query, limit.HasValue ? limit.Value : 10, name, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<BankBranchModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find currencies using criterias Code or Description.
        /// </summary>
        /// <param name="query">Code or Description.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<BankBranchModel>))]
        [HttpGet]
        [Route("api/Bank/SearchDetailIBank")]
        public HttpResponseMessage SearchDetailIBank(int query)
        {
            IList<BankBranchModel> output = new List<BankBranchModel>();

            if (!repository.GetBank(query, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<BankBranchModel>>(HttpStatusCode.OK, output);
            }
        }
        #endregion

        /// <summary>
        /// Get BeneSegment by ID.
        /// </summary>
        /// <param name="id">BeneSegment ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(BankModel))]
        [Route("api/Bank/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            BankModel output = new BankModel();

            if (!repository.GetBankByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<BankModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new BizSegment.
        /// </summary>
        /// <param name="Bank">Cumrrency data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(BankModel bank)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddBank(bank, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New Bank data has been created." });
                }
            }
        }

        /// <summary>
        /// Modify existing BizSegment.
        /// </summary>
        /// <param name="id">ID of BizSegment to be modify.</param>
        /// <param name="customer">BizSegment data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/Bank/{id}")]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(int id, BankModel bank)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateBank(id, bank, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Bank data has been updated." });
                }
            }
        }

        /// <summary>
        /// Remove exisiting Biz Segment.
        /// </summary>
        /// <param name="id">ID of Biz Segment to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/Bank/{id}")]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteBank(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Bank data has been deleted." });
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}