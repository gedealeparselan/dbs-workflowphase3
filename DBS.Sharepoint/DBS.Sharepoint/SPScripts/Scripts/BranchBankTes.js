﻿var BankModel =function(id,BankDescription) {
    ID =  ko.observable(id),
    BankDescription = ko.observable(BankDescription)
};

var CityModel =function(id,description) {
    CityID = ko.observable(id),
    Description =  ko.observable(description)
};

var ViewModel = function () {
    var self = this;

    self.updateCallback = function () { };

    self.SetUpdateCallback = function (callback) {
        self.updateCallback = callback;
    };

    //Properties
    //self.SortColumn = ko.observable();
    //self.SortOrder = ko.observable();
    //self.AllowFilter = ko.observable(false);
    //self.AvailableSizes = ko.observableArray([10, 25, 50, 75, 100]);
    //self.Page = ko.observable(1);
    //self.Size = ko.observableArray([10]);
    //self.Total = ko.observable(0);
    //self.TotalPages = ko.observable(0);

    //bind with ui
    self.BranchID = ko.observable("");
    self.BranchCode = ko.observable("");
    self.Bank = ko.observable(new BankModel('', ''));
    self.BankID = ko.observable("");
    self.BankDescription = ko.observable('');
    self.BankAccount = ko.observable("");
    self.CommonName = ko.observable("");
  
    self.PGSL = ko.observable("");
    self.Cities = ko.observable(new CityModel('', ''));
    self.City = ko.observable('');
    self.CityID = ko.observable("");
    self.CityDescription = ko.observable('');
    
    self.LastModifiedBy = ko.observable("");
    self.LastModifiedDate = ko.observable("");

    self.IsRoleMaker = ko.observable(false);
    self.IsPermited = ko.observable(false);
    self.ApproverId = ko.observable("");
    self.WorkflowInstanceID = ko.observable("");
    self.IsWorkflow = ko.observable(false);

    //Filter
    self.FilterBranchCode = ko.observable("");
    self.FilterBankDescription = ko.observable("");
    self.FilterBankAccount = ko.observable("");
    self.FilterCommonName = ko.observable("");
    self.FilterPGSL = ko.observable("");
    self.FilterCityDescription = ko.observable("");
    self.FilterModifiedBy = ko.observable("");
    self.FilterModifiedDate = ko.observable("");

    // New Data flag
    self.IsNewData = ko.observable(false);
    self.Readonly = ko.observable(false);

    // Declare an ObservableArray for Storing the JSON Response
    self.BranchBanks = ko.observableArray([]);

    // grid properties
    self.GridBranchProperties = ko.observable();
    self.GridBranchProperties(new GridPropertiesModel(GetData));
    self.GridBranchProperties().SortColumn("BranchCode");
    self.GridBranchProperties().SortOrder("DESC");

    // bind get data function to view
    self.GetData = function () {
        GetData();
    };

    // bind clear filters
    self.ClearFilters = function () {

        self.FilterBranchCode("");
        self.FilterBankDescription("");
        self.FilterCommonName("");
        self.FilterPGSL("");
        self.FilterCityDescription("");
        self.FilterModifiedBy("");
        self.FilterModifiedDate("");

        GetData();
    };

    var BranchBank = {
        BranchID: self.BranchID,
        BranchCode: self.BranchCode,
        BankID: self.BankID,
        BankAccount: self.BankAccount,
        CommonName: self.CommonName,
        PGSL: self.PGSL,
        CityID: self.CityID,
        BankDescription: self.BankDescription,
        CityDescription: self.CityDescription,
        LastModifiedBy: self.LastModifiedBy,
        LastModifiedDate: self.LastModifiedDate
    };

    // Get filtered columns value
    function GetFilteredColumns() {
        // define filter
        var filters = [];      
        if (self.FilterBranchCode() != "") filters.push({ Field: 'BranchCode', Value: self.FilterBranchCode() });
        if (self.FilterBankDescription() != "") filters.push({Field: 'BankDescription', value: self.FilterBankDescription()});
        if (self.FilterCommonName() != "") filters.push({ Field: 'CommonName', Value: self.FilterCommonName() });
        if (self.FilterPGSL() != "") filters.push({ Field: 'PGSL', Value: self.FilterPGSL() });
        if (self.FilterCityDescription() != "") filters.push({ Field: 'Description', Value: self.FilterCityDescription() });
        if (self.FilterModifiedBy() != "") filters.push({ Field: 'LastModifiedBy', Value: self.FilterModifiedBy() });
        if (self.FilterModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.FilterModifiedDate() });
        return filters;
    };


    // Local Date
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return "";
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-17
    };

 
    //Function to Read All Data
    function GetData() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.branchbank,
            params: {
                page: self.GridBranchProperties().Page(),
                size: self.GridBranchProperties().Size(),
                sort_column: self.GridBranchProperties().SortColumn(),
                sort_order: self.GridBranchProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);


            Helper.Ajax.Post(options, OnSuccessGetData, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetData, OnError, OnAlways);
        }
    }

    self.GetSelectedRow = function (data) {
        //console.log("select");
       console.log(data);
        self.IsNewData(false);

        if (self.IsWorkflow()) $("#modal-form-workflow").modal('show');
        else $("#modal-form").modal('show');

        self.BranchID(data.BranchID);
        self.BranchCode(data.BranchCode);
        self.BankID(data.BankID);
        self.BankDescription(data.BankDescription);
        self.BankAccount(data.BankAccount);
        self.CommonName(data.CommonName);
        self.PGSL(data.PGSL);
        self.CityID(data.CityID);
        self.CityDescription(data.CityDescription);
        self.LastModifiedBy(data.LastModifiedBy);
        self.LastModifiedDate(data.LastModifiedDate);
    };

    
    //untuk autocomplete Bank   
    $("#BankDescription").autocomplete({
        source: function (request, response) {
            // declare options variable for ajax get request
            var options = {
                url: api.server + api.url.bank + "/Search",
                data: {
                    query: request.term,
                    limit: 20
                },
                token: accessToken
            };

            // exec ajax request
            Helper.Ajax.AutoComplete(options, response, OnSuccessAutoCompleteBank, OnError);
        },
        minLength: 2,
        select: function (event, ui) {
            
            if (ui.item.data != undefined || ui.item.data != null) {
                
                //self.Bank(ko.mapping.toJS(ui.item.data, mapping));
                var data = ko.mapping.toJS(ui.item.data, mapping);
                self.BankID(data.BankID);
                self.BankDescription(data.BankDescription);
                //console.log(data);
                var mapping = {
                    'ignore': ["LastModifiedDate", "LastModifiedBy"]
                };
                


            }
            else
                self.Bank(new BankModel(null, null));
           
        }
    });
    
    //untuk autocomplete City   
    $("#CityDescription").autocomplete({
        source: function (request, response) {
            // declare options variable for ajax get request
            var options = {
                url: api.server + api.url.city + "/SearchDetail",
                data: {
                    query: request.term,
                    limit: 20
                },
                token: accessToken
            };

            // exec ajax request
            Helper.Ajax.AutoComplete(options, response, OnSuccessAutoCompleteCity, OnError);
        },
        minLength: 2,
        select: function (event, ui) {

            if (ui.item.data != undefined || ui.item.data != null) {

                //self.City(ko.mapping.toJS(ui.item.data, mapping));
                var data = ko.mapping.toJS(ui.item.data, mapping);
                self.CityID(data.CityID);
                self.CityDescription(data.Description)
                var mapping = {
                    'ignore': ["LastModifiedDate", "LastModifiedBy"]
                };



            }
            else
                self.City(new CityModel(null, null));

        }
    });

    // On Success Autocomplete Bank
    function OnSuccessAutoCompleteBank(response, data, textStatus, jqXHR) {
        response($.map(data, function (item) {
           
            return {

                // default autocomplete object
                label: item.Code + " - " + item.Description,
                value: item.Description,

                // custom object binding
                data: {
                    BankID: item.ID,
                    BankDescription: item.Description                   
                }
            }
        })
         );
    }

    // On Success Autocomplete City
    function OnSuccessAutoCompleteCity(response, data, textStatus, jqXHR) {
        response($.map(data, function (item) {
           
            return {
                
                label: item.CityCode + " - " + item.Description,
                value: item.Description,

                data: {
                    CityID: item.CityID,
                    Description: item.Description
                }
            }
        })
         );
    }

    //insert new
    self.NewData = function () {
       
        self.IsNewData(true);
        self.Readonly(false);
        self.BranchID(0);
        self.BranchCode('');
        self.BankID();
        self.BankDescription('');
        self.BankAccount('');
        self.CommonName('');
        self.PGSL('');
        self.CityID();
        self.CityDescription('');

        $(':checkbox').prop('checked', false);
    };

    self.save = function () {
        // validation
        var form = $("#aspnetForm");
        //var CurrencySave = $('#currencyhidden option:selected').val();
        form.validate();

        if (form.valid()) {
            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {
                    $.ajax({
                        type: "POST",
                        url: api.server + api.url.branchbank,
                        data: ko.toJSON(BranchBank), //Convert the Observable Data into JSON
                        
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {
                                // send notification
                                ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // hide current popup window
                                $("#modal-form").modal('hide');

                                // refresh data
                                GetData();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });
                }
            });
        }
    };

    self.update = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {

            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {
                    //Ajax call to update the Customer
                    //console.log(ko.toJSON(BranchBank));
                    //return;
                    $.ajax({
                        type: "PUT",
                        url: api.server + api.url.branchbank + "/" + BranchBank.BranchID(),
                        data: ko.toJSON(BranchBank),
                        
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {
                                // send notification
                                ShowNotification('Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // hide current popup window
                                $("#modal-form").modal('hide');

                                // refresh data
                                GetData();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });
                }
            });
        }
    };

    self.delete = function () {
        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                $("#modal-form").modal('show');
            } else {
               // Bank.Currency(CurrencyDelete);
                //Ajax call to delete the Customer
                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.branchbank + "/" + BranchBank.BranchID(),
                    data: ko.toJSON(BranchBank),
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if (jqXHR.status = 200) {
                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                            // refresh data
                            GetData();
                        } else
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
            }
        });
    };

    function OnSuccessGetData(data, textStatus, jqXHR) {
        //console.log('awal');
        if (jqXHR.status = 200) {
            self.BranchBanks(data.Rows);
          //  console.log(self.BranchBanks());
            self.GridBranchProperties().Page(data['Page']);
            self.GridBranchProperties().Size(data['Size']);
            self.GridBranchProperties().Total(data['Total']);
            self.GridBranchProperties().TotalPages(Math.ceil(self.GridBranchProperties().Total() / self.GridBranchProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    // On Error callback
    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    // On Always callback
    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }

    self.IsPermited = function (IsPermitedResult) {
        //Ajax call to delete the Customer
        spUser = $.cookie(api.cookie.spUser);

        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/CheckUserPermited/" + _spFriendlyUrlPageContextInfo.title,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    //compare user role to page permission to get checker validation
                    console.log("spUser Roles : " + ko.toJSON(spUser.Roles));
                    console.log("spUser Permited : " + data);
                    for (i = 0; i < spUser.Roles.length; i++) {
                        for (j = 0; j < data.length; j++) {
                            if (Const_RoleName[spUser.Roles[i].ID] == data[j]) { //if (spUser.Roles[i].Name == data[j]) {
                                if (Const_RoleName[spUser.Roles[i].ID].endsWith('Maker')) { //if (spUser.Roles[i].Name.endsWith('Maker')) {
                                    self.IsRoleMaker(true);
                                    return;
                                }
                                else {
                                    self.IsRoleMaker(false);
                                }
                            }
                        }
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }

    });
}