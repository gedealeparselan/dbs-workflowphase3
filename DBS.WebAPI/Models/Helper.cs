﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace DBS.WebAPI.Models
{
    #region Constant
    public enum DBSProductID
    {
        #region Production/UAT
        RTGSProductIDCons = 1,
        SKNProductIDCons = 2,
        OTTProductIDCons = 3,
        IDInvestmentProductIDCons = 7,
        SavingPlanProductIDCons = 8, //ID Product Saving Plan pd tb  Master Product di sesuaikan dengan ID di UAT
        UTOnshoreproductIDCons = 9, //ID Product UT On Shore pd tb  Master Product di sesuaikan dengan ID di UAT
        UTOffshoreProductIDCons = 10, //ID Product Off Shore pd tb  Master Product di sesuaikan dengan ID di UAT
        UTCPFProductIDCons = 11, //ID Product UT CPF pd tb  Master Product di sesuaikan dengan ID di UAT
        FDProductIDCons = 12,
        CollateralProductIDCons = 13,
        CIFProductIDCons = 14,
        LoanDisbursmentProductIDCons = 15,
        LoanRolloverProductIDCons = 16,
        LoanIMProductIDCons = 17,
        LoanSettlementProductIDCons = 18,
        TMOProductIDCons = 19,
        OverbookingProductIDCons = 21, //sesuai uat
        FXProductIDCons = 46,
        SKNBulkProductIDCons = 22,
        DFProductIDCons = 23
        #endregion

        #region Dev
        //RTGSProductIDCons = 1,
        //SKNProductIDCons = 2,
        //OTTProductIDCons = 3,
        //IDInvestmentProductIDCons = 12,
        //SavingPlanProductIDCons = 13,
        //UTOnshoreproductIDCons = 14,
        //UTOffshoreProductIDCons = 15,
        //UTCPFProductIDCons = 16,
        //FDProductIDCons = 17,
        //CollateralProductIDCons = 18,
        //CIFProductIDCons = 19,
        //LoanDisbursmentProductIDCons = 20,
        //LoanRolloverProductIDCons = 21,
        //LoanIMProductIDCons = 22,
        //LoanSettlementProductIDCons = 1014,
        //TMOProductIDCons = 1013,
        //OverbookingProductIDCons = 1015,
        //FXProductIDCons = 46
        #endregion
    }
    public enum ConsTransactionType
    {
        fdNewPlacement = 13,
        fdPrematurebreak = 14,
        fdBreakmaturity = 15,
        fdChangeInstruction = 16,
        fdMaintenance = 17
    }
    public enum ConsRequestType
    {
    }
    public enum UTOperative
    {
        offshore = 1,
        onshore = 2,
        cpf = 3
    }
    public enum ConsCIFMaintenanceType
    {
        PengkinianDataCons = 1,
        UpdateFXTierCons = 2,
        AtmCardCons = 3,
        RiskRatingCons = 4,
        AddCurrencyCons = 5,
        AdditionalAccountCons = 6,
        LinkFFDCons = 7,
        FreezeUnCons = 8,
        ActiveDormantCons = 9,
        LPSCons = 10,
        LOIPOIPOACons = 11,
        ChangeRMCons = 12,
        ChangeDispatchCons = 13,
        ActiveHPSPCons = 14,
        ClosureCIF = 15,
        UnsuspendCIFCons = 16,
        NewStandingCons = 17
    }
    public class CONSparsys
    {
        public string clTypeDoc = "CL_TYPE_OF_DOCUMENT";
        public string clCollateralStatus = "CL_COLLATERAL_STATUS";
        public string clLoanStatus = "CL_LOAN_STATUS";
        public string fdCuttOff = "FD_CUTTOFF";
        public string fdRemarks = "FD_REMARKS";
        public string utCuttOffIdInvestment = "UT_CUTOFF_IDINVESTMENT";
        public string utCuttOffSP = "UT_CUTOFF_SAVINGPLAN";
        public string utCuttoffUT = "UT_CUTOFF_UTPRODUCT";
        public string utBringupSP = "UT_BRINGUP_SAVINGPLAN";
        public string utBringupUT = "UT_BRINGUP_UTPRODUCT";
        public string utMaxRisk = "UT_MAXIMUMRISK";
        public string utMinRisk = "UT_MINIMUM_RISK";
        public string utFNACore = "UT_FNACORE";
        public string utFunctionType = "UT_FUNCTION_TYPE";
        public string utAccType = "UT_ACCOUNT_TYPE";
        public string cifCuttof = "CIF_CUTOFF";
        public string cifBringup = "CIF_BRINGUP";
        public string cifMaintenanceType = "CIF_MAINTENANCE_TYPE";
        public string cifReasonCallback = "CIF_REASON_CALLBACK";
        public string cifJenisIdentitas = "CIF_JENIS_IDENTITAS";
        public string cifSumberDana = "CIF_SUMBER_DANA";
        public string cifAssetBersih = "CIF_ASSET_BERSIH";
        public string cifPendapatanBulanan = "CIF_PENDAPATAN_BULANAN";
        public string cifPenghasilanTambahan = "CIF_PENGHASILAN_TAMBAHAN";
        public string cifPekerjaan = "CIF_PEKERJAAN";
        public string cifTujuanPembukaanRek = "CIF_TUJUAN_PEMBUKAAN_REKENING";
        public string cifPerkiraanDanaKeluar = "CIF_PERKIRAAN_DANA_KELUAR";
        public string cifPerkiraanTransKeluar = "CIF_PERKIRAAN_TRANSAKSI_KELUAR";
        public string cifStaffTagging = "CIF_STAFF_TAGGING";
        public string tmoMaxUnderlying = "TMO_MAX_DAY_UNDERLYING";
        public string tmoMaxStatement = "TMO_MAX_DAY_STATEMENT";
        public string tmoResBuy = "TMO_RESIDENT_BUY_THRESHOLD";
        public string tmoResSell = "TMO_RESIDENT_SELL_THRESHOLD";
        public string tmoNonResBuy = "TMO_NONRESIDENT_BUY_THRESHOLD";
        public string tmoNonResSell = "TMO_NONRESIDENT_SELL_THRESHOLD";
        public string loanCustCategory = "LOAN_CUSTOMER_CATEGORY";
        public string loanType = "LOAN_TYPE";
        public string loanProgramType = "LOAN_PROGRAM_TYPE";
        public string loanSchemeCode = "LOAN_SCHEME_CODE";
        public string loanInterestCode = "LOAN_INTEREST_RATE_CODE";
        public string loanrepricingPlan = "LOAN_REPRICING_PLAN";
        public string loanPrincipalFreq = "LOAN_PRINCIPAL_FREQ";
        public string loanInterestFreq = "LOAN_INTEREST_FREQ";
        public string loanPeggingFreq = "LOAN_PEGGING_FREQ";
        public string loanBringup = "LOAN_BRINGUP";
        public string loanMaintenanceType = "LOAN_MAINTENANCE_TYPE";
    }
    public enum StateID
    {
        OnProgress = 1,
        Completed = 2,
        Cancelled = 3
    }
    public class Util
    {
        public static string ExistingDoctype = "spitemdoc";
    }
    public enum ConsFunctionType
    {
        UTAdd = 19,
        utCPFSub = 10,
        utOffSub = 7,
        utOnSub = 4
    }
    public enum UTSwitching
    {
        onShore = 6,
        ofShore = 9,
        cpf = 12
    }
    public enum CurrencyID
    {
        NullCurrency = 25
    }
    #endregion

    #region Property
    [ModelName("Helper")]
    public class REFModel
    {
        public long ID { get; set; }
    }


    public class StatementModel
    {
        public bool IsStatementA { get; set; }
    }

    public class TZNumberModel
    {
        public int ID { get; set; }
        public string TZNumber { get; set; }
        public string Message { get; set; }
        public string Class { get; set; }
    }

    public class TZReferenceModel
    {
        #region basri
        public long ID { get; set; }
        #endregion
        public string TZReference { get; set; }
        public string CIF { get; set; }
        public string CustomerName { get; set; }
        #region basri
        public string MurexNumber { get; set; }
        public string SwapDealNumber { get; set; }
        public DateTime? ExpectedDateStatementLetter { get; set; }
        public DateTime? ExpectedDateUnderlying { get; set; }
        public DateTime? ActualDateStatementLetter { get; set; }
        public DateTime? ActualDateUnderlying { get; set; }
        #endregion
    }

    public class Users
    {
        public string Email { get; set; }
        public string UserName { get; set; }
        public string DisplayName { get; set; }
        public string isMapped { get; set; }
        public string PhysicalDeliveryOfficeName { get; set; }
        public string Department { get; set; }
        public string Title { get; set; }
    }
    public class TotalPPUModel
    {
        public decimal RateIDR { get; set; }
        public decimal TreshHold { get; set; }
        public decimal FCYIDRTrashHold { get; set; }
        public TotalPPUDetailModel Total { get; set; }
    }
    public class TotalDealModel
    {
        public decimal RateIDR { get; set; }
        public decimal TreshHold { get; set; }
        public decimal FCYIDRTrashHold { get; set; }
        public decimal AvailableUnderlying { get; set; }
        public TotalDealDetailModel Total { get; set; }
    }

    public class TotalDealDetailModel
    {
        public decimal IDRFCYDeal { get; set; }
        public decimal RemainingBalance { get; set; }
        public decimal UtilizationDeal { get; set; }
    }

    public class TotalPPUDetailModel
    {
        public decimal IDRFCYPPU { get; set; }
        public decimal RemainingBalance { get; set; }
        public decimal UtilizationPPU { get; set; }
    }
    public class ParameterModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    public class StatementAStatusModel
    {
        public string CIF { get; set; }
        public string AccountNumber { get; set; }
    }
    public class DropdownListsModel
    {
        public int ID { get; set; }
        public string CatagoryName { get; set; }
        public string Name { get; set; }
        public string Itemvalue { get; set; }
        public string Modulename { get; set; }
    }
    public class ThresholdModel
    {
        public bool? IsHitThreshold { get; set; }
        public decimal ThresholdValue { get; set; }
        public decimal RoundingValue { get; set; }
        public string TransactionType { get; set; }
        public string ResidentType { get; set; }
    }

    public class TotalTransactionModel
    {
        // total IDR-FCY
        public decimal TotalTransaction { get; set; }
        public decimal TotalUtilizationAmount { get; set; }
        public decimal TotalUtilizationDeal { get; set; }
        public decimal AvailableUnderlyingAmount { get; set; }
        public decimal RoundingValue { get; set; }
        public string ThresholdType { get; set; }
        public decimal ThresholdValue { get; set; }

    }

    public class ParamThresholdModel
    {
        public int ProductTypeID { get; set; }
        public string DebitCurrencyCode { get; set; }
        public string TransactionCurrencyCode { get; set; }
        public bool IsResident { get; set; }
        public decimal AmountUSD { get; set; }
        public string CIF { get; set; }
        public string AccountNumber { get; set; }
        public bool IsJointAccount { get; set; }
        public int? StatementLetterID { get; set; }

        public string TZReference { get; set; }
    }
    public class ParamThresholdFXModel
    {
        public string TzRef { get; set; }
        public int StatementLetterID { get; set; }
        public int ProductTypeID { get; set; }
        public string DebitCurrencyCode { get; set; }
        public string TransactionCurrencyCode { get; set; }
        public bool IsResident { get; set; }
        public decimal AmountUSD { get; set; }
        public string CIF { get; set; }
        public string AccountNumber { get; set; }
        public bool IsJointAccount { get; set; }
    }
    public class ThresholdParameterModel
    {
        public decimal RupiahRate { get; set; }
        public decimal ThresholdBuy { get; set; }
        public decimal ThresholdSell { get; set; }
    }

    public class SwapModel
    {
        public string DFNumber { get; set; }
        public string DFSwapNumber { get; set; }
        public int SwapType { get; set; }
        public long SwapTransactionID { get; set; }

    }

    public class IsNettingModel
    {
        public string DFNumber { get; set; }
        public string ApplicationID { get; set; }
        public long TransactionDealID { get; set; }

    }

    #endregion

    #region Interface
    public interface IHelperRepository : IDisposable
    {
        bool GetRefID(string cif, ref REFModel refID, ref string message);
        bool GetStatementA(string cif, string accountNumber, bool isJointAccount, ref StatementModel status, ref string message);
        bool GetAmountUSD(int currencyId, decimal amount, ref decimal uSDAmount, ref string message);
        bool GetAvailableStatementB(string Cif, ref decimal output, ref string message);
        bool GetAmountUSDStatementB(string Cif, ref decimal output, ref string message);
        bool GetIsIDR(ref bool isIDR, string cif, ref string message);
        bool GetUserAD(string username, int limit, ref IList<Users> lstADUsers, ref string message);
        bool GetTreshHold(ref decimal output, ref string message);
        bool GetFCYIDRTrashHold(ref decimal output, ref string message);
        bool GetTotalIDRFCYDeal(ref decimal output, string cif, ref string message);
        bool GetTotalIDRFCYPPU(ref decimal output, string cif, ref string message);
        bool GetTotalUtilizationDeal(ref decimal output, string cif, ref string message);
        bool GetTotalUtilizationPPU(ref decimal output, string cif, ref string message);
        bool GetTotalAllIDRFCY(ref decimal output, string cif, ref string message);
        bool GetTotalIDRFCYDealStatementB(ref decimal output, string cif, ref string message);
        bool CheckTZNumberAvailable(string TZNumber, ref TZNumberModel output, ref string message);
        bool CheckRefNumberAvailable(string RefNumber, ref bool output, ref string message);
        bool IsUserCurrentAccess(ref IList<string> userRoles, string MenuName, ref string message);
        bool CheckTransactionalHandling(ref List<string> output, TransactionCheckerDataModel checker, long? transactionid, ref string message);
        bool GetTZRef(string key, int limit, ref IList<TZReferenceModel> TZRef, ref string message);
        bool GetDropdownLists(ref IList<DropdownListsModel> dropdownLits, ref string message);
        bool IsAnnualStatement(ref bool output, string cif, ref string message);
        bool IsAlreadyTZNumberPPU(string TZNumber, ref bool output, ref string message);

        #region basri
        bool GetCompleteTZRef(string key, int limit, string cif, ref IList<TZReferenceModel> TZRef, ref string message);
        bool GetCompleteTZRefTMO(string key, int limit, string cif, ref IList<TZReferenceModel> TZRef, ref string message);
        bool GetIncompleteTZRef(string key, int limit, string cif, ref IList<TZReferenceModel> TZRef, ref string message);
        bool GetDetailTZRef(string cif, string TZReference, string MurexNumber, ref TZTransactionModel TZRefInfo, ref string message);
        bool GetLoanSettlementType(long transactionid, ref bool isScheduledSettlementLoan, ref string message);
        bool GetCIFFromUploadedData(IList<string> arrCIF, ref string CIF, ref string message);
        #endregion
        #region PBI Mode
        bool GetThresholdValue(ParamThresholdModel param, ref ThresholdModel threshold, ref string message);
        bool GetThresholdFXValue(ParamThresholdFXModel param, ref ThresholdModel threshold, ref string message);
        bool GetTotalIDRFCY(ParamThresholdModel param, ref TotalTransactionModel threshold, ref string message);
        bool GetThresholdParameter(ref ThresholdParameterModel threshold, ref string message);
        bool GetSwapTransaction(string MurexNumber, string dfNumber, ref SwapModel output, ref string message);
        bool GetNettingTransaction(string dfNumber, ref SwapModel output, ref string message);
        bool GetTMONettingTransaction(string dfNumber, ref SwapModel output, ref string message);

        #endregion
    }
    #endregion

    #region Repository
    public class HelperRepository : IHelperRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetRefID(string cif, ref REFModel refNumber, ref string message)
        {
            bool isSuccess = false;

            try
            {
                refNumber.ID = context.CustomerUnderlyings.Where(x => x.CIF.Equals(cif)).Count();
                long DraftID = context.CustomerUnderlyingDrafts.Where(x => x.CIF.Equals(cif) && x.IsDeleted.Equals(false)).Count();
                refNumber.ID += 1 + DraftID;
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetStatementA(string cif, string accountNumber, bool isJointAccount, ref StatementModel status, ref string message)
        {
            bool isSuccess = false;
            try
            {
                int data = 0;
                status.IsStatementA = false;
                DateTime curDate = DateTime.Now;
                if (isJointAccount)
                {
                    data = context.CustomerUnderlyings.Where(a => a.ExpiredDate >= curDate && a.StatementLetterID == 1 && a.AccountNumber.Equals(accountNumber) && a.IsDeleted.Equals(false)).Count();
                }
                else
                {
                    data = context.CustomerUnderlyings.Where(a => a.ExpiredDate >= curDate && a.StatementLetterID == 1 && a.CIF.Equals(cif) && a.AccountNumber != null && a.AccountNumber != "" && a.IsDeleted.Equals(false)).Count();

                }

                if (data > 0)
                {
                    status.IsStatementA = true;
                }
                else
                {
                    status.IsStatementA = false;
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool GetAvailableStatementB(string Cif, ref decimal output, ref string message)
        {
            bool isSuccess = false;
            output = 0;
            try
            {
                DateTime currenDate = DateTime.Now;
                var result = context.CustomerUnderlyings.Where(x => x.CIF.Equals(Cif)
                    //&& x.CreateDate.Month == currenDate.Month && x.CreateDate.Year == currenDate.Year 
                    && currenDate <= x.ExpiredDate
                    && x.StatementLetterID == 2 && x.IsDeleted.Equals(false)).GroupBy(x => x.CIF)
                    .Select(g => new { Id = g.Key, Sum = g.Sum(x => x.AvailableAmountUSD) }).SingleOrDefault();
                if (result != null)
                {
                    output = result.Sum ?? 0.0M;
                }
                else
                {
                    output = 0;
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool GetAmountUSDStatementB(string Cif, ref decimal output, ref string message)
        {
            bool isSuccess = false;
            output = 0;
            try
            {
                DateTime currenDate = DateTime.Now;
                var result = context.CustomerUnderlyings.Where(x => x.CIF.Equals(Cif)
                    && currenDate <= x.ExpiredDate
                    && x.StatementLetterID == 2 && x.IsDeleted.Equals(false)).GroupBy(x => x.CIF)
                    .Select(g => new { Id = g.Key, Sum = g.Sum(x => x.AmountUSD) }).SingleOrDefault();
                if (result != null)
                {
                    output = result.Sum ?? 0.0M;
                }
                else
                {
                    output = 0;
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool CheckTZNumberAvailable(string TZNumber, ref TZNumberModel output, ref string message)
        {
            bool isSuccess = false;
            try
            {
                output.TZNumber = TZNumber;

                var returnTZ = context.TransactionDeals.Where(x => x.TZRef.Equals(TZNumber)).FirstOrDefault();
                if (returnTZ != null)
                {
                    bool isTransaction = returnTZ.WorkflowInstanceID.HasValue;
                    if (isTransaction)
                    {
                        output.ID = 1;
                        output.Message = "TZ Number already exist in transaction!";
                        output.Class = "help-block col-xs-12 col-sm-reset inline red";
                    }
                    else
                    {
                        output.ID = 2;
                        output.Message = "TZ Number available on transaction!";
                        output.Class = "help-block col-xs-12 col-sm-reset inline blue";
                    }
                }
                else
                {
                    output.ID = 3;
                    output.Message = "TZ Number available to use!";
                    output.Class = "help-block col-xs-12 col-sm-reset inline green";
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool CheckRefNumberAvailable(string RefNumber, ref bool output, ref string message)
        {
            bool isSuccess = false;
            try
            {
                output = context.TransactionDeals.Where(x => x.TZRef.Equals(RefNumber)).Any();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool IsAlreadyTZNumberPPU(string TZNumber, ref bool output, ref string message)
        {
            bool isSuccess = false;
            try
            {
                output = context.Transactions.Where(x => x.TZNumber.Equals(TZNumber) && (x.Transaction_Status.ToLower() != "canceled" && x.Transaction_Status.ToLower() != "canceled by ipe")).Any();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool GetAmountUSD(int currencyId, decimal amount, ref decimal uSDAmount, ref string message)
        {
            bool isSuccess = false;
            try
            {

                var data = (from a in context.Currencies
                            where a.CurrencyID.Equals(currencyId)
                            orderby a.CurrencyID
                            select new
                            {
                                ID = a.CurrencyID,
                                RupahRate = a.RupiahRate,
                                USDAmount = (amount * a.RupiahRate) / context.Currencies.Where(x => x.CurrencyID == 1).Select(z => z.RupiahRate).FirstOrDefault()
                            }).SingleOrDefault();
                if (data != null)
                {
                    uSDAmount = data.USDAmount;
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool GetIsIDR(ref bool isIDR, string cif, ref string message)
        {
            bool isSuccess = false;
            isIDR = false;
            try
            {
                var data = context.CustomerAccounts.Where(a => a.CIF.Equals(cif) && a.IsDeleted.Equals(false) && a.CurrencyID.Equals(13)).SingleOrDefault();
                if (data != null)
                {
                    isIDR = true;
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool GetUserAD(string username, int limit, ref IList<Users> lstADUsers, ref string message)
        {
            bool isSuccess = false;

            DataTable dt = new DataTable();

            try
            {
                using (SqlConnection con =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["DBSEntitiesSP"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        //Execute Stored Procedure
                        cmd.CommandText = string.Format("exec [dbo].[GetUserFromADByLoginName] '{0}'", username);

                        using (SqlDataAdapter sda = new SqlDataAdapter())
                        {
                            cmd.Connection = con;
                            sda.SelectCommand = cmd;
                            using (DataSet ds = new DataSet())
                            {
                                dt = new DataTable();
                                sda.Fill(dt);
                            }
                        }
                    }
                }

                foreach (DataRow row in dt.Rows)
                {
                    Users user = new Users();
                    user.DisplayName = row["displayName"].ToString();
                    user.UserName = row["SAMAccountName"].ToString();
                    user.Email = row["mail"].ToString();

                    string Department = row["department"].ToString();
                    if (!string.IsNullOrEmpty(Department))
                    {
                        var Segment = context.BizSegments.Where(x => x.BizSegmentDescription.Equals(Department)).Select(y => y).FirstOrDefault();
                        if (Segment != null)
                        {
                            user.Department = Segment.BizSegmentID.ToString();
                        }
                    }

                    string PhysicalDeliveryOfficeName = row["physicalDeliveryOfficeName"].ToString();
                    if (!string.IsNullOrEmpty(PhysicalDeliveryOfficeName))
                    {
                        var Branch = context.Locations.Where(x => x.LocationName.Equals(PhysicalDeliveryOfficeName)).Select(y => y).FirstOrDefault();
                        if (Branch != null)
                        {
                            user.PhysicalDeliveryOfficeName = Branch.LocationID.ToString();
                        }
                    }

                    string Title = row["title"].ToString();
                    if (!string.IsNullOrEmpty(Title))
                    {
                        var Rank = context.Ranks.Where(x => x.RankDescription.Equals(Title)).Select(y => y).FirstOrDefault();
                        if (Rank != null)
                        {
                            user.Title = Rank.RankID.ToString();
                        }
                    }

                    lstADUsers.Add(user);
                }


                isSuccess = true;

                #region oldcode
                //List<Users> lstADUsers = new List<Users>();

                //string DCFullyQualifiedPath = ConfigurationManager.AppSettings["DCFullyQualifiedPath"].ToString(); 
                //DirectoryEntry dirEntry = new DirectoryEntry(DCFullyQualifiedPath);

                //DirectorySearcher search = new DirectorySearcher(dirEntry);

                //search.Filter = "(&(objectClass=user)(objectCategory=person))";
                //search.PropertiesToLoad.Add("samaccountname");
                //search.PropertiesToLoad.Add("mail");
                //search.PropertiesToLoad.Add("usergroup");
                //search.PropertiesToLoad.Add("displayname");

                //SearchResult result;
                //SearchResultCollection resultCol = search.FindAll();

                //if (resultCol != null)
                //{
                //    for (int i = 0; i < resultCol.Count; i++)
                //    {
                //        string UsernameEmailString = string.Empty;
                //        result = resultCol[i];

                //        if (result.Properties.Contains("samaccountname"))
                //        {
                //            if (result.Properties["samaccountname"][0].ToString().ToLower().Contains(username.ToLower()))
                //            {
                //                //Console.WriteLine((string)result.Properties["samaccountname"][0]);
                //                //Console.WriteLine((string)result.Properties["displayname"][0]);
                //                //Console.WriteLine((string)result.Properties["mail"][0] + "^" + (string)result.Properties["displayname"][0]);

                //                Users objSurveyUsers = new Users();

                //                try { objSurveyUsers.UserName = (string)result.Properties["samaccountname"][0]; }
                //                catch (Exception) { objSurveyUsers.UserName = string.Empty; }

                //                try { objSurveyUsers.DisplayName = (string)result.Properties["displayname"][0]; }
                //                catch (Exception) { objSurveyUsers.DisplayName = string.Empty; }

                //                try { objSurveyUsers.Email = (string)result.Properties["mail"][0]; }
                //                catch (Exception) { objSurveyUsers.Email = string.Empty; }

                //                lstADUsers.Add(objSurveyUsers);
                //            }
                //        }
                //    }
                //}
                #endregion
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        #region FX Calculate Already Remark
        public bool GetTreshHold(ref decimal output, ref string message)
        {
            bool isSuccess = false;
            try
            {
                var value = (from a in context.Configs
                             where a.Parameter == "FX_TRANSACTION_TRESHOLD"
                             select a.Value).FirstOrDefault();
                if (!decimal.TryParse(value, out output))
                {
                    output = 0;
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool GetFCYIDRTrashHold(ref decimal output, ref string message)
        {
            bool isSuccess = false;
            try
            {
                var value = (from a in context.Configs
                             where a.Parameter == "FCY_IDR_TRANSACTION_TRESHOLD"
                             select a.Value).FirstOrDefault();
                if (!decimal.TryParse(value, out output))
                {
                    output = 0;
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool GetTotalIDRFCYDeal(ref decimal output, string cif, ref string message)
        {
            bool isSuccess = false;
            try
            {

                DateTime currenDate = DateTime.UtcNow;
                output = (from a in context.TransactionDeals
                          where a.CIF == cif && a.TradeDate.Value.Month == currenDate.Month && a.TradeDate.Value.Year == currenDate.Year && a.IsFxTransaction.Value == true
                         && (a.IsCanceled.Value == false || a.IsCanceled == null || (a.TradeDate.Value.Day == currenDate.Day && (a.IsTMO.Value == false || a.IsTMO == null)))
                          //&& (a.IsCanceled == true || a.TradeDate.Value.Day == currenDate.Day)
                          select new { Amount = a.AmountUSD }).Sum(o => (decimal?)o.Amount) ?? 0.0M;

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool GetTotalIDRFCYDealStatementB(ref decimal output, string cif, ref string message)
        {
            bool isSuccess = false;
            try
            {
                DateTime currenDate = DateTime.Now;
                output = (from a in context.TransactionDeals
                          where a.CIF == cif
                          && a.TradeDate.Value.Month == currenDate.Month
                          && a.TradeDate.Value.Year == currenDate.Year
                          && a.IsFxTransaction == true
                          && a.StatementLetter.StatementLetterID == 2
                          && (a.IsCanceled.Value == false || a.IsCanceled == null || a.TradeDate.Value.Day == currenDate.Day)
                              //&& (a.IsCanceled == true || a.TradeDate.Value.Day == currenDate.Day)
                          && a.CurrencyID != 13 //IDR
                          select new { Amount = a.AmountUSD }).Sum(o => (decimal?)o.Amount) ?? 0.0M;

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool GetTotalIDRFCYPPU(ref decimal output, string cif, ref string message)
        {
            bool isSuccess = false;
            try
            {
                DateTime currenDate = DateTime.Now;
                output = (from a in context.TransactionDeals
                          where a.CIF == cif && a.TradeDate.Value.Month == currenDate.Month && a.TradeDate.Value.Year == currenDate.Year && a.IsFxTransaction == true && (a.IsCanceled.Value == false || a.IsCanceled == null)
                          select new { Amount = a.AmountUSD }).Sum(o => (decimal?)o.Amount) ?? 0.0M;

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool GetTotalUtilizationDeal(ref decimal output, string cif, ref string message)
        {
            bool isSuccess = false;
            try
            {
                DateTime currenDate = DateTime.Now;
                output = (from a in context.TransactionDeals
                          where a.CIF == cif && a.CreatedDate.Month == currenDate.Month && a.CreatedDate.Year == currenDate.Year && a.IsFxTransaction == true
                           && (a.IsCanceled.Value == false || a.IsCanceled == null || a.TradeDate.Value.Day == currenDate.Day) &&
                           (a.IsCanceled == true || a.TradeDate.Value.Day == currenDate.Day)
                          select new { Amount = a.UtilizedAmount }).Sum(o => (decimal?)o.Amount) ?? 0.0M;

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool GetTotalUtilizationPPU(ref decimal output, string cif, ref string message)
        {
            bool isSuccess = false;
            try
            {
                DateTime currenDate = DateTime.Now;
                output = (from a in context.Transactions
                          join b in context.CustomerAccounts on a.AccountNumber equals b.AccountNumber
                          where a.CIF == cif && a.CurrencyID != 13 && b.CurrencyID == 13 && a.CreateDate.Month == currenDate.Month && a.CreateDate.Year == currenDate.Year
                          select new { amount = a.AmountUSD }).Sum(o => (decimal?)o.amount) ?? 0.0M;
                if (output == null)
                {
                    output = 0;
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool GetTotalAllIDRFCY(ref decimal output, string cif, ref string message)
        {
            bool isSuccess = false;
            try
            {
                DateTime currenDate = DateTime.Now;
                output = (from a in context.Transactions
                          join b in context.CustomerAccounts on a.AccountNumber equals b.AccountNumber
                          where a.CIF == cif && a.CurrencyID != 13 && b.CurrencyID == 13 && a.CreateDate.Month == currenDate.Month && a.CreateDate.Year == currenDate.Year
                          select new { amount = a.AmountUSD }).Union
                        (from a in context.TransactionDeals
                         where a.CIF == cif && a.CreatedDate.Month == currenDate.Month && a.CreatedDate.Year == currenDate.Year && a.IsFxTransaction == true
                         select new { amount = a.AmountUSD }).Sum(o => (decimal?)o.amount) ?? 0.0M;

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool IsUserCurrentAccess(ref IList<string> userRoles, string MenuName, ref string message)
        {
            bool isSuccess = false;

            try
            {
                var RoleName = context.V_RoleMenuMapping.Where(x => x.MenuName.Equals(MenuName)).Select(y => y.RoleName).ToList();
                foreach (string roler in RoleName)
                {
                    userRoles.Add(roler);
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.ToString();
                return false;
            }

            return isSuccess;
        }

        public bool CheckTransactionalHandling(ref List<string> output, TransactionCheckerDataModel checker, long? transactionId, ref string message)
        {
            bool isSuccess = false;
            try
            {
                output = new List<string>();
                var dataMatrix = context.SP_CheckExceptionHandling((long)transactionId, checker.Others,
                checker.IsSignatureVerified,
                checker.IsDormantAccount,
                checker.IsFreezeAccount, checker.IsCallbackRequired,
                checker.IsLOIAvailable, checker.IsStatementLetterCopy).ToList();

                if (dataMatrix != null && dataMatrix.Count() > 0)
                {
                    foreach (var item in dataMatrix)
                    {
                        output.Add(item.ToString());
                    }
                }
                isSuccess = true;

            }
            catch (Exception ex)
            {
                message = ex.ToString();
                return false;
            }

            return isSuccess;
        }

        #endregion

        public bool GetDropdownLists(ref IList<DropdownListsModel> dropdownLits, ref string message)
        {
            bool result = false;

            try
            {
                using (DBSEntities context1 = new DBSEntities())
                {
                    dropdownLits = (from a in context1.StaticDropdowns
                                    select new DropdownListsModel
                                    {
                                        ID = a.DropdownID,
                                        CatagoryName = a.DropdownCategoryname,
                                        Name = a.DropdownItem,
                                        Itemvalue = a.DropdownItemvalue,
                                        Modulename = a.ModuleName
                                    }).ToList();
                }
                result = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return result;
        }
        public bool GetTZRef(string key, int limit, ref IList<TZReferenceModel> TZRef, ref string message)
        {
            bool isSuccess = false;
            try
            {
                TZRef = (from a in context.TransactionDeals
                         join b in context.Customers
                         on a.CIF equals b.CIF
                         where a.TZRef.Contains(key)
                         orderby new { a.TZRef }
                         select new TZReferenceModel
                         {
                             ID = a.TransactionDealID,
                             TZReference = a.TZRef,
                             CIF = a.CIF,
                             CustomerName = b.CustomerName
                         }).Take(limit).ToList();




                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool IsAnnualStatement(ref bool output, string cif, ref string message)
        {
            bool isSuccess = false;
            try
            {

                output = context.CustomerUnderlyings.Where(x => x.CIF.Equals(cif) && x.IsDeleted.Equals(false) && x.StatementLetterID.Equals(4)).Any();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;

        }

        #region New Calculate Transaction Threshold

        public bool GetThresholdFXValue(ParamThresholdFXModel param, ref ThresholdModel threshold, ref string message)
        {
            bool isSuccess = false;
            try
            {
                threshold = (from a in context.SP_CheckThresholdFX(
                                param.TzRef,
                                param.StatementLetterID,
                                param.ProductTypeID,
                                param.DebitCurrencyCode,
                                param.TransactionCurrencyCode,
                                param.IsResident,
                                param.AmountUSD,
                                param.CIF,
                                param.AccountNumber,
                                param.IsJointAccount)
                             select new ThresholdModel
                             {
                                 IsHitThreshold = a.IsHitThreshold,
                                 ResidentType = a.ResidentType,
                                 RoundingValue = a.RoundingValue != null ? a.RoundingValue.Value : default(decimal),
                                 ThresholdValue = a.ThresholdValue != null ? a.ThresholdValue.Value : default(decimal),
                                 TransactionType = a.TransactionType
                             }).SingleOrDefault();
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }


        public bool GetThresholdValue(ParamThresholdModel param, ref ThresholdModel threshold, ref string message)
        {
            bool isSuccess = false;
            try
            {
                threshold = (from a in context.SP_CheckThreshold(param.ProductTypeID,
                                param.DebitCurrencyCode,
                                param.TransactionCurrencyCode,
                                param.IsResident,
                                param.AmountUSD,
                                param.CIF,
                                param.AccountNumber,
                                param.IsJointAccount)
                             select new ThresholdModel
                             {
                                 IsHitThreshold = a.IsHitThreshold,
                                 ResidentType = a.ResidentType,
                                 RoundingValue = a.RoundingValue != null ? a.RoundingValue.Value : default(decimal),
                                 ThresholdValue = a.ThresholdValue != null ? a.ThresholdValue.Value : default(decimal),
                                 TransactionType = a.TransactionType
                             }).SingleOrDefault();
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool GetTotalIDRFCY(ParamThresholdModel param, ref TotalTransactionModel threshold, ref string message)
        {
            bool isSuccess = false;
            try
            {
                threshold = (from a in context.SP_GetTotalTransactionIDRFCY(
                                param.ProductTypeID,
                                param.DebitCurrencyCode,
                                param.TransactionCurrencyCode,
                                param.IsResident,
                                param.CIF,
                                param.AccountNumber,
                                param.IsJointAccount,
                                param.StatementLetterID,
                                param.TZReference)
                             select new TotalTransactionModel
                             {
                                 TotalTransaction = a.TotalAmountUSD.HasValue ? a.TotalAmountUSD.Value : 0,
                                 TotalUtilizationAmount = a.TotalUtilization.HasValue ? a.TotalUtilization.Value : 0,
                                 TotalUtilizationDeal = a.TotalUtilizationDeal.HasValue ? a.TotalUtilizationDeal.Value : 0,
                                 AvailableUnderlyingAmount = a.AvailableUnderlyingAmount.HasValue ? a.AvailableUnderlyingAmount.Value : 0,
                                 RoundingValue = a.RoundingValue.HasValue ? a.RoundingValue.Value : 0,
                                 ThresholdType = a.ThresholdType,
                                 ThresholdValue = a.ThresholdValue.HasValue ? a.ThresholdValue.Value : 0

                             }).SingleOrDefault();
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }


        public bool GetThresholdParameter(ref ThresholdParameterModel threshold, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    var data = context.Configs.Where(x => x.Parameter.ToUpper().Equals("FX_TRANSACTION_TRESHOLD")).Select(x => x.Value).FirstOrDefault();
                    threshold.ThresholdBuy = decimal.Parse(data);
                };

                using (DBSEntities context = new DBSEntities())
                {
                    var data = context.Configs.Where(x => x.Parameter.ToUpper().Equals("FCY_IDR_TRANSACTION_TRESHOLD")).Select(x => x.Value).FirstOrDefault();
                    threshold.ThresholdSell = decimal.Parse(data);

                };

                using (DBSEntities context = new DBSEntities())
                {
                    threshold.RupiahRate = context.Currencies.Where(x => x.IsDeleted.Equals(false) && x.CurrencyID.Equals(1)).Select(x => x.RupiahRate).FirstOrDefault();
                };

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool GetSwapTransaction(string murexNumber, string dfNumber, ref SwapModel output, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities contex = new DBSEntities())
                {
                    output = (from a in contex.TransactionDeals.Where(x => x.NB.Equals(murexNumber) && !x.TZRef.Equals(dfNumber))
                              select new SwapModel()
                              {
                                  DFNumber = a.TZRef,
                                  DFSwapNumber = a.SwapTZDealNo,
                                  SwapType = a.SwapType.HasValue ? a.SwapType.Value : 0,
                                  SwapTransactionID = a.TransactionDealID
                              }).FirstOrDefault();
                    if (output == null)
                    {
                        return false;
                    }
                };
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }


        public bool GetNettingTransaction(string dfNumber, ref SwapModel output, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities contex = new DBSEntities())
                {
                    output = (from a in contex.TransactionNettings.Where(x => x.TZRef.Equals(dfNumber) || x.NettingDealNo.Equals(dfNumber)).OrderByDescending(x => x.NettingTransactionID)
                              select new SwapModel()
                              {
                                  DFNumber = a.TZRef != dfNumber ? a.TZRef : a.NettingDealNo,
                                  SwapTransactionID = a.NettingTransactionID.HasValue ? a.NettingTransactionID.Value : 0
                              }).FirstOrDefault();
                    if (output == null)
                    {
                        return false;
                    }
                };
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        #endregion

        #region Agung
        public bool GetTMONettingTransaction(string dfNumber, ref SwapModel output, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    output = (from a in context.TMONettings
                              join b in context.TMONettingDetails on a.TMONettingID equals b.TMONettingID into zi
                              from c in zi.DefaultIfEmpty()
                              where a.TZRef.Equals(dfNumber) || a.SwapDealNumber.Equals(dfNumber) || c.TZRef.Equals(dfNumber) || c.SwapDealNumber.Equals(dfNumber)
                              select new SwapModel()
                              {
                                  DFNumber = a.ApplicationID != null ? a.ApplicationID : a.ApplicationID,
                                  SwapTransactionID = a.TMONettingID != null? a.TMONettingID : 0
                              }).FirstOrDefault();

                    if (output == null)
                    {
                        return false;
                    }
                };
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        #endregion
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }
        #region Agung
        public bool GetCompleteTZRefTMO(string key, int limit, string cif, ref IList<TZReferenceModel> TZRef, ref string message)
        {
            bool isSuccess = false;
            try
            {
                var index = 1;

                TZRef = (from a in context.Customers
                         join b in context.TransactionDeals
                         on a.CIF equals b.CIF
                         where (b.TZRef.Contains(key) || b.NB.Contains(key)) && a.CIF == cif 
                                    && b.IsCanceled != true && (b.TransactionStatus.Equals("Completed") || b.TransactionStatus == null)
                         orderby new { b.TZRef }
                         select new TZReferenceModel
                         {
                             ID = b.TransactionDealID,
                             TZReference = b.TZRef,
                             CIF = b.CIF,
                             CustomerName = b.Customer.CustomerName,
                             MurexNumber = b.NB,
                             SwapDealNumber = b.SwapTZDealNo,
                             ExpectedDateStatementLetter = b.ExpectedSubmissionDateSL ??  null,
                             ExpectedDateUnderlying = b.ExpectedSubmissionDateUnd ?? null,
                             ActualDateStatementLetter = b.ActualSubmissionDateSL ?? null,
                             ActualDateUnderlying = b.ActualSubmissionDateUnderlying ?? null
                         }).Take(limit).ToList().Select(x =>
                         {
                             var swap = x.SwapDealNumber != null ? x.SwapDealNumber.Split('#') : null;
                             return new TZReferenceModel
                             {
                                 ID = x.ID,
                                 TZReference = x.TZReference,
                                 CIF = x.CIF,
                                 CustomerName = x.CustomerName,
                                 MurexNumber = x.MurexNumber,
                                 SwapDealNumber = swap != null ? (swap.Length > 1 ? swap[1] : null) : null,
                                 ExpectedDateStatementLetter = x.ExpectedDateStatementLetter,
                                 ExpectedDateUnderlying = x.ExpectedDateUnderlying,
                                 ActualDateStatementLetter = x.ActualDateStatementLetter,
                                 ActualDateUnderlying = x.ActualDateUnderlying
                             };
                         }).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        #endregion
        #region basri
        public bool GetCompleteTZRef(string key, int limit, string cif, ref IList<TZReferenceModel> TZRef, ref string message)
        {
            bool isSuccess = false;
            try
            {

                TZRef = (from a in context.Customers
                         join b in context.TransactionDeals
                         on a.CIF equals b.CIF
                         join c in context.TransactionNettings on b.TZRef equals c.TZRef into zi
                         from c in zi.DefaultIfEmpty()
                         where (b.TZRef.Contains(key) || b.NB.Contains(key)) && a.CIF == cif
                             //&& b.ActualSubmissionDateSL != null
                             //&& b.IsUnderlyingCompleted == true
                                    && c.IsNettingTransaction != true
                                    && b.IsCanceled != true
                         orderby new { b.TZRef }
                         select new TZReferenceModel
                         {
                             ID = b.TransactionDealID,
                             TZReference = b.TZRef,
                             CIF = b.CIF,
                             CustomerName = b.Customer.CustomerName,
                             MurexNumber = b.NB,
                             SwapDealNumber = b.SwapTZDealNo,
                             ExpectedDateStatementLetter = b.ExpectedSubmissionDateSL != null ? b.ExpectedSubmissionDateSL : null,
                             ExpectedDateUnderlying = b.ExpectedSubmissionDateUnd != null ? b.ExpectedSubmissionDateUnd : null,
                             ActualDateStatementLetter = b.ActualSubmissionDateSL != null ? b.ActualSubmissionDateSL : null,
                             ActualDateUnderlying = b.ActualSubmissionDateUnd != null ? b.ActualSubmissionDateUnd : null

                         }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool GetIncompleteTZRef(string key, int limit, string cif, ref IList<TZReferenceModel> TZRef, ref string message)
        {
            bool isSuccess = false;
            try
            {
                if (!string.IsNullOrEmpty(cif))
                {
                    TZRef = (from a in context.Customers
                             join b in context.TransactionDeals
                             on a.CIF equals b.CIF
                             where (b.TZRef.Contains(key) || b.SwapTZDealNo.Contains(key)) && a.CIF == cif
                                    && (b.ActualSubmissionDateSL == null
                                    || b.IsUnderlyingCompleted != true)
                                    && b.IsCanceled != true
                             orderby new { b.TZRef }
                             select new TZReferenceModel
                             {
                                 ID = b.TransactionDealID,
                                 TZReference = b.TZRef,
                                 CIF = b.CIF,
                                 CustomerName = b.Customer.CustomerName,
                                 MurexNumber = b.NB,
                                 SwapDealNumber = b.SwapTZDealNo
                             }).Take(limit).ToList();

                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool GetDetailTZRef(string cif, string TZReference, string MurexNumber, ref TZTransactionModel TZRefInfo, ref string message)
        {
            bool isSuccess = false;
            string NewMurexNumber;
            string NewTZReference;
            if (MurexNumber.Contains('@'))
            {
                NewMurexNumber = MurexNumber.Replace('@', '#');
            }
            else
            {
                NewMurexNumber = MurexNumber;
            }
            if (TZReference.Contains('@'))
            {
                NewTZReference = TZReference.Replace('@', '#');
            }
            else
            {
                NewTZReference = TZReference;
            }

            try
            {
                if (!string.IsNullOrEmpty(cif))
                {
                    TZTransactionModel data = (from a in context.TransactionDeals
                                               join b in context.Customers on a.CIF equals b.CIF
                                               join c in context.ProductTypes on a.ProductTypeID equals c.ProductTypeID
                                               join d in context.Currencies on a.CurrencyID equals d.CurrencyID
                                               join e in context.StatementLetters on a.StatementLetterID equals e.StatementLetterID
                                               where a.CIF == cif && (a.TZRef == NewTZReference || a.NB == NewMurexNumber)
                                               select new TZTransactionModel()
                                               {
                                                   ID = a.TransactionDealID,
                                                   CustomerName = b.CustomerName,
                                                   TZRef = a.TZRef,
                                                   Amount = a.Amount,
                                                   utilizationAmount = a.UtilizedAmount.HasValue ? a.UtilizedAmount.Value : 0,
                                                   IsFxTransaction = a.IsFxTransaction,
                                                   Rate = a.Rate,
                                                   TradeDate = a.TradeDate,
                                                   AccountNumber = (a.IsOtherAccountNumber.HasValue && a.IsOtherAccountNumber.Value ? a.OtherAccountNumber : a.AccountNumber),
                                                   Currency = new CurrencyModel()
                                                   {
                                                       ID = d.CurrencyID,
                                                       Code = d.CurrencyCode,
                                                       Description = d.CurrencyDescription
                                                   },
                                                   ProductType = new ProductTypeModel
                                                   {
                                                       ID = c.ProductTypeID,
                                                       Code = c.ProductTypeCode,
                                                       Description = c.ProductTypeDesc
                                                   },
                                                   StatementLetter = new StatementLetterModel()
                                                   {
                                                       ID = e.StatementLetterID,
                                                       Name = e.StatementLetterName,
                                                       LastModifiedBy = e.UpdateBy,
                                                       LastModifiedDate = e.UpdateDate
                                                   },
                                                   IsJointAccount = a.IsJointAccount.HasValue ? a.IsJointAccount.Value : false
                                               }).SingleOrDefault();
                    TZRefInfo = data;

                    isSuccess = true;

                }

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool GetLoanSettlementType(long transactionid, ref bool isScheduledSettlementLoan, ref string message)
        {
            bool isSuccess = false;
            try
            {
                isScheduledSettlementLoan = false;
                long transactionRolloverID = context.TransactionRollovers.Where(a => a.TransactionID == transactionid).Select(g => g.TransactionRolloverID).FirstOrDefault();

                int transactionRolloverCount = context.CSORollovers.Where(a => a.TransactionRolloverID == transactionRolloverID).Count();

                if (transactionRolloverCount > 0)
                {
                    isScheduledSettlementLoan = true;
                }
                else
                {
                    isScheduledSettlementLoan = false;
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }

            return isSuccess;
        }

        public bool GetCIFFromUploadedData(IList<string> arrCIF, ref string CIF, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                var getCIF = (from a in context.Customers
                              where arrCIF.Contains(a.CIF)
                              select new
                              {
                                  a.CIF
                              }).FirstOrDefault();
                CIF = getCIF.CIF;
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }
            return IsSuccess;
        }
        #endregion
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}