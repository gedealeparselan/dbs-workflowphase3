﻿using DBS.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class FDGenerateController : ApiController
    {
        private string message = string.Empty;

        private readonly IFDGenerateRepository repository = null;

        public FDGenerateController()
        {
            this.repository = new FDGenerateRepository();
        }

        public FDGenerateController(IFDGenerateRepository repository)
        {
            this.repository = repository;
        }

        [ResponseType(typeof(FDIMGrid))]
        //[HttpGet]
        [Route("api/FDGenerate/{tanggal}")]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Search(DateTime? tanggal, int? page, int? size, string sort_column, string sort_order, IList<FDIMFilter> filters)
        {
            FDIMGrid output = new FDIMGrid();

            if (!repository.GetFDInterest(tanggal, page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "SchemeType" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<FDIMGrid>(HttpStatusCode.OK, output);
            }
        }

    }
}
