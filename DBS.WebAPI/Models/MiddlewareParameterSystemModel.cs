﻿using DBS.WebAPI.Models;
using DBS.WebAPI.Helpers;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Linq.Dynamic;

namespace DBS.WebAPI.Models
{
    #region Property
    public class MiddlewareParameterSystemModel
    {
        public int IDParameter { get; set; }
        public string ParameterName { get; set; }
        public string Description { get; set; }
        public string ParameterContents { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }

        public bool? IsTempListenerStatus { get; set; }
        public bool? IsTempSenderStatus { get; set; }
    }
    public class MiddlewareParameterSystemRow : MiddlewareParameterSystemModel
    {
        public int RowID { get; set; }

    }
    public class MiddlewareParameterSystemGrid : Grid
    {
        public IList<MiddlewareParameterSystemRow> Rows { get; set; }
    }
    #endregion

    #region Filter
    public class MiddlewareParameterSystemFilter : Filter { }
    #endregion

    #region interface
    public interface IMiddlewareParameterSystemRepository : IDisposable
    {
        bool GetMiddlewareParameterSystem(ref IList<MiddlewareParameterSystemModel> Middleware, ref string message);
        bool GetMiddlewareParameterSystem(int page, int size, IList<MiddlewareParameterSystemFilter> filters, string sortColumn, string sortOrder, ref MiddlewareParameterSystemGrid ParameterSystem, ref string message);
        bool UpdateMiddlewareParameterSystem(int id, MiddlewareParameterSystemModel ParameterSystem, ref string message);
    }
    #endregion

    #region Repository
    public class MiddlewareParameterSystemRepository : IMiddlewareParameterSystemRepository 
    {

        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetMiddlewareParameterSystem(int page, int size, IList<MiddlewareParameterSystemFilter> filters, string sortColumn, string sortOrder, ref MiddlewareParameterSystemGrid ParameterSystem, ref string message)
        {
           // throw new NotImplementedException();
            bool IsSuccess = false;
            MiddlewareParameterSystemModel model = new MiddlewareParameterSystemModel();
            try {

                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                MiddlewareParameterSystemModel filter = new MiddlewareParameterSystemModel();
                if (filters != null)
                {
                    filter.ParameterName = (string)filters.Where(a => a.Field.Equals("ParameterName")).Select(a => a.Value).SingleOrDefault();
                    filter.Description = (string)filters.Where(a => a.Field.Equals("Description")).Select(a => a.Value).SingleOrDefault();
                    filter.ParameterContents = (string)filters.Where(a => a.Field.Equals("ParameterContents")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.ParameterSystem1
                                join employee in context.Employees on a.CreateBy equals employee.EmployeeName into joinEmployee
                                from employee in joinEmployee.DefaultIfEmpty() 
                                let isParameterName = !string.IsNullOrEmpty(filter.ParameterName)
                                let isDescription = !string.IsNullOrEmpty(filter.Description)
                                let isParameterContents = !string.IsNullOrEmpty(filter.ParameterContents) 
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where
                                    (isParameterName ? a.ParameterName.Contains(filter.ParameterName):true)&&
                                    (isParameterContents ? a.ParameterContents.Contains(filter.ParameterContents):true)&&
                                    (isDescription ? a.Description.Contains(filter.Description) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new MiddlewareParameterSystemModel
                                {
                                    IDParameter = a.IDParameter,
                                    ParameterName = a.ParameterName,//=="listenerstatus" || a.ParameterName=="senderstatus"?(a.ParameterContents == "1" ? "on" :"off"):a.ParameterName,
                                    ParameterContents = a.ParameterName=="listenerstatus" || a.ParameterName=="senderstatus"?(a.ParameterContents=="1"?"on":"off"):a.ParameterContents,
                                    //ParameterContents = a.ParameterContents == "1" ? "on" : a.ParameterContents == "0" ? "off": a.ParameterContents,
                                    Description = a.Description,
                                    LastModifiedBy =(employee.EmployeeUsername==null? a.CreateBy: employee.EmployeeName),
                                    //LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    ParameterSystem.Page = page;
                    ParameterSystem.Size = size;
                    ParameterSystem.Total = data.Count();
                    ParameterSystem.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    ParameterSystem.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new MiddlewareParameterSystemRow
                        {
                            RowID = i + 1,
                            IDParameter = a.IDParameter,
                            ParameterName = a.ParameterName,
                           
                            ParameterContents = a.ParameterContents,
                            Description = a.Description,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                IsSuccess = true;
            }
            catch(Exception ex)
            {
              message = ex.Message;
            }
            return IsSuccess;
        }

        public bool UpdateMiddlewareParameterSystem(int id, MiddlewareParameterSystemModel ParameterSystem, ref string message)
        {
            bool isSucces = false;
            try
            {
                Entity.ParameterSystem1 data = context.ParameterSystem1.Where
                                               (middlewareParamSystem => middlewareParamSystem.IDParameter.Equals(id)).SingleOrDefault();
                    
                if (data != null)
                {

                    data.ParameterName = ParameterSystem.ParameterName;
                   // data.Description = ParameterSystem.Description;
                   // data.ParameterContents = || ParameterSystem. ;
                    //ParameterSystem.IsTempListenerStatus != null
                    if (data.ParameterName =="listenerstatus") {
                        data.ParameterContents = ParameterSystem.IsTempListenerStatus == true ? "1" : "0";
                        
                       // data.Description = ParameterSystem.IsTempListenerStatus == true ? "Status of listener service, service will receive ACK from IPE if this value = ON" : "Status of listener service, service will receive ACK from IPE if this value = OFF"; 
                    }
                    else if (data.ParameterName == "senderstatus")//ParameterSystem.IsTempSenderStatus != null
                    {
                        data.ParameterContents = ParameterSystem.IsTempSenderStatus == true ? "1" : "0";
                        //data.Description = ParameterSystem.Description;
                       // data.Description = ParameterSystem.IsTempSenderStatus == true ? "Status of sender service, service will send transaction to IPE if this value = ON" : "Status of sender service, service will send transaction to IPE if this value = OFF";

                    }
                    else {
                        data.ParameterContents = ParameterSystem.ParameterContents;
                       // data.Description = ParameterSystem.Description;
                    }
                    data.Description = ParameterSystem.Description;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                    data.UpdateDate = DateTime.UtcNow;
                    context.SaveChanges();
                    isSucces = true;
                }
                else
                {
                    message = string.Format("Data Middleware Parameter System {0} is does not exist.", id);
                }
                //isSucces = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSucces;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        public bool GetMiddlewareParameterSystem(ref IList<MiddlewareParameterSystemModel> Middleware, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    Middleware = (from a in context.ParameterSystem1
                                   //where a.IsDeleted.Value.Equals(false)
                                   orderby new { a.ParameterName, a.Description }
                                  select new MiddlewareParameterSystemModel
                                   {
                                       IDParameter = a.IDParameter,
                                       ParameterName = a.ParameterName,
                                       ParameterContents = a.ParameterContents,
                                      Description = a.Description,
                                       
                                       LastModifiedBy = a.CreateBy,
                                       LastModifiedDate = a.CreateDate,
                                   }).ToList();
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }
    }
    #endregion

} 