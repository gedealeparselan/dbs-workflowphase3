﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoginDBS.aspx.cs" Inherits="AceNavigation.Layouts.DBS.Login.LoginDBS" %>


<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<link rel="stylesheet" href="Scripts/assets/css/ace.min.css" />
<link rel="stylesheet" href="Scripts/assets/css/uncompressed/ace-rtl.css" />
<link rel="stylesheet" href="Scripts/assets/css/uncompressed/ace-skins.css" />
<link rel="stylesheet" href="Scripts/assets/css/uncompressed/bootstrap.css" />
<link rel="stylesheet" href="Scripts/dbs-style/dbs-style2.css" />
<link rel="stylesheet" href="Scripts/assets/css/uncompressed/font-awesome.min.css" />

<style type="text/css">
.login-layout .login-box .widget-main {
    padding: 10px 21px 25px;
}
.login-layout .widget-box .widget-main {
    background: none repeat scroll 0 0 #f7f7f7;
}

</style>


   
    <title>Sign In</title>

</head>
<body class="login-layout">

    <form runat="server" id="form1">
        <div class="main-container">
        <div class="main-content">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <div class="login-container" style="background:#fff">
                        <div class="center" style="margin-top:30px">
                            <img src="Logo.png" alt="Logo_Desc" />
                            <h2><span class="Red">DBSI Workflow Application</span></h2>
                        </div>

                        <div class="space-6"></div>

                        <div class="position-relative">
                            <div id="login-box" class="login-box visible widget-box no-border">
                                <div class="widget-body">
                                    <div class="widget-main">
                                       
                                        <h6 class="header red lighter bigger">
                                            <i class="icon-user red"></i>
                                            Input Username and Password
                                        </h6>
                                        
                                        <div class="space-6"></div>

                                        <form action="@Url.Action("LoginProcess")" method="post">
                                            <fieldset>
                                                <label class="block clearfix">
                                                    <span class="block input-icon input-icon-right">
													<asp:TextBox ID="txtUsername" runat="server" ToolTip="Insert User Name"
													name="username"
													ForeColor="Gray"  autocomplete="off" CssClass="form-control" placeholder="Username"></asp:TextBox>
                                                        
                                                    </span>
                                                </label>

                                                <label class="block clearfix">
												<span class="block input-icon input-icon-right">
												<asp:TextBox ID="txtPassword" runat="server" ToolTip="Insert Password" TextMode="Password" name="password" autocomplete="off" CssClass="form-control" placeholder="Password"></asp:TextBox>
                                                    
                                                       
                                                    </span>
                                                </label>

                                               <div class="inputwrapper login-alert">
                                               <asp:Label ID="lblMessage" CssClass="alert alert-error" runat="server" ForeColor="Red"></asp:Label>
                                               </div>
                                                <div class="space"></div>

                                                <div class="clearfix">
                                                    
                                                    
													
													<asp:Button ID="btnSubmit" Text="Login" runat="server" OnClick="btnSubmit_Click" CssClass="width-15 pull-right btn btn-ojk-red"
                                                     name="submit" UseSubmitBehavior="true"></asp:Button>
                                                </div>
                                                <div class="space-4">

                                                </div>
<div class="inputwrapper animate4 bounceIn">
                            <asp:HyperLink runat="server" ID="imgAdmin" NavigateUrl="/_windows/default.aspx?ReturnUrl=%2f_layouts%2fAuthenticate.aspx%3fSource%3d%252F&Source=%2F"
                                ImageUrl="~/_Layouts/15/DBS.Login/admin_login_icon.png" Visible="true"></asp:HyperLink>
                        </div>
                                                
                                            </fieldset>
                                        </form>
                                    </div>
                                    <!-- /widget-main -->

                                </div>
								<div style="background-color:white; text-align:center;">
									<p>&copy; 2014. All Rights Reserved.</p>
								</div>
                                <!-- /widget-body -->
                            </div>
                            <!-- /login-box -->
                        </div>
                        <!-- /position-relative -->
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
    </div>

    </form>
    <center>
        <p>&copy; 2014. All Rights Reserved.</p>
    </center>
	
</body>
</html>
