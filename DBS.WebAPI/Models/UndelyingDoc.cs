﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Models
{
    #region Property
    [ModelName("UnderlyingDocument")]
    public class UnderlyingDocModel
    {
        /// <summary>
        /// UnderlyingDoc ID.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Underlying Document Code.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Underlying Document Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Underlying Document Description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }
    }

    public class UnderlyingDocRow : UnderlyingDocModel
    {
        public int RowID { get; set; }
    }

    public class UnderlyingDocGrid : Grid
    {
        public IList<UnderlyingDocRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class UnderlyingDocFilter : Filter {}
    #endregion

    #region Interface
    public interface IUnderlyingDocRepository : IDisposable
    {
        bool GetUnderlyingDocByID(int id, ref UnderlyingDocModel UnderlyingDoc, ref string message);
        bool GetUnderlyingDoc(ref IList<UnderlyingDocModel> underlyingDocs, int limit, int index, ref string message);
        bool GetUnderlyingDoc(ref IList<UnderlyingDocModel> underlyingDocs, ref string message);
        bool GetUnderlyingDoc(int page, int size, IList<UnderlyingDocFilter> filters, string sortColumn, string sortOrder, ref UnderlyingDocGrid UnderlyingDoc, ref string message);
        bool GetUnderlyingDoc(UnderlyingDocFilter filter, ref IList<UnderlyingDocModel> underlyingDocs, ref string message);
        bool GetUnderlyingDoc(string key, int limit, ref IList<UnderlyingDocModel> underlyingDocs, ref string message);
        bool AddUnderlyingDoc(UnderlyingDocModel UnderlyingDoc, ref string message);
        bool UpdateUnderlyingDoc(int id, UnderlyingDocModel UnderlyingDoc, ref string message);
        bool DeleteUnderlyingDoc(int id, ref string message);
    }
    #endregion

    #region Repository
    public class UnderlyingDocRepository : IUnderlyingDocRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetUnderlyingDocByID(int id, ref UnderlyingDocModel UnderlyingDoc, ref string message)
        {
            bool isSuccess = false;

            try
            {
                UnderlyingDoc = (from a in context.UnderlyingDocuments
                            where a.IsDeleted.Equals(false) && a.UnderlyingDocID.Equals(id)
                            select new UnderlyingDocModel
                            {
                                ID = a.UnderlyingDocID,
                                Code = a.UnderlyingDocCode,
                                Name = a.UnderlyingDocName,
                                LastModifiedDate = a.CreateDate,
                                LastModifiedBy = a.CreateBy
                            }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetUnderlyingDoc(ref IList<UnderlyingDocModel> UnderlyingDoc, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    UnderlyingDoc = (from a in context.UnderlyingDocuments
                                where a.IsDeleted.Equals(false)
                                orderby a.UnderlyingDocName
                                select new UnderlyingDocModel
                                {
                                    ID = a.UnderlyingDocID,
                                    Code = a.UnderlyingDocCode,
                                    Name = a.UnderlyingDocName,
                                     LastModifiedBy = a.CreateBy,
                                     LastModifiedDate = a.CreateDate
                                }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

       public bool GetUnderlyingDoc(ref IList<UnderlyingDocModel> underlyingDocs, ref string message)
        { 
         bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    underlyingDocs = (from a in context.UnderlyingDocuments
                                     where a.IsDeleted.Equals(false)
                                     orderby a.UnderlyingDocID
                                     select new UnderlyingDocModel
                                     {
                                         ID = a.UnderlyingDocID,
                                         Code = a.UnderlyingDocCode,
                                         Name = a.UnderlyingDocName,
                                         LastModifiedBy = a.CreateBy,
                                         LastModifiedDate = a.CreateDate
                                     }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool GetUnderlyingDoc(int page, int size, IList<UnderlyingDocFilter> filters, string sortColumn, string sortOrder, ref UnderlyingDocGrid customer, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                UnderlyingDocModel filter = new UnderlyingDocModel();
                if (filters != null)
                {
                    filter.Code = filters.Where(a => a.Field.Equals("Code")).Select(a => a.Value).SingleOrDefault();
                    filter.Name = filters.Where(a => a.Field.Equals("Name")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse(filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.UnderlyingDocuments
                                let isCode = !string.IsNullOrEmpty(filter.Code)
                                let isName = !string.IsNullOrEmpty(filter.Name)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) &&
                                    (isCode ? a.UnderlyingDocCode.Contains(filter.Code) : true) &&
                                    (isName ? a.UnderlyingDocName.Contains(filter.Name) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new UnderlyingDocModel
                                {
                                    ID = a.UnderlyingDocID,
                                    Code = a.UnderlyingDocCode,
                                    Name = a.UnderlyingDocName,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    customer.Page = page;
                    customer.Size = size;
                    customer.Total = data.Count();
                    customer.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    customer.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new UnderlyingDocRow {
                            RowID = i + 1,
                            ID = a.ID,
                            Code = a.Code,
                            Name = a.Name,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate 
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetUnderlyingDoc(UnderlyingDocFilter filter, ref IList<UnderlyingDocModel> customer, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetUnderlyingDoc(string key, int limit, ref IList<UnderlyingDocModel> UnderlyingDoc, ref string message)
        {
            bool isSuccess = false;

            try
            {
                UnderlyingDoc = (from a in context.UnderlyingDocuments
                            where a.IsDeleted.Equals(false) && a.UnderlyingDocName.Contains(key)
                            orderby a.UnderlyingDocName
                            select new UnderlyingDocModel
                            {
                                ID = a.UnderlyingDocID,
                                Code = a.UnderlyingDocCode,
                                Name = a.UnderlyingDocName,
                            }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddUnderlyingDoc(UnderlyingDocModel underlyingDoc, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (!context.UnderlyingDocuments.Where(a => a.UnderlyingDocCode.Equals(underlyingDoc.Code) && a.IsDeleted.Equals(false)).Any())
                {
                    context.UnderlyingDocuments.Add(new UnderlyingDocument()
                    {
                        UnderlyingDocName = underlyingDoc.Name,
                        UnderlyingDocCode = underlyingDoc.Code,
                        IsDeleted = false,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    });

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("UnderlyingDoc data for UnderlyingDoc code {0} is already exist.", underlyingDoc.Name);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateUnderlyingDoc(int id, UnderlyingDocModel underlyingDoc, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.UnderlyingDocument data = context.UnderlyingDocuments.Where(a => a.UnderlyingDocID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    if (context.UnderlyingDocuments.Where(a => a.UnderlyingDocID != underlyingDoc.ID && a.UnderlyingDocCode.Equals(underlyingDoc.Code) && a.IsDeleted.Equals(false)).Count() >= 1)
                    {
                        message = string.Format("Underlying Document Name {0} is exist.", underlyingDoc.Name);
                    }
                    else
                    {
                        data.UnderlyingDocName = underlyingDoc.Name;
                        data.UnderlyingDocCode = underlyingDoc.Code;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        context.SaveChanges();

                        isSuccess = true;
                    }
                }
                else
                {
                    message = string.Format("Underlying Documnet data for ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeleteUnderlyingDoc(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.UnderlyingDocument data = context.UnderlyingDocuments.Where(a => a.UnderlyingDocID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Underlying Document data for ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    } 
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}