﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Models
{
    #region Property
    [ModelName("ChargesType")]
    public class ChargesTypeModel
    {
        /// <summary>
        /// ChargesType ID.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Curreny Code.
        /// </summary>
        //[MaxLength(3, ErrorMessage = "ChargesType Code is must be in {1} characters.")]
        public string Code { get; set; }

        /// <summary>
        /// ChargesType Description.
        /// </summary>
       // [MaxLength(255, ErrorMessage = "Maximum length is {1} characters.")]
        public string Description { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }
    }

    public class ChargesTypeRow : ChargesTypeModel
    {
        public int RowID { get; set; }

    }

    public class ChargesTypeGrid : Grid
    {
        public IList<ChargesTypeRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class ChargesTypeFilter : Filter {}
    #endregion

    #region Interface
    public interface IChargesTypeRepository : IDisposable
    {
        bool GetChargesTypeByID(int id, ref ChargesTypeModel chargesType, ref string message);
        bool GetChargesType(ref IList<ChargesTypeModel> chargesTypes, int limit, int index, ref string message);
        bool GetChargesType(ref IList<ChargesTypeModel> chargesTypes, ref string message);
        bool GetChargesType(int page, int size, IList<ChargesTypeFilter> filters, string sortColumn, string sortOrder, ref ChargesTypeGrid chargesType, ref string message);
        bool GetChargesType(ChargesTypeFilter filter, ref IList<ChargesTypeModel> chargesTypes, ref string message);
        bool GetChargesType(string key, int limit, ref IList<ChargesTypeModel> chargesTypes, ref string message);
        bool AddChargesType(ChargesTypeModel chargesType, ref string message);
        bool UpdateChargesType(int id, ChargesTypeModel chargesType, ref string message);
        bool DeleteChargesType(int id, ref string message);
    }
    #endregion

    #region Repository
    public class ChargesTypeRepository : IChargesTypeRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetChargesTypeByID(int id, ref ChargesTypeModel chargesType, ref string message)
        {
            bool isSuccess = false;

            try
            {
                chargesType = (from a in context.Charges
                            where a.IsDeleted.Equals(false) && a.ChargesID.Equals(id)
                            select new ChargesTypeModel
                            {
                                ID = a.ChargesID,
                                Code = a.ChargesCode,
                                Description = a.ChargesDescription,
                                LastModifiedDate = a.CreateDate,
                                LastModifiedBy = a.CreateBy
                            }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetChargesType(ref IList<ChargesTypeModel> chargesTypes, ref string message)
        { 
        bool isSuccess = false;

        try
        {
            using (DBSEntities context = new DBSEntities())
            {
                chargesTypes = (from a in context.Charges
                               where a.IsDeleted.Equals(false)
                               orderby new { a.ChargesCode, a.ChargesDescription }
                               select new ChargesTypeModel
                               {
                                   ID = a.ChargesID,
                                   Code = a.ChargesCode,
                                   Description = a.ChargesDescription,
                                   LastModifiedBy = a.CreateBy,
                                   LastModifiedDate = a.CreateDate
                               }).ToList();
            }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        
        }
        public bool GetChargesType(ref IList<ChargesTypeModel> chargesType, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    chargesType = (from a in context.Charges
                                where a.IsDeleted.Equals(false)
                                orderby new { a.ChargesCode, a.ChargesDescription }
                                select new ChargesTypeModel
                                {
                                     ID = a.ChargesID,
                                     Code = a.ChargesCode,
                                     Description = a.ChargesDescription,
                                     LastModifiedBy = a.CreateBy,
                                     LastModifiedDate = a.CreateDate
                                }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetChargesType(int page, int size, IList<ChargesTypeFilter> filters, string sortColumn, string sortOrder, ref ChargesTypeGrid customer, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                ChargesTypeModel filter = new ChargesTypeModel();
                if (filters != null)
                {
                    filter.Code = (string)filters.Where(a => a.Field.Equals("Code")).Select(a => a.Value).SingleOrDefault();
                    filter.Description = (string)filters.Where(a => a.Field.Equals("Description")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate =DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.Charges
                                let isCode = !string.IsNullOrEmpty(filter.Code)
                                let isDesc = !string.IsNullOrEmpty(filter.Description)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) &&
                                    (isCode ? a.ChargesCode.Contains(filter.Code) : true) &&
                                    (isDesc ? a.ChargesDescription.Contains(filter.Description) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new ChargesTypeModel
                                {
                                    ID = a.ChargesID,
                                    Code = a.ChargesCode,
                                    Description = a.ChargesDescription,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    customer.Page = page;
                    customer.Size = size;
                    customer.Total = data.Count();
                    customer.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    customer.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new ChargesTypeRow {
                            RowID = i + 1,
                            ID = a.ID,
                            Code = a.Code,
                            Description = a.Description,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate 
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetChargesType(ChargesTypeFilter filter, ref IList<ChargesTypeModel> customer, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetChargesType(string key, int limit, ref IList<ChargesTypeModel> chargesType, ref string message)
        {
            bool isSuccess = false;

            try
            {
                chargesType = (from a in context.Charges
                            where a.IsDeleted.Equals(false) &&
                                a.ChargesCode.Contains(key) || a.ChargesDescription.Contains(key)
                            orderby new { a.ChargesCode, a.ChargesDescription }
                            select new ChargesTypeModel
                            {
                                Code = a.ChargesCode,
                                Description = a.ChargesDescription
                            }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddChargesType(ChargesTypeModel chargesType, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (!context.Charges.Where(a => a.ChargesCode.Equals(chargesType.Code) && a.IsDeleted.Equals(false)).Any())
                {
                    context.Charges.Add(new Entity.Charge()
                    {
                        ChargesCode = chargesType.Code,
                        ChargesDescription = chargesType.Description,
                        IsDeleted = false,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    });

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Charges Type data for Charges Type code {0} is already exist.", chargesType.Code);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateChargesType(int id, ChargesTypeModel chargesType, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.Charge data = context.Charges.Where(a => a.ChargesID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    if (context.Charges.Where(a => a.ChargesID != chargesType.ID && a.ChargesCode.Equals(chargesType.Code) && a.IsDeleted.Equals(false)).Count() >= 1)
                    {
                        message = string.Format("Charges Type Code {0} is exist.", chargesType.Code);
                    }
                    else
                    {
                        data.ChargesCode = chargesType.Code;
                        data.ChargesDescription = chargesType.Description;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        context.SaveChanges();

                        isSuccess = true;
                    }
                }
                else
                {
                    message = string.Format("Charges Type data for Charges Type ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeleteChargesType(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.Charge data = context.Charges.Where(a => a.ChargesID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Charges Type data for Charges Type ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    } 
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}