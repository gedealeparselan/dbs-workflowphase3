﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DBS.WebAPI.Models
{
    public class DataTableModel
    {
    }

    public class Grid
    {
        public int Size { get; set; }
        public int Page { get; set; }
        public int Total { get; set; }
        public Sort Sort { get; set; }
    }

    public class Sort
    {
        public string Column { get; set; }
        public string Order { get; set; }
    }

    public class Filter
    {
        public string Field { get; set; }
        public string Value { get; set; }
    }
}