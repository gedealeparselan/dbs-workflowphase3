﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class HelperController : ApiController
    {
        private string message = string.Empty;

        private readonly IHelperRepository repository = null;

        public HelperController()
        {
            this.repository = new HelperRepository();
        }

        public HelperController(IHelperRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get Single value Generate Ref ID by CIF .
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(REFModel))]
        [Route("api/Helper/GetRefID/{cif}")]
        [HttpGet]
        public HttpResponseMessage GetRefID(string cif)
        {
            REFModel output = new REFModel();

            if (!repository.GetRefID(cif, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<REFModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Single value Statement by CIF .
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(StatementModel))]
        [Route("api/Helper/GetStatementA/{cif}/{isJointAccount}/{accountNumber}")]
        [HttpGet]
        public HttpResponseMessage GetStatementA(string cif, string accountNumber, bool isJointAccount)
        {
            StatementModel output = new StatementModel();

            if (!repository.GetStatementA(cif, accountNumber, isJointAccount, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<StatementModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Single value Customer have IDR Account by CIF .
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(bool))]
        [Route("api/Helper/GetIDR/{cif}")]
        [HttpGet]
        public HttpResponseMessage GetIsIDR(string cif)
        {
            bool output = new bool();

            if (!repository.GetIsIDR(ref output, cif, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<bool>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Single value Customer have IDR Account by CIF .
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(decimal))]
        [Route("api/Helper/GetAmountUSD/{currencyID}/{amount}")]
        [HttpGet]
        public HttpResponseMessage GetAmountUSD(int currencyID, int amount)
        {
            decimal output = new decimal();


            if (!repository.GetAmountUSD(currencyID, amount, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<decimal>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Available Amount In USD Underlying Statement B .
        /// </summary>
        /// <returns></returns>
        /// //bool GetAmountTransaction(string Cif, ref decimal output, ref string message);
        [ResponseType(typeof(decimal))]
        [Route("api/Helper/GetAvailableStatementB/{cif}")]
        [HttpGet]
        public HttpResponseMessage GetAvailableStatementB(string cif)
        {
            decimal output = new decimal();


            if (!repository.GetAvailableStatementB(cif, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<decimal>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get users active directory.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<Users>))]
        [Route("api/Helper/GetUserAD")]
        [HttpGet]
        public HttpResponseMessage GetUserAD(string username, int? limit)
        {
            IList<Users> output = new List<Users>();

            if (!repository.GetUserAD(username, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<IList<Users>>(HttpStatusCode.OK, output);
            }
        }


        /// <summary>
        /// Get TreshHold parameter config
        /// </summary>
        /// <returns></returns>       
        [ResponseType(typeof(decimal))]
        [Route("api/Helper/GetTreshHold")]
        [HttpGet]
        public HttpResponseMessage GetTreshHold()
        {
            decimal output = new decimal();


            if (!repository.GetTreshHold(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<decimal>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get FCY - IDR TreshHold  parameter config
        /// </summary>
        /// <returns></returns>       
        [ResponseType(typeof(decimal))]
        [Route("api/Helper/GetFCYIDRTrashHold")]
        [HttpGet]
        public HttpResponseMessage GetFCYIDRTrashHold()
        {
            decimal output = new decimal();


            if (!repository.GetFCYIDRTrashHold(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<decimal>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Total IDR - FCY for Deal
        /// </summary>
        /// <returns></returns>       
        [ResponseType(typeof(decimal))]
        [Route("api/Helper/GetTotalIDRFCYDeal/{cif}")]
        [HttpGet]
        public HttpResponseMessage GetTotalIDRFCYDeal(string cif)
        {
            decimal output = new decimal();


            if (!repository.GetTotalIDRFCYDeal(ref output, cif, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<decimal>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Calculate PPU and Deal
        /// </summary>
        /// <returns></returns>       
        [ResponseType(typeof(TotalDealModel))]
        [Route("api/Helper/GetCalculateDeal/{cif}")]
        [HttpGet]
        public HttpResponseMessage GetCalculateDeal(string cif)
        {
            TotalDealModel output = new TotalDealModel();

            decimal TotalDeal = new decimal();
            decimal tIDRFCYDealStatementB = new decimal();
            decimal totalUtilizationDeal = new decimal();
            decimal availableStatementB = new decimal();
            decimal totalUSDStatementB = new decimal();

            if (!repository.GetTotalIDRFCYDeal(ref TotalDeal, cif, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (!repository.GetTotalIDRFCYDealStatementB(ref tIDRFCYDealStatementB, cif, ref message))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
                }
                else
                {
                    if (!repository.GetTotalUtilizationDeal(ref totalUtilizationDeal, cif, ref message))
                    {
                        return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
                    }
                    else
                    {
                        if (!repository.GetAvailableStatementB(cif, ref availableStatementB, ref message))
                        {
                            return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
                        }
                        else
                        {
                            if (!repository.GetAmountUSDStatementB(cif, ref totalUSDStatementB, ref message))
                            {
                                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
                            }
                            else
                            {
                                output.Total = new TotalDealDetailModel();
                                output.Total.IDRFCYDeal = TotalDeal;
                                output.Total.UtilizationDeal = totalUtilizationDeal;
                                output.Total.RemainingBalance = totalUSDStatementB - tIDRFCYDealStatementB;
                                output.AvailableUnderlying = availableStatementB;
                                return Request.CreateResponse<TotalDealModel>(HttpStatusCode.OK, output);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Get Calculate PPU and Deal
        /// </summary>
        /// <returns></returns>       
        [ResponseType(typeof(TotalPPUModel))]
        [Route("api/Helper/GetCalculatePPU/{cif}")]
        [HttpGet]
        public HttpResponseMessage GetCalculatePPU(string cif)
        {
            TotalPPUModel output = new TotalPPUModel();
            TotalPPUDetailModel totalPPU = new TotalPPUDetailModel();
            decimal TotalDeal = new decimal(); // total PPU di ambil dari deal transaction
            decimal totalUtilizationPPU = new decimal();


            if (!repository.GetTotalIDRFCYDeal(ref TotalDeal, cif, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (!repository.GetTotalUtilizationPPU(ref totalUtilizationPPU, cif, ref message))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
                }
                else
                {
                    output.Total = new TotalPPUDetailModel();
                    output.Total.IDRFCYPPU = TotalDeal;
                    output.Total.UtilizationPPU = totalUtilizationPPU;
                    return Request.CreateResponse<TotalPPUModel>(HttpStatusCode.OK, output);
                }
            }
        }

        /// <summary>
        /// Get Total IDR - FCY for PPU
        /// </summary>
        /// <returns></returns>       
        [ResponseType(typeof(decimal))]
        [Route("api/Helper/GetTotalIDRFCYPPU/{cif}")]
        [HttpGet]
        public HttpResponseMessage GetTotalIDRFCYPPU(string cif)
        {
            decimal output = new decimal();


            if (!repository.GetTotalIDRFCYPPU(ref output, cif, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<decimal>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Total Utilization Deal
        /// </summary>
        /// <returns></returns>       
        [ResponseType(typeof(decimal))]
        [Route("api/Helper/GetTotalUtilizationDeal/{cif}")]
        [HttpGet]
        public HttpResponseMessage GetTotalUtilizationDeal(string cif)
        {
            decimal output = new decimal();


            if (!repository.GetTotalUtilizationDeal(ref output, cif, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<decimal>(HttpStatusCode.OK, output);
            }
        }


        /// <summary>
        /// Get Total Utilization PPU
        /// </summary>
        /// <returns></returns>       
        [ResponseType(typeof(decimal))]
        [Route("api/Helper/GetTotalUtilizationPPU/{cif}")]
        [HttpGet]
        public HttpResponseMessage GetTotalUtilizationPPU(string cif)
        {
            decimal output = new decimal();


            if (!repository.GetTotalUtilizationPPU(ref output, cif, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<decimal>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Total All IDR-FCY
        /// </summary>
        /// <returns></returns>       
        [ResponseType(typeof(decimal))]
        [Route("api/Helper/GetTotalAllIDRFCY/{cif}")]
        [HttpGet]
        public HttpResponseMessage GetTotalAllIDRFCY(string cif)
        {
            decimal output = new decimal();


            if (!repository.GetTotalAllIDRFCY(ref output, cif, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<decimal>(HttpStatusCode.OK, output);
            }
        }


        /// <summary>
        /// Get Total IDR-FCY DealStatement B
        /// </summary>
        /// <returns></returns>       
        [ResponseType(typeof(decimal))]
        [Route("api/Helper/GetTotalIDRFCYDealStatementB/{cif}")]
        [HttpGet]
        public HttpResponseMessage GetTotalIDRFCYDealStatementB(string cif)
        {
            decimal output = new decimal();


            if (!repository.GetTotalIDRFCYDealStatementB(ref output, cif, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<decimal>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Check Availbale TZ
        /// </summary>
        /// <returns></returns>       
        [ResponseType(typeof(decimal))]
        [Route("api/Helper/CheckAvailbaleTZ/{TZNumber}")]
        [HttpGet]
        public HttpResponseMessage CheckAvailbaleTZ(string TZNumber)
        {
            TZNumberModel output = new TZNumberModel();


            if (!repository.CheckTZNumberAvailable(TZNumber, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<TZNumberModel>(HttpStatusCode.OK, output);
            }
        }
        /// <summary>
        /// Check TZ NUmber for PPU
        /// </summary>
        /// <returns></returns>       
        [ResponseType(typeof(decimal))]
        [Route("api/Helper/IsAlreadyTZNumber/{TZNumber}")]
        [HttpGet]
        public HttpResponseMessage CheckAvailbaleTZPPU(string TZNumber)
        {
            bool output = false;


            if (!repository.IsAlreadyTZNumberPPU(TZNumber, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<bool>(HttpStatusCode.OK, output);
            }
        }
        /// <summary>
        /// Check Availbale Ref Number
        /// </summary>
        /// <returns></returns>       
        [ResponseType(typeof(decimal))]
        [Route("api/Helper/CheckAvailbaleBookNumber/{RefNumber}")]
        [HttpGet]
        public HttpResponseMessage CheckAvailableRefNumber(string RefNumber)
        {
            bool output = false;

            if (!repository.CheckRefNumberAvailable(RefNumber, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<bool>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Check User Permited
        /// </summary>
        /// <param name="MenuName">Menu Name</param>
        /// <returns></returns>       
        [ResponseType(typeof(UserRole))]
        [Route("api/Helper/CheckUserPermited/{MenuName}")]
        [HttpGet]
        public HttpResponseMessage CheckUserPermited(string MenuName)
        {
            //bool output = false;
            IList<string> output = new List<string>();

            if (!repository.IsUserCurrentAccess(ref output, MenuName, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {

                return Request.CreateResponse(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Check Transaction Exceptional Handling
        /// </summary>
        /// <returns></returns>       
        [ResponseType(typeof(decimal))]
        [Route("api/Helper/CheckExceptionalHandling/{transactionid}")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage CheckTransactionalHandling(long? transactionid, TransactionCheckerDataModel checkerData)
        {
            List<string> output = new List<string>();


            if (!repository.CheckTransactionalHandling(ref output, checkerData, transactionid, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<List<string>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Dropdownlists
        /// </summary>
        /// <returns></returns>       
        [ResponseType(typeof(IDictionary<string, object>))]
        [Route("api/Helper/Dropdownlists/{select}")]
        [AcceptVerbs("GET")]
        public HttpResponseMessage GetDropdownLists(string select)
        {
            IDictionary<string, object> output = new Dictionary<string, object>();
            string messages = string.Empty;
            if (!string.IsNullOrEmpty(select))
            {
                IList<DropdownListsModel> dropdownLists = new List<DropdownListsModel>();
                if (!repository.GetDropdownLists(ref dropdownLists, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                }
                else
                {
                    string[] val = select.Split(',');
                    foreach (string item in val)
                    {
                        output.Add(item, dropdownLists.Where(x => x.CatagoryName == item));
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, output);
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
        }
        /// <summary>
        /// Find TZRef.
        /// </summary>
        /// <param name="query">TZRef.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<TZReferenceModel>))]
        [HttpGet]
        [Route("api/Helper/TZRef/Search")]
        public HttpResponseMessage SearchTZRef(string query, int? limit)
        {
            IList<TZReferenceModel> TZRef = new List<TZReferenceModel>();

            if (!repository.GetTZRef(query, limit.HasValue ? limit.Value : 10, ref TZRef, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<TZReferenceModel>>(HttpStatusCode.OK, TZRef);
            }
        }
        /// <summary>
        /// Check Customer have annual statement
        /// </summary>
        /// <returns></returns>       
        [ResponseType(typeof(bool))]
        [Route("api/Helper/CheckAnnualStatement/{cif}")]
        [HttpGet]
        public HttpResponseMessage CheckAnnualStatement(string cif)
        {
            bool output = false;
            if (!repository.IsAnnualStatement(ref output, cif, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<bool>(HttpStatusCode.OK, output);
            }
        }

        #region New BPI Mode
        /// <summary>
        /// Get Threshold Value
        /// </summary>
        /// <returns></returns>       
        [ResponseType(typeof(ThresholdModel))]
        [Route("api/Helper/GetThresholdValue")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage GetThreshold(ParamThresholdModel paramhreshold)
        {
            ThresholdModel output = new ThresholdModel();
            if (!repository.GetThresholdValue(paramhreshold, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<ThresholdModel>(HttpStatusCode.OK, output);
            }
        }
        /// <summary>
        /// Get Threshold Value
        /// </summary>
        /// <returns></returns>       
        [ResponseType(typeof(ThresholdModel))]
        [Route("api/Helper/GetThresholdFXValue")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage GetThreshold(ParamThresholdFXModel paramhreshold)
        {
            ThresholdModel output = new ThresholdModel();
            if (!repository.GetThresholdFXValue(paramhreshold, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<ThresholdModel>(HttpStatusCode.OK, output);
            }
        }
        /// <summary>
        /// Get Threshold Value
        /// </summary>
        /// <returns></returns>       
        [ResponseType(typeof(ThresholdModel))]
        [Route("api/Helper/GetTotalTransactionIDRFCY")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage GetTotalTransactionIDRFCY(ParamThresholdModel paramhreshold)
        {
            TotalTransactionModel output = new TotalTransactionModel();
            if (!repository.GetTotalIDRFCY(paramhreshold, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<TotalTransactionModel>(HttpStatusCode.OK, output);
            }
        }


        /// <summary>
        /// Get Threshold Parameter
        /// </summary>
        /// <returns></returns>       
        [ResponseType(typeof(ThresholdModel))]
        [Route("api/Helper/ThresholdParameter")]
        [AcceptVerbs("GET")]
        public HttpResponseMessage GetTotalTransactionIDRFCY()
        {
            ThresholdParameterModel output = new ThresholdParameterModel();
            if (!repository.GetThresholdParameter(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<ThresholdParameterModel>(HttpStatusCode.OK, output);
            }
        }
        #endregion
        /// <summary>
        /// Get Dropdownlists by module
        /// </summary>
        /// <returns></returns>       
        [ResponseType(typeof(IDictionary<string, object>))]
        [Route("api/Helper/Dropdownlists/{select}/{module}")]
        [AcceptVerbs("GET")]
        public HttpResponseMessage GetDropdownLists(string select, string module)
        {
            IDictionary<string, object> output = new Dictionary<string, object>();
            string messages = string.Empty;
            if (!string.IsNullOrEmpty(select))
            {
                IList<DropdownListsModel> dropdownLists = new List<DropdownListsModel>();
                if (!repository.GetDropdownLists(ref dropdownLists, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                }
                else
                {
                    string[] val = select.Split(',');
                    foreach (string item in val)
                    {
                        output.Add(item, dropdownLists.Where(x => x.CatagoryName == item && x.Modulename.ToLower() == module.ToLower()));
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, output);
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
        }

        #region Agung
        [ResponseType(typeof(IList<TZReferenceModel>))]
        [HttpGet]
        [Route("api/Helper/TZRefTMO/Complete/Search")]
        public HttpResponseMessage SearchCompleteTZRefTMO(string query, int? limit, string cif)
        {
            IList<TZReferenceModel> TZRef = new List<TZReferenceModel>();

            if (!repository.GetCompleteTZRefTMO(query, limit.HasValue ? limit.Value : 10, cif, ref TZRef, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<TZReferenceModel>>(HttpStatusCode.OK, TZRef);
            }
        }
        #endregion

        #region basri
        /// <summary>
        /// Find Complete TZRef.
        /// </summary>
        /// <param name="query">TZRef.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="cif">CIF Customer</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<TZReferenceModel>))]
        [HttpGet]
        [Route("api/Helper/TZRef/Complete/Search")]
        public HttpResponseMessage SearchCompleteTZRef(string query, int? limit, string cif)
        {
            IList<TZReferenceModel> TZRef = new List<TZReferenceModel>();

            if (!repository.GetCompleteTZRef(query, limit.HasValue ? limit.Value : 10, cif, ref TZRef, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<TZReferenceModel>>(HttpStatusCode.OK, TZRef);
            }
        }

        /// <summary>
        /// Find Incomplete TZRef.
        /// </summary>
        /// <param name="query">TZRef.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<TZReferenceModel>))]
        [HttpGet]
        [Route("api/Helper/TZRef/Incomplete/Search")]
        public HttpResponseMessage SearchIncompleteTZRef(string query, int? limit, string cif)
        {
            IList<TZReferenceModel> TZRef = new List<TZReferenceModel>();

            if (!repository.GetIncompleteTZRef(query, limit.HasValue ? limit.Value : 10, cif, ref TZRef, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<TZReferenceModel>>(HttpStatusCode.OK, TZRef);
            }
        }

        /// <summary>
        /// GetTZRef.
        /// </summary>
        /// <param name="query">TZRef.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<TZTransactionModel>))]
        [HttpGet]
        [Route("api/Helper/TZRef/GetDetailTZReference")]
        public HttpResponseMessage GetDetailTZRef(string cif, string TZReference, string MurexNumber)
        {
            TZTransactionModel TZRefInfo = new TZTransactionModel();

            if (!repository.GetDetailTZRef(cif, TZReference, MurexNumber, ref TZRefInfo, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<TZTransactionModel>(HttpStatusCode.OK, TZRefInfo);
            }
        }

        [ResponseType(typeof(bool))]
        [HttpGet]
        [Route("api/Helper/GetLoanSettlementType/{id}")]
        public HttpResponseMessage GetLoanSettlementType(long id)
        {
            bool isScheduledSettlement = false;

            if (!repository.GetLoanSettlementType(id, ref isScheduledSettlement, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<bool>(HttpStatusCode.Created, isScheduledSettlement);
            }

        }

        [ResponseType(typeof(string))]
        [Route("api/Helper/CheckChangeRMCustomerUploaded")]
        [HttpPost]
        public HttpResponseMessage CheckChangeRMCustomerUploaded(IList<string> CollCIF)
        {
            string CIF = string.Empty;
            if (!repository.GetCIFFromUploadedData(CollCIF, ref CIF, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<string>(HttpStatusCode.Created, CIF);
            }
        }

        [ResponseType(typeof(string))]
        [Route("api/Helper/GetSwapDataTransaction/{MurexNumber}/{dfNumber}")]
        [HttpGet]
        public HttpResponseMessage GetSwapDataTransaction(string MurexNumber, string dfNumber)
        {
            SwapModel output = new SwapModel();
            if (!repository.GetSwapTransaction(MurexNumber, dfNumber, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<SwapModel>(HttpStatusCode.Created, output);
            }
        }
        [ResponseType(typeof(string))]
        [Route("api/Helper/GetNettingDataTransaction/{dfNumber}")]
        [HttpGet]
        public HttpResponseMessage GetSwapDataTransaction(string dfNumber)
        {
            SwapModel output = new SwapModel();
            if (!repository.GetNettingTransaction(dfNumber, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<SwapModel>(HttpStatusCode.Created, output);
            }
        }
        #endregion

        #region Agung
        [ResponseType(typeof(string))]
        [Route("api/Helper/GetTMONettingTransaction/{dfNumber}")]
        [HttpGet]
        public HttpResponseMessage GetTMONettingTransactions(string dfNumber)
        {
            SwapModel output = new SwapModel();
            if (!repository.GetTMONettingTransaction(dfNumber, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<SwapModel>(HttpStatusCode.Created, output);
            }
        }
        #endregion
        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}