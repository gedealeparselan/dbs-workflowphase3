﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Models
{
    #region Property
    [ModelName("Product")]
    public class ProductModel
    {
        /// <summary>
        /// Product ID.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Product Code.
        /// </summary>
        //[MaxLength(2, ErrorMessage = "Product Code is must be in {1} characters.")]
        public string Code { get; set; }

        /// <summary>
        /// Product Description.
        /// </summary>
        //[MaxLength(255, ErrorMessage = "Maximum length is {1} characters.")]

        public string Name { get; set; }

        public string IPEProductCode { get; set; }
        /// <summary>
        /// Workflow ID will determine the flow on the Payment Unit.
        /// </summary>
        public int WorkflowID { get; set; }

        /// <summary>
        /// Workflow will determine the flow on the Payment Unit.
        /// </summary>
        public string Workflow { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }
    }

    public class ProductRow : ProductModel
    {
        public int RowID { get; set; }

    }

    public class ProductGrid : Grid
    {
        public IList<ProductRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class ProductFilter : Filter { }
    #endregion

    #region Interface
    public interface IProductRepository : IDisposable
    {
        bool GetProductByID(int id, ref ProductModel Product, ref string message);
        bool GetProduct(ref IList<ProductModel> currencies, int limit, int index, ref string message);
        bool GetProduct(ref IList<ProductModel> currencies, ref string message);
        bool GetProduct(int page, int size, IList<ProductFilter> filters, string sortColumn, string sortOrder, ref ProductGrid Product, ref string message);
        bool GetProduct(ProductFilter filter, ref IList<ProductModel> currencies, ref string message);
        bool GetProduct(string key, int limit, ref IList<ProductModel> currencies, ref string message);
        bool AddProduct(ProductModel Product, ref string message);
        bool UpdateProduct(int id, ProductModel Product, ref string message);
        bool DeleteProduct(int id, ref string message);
    }
    #endregion

    #region Repository
    public class ProductRepository : IProductRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetProductByID(int id, ref ProductModel Product, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Product = (from a in context.Products
                           join b in context.ProductWorkflows on a.ProductWorkflowID equals b.WorkflowID
                           where a.IsDeleted.Equals(false) && b.IsDeleted.Equals(false) && a.ProductID.Equals(id)
                           select new ProductModel
                           {
                               ID = a.ProductID,
                               Code = a.ProductCode,
                               Name = a.ProductName,
                               IPEProductCode = a.IPEProductCode,
                               WorkflowID = b.WorkflowID,
                               Workflow = b.WorkflowName,
                               LastModifiedDate = a.CreateDate,
                               LastModifiedBy = a.CreateBy
                           }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetProduct(ref IList<ProductModel> Product, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    Product = (from a in context.Products
                               join b in context.ProductWorkflows on a.ProductWorkflowID equals b.WorkflowID
                               where a.IsDeleted.Equals(false) && b.IsDeleted.Equals(false)
                               orderby new { a.ProductCode, a.ProductName }
                               select new ProductModel
                               {
                                   ID = a.ProductID,
                                   Code = a.ProductCode,
                                   Name = a.ProductName,
                                   IPEProductCode = a.IPEProductCode,
                                   WorkflowID = b.WorkflowID,
                                   Workflow = b.WorkflowName,
                                   LastModifiedDate = a.CreateDate,
                                   LastModifiedBy = a.CreateBy
                               }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetProduct(ref IList<ProductModel> Product, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    Product = (from a in context.Products
                               join b in context.ProductWorkflows on a.ProductWorkflowID equals b.WorkflowID
                               where a.IsDeleted.Equals(false) && b.IsDeleted.Equals(false)
                               orderby new { a.ProductCode, a.ProductName }
                               select new ProductModel
                               {
                                   ID = a.ProductID,
                                   Code = a.ProductCode,
                                   Name = a.ProductName,
                                   IPEProductCode = a.IPEProductCode,
                                   WorkflowID = b.WorkflowID,
                                   Workflow = b.WorkflowName,
                                   LastModifiedDate = a.CreateDate,
                                   LastModifiedBy = a.CreateBy
                               }).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetProduct(int page, int size, IList<ProductFilter> filters, string sortColumn, string sortOrder, ref ProductGrid product, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                ProductModel filter = new ProductModel();
                if (filters != null)
                {
                    filter.Code = filters.Where(a => a.Field.Equals("Code")).Select(a => a.Value).SingleOrDefault();
                    filter.Name = filters.Where(a => a.Field.Equals("Name")).Select(a => a.Value).SingleOrDefault();
                    filter.IPEProductCode = filters.Where(a => a.Field.Equals("IPEProductCode")).Select(a => a.Value).SingleOrDefault();
                    filter.Workflow = filters.Where(a => a.Field.Equals("Workflow")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse(filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.Products
                                join b in context.ProductWorkflows on a.ProductWorkflowID equals b.WorkflowID
                                let isCode = !string.IsNullOrEmpty(filter.Code)
                                let isName = !string.IsNullOrEmpty(filter.Name)
                                let isIPEProductCode = !string.IsNullOrEmpty(filter.IPEProductCode)
                                let isWorkflow = !string.IsNullOrEmpty(filter.Workflow)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) &&
                                    (isCode ? a.ProductCode.Contains(filter.Code) : true) &&
                                    (isName ? a.ProductName.Contains(filter.Name) : true) &&
                                    (isIPEProductCode ? a.IPEProductCode.Contains(filter.IPEProductCode) : true) &&
                                    (isWorkflow ? b.WorkflowName.Contains(filter.Workflow) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new ProductModel
                                {
                                    ID = a.ProductID,
                                    Code = a.ProductCode,
                                    Name = a.ProductName,
                                    IPEProductCode = a.IPEProductCode,
                                    WorkflowID = b.WorkflowID,
                                    Workflow = b.WorkflowName,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    product.Page = page;
                    product.Size = size;
                    product.Total = data.Count();
                    product.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    product.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new ProductRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            Code = a.Code,
                            Name = a.Name,
                            IPEProductCode = a.IPEProductCode,
                            WorkflowID = a.WorkflowID,
                            Workflow = a.Workflow,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetProduct(ProductFilter filter, ref IList<ProductModel> customer, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetProduct(string key, int limit, ref IList<ProductModel> Product, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Product = (from a in context.Products
                           join b in context.ProductWorkflows on a.ProductWorkflowID equals b.WorkflowID
                           where a.IsDeleted.Equals(false) && b.IsDeleted.Equals(false) &&
                                (a.ProductName.Equals(key) || a.ProductCode.Contains(key) || b.WorkflowName.Contains(key))
                           select new ProductModel
                           {
                               ID = a.ProductID,
                               Code = a.ProductCode,
                               Name = a.ProductName,
                               IPEProductCode = a.IPEProductCode,
                               WorkflowID = b.WorkflowID,
                               Workflow = b.WorkflowName,
                               LastModifiedDate = a.CreateDate,
                               LastModifiedBy = a.CreateBy
                           }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddProduct(ProductModel Product, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (!context.Products.Where(a => a.ProductCode.Equals(Product.Code) && a.IsDeleted.Equals(false)).Any())
                {
                    context.Products.Add(new Entity.Product()
                    {
                        ProductCode = Product.Code,
                        ProductName = Product.Name,
                        IPEProductCode = Product.IPEProductCode,
                        ProductWorkflowID = Product.WorkflowID,
                        IsDeleted = false,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    });

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Product data for Product code {0} is already exist.", Product.Code);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateProduct(int id, ProductModel Product, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.Product data = context.Products.Where(a => a.ProductID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    if (context.Products.Where(a => a.ProductID != Product.ID && a.ProductCode.Equals(Product.Code) && a.IsDeleted.Equals(false)).Count() >= 1)
                    {
                        message = string.Format("Product Code {0} is exist.", Product.Code);
                    }
                    else
                    {
                        data.ProductCode = Product.Code;
                        data.ProductName = Product.Name;
                        data.IPEProductCode = Product.IPEProductCode;
                        data.ProductWorkflowID = Product.WorkflowID;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        context.SaveChanges();

                        isSuccess = true;
                    }
                }
                else
                {
                    message = string.Format("Product data for Product ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeleteProduct(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.Product data = context.Products.Where(a => a.ProductID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Product data for Product ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}