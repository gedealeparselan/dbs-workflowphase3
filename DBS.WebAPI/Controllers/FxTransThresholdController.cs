﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    public class FXTransThresholdController : ApiController
    {
        private string message = string.Empty;

        private readonly IFXTransThresholdRepository repository = null;

        public FXTransThresholdController()
        {
            this.repository = new FXTransThresholdRepository();
        }

        public FXTransThresholdController(IFXTransThresholdRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all currencies.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<FXTransThresholdModel>))]
        public HttpResponseMessage Get()
        {
            IList<FXTransThresholdModel> output = new List<FXTransThresholdModel>();

            if (!repository.GetFXTransThreshold(ref output, 10, 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<FXTransThresholdModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get currencies with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<FXTransThresholdModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<FXTransThresholdModel> output = new List<FXTransThresholdModel>();

            if (!repository.GetFXTransThreshold(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<IList<FXTransThresholdModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get currencies for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(FXTransThresholdGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<FXTransThresholdFilter> filters)
        {
            FXTransThresholdGrid output = new FXTransThresholdGrid();

            if (!repository.GetFXTransThreshold(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "ThresholdAmount" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<FXTransThresholdGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find currencies using criterias Code or Description.
        /// </summary>
        /// <param name="query">Code or Description.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
      /*   [ResponseType(typeof(IList<FXTransThreshold>))]
        [HttpGet]
        [Route("api/FXTransThreshold/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
           IList<FXTransThreshold> output = new List<FXTransThreshold>();

            if (!repository.GetFXTransThreshold(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<FXTransThreshold>>(HttpStatusCode.OK, output);
            }
        } */

        /// <summary>
        /// Get FXTransThreshold by ID.
        /// </summary>
        /// <param name="id">FXTransThreshold ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(FXTransThresholdModel))]
        [Route("api/FXTransThreshold/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            FXTransThresholdModel output = new FXTransThresholdModel();

            if (!repository.GetFXTransThresholdByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<FXTransThresholdModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new FXTransThreshold.
        /// </summary>
        /// <param name="FXTransThreshold">Cumrrency data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Post(FXTransThresholdModel fXTransThreshold)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddFXTransThreshold(fXTransThreshold, ref message))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse<string>(HttpStatusCode.Created, "New FXTransThreshold data has been created.");
                }
            }
        }

        /// <summary>
        /// Modify existing FXTransThreshold.
        /// </summary>
        /// <param name="id">ID of FXTransThreshold to be modify.</param>
        /// <param name="customer">FXTransThreshold data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/FXTransThreshold/{id}")]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(int id, FXTransThresholdModel fXTransThreshold)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateFXTransThreshold(id, fXTransThreshold, ref message))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse<string>(HttpStatusCode.OK, "Transaction Threshold data has been updated.");
                }
            }
        }

        /// <summary>
        /// Remove exisiting FXTransThreshold.
        /// </summary>
        /// <param name="id">ID of FXTransThreshold to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/FXTransThreshold/{id}")]
        //[Authorize(Roles = "DBS Admin")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteFXTransThreshold(id, ref message))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse<string>(HttpStatusCode.OK, "Transaction Threshold data has been deleted.");
                }
            }
        }
        
        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}