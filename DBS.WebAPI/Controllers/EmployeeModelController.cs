﻿using DBS.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class EmployeeModelController : ApiController
    {
        private string message = string.Empty;

        private readonly IEmployeeModelRepository repository = null;

        public EmployeeModelController()
        {
            this.repository = new EmployeeModelRepository();
        }

        public EmployeeModelController(IEmployeeModelRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all UserApproval.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<EmployeeModel>))]
        public HttpResponseMessage Get()
        {
            IList<EmployeeModel> output = new List<EmployeeModel>();

            if (!repository.GetUserApproval(ref output, 10, 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<EmployeeModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get all UserApprovalDraft.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<EmployeeModel>))]
        [Route("api/EmployeeModel/Draft/All")]
        public HttpResponseMessage GetAllDraft()
        {
            IList<EmployeeModel> output = new List<EmployeeModel>();

            if (!repository.GetUserApprovalDraft(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<EmployeeModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get products with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<EmployeeModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<EmployeeModel> output = new List<EmployeeModel>();

            if (!repository.GetUserApproval(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<IList<EmployeeModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get employee for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(EmployeeModelGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<EmployeeModelFilter> filters)
        {
            EmployeeModelGrid output = new EmployeeModelGrid();

            if (!repository.GetUserApproval(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Employee Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<EmployeeModelGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get employee draft for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(EmployeeModelGrid))]
        [AcceptVerbs("GET", "POST")]
        [Route("api/EmployeeModel/Draft")]
        public HttpResponseMessage GetDraft(int? page, int? size, string sort_column, string sort_order, IList<EmployeeModelFilter> filters)
        {
            EmployeeModelGrid output = new EmployeeModelGrid();

            if (!repository.GetUserApprovalDraft(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Employee Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<EmployeeModelGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find products using criterias Code or Description.
        /// </summary>
        /// <param name="query">Code or Description.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<EmployeeModel>))]
        [HttpGet]
        [Route("api/EmployeeModel/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<EmployeeModel> output = new List<EmployeeModel>();

            if (!repository.GetUserApproval(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<EmployeeModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Employee by ID.
        /// </summary>
        /// <param name="id">Employee ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(EmployeeModel))]
        [Route("api/EmployeeModel/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(string id)
        {
            EmployeeModel output = new EmployeeModel();

            if (!repository.GetUserApprovalByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<EmployeeModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Employee Draft by ID.
        /// </summary>
        /// <param name="id">Employee ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(EmployeeModel))]
        [Route("api/EmployeeModel/Draft/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(Guid id)
        {
            EmployeeModel output = new EmployeeModel();

            if (!repository.GetUserApprovalDraftByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<EmployeeModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new Employee.
        /// </summary>
        /// <param name="product">Product data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(EmployeeModel UserApproval)
        {
            long EmployeeID = 0;

            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddUserApproval(UserApproval, ref EmployeeID, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { ID = EmployeeID, Message = message });
                }
            }
        }

        /// <summary>
        /// Update Employee.
        /// </summary>
        /// <param name="product">Product data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        [Route("api/EmployeeModel/Update")]
        public HttpResponseMessage Update(EmployeeModel UserApproval)
        {
            long EmployeeID = 0;

            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateUserApproval(UserApproval, ref EmployeeID, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { ID = EmployeeID, Message = message });
                }
            }
        }
        /// <summary>
        /// Andi,7 October 2015
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        [ResponseType(typeof(BranchModel))]
        [Route("api/EmployeeModel/Loc/{username}")]
        [HttpGet]
        public HttpResponseMessage GetEmployeeLocation(string username)
        {
            BranchModel output = new BranchModel();

            if (!repository.GetEmployeeLocation(username.Trim(), ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<BranchModel>(HttpStatusCode.OK, output);
            }
        }
        /// <summary>
        /// Remove exisiting employee draft.
        /// </summary>
        /// <param name="id">ID of channel to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/EmployeeModel/{id}")]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteEmployeeDraft(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Draft data has been deleted." });
                }
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}