﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FXTracking_TMONettingUserControl.ascx.cs" Inherits="DBS.Sharepoint.SPWebparts.FXTracking.FXTracking_TMONetting.FXTracking_TMONettingUserControl" %>

<div id="new-transaction">
    <h1 class="header smaller no-margin-top lighter dark">New Netting Transaction</h1>
    <div class="modal-body overflow-visible">
        <div class="row" data-bind="with: TransactionNetting">
            <div class="col-xs-12">
                <div id="customer-form" class="form-horizontal" role="form">
                    <div class="form-group">
                        <label class="col-sm-3 control-label bolder no-padding-right">
                            <span data-bind="text: $root.LocalDate(CreateDate(), true, true)"></span>
                        </label>

                        <label class="col-sm-4 pull-right">
                            <b>Application ID</b> : <span data-bind="text: ApplicationID"></span><br />
                            <b>User Name</b> : <span data-bind="text: $root.SPUser().DisplayName"></span>
                        </label>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="customer-name">
						Customer Name</label>

                        <div class="col-sm-9">
                            <div class="clearfix">
                                <input type="text" class="col-xs-12 col-sm-5" name="customer-name" id="customer-name" data-bind="value: Customer().Name" data-rule-required="true" />
                                <label class="control-label bolder text-danger" for="customer-name">
								*</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="cif">
						CIF</label>
                        <div class="col-sm-9">
                            <div class="clearfix">
                                <input type="text" class="col-sm-3" name="cif" id="cif" data-bind="value: Customer().CIF, disable:true" class="col-xs-10 col-sm-2" data-rule-required="true"/>
                                    <label class="control-label bolder text-danger" for="cif">
								*</label>
                            </div>
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="netting-purpose">Netting Purpose</label>
                        <div class="col-sm-9">
                            <div class="clearfix">
                                <select id="netting-purpose" name="netting-purpose" data-bind="value: $root.Selected().NettingPurpose, options: $root.Parameter().NettingPurpose, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Please Select...', event: {click: OnChangeNettingPurpose}" data-rule-required="true" class="col-xs-3"></select>
                                <label class="control-label bolder text-danger" for="netting-purpose">*</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="new-tz-reference">
						Deal Number</label>
                        <div class="col-sm-9">
                            <div class="clearfix">
                                <input type="text" id="tz-reference" name="tz-reference" class="col-sm-3" data-rule-required="true" data-bind="value: TZReference" />
                                <label class="control-label bolder text-danger" for="tz-reference">
								*</label>&nbsp;&nbsp;
                                <input type="text" id="murex" name="murex" data-bind="value: MurexNumber" disabled="disabled"/>                                    
                            </div>
                        </div>                           
                    </div>
                    <div class="form-group" data-bind="visible: !$root.Unwind()">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="netting-tz-reference">
						Netting Deal Number</label>
                        <div class="col-sm-9">
                            <div class="clearfix">
                                <div class="dataTables_wrapper col-sm-6 no-padding" role="grid">
                                    <table class="table table-striped table-bordered table-hover dataTable">
                                        <thead>
                                            <tr>
                                                <th>DF</th>
                                                <th>SWAP DF</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody data-bind="foreach: ''">
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td><span data-bind="click: ''"><i class="icon-edit bigger-110"></i></span><span data-bind="click: ''"><i class="icon-delete bigger-110"></i></span></td>
                                            </tr>
                                        </tbody>
                                        <tbody data-bind="visible: ''">
                                            <tr>
                                                <td colspan="3" class="text-center">
                                                    No entries available.
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class='col-sm-3'>
                                    <button class="btn btn-sm btn-primary pull-left" data-bind="click: $root.AddNetting">Add</button>
                                </div>                               
                            </div>
                        </div>                            
                    </div>
                    <div class="form-group" data-bind="visible: $root.Unwind()">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="netting-tz-reference-unwind">
						Netting Deal Number</label>
                        <div class="col-sm-9">
                            <div class="clearfix">                                
                                <input type="text" id="netting-tz-reference-unwind" name="netting-tz-reference-unwind" class="col-sm-3" data-rule-required="true" data-bind="value: NettingTZReference" />
                                <label class="control-label bolder text-danger" for="netting-tz-reference-unwind">
								*</label>&nbsp;&nbsp; 
                                <input type="text" id="swapdeal" name="swapdeal" data-bind="value: SwapDealNumber" disabled="disabled" />                                
                            </div>
                        </div>                            
                    </div>
                    <div class="form-group" data-bind="visible: !$root.Unwind()">
                        <label class="col-sm-3 control-label bolder no-padding-right">
						After Netting Deal Number</label>
                        <div class="col-sm-9">
                            <div class="clearfix">
                                <div class="dataTables_wrapper col-sm-6 no-padding" role="grid">
                                    <table class="table table-striped table-bordered table-hover dataTable">
                                        <thead>
                                            <tr>
                                                <th>DF</th>
                                                <th>SWAP DF</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody data-bind="foreach: ''">
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td><span data-bind="click: ''"><i class="icon-edit bigger-110"></i></span><span data-bind="    click: ''"><i class="icon-delete bigger-110"></i></span></td>
                                            </tr>
                                        </tbody>
                                        <tbody data-bind="visible: ''">
                                            <tr>
                                                <td colspan="3" class="text-center">
                                                    No entries available.
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class='col-sm-3'>
                                    <button class="btn btn-sm btn-primary pull-left" data-bind="click: $root.AddAfterNetting">Add</button>
                                </div>                                                               
                            </div>
                        </div>
                            
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="value-date">
						Expected Submission Date Statement Letter</label>
                        <div class="col-sm-9">
                            <div class="clearfix input-group col-sm-3 no-padding">
                                <input class="form-control date-picker" type="text" data-date-format="dd-M-yyyy" id="expected-date-statement-letter" name="expected-date-statement-letter" data-bind="value: ExpectedDateStatementLetter">
                                <span class="input-group-addon">
                                    <i class="icon-calendar bigger-110"></i>
                                </span>                                
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="value-date">
						Expected Submission Date Underlying</label>
                        <div class="col-sm-9">
                            <div class="clearfix input-group col-sm-3 no-padding">
                                <input class="form-control date-picker" type="text" data-date-format="dd-M-yyyy" id="expected-date-underlying" name="expected-date-underlying" data-bind="value: ExpectedDateUnderlying">
                                <span class="input-group-addon">
                                    <i class="icon-calendar bigger-110"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="value-date">
						Actual Submission Date Statement Letter</label>
                        <div class="col-sm-9">
                            <div class="clearfix input-group col-sm-3 no-padding">
                                <input class="form-control date-picker" type="text" data-date-format="dd-M-yyyy" id="actual-date-statement-letter" name="actual-date-statement-letter" data-bind="value: ActualDateStatementLetter">
                                <span class="input-group-addon">
                                    <i class="icon-calendar bigger-110"></i>
                                </span>                                
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="value-date">
						Actual Submission Date Underlying</label>
                        <div class="col-sm-9">
                            <div class="clearfix input-group col-sm-3 no-padding">
                                <input class="form-control date-picker" type="text" data-date-format="dd-M-yyyy" id="actual-date-underlying" name="actual-date-underlying" data-bind="value: ActualDateUnderlying">
                                <span class="input-group-addon">
                                    <i class="icon-calendar bigger-110"></i>
                                </span>
                            </div>
                        </div>
                    </div>                      
                   
                    <div class="form-group">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="Remarks">
						Remarks</label>

						<div class="col-xs-9 align-left">
							<div class="clearfix">
								<textarea class="col-lg-10" id="remarks" rows="6" data-bind="value: Remarks"></textarea>								    
							</div>
						</div>
                    </div>
                        
                    <div class="space-12"></div>
                    <div class="padding-4" data-bind="visible: $root.IsTZAvailable">                            
                        <div class="table-responsive">
                            <div class="dataTables_wrapper" role="grid">
                                <table class="table table-striped table-bordered table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Statement Letter</th>
                                            <th>Underlying</th>
                                            <th>Type of Docs</th>
                                            <th>Currency</th>
                                            <th>Amount</th>
                                            <th>Available Amount in USD</th>
                                            <th>Date</th>
                                            <th>Supllier Name</th>
                                            <th>Ref. Number</th>
                                            <th style="width:50px">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody data-bind="foreach: $root.CustomerUnderlyings, visible: $root.CustomerUnderlyings().length > 0"">
                                        <tr>
                                            <td><span data-bind="text: $index() +1"></span></td>
                                            <td><span data-bind="text: StatementLetter.Name"></span></td>
                                            <td><span data-bind="text: UnderlyingDocument.Name"></span></td>
                                            <td><span data-bind="text: DocumentType.Name"></span></td>
                                            <td><span data-bind="text: Currency.Code"></span></td>
                                            <td><span data-bind="text: $root.FormatNumber(Amount)"></span></td>
                                            <td><span data-bind="text: $root.FormatNumber(AvailableAmount)"></span></td>
                                            <td><span data-bind="text: $root.LocalDate(ExpiredDate, true, false)"></span></td>
                                            <td><span data-bind="text: SupplierName"></span></td>
                                            <td><span data-bind="text: ReferenceNumber"></span></td>
                                            <td><span class="label label-info arrowed-in-right arrowed" data-target="#modal-form-Underlying" data-bind="click: $root.GetUnderlyingSelectedRow,text:IsUtilize?'view':'Update'"></span></td>
                                        </tr>
                                    </tbody>
                                    <tbody data-bind="visible: $root.CustomerUnderlyings().length == 0">
                                        <tr>
                                            <td colspan="11" class="text-center">
                                                No 
											    entries 
											    available.
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--- Added form custome Customer Underlying End -->

                    <div class="space-4"></div>
                      
                    <div  data-bind="visible: $root.IsTZAvailable">
                        <div class="form-horizontal" role="form">
                            <div class="form-group">
                                <label class="col-sm-3 control-label bolder no-padding-right" for="utilization-amount">
								Total Underlying</label>

                                <div class="col-sm-9">
                                    <div class="clearfix">                                        
                                        <input type="text" class="col-sm-4 align-right" name="utilization-amount" id="utilization-amount" data-rule-required="false" disabled="disabled" data-rule-number="true" data-bind="value : formatNumber_r($root.utilizationAmount())" />
                                       <%-- <input type="text" class="col-sm-4 align-right" name="utilization-amount" id="utilization-amount" data-rule-required="false" disabled="disabled" data-rule-number="true" data-bind="value : formatNumber_r($root.AmountModel().TotalDealUtilization())" />--%>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label bolder no-padding-right" for="total-utilization">
								Total Utilization</label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <input type="text" class="col-sm-4 align-right" name="utilization-amount" id="TotalAmountTrx" data-rule-required="false" disabled="disabled" data-rule-number="true" data-bind="value : formatNumber_r($root.AmountModel().TotalDealAmountsUSD())" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!---Grid Underlying Proforma Start -->
                    <!-- widget header start -->
                    <div class="space-16"></div>                        
                    <div class="padding-4" data-bind="visible: $root.IsTZAvailable">  
                        <h3 class="header smaller lighter dark">
                        Instruction Form, Underlying and Document
                            <!--   <button class="btn btn-sm btn-primary pull-right" data-bind="click: $root.UploadDocumentUnderlying, disable: !$root.IsEditable(),visible:(Currency().Code != 'IDR' && Account().Currency.Code == 'IDR')||(Currency().Code == 'IDR' && Account().Currency.Code != 'IDR' && ProductType().IsFlowValas==true && $root.fixAmountUSD()>=1000000)"> -->
                            <!--<button class="btn btn-sm btn-primary pull-right" data-bind="click: $root.UploadDocumentUnderlying, disable: !$root.IsEditable()">
                                    <i class="icon-plus"></i>
                                    Attach
                            </button> -->                               
                        </h3>
                        <div class="dataTables_wrapper" role="grid">
                            <table class="table table-striped table-bordered table-hover dataTable">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>File Name</th>
                                        <th>Purpose of Docs</th>
                                        <th>Type of Docs</th>
                                        <th>Modified</th>                                        
                                    </tr>
                                </thead>
                                <tbody data-bind="foreach: $root.CustomerDocuments, visible: $root.CustomerDocuments().length > 0">
                                    <tr>
                                        <td><span data-bind="text: $index() +1"></span></td>
                                        <td>
                                            <div data-bind="if:IsNewDocument">
                                                <span data-bind="text: FileName"></span>
                                            </div>
                                            <div data-bind="ifnot:IsNewDocument">
                                                <a data-bind="attr: { href: DocumentPath, target:'_blank' }, text: FileName"></a>
                                            </div>
                                        </td>
                                        <td><span data-bind="text: Purpose.Name"></span></td>
                                        <td><span data-bind="text: Type.Name"></span></td>
                                        <td><span data-bind="text: $root.LocalDate(LastModifiedDate)"></span></td>                                        
                                    </tr>
                                </tbody>
                                <!--<tbody data-bind="foreach: Transaction.Documents, visible: Transaction.Documents.length > 0">
                                <tr>
                                    <td><span data-bind="text: $index() +1"></span></td>
                                    <td><a data-bind="attr: { href: DocumentPath }, text: FileName"></a></td>
                                    <td><span data-bind="text: Purpose.Name"></span></td>
                                    <td><span data-bind="text: Type.Name"></span></td>
                                    <td><span data-bind="text: $root.LocalDate(LastModifiedDate)"></span></td>
                                    <td><a href="#" data-bind="click: $root.RemoveDocumentData">Remove</a></td>
                                </tr>
                                </tbody>-->
                                <tbody data-bind="visible: $root.CustomerDocuments().length == 0">
                                    <tr>
                                        <td colspan="5" class="text-center">
                                            No entries available.
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div> 
                            
                    <!--- Grid Underlying Proforma End -->

                </div>
            </div>
        </div>
    </div>

        <div class="space-8"></div>

        <div class="row">
            <div class="col-sm-12 align-right">
                <button class="btn btn-sm btn-info" data-bind="click: SaveDraftNettingTransaction, disable: !IsEnableSubmit()"><i class="icon-ok bigger-110"></i>
                    Draft
                </button>                    
                <button class="btn btn-sm btn-primary" data-bind="click: SaveNettingTransaction, disable: !IsEnableSubmit()">
                    Submit
                </button>
            </div>
        </div>

        <!-- modal form start Attach Document Underlying Form start -->
        <div id="modal-form-Attach" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
            <div class="modal-dialog" style="width:100%">
                <div class="modal-content">
                    <!-- modal header start Attach FIle Form -->
                    <div class="modal-header">
                        <h4 class="blue bigger">
                            <span>Added Attachment File</span>
                        </h4>
                    </div>
                    <!-- modal header end Attach file -->
                    <!-- modal body start Attach File -->
                    <div class="modal-body overflow-visible">
                        <div class="row">
                            <div class="col-xs-12">
                                <!-- modal body form start -->
                                <div id="customer-form" class="form-horizontal" role="form">
                                    <div class="space-4"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="Name">
                                            Purpose of Doc
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <select id="documentPurpose" name="documentPurpose" data-bind="value:$root.DocumentPurpose_a().ID, options: ddlDocumentPurpose_a, optionsText: 'Name',optionsValue: 'ID',optionsCaption: 'Please Select...'" data-rule-required="true" class="col-xs-7"></select>
                                                <label class="control-label bolder text-danger" for="Name">*</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="space-4"></div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="description">
                                            Type of Doc
                                        </label>

                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <select id="documentType" name="documentType" data-bind="value:$root.DocumentType_a().ID, options: ddlDocumentType_a, optionsText: 'Name',optionsValue: 'ID',optionsCaption: 'Please Select...'" data-rule-required="true" class="col-xs-7"></select>
                                                <label class="control-label bolder text-danger">*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space-4"></div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="document-path-upload">
										Document</label>

                                        <div class="col-sm-7">
                                            <div class="clearfix">
                                                <input type="file"  id="document-path-upload" name="document-path-upload" data-bind="file: DocumentPath_a" />
                                            </div>
                                        </div>
                                        <label class="control-label bolder text-danger">*</label>
                                    </div>
                                </div>
                                <!-- modal body form Attach File End -->
                                <!-- widget box Underlying Attach start -->
                                <div id="widget-box" class="widget-box">
                                    <!-- widget header Underlying Attach start -->
                                    <div class="widget-header widget-hea1der-small header-color-dark">
                                        <h6>Underlying Form Table</h6>

                                        <div class="widget-toolbar">
                                            <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                                <i class="blue icon-filter"></i>
                                                <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: UnderlyingAttachGridProperties().AllowFilter" />
                                                <span class="lbl"></span>
                                            </label>
                                            <a href="#" data-bind="click: GetDataUnderlyingAttach" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                                <i class="blue icon-refresh"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <!-- widget header end -->
                                    <!-- widget body underlying Attach start -->
                                    <div class="widget-body">
                                        <!-- widget main start -->
                                        <div class="widget-main padding-0">
                                            <!-- widget slim control start -->
                                            <div class="slim-scroll" data-height="400">
                                                <!-- widget content start -->
                                                <div class="content">
                                                    <!-- table responsive start -->
                                                    <div class="table-responsive">
                                                        <div class="dataTables_wrapper" role="grid">
                                                            <table id="Customer Underlying-table" class="table table-striped table-bordered table-hover dataTable">
                                                                <thead>
                                                                    <tr data-bind="with: $root.UnderlyingAttachGridProperties">
                                                                        <!-- <th style="width:50px">Select</th> -->
                                                                        <th style="width:50px">
																		No.</th>
                                                                        <th style="width:50px">
																		Select</th>
                                                                        <th data-bind="click: function () { Sorting('StatementLetter'); }, css: GetSortedColumn('StatementLetter')">
																		Statement 
																		Letter</th>
                                                                        <th data-bind="click: function () { Sorting('UnderlyingDocument'); }, css: GetSortedColumn('UnderlyingDocument')">
																		Underlying 
																		Document</th>
                                                                        <th data-bind="click: function () { Sorting('DocumentType'); }, css: GetSortedColumn('DocumentType')">
																		Type 
																		of 
																		Doc</th>
                                                                        <th data-bind="click: function () { Sorting('Currency'); }, css: GetSortedColumn('Currency')">
																		Currency</th>
                                                                        <th data-bind="click: function () { Sorting('Amount'); }, css: GetSortedColumn('Amount')">
																		Amount</th>
                                                                        <th data-bind="click: function () { Sorting('AvailableAmount'); }, css: GetSortedColumn('AvailableAmount')">
																		AvailableAmount</th>
                                                                        <th data-bind="click: function () { Sorting('DateOfUnderlying'); }, css: GetSortedColumn('DateOfUnderlying')">
																		Date</th>
                                                                        <th data-bind="click: function () { Sorting('SupplierName'); }, css: GetSortedColumn('SupplierName')">
																		Supplier 
																		Name</th>
                                                                        <!-- <th data-bind="click: function () { Sorting('ReferenceNumber'); }, css: GetSortedColumn('ReferenceNumber')">Reference Number</th> -->
                                                                        <th data-bind="click: function () { Sorting('ExpiredDate'); }, css: GetSortedColumn('ExpiredDate')">
																		Expiry 
																		Date</th>
                                                                    </tr>
                                                                </thead>
                                                                <thead data-bind="visible: $root.UnderlyingAttachGridProperties().AllowFilter" class="table-filter">
                                                                    <tr>
                                                                        <th class="clear-filter">
                                                                            <a href="#" data-bind="click: $root.UnderlyingAttachClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                                <i class="green icon-trash"></i>
                                                                            </a>
                                                                        </th>
                                                                        <th class="clear-filter"></th>
                                                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterStatementLetter, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterUnderlyingDocument, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterDocumentType, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterCurrency, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                        <th><input type="text" class="input-sm col-xs-12 input-numericonly" data-bind="value: $root.UnderlyingAttachFilterAmount, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                        <th><input type="text" class="input-sm col-xs-12 input-numericonly" data-bind="value: $root.UnderlyingAttachFilterAvailableAmount, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                        <th><input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.UnderlyingAttachFilterDateOfUnderlying, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterSupplierName, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                        <!-- <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterReferenceNumber, event: { change: UnderlyingAttachGridProperties().Filter }" /></th> -->
                                                                        <th><input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.UnderlyingAttachFilterExpiredDate, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody data-bind="foreach: {data: $root.CustomerAttachUnderlyings, as: 'AttachData'}, visible: $root.CustomerAttachUnderlyings().length > 0">
                                                                    <tr>
                                                                        <td><span data-bind="text: RowID"></span></td>
                                                                        <td align="center"><input type="checkbox" id="isSelected2" name="isSelect2" data-bind="checked:IsSelectedAttach,event:{change:function(data){$root.onSelectionAttach($index(),$data)}}, enable: $root.SelectingUnderlying" /></td>
                                                                        <td><span data-bind="text: StatementLetter.Name"></span></td>
                                                                        <td><span data-bind="text: UnderlyingDocument.Name"></span></td>
                                                                        <td><span data-bind="text: DocumentType.Name"></span></td>
                                                                        <td><span data-bind="text: Currency.Code"></span></td>
                                                                        <td align="right"><span data-bind="text: formatNumber(Amount)"></span></td>
                                                                        <td align="right"><span data-bind="text: formatNumber(AvailableAmount)"></span></td>
                                                                        <td><span data-bind="text: $root.LocalDate(DateOfUnderlying,true,false)"></span></td>
                                                                        <td><span data-bind="text: SupplierName"></span></td>
                                                                        <!-- <td><span data-bind="text: ReferenceNumber"></span></td> -->
                                                                        <td><span data-bind="text: $root.LocalDate(ExpiredDate,true,false)"></span></td>
                                                                    </tr>
                                                                </tbody>
                                                                <tbody data-bind="visible: $root.CustomerAttachUnderlyings().length == 0">
                                                                    <tr>
                                                                        <td colspan="11" class="text-center">
                                                                            No entries available.
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <!-- table responsive end -->
                                                </div>
                                                <!-- widget content end -->
                                            </div>
                                            <!-- widget slim control end -->
                                            <!-- widget footer start -->
                                            <div class="widget-toolbox padding-8 clearfix">
                                                <div class="row" data-bind="with: UnderlyingAttachGridProperties">
                                                    <!-- pagination size start -->
                                                    <div class="col-sm-6">
                                                        <div class="dataTables_paginate paging_bootstrap pull-left">
                                                            Showing <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                                            rows of <span data-bind="text: Total"></span>
                                                            entries
                                                        </div>
                                                    </div>
                                                    <!-- pagination size end -->
                                                    <!-- pagination page jump start -->
                                                    <div class="col-sm-3">
                                                        <div class="dataTables_paginate paging_bootstrap">
                                                            Page <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width:50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                            of <span data-bind="text: TotalPages"></span>
                                                        </div>
                                                    </div>
                                                    <!-- pagination page jump end -->
                                                    <!-- pagination navigation start -->
                                                    <div class="col-sm-3">
                                                        <div class="dataTables_paginate paging_bootstrap">
                                                            <ul class="pagination">
                                                                <li data-bind="click: FirstPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                        <i class="icon-double-angle-left"></i>
                                                                    </a>
                                                                </li>
                                                                <li data-bind="click: PreviousPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                        <i class="icon-angle-left"></i>
                                                                    </a>
                                                                </li>
                                                                <li data-bind="click: NextPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                        <i class="icon-angle-right"></i>
                                                                    </a>
                                                                </li>
                                                                <li data-bind="click: LastPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                        <i class="icon-double-angle-right"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <!-- pagination navigation end -->

                                                </div>
                                            </div>
                                            <!-- widget footer end -->

                                        </div>
                                        <!-- widget main end -->
                                    </div>
                                    <!-- widget body end -->
                                </div>
                                <!-- widget box end -->
                            </div>
                        </div>
                    </div>
                    <!-- modal body end Attach File -->
                    <!-- modal footer start Attach File-->
                    <div class="modal-footer">
                        <button class="btn btn-sm btn-primary" data-bind="click: $root.save_a, disable: !IsEditable(), disable: $root.IsUploading()">
                            <i class="icon-save"></i>
                            Save
                        </button>
                        <button class="btn btn-sm" data-dismiss="modal" data-bind="click: $root.cancel_a">
                            <i class="icon-remove"></i>
                            Cancel
                        </button>
                    </div>
                    <!-- modal footer end attach file -->

                </div>
            </div>
        </div>
        <!-- modal form end Attach Document Underlying Form end -->            

        <!-- Modal upload form start -->
        <div id="modal-form-upload" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body overflow-visible">
                        <div class="row">
                            <div class="col-xs-12">
                                <div id="customer-form" class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="document-type">
										Document Type</label>

                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <select id="document-type" name="document-type" data-rule-required="true" data-bind="options: Parameter().DocumentTypes, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Options..', value: Selected().DocumentType"></select>
                                                    <label class="control-label bolder text-danger">*</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="space-4"></div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="document-purpose">
										Purpose of Docs</label>

                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <select id="document-purpose" name="document-purpose" data-rule-required="true" data-bind="options: Parameter().DocumentPurposes, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Options..', value: Selected().DocumentPurpose"></select>
                                                    <label class="control-label bolder text-danger">*</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="space-4"></div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="document-path">
										Document</label>
                                        <div class="col-sm-7">
                                            <div class="clearfix">
                                                <input type="file" id="document-path" name="document-path" data-bind="file: DocumentPath" /><!---->
                                            </div>
                                        </div>
                                            <label class="control-label bolder text-danger">*</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button class="btn btn-sm btn-primary" data-bind="click: $root.AddDocument, disable: !IsEditable()">
                            <i class="icon-save"></i>
                            Save
                        </button>
                        <button class="btn btn-sm" data-dismiss="modal">
                            <i class="icon-remove"></i>
                            Cancel
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal upload form end -->

        <!-- modal form start Underlying Form -->
        <div id="modal-form-Underlying" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
            <div class="modal-dialog" style="width: 100%">
                <div class="modal-content">
                    <!-- modal header start Underlying Form -->
                    <div class="modal-header">
                        <h4 class="blue bigger">
                            <span data-bind="if: $root.IsNewDataUnderlying()">Create a new Customer Underlying Parameter</span>
                            <span data-bind="if: !$root.IsNewDataUnderlying()">Modify a Customer Underlying Parameter</span>
                        </h4>
                    </div>
                    <!-- modal header end -->
                    <!-- modal body start -->
                    <div class="modal-body overflow-visible">
                        <div id="modalUnderlyingtest">
                            <div class="row">
                                <div class="col-xs-12">
                                    <!-- modal body form start -->

                                    <div id="customerUnderlyingform" class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="JointAccountID">
                                                Joint Account</label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <select id="book-joinAcc2" name="book-statement" data-bind="value: $root.IsJointAccount_u,
    options: $root.JointAccounts,
    optionsText: 'Name',
    optionsValue: 'ID',
    event: { change: $root.OnChangeJointAccUnderlying },enable:$root.IsJointAccount"
                                                        class="col-xs-3">
                                                    </select>&nbsp;      
                                                                                        <select id="debit-acc-number" name="debit-acc-number" data-rule-required="true" data-bind="options: $root.JointAccountNumbers,
    optionsText: function (item) {
        if (item.CustomerName != null) {
            return item.AccountNumber + ' (' + item.Currency.Code + ') - ' + item.CustomerName
        } return item.AccountNumber + ' (' + item.Currency.Code + ')'                                          
    },
    optionsValue: 'AccountNumber',
    optionsCaption: 'Please Select...',
    value: $root.AccountNumber_u,
    event: { change: $root.OnChangeAccountNumber },visible:$root.IsJointAccount_u"
                                                                                            class="col-xs-5">
                                                                                        </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="StatementLetterID">
                                                Statement Letter
                                            </label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <select id="statementLetter" name="statementLetter" data-bind="value:$root.StatementLetter_u().ID, options: ddlStatementLetter_u, optionsText: 'Name',optionsValue: 'ID',optionsCaption: 'Please Select...',enable:$root.IsStatementA(),event: { change: $root.OnChangeStatementLetterUnderlying }" data-rule-required="true" class="col-xs-7"></select>
                                                    <label class="control-label bolder text-danger" for="customer-name">*</label>
                                                    <!--   <select id="statementLetter" name="statementLetter" data-bind="value:$root.StatementLetter_u().ID, options: ddlStatementLetter_u, optionsText: 'Name',optionsValue: 'ID',optionsCaption: 'Please Select...'" data-rule-required="true" class="col-xs-7" disabled="disabled"></select> -->
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="UnderlyingDocument">
                                                Underlying Document
                                            </label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <select id="underlyingDocument" name="underlyingDocument" data-bind="value:UnderlyingDocument_u().ID, options: ddlUnderlyingDocument_u, optionsText: 'CodeName',optionsValue: 'ID',optionsCaption: 'Please Select...'" data-rule-required="true" class="col-xs-7"></select>
                                                    <label class="control-label bolder text-danger" for="customer-name">*</label>
                                                </div>
                                            </div>
                                        </div>

                                        <!--    						<span aa-bind="text:UnderlyingDocument_u().Code()"></span>-->
                                        <div data-bind="visible:UnderlyingDocument_u().ID()=='50'" class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="otherUnderlying">
                                                Other Underlying
                                            </label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <input type="text" id="otherUnderlying" name="otherUnderlying" data-bind="value: $root.OtherUnderlying_u" class="col-xs-7" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="DocumentType">
                                                Type of Document
                                            </label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <select id="documentType" name="documentType" data-bind="value:$root.DocumentType_u().ID, options: ddlDocumentType_u, optionsText: 'Name',optionsValue: 'ID',optionsCaption: 'Please Select...'" data-rule-required="true" class="col-xs-7"></select>
                                                    <label class="control-label bolder text-danger" for="customer-name">*</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="Currency">
                                                Transaction Currency
                                            </label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <select id="currency_u" name="currency_u" data-bind="value:$root.Currency_u().ID, options:ddlCurrency_u, optionsText:'Code',optionsValue: 'ID',optionsCaption: 'Please Select...',event: {change:function(data){OnCurrencyChange(Currency_u().ID())}}" data-rule-required="true" class="col-xs-2"></select><%--$root.StatementLetter_u().ID()==1--%>
                                                    <label class="control-label bolder text-danger" for="customer-name">*</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="Amount">
                                                Invoice Amount
                                            </label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <input type="text" id="Amount_u" name="Amount_u" data-bind="value: $root.Amount_u(),disable:$root.StatementLetter_u().ID()==1" autocomplete="off" onkeyup="read_u();" data-rule-required="true" data-rule-number="true" class="col-xs-4 align-right" />
                                                    <label class="control-label bolder text-danger" for="customer-name">*</label>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-sm-3 control-label bolder no-padding-right" for="rate">
                                                Rate</label>

                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <input type="text" class="col-sm-4 align-right" name="rate_u" id="rate_u" disabled="disabled" data-rule-required="true" data-rule-number="true" data-bind="value: formatNumber($root.Rate_u())" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label bolder no-padding-right" for="eqv-usd">
                                                Eqv. USD</label>

                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <input type="text" class="col-sm-4 align-right" disabled="disabled" name="eqv-usd_u" id="eqv-usd_u" data-rule-required="true" data-rule-number="true" data-bind="value: formatNumber($root.AmountUSD_u())" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="DateOfUnderlying">
                                                Date of Underlying
                                            </label>
                                            <div class="col-sm-2">
                                                <div class="input-group">
                                                    <input type="text" id="DateOfUnderlying" name="DateOfUnderlying_u" data-date-format="dd-M-yyyy" autocomplete="off" data-bind="value:DateOfUnderlying_u, event: { change:$root.onChangeDateOfUnderlying}" class="form-control date-picker col-xs-6" data-rule-required="true" />
                                                    <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                                    <label class="control-label bolder text-danger starremove" for="DateOfUnderlying_u" style="position: absolute;">*</label>
                                                </div>
                                            </div>
                                            <label class="col-sm-2 control-label no-padding-right" for="ExpiredDate">
                                                Expiry Date
                                            </label>
                                            <div class="col-sm-2">
                                                <div class="input-group">
                                                    <input type="text" id="ExpiredDate" name="ExpiredDate" data-date-format="dd-M-yyyy" autocomplete="off" data-bind="value:ExpiredDate_u" class="form-control date-picker col-xs-6" data-rule-required="true" />
                                                    <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                                    <label class="control-label bolder text-danger starremove" for="ExpiredDate_u" style="position: absolute;">*</label>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group" data-bind="visible:false">
                                            <label class="col-sm-3 control-label no-padding-right" for="IsDeclarationOfException">
                                                Declaration of Exception
                                            </label>
                                            <div class="col-sm-9">
                                                <label>
                                                    <input name="switch-field-1" id="IsDeclarationOfException" data-bind="checked: $root.IsDeclarationOfException_u" class="ace ace-switch ace-switch-6" type="checkbox" />
                                                    <span class="lbl"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="space-2" data-bind="visible:false"></div>
                                        <div class="form-group" data-bind="visible:false">
                                            <label class="col-sm-3 control-label no-padding-right" for="StartDate">
                                                Start Date
                                            </label>
                                            <div class="col-sm-2">
                                                <div class="clearfix">
                                                    <div class="input-group">
                                                        <input id="StartDate" name="StartDate" data-bind="value:StartDate_u" data-date-format="dd-M-yyyy" class="form-control date-picker col-xs-6" type="text" />
                                                        <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <label class="col-sm-2 control-label no-padding-right" for="EndDate">
                                                End Date
                                            </label>
                                            <div class="col-sm-2">
                                                <div class="clearfix">
                                                    <div class="input-group">
                                                        <input type="text" id="EndDate" name="EndDate" data-bind="value:EndDate_u" data-date-format="dd-M-yyyy" class="form-control date-picker col-xs-6" />
                                                        <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="space-2" data-bind="visible:false"></div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="ReferenceNumber">
                                                Reference Number
                                            </label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <input type="text" id="ReferenceNumber" name="ReferenceNumber" data-bind="value: $root.ReferenceNumber_u" disabled="disabled" class="col-xs-9" />

                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="SupplierName">
                                                Supplier Name
                                            </label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <input type="text" id="SupplierName" name="SupplierName" data-bind="value: $root.SupplierName_u" autocomplete="off" class="col-xs-8" data-rule-required="true" />
                                                    <label class="control-label bolder text-danger" for="SupplierName_u">*</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="InvoiceNumber">
                                                Invoice Number
                                            </label>
                                            <div class="col-sm-9">
                                                <div class="clearfix">
                                                    <input type="text" id="invoiceNumber" name="invoiceNumber" data-bind="value: $root.InvoiceNumber_u" autocomplete="off" class="col-xs-8" data-rule-required="true" />
                                                    <label class="control-label bolder text-danger" for="InvoiceNumber_u">*</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="AttachmentNo">
                                                Is This doc Purposed to 
												replace the proforma doc
                                            </label>
                                            <div class="col-sm-9">
                                                <label>
                                                    <input name="switch-field-1" id="IsProforma" data-bind="checked:IsProforma_u,disable:DocumentType_u().ID()==2" class="ace ace-switch ace-switch-6" type="checkbox" />
                                                    <span class="lbl"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <!-- grid proforma begin -->

                                        <!-- widget box start -->
                                        <div id="widget-box" class="widget-box" data-bind="visible:IsProforma_u()">
                                            <!-- widget header start -->
                                            <div class="widget-header widget-hea1der-small header-color-dark">
                                                <h6>Customer Underlying 
													Proforma Table</h6>

                                                <div class="widget-toolbar">
                                                    <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                                        <i class="blue icon-filter"></i>
                                                        <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: UnderlyingProformaGridProperties().AllowFilter" />
                                                        <span class="lbl"></span>
                                                    </label>
                                                    <a href="#" data-bind="click: GetDataUnderlyingProforma" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                                        <i class="blue icon-refresh"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- widget header end -->
                                            <!-- widget body start -->
                                            <div class="widget-body">
                                                <!-- widget main start -->
                                                <div class="widget-main padding-0">
                                                    <!-- widget slim control start -->
                                                    <div class="slim-scroll" data-height="400">
                                                        <!-- widget content start -->
                                                        <div class="content">

                                                            <!-- table responsive start -->
                                                            <div class="table-responsive">
                                                                <div class="dataTables_wrapper" role="grid">
                                                                    <table id="Customer Underlying-table" class="table table-striped table-bordered table-hover dataTable">
                                                                        <thead>
                                                                            <tr data-bind="with: UnderlyingProformaGridProperties">
                                                                                <th style="width: 50px">No.</th>
                                                                                <th style="width: 50px">Select</th>
                                                                                <th data-bind="click: function () { Sorting('StatementLetter'); }, css: GetSortedColumn('StatementLetter')">Statement Letter</th>
                                                                                <th data-bind="click: function () { Sorting('UnderlyingDocument'); }, css: GetSortedColumn('UnderlyingDocument')">Underlying Document</th>
                                                                                <th data-bind="click: function () { Sorting('DocumentType'); }, css: GetSortedColumn('DocumentType')">Type of Doc</th>
                                                                                <th data-bind="click: function () { Sorting('Currency'); }, css: GetSortedColumn('Currency')">Currency</th>
                                                                                <th data-bind="click: function () { Sorting('Amount'); }, css: GetSortedColumn('Amount')">Amount</th>
                                                                                <th data-bind="click: function () { Sorting('DateOfUnderlying'); }, css: GetSortedColumn('DateOfUnderlying')">Date</th>
                                                                                <th data-bind="click: function () { Sorting('SupplierName'); }, css: GetSortedColumn('SupplierName')">Supplier Name</th>
                                                                                <th data-bind="click: function () { Sorting('ReferenceNumber'); }, css: GetSortedColumn('ReferenceNumber')">Reference Number</th>
                                                                                <th data-bind="    click: function () { Sorting('ExpiredDate'); }, css: GetSortedColumn('ExpiredDate')">Expiry Date</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <thead data-bind="visible: UnderlyingProformaGridProperties().AllowFilter" class="table-filter">
                                                                            <tr>
                                                                                <th class="clear-filter">
                                                                                    <a href="#" data-bind="click: ProformaClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                                        <i class="green icon-trash"></i>
                                                                                    </a>
                                                                                </th>
                                                                                <th class="clear-filter"></th>
                                                                                <th>
                                                                                    <input type="text" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterStatementLetter, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                                <th>
                                                                                    <input type="text" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterUnderlyingDocument, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                                <th>
                                                                                    <input type="text" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterDocumentType, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                                <th>
                                                                                    <input type="text" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterCurrency, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                                <th>
                                                                                    <input type="text" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterAmount, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                                <th>
                                                                                    <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.ProformaFilterDateOfUnderlying, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                                <th>
                                                                                    <input type="text" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterSupplierName, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                                <th>
                                                                                    <input type="text" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterReferenceNumber, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                                <th>
                                                                                    <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.ProformaFilterExpiredDate, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody data-bind="foreach: {data: CustomerUnderlyingProformas, as: 'ProformasData'}, visible: CustomerUnderlyingProformas().length > 0">
                                                                            <tr>
                                                                                <td><span data-bind="text: RowID"></span></td>
                                                                                <td align="center">
                                                                                    <input type="checkbox" id="isSelectedProforma" name="isSelect" data-bind="checked:ProformasData.IsSelectedProforma, event: {change:function(data){$root.onSelectionProforma($index(),$data)}}" /></td>
                                                                                <td><span data-bind="text: StatementLetter.Name"></span></td>
                                                                                <td><span data-bind="text: UnderlyingDocument.Name"></span></td>
                                                                                <td><span data-bind="text: DocumentType.Name"></span></td>
                                                                                <td><span data-bind="text: Currency.Code"></span></td>
                                                                                <td align="right"><span data-bind="text: formatNumber(Amount)"></span></td>
                                                                                <td><span data-bind="text: $root.LocalDate(DateOfUnderlying,true,false)"></span></td>
                                                                                <td><span data-bind="text: SupplierName"></span></td>
                                                                                <td><span data-bind="text: ReferenceNumber"></span></td>
                                                                                <td><span data-bind="text: $root.LocalDate(ExpiredDate,true,false)"></span></td>
                                                                            </tr>
                                                                        </tbody>
                                                                        <tbody data-bind="visible: CustomerUnderlyingProformas().length == 0">
                                                                            <tr>
                                                                                <td colspan="11" class="text-center">No entries available.
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <!-- table responsive end -->
                                                        </div>
                                                        <!-- widget content end -->
                                                    </div>
                                                    <!-- widget slim control end -->
                                                    <!-- widget footer start -->
                                                    <div class="widget-toolbox padding-8 clearfix">
                                                        <div class="row" data-bind="with: UnderlyingProformaGridProperties">
                                                            <!-- pagination size start -->
                                                            <div class="col-sm-6">
                                                                <div class="dataTables_paginate paging_bootstrap pull-left">
                                                                    Showing
                                                                    <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                                                    rows of <span data-bind="text: Total"></span>
                                                                    entries
                                                                </div>
                                                            </div>
                                                            <!-- pagination size end -->
                                                            <!-- pagination page jump start -->
                                                            <div class="col-sm-3">
                                                                <div class="dataTables_paginate paging_bootstrap">
                                                                    Page
                                                                    <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                                    of <span data-bind="text: TotalPages"></span>
                                                                </div>
                                                            </div>
                                                            <!-- pagination page jump end -->
                                                            <!-- pagination navigation start -->
                                                            <div class="col-sm-3">
                                                                <div class="dataTables_paginate paging_bootstrap">
                                                                    <ul class="pagination">
                                                                        <li data-bind="click: FirstPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                                            <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                                <i class="icon-double-angle-left"></i>
                                                                            </a>
                                                                        </li>
                                                                        <li data-bind="click: PreviousPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                                            <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                                <i class="icon-angle-left"></i>
                                                                            </a>
                                                                        </li>
                                                                        <li data-bind="click: NextPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                                            <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                                <i class="icon-angle-right"></i>
                                                                            </a>
                                                                        </li>
                                                                        <li data-bind="click: LastPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                                            <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                                <i class="icon-double-angle-right"></i>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <!-- pagination navigation end -->

                                                        </div>
                                                    </div>
                                                    <!-- widget footer end -->

                                                </div>
                                                <!-- widget main end -->
                                            </div>
                                            <!-- widget body end -->

                                        </div>
                                        <!-- widget box end -->
                                        <!-- grid proforma end -->



                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="IsBulkUnderlying">
                                                Is This Bulk Underlying</label>
                                            <div class="col-sm-9">
                                                <label>
                                                    <input name="switch-field-1" id="IsBulkUnderlying" data-bind="checked:IsBulkUnderlying_u" class="ace ace-switch ace-switch-6" type="checkbox" />
                                                    <span class="lbl"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <!-- grid Bulk begin -->
                                        <!-- widget box start -->
                                        <div class="widget-box" data-bind="visible:IsBulkUnderlying_u()">
                                            <!-- widget header start -->
                                            <div class="widget-header widget-hea1der-small header-color-dark">
                                                <h6>Customer Bulk Underlying 
													Table</h6>

                                                <div class="widget-toolbar">
                                                    <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                                        <i class="blue icon-filter"></i>
                                                        <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: UnderlyingBulkGridProperties().AllowFilter" />
                                                        <span class="lbl"></span>
                                                    </label>
                                                    <a href="#" data-bind="click: GetDataBulkUnderlying" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                                        <i class="blue icon-refresh"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- widget header end -->

                                            <!-- widget body start -->
                                            <div class="widget-body">
                                                <!-- widget main start -->
                                                <div class="widget-main padding-0">
                                                    <!-- widget slim control start -->
                                                    <div class="slim-scroll" data-height="400">
                                                        <!-- widget content start -->
                                                        <div class="content">

                                                            <!-- table responsive start -->
                                                            <div class="table-responsive">
                                                                <div class="dataTables_wrapper" role="grid">
                                                                    <table id="Customer Underlying-table" class="table table-striped table-bordered table-hover dataTable">
                                                                        <thead>
                                                                            <tr data-bind="with: UnderlyingBulkGridProperties">
                                                                                <th style="width: 50px">No.</th>
                                                                                <th style="width: 50px">Select</th>
                                                                                <th data-bind="click: function () { Sorting('StatementLetter'); }, css: GetSortedColumn('StatementLetter')">Statement Letter</th>
                                                                                <th data-bind="click: function () { Sorting('UnderlyingDocument'); }, css: GetSortedColumn('UnderlyingDocument')">Underlying Document</th>
                                                                                <th data-bind="click: function () { Sorting('DocumentType'); }, css: GetSortedColumn('DocumentType')">Type of Doc</th>
                                                                                <th data-bind="click: function () { Sorting('Currency'); }, css: GetSortedColumn('Currency')">Currency</th>
                                                                                <th data-bind="click: function () { Sorting('Amount'); }, css: GetSortedColumn('Amount')">Amount</th>
                                                                                <th data-bind="click: function () { Sorting('SupplierName'); }, css: GetSortedColumn('SupplierName')">Supplier Name</th>
                                                                                <th data-bind="click: function () { Sorting('InvoiceNumber'); }, css: GetSortedColumn('InvoiceNumber')">Invoice Number</th>
                                                                                <th data-bind="click: function () { Sorting('DateOfUnderlying'); }, css: GetSortedColumn('DateOfUnderlying')">Date</th>
                                                                                <th data-bind="click: function () { Sorting('ExpiredDate'); }, css: GetSortedColumn('ExpiredDate')">Expiry Date</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <thead data-bind="visible: UnderlyingBulkGridProperties().AllowFilter" class="table-filter">
                                                                            <tr>
                                                                                <th class="clear-filter">
                                                                                    <a href="#" data-bind="click: BulkClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                                        <i class="green icon-trash"></i>
                                                                                    </a>
                                                                                </th>
                                                                                <th class="clear-filter"></th>
                                                                                <th>
                                                                                    <input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterStatementLetter, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                                <th>
                                                                                    <input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterUnderlyingDocument, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                                <th>
                                                                                    <input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterDocumentType, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                                <th>
                                                                                    <input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterCurrency, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                                <th>
                                                                                    <input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterSupplierName, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                                <th>
                                                                                    <input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterAmount, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                                <th>
                                                                                    <input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterInvoiceNumber, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                                <th>
                                                                                    <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.BulkFilterDateOfUnderlying, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                                <th>
                                                                                    <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.BulkFilterExpiredDate, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody data-bind="foreach: {data: CustomerBulkUnderlyings, as: 'BulkDatas'}, visible: CustomerBulkUnderlyings().length > 0">
                                                                            <tr>
                                                                                <td><span data-bind="text: RowID"></span></td>
                                                                                <td align="center">
                                                                                    <input type="checkbox" id="isSelectedBulk" name="isSelect" data-bind="checked:BulkDatas.IsSelectedBulk, event: {change:function(data){$root.onSelectionBulk($index(),$data)}}" /></td>
                                                                                <td><span data-bind="text: StatementLetter.Name"></span></td>
                                                                                <td><span data-bind="text: UnderlyingDocument.Name"></span></td>
                                                                                <td><span data-bind="text: DocumentType.Name"></span></td>
                                                                                <td><span data-bind="text: Currency.Code"></span></td>
                                                                                <td align="right"><span data-bind="text: formatNumber(Amount)"></span></td>
                                                                                <td><span data-bind="text: SupplierName"></span></td>
                                                                                <td><span data-bind="text: InvoiceNumber"></span></td>
                                                                                <td><span data-bind="text: $root.LocalDate(DateOfUnderlying,true,false)"></span></td>
                                                                                <td><span data-bind="text: $root.LocalDate(ExpiredDate,true,false)"></span></td>
                                                                            </tr>
                                                                        </tbody>
                                                                        <tbody data-bind="visible: CustomerBulkUnderlyings().length == 0">
                                                                            <tr>
                                                                                <td colspan="11" class="text-center">No entries available.
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <!-- table responsive end -->
                                                        </div>
                                                        <!-- widget content end -->
                                                    </div>
                                                    <!-- widget slim control end -->

                                                    <!-- widget footer start -->
                                                    <div class="widget-toolbox padding-8 clearfix">
                                                        <div class="row" data-bind="with: UnderlyingBulkGridProperties">
                                                            <!-- pagination size start -->
                                                            <div class="col-sm-6">
                                                                <div class="dataTables_paginate paging_bootstrap pull-left">
                                                                    Showing
                                                                    <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                                                    rows of <span data-bind="text: Total"></span>
                                                                    entries
                                                                </div>
                                                            </div>
                                                            <!-- pagination size end -->

                                                            <!-- pagination page jump start -->
                                                            <div class="col-sm-3">
                                                                <div class="dataTables_paginate paging_bootstrap">
                                                                    Page
                                                                    <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                                    of <span data-bind="text: TotalPages"></span>
                                                                </div>
                                                            </div>
                                                            <!-- pagination page jump end -->

                                                            <!-- pagination navigation start -->
                                                            <div class="col-sm-3">
                                                                <div class="dataTables_paginate paging_bootstrap">
                                                                    <ul class="pagination">
                                                                        <li data-bind="click: FirstPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                                            <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                                <i class="icon-double-angle-left"></i>
                                                                            </a>
                                                                        </li>
                                                                        <li data-bind="click: PreviousPage, attr: { class : Page() > 1 ? '' : 'disabled' }">
                                                                            <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                                <i class="icon-angle-left"></i>
                                                                            </a>
                                                                        </li>
                                                                        <li data-bind="click: NextPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                                            <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                                <i class="icon-angle-right"></i>
                                                                            </a>
                                                                        </li>
                                                                        <li data-bind="click: LastPage, attr: { class : Page() < TotalPages() ? '' : 'disabled' }">
                                                                            <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                                <i class="icon-double-angle-right"></i>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <!-- pagination navigation end -->

                                                        </div>
                                                    </div>
                                                    <!-- widget footer end -->

                                                </div>
                                                <!-- widget main end -->
                                            </div>
                                            <!-- widget body end -->

                                        </div>
                                        <!-- widget box end -->
                                        <!-- grid Bulk end -->

                                    </div>

                                    <!-- modal body form end -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- modal body end -->
                    <!-- modal footer start -->
                    <div class="modal-footer">
                        <button class="btn btn-sm btn-primary" data-bind="click: $root.save_u, visible: $root.IsNewDataUnderlying(), disable: !$root.IsEditTableUnderlying()">
                            <i class="icon-save"></i>
                            Save
                        </button>
                        <button class="btn btn-sm btn-primary" data-bind="click: $root.update_u, visible: (!$root.IsNewDataUnderlying() && $root.IsUtilize_u()), disable: !$root.IsEditable()">
                            <i class="icon-edit"></i>
                            Update
                        </button>
                        <button class="btn btn-sm btn-warning" data-bind="click: $root.delete_u, visible: (!$root.IsNewDataUnderlying() && $root.IsUtilize_u())">
                            <i class="icon-trash"></i>
                            Delete
                        </button>
                        <button class="btn btn-sm" data-bind="click: $root.cancel_u" data-dismiss="modal">
                            <i class="icon-remove"></i>
                            Cancel
                        </button>
                    </div>
                    <!-- modal footer end -->
                </div>
            </div>
        </div>
        </div>
        <!-- modal form end Underlying Form -->
        <!-- modal form Tz Information -->
        <div id="modal-form-Tz" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
            <div class="modal-dialog" style="width:90%">
                <div class="modal-content">
                    <!-- modal header start Underlying Form -->
                    <div class="modal-header">
                            <button type="button" class="bootbox-close-button close" data-dismiss="modal" style="margin-top: -10px;font-size:30px;margin-right: -8px;" data-bind="click:$root.btnCloseTZ">×</button>
                        <h5 class="blue bigger">
                            <span>Please Choose TZ Number</span>
                        </h5>
                    </div>
                    <!-- modal header end -->
                    <!-- modal body start -->
                    <div class="modal-body overflow-visible">
                        <div id="modalformTz">
                            <div class="row">
                                <div class="col-xs-12">
                                
                                    <!-- modal body form start -->
                                    <div class="dataTables_wrapper" role="grid">
                                            <table class="table table-striped table-bordered table-hover dataTable">
                                                <thead>
                                                    <tr>
                                                        <!--<th>Choose</th>-->
                                                        <th>No</th>
                                                        <th>Booking Reference</th>
                                                        <th>Customer Name</th>
                                                        <th>Product Type</th>
                                                        <th>Trade Date</th>
                                                        <th>Currency</th>
                                                        <th>Trxn Amount</th>
                                                        <th>Account Number</th>
                                                        <th>Exchange Rate</th>
                                                        <th>Fx Transaction</th>
                                                        <th>Statement Letter</th>
                                                    </tr>
                                                </thead>
                                                <tbody data-bind="foreach: $root.TZInformations, visible: $root.TZInformations().length > 0">
                                                    <tr data-bind="click: $root.SelectedTZ,event: { dblclick: $root.SelecteddbTZ }">
                                                        <!--<td align="center"><input type="checkbox" id="tzSelection" name="tzSelection" data-bind="checked: IsTZNumber, event: { change: function (data) { $root.onSelectionTZ($index(), $data) } }" /></td> -->
                                                        <!--<td class="center"><label><input type="checkbox" data-bind="event: { change: $root.SelectedTZ }" /></label></td> -->
                                                        <td><span data-bind="text: RowID"></span></td>
                                                        <td><span data-bind="text: TZRef"></span></td>
                                                        <td><span data-bind="text: CustomerName"></span></td>
                                                        <td><span data-bind="text: ProductType.Code"></span></td>
                                                        <td><span data-bind="text: $root.LocalDate(TradeDate,true,false)"></span></td>
                                                        <td><span data-bind="text: Currency.Code"></span></td>
                                                        <td><span data-bind="text: $root.FormatNumber(Amount)"></span></td>
                                                        <td><span data-bind="text: AccountNumber"></span></td>
                                                        <td><span data-bind="text: Rate"></span></td>
                                                        <td><span data-bind="text: IsFxTransaction"></span></td>
                                                        <td><span data-bind="text: StatementLetter.Name"></span></td>
                                                    </tr>
                                                </tbody>
                                                <tbody data-bind="visible: $root.TZInformations().length == 0">
                                                    <tr>
                                                        <td colspan="13" class="text-center">
                                                            No entries available.
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                    </div> 
                                    <!-- modal body form end -->
                                
                                    <!-- widget footer start -->
                                    <div class="widget-toolbox padding-8 clearfix">
                                        <div class="row" data-bind="with: TZGridProperties">
                                            <!-- pagination size start -->
                                            <div class="col-sm-6">
                                                <div class="dataTables_paginate paging_bootstrap pull-left">
                                                    Showing <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                                    rows of <span data-bind="text: Total"></span>
                                                    entries
                                                </div>
                                            </div>
                                            <!-- pagination size end -->
                                            <!-- pagination page jump start -->
                                            <div class="col-sm-3">
                                                <div class="dataTables_paginate paging_bootstrap">
                                                    Page <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width:50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                    of <span data-bind="text: TotalPages"></span>
                                                </div>
                                            </div>
                                            <!-- pagination page jump end -->
                                            <!-- pagination navigation start -->
                                            <div class="col-sm-3">
                                                <div class="dataTables_paginate paging_bootstrap">
                                                    <ul class="pagination">
                                                        <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                            <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                <i class="icon-double-angle-left"></i>
                                                            </a>
                                                        </li>
                                                        <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                            <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                <i class="icon-angle-left"></i>
                                                            </a>
                                                        </li>
                                                        <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                            <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                <i class="icon-angle-right"></i>
                                                            </a>
                                                        </li>
                                                        <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                            <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                <i class="icon-double-angle-right"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <!-- pagination navigation end -->

                                        </div>
                                    </div>
                                    <!-- widget footer end -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- modal body end -->
                    <!-- modal footer start -->
                    <div class="modal-footer">
                        <button class="btn btn-sm" data-bind="click: $root.btnCloseTZ" data-dismiss="modal">
                            <i class="icon-remove"></i>
                            Close
                        </button>
                    </div>
                    <!-- modal footer end -->
                </div>
            </div>
        </div>
        <!-- modal form enderlying end -->
        <!-- Modal application ID List start-->
        <div id="modal-form-applicationID" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="blue bigger">Application ID List
                        </h4>
                    </div>
                    <div class="modal-body overflow-visible">
                        <div class="row">
                            <div class="col-xs-12">
                                <div id="customer-form" class="form-horizontal" role="form">
                                    <div class="dataTables_wrapper" role="grid">
                                        <table class="table table-striped table-bordered table-hover dataTable">
                                            <thead>
                                                <tr>
                                                    <th style="width: 50px">No.</th>
                                                    <th>Application ID</th>
                                                    <th>Customer</th>
                                                </tr>
                                            </thead>
                                            <tbody data-bind="foreach: $root.ApplicationIDColl, visible: $root.ApplicationIDColl().length > 0">
                                                <tr data-toggle="modal">
                                                    <td><span data-bind="text: $index() +1"></span></td>
                                                    <td><span data-bind="text: ApplicationID"></span></td>
                                                    <td><span data-bind="text: Customer.Name"></span></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <br />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-sm btn-primary" data-bind="click: $root.FinishBulk">
                            <i class="icon-save"></i>
                            OK
                        </button>
                    </div>
                </div>
            </div>
        </div>
    <!-- Modal application ID List end-->
    <!-- modal netting deal number start-->
    <div id="modal-form-netting-deal-number" class="modal fade" tabindex="-1" data-backdrop="static" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="blue bigger"><span>Netting Deal Number</span></h4>
                </div>
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="form-netting-deal-number" class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right">DF Number</label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="df-number" data-rule-required="true" data-bind="" />
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="space-4"></div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right">SWAP DF</label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="swap-df" data-rule-required="true" data-bind="" />
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: ''">
                        <i class="icon-save"></i>
                        Save
                    </button>
                    <button class="btn btn-sm" data-dismiss="modal">
                        <i class="icon-remove"></i>
                        Cancel
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- modal netting deal number end-->
    <!-- modal after netting deal number start-->
    <div id="modal-form-after-netting-deal-number" class="modal fade" tabindex="-1" data-backdrop="static" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="blue bigger"><span>After Netting Deal Number</span></h4>
                </div>
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="form-after-netting-deal-number" class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right">DF Number</label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="after-df-number" data-rule-required="true" data-bind="" />
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="space-4"></div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right">SWAP DF</label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="after-swap-df" data-rule-required="true" data-bind="" />
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: ''">
                        <i class="icon-save"></i>
                        Save
                    </button>
                    <button class="btn btn-sm" data-dismiss="modal">
                        <i class="icon-remove"></i>
                        Cancel
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- modal after netting deal number end -->
  </div>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/Knockout/knockout.mapping-latest.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/Helper.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/CalculatedFX.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/NewNettingTransaction.js"></script>
 