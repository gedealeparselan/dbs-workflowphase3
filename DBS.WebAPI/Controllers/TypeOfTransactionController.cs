﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class TypeOfTransactionController : ApiController
    {
         private string message = string.Empty;

        private readonly ITypeOfTransactionRepository repository = null;

        public TypeOfTransactionController()
        {
            this.repository = new TypeOfTransactionRepository();
        }

        public TypeOfTransactionController(ITypeOfTransactionRepository repository)
        {
            this.repository = repository;
        }

        [ResponseType(typeof(IList<TypeOfTransactionModel>))]
        public HttpResponseMessage Get()
        {
            IList<TypeOfTransactionModel> output = new List<TypeOfTransactionModel>();

            if (!repository.GetTypeOfTransaction(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<TypeOfTransactionModel>>(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(IList<TypeOfTransactionModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<TypeOfTransactionModel> output = new List<TypeOfTransactionModel>();

            if (!repository.GetTypeOfTransaction(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<TypeOfTransactionModel>>(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(TypeOfTransactionGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<TypeOfTransactionFilter> filters)
        {
            TypeOfTransactionGrid output = new TypeOfTransactionGrid();

            if (!repository.GetTypeOfTransaction(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Code" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<TypeOfTransactionGrid>(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(IList<TypeOfTransactionModel>))]
        [HttpGet]
        [Route("api/TypeOfTransaction/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<TypeOfTransactionModel> output = new List<TypeOfTransactionModel>();

            if (!repository.GetTypeOfTransaction(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<TypeOfTransactionModel>>(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(TypeOfTransactionModel))]
        [Route("api/TypeOfTransaction/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            TypeOfTransactionModel output = new TypeOfTransactionModel();

            if (!repository.GetTypeOfTransactionByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<TypeOfTransactionModel>(HttpStatusCode.OK, output);
            }
        }

        [HttpPost]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(TypeOfTransactionModel typeoftransaction)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddTypeOfTransaction(typeoftransaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New TypeOfTransaction data has been created." });
                }
            }
        }

        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/TypeOfTransaction/{id}")]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(int id, TypeOfTransactionModel typeoftransaction)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateTypeOfTransaction(id, typeoftransaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "TypeOfTransaction data has been updated." });
                }
            }
        }

        [HttpDelete]
        [Route("api/TypeOfTransaction/{id}")]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteTypeOfTransaction(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "TypeOfTransaction data has been deleted." });
                }
            }
        }
        
        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}