﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class FDTransferController : ApiController
    {
         private string message = string.Empty;

        private readonly IFDTransferRepository repository = null;

        public FDTransferController()
        {
            this.repository = new FDTransferRepository();
        }

        public FDTransferController(IFDTransferRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all currencies.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<FDTransferModel>))]
        public HttpResponseMessage Get()
        {
            IList<FDTransferModel> output = new List<FDTransferModel>();

            if (!repository.GetFDTransfer(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<FDTransferModel>>(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(IList<FDTransferModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<FDTransferModel> output = new List<FDTransferModel>();

            if (!repository.GetFDTransfer(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<FDTransferModel>>(HttpStatusCode.OK, output);
            }
        }
        
        [ResponseType(typeof(FDTransferGrid))]
        [Route("api/FDTransfer")]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<FDTransferFilter> filters)
        {
            FDTransferGrid output = new FDTransferGrid();

            if (!repository.GetFDTransfer(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Code" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<FDTransferGrid>(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(IList<FDTransferModel>))]
        [HttpGet]
        [Route("api/FDTransfer/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<FDTransferModel> output = new List<FDTransferModel>();

            if (!repository.GetFDTransfer(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<FDTransferModel>>(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(FDTransferModel))]
        [Route("api/FDTransfer/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            FDTransferModel output = new FDTransferModel();

            if (!repository.GetFDTransferByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<FDTransferModel>(HttpStatusCode.OK, output);
            }
        }

        public HttpResponseMessage Post(FDTransferModel fdtransfer)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddFDTransfer(fdtransfer, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New fdtransfer data has been created." });
                }
            }
        }

        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/FDTransfer/{id}")]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(int id, FDTransferModel fdtransfer)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateFDTransfer(id, fdtransfer, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "FDTransfer data has been updated." });
                }
            }
        }

        [HttpDelete]
        [Route("api/FDTransfer/{id}")]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteFDTransfer(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "FDTransfer data has been deleted." });
                }
            }
        }
        
        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}