﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class BizSegmentController : ApiController
    {
        private string message = string.Empty;

        private readonly IBizSegmentRepository repository = null;

        public BizSegmentController()
        {
            this.repository = new BizSegmentRepository();
        }

        public BizSegmentController(IBizSegmentRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all biz segments.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<BizSegmentModel>))]
        public HttpResponseMessage Get()
        {
            IList<BizSegmentModel> output = new List<BizSegmentModel>();

            if (!repository.GetBizSegment(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<BizSegmentModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get biz segments with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<BizSegmentModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<BizSegmentModel> output = new List<BizSegmentModel>();

            if (!repository.GetBizSegment(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<BizSegmentModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get biz segments for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(BankGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<BizSegmentFilter> filters)
        {
            BizSegmentGrid output = new BizSegmentGrid();

            if (!repository.GetBizSegment(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<BizSegmentGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find biz segments using criterias Code or Description.
        /// </summary>
        /// <param name="query">Code or Description.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<BizSegmentModel>))]
        [HttpGet]
        [Route("api/BizSegment/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<BizSegmentModel> output = new List<BizSegmentModel>();

            if (!repository.GetBizSegment(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<BizSegmentModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Biz Segment by ID.
        /// </summary>
        /// <param name="id">Biz Segment ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(BizSegmentModel))]
        [Route("api/BizSegment/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            BizSegmentModel output = new BizSegmentModel();

            if (!repository.GetBizSegmentByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<BizSegmentModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new Biz Segment.
        /// </summary>
        /// <param name="bizSegment">Biz segment data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(BizSegmentModel bizSegment)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddBizSegment(bizSegment, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New Biz Segment data has been created." });
                }
            }
        }

        /// <summary>
        /// Modify existing BizSegment.
        /// </summary>
        /// <param name="id">ID of Biz Segment to be modify.</param>
        /// <param name="bizSegment">Biz Segment data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
       // [Authorize(Roles = "DBS Admin")]
        [ResponseType(typeof(string))]
        [Route("api/BizSegment/{id}")]
        public HttpResponseMessage Put(int id, BizSegmentModel bizSegment)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateBizSegment(id, bizSegment, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Biz Segment data has been updated." });
                }
            }
        }

        /// <summary>
        /// Remove exisiting Biz Segment.
        /// </summary>
        /// <param name="id">ID of Biz Segment to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/BizSegment/{id}")]
        [ResponseType(typeof(string))]
       // [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteBizSegment(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "BizSegment data has been deleted." });
                }
            }
        }
        
        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}