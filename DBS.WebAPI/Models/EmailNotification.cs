﻿using DBS.Entity;
using DBS.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;

namespace DBS.WebAPI.Models
{
    #region property
    public class EmailNotificationModel
    {
        public long TransactionID { get; set; }
        public string ApplicationID { get; set; }
        public string CustomerName { get; set; }
        public string Product { get; set; }
        public string Currency { get; set; }
        public decimal TransactionAmount { get; set; }
        public decimal EqvUsd { get; set; }
        public string FXTransaction { get; set; }
        public string IsTopUrgent { get; set; }
        public string TranstactionStatus { get; set; }
        public string LastSentBy { get; set; }
        public DateTime? LastSentDate { get; set; }
        public string CurrentUser { get; set; }

    }

    public class EmailProperty
    {
        public int MatrixID { get; set; }
        public string EmailBody { get; set; }
        public string Subject { get; set; }
        public string UserTo { get; set; }
    }
    #endregion

    #region interface
    public interface IEmailAlertRepository : IDisposable
    {
        bool GetEmailNotificationByID(string key, int limit, ref IList<EmailNotificationModel> EmailNotification, ref string message);
        bool SaveEmailNotification(EmailNotificationModel EmailNotification, int MatrixID, ref string message);
        bool GetEmailNotificationProperties(int MatrixID, int TransactionID, ref EmailProperty emailProperty, ref string message);
    }
    #endregion

    #region repository
    public class EmailNotificationRepository : IEmailAlertRepository
    {        
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        // This Connection string is used for get dynamic StoredProcedure Field as Email variable
        private string ConString = "DBSEntitiesSP"; 
        public bool GetEmailNotificationByID(string key, int limit, ref IList<EmailNotificationModel> EmailNotification, ref string message)
        {
            bool isSuccess = false;

            try
            {
                EmailNotification = (from a in context.SP_GetEmailNotifications(key, limit)
                                     select new EmailNotificationModel
                                     {
                                         TransactionID = a.TransactionID,
                                         ApplicationID = a.ApplicationID,
                                         CustomerName = a.CustomerName,
                                         Product = a.ProductName,
                                         Currency = a.CurrencyDescription,
                                         TransactionAmount = a.Amount,
                                         EqvUsd = a.AmountUSD,
                                         FXTransaction = a.FXTransaction,
                                         IsTopUrgent = a.IsTopUrgent,
                                         TranstactionStatus = a.StateName,
                                         LastSentBy = a.LastSentBy,
                                         LastSentDate = a.LastSentDate
                                     }).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool SaveEmailNotification(EmailNotificationModel EmailNotification, int MatrixID, ref string message)
        {
            bool isSuccess = false;

            try
            {
                context.TransactionEmailNotifications.Add(new Entity.TransactionEmailNotification()
                {
                    TransactionID = EmailNotification.TransactionID,
                    MatrixID = MatrixID,
                    CreateBy = EmailNotification.CurrentUser,
                    CreateDate = DateTime.Now
                });

                context.SaveChanges();

                isSuccess = true;

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool GetEmailNotificationProperties(int MatrixID, int TransactionID, ref EmailProperty emailProperty, ref string message)
        {
            bool isSuccesed = false;            

            try
            {
                //Get email body & subject value from Matrix table
                var dataMatrix = (from a in context.Matrices select a).FirstOrDefault();
                string emailBody = dataMatrix.MatrixMailBody;
                string emailSubject = dataMatrix.MatrixMailSubject;
                string emailUserTo = string.Empty;

                DataTable dt = new DataTable();

                if (OpenConnectionSP(TransactionID, MatrixID, ref dt, ref message))
                {
                    foreach (DataColumn col in dt.Columns)
                    {
                        //Subject email - replace variable string email with value from SP
                        emailSubject = GetEmailPropertiesReplace(emailSubject, col.ColumnName, dt.Rows[0][col].ToString());

                        //Body email - replace variable string email with value from SP
                        emailBody = GetEmailPropertiesReplace(emailBody, col.ColumnName, dt.Rows[0][col].ToString());

                        //Get customer group based on CIF location
                        if (col.ColumnName.Equals("CIF"))
                        {
                            emailUserTo = GetUserEmailGroup(dt.Rows[0][col].ToString());
                        }
                    }

                    emailProperty.MatrixID = MatrixID;
                    emailProperty.EmailBody = emailBody;
                    emailProperty.Subject = emailSubject;
                    emailProperty.UserTo = emailUserTo;

                    isSuccesed = true;
                }
                else
                {
                    message = string.Format("Could not open ConnectionString : {0} ", ConString);
                }
            }
            catch (Exception ex)
            {
                message = ex.ToString();
            }

            return isSuccesed;
        }
        public bool OpenConnectionSP(int TransactionID, int MatrixID, ref DataTable dt, ref string message)
        {
            bool isOpen = false;

            try
            {
                using (SqlConnection con = 
                    new SqlConnection(ConfigurationManager.ConnectionStrings[ConString].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        //Execute Stored Procedure
                        cmd.CommandText = string.Format("exec [dbo].[SP_WF_GetLastTransaction] {0},{1}", 
                            TransactionID, MatrixID);

                        using (SqlDataAdapter sda = new SqlDataAdapter())
                        {
                            cmd.Connection = con;
                            sda.SelectCommand = cmd;
                            using (DataSet ds = new DataSet())
                            {
                                dt = new DataTable();
                                sda.Fill(dt);
                            }
                        }
                    }
                }

                isOpen = true;
            }
            catch (Exception ex)
            {
                message = ex.ToString();
                isOpen = false;
            }

            return isOpen;
        }
        public string GetEmailPropertiesReplace(string source, string columnName, string columnValue)
        {
            foreach (Match match in Regex.Matches(source, "(\\@\\w+) "))
            {
                string variable = match.Groups[1].Value;
                string variableTrim = variable.Replace("@", String.Empty);

                if (columnName.Equals(variableTrim))
                {
                    source = source.Replace(
                        string.Format(" {0} ", variable),
                        string.Format(" {0} ", columnValue)
                    );
                }
            }
            return source;
        }
        public string GetUserEmailGroup(String CIF)
        {
            string LocationName = (from a in context.Customers
                                   join b in context.Locations on a.LocationID equals b.LocationID
                                   where a.CIF == CIF
                                   select b.LocationName).FirstOrDefault();

            return "DBS Biz Unit " + LocationName;
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }

    #endregion
}
