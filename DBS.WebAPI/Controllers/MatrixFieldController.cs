﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;
using DBS.Entity;


namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class MatrixFieldController : ApiController
    {
        private string message = string.Empty;

        private readonly IMatrixFieldRepository repository = null;

        public MatrixFieldController()
        {
            this.repository = new MatrixFieldRepository();
        }

        public MatrixFieldController(IMatrixFieldRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get Matrix Field.
        /// </summary>
        /// <param name="id">Matrix  ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<MatrixFieldModel>))]
        public HttpResponseMessage Get()
        {
            IList<MatrixFieldModel> output = new List<MatrixFieldModel>();

            if (!repository.GetMatrixField(ref output, ref message))                
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<MatrixFieldModel>>(HttpStatusCode.OK, output);
            }
        }
        
        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}