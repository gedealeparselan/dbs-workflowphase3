﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Models
{
    #region Property
    [ModelName("TBODocsSLA")]
    public class TBODocsSLAModel
    {
        public long ID { get; set; }

        public int? Days { get; set; }

        public string Description { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public string LastModifiedBy { get; set; }
    }

    public class TBODocsSLARow : TBODocsSLAModel
    {
        public int RowID { get; set; }

    }

    public class TBODocsSLAGrid : Grid
    {
        public IList<TBODocsSLARow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class TBODocsSLAFilter : Filter { }
    #endregion

    #region Interface
    public interface ITBODocsSLARepository : IDisposable
    {
        bool GetTBODocsSLAByID(int id, ref TBODocsSLAModel tbodocssla, ref string message);
        bool GetTBODocsSLA(ref IList<TBODocsSLAModel> tbodocsslas, int limit, int index, ref string message);
        bool GetTBODocsSLA(ref IList<TBODocsSLAModel> tbodocsslas, ref string message);
        bool GetTBODocsSLA(int page, int size, IList<TBODocsSLAFilter> filters, string sortColumn, string sortOrder, ref TBODocsSLAGrid tbodocssla, ref string message);
        bool GetTBODocsSLA(TBODocsSLAFilter filter, ref IList<TBODocsSLAModel> tbodocsslas, ref string message);
        bool GetTBODocsSLA(string key, int limit, ref IList<TBODocsSLAModel> tbodocsslas, ref string message);
        bool AddTBODocsSLA(TBODocsSLAModel tbodocssla, ref string message);
        bool UpdateTBODocsSLA(int id, TBODocsSLAModel tbodocssla, ref string message);
        bool DeleteTBODocsSLA(int id, ref string message);
    }
    #endregion

    #region Repository
    public class TBODocsSLARepository : ITBODocsSLARepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetTBODocsSLAByID(int id, ref TBODocsSLAModel tbodocssla, ref string message)
        {
            bool isSuccess = false;
            try {
                tbodocssla = (from a in context.TBOSLAs
                              where a.IsDeleted.Equals(false) && a.TBOSLAID.Equals(id)
                              select new TBODocsSLAModel
                              {
                                  ID = a.TBOSLAID,
                                  Days = a.Days,
                                  Description = a.Description,
                                  LastModifiedBy = a.CreateBy,
                                  LastModifiedDate = a.CreateDate
                              }).SingleOrDefault();
                isSuccess = true;
            
            }
            catch(Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool GetTBODocsSLA(ref IList<TBODocsSLAModel> tbodocsslas, int limit, int index, ref string message)
        {
            bool isSuccess = false;
            
            try {
                int skip = limit * index;
                using(DBSEntities context = new DBSEntities()){
                    tbodocsslas = (from a in context.TBOSLAs
                                   where a.IsDeleted.Equals(false)
                                   orderby new { a.Days }
                                   select new TBODocsSLAModel
                                   {
                                       ID = a.TBOSLAID,
                                       Days = a.Days,
                                       Description = a.Description,
                                       LastModifiedBy = a.CreateBy,
                                       LastModifiedDate = a.CreateDate
                                   }).Skip(skip).Take(limit).ToList();
                }
                isSuccess = true;
            }
            catch(Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool GetTBODocsSLA(ref IList<TBODocsSLAModel> tbodocsslas, ref string message)
        {
            bool isSuccess = false;
            try {
                using (DBSEntities context = new DBSEntities()) {
                    tbodocsslas = (from a in context.TBOSLAs
                                   where a.IsDeleted.Equals(false)
                                   orderby new { a.Days }
                                   select new TBODocsSLAModel { 
                                   ID = a.TBOSLAID,
                                   Days = a.Days,
                                   Description = a.Description,
                                   LastModifiedBy = a.CreateBy,
                                   LastModifiedDate =a.CreateDate 
                                   }).ToList();
                }
            isSuccess = true;
            }
            catch(Exception ex){
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool GetTBODocsSLA(int page, int size, IList<TBODocsSLAFilter> filters, string sortColumn, string sortOrder, ref TBODocsSLAGrid tbodocsslagrid, ref string message)
        {
            bool isSuccess = false;
            try {
                int skip = (page - 1) * size;
                string orderBy = sortColumn +" "+ sortOrder;
                DateTime maxDate = new DateTime();

                TBODocsSLAModel filter = new TBODocsSLAModel();
                if (filters != null)
                {
                    filter.Days = Convert.ToInt32((string)filters.Where(a => a.Field.Equals("Days")).Select(a => a.Value).SingleOrDefault()??"0");
                    filter.Description = filters.Where(a => a.Field.Equals("Description")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any()) {
                        filter.LastModifiedDate = DateTime.Parse(filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities()) {

                    var data = (from a in context.TBOSLAs
                                let isDays = filter.Days>0
                                let isDescription = !string.IsNullOrEmpty(filter.Description)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) &&
                               (isDays ? a.Days==filter.Days : true) &&
                               (isDescription ? a.Description.Contains(filter.Description) : true) &&
                               (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                               (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new TBODocsSLAModel
                                {
                                    ID = a.TBOSLAID,
                                    Days = a.Days,
                                    Description = a.Description,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });
                    tbodocsslagrid.Page = page;
                    tbodocsslagrid.Size = size;
                    tbodocsslagrid.Total = data.Count();
                    tbodocsslagrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    tbodocsslagrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                     .Select((a, i) => new TBODocsSLARow
                     {
                         RowID = i + 1,
                         ID = a.ID,
                         Days = a.Days,
                         Description = a.Description,
                         LastModifiedBy = a.LastModifiedBy,
                         LastModifiedDate = a.LastModifiedDate
                     })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }
                isSuccess = true;
            }
            catch(Exception ex){
                message = ex.Message+" TRACE: "+ex.StackTrace.ToString();
            }
            return isSuccess;
        }
        public bool GetTBODocsSLA(TBODocsSLAFilter filter, ref IList<TBODocsSLAModel> tbodocsslas, ref string message)
        {
            throw new NotImplementedException();
        }
        public bool GetTBODocsSLA(string key, int limit, ref IList<TBODocsSLAModel> tbodocsslas, ref string message)
        {
            bool isSuccess = false;
            int day = 0;
            int.TryParse(key, out day);
            try {
                tbodocsslas = (from a in context.TBOSLAs
                               let isDay = day>0
                               where a.IsDeleted.Equals(false) && 
                               (isDay? a.Days == day: true) 
                               orderby new { a.Days }
                               select new TBODocsSLAModel { 
                               Days = a.Days,
                               Description = a.Description
                               }).Take(limit).ToList();
                isSuccess = true; 
            }
            catch(Exception ex)
            { message = ex.Message; }
            return isSuccess;
        }
        public bool AddTBODocsSLA(TBODocsSLAModel tbodocssla,ref string message) {
            bool isSuccess = false;
            try { 
                if(!context.TBOSLAs.Where(a => a.Days.Equals(tbodocssla.Days) && a.IsDeleted.Equals(false)).Any()){
                    context.TBOSLAs.Add(new Entity.TBOSLA()
                    {
                        TBOCode ="",
                        Days = tbodocssla.Days,
                        Description = tbodocssla.Description,
                        IsDeleted = false,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    });
                    context.SaveChanges();

                    isSuccess = true;
                 }
                else
                {
                    message = string.Format("TBODocsSLA data for tbodocssla name {0} is already exist.", tbodocssla.Days);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool UpdateTBODocsSLA(int id, TBODocsSLAModel tbodocssla, ref string message) {
            bool isSuccess = false;
            try
            {
                Entity.TBOSLA data = context.TBOSLAs.Where(a => a.TBOSLAID.Equals(id)).SingleOrDefault();
                if (data != null)
                {
                    if (context.TBOSLAs.Where(a => a.TBOSLAID != tbodocssla.ID && a.Days.Equals(tbodocssla.Days) && a.IsDeleted.Equals(false)).Count() >= 1)
                    {
                        message = string.Format("Days {0} is exist.", tbodocssla.Days);
                    }
                    else
                    {
                        data.Days = tbodocssla.Days;
                        data.Description = tbodocssla.Description;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                        context.SaveChanges();
                        isSuccess = true;
                    }
                }
                else 
                {
                    message = string.Format("TBODocsSLA data for TBODocsSLA ID {0} is does not exist.", id);
                }

            }
            catch(Exception ex){
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool DeleteTBODocsSLA(int id, ref string message) {
            bool isSuccess = false;
            try {
                Entity.TBOSLA data = context.TBOSLAs.Where(a => a.TBOSLAID.Equals(id)).SingleOrDefault();
                if (data != null) {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("TBODocsSLA data for TBODocsSLA ID {0} is does not exist.", id);
                }
            }
            catch(Exception ex){
                message = ex.Message;            
            }
            return isSuccess;
        }
       
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}