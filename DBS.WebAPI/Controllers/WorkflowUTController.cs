﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

//=======================================================================================//
///FOR ALL DEVELOPER...
///PLEASE ADD NEW REGION BY YOUR NAME TO KEEP CLEAN !!! DONT BE A DIRTY PROGRAMMER!!!
///ANDIWIJAYA
//=======================================================================================//

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class WorkflowUTController : ApiController
    {
        private string message = string.Empty;
        /// <summary>
        /// Define repo here
        /// </summary>
        private readonly IWorkflowUTRepository repoUT = null;
        public WorkflowUTController()
        {
            this.repoUT = new WorkflowUTRepository();
        }

        #region Andri
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowUT/{workflowInstanceID}/TransactionUT/BranchChecker/Update/{approverID}")]
        public HttpResponseMessage UpdateTransactionCheckerDetails(Guid workflowInstanceID, long approverID, UTBranchCheckerDetailModel transactionChecker)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoUT.UpdateTransactionCheckerDetails(workflowInstanceID, approverID, transactionChecker, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been updated." });
                }
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowUT/{workflowInstanceID}/TransactionUT/UTChecker/Update/{approverID}")]
        public HttpResponseMessage UpdateUTChecker(Guid workflowInstanceID, long approverID, UTBranchCheckerDetailModel Transaction)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoUT.UpdateUTCheckerData(workflowInstanceID, approverID, ref Transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been saved." });
                }
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowUT/{workflowInstanceID}/TransactionUT/BranchChecker/Add/{approverID}")]
        public HttpResponseMessage AddBranchCheckerDetails(Guid workflowInstanceID, long approverID, UTBranchCheckerDetailModel Transaction)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoUT.AddUTBranchChecker(workflowInstanceID, approverID, ref Transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been saved." });
                }
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowUT/{workflowInstanceID}/TransactionUT/UTChecker/Add/{approverID}")]
        public HttpResponseMessage AddUTCheckerDetails(Guid workflowInstanceID, long approverID, UTBranchCheckerDetailModel Transaction)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoUT.AddUTChecker(workflowInstanceID, approverID, ref Transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been saved." });
                }
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowUT/{workflowInstanceID}/TransactionUT/UTAccountChecker/Add/{approverID}")]
        public HttpResponseMessage AddUTAccountCheckerDetails(Guid workflowInstanceID, long approverID, UTBranchCheckerDetailModel Transaction)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoUT.AddUTCBOAccountChecker(workflowInstanceID, approverID, ref Transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been saved." });
                }
            }
        }



        [ResponseType(typeof(UTBranchCheckerDetailModel))]
        [HttpGet]
        [Route("api/WorkflowUT/{workflowInstanceID}/TransactionUT/CBOAccountChecker/{approverID}")]
        public HttpResponseMessage GetCBOAccountCheckerDetails(Guid workflowInstanceID, long approverID)
        {
            UTDetailModel Transaction = new UTDetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            UTBranchCheckerDetailModel output = new UTBranchCheckerDetailModel();
            //IList<MutualFundCBOCheckerModel> MutualFund = new List<MutualFundCBOCheckerModel>();
            IList<JoinAccountBranchCheckerModel> JoinAccount = new List<JoinAccountBranchCheckerModel>();
            try
            {
                // #1 Get UT Details
                if (!repoUT.GetTransactionDetails(workflowInstanceID, ref Transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoUT.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {   //#3 Get Transaction Column
                        if (!repoUT.CboAccountChecker(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            if (!repoUT.GetTransactionJoinAccountBranch(workflowInstanceID, ref JoinAccount, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                            }
                            else
                            {
                                output.Transaction = Transaction;
                                output.Timelines = timelines;
                                output.JoinAccount = JoinAccount;


                                return Request.CreateResponse(HttpStatusCode.OK, output);
                            }

                        }
                    }

                }
            }
            finally
            {
                Transaction = null;
                timelines = null;
            }
        }



        [ResponseType(typeof(UTBranchCheckerDetailModel))]
        [HttpGet]
        [Route("api/WorkflowUT/{workflowInstanceID}/TransactionUT/BranchChecker/{approverID}")]
        public HttpResponseMessage GetUTBrancCheckerDetails(Guid workflowInstanceID, long approverID)
        {
            UTDetailModel Transaction = new UTDetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            UTBranchCheckerDetailModel output = new UTBranchCheckerDetailModel();
            IList<MutualFundCBOCheckerModel> MutualFund = new List<MutualFundCBOCheckerModel>();
            IList<JoinAccountBranchCheckerModel> JoinAccount = new List<JoinAccountBranchCheckerModel>();
            try
            {
                // #1 Get UT Details
                if (!repoUT.GetTransactionDetails(workflowInstanceID, ref Transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoUT.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {   //#3 Get Transaction Column
                        if (!repoUT.GetTransactionCheckerDetails(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            if (!repoUT.GetTransactionMutualFundBranch(workflowInstanceID, ref MutualFund, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                            }
                            else
                            {
                                if (!repoUT.GetTransactionJoinAccountBranch(workflowInstanceID, ref JoinAccount, ref message))
                                {
                                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                                }


                                else
                                {
                                    output.Transaction = Transaction;
                                    output.Timelines = timelines;
                                    output.MutualFund = MutualFund;
                                    output.JoinAccount = JoinAccount;

                                    return Request.CreateResponse(HttpStatusCode.OK, output);
                                }
                            }
                        }
                    }

                }
            }
            finally
            {
                Transaction = null;
                timelines = null;
            }
        }

        [ResponseType(typeof(UTBranchCheckerDetailModel))]
        [HttpGet]
        [Route("api/WorkflowUT/{workflowInstanceID}/TransactionUT/UTChecker/{approverID}")]
        public HttpResponseMessage GetCBOUTChecker(Guid workflowInstanceID, long approverID)
        {
            UTDetailModel Transaction = new UTDetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            UTBranchCheckerDetailModel output = new UTBranchCheckerDetailModel();
            UTCBOChecker output2 = new UTCBOChecker();
            IList<MutualFundCBOCheckerModel> MutualFund = new List<MutualFundCBOCheckerModel>();
            IList<JoinAccountBranchCheckerModel> JoinAccount = new List<JoinAccountBranchCheckerModel>();

            try
            {
                // #1 Get UT Details
                if (!repoUT.GetTransactionDetails(workflowInstanceID, ref Transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoUT.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {   //#3 Get Transaction Column
                        if (!repoUT.GetTransactionUTCheckerDetails(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {//#4 Get Fund
                            if (!repoUT.GetTransactionMutualFund(workflowInstanceID, ref MutualFund, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                            }
                            else
                            {
                                if (!repoUT.GetTransactionJoinAccountUTChecker(workflowInstanceID, ref JoinAccount, ref message))
                                {
                                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                                }

                                else
                                {

                                    output.Transaction = Transaction;
                                    output.Timelines = timelines;
                                    output.MutualFund = MutualFund;
                                    output.JoinAccount = JoinAccount;

                                    return Request.CreateResponse(HttpStatusCode.OK, output);
                                }
                            }
                        }
                    }
                }
            }
            finally
            {
                Transaction = null;
                timelines = null;
            }
        }
        #endregion

        #region Agung
        /// <summary>
        /// Add Controller below...
        /// </summary>
        //Agung SUhendar 17 Oktober 2015
        /// <summary>
        /// Get payment details of transaction by WF Instance ID
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <returns></returns>
        [ResponseType(typeof(UTCheckerDetailModel))]
        [HttpGet]
        [Route("api/WorkflowUT/{workflowInstanceID}/TransactionUT/Checker/{approverID}")]
        public HttpResponseMessage GetUTCheckerDetails(Guid workflowInstanceID, long approverID)
        {
            UTDetailModel ut = new UTDetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            UTCheckerDetailModel output = new UTCheckerDetailModel();

            try
            {
                // #1 Get UT Details
                if (!repoUT.GetUTDetails(workflowInstanceID, ref ut, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoUT.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        output.Transaction = ut;
                        output.Timelines = timelines;

                        return Request.CreateResponse(HttpStatusCode.OK, output);
                    }
                }
            }
            finally
            {
                ut = null;
                timelines = null;
            }
        }
        #endregion

        #region Team
        #region Dani
        [ResponseType(typeof(ReSubmitBranchMakerDetailModel))]
        [HttpGet]
        [Route("api/WorkflowUT/{workflowInstanceID}/TransactionUT/BranchMakerResubmit/{approverID}")]
        public HttpResponseMessage GetUTResubmitBranchmakerDetail(Guid workflowInstanceID, long approverID)
        {
            UTDetailModel Transaction = new UTDetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            ReSubmitBranchMakerDetailModel output = new ReSubmitBranchMakerDetailModel();
            IList<MutualFundBranchMakerModel> MutualFund = new List<MutualFundBranchMakerModel>();
            IList<JoinAccountBranchMakerModel> JoinAccount = new List<JoinAccountBranchMakerModel>();
            try
            {
                // #1 Get UT Details
                if (!repoUT.GetUTDetails(workflowInstanceID, ref Transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoUT.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {   //#3 Get 
                        if (!repoUT.GetTransactionCheckerDetailForBranchMaker(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            //#4
                            if (!repoUT.GetTransactionMutualFundBranchMaker(workflowInstanceID, ref MutualFund, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                            }

                            else
                            {
                                //#5
                                if (!repoUT.GetTransactionJoinAccountBranchMaker(workflowInstanceID, ref JoinAccount, ref message))
                                {
                                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                                }
                                else
                                {
                                    output.Transaction = Transaction;
                                    output.Timelines = timelines;
                                    output.MutualFund = MutualFund;
                                    output.JoinAccount = JoinAccount;
                                    return Request.CreateResponse(HttpStatusCode.OK, output);
                                }
                                
                            }
                        }
                    }

                }
            }
            finally
            {
                Transaction = null;
                timelines = null;
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowUT/TransactionUT/BranchMakerResubmit")]
        public HttpResponseMessage UpdateUTResubmitBranchmakerDetail(ReSubmitBranchMakerDetailModel data)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoUT.UpdateTransactionResubmitByBranchMaker(data, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "Update Data has been created." });
                }
            }
        }

        [ResponseType(typeof(ResubmitCBOUTMakerModel))]
        [HttpGet]
        [Route("api/WorkflowUT/{workflowInstanceID}/TransactionUT/CBOUTMakerResubmit/{approverID}")]
        public HttpResponseMessage GetUTResubmitCBOUTMakerDetail(Guid workflowInstanceID, long approverID)
        {
            UTDetailModel Transaction = new UTDetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            ResubmitCBOUTMakerModel output = new ResubmitCBOUTMakerModel();
            IList<MutualFundCBOUTMakerModel> MutualFund = new List<MutualFundCBOUTMakerModel>();
            IList<JoinAccountBranchMakerModel> JoinAccount = new List<JoinAccountBranchMakerModel>();
            try
            {
                // #1 Get UT Details
                if (!repoUT.GetUTDetails(workflowInstanceID, ref Transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoUT.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {   //#3 Get 
                        if (!repoUT.GetTransactionCheckerDetailForCBOUTMaker(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            //#4
                            if (!repoUT.GetMutualFundTransactionForCBOMaker(workflowInstanceID, ref MutualFund, ref message))
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                            }

                            else
                            {
                                //#5
                                if (!repoUT.GetTransactionJoinAccountBranchMaker(workflowInstanceID, ref JoinAccount, ref message))
                                {
                                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                                }
                                else
                                {
                                    output.Transaction = Transaction;
                                    output.Timelines = timelines;
                                    output.MutualFund = MutualFund;
                                    output.JoinAccount = JoinAccount;
                                    return Request.CreateResponse(HttpStatusCode.OK, output);
                                }

                            }
                        }
                    }

                }
            }
            finally
            {
                Transaction = null;
                timelines = null;
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowUT/TransactionUT/CBOUTMakerResubmit")]
        public HttpResponseMessage UpdateUTResubmitBranchmakerDetail(ResubmitCBOUTMakerModel data)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoUT.UpdateTransactionResubmitByCBOUTMaker(data, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "Update Data has been created." });
                }
            }
        }
        #endregion

        #region Basri
        #endregion

        #region Chandra
        #endregion

        #region Andi
        #endregion

        #region Afif
        [ResponseType(typeof(UTCBOAccountMakerModel))]
        [HttpGet]
        [Route("api/WorkflowUT/{workflowInstanceID}/TransactionUT/CBOAccountMaker/{approverID}")]
        public HttpResponseMessage GetUTDetailsCBOAccountMaker(Guid workflowInstanceID, long approverID)
        {
            UTCBOAccountMakerModel output = new UTCBOAccountMakerModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            try
            {
                // #1 Get UT Details
                if (!repoUT.GetUTCBOAccountMaker(workflowInstanceID, ref output, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoUT.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        output.Timelines = timelines;
                        return Request.CreateResponse(HttpStatusCode.OK, output);
                    }
                }
            }
            finally
            {
                output = null;
                timelines = null;
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowUT/{workflowInstanceID}/TransactionUT/CBOAccountMakerUpdate/{approverID}")]
        public HttpResponseMessage AddUTDetailsCBOAccountMaker(Guid workflowInstanceID, long approverID, UTCBOAccountMakerModel transaction)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoUT.AddTransactionCBOAccountMaker(workflowInstanceID, approverID, transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction maker data revision has been saved." });
                }
            }
        }

        [ResponseType(typeof(UTCBOUTMakerModel))]
        [HttpGet]
        [Route("api/WorkflowUT/{workflowInstanceID}/TransactionUT/CBOUTMaker/{approverID}")]
        public HttpResponseMessage GetUTDetailsCBOUTMaker(Guid workflowInstanceID, long approverID)
        {
            UTCBOUTMakerModel output = new UTCBOUTMakerModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            try
            {
                // #1 Get UT Details
                if (!repoUT.GetUTCBOUTMaker(workflowInstanceID, ref output, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoUT.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        output.Timelines = timelines;
                        return Request.CreateResponse(HttpStatusCode.OK, output);
                    }
                }
            }
            finally
            {
                output = null;
                timelines = null;
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowUT/{workflowInstanceID}/TransactionUT/CBOUTMakerUpdate/{approverID}")]
        public HttpResponseMessage AddUTDetailsCBOUTMaker(Guid workflowInstanceID, long approverID, UTBranchCheckerDetailModel transaction)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoUT.AddTransactionCBOUTMaker(workflowInstanceID, approverID, transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction maker data revision has been saved." });
                }
            }
        }
        #endregion
        #endregion

        /// <summary>
        /// Add Controller below...
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (repoUT != null)
                repoUT.Dispose();


            base.Dispose(disposing);
        }
    }
}

//=======================================================================================//
///FOR ALL DEVELOPER...
///PLEASE ADD NEW REGION BY YOUR NAME TO KEEP CLEAN !!! DONT BE A DIRTY PROGRAMMER!!!
///ANDIWIJAYA
//=======================================================================================//