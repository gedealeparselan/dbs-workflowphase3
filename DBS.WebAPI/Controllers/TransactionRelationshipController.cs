﻿using System;
using DBS.WebAPI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Description;
using System.Web;
using System.Web.Http;

namespace DBS.WebAPI.Controllers
{
    //[Authorize]
    public class TransactionRelationshipController : ApiController
    {
        private string message = string.Empty;

        private readonly ITransactionRelationshipRepository repository = null;

        public TransactionRelationshipController()
        {
            this.repository = new TransactionRelationshipRepository();
        }

        public TransactionRelationshipController(ITransactionRelationshipRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all bene segments.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<TransactionRelationshipModel>))]
        public HttpResponseMessage Get()
        {
            IList<TransactionRelationshipModel> output = new List<TransactionRelationshipModel>();

            if (!repository.GetTransactionRelationship(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<TransactionRelationshipModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get bene segments with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<TransactionRelationshipModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<TransactionRelationshipModel> output = new List<TransactionRelationshipModel>();

            if (!repository.GetTransactionRelationship(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<TransactionRelationshipModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get transaction relationships for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionRelationshipGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<TransactionRelationshipFilter> filters)
        {
            TransactionRelationshipGrid output = new TransactionRelationshipGrid();

            if (!repository.GetTransactionRelationship(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<TransactionRelationshipGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find bene segments using criterias.
        /// </summary>
        /// <param name="query">Code or Description.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<TransactionRelationshipModel>))]
        [HttpGet]
        [Route("api/TransactionRelationship/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<TransactionRelationshipModel> output = new List<TransactionRelationshipModel>();

            if (!repository.GetTransactionRelationship(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<TransactionRelationshipModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get bene segment by ID.
        /// </summary>
        /// <param name="id">Bene Segment ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionRelationshipModel))]
        [Route("api/TransactionRelationship/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            TransactionRelationshipModel output = new TransactionRelationshipModel();

            if (!repository.GetTransactionRelationshipByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<TransactionRelationshipModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new transaction relationship.
        /// </summary>
        /// <param name="transactionRelationship">Transaction Relationship data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(TransactionRelationshipModel transactionRelationship)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddTransactionRelationship(transactionRelationship, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New Transaction Relationship data has been created." });
                }
            }
        }

        /// <summary>
        /// Modify existing transaction relationship.
        /// </summary>
        /// <param name="id">ID of Transaction Relationship to be modify.</param>
        /// <param name="transactionRelationship">Transaction relationship data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/TransactionRelationship/{id}")]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(int id, TransactionRelationshipModel transactionRelationship)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateTransactionRelationship(id, transactionRelationship, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction Relationship data has been updated." });
                }
            }
        }

        /// <summary>
        /// Remove exisiting transaction relationship.
        /// </summary>
        /// <param name="id">ID of transaction relationship to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/TransactionRelationship/{id}")]
        //[Authorize(Roles = "DBS Admin")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteTransactionRelationship(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction Relationship data has been deleted." });
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }

}