﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using System.Net.Http;
using System.Web.Script.Serialization;

namespace DBS.WebAPI.Models
{
    #region Property

    [ModelName("WorkflowEntity")]
    public class WorkflowEntityModel
    {
        /// <summary>
        /// Role ID.
        /// </summary>
        public long ID { get; set; }

        /// <summary>
        /// Entity Name.
        /// </summary>
        public string EntityName { get; set; }

        /// <summary>
        /// Entity Name.
        /// </summary>
        public string TaskName { get; set; }

        /// <summary>
        /// Entity Name.
        /// </summary>
        public string ActionType { get; set; }

        /// <summary>
        /// Entity Name.
        /// </summary>
        public Guid WorkflowID { get; set; }

        /// <summary>
        /// Entity Name.
        /// </summary>
        public string KeyID { get; set; }

        /// <summary>
        /// Entity Name.
        /// </summary>
        public string Data { get; set; }
        public string ApproverID { get; set; }
        public string SPWebID { get; set; }
        public string SPSiteID { get; set; }
        public Guid SPListID { get; set; }
        public int SPListItemID { get; set; }
        public Guid SPTaskListID { get; set; }
        public int SPTaskListItemID { get; set; }
        public string Initiator { get; set; }
        public Guid WorkflowInstanceID { get; set; }
        public string WorkflowName { get; set; }
        public string StartTime { get; set; }
        public string StateID { get; set; }
        public string StateDescription { get; set; }
        public string IsAuthorized { get; set; }

        public string FilterValue { get; set; }
        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }
        public int TaskType { get; set; }
        public int WorkflowTaskType { get; set; }
    }

    public class WorkflowEntityRow : WorkflowEntityModel
    {
        public int RowID { get; set; }

    }

    public class WorkflowEntityGrid : Grid
    {
        public IList<WorkflowEntityRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class WorkflowEntityFilter : Filter { }
    #endregion

    #region Interface
    public interface IWorkflowEntityRepository : IDisposable
    {
        bool ApproveByID(string id, ref WorkflowEntityModel data, ref string message);
        bool GetByID(string id, ref WorkflowEntityModel data, ref string message);
        bool Get(ref IList<WorkflowEntityModel> data, int limit, int index, ref string message);
        bool Get(ref IList<WorkflowEntityModel> data, ref string message);
        bool Get(int page, int size, IList<WorkflowEntityFilter> filters, string sortColumn, string sortOrder, ref WorkflowEntityGrid roleGrid, ref string message);
        bool GetSPHome(int page, int size, IList<WorkflowEntityFilter> filters, string sortColumn, string sortOrder, ref WorkflowEntityGrid roleGrid, ref string message);
        bool GetSP(int page, int size, IList<WorkflowEntityFilter> filters, string sortColumn, string sortOrder, ref WorkflowEntityGrid roleGrid, ref string message);
        bool Get(WorkflowEntityFilter filter, ref IList<WorkflowEntityModel> data, ref string message);
        bool Get(string key, int limit, ref IList<WorkflowEntityModel> data, ref string message);
        bool Add(WorkflowEntityModel data, ref string message);
        bool Update(long id, WorkflowEntityModel data, ref string message);
        bool Delete(long id, ref string message);
    }
    #endregion

    #region Repository

    public class FileLog
    {
        string _file;
        System.IO.StreamWriter writer;
        public FileLog(string file)
        {
            this._file = file;
        }
        public void Open()
        {
            writer = new System.IO.StreamWriter(this._file);
        }
        public void Close()
        {
            writer.Close();
        }
        public void WriteLine(string s)
        {
            writer.WriteLine(s);
        }
    }
    public class WorkflowEntityRepository : IWorkflowEntityRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        private IQueryable<WorkflowEntityModel> GetAllJoin(DBSEntities context, string EntityName = null, string filterCIF = null)
        {

            List<WorkflowEntity> filterEntity = new List<WorkflowEntity>();
            if (EntityName != null && EntityName.Equals("CustomerContact"))
            {
                JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                var x = from a in context.WorkflowEntities
                        where a.EntityName == EntityName && a.IsDeleted.Equals(false)
                        select a;

                foreach (var item in x)
                {
                    CustomerContactModel c = json_serializer.Deserialize<CustomerContactModel>(item.Data);
                    if (filterCIF != null && c.CIF.Equals(filterCIF))
                    {
                        filterEntity.Add(item);
                    }
                }

            }
            else
            {
                filterEntity = (from a in context.WorkflowEntities
                                select a).ToList();
            }

            string userName = currentUser.GetCurrentUser().LoginName;
            string[] roles = currentUser.GetCurrentUser().Roles.Select(r => r.Name).ToArray();
            var A = (
                //from a in context.WorkflowEntities
                    from a in filterEntity
                    join b in context.V_NintexWorkflowTasks on a.WorkflowID.Value equals b.WorkflowInstanceID
                    let isEntityName = !string.IsNullOrEmpty(EntityName)
                    //where a.IsDeleted.Equals(false) && (roles.Any(x => b.WorkflowTaskUserContribute.Contains(x)) || (isEntityName ? roles.Any(x => b.WorkflowInitiator.Equals(userName)) : true)) && b.WorkflowOutcomeDescription.Equals("Pending")
                    where
                    (isEntityName ? roles.Any(x => b.WorkflowInitiator.Equals(userName)) : (roles.Any(x => b.WorkflowTaskUser.Contains(x)) || roles.Any(x => b.WorkflowTaskUser.Contains(userName)))) &&
                    a.IsDeleted.Equals(false) && b.WorkflowOutcomeDescription.Equals("Pending")
                    orderby a.CreateDate descending
                    select new WorkflowEntityModel()
                    {
                        ID = a.WorkflowEntityID,
                        EntityName = a.EntityName,
                        TaskName = a.TaskName,
                        ActionType = a.ActionType,
                        Initiator = b.WorkflowInitiator,
                        SPListItemID = b.SPListItemID,
                        SPTaskListID = (Guid)b.SPTaskListID,
                        SPTaskListItemID = (int)b.SPTaskListItemID,
                        WorkflowID = (Guid)a.WorkflowID,
                        LastModifiedDate = a.CreateDate,
                        LastModifiedBy = a.CreateBy,
                        Data = a.Data,
                    }
                    );//.Skip(skip).Take(limit);
            var B = (
                    from a in context.RoleDrafts
                    join b in context.V_NintexWorkflowTasks on a.WorkflowInstanceID.Value equals b.WorkflowInstanceID
                    //where a.IsDeleted.Equals(false) && roles.Any(x => b.WorkflowTaskUserContribute.Contains(x)) && b.WorkflowOutcomeDescription.Equals("Pending")
                    where a.IsDeleted.Equals(false) && (roles.Any(x => b.WorkflowTaskUser.Contains(x)) || roles.Any(x => b.WorkflowTaskUser.Contains(userName))) && b.WorkflowOutcomeDescription.Equals("Pending")
                    orderby a.CreateDate descending
                    select new WorkflowEntityModel()
                    {
                        ID = a.RoleID,
                        EntityName = "Role",
                        //TaskName = "Master Role " + a.ActionType + " Approval Task",
                        TaskName = b.WorkflowTaskTitle,
                        ActionType = a.ActionType,
                        Initiator = b.WorkflowInitiator,
                        SPListItemID = b.SPListItemID,
                        SPTaskListID = (Guid)b.SPTaskListID,
                        SPTaskListItemID = (int)b.SPTaskListItemID,
                        WorkflowID = (Guid)a.WorkflowInstanceID,
                        LastModifiedDate = a.CreateDate,
                        LastModifiedBy = a.CreateBy,
                        Data = "{}"
                    }
                    );//.Skip(skip).Take(limit);
            var C = (
                    from a in context.EmployeeDrafts
                    join b in context.V_NintexWorkflowTasks on a.WorkflowInstanceID.Value equals b.WorkflowInstanceID
                    where a.IsDeleted.Equals(false) && (roles.Any(x => b.WorkflowTaskUser.Contains(x)) || roles.Any(x => b.WorkflowTaskUser.Contains(userName))) && b.WorkflowOutcomeDescription.Equals("Pending")
                    orderby a.CreateDate descending
                    select new WorkflowEntityModel()
                    {
                        ID = a.EmployeeDraftID,
                        EntityName = "User",
                        TaskName = b.WorkflowTaskTitle,
                        ActionType = b.WorkflowTaskTitle,
                        Initiator = b.WorkflowInitiator,
                        SPListItemID = b.SPListItemID,
                        SPTaskListID = (Guid)b.SPTaskListID,
                        SPTaskListItemID = (int)b.SPTaskListItemID,
                        WorkflowID = (Guid)a.WorkflowInstanceID,
                        LastModifiedDate = a.CreateDate,
                        LastModifiedBy = a.CreateBy,
                        Data = "{}"
                    });//.Skip(skip).Take(limit);
            var D = (
                    from a in context.CustomerUnderlyingDrafts
                    join b in context.V_NintexWorkflowTasks on a.WorkflowInstanceID.Value equals b.WorkflowInstanceID
                    where a.IsDeleted.Equals(false) && b.WorkflowOutcomeDescription.Equals("Pending")
                    orderby a.CreateDate descending
                    select new WorkflowEntityModel()
                    {
                        ID = a.UnderlyingID,
                        EntityName = "MasterUnderlying",
                        TaskName = b.WorkflowTaskTitle,
                        ActionType = a.Action,
                        Initiator = b.WorkflowInitiator,
                        SPListItemID = b.SPListItemID,
                        SPTaskListID = (Guid)b.SPTaskListID,
                        SPTaskListItemID = (int)b.SPTaskListItemID,
                        WorkflowID = (Guid)a.WorkflowInstanceID,
                        LastModifiedDate = a.CreateDate,
                        LastModifiedBy = a.CreateBy,
                        Data = "{}"
                    });
            return A.Concat(B).Concat(C).Concat(D).OrderByDescending(p => p.LastModifiedDate).AsQueryable();
        }

        //change by fandi for filter workflow entyti
        //private IQueryable<WorkflowEntityModel> GetSPAllJoinHome(DBSEntities context, string EntityName = null, string filterCIF = null)
        private IQueryable<WorkflowEntityModel> GetSPAllJoinHome(DBSEntities context, string TaskName, string ActionType, string ModifiedBy, DateTime? ModifiedDate)
        //end
        {

            /*  List<WorkflowEntity> filterEntity = new List<WorkflowEntity>();
              if (EntityName != null && EntityName.Equals("CustomerContact"))
              {
                  JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                  var x = from a in context.WorkflowEntities
                          where a.EntityName == EntityName && a.IsDeleted.Equals(false)
                          select a;

                  foreach (var item in x)
                  {
                      CustomerContactModel c = json_serializer.Deserialize<CustomerContactModel>(item.Data);
                      if (filterCIF != null && c.CIF.Equals(filterCIF))
                      {
                          filterEntity.Add(item);
                      }
                  }

              }
              else
              {
                  filterEntity = (from a in context.WorkflowEntities
                                  select a).ToList();
              } */

            string userName = currentUser.GetCurrentUser().LoginName;
            //change by fandi
            //var data = (from a in context.SP_GetTaskWorkflowHome(userName, null, null, null, null)
            var data = (from a in context.SP_GetTaskWorkflowHome(userName, TaskName, ActionType, ModifiedBy, ModifiedDate)
                        //end
                        select new WorkflowEntityModel
                        {
                            ID = a.ID,
                            EntityName = a.EntityName,
                            TaskName = a.TaskName,
                            ActionType = a.ActionType,
                            Initiator = a.WorkflowInitiator,
                            SPListItemID = a.SPListItemID,
                            SPTaskListID = (Guid)a.SPTaskListID,
                            SPTaskListItemID = (int)a.SPTaskListItemID,
                            WorkflowID = (Guid)a.WorkflowID,
                            LastModifiedDate = a.CreateDate,
                            LastModifiedBy = a.CreateBy,
                            Data = a.Data,
                            TaskType = a.WorkflowTaskType
                        }).ToList();

            return data.AsQueryable();
        }

        private IQueryable<WorkflowEntityModel> GetSPAllJoin(DBSEntities context, string EntityName, string filterCIF, string Taskname, string ActionType, string LastModifiedBy, DateTime? LastModifiedDate)
        {

            /*  List<WorkflowEntity> filterEntity = new List<WorkflowEntity>();
              if (EntityName != null && EntityName.Equals("CustomerContact"))
              {
                  JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                  var x = from a in context.WorkflowEntities
                          where a.EntityName == EntityName && a.IsDeleted.Equals(false)
                          select a;

                  foreach (var item in x)
                  {
                      CustomerContactModel c = json_serializer.Deserialize<CustomerContactModel>(item.Data);
                      if (filterCIF != null && c.CIF.Equals(filterCIF))
                      {
                          filterEntity.Add(item);
                      }
                  }

              }
              else
              {
                  filterEntity = (from a in context.WorkflowEntities
                                  select a).ToList();
              } */
            List<WorkflowEntity> filterEntity = new List<WorkflowEntity>();
            string cifValue = null;
            if (EntityName != null && (EntityName.Equals("CustomerContact") || EntityName.Equals("CustomerCallback") || EntityName.Equals("CustomerPOAEmail")))
            {
                if (filterCIF != null)
                {
                    cifValue = filterCIF;
                }
            }
            //start add azam
            if (EntityName != null && EntityName.Equals("CustomerCSO"))
            {
                if (filterCIF != null)
                {
                    cifValue = filterCIF;
                }
            }

            //end azam
            string userName = currentUser.GetCurrentUser().LoginName;
            var data = (from a in context.SP_GetTaskWorkflow(EntityName, cifValue, Taskname, ActionType, LastModifiedBy, LastModifiedDate)
                        select new WorkflowEntityModel
                        {
                            ID = a.ID,
                            EntityName = a.EntityName,
                            TaskName = a.TaskName,
                            ActionType = a.ActionType,
                            //Initiator = a.,
                            //SPListItemID = a.SPListItemID,
                            //SPTaskListID = (Guid)a.SPTaskListID,
                            //SPTaskListItemID = (int)a.SPTaskListItemID,
                            WorkflowID = (Guid)a.WorkflowID,
                            LastModifiedDate = a.CreateDate,
                            LastModifiedBy = a.CreateBy,
                            Data = a.Data
                        }).ToList();

            return data.AsQueryable();
        }

        public bool ApproveByID(string id, ref WorkflowEntityModel rec, ref string message)
        {
            bool isSuccess = false;
            try
            {
                isSuccess = this.GetByID(id, ref rec, ref message);
                if (isSuccess)
                {
                    //filelog.WriteLine("Approval " + rec.EntityName);
                    /*
                    DBS.WebAPI.Helpers.WorkflowEntityProcess p = new DBS.WebAPI.Helpers.WorkflowEntityProcess(@"http://mysweetlife:90/", "Admin1", "pass@word1");

                    Guid gid = rec.WorkflowID;
                    var item = (from a in context.V_NintexWorkflowTasks
                               where a.WorkflowInstanceID == gid
                               select new {
                                TaskListID = a.SPTaskListID==null?Guid.Empty:(Guid)a.SPTaskListID,
                                TaskListItemID = a.SPTaskListItemID == null ? 0 : (int)a.SPTaskListItemID
                               }).Single();

                    DBS.WebAPI.Helpers.RespondApprovalTaskResult result = p.RespondApprovalTask(item.TaskListID, item.TaskListItemID, "0", "Testing comment approval").Result;

                    if (result.IsSuccess)
                    {*/
                    JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                    int iID = 0;
                    Guid gID = Guid.Empty;
                    switch (rec.EntityName)
                    {
                        case "Adhoc":
                            var Adhoc = json_serializer.Deserialize<AdhocModel>(rec.Data);
                            var repoAdhoc = new AdhocRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repoAdhoc.AddAdhoc(Adhoc, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoAdhoc.UpdateAdhoc(iID, Adhoc, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoAdhoc.DeleteAdhoc(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "MainDataCustomer":
                            try
                            {
                                var MainDataCustomer = json_serializer.Deserialize<IList<MainDataCustomerCenterModel>>(rec.Data);
                                var repomain = new MainDataCustomerCenterRepository();
                                switch (rec.ActionType)
                                {
                                    case "ADD":
                                        if (!repomain.AddMainDataCustomerCenter(MainDataCustomer, ref message)) rec.Data = message;
                                        break;
                                    //case "UPDATE":
                                    //    int.TryParse(rec.KeyID, out iID);
                                    //    if (!repocbomaintenancepriorityOrder.(iID, bank, ref message)) rec.Data = message;
                                    //    break;
                                    //case "DELETE":
                                    //    int.TryParse(rec.KeyID, out iID);
                                    //    if (!repoabank.DeleteBank(iID, ref message)) rec.Data = message;
                                    //    break;
                                }
                            }
                            catch (Exception ex)
                            {

                                message = ex.Message;
                                FileLog filelog = new FileLog(@"C:\WINDOWS\TEMP\DBSLog2.txt");
                                filelog.Open();
                                filelog.WriteLine("WorkflowEntity Approval");
                                filelog.WriteLine(ex.Message);
                                if (rec != null) filelog.WriteLine(rec.Data);
                                filelog.WriteLine("SDATA");
                                filelog.Close();
                            }

                            break;
                        case "CBOMaintenancePriorityOrder":
                            try
                            {
                                var cbomaintenancepriorityOrder = json_serializer.Deserialize<IList<CBOMaintenancePriorityOrderModel>>(rec.Data);
                                var repocbomaintenancepriorityOrder = new CBOMaintenanceOrderRepository();
                                switch (rec.ActionType)
                                {
                                    case "ADD":
                                        if (!repocbomaintenancepriorityOrder.AddCBOMaintenaceType(cbomaintenancepriorityOrder, ref message)) rec.Data = message;
                                        break;
                                    //case "UPDATE":
                                    //    int.TryParse(rec.KeyID, out iID);
                                    //    if (!repocbomaintenancepriorityOrder.(iID, bank, ref message)) rec.Data = message;
                                    //    break;
                                    //case "DELETE":
                                    //    int.TryParse(rec.KeyID, out iID);
                                    //    if (!repoabank.DeleteBank(iID, ref message)) rec.Data = message;
                                    //    break;
                                }
                            }
                            catch (Exception ex)
                            {

                                message = ex.Message;
                                FileLog filelog = new FileLog(@"C:\WINDOWS\TEMP\DBSLog2.txt");
                                filelog.Open();
                                filelog.WriteLine("WorkflowEntity Approval");
                                filelog.WriteLine(ex.Message);
                                if (rec != null) filelog.WriteLine(rec.Data);
                                filelog.WriteLine("SDATA");
                                filelog.Close();
                            }

                            break;
                        case "Holiday":
                            var Holiday = json_serializer.Deserialize<HolidayModel>(rec.Data);
                            var repoHoliday = new HolidayRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repoHoliday.AddHoliday(Holiday, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoHoliday.UpdateHoliday(iID, Holiday, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoHoliday.DeleteHoliday(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "MaintenanceType":
                            var MaintenanceType = json_serializer.Deserialize<MaintenanceTypeModel>(rec.Data);
                            var repoMaintenanceType = new MaintenanceTypeRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repoMaintenanceType.AddMaintenanceType(MaintenanceType, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoMaintenanceType.UpdateMaintenanceType(iID, MaintenanceType, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoMaintenanceType.DeleteMaintenanceType(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "MainDataCustomerCenter":
                            var MainDataCustomerCenter = json_serializer.Deserialize<MainDataCustomerCenterModel>(rec.Data);
                            var repoMainDataCustomerCenter = new MainDataCustomerCenterRepository();
                            switch (rec.ActionType)
                            {
                                //case "ADD":
                                //    if (!repoMainDataCustomerCenter.AddAdhoc(Adhoc, ref message)) rec.Data = message;
                                //    break;
                                //case "UPDATE":
                                //    int.TryParse(rec.KeyID, out iID);
                                //    if (!repoAdhoc.UpdateAdhoc(iID, Adhoc, ref message)) rec.Data = message;
                                //    break;
                                //case "DELETE":
                                //    int.TryParse(rec.KeyID, out iID);
                                //    if (!repoAdhoc.DeleteAdhoc(iID, ref message)) rec.Data = message;
                                //    break;
                            }
                            break;
                        case "AgeingReport":
                            var aegingreport = json_serializer.Deserialize<AgeingReportModel>(rec.Data);
                            var repoaegingreport = new AgeingReportRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repoaegingreport.AddAgeingReport(aegingreport, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoaegingreport.UpdateAgeingReport(iID, aegingreport, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoaegingreport.DeleteAgeingReport(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "AuditFirm":
                            var auditfirm = json_serializer.Deserialize<AuditFirmModel>(rec.Data);
                            var repoauditfirm = new AuditFirmRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repoauditfirm.AddAuditFirm(auditfirm, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoauditfirm.UpdateAuditFirm(iID, auditfirm, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoauditfirm.DeleteAuditFirm(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "TransactionType":
                            var transactiontype = json_serializer.Deserialize<TransactionTypeModel>(rec.Data);
                            var repotransactiontype = new TransactionTypeRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repotransactiontype.AddTransactionType(transactiontype, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repotransactiontype.UpdateTransactionType(iID, transactiontype, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repotransactiontype.DeleteTransactionType(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "Bank":
                            var bank = json_serializer.Deserialize<BankModel>(rec.Data);
                            var repoabank = new BankRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repoabank.AddBank(bank, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoabank.UpdateBank(iID, bank, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoabank.DeleteBank(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;

                        case "CustomerCSO":
                            var customerCso = json_serializer.Deserialize<CustomerCSOModel>(rec.Data);
                            var repoCustomerCso = new CustomerCSORepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repoCustomerCso.AddCustomerCSO(customerCso.CIF, customerCso, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoCustomerCso.UpdateCustomerCSO(customerCso.CIF, iID, customerCso, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoCustomerCso.DeleteCustomerCSO(customerCso.CIF, customerCso.CustCSOID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "CustomerCallback":
                            var customerCallback = json_serializer.Deserialize<CustomerCallbackModel>(rec.Data);
                            var repoCustomerCallback = new CustomerCallbackRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repoCustomerCallback.Add(customerCallback, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "Nostro":
                            var nostro = json_serializer.Deserialize<NostroModel>(rec.Data);
                            var reponostro = new NostroRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!reponostro.AddNostro(nostro, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!reponostro.UpdateNostro(iID, nostro, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!reponostro.DeleteNostro(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "Sundry":
                            var sundry = json_serializer.Deserialize<SundryModel>(rec.Data);
                            var reposundry = new SundryRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!reposundry.AddSundry(sundry, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!reposundry.UpdateSundry(iID, sundry, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!reposundry.DeleteSundry(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;

                        //parameter-maintenence by Adi
                        case "FindingEmailTime":
                            var Model = json_serializer.Deserialize<FindingEmailTimeModel>(rec.Data);
                            var repoFindingEmail = new FindingEmailTimeRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repoFindingEmail.AddFindingEmaliTime(Model, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoFindingEmail.UpdateFindingEmailTime(iID, Model, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoFindingEmail.deleteFindingEmailTime(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;

                        case "CBOFund":
                            var cbofund = json_serializer.Deserialize<CBOFundModel>(rec.Data);
                            var repocbofund = new CBOFundRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repocbofund.AddCBOFund(cbofund, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repocbofund.UpdateCBOFund(iID, cbofund, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repocbofund.DeleteCBOFund(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;

                        case "BizSegment":
                            var bizsegment = json_serializer.Deserialize<BizSegmentModel>(rec.Data);
                            var repobizsegment = new BizSegmentRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repobizsegment.AddBizSegment(bizsegment, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repobizsegment.UpdateBizSegment(iID, bizsegment, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repobizsegment.DeleteBizSegment(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "Rank":
                            var rank = json_serializer.Deserialize<RankModel>(rec.Data);
                            var reporank = new RankRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!reporank.AddRank(rank, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!reporank.UpdateRank(iID, rank, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!reporank.DeleteRank(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "Region":
                            var region = json_serializer.Deserialize<RegionModel>(rec.Data);
                            var reporegion = new RegionRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!reporegion.AddRegion(region, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!reporegion.UpdateRegion(iID, region, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!reporegion.DeleteRegion(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "Branch":
                            var branch = json_serializer.Deserialize<BranchModel>(rec.Data);
                            var repobranch = new BranchRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repobranch.AddBranch(branch, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repobranch.UpdateBranch(iID, branch, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repobranch.DeleteBranch(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "Channel":
                            var channel = json_serializer.Deserialize<ChannelModel>(rec.Data);
                            var repochannel = new ChannelRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repochannel.AddChannel(channel, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repochannel.UpdateChannel(iID, channel, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repochannel.DeleteChannel(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "ChargesType":
                            var chargestype = json_serializer.Deserialize<ChargesTypeModel>(rec.Data);
                            var repochargestype = new ChargesTypeRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repochargestype.AddChargesType(chargestype, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repochargestype.UpdateChargesType(iID, chargestype, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repochargestype.DeleteChargesType(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "POAFunction":
                            var poafunction = json_serializer.Deserialize<POAFunctionModel>(rec.Data);
                            var repopoafunction = new POAFunctionRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repopoafunction.AddPOAFunction(poafunction, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repopoafunction.UpdatePOAFunction(iID, poafunction, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repopoafunction.DeletePOAFunction(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "Currency":
                            var currency = json_serializer.Deserialize<CurrencyRateModel>(rec.Data);
                            var repocurrency = new CurrencyRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repocurrency.AddCurrency(currency, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repocurrency.UpdateCurrency(iID, currency, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repocurrency.DeleteCurrency(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "customertype":
                            var customertype = json_serializer.Deserialize<CustomerTypeModel>(rec.Data);
                            var repocustomertype = new CustomerTypeRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repocustomertype.AddCustomerType(customertype, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repocustomertype.UpdateCustomerType(iID, customertype, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repocustomertype.DeleteCustomerType(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "ExceptionHandling":
                            var exceptionHandling = json_serializer.Deserialize<ExceptionHandlingModel>(rec.Data);
                            var repoexceptionHandling = new ExceptionHandlingRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repoexceptionHandling.AddExceptionHandling(exceptionHandling, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoexceptionHandling.UpdateExceptionHandling(iID, exceptionHandling, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoexceptionHandling.DeleteExceptionHandling(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "functionrole":
                            var functionrole = json_serializer.Deserialize<FunctionRoleModel>(rec.Data);
                            var repofunctionrole = new FunctionRoleRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repofunctionrole.AddFunctionRole(functionrole, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repofunctionrole.UpdateFunctionRole(iID, functionrole, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repofunctionrole.DeleteFunctionRole(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "Matrix":
                            var matrixapproval = json_serializer.Deserialize<MatrixModel>(rec.Data);
                            var repomatrixapproval = new MatrixRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repomatrixapproval.AddMatrix(matrixapproval, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repomatrixapproval.UpdateMatrix(iID, matrixapproval, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repomatrixapproval.DeleteMatrix(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "MatrixDOA":
                            var matrixapprovaldoa = json_serializer.Deserialize<MatrixDOAModel>(rec.Data);
                            var repomatrixapprovaldoa = new MatrixDOARepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repomatrixapprovaldoa.AddMatrixDOA(matrixapprovaldoa, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repomatrixapprovaldoa.UpdateMatrixDOA(iID, matrixapprovaldoa, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repomatrixapprovaldoa.DeleteMatrixDOA(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "Product":
                            var product = json_serializer.Deserialize<ProductModel>(rec.Data);
                            var repoproduct = new ProductRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repoproduct.AddProduct(product, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoproduct.UpdateProduct(iID, product, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoproduct.DeleteProduct(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "purposedocs":
                            var purposedocs = json_serializer.Deserialize<DocumentPurposeModel>(rec.Data);
                            var repopurposedocs = new PurposeDocsRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repopurposedocs.AddPurposeDocs(purposedocs, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repopurposedocs.UpdatePurposeDocs(iID, purposedocs, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repopurposedocs.DeletePurposeDocs(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "SLASetting":
                            var slasetting = json_serializer.Deserialize<SLASettingModel>(rec.Data);
                            var reposlasetting = new SLASettingRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!reposlasetting.AddSLASetting(slasetting, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!reposlasetting.UpdateSLASetting(iID, slasetting, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!reposlasetting.DeleteSLASetting(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "DocType":
                            var doctype = json_serializer.Deserialize<DocumentTypeModel>(rec.Data);
                            var repodoctype = new DocumentTypeRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repodoctype.AddDocType(doctype, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repodoctype.UpdateDocType(iID, doctype, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repodoctype.DeleteDocType(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "UnderlyingDoc":
                            var underlyingdoc = json_serializer.Deserialize<UnderlyingDocModel>(rec.Data);
                            var repounderlyingdoc = new UnderlyingDocRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repounderlyingdoc.AddUnderlyingDoc(underlyingdoc, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repounderlyingdoc.UpdateUnderlyingDoc(iID, underlyingdoc, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repounderlyingdoc.DeleteUnderlyingDoc(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "fxtransthreshold":
                            var fxtransthreshold = json_serializer.Deserialize<FXTransThresholdModel>(rec.Data);
                            var repofxtransthreshold = new FXTransThresholdRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repofxtransthreshold.AddFXTransThreshold(fxtransthreshold, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repofxtransthreshold.UpdateFXTransThreshold(iID, fxtransthreshold, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repofxtransthreshold.DeleteFXTransThreshold(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "fxcompliance":
                            var fxcompliance = json_serializer.Deserialize<FXComplianceModel>(rec.Data);
                            var repofxcompliance = new FXComplianceRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repofxcompliance.AddFXCompliance(fxcompliance, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repofxcompliance.UpdateFXCompliance(iID, fxcompliance, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repofxcompliance.DeleteFXCompliance(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "ProductType":
                            string sdata = rec.Data;
                            sdata = sdata.Replace("\"IsFlowValas\":0", "\"IsFlowValas\":false");
                            sdata = sdata.Replace("\"IsFlowValas\":1", "\"IsFlowValas\":true");

                            var ProductType = json_serializer.Deserialize<ProductTypeModel>(sdata);
                            var repoProductType = new ProductTypeRepository();

                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repoProductType.AddProductType(ProductType, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoProductType.UpdateProductType(iID, ProductType, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoProductType.DeleteProductType(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "LLD":
                            var LLD = json_serializer.Deserialize<LLDModel>(rec.Data);
                            var repoLLD = new LLDRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repoLLD.AddLLD(LLD, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoLLD.UpdateLLD(iID, LLD, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoLLD.DeleteLLD(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "LLDDocument":
                            var LLDDocument = json_serializer.Deserialize<LLDDocumentModel>(rec.Data);
                            var repoLLDDocument = new LDDDocumentRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repoLLDDocument.AddLLDDocument(LLDDocument, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoLLDDocument.UpdateLLDDocument(iID, LLDDocument, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoLLDDocument.DeleteLLDDocument(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "MiddlewareParameterSystem":
                            var MiddlewareParameterSystem = json_serializer.Deserialize<MiddlewareParameterSystemModel>(rec.Data);
                            var MiddlewareRepository = new MiddlewareParameterSystemRepository();
                            switch (rec.ActionType)
                            {
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!MiddlewareRepository.UpdateMiddlewareParameterSystem(iID, MiddlewareParameterSystem, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "ParameterSystem":
                            var ParamSystem = json_serializer.Deserialize<ParameterSystemModel>(rec.Data);
                            var repoParamSystem = new ParameterSystemRepository();
                            string modul = "system";
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repoParamSystem.AddParamSystem(ParamSystem, ref message, modul)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoParamSystem.UpdateParamSystem(iID, ParamSystem, ref message, modul)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoParamSystem.DeleteParamSystem(iID, ref message, modul)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "ParameterSystemCollateral":
                            var ParamSystemCollateral = json_serializer.Deserialize<ParameterSystemModel>(rec.Data);
                            var repoParamSystemCollateral = new ParameterSystemRepository();
                            string modulCollateral = "collateral";
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repoParamSystemCollateral.AddParamSystem(ParamSystemCollateral, ref message, modulCollateral)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoParamSystemCollateral.UpdateParamSystem(iID, ParamSystemCollateral, ref message, modulCollateral)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoParamSystemCollateral.DeleteParamSystem(iID, ref message, modulCollateral)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "ParameterSystemLoan":
                            var ParamSystemLoan = json_serializer.Deserialize<ParameterSystemModel>(rec.Data);
                            var repoParamSystemLoan = new ParameterSystemRepository();
                            string modulLoan = "loan";
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repoParamSystemLoan.AddParamSystem(ParamSystemLoan, ref message, modulLoan)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoParamSystemLoan.UpdateParamSystem(iID, ParamSystemLoan, ref message, modulLoan)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoParamSystemLoan.DeleteParamSystem(iID, ref message, modulLoan)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "ParameterSystemTmo":
                            var ParamSystemTmo = json_serializer.Deserialize<ParameterSystemModel>(rec.Data);
                            var repoParamSystemTmo = new ParameterSystemRepository();
                            string modulTmo = "tmo";
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repoParamSystemTmo.AddParamSystem(ParamSystemTmo, ref message, modulTmo)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoParamSystemTmo.UpdateParamSystem(iID, ParamSystemTmo, ref message, modulTmo)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoParamSystemTmo.DeleteParamSystem(iID, ref message, modulTmo)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "ParameterSystemCif":
                            var ParamSystemCif = json_serializer.Deserialize<ParameterSystemModel>(rec.Data);
                            var repoParamSystemCif = new ParameterSystemRepository();
                            string modulCif = "cif";
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repoParamSystemCif.AddParamSystem(ParamSystemCif, ref message, modulCif)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoParamSystemCif.UpdateParamSystem(iID, ParamSystemCif, ref message, modulCif)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoParamSystemCif.DeleteParamSystem(iID, ref message, modulCif)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "ParameterSystemUt":
                            var ParamSystemUt = json_serializer.Deserialize<ParameterSystemModel>(rec.Data);
                            var repoParamSystemUt = new ParameterSystemRepository();
                            string modulUt = "ut";
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repoParamSystemUt.AddParamSystem(ParamSystemUt, ref message, modulUt)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoParamSystemUt.UpdateParamSystem(iID, ParamSystemUt, ref message, modulUt)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoParamSystemUt.DeleteParamSystem(iID, ref message, modulUt)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "ParameterSystemFd":
                            var ParamSystemFd = json_serializer.Deserialize<ParameterSystemModel>(rec.Data);
                            var repoParamSystemFd = new ParameterSystemRepository();
                            string modulFd = "fd";
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repoParamSystemFd.AddParamSystem(ParamSystemFd, ref message, modulFd)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoParamSystemFd.UpdateParamSystem(iID, ParamSystemFd, ref message, modulFd)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoParamSystemFd.DeleteParamSystem(iID, ref message, modulFd)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "DAYS_OF_DOUBLE_TRANSACTION_CHECK":
                        case "SCHEDULLER_BI_MIDRATE":
                        case "SCHEDULLER_CIF_FROM_FINACLE":
                        case "SCHEDULLER_TZ_REPORT_RECONCILE":
                        case "UNDERLYING_DOCUMENT_EXPIRY":
                        case "FX_TRANSACTION_TRESHOLD":
                        case "SMS":
                        case "CALLBACK_AMOUNT_TRESHOLD":
                        case "UTC_ATTEMPS":
                        case "PAYMENT_MODE":
                            var singlevalue = json_serializer.Deserialize<SingleValueParameter>(rec.Data);
                            var reposinglevalue = new SingleValueParameterRepository();
                            switch (rec.ActionType)
                            {
                                case "UPDATE":

                                    if (!reposinglevalue.UpdateSingleValueParameter(rec.EntityName, singlevalue, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "CUT_OFF_TIME":
                            /* case "CALLBACK_AMOUNT_TRESHOLD": */
                            var singlevaluearray = json_serializer.Deserialize<List<SingleValueParameter>>(rec.Data);
                            var reposinglevaluearray = new SingleValueParameterRepository();
                            switch (rec.ActionType)
                            {
                                case "UPDATE":
                                    foreach (var item in singlevaluearray)
                                    {
                                        if (!reposinglevaluearray.UpdateSingleValueParameter(item.Name, item, ref message)) rec.Data = message;
                                    }
                                    break;
                            }
                            break;
                        case "CustomerContact":
                            string scustomercontact = "";
                            try
                            {
                                scustomercontact = rec.Data;
                                //scustomercontact = scustomercontact.Replace("\"POAFunction\":\"\"", "\"POAFunction\":{}"); {"ID":0,"CIF":"0000111","Name":"AAAAA AAAAAA","BizSegment":{"ID":2,"Name":"CBG","Description":"Affluent Onshore"},"Branch":{"ID":1,"Name":"SURABAYA PLAZA BRI BRANCH"},"Type":{"ID":1,"Name":"Prime"},"RM":{"Name":"","SegmentID":0,"RankID":0,"BranchID":0,"FullName":"0 - ","Email":""},"NameContact":"sdfd","SourceID":"2","PhoneNumber":"sdfsf","DateOfBirth":"","IDNumber":"sdfsf","POAFunction":{"ID":7,"Name":"","Description":"","FullName":" - "},"OccupationInID":"sdfdsf","PlaceOfBirth":"sdfds","EffectiveDate":"","CancellationDate":"","LastModifiedBy":"","LastModifiedDate":""}
                                var customercontact = json_serializer.Deserialize<CustomerContactWFModel>(scustomercontact);
                                var repocustomercontact = new CustomerContactRepository();
                                switch (rec.ActionType)
                                {
                                    case "ADD":
                                        if (!repocustomercontact.AddCustomerContactWF(customercontact, ref message)) rec.Data = message;
                                        break;
                                    case "UPDATE":
                                        int.TryParse(rec.KeyID, out iID);
                                        if (!repocustomercontact.UpdateCustomerContactWF(iID, customercontact, ref message)) rec.Data = message;
                                        break;
                                    case "DELETE":
                                        int.TryParse(rec.KeyID, out iID);
                                        if (!repocustomercontact.DeleteCustomerContact(iID, ref message)) rec.Data = message;
                                        break;
                                }
                            }
                            catch (Exception ex)
                            {
                                message = ex.Message;
                                FileLog filelog = new FileLog(@"C:\WINDOWS\TEMP\DBSLog2.txt");
                                filelog.Open();
                                filelog.WriteLine("WorkflowEntity Approval");
                                filelog.WriteLine(ex.Message);
                                if (rec != null) filelog.WriteLine(rec.Data);
                                filelog.WriteLine("SDATA");
                                filelog.WriteLine(scustomercontact);
                                filelog.Close();
                            }
                            break;
                        case "Roles":
                            var role = json_serializer.Deserialize<RoleModel>(rec.Data);
                            var reporole = new RoleRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!reporole.AddRole(role, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!reporole.UpdateRole(iID, role, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!reporole.DeleteRole(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "EscalationCustomer":
                            var escalationcustomer = json_serializer.Deserialize<IList<EscalationCustomerModel>>(rec.Data);
                            var repoescalationcustomer = new EscalationCustomerRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repoescalationcustomer.AddEscalationCustomer(escalationcustomer, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "CollateralNotificationEmail":
                            string modulcollateral = "Collateral";
                            var collateralnotificationemail = json_serializer.Deserialize<NotificationEmailModel>(rec.Data);
                            var repocollateralnotificationemail = new NotificationEmailRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repocollateralnotificationemail.AddNotificationEmail(modulcollateral, collateralnotificationemail, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repocollateralnotificationemail.UpdateNotificationEmail(modulcollateral, iID, collateralnotificationemail, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repocollateralnotificationemail.DeleteNotificationEmail(modulcollateral, iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "CBONotificationEmail":
                            string modulcbo = "CBO";
                            var cbonotificationemail = json_serializer.Deserialize<NotificationEmailModel>(rec.Data);
                            var repocbonotificationemail = new NotificationEmailRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repocbonotificationemail.AddNotificationEmail(modulcbo, cbonotificationemail, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repocbonotificationemail.UpdateNotificationEmail(modulcbo, iID, cbonotificationemail, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repocbonotificationemail.DeleteNotificationEmail(modulcbo, iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;

                        //priority by henggar
                        case "PriorityProduct":
                            var priority = json_serializer.Deserialize<PriorityProductModel>(rec.Data);
                            var repopriorityproduct = new PriorityProductModelRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repopriorityproduct.AddPriorityProduct(priority, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repopriorityproduct.UpdatePriorityProduct(iID, priority, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repopriorityproduct.DeletePriorityProduct(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        //end henggar

                        //murex by dani
                        case "MurexMapping":
                            var murex = json_serializer.Deserialize<MurexMappingModel>(rec.Data);
                            var repomurex = new MurexMappingRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repomurex.AddMurex(murex, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repomurex.UpdateMurex(iID, murex, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repomurex.DeleteMurex(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        //murex by dani end                     

                        case "ProductTypeThresholdMapping":
                            //var producttypethreshold = json_serializer.Deserialize<ProductTypeThresholdMappingModel>(rec.Data);
                            var repoproducttypethreshold = new ProductTypeThresholdModelRepository();
                            var thresholdgroup = json_serializer.Deserialize<ThresholdGroupModel>(rec.Data);

                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repoproducttypethreshold.AddProductTypeThresholdMapping(thresholdgroup, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoproducttypethreshold.UpdateProductThreshold(iID, thresholdgroup, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoproducttypethreshold.DeleteProductTypeThresholdMapping(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;

                        case "BranchBank":
                            var repoBranchBank = new BranchBankTesRepository();
                            var BranchBank = json_serializer.Deserialize<BranchBankTesModel>(rec.Data);

                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repoBranchBank.addBranchBank(BranchBank, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoBranchBank.UpdateBranchBank(iID, BranchBank, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoBranchBank.DeleteBranchBank(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "RejectCode":
                            var rejectCode = json_serializer.Deserialize<RejectCodeModel>(rec.Data);
                            var repoRejectCode = new RejectCodeRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repoRejectCode.AddRejectCode(rejectCode, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoRejectCode.UpdateRejectCode(iID, rejectCode, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoRejectCode.DeleteRejectCode(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "UserCategory":
                            var userCategory = json_serializer.Deserialize<UserCategoryModel>(rec.Data);
                            var repoUserCategory = new UserCategoryRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repoUserCategory.AddUserCategory(userCategory, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoUserCategory.UpdateUserCategory(iID, userCategory, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoUserCategory.DeleteUserCategory(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        //add aridya
                        case "BeneficiaryCountry":
                            var beneficiaryCountry = json_serializer.Deserialize<BeneficiaryCountryModel>(rec.Data);
                            var repoBeneficiaryCountry = new BeneficiaryCountryRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repoBeneficiaryCountry.AddBeneficiaryCountry(beneficiaryCountry, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoBeneficiaryCountry.UpdateBeneficiaryCountry(iID, beneficiaryCountry, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoBeneficiaryCountry.DeleteBeneficiaryCountry(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "TransactionRelationship":
                            var transactionRelationship = json_serializer.Deserialize<TransactionRelationshipModel>(rec.Data);
                            var repoTransactionRelationship = new TransactionRelationshipRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repoTransactionRelationship.AddTransactionRelationship(transactionRelationship, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoTransactionRelationship.UpdateTransactionRelationship(iID, transactionRelationship, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoTransactionRelationship.DeleteTransactionRelationship(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        //end add aridya

                        //add azam
                        case "beneficiarybusinessType":
                        case "BeneficiaryBusinessType":
                            var beneficiaryBusinessType = json_serializer.Deserialize<BeneficiaryBusinessTypeModel>(rec.Data);
                            var repoBeneficiaryBusinessType = new BeneficiaryRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repoBeneficiaryBusinessType.AddBeneficiary(beneficiaryBusinessType, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoBeneficiaryBusinessType.UpdateBeneficiary(iID, beneficiaryBusinessType, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoBeneficiaryBusinessType.DeleteBeneficiary(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        //add azam 03/10/2016
                        case "ForceComplete":
                            var ForceComplete = json_serializer.Deserialize<ForceCompleteModel>(rec.Data);
                            var repoForceComplete = new ForceCompleteRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repoForceComplete.AddForceComplete(ForceComplete, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoForceComplete.UpdateForceComplete(iID, ForceComplete, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoForceComplete.DeleteForceComplete(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "TransactionLimit":
                        case "TransactionLimitProduct":
                        case "transactionlimit":
                            var transactionLimit = json_serializer.Deserialize<TransactionLimitModel>(rec.Data);
                            var repoTransactionLimit = new TransactionLimitRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repoTransactionLimit.AddTransactionLimit(transactionLimit, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoTransactionLimit.UpdateTransactionLimit(iID, transactionLimit, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoTransactionLimit.DeleteTransactionLimit(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "City":
                            var city = json_serializer.Deserialize<CityModel>(rec.Data);
                            var repoCity = new CityRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repoCity.AddCity(city, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoCity.UpdateCity(iID, ref city, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoCity.DeleteCity(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "ColumnComparison":
                            var colum = json_serializer.Deserialize<ColumnComparisonModel>(rec.Data);
                            var repoColum = new ColumnComparisonRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repoColum.AddColumnComparison(colum, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoColum.UpdateColumnComparison(iID, ref colum, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoColum.DeleteColumnComparison(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        //end azam
                        //start David
                        case "POAEmailProduct":
                            var poaemailproduct = json_serializer.Deserialize<POAEmailModel>(rec.Data);
                            var repoPOAEmailProduct = new POAEmailProductRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repoPOAEmailProduct.AddPOAEmailProduct(poaemailproduct, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoPOAEmailProduct.UpdatePOAEmailProduct(iID, ref poaemailproduct, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repoPOAEmailProduct.DeletePOAEmailProduct(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        case "Recipient":
                            var recipient = json_serializer.Deserialize<RecipientModel>(rec.Data);
                            var reporecipient = new RepicientRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!reporecipient.AddRecipient(recipient, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!reporecipient.UpdateRecipient(iID, recipient, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!reporecipient.DeleteRecipient(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;

                        case "TBODocsSLA":
                            var tbodocssla = json_serializer.Deserialize<TBODocsSLAModel>(rec.Data);
                            var repotbodocssla = new TBODocsSLARepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repotbodocssla.AddTBODocsSLA(tbodocssla, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repotbodocssla.UpdateTBODocsSLA(iID, tbodocssla, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repotbodocssla.DeleteTBODocsSLA(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;

                        case "FDTransfer":
                            var fdtransfer = json_serializer.Deserialize<FDTransferModel>(rec.Data);
                            var repofdtransfer = new FDTransferRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repofdtransfer.AddFDTransfer(fdtransfer, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repofdtransfer.UpdateFDTransfer(iID, fdtransfer, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repofdtransfer.DeleteFDTransfer(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;

                        case "TypeOfTransaction":
                            var typeoftransaction = json_serializer.Deserialize<TypeOfTransactionModel>(rec.Data);
                            var repotypeoftransaction = new TypeOfTransactionRepository();
                            switch (rec.ActionType)
                            {
                                case "ADD":
                                    if (!repotypeoftransaction.AddTypeOfTransaction(typeoftransaction, ref message)) rec.Data = message;
                                    break;
                                case "UPDATE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repotypeoftransaction.UpdateTypeOfTransaction(iID, typeoftransaction, ref message)) rec.Data = message;
                                    break;
                                case "DELETE":
                                    int.TryParse(rec.KeyID, out iID);
                                    if (!repotypeoftransaction.DeleteTypeOfTransaction(iID, ref message)) rec.Data = message;
                                    break;
                            }
                            break;
                        //end david
                        case "CustomerPOAEmail":
                            string scustomerpoaEmail = "";
                            try
                            {
                                scustomerpoaEmail = rec.Data;
                                var POAEmail = json_serializer.Deserialize<JSONSentModel>(scustomerpoaEmail);
                                var POAEmailUpdate = json_serializer.Deserialize<JSONSentUpdateModel>(scustomerpoaEmail);
                                var POAEmailDelete = json_serializer.Deserialize<JSONSentDeleteModel>(scustomerpoaEmail);
                                //var Email = 
                                var repocustomerpoaEmail = new CustomerPOAEmailRepository();
                                switch (rec.ActionType)
                                {
                                    case "ADD":
                                        if (!repocustomerpoaEmail.AddCustomerPOAEmailWF(POAEmail.POAEmail, POAEmail.Email, ref message)) rec.Data = message;
                                        break;
                                    case "UPDATE":
                                        if (!repocustomerpoaEmail.UpdateCustomerPOAEmailWF(POAEmailUpdate.OldEmail, POAEmailUpdate.Email, POAEmailUpdate.POAEmail, ref message)) rec.Data = message;
                                        break;
                                    case "DELETE":
                                        if (!repocustomerpoaEmail.DeleteCustomerPOAEmailWF(POAEmailDelete.CIF, POAEmailDelete.Email, POAEmailDelete.POAEmail, ref message)) rec.Data = message;
                                        break;
                                }
                            }
                            catch (Exception ex)
                            {
                                message = ex.Message;
                                FileLog filelog = new FileLog(@"C:\WINDOWS\TEMP\DBSLog2.txt");
                                filelog.Open();
                                filelog.WriteLine("WorkflowEntity Approval");
                                filelog.WriteLine(ex.Message);
                                if (rec != null) filelog.WriteLine(rec.Data);
                                filelog.WriteLine("SDATA");
                                filelog.WriteLine(scustomerpoaEmail);
                                filelog.Close();
                            }
                            break;
                    }
                }
                Delete(rec.ID, ref message);
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
                FileLog filelog = new FileLog(@"C:\WINDOWS\TEMP\DBSLog.txt");
                filelog.Open();
                filelog.WriteLine("WorkflowEntity Approval");
                filelog.WriteLine(ex.Message);
                if (rec != null) filelog.WriteLine(rec.Data);
                filelog.Close();
            }
            return isSuccess;
        }
        public bool GetByID(string id, ref WorkflowEntityModel rec, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Guid gid = Guid.Empty;
                bool isguid = Guid.TryParse(id, out gid);

                if (isguid)
                {
                    rec = (from a in context.WorkflowEntities
                           join b in context.V_NintexWorkflowTasks on a.WorkflowID.Value equals b.WorkflowInstanceID
                           where a.IsDeleted.Equals(false) && a.WorkflowID.Value == gid
                           select new WorkflowEntityModel
                           {
                               ID = a.WorkflowEntityID,
                               EntityName = a.EntityName,
                               TaskName = a.TaskName,
                               ActionType = a.ActionType,
                               WorkflowID = (Guid)a.WorkflowID == null ? Guid.Empty : (Guid)a.WorkflowID,
                               Data = a.Data,
                               KeyID = a.KeyID,
                               SPTaskListID = (Guid)b.SPTaskListID,
                               SPTaskListItemID = (int)b.SPTaskListItemID,
                               Initiator = b.WorkflowInitiator,
                               LastModifiedDate = a.CreateDate,
                               LastModifiedBy = a.CreateBy
                           }).SingleOrDefault();
                }
                else
                {
                    long l = 0;
                    long.TryParse(id, out l);
                    rec = (from a in context.WorkflowEntities
                           join b in context.V_NintexWorkflowTasks on a.WorkflowID.Value equals b.WorkflowInstanceID
                           where a.IsDeleted.Equals(false) && a.WorkflowEntityID == l
                           select new WorkflowEntityModel
                           {
                               ID = a.WorkflowEntityID,
                               EntityName = a.EntityName,
                               TaskName = a.TaskName,
                               ActionType = a.ActionType,
                               WorkflowID = (Guid)a.WorkflowID == null ? Guid.Empty : (Guid)a.WorkflowID,
                               Data = a.Data,
                               KeyID = a.KeyID,
                               SPTaskListID = (Guid)b.SPTaskListID,
                               SPTaskListItemID = (int)b.SPTaskListItemID,
                               Initiator = b.WorkflowInitiator,
                               LastModifiedDate = a.CreateDate,
                               LastModifiedBy = a.CreateBy
                           }).SingleOrDefault();
                }



                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool Get(ref IList<WorkflowEntityModel> data, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {

                    data = GetAllJoin(context).Skip(skip).Take(limit).ToList();

                    //data = (from a in context.WorkflowEntities
                    //        join b in context.V_NintexWorkflowTasks on a.WorkflowID.Value equals b.WorkflowInstanceID
                    //                where a.IsDeleted.Equals(false)
                    //         orderby new { a.EntityName, a.TaskName }
                    //         select new WorkflowEntityModel
                    //                {
                    //                    ID = a.WorkflowEntityID,
                    //                    EntityName = a.EntityName,
                    //                    TaskName = a.TaskName,
                    //                    ActionType = a.ActionType,
                    //                    SPTaskListID = (Guid)b.SPTaskListID,
                    //                    SPTaskListItemID = (int)b.SPTaskListItemID,
                    //                    Initiator = b.WorkflowInitiator,
                    //                    WorkflowID = (Guid)a.WorkflowID == null ? Guid.Empty : (Guid)a.WorkflowID,
                    //                    LastModifiedDate = a.CreateDate,
                    //                    LastModifiedBy = a.CreateBy
                    //                }).Skip(skip).Take(limit).ToList();

                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool Get(ref IList<WorkflowEntityModel> data, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    data = GetAllJoin(context).ToList();

                    //data = (from a in context.WorkflowEntities
                    //        join b in context.V_NintexWorkflowTasks on a.WorkflowID.Value equals b.WorkflowInstanceID
                    //                where a.IsDeleted.Equals(false)
                    //                orderby new { a.EntityName, a.TaskName }
                    //         select new WorkflowEntityModel
                    //                {
                    //                    ID = a.WorkflowEntityID,
                    //                    EntityName = a.EntityName,
                    //                    TaskName = a.TaskName,
                    //                    ActionType = a.ActionType,
                    //                    SPTaskListID = (Guid)b.SPTaskListID,
                    //                    SPTaskListItemID = (int)b.SPTaskListItemID,
                    //                    Initiator = b.WorkflowInitiator,
                    //                    WorkflowID = (Guid)a.WorkflowID == null ? Guid.Empty : (Guid)a.WorkflowID,
                    //                    LastModifiedBy = a.CreateBy,
                    //                    LastModifiedDate = a.CreateDate
                    //                }).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool Get(int page, int size, IList<WorkflowEntityFilter> filters, string sortColumn, string sortOrder, ref WorkflowEntityGrid roleGrid, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                WorkflowEntityModel filter = new WorkflowEntityModel();
                if (filters != null)
                {
                    filter.EntityName = (string)filters.Where(a => a.Field.Equals("EntityName")).Select(a => a.Value).SingleOrDefault();
                    filter.TaskName = (string)filters.Where(a => a.Field.Equals("TaskName")).Select(a => a.Value).SingleOrDefault();
                    filter.ActionType = (string)filters.Where(a => a.Field.Equals("Action")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
                    filter.FilterValue = (string)filters.Where(a => a.Field.Equals("FilterValue")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    /*
                    var src = (
                              from a in context.WorkflowEntities
                              //where a.IsDeleted.Equals(false)
                              select new WorkflowEntity()
                              {
                                  ActionType = a.ActionType,
                                  CreateBy = a.CreateBy,
                                  CreateDate = a.CreateDate,
                                  Data = a.Data,
                                  EntityName = a.EntityName,
                                  IsDeleted = a.IsDeleted,
                                  KeyID = a.KeyID,
                                  TaskName = a.TaskName,
                                  UpdateBy = a.UpdateBy,
                                  UpdateDate = a.UpdateDate,
                                  WorkflowEntityID = a.WorkflowEntityID,
                                  WorkflowID = a.WorkflowID
                              }
                              //select a
                                  );
                    var data = (
                            //from a in GetAllJoin(context)
                                from a in src
                                //from a in context.WorkflowEntities
                                join b in context.V_NintexWorkflowTasks on a.WorkflowID.Value equals b.WorkflowInstanceID
                                let isEntityName = !string.IsNullOrEmpty(filter.EntityName)
                                let isTaskName = !string.IsNullOrEmpty(filter.TaskName)
                                let isActionType = !string.IsNullOrEmpty(filter.ActionType)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) &&
                                //where
                                    (isEntityName ? a.EntityName.Contains(filter.EntityName) : true) &&
                                    (isTaskName ? a.TaskName.Contains(filter.TaskName) : true) &&
                                    (isActionType ? a.ActionType.Contains(filter.ActionType) : true) &&
                                    //(isCreateBy ? (a.LastModifiedBy == null ? a.LastModifiedBy.Contains(filter.LastModifiedBy) : a.LastModifiedBy.Contains(filter.LastModifiedBy)) : true) &&
                                    //(isCreateDate ? ((a.LastModifiedDate == null ? a.LastModifiedDate : a.LastModifiedDate) > filter.LastModifiedDate.Value && ((a.LastModifiedDate == null ? a.LastModifiedDate : a.LastModifiedDate.Value) < maxDate)) : true)
                                (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new WorkflowEntityModel
                                {
                                    ID = a.WorkflowEntityID,
                                    //ID = a.ID,
                                    EntityName = a.EntityName,
                                    TaskName = a.TaskName,
                                    ActionType = a.ActionType,
                                    Data = a.Data,
                                    SPTaskListID = (Guid)b.SPTaskListID,
                                    SPTaskListItemID = (int)b.SPTaskListItemID,
                                    //SPTaskListID = a.SPTaskListID,
                                    //SPTaskListItemID = a.SPTaskListItemID,

                                    Initiator = b.WorkflowInitiator,
                                    WorkflowID = (Guid)a.WorkflowID == null ? Guid.Empty : (Guid)a.WorkflowID,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                    //LastModifiedBy = a.LastModifiedBy,
                                    //LastModifiedDate = a.LastModifiedDate
                                });
                    */
                    var data = from a in GetAllJoin(context, filter.EntityName, filter.FilterValue)
                               let isEntityName = !string.IsNullOrEmpty(filter.EntityName)
                               let isTaskName = !string.IsNullOrEmpty(filter.TaskName)
                               let isActionType = !string.IsNullOrEmpty(filter.ActionType)
                               let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                               let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                               where
                                    (isEntityName ? a.EntityName.Contains(filter.EntityName) : true) &&
                                    (isTaskName ? a.TaskName.Contains(filter.TaskName) : true) &&
                                    (isActionType ? a.ActionType.Contains(filter.ActionType) : true) &&
                                    (isCreateBy ? (a.LastModifiedBy == null ? a.LastModifiedBy.Contains(filter.LastModifiedBy) : a.LastModifiedBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.LastModifiedDate == null ? a.LastModifiedDate : a.LastModifiedDate) > filter.LastModifiedDate.Value && ((a.LastModifiedDate == null ? a.LastModifiedDate : a.LastModifiedDate.Value) < maxDate)) : true)
                               select a;
                    roleGrid.Page = page;
                    roleGrid.Size = size;
                    roleGrid.Total = data.Count();
                    roleGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    roleGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new WorkflowEntityRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            EntityName = a.EntityName,
                            TaskName = a.TaskName,
                            ActionType = a.ActionType,
                            Data = a.Data,
                            SPTaskListID = a.SPTaskListID,
                            SPTaskListItemID = a.SPTaskListItemID,
                            Initiator = a.Initiator,
                            WorkflowID = (Guid)a.WorkflowID == null ? Guid.Empty : (Guid)a.WorkflowID,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetSP(int page, int size, IList<WorkflowEntityFilter> filters, string sortColumn, string sortOrder, ref WorkflowEntityGrid roleGrid, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                WorkflowEntityModel filter = new WorkflowEntityModel();
                if (filters != null)
                {
                    filter.EntityName = (string)filters.Where(a => a.Field.Equals("EntityName")).Select(a => a.Value).SingleOrDefault();
                    filter.TaskName = (string)filters.Where(a => a.Field.Equals("TaskName")).Select(a => a.Value).SingleOrDefault();
                    filter.ActionType = (string)filters.Where(a => a.Field.Equals("ActionType")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
                    filter.FilterValue = (string)filters.Where(a => a.Field.Equals("FilterValue")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = from a in GetSPAllJoin(context, filter.EntityName, filter.FilterValue, filter.TaskName, filter.ActionType, filter.LastModifiedBy, filter.LastModifiedDate)
                               select a;
                    roleGrid.Page = page;
                    roleGrid.Size = size;
                    roleGrid.Total = data.Count();
                    roleGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    roleGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new WorkflowEntityRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            EntityName = a.EntityName,
                            TaskName = a.TaskName,
                            ActionType = a.ActionType,
                            Data = a.Data,
                            SPTaskListID = a.SPTaskListID,
                            SPTaskListItemID = a.SPTaskListItemID,
                            Initiator = a.Initiator,
                            WorkflowID = (Guid)a.WorkflowID == null ? Guid.Empty : (Guid)a.WorkflowID,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetSPHome(int page, int size, IList<WorkflowEntityFilter> filters, string sortColumn, string sortOrder, ref WorkflowEntityGrid roleGrid, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                WorkflowEntityModel filter = new WorkflowEntityModel();
                if (filters != null)
                {
                    filter.EntityName = (string)filters.Where(a => a.Field.Equals("EntityName")).Select(a => a.Value).SingleOrDefault();
                    filter.TaskName = (string)filters.Where(a => a.Field.Equals("TaskName")).Select(a => a.Value).SingleOrDefault();
                    //filter.ActionType = (string)filters.Where(a => a.Field.Equals("Action")).Select(a => a.Value).SingleOrDefault();
                    filter.ActionType = (string)filters.Where(a => a.Field.Equals("ActionType")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
                    //filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("FilterModifiedBy")).Select(a => a.Value).SingleOrDefault();
                    filter.FilterValue = (string)filters.Where(a => a.Field.Equals("FilterValue")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    //change by fandi
                    //var data = from a in GetSPAllJoinHome(context, filter.EntityName, filter.FilterValue)
                    var data = from a in GetSPAllJoinHome(context, filter.TaskName, filter.ActionType, filter.LastModifiedBy, filter.LastModifiedDate)
                               //end
                               select a;
                    roleGrid.Page = page;
                    roleGrid.Size = size;
                    roleGrid.Total = data.Count();
                    roleGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    roleGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new WorkflowEntityRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            EntityName = a.EntityName,
                            TaskName = a.TaskName,
                            ActionType = a.ActionType,
                            Data = a.Data,
                            SPTaskListID = a.SPTaskListID,
                            SPTaskListItemID = a.SPTaskListItemID,
                            Initiator = a.Initiator,
                            WorkflowID = (Guid)a.WorkflowID == null ? Guid.Empty : (Guid)a.WorkflowID,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate,
                            TaskType = a.TaskType,
                            WorkflowTaskType = a.TaskType
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool Get(WorkflowEntityFilter filter, ref IList<WorkflowEntityModel> data, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool Get(string key, int limit, ref IList<WorkflowEntityModel> data, ref string message)
        {
            bool isSuccess = false;

            try
            {
                data = (from a in GetAllJoin(context)
                        //from a in context.WorkflowEntities
                        //join b in context.V_NintexWorkflowTasks on a.WorkflowID.Value equals b.WorkflowInstanceID
                        //where a.IsDeleted.Equals(false) &&
                        where a.EntityName.Contains(key) || a.TaskName.Contains(key)
                        orderby new { a.EntityName, a.TaskName }
                        select new WorkflowEntityModel
                        {
                            EntityName = a.EntityName,
                            TaskName = a.TaskName
                        }).Take(limit).ToList();



                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool Add(WorkflowEntityModel rec, ref string message)
        {
            bool isSuccess = false;

            try
            {
                bool allow = true;
                if (rec.ActionType.Equals("ADD"))
                    allow = true;
                else
                    allow = (!context.WorkflowEntities.Where(a => a.EntityName.Equals(rec.EntityName) && a.KeyID.Equals(rec.KeyID) && a.IsDeleted.Equals(false)).Any());

                if (allow)
                {
                    Entity.WorkflowEntity item = new Entity.WorkflowEntity()
                    {
                        EntityName = rec.EntityName,
                        TaskName = rec.TaskName,
                        ActionType = rec.ActionType,
                        WorkflowID = (Guid)rec.WorkflowID == null ? Guid.Empty : (Guid)rec.WorkflowID,
                        Data = rec.Data,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    };

                    context.WorkflowEntities.Add(item);
                    context.SaveChanges();
                    //message = item.WorkflowEntityID.ToString();

                    //DBS.WebAPI.Helpers.WorkflowEntityProcess p = new DBS.WebAPI.Helpers.WorkflowEntityProcess(@"http://mysweetlife:90/", "Admin1", "pass@word1");
                    //int retVal = p.PostingWorkflow(item.EntityName, item.ActionType, item.WorkflowEntityID.ToString()).Result;
                    /*

                    System.Net.Http.HttpClientHandler httpClientHandler = new HttpClientHandler();
                    httpClientHandler.UseDefaultCredentials = true;
                    HttpClient client = new HttpClient(httpClientHandler);
                    client.DefaultRequestHeaders.Add("Accept", "application/atom+xml");
                    client.DefaultRequestHeaders.Add("ContentType", "application/atom+xml;type=entry");
                    string restUrl = "http://mysweetlife:90/_api/lists/getbytitle('WorkflowEntity')/items";
                    client.
                    */

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Workflow for Entity {0} for action {1} is already exist.", rec.EntityName, rec.ActionType);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool Update(long id, WorkflowEntityModel rec, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.WorkflowEntity data = context.WorkflowEntities.Where(a => a.WorkflowEntityID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    if (context.WorkflowEntities.Where(a => a.WorkflowEntityID != rec.ID && a.TaskName.Equals(rec.TaskName) && a.IsDeleted.Equals(false)).Count() >= 1)
                    {
                        message = string.Format("Role Draft Name {0} is exist.", rec.TaskName);
                    }
                    else
                    {
                        data.WorkflowEntityID = rec.ID;
                        data.EntityName = rec.EntityName;
                        data.TaskName = rec.TaskName;
                        data.ActionType = rec.ActionType;
                        data.WorkflowID = (Guid)rec.WorkflowID == null ? Guid.Empty : (Guid)rec.WorkflowID;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        context.SaveChanges();

                        isSuccess = true;
                    }
                }
                else
                {
                    message = string.Format("Role data for Role ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool Delete(long id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.WorkflowEntity data = context.WorkflowEntities.Where(a => a.WorkflowEntityID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Workflow Entity data for Role ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}