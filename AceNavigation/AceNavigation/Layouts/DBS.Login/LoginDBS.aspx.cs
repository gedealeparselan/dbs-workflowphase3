﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Web;
using Microsoft.SharePoint.IdentityModel;
using System.Web.Security;
using System.Web.UI;
using AceNavigation.Repository;
using AceNavigation.Model;
using System.Collections.Generic;
using System.Linq;

namespace AceNavigation.Layouts.DBS.Login
{
    public partial class LoginDBS : Page
    {
        bool IsCCU;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Request.Cookies["DBSApi"] != null)
                {
                    HttpCookie dbsCookie = new HttpCookie("DBSApi");
                    dbsCookie.Expires = DateTime.Now.AddDays(-1d);
                    Response.Cookies.Add(dbsCookie);
                }

                if (Request.Cookies["SPUser"] != null)
                {
                    HttpCookie dbsCookieUser = new HttpCookie("SPUser");
                    dbsCookieUser.Expires = DateTime.Now.AddDays(-1d);
                    Response.Cookies.Add(dbsCookieUser);
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string userName = txtUsername.Text.Trim();
            string sUrl = HttpContext.Current.Request.Url.AbsolutePath;
            try
            {
                if (CekNilai())
                {
                    bool status = SPClaimsUtility.AuthenticateFormsUser(Context.Request.UrlReferrer, txtUsername.Text.Trim(), txtPassword.Text);
                    if (status)
                    {
                        if (CheckSharePointUser("i:0#.f|dbsmembership|" + userName)) //Untuk memastikan tidak ada SharePoint user yang copot
                        {
                            Insert("i:0#.f|dbsmembership|" + userName, "Login", sUrl);
                            if (Context.Request.QueryString.Keys.Count > 1)
                            {
                                if (IsCCU)
                                {
                                    Response.Redirect("/home-ccu", false);
                                }
                                else
                                {
                                    //Response.Redirect(Context.Request.QueryString["ReturnUrl"].ToString(), false);
                                    //Response.Redirect(Context.Request.QueryString["Source"].ToString(), false);
                                    Response.Redirect("/Home", false);
                                }
                            }
                            else
                            {
                                if (IsCCU)
                                {
                                    Response.Redirect("/home-ccu", false);
                                }
                                else
                                {
                                    Response.Redirect("/Home", false);
                                }
                            }
                        }
                        else
                        {
                            Insert(userName, "Unauthorized Access", sUrl);
                            lblMessage.Text = "Unauthorized Access.";
                        }
                    }
                    else
                    {
                        Insert(userName, "Invalid Login", sUrl);
                        lblMessage.Text = "The server could not sign you in. Make sure your user name and password are correct, and then try again.";
                    }

                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Could"))
                {
                    Insert(userName, "Unauthorized Access", sUrl);
                    lblMessage.Text = "Username not found.";
                }
                else if (ex.Message.Contains("Object"))
                {
                    Insert(userName, "Connection Closed", sUrl);
                    lblMessage.Text = "Something went wrong.";
                }
                else
                {
                    Insert(userName, "Invalid Login", sUrl);
                    lblMessage.Text = ex.Message;
                }
            }
        }

        public bool CekNilai()
        {
            if (txtUsername.Text == "")
            {
                lblMessage.Text = "Username required.";
                return false;
            }

            if (txtPassword.Text == "")
            {
                lblMessage.Text = "Password required.";
                return false;
            }

            if (txtPassword.Text == "" && txtUsername.Text == "")
            {
                lblMessage.Text = "Username and Password required.";
                return false;
            }

            if (txtUsername.Text.Contains(" "))
            {
                lblMessage.Text = "Username can't contain spaces.";
                return false;
            }

            return true;
        }

        public void Insert(string userName, string logType, string remark)
        {
            try
            {
                SystemLogRepo repo = new SystemLogRepo();
                repo.InsertSystemLog(userName, logType, remark);
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        public bool IsHasPermission()
        {
            bool isAuth = false;
            try
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    using (SPSite site = new SPSite(SPContext.Current.Site.Url))
                    {
                        using (SPWeb web = site.OpenWeb())
                        {
                            SPBasePermissions permissionToCheck = SPBasePermissions.ViewPages;
                            string LoginName = "i:0#.f|DBSMembership|" + txtUsername.Text.Trim();
                            isAuth = web.DoesUserHavePermissions(LoginName, permissionToCheck);
                        }
                    }
                });

                return isAuth;
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
            return isAuth;
        }

        public bool CheckSharePointUser(string UserName)
        {
            bool IsValid = true;

            try
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    using (SPSite site = new SPSite(SPContext.Current.Site.Url))
                    {
                        using (SPWeb web = site.OpenWeb())
                        {
                            RoleRepo roleRepo = new RoleRepo();
                            IList<Model.RoleModel> roleModel = roleRepo.GetRoleByUsername(UserName);
                            if (roleModel != null && roleModel.Count > 0)
                            {
                                var a = roleModel.Where(rm => rm.RoleName.ToLower().Contains("dbs ccu")).ToList();
                                if (a != null && a.Count > 0)
                                {
                                    IsCCU = true;
                                }
                                else
                                {
                                    IsCCU = false;
                                }

                                SPUser user = web.EnsureUser(UserName);
                                try
                                {
                                    SPGroup group = user.Groups.GetByName("SharePoint Application Members");

                                    //Handle SharePoint user yang username-nya mengandung spasi
                                    if (user.LoginName.Contains(" "))
                                    {
                                        web.AllowUnsafeUpdates = true;
                                        group.AddUser(user);
                                        web.Update();
                                        web.AllowUnsafeUpdates = false;
                                    }
                                }
                                catch (Exception) //Jika user tidak ada pada group "SharePoint Application Members"
                                {
                                    web.AllowUnsafeUpdates = true;

                                    foreach (var role in roleModel)
                                    {
                                        try
                                        {
                                            SPGroup userGroup = web.Groups[role.RoleName];
                                            if (userGroup != null)
                                            {
                                                userGroup.AddUser(user);
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            continue;
                                        }
                                    }

                                    SPGroup group = web.Groups["SharePoint Application Members"];
                                    group.AddUser(user);

                                    web.Update();
                                    web.AllowUnsafeUpdates = false;
                                }
                            }
                            else
                            {
                                IsValid = false;
                            }
                        }
                    }
                });
            }
            catch (Exception)
            {
                IsValid = false;
            }

            return IsValid;
        }

    }
}
