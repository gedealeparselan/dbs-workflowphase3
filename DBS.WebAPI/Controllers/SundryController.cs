﻿using DBS.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class SundryController : ApiController
    {
        private string message = string.Empty;
        private readonly ISundryRepository repository = null;
        public SundryController()
        {
            this.repository = new SundryRepository();
        }
        public SundryController(ISundryRepository repository)
        {
            this.repository = repository;
        }

        [ResponseType(typeof(IList<SundryModel>))]
        public HttpResponseMessage GetAll()
        {
            IList<SundryModel> result = new List<SundryModel>();
            if (!this.repository.GetSundry(ref result, 10, 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<SundryModel>>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(IList<SundryModel>))]
        public HttpResponseMessage GetAll(int? limit, int? index)
        {
            IList<SundryModel> result = new List<SundryModel>();
            if (!repository.GetSundry(ref result, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<IList<SundryModel>>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(SundryGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<SundryFilter> filters)
        {
            SundryGrid result = new SundryGrid();
            if (!repository.GetSundry(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "UnitName" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref result, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<SundryGrid>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(IList<SundryModel>))]
        [HttpGet]
        [Route("api/Sundry/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<SundryModel> result = new List<SundryModel>();

            if (!repository.GetSundry(query, limit.HasValue ? limit.Value : 10, ref result, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<SundryModel>>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(SundryModel))]
        [Route("api/Sundry/{id}")]
        [HttpGet]
        public HttpResponseMessage GetById(int Id)
        {
            SundryModel result = new SundryModel();

            if (!repository.GetSundryByID(Id, ref result, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<SundryModel>(HttpStatusCode.OK, result);
            }
        }

        [HttpPost]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Post(SundryModel sundry)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddSundry(sundry, ref message))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse<string>(HttpStatusCode.Created, "New Sundry data has been created.");
                }
            }
        }

        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/Sundry/{id}")]
        public HttpResponseMessage Put(int id, SundryModel sundry)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateSundry(id, sundry, ref message))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Sundry data has been updated.");
                }
            }
        }

        [HttpDelete]
        [Route("api/Sundry/{id}")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteSundry(id, ref message))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse<string>(HttpStatusCode.OK, "Sundry data has been deleted.");
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }

    }
}