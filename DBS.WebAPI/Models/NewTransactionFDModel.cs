﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Transactions;
using DBS.Entity;
using DBS.WebAPI.Helpers;
using System.IO;
using System.Net;
namespace DBS.WebAPI.Models
{
    #region Property
    public class TransactionFDDetailModel
    {
        public Guid? WorkflowInstanceID { get; set; }
        public long ID { get; set; }
        public long TransactionID { get; set; }
        public string ApplicationID { get; set; }
        public bool IsTopUrgent { get; set; }
        public bool IsTopUrgentChain { get; set; }
        public CustomerModel Customer { get; set; }
        public bool IsNewCustomer { get; set; }
        public ProductModel Product { get; set; }
        public CurrencyModel Currency { get; set; }
        public decimal Amount { get; set; }
        public decimal Rate { get; set; }
        public decimal AmountUSD { get; set; }
        public DateTime ApplicationDate { get; set; }
        public bool IsDraft { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public IList<TransactionDocumentModel> Documents { get; set; }
        public string DetailCompliance { get; set; }
        public string CreateBy { get; set; }
        public TransactionTypeParameterModel TransactionType { get; set; }
        public string FDAccNumber { get; set; }
        public string FDBankName { get; set; }
        public string CreditAccNumber { get; set; }
        public string DebitAccNumber { get; set; }
        public decimal? InterestRate { get; set; }
        public string Tenor { get; set; }
        public DateTime? ValueDate { get; set; }
        public DateTime? MaturityDate { get; set; }
        public ParsysModel Remarks { get; set; }
        public string AttachmentRemarks { get; set; }
        public bool? DocsComplete { get; set; }
        public decimal? AllInRate { get; set; }
        #region basri
        public long? ApproverID { get; set; }
        public ChannelModel Channel { get; set; }
        public bool IsSignatureVerified { get; set; }
        public bool IsDormantAccount { get; set; }
        public bool IsFreezeAccount { get; set; }
        public bool IsCallbackRequired { get; set; }
        public bool IsLOIAvailable { get; set; }
        public bool IsDocumentComplete { get; set; }
        public string TransactionRemarks { get; set; }
        public int? IsTopUrgentValue { get; set; }
        public string BankNameAccNumber { get; set; }
        #endregion
        public bool IsBringupTask { get; set; }
        public int SourceID { get; set; }
        public SourceModel Source { get; set; }
        public string FTPRate { get; set; }
    }
    public class TransactionFDDraftModel
    {
        public Guid? WorkflowInstanceID { get; set; }
        public long ID { get; set; }
        public long TransactionID { get; set; }
        public string ApplicationID { get; set; }
        public bool? IsTopUrgent { get; set; }
        public bool? IsTopUrgentChain { get; set; }
        public bool? IsNormal { get; set; }
        public CustomerModel Customer { get; set; }
        public bool? IsNewCustomer { get; set; }
        public ProductModel Product { get; set; }
        public CurrencyModel Currency { get; set; }
        public decimal? Amount { get; set; }
        public decimal? Rate { get; set; }
        public decimal? AmountUSD { get; set; }
        public DateTime? ApplicationDate { get; set; }
        public bool? IsDraft { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public IList<TransactionDocumentDraftModel> Documents { get; set; }
        public string DetailCompliance { get; set; }
        public string CreateBy { get; set; }
        public TransactionTypeParameterModel TransactionType { get; set; }
        public string FDAccNumber { get; set; }
        public string FDBankName { get; set; }
        public string CreditAccNumber { get; set; }
        public string DebitAccNumber { get; set; }
        public decimal? InterestRate { get; set; }
        public string Tenor { get; set; }
        public DateTime? ValueDate { get; set; }
        public DateTime? MaturityDate { get; set; }
        public ParsysModel Remarks { get; set; }
        public string AttachmentRemarks { get; set; }
        public bool? DocsComplete { get; set; }
        public decimal? AllInRate { get; set; }
        public ChannelModel Channel { get; set; }
        public int SourceID { get; set; }
        public string FTPRate { get; set; }
    }
    public class TransactionDocumentDraftModel
    {
        public long ID { get; set; }
        public DocumentTypeModel Type { get; set; }
        public DocumentPurposeModel Purpose { get; set; }
        public string FileName { get; set; }
        public DocumentPathModel DocumentPath { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public bool? IsDormant { get; set; }
    }
    public class DocumentPathModel
    {
        public string name { get; set; }
        public string DocPath { get; set; }
        public string lastModified { get; set; }
        public DateTime lastModifiedDate { get; set; }
        public string type { get; set; }
    }
    public class IquoteModel{
        public int IQuoteID {set; get;}
        public string Tenor {set; get;}
        public decimal? IDR {set; get;}
        public decimal? USD {set; get;}
        public decimal? SGD {set; get;}
        public decimal? JPY {set; get;}
        public decimal? AUD {set; get;}
        public decimal? NZD {set; get;}
        public decimal? GBP {set; get;}
        public decimal? EUR {set; get;}
        public decimal? CHF {set; get;}
        public decimal? CNH {set; get;}
    }
    #endregion

    #region Interface
    public interface INewTransactionFDRepository : IDisposable
    {
        bool AddNewTransactionFD(TransactionFDDetailModel transaction, ref long transactionID, ref string ApplicationID, ref string transactionType, ref string message);
        bool GetTransactionDraftFDByID(long id, ref TransactionFDDraftModel transaction, ref string message);
        bool DeleteTransactionDraftByID(int id, ref string Message);
        bool GetIQuoteData(ref IList<IquoteModel> IQuote, ref string Message);
    }
    #endregion

    #region Repository
    public class NewTransactionFDRepository : INewTransactionFDRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);

            GC.Collect();
            GC.SuppressFinalize(this);
        }

        public bool AddNewTransactionFD(TransactionFDDetailModel transaction, ref long transactionID, ref string ApplicationID, ref string transactionType, ref string message)
        {
            bool IsSuccess = false;
            long TrIDInserted = 0;
            using (DBSEntities context_ts = new DBSEntities())
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    try
                    {
                        if (transaction.IsDraft)
                        {
                            if (transaction.ID != null && transaction.ID > 0)
                            {
                                Entity.TransactionDraft data = context.TransactionDrafts.Where(a => a.TransactionID.Equals(transaction.ID)).SingleOrDefault();
                                if (data != null)
                                {
                                    data.ApplicationDate = DateTime.UtcNow;
                                    data.IsTopUrgent = (transaction.IsTopUrgent == false && transaction.IsTopUrgentChain == false) ? 0 : (transaction.IsTopUrgent == true ? 1 : 2);
                                    data.IsNewCustomer = transaction.IsNewCustomer;
                                    data.CIF = transaction.Customer.CIF;
                                    data.ProductID = transaction.Product.ID;
                                    data.IsDraft = true;
                                    data.ApplicationID = string.Empty;
                                    data.StateID = (int)StateID.OnProgress;
                                    data.CreateDate = DateTime.UtcNow;
                                    data.CreateBy = currentUser.GetCurrentUser().LoginName;
                                    data.Remarks = transaction.Remarks.ID == 0 ? null : transaction.Remarks.ID.ToString();
                                    data.ValueDate = transaction.ValueDate;
                                    data.Amount = transaction.Amount;
                                    data.CurrencyID = transaction.Currency.ID;
                                    data.ChannelID = transaction.Channel.ID;
                                    data.TransactionTypeID = transaction.TransactionType.TransTypeID;
                                    data.FDAccNumber = transaction.FDAccNumber;
                                    data.CreditAccNumber = transaction.CreditAccNumber;
                                    data.DebitAccNumber = transaction.DebitAccNumber;
                                    data.InterestRate = transaction.InterestRate;
                                    data.Tenor = transaction.Tenor;
                                    data.MaturityDate = transaction.MaturityDate;
                                    data.FDBankName = transaction.FDBankName;
                                    data.AttachmentRemarks = transaction.AttachmentRemarks;
                                    if (transaction.Source != null)
                                    {
                                        if (transaction.Source.Name != null)
                                        {
                                            data.SourceID = transaction.Source.ID;
                                        }
                                    }
									data.SourceID = transaction.Source.ID;
									data.DocsComplete = transaction.DocsComplete;
                                    //data.AllInRate = transaction.AllInRate;
                                    data.FTPRate = transaction.FTPRate;
                                    //data.SourceID = transaction.Source.ID;
                                    context.SaveChanges();
                                    #region Save Document
                                    var existingDocs = context.TransactionDocumentDrafts.Where(a => a.TransactionID.Equals(transaction.ID)).ToList();
                                    if (existingDocs != null)
                                    {
                                        if (existingDocs.Count() > 0)
                                        {
                                            foreach (var item in existingDocs)
                                            {
                                                var deleteDoc = context.TransactionDocumentDrafts.Where(a => a.TransactionDocumentID.Equals(item.TransactionDocumentID)).SingleOrDefault();
                                                deleteDoc.IsDeleted = true;
                                                context.SaveChanges();
                                            }
                                        }
                                    }
                                    if (transaction.Documents != null)
                                    {
                                        if (transaction.Documents.Count > 0)
                                        {
                                            foreach (var item in transaction.Documents)
                                            {
                                                Entity.TransactionDocumentDraft document = new Entity.TransactionDocumentDraft()
                                                {
                                                    TransactionID = data.TransactionID,
                                                    DocTypeID = item.Type.ID,
                                                    PurposeID = item.Purpose.ID,
                                                    Filename = item.FileName,
                                                    DocumentPath = item.DocumentPath,
                                                    CreateDate = DateTime.UtcNow,
                                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                                };
                                                context_ts.TransactionDocumentDrafts.Add(document);
                                            }
                                            context_ts.SaveChanges();
                                        }
                                    }
                                    #endregion
                                }
                                ts.Complete();
                                IsSuccess = true;
                            }
                            else
                            {
                                #region Save As Draft
                                bool isBringup = false;
                                if (transaction.IsBringupTask == true)
                                    isBringup = true;
                                DBS.Entity.TransactionDraft data = new DBS.Entity.TransactionDraft()
                                {
                                    #region Unused field but Not Null
                                    BeneName = "-",
                                    BankID = 1,
                                    IsResident = false,
                                    IsCitizen = false,
                                    AmountUSD = 0.00M,
                                    Rate = 0.00M,
                                    DebitCurrencyID = 1,
                                    BizSegmentID = 1,
                                    //ChannelID = 1,
                                    #endregion
                                    #region Primary Field
                                    ApplicationDate = DateTime.UtcNow,
                                    IsTopUrgent = (transaction.IsTopUrgent == false && transaction.IsTopUrgentChain == false) ? 0 : (transaction.IsTopUrgent == true ? 1 : 2),
                                    IsNewCustomer = transaction.IsNewCustomer,
                                    CIF = transaction.Customer.CIF,
                                    ProductID = transaction.Product.ID,
                                    IsDraft = true,
                                    ApplicationID = string.Empty,
                                    StateID = (int)StateID.OnProgress,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName,
                                    Remarks = transaction.Remarks.ID == 0 ? null : transaction.Remarks.ID.ToString(),
                                    ValueDate = transaction.ValueDate,
                                    Amount = transaction.Amount,
                                    CurrencyID = transaction.Currency.ID,
                                    ChannelID = transaction.Channel.ID,
                                    TransactionTypeID = transaction.TransactionType.TransTypeID,
                                    FDAccNumber = transaction.FDAccNumber,
                                    CreditAccNumber = transaction.CreditAccNumber,
                                    DebitAccNumber = transaction.DebitAccNumber,
                                    InterestRate = transaction.InterestRate,
                                    Tenor = transaction.Tenor,
                                    MaturityDate = transaction.MaturityDate,
                                    FDBankName = transaction.FDBankName,
                                    AttachmentRemarks = transaction.AttachmentRemarks,
                                    IsBringUp = isBringup,
                                    SourceID = transaction.Source == null ? 0 : transaction.Source.ID,
									DocsComplete = transaction.DocsComplete,
                                    //AllInRate = transaction.AllInRate,
                                    FTPRate = transaction.FTPRate
                                    #endregion
                                };
                                if (transaction.Source != null)
                                {
                                    if (transaction.Source.Name != null)
                                    {
                                        data.SourceID = transaction.Source.ID;
                                    }
                                }
                                context_ts.TransactionDrafts.Add(data);
                                context_ts.SaveChanges();

                                transactionID = data.TransactionID;
                                TrIDInserted = transactionID;
                                #region Save Document
                                if (transaction.Documents != null)
                                {
                                    if (transaction.Documents.Count > 0)
                                    {
                                        foreach (var item in transaction.Documents)
                                        {
                                            Entity.TransactionDocumentDraft document = new Entity.TransactionDocumentDraft()
                                            {
                                                TransactionID = data.TransactionID,
                                                DocTypeID = item.Type.ID,
                                                PurposeID = item.Purpose.ID,
                                                Filename = item.FileName,
                                                DocumentPath = item.DocumentPath,
                                                CreateDate = DateTime.UtcNow,
                                                CreateBy = currentUser.GetCurrentUser().LoginName
                                            };
                                            context_ts.TransactionDocumentDrafts.Add(document);
                                        }
                                        context_ts.SaveChanges();
                                    }
                                }
                                #endregion
                                #region BringUp
                                if (transaction.IsBringupTask == true)
                                {
                                    Entity.TransactionBringupTask Bringup = new Entity.TransactionBringupTask()
                                    {
                                        TransactionID = TrIDInserted,
                                        TaskName = "Bring Up FD",
                                        AssignedTo = currentUser.GetCurrentUser().LoginName,
                                        AssignedFrom = currentUser.GetCurrentUser().LoginName,
                                        DateIn = DateTime.UtcNow,
                                        IsSubmitted = false,
                                        CreateDate = DateTime.UtcNow,
                                        CreateBy = currentUser.GetCurrentUser().LoginName,
                                    };
                                    context_ts.TransactionBringupTasks.Add(Bringup);
                                    context_ts.SaveChanges();
                                }
                                #endregion
                                ts.Complete();
                                IsSuccess = true;
                                #endregion
                            }
                        }
                        else
                        {
                            #region Submit New Transaction
                            DBS.Entity.Transaction data = new DBS.Entity.Transaction();
                            #region Unused field but Not Null
                            data.IsSignatureVerified = false;
                            data.IsDormantAccount = false;
                            data.IsFrezeAccount = false;
                            data.BeneName = "-";
                            data.BankID = 1;
                            data.IsResident = false;
                            data.IsCitizen = false;
                            data.AmountUSD = 0.00M;
                            data.Rate = 0.00M;
                            data.DebitCurrencyID = 1;
                            data.BizSegmentID = 1;
                            //data.ChannelID = 1;
                            #endregion
                            #region non-null
                            data.ApplicationDate = DateTime.UtcNow;
                            data.CIF = transaction.Customer.CIF;
                            data.ProductID = transaction.Product.ID;
                            data.CurrencyID = transaction.Currency.ID;
                            data.Amount = transaction.Amount;    
                            data.IsNewCustomer = transaction.IsNewCustomer;
                            data.IsDocumentComplete = false;
                            data.IsDraft = false;
                            data.StateID = (int)StateID.OnProgress;
                            data.CreateDate = DateTime.UtcNow;
                            data.CreateBy = currentUser.GetCurrentUser().LoginName;
                            #endregion

                            #region Primary Field
                            data.IsTopUrgent = (transaction.IsTopUrgent == false && transaction.IsTopUrgentChain == false) ? 0 : (transaction.IsTopUrgent == true ? 1 : 2);
                            data.ApplicationID = "-";
                            data.Remarks = transaction.Remarks.ID == 0 ? null : transaction.Remarks.ID.ToString();
                            data.ValueDate = transaction.ValueDate;
                            data.TransactionTypeID = transaction.TransactionType.TransTypeID;
                            data.ChannelID = transaction.Channel.ID;
                            data.FDAccNumber = transaction.FDAccNumber;
                            data.CreditAccNumber = transaction.CreditAccNumber;
                            data.DebitAccNumber = transaction.DebitAccNumber;
                            data.InterestRate = transaction.InterestRate;
                            data.Tenor = transaction.Tenor;
                            data.MaturityDate = transaction.MaturityDate == null ? null : transaction.MaturityDate;
                            data.FDBankName = transaction.FDBankName;
                            data.AttachmentRemarks = transaction.AttachmentRemarks;
                            data.TouchTimeStartDate = transaction.CreateDate;
                            data.DocsComplete = transaction.DocsComplete;
                            //data.AllInRate = transaction.AllInRate;
                            data.FTPRate = transaction.FTPRate;
                            if (transaction.Source != null)
                            {
                                if (transaction.Source.Name != null)
                                {
                                    data.SourceID = transaction.Source.ID;
                                }
                            }
                            #endregion
                                
                            context_ts.Transactions.Add(data);
                            context_ts.SaveChanges();

                            transactionID = data.TransactionID;
                            TrIDInserted = transactionID;
                            #region Document
                            if (transaction.Documents != null)
                            {
                                if (transaction.Documents.Count > 0)
                                {
                                    foreach (var item in transaction.Documents)
                                    {
                                        Entity.TransactionDocument document = new Entity.TransactionDocument()
                                        {
                                            TransactionID = data.TransactionID,
                                            DocTypeID = item.Type.ID,
                                            PurposeID = item.Purpose.ID,
                                            Filename = item.FileName,
                                            DocumentPath = item.DocumentPath,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().LoginName
                                        };
                                        context_ts.TransactionDocuments.Add(document);
                                    }
                                    context_ts.SaveChanges();
                                }
                            }
                            #endregion
                            ts.Complete();
                            #endregion
                        }
                    }
                    catch (Exception ex)
                    {
                        ts.Dispose();
                        message = ex.Message + ex.InnerException.ToString();
                    }
                }
            }
            if (!transaction.IsDraft)
            {
                using (DBSEntities context = new DBSEntities())
                {
                    var getTR = (from TR in context.Transactions
                                 join b in context.TransactionTypes on TR.TransactionTypeID equals b.TransTypeID
                                 where TR.TransactionID == TrIDInserted
                                 && b.IsDeleted == false
                                 select new
                                 {
                                     ApplicationID = TR.ApplicationID,
                                     TransactionType = b.TransactionType1
                                 }).SingleOrDefault();
                    ApplicationID = getTR.ApplicationID;
                    transactionType = getTR.TransactionType;
                    IsSuccess = true;
                }
            }

            return IsSuccess;
        }
        public bool GetTransactionDraftFDByID(long id, ref TransactionFDDraftModel transaction, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    var transactionDraft = (from trDraft in context.TransactionDrafts
                                            join Prd in context.Products on trDraft.ProductID equals (Prd.ProductID)
                                            join Cust in context.Customers on trDraft.CIF equals (Cust.CIF)
                                            join Curr in context.Currencies on trDraft.CurrencyID equals (Curr.CurrencyID)
                                            join TrType in context.TransactionTypes on trDraft.TransactionTypeID equals (TrType.TransTypeID)
                                            where trDraft.TransactionID == id
                                            select new TransactionFDDraftModel
                                             {
                                                 ID = trDraft.TransactionID,
                                                 TransactionID = trDraft.TransactionID,
                                                 ApplicationDate = trDraft.ApplicationDate == null ? DateTime.Now : trDraft.ApplicationDate,
                                                 IsTopUrgent = (trDraft.IsTopUrgent == 1) ? true : false,
                                                 IsTopUrgentChain = (trDraft.IsTopUrgent == 2) ? true : false,
                                                 IsNormal = (trDraft.IsTopUrgent == 0) ? true : false,
                                                 IsNewCustomer = trDraft.IsNewCustomer,
                                                 Customer = new CustomerModel { CIF = Cust.CIF, Name = Cust.CustomerName },
                                                 Product = new ProductModel { ID = Prd.ProductID, Name = Prd.ProductName },
                                                 IsDraft = true,
                                                 ApplicationID = trDraft.ApplicationID,
                                                 CreateDate = trDraft.CreateDate == null ? DateTime.Now : trDraft.CreateDate,
                                                 CreateBy = trDraft.CreateBy,
                                                 Channel = new ChannelModel()
                                                 {
                                                     ID = trDraft.Channel.ChannelID,
                                                     Name = trDraft.Channel.ChannelName,
                                                     LastModifiedBy = trDraft.Channel.UpdateDate == null ? trDraft.Channel.CreateBy : trDraft.Channel.UpdateBy,
                                                     LastModifiedDate = trDraft.Channel.UpdateDate ?? trDraft.Channel.CreateDate
                                                 },
                                                 Remarks = new ParsysModel { ID = 0, Name = trDraft.Remarks },
                                                 ValueDate = trDraft.ValueDate,
                                                 Amount = trDraft.Amount,
                                                 Currency = new CurrencyModel { ID = Curr.CurrencyID, Code = Curr.CurrencyCode },
                                                 TransactionType = new TransactionTypeParameterModel { TransTypeID = TrType.TransTypeID, TransactionTypeName = TrType.TransactionType1, ProductID = TrType.ProductID },
                                                 FDAccNumber = trDraft.FDAccNumber,
                                                 CreditAccNumber = trDraft.CreditAccNumber,
                                                 DebitAccNumber = trDraft.DebitAccNumber,
                                                 InterestRate = trDraft.InterestRate,
                                                 Tenor = trDraft.Tenor,
                                                 MaturityDate = trDraft.MaturityDate,
                                                 FDBankName = trDraft.FDBankName,
                                                 AttachmentRemarks = trDraft.AttachmentRemarks,
                                                 SourceID = trDraft.SourceID.HasValue? trDraft.SourceID.Value : 0,
												 DocsComplete = trDraft.DocsComplete,
                                                 //AllInRate = trDraft.AllInRate,
                                                 FTPRate = trDraft.FTPRate
                                             }).SingleOrDefault();

                    var docDraft = (from doc in context.TransactionDocumentDrafts
                                    where doc.TransactionID == id && doc.IsDeleted.Equals(false)
                                    select new TransactionDocumentDraftModel
                                    {
                                        ID = doc.TransactionDocumentID,
                                        IsDormant = doc.IsDormant,
                                        LastModifiedBy = doc.UpdateBy == null ? doc.CreateBy : doc.UpdateBy,
                                        LastModifiedDate = doc.UpdateDate == null ? doc.CreateDate : doc.CreateDate,
                                        Purpose = new DocumentPurposeModel { ID = doc.DocumentPurpose.PurposeID, Name = doc.DocumentPurpose.PurposeName },
                                        Type = new DocumentTypeModel { ID = doc.DocumentType.DocTypeID, Name = doc.DocumentType.DocTypeName },
                                        FileName = doc.Filename,
                                        DocumentPath = new DocumentPathModel { name = doc.Filename, lastModified = string.Empty, lastModifiedDate = DateTime.Now, DocPath = doc.DocumentPath, type = Util.ExistingDoctype }
                                    }).ToList();
                    if (docDraft != null)
                    {
                        transactionDraft.Documents = docDraft;
                    }
                    transaction = transactionDraft;
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool DeleteTransactionDraftByID(int id, ref string Message)
        {
            bool isDelete = false;
            using (DBSEntities context_ts = new DBSEntities())
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    try
                    {
                        Entity.TransactionDraft data = context.TransactionDrafts.Where(a => a.TransactionID.Equals(id)).SingleOrDefault();
                        if (data != null)
                        {
                            Entity.TransactionBringupTask dataBringup = context.TransactionBringupTasks.Where(a => a.TransactionID.Equals(id)).FirstOrDefault();
                            if (dataBringup != null)
                            {
                                data.IsDeleted = true;
                                context.SaveChanges();

                                dataBringup.DateOut = DateTime.UtcNow;
                                dataBringup.IsSubmitted = true;
                                dataBringup.SubmittedBy = currentUser.GetCurrentUser().LoginName;
                                dataBringup.UpdateBy = currentUser.GetCurrentUser().LoginName;
                                dataBringup.UpdateDate = DateTime.UtcNow;
                                context.SaveChanges();
                            }
                            else
                            {
                                var existingDocs = context.TransactionDocumentDrafts.Where(a => a.TransactionID.Equals(id)).ToList();
                                if (existingDocs != null)
                                {
                                    if (existingDocs.Count() > 0)
                                    {
                                        foreach (var item in existingDocs)
                                        {
                                            var deleteDoc = context.TransactionDocumentDrafts.Where(a => a.TransactionDocumentID.Equals(item.TransactionDocumentID)).SingleOrDefault();
                                            context.TransactionDocumentDrafts.Remove(deleteDoc);
                                            context.SaveChanges();
                                        }
                                    }
                                }
                                context.TransactionDrafts.Remove(data);
                                context.SaveChanges();
                            }
                            isDelete = true;
                        }
                        else
                        {
                            Message = string.Format("Draft ID {0} is does not exist.", id);
                            isDelete = false;
                        }
                        ts.Complete();
                    }
                    catch (Exception ex)
                    {
                        ts.Dispose();
                        Message = ex.Message;
                    }
                }
            }
            return isDelete;
        }
        public bool GetIQuoteData(ref IList<IquoteModel> IQuote, ref string Message)
        {
            bool isSuccess = false;
            try
            {
                IList<Entity.IQuote> data = context.IQuotes.Where(a => a.IsDeleted == false).ToList();
                if(data != null) {                    
                    foreach(var dat in data) {
                        IquoteModel quote = new IquoteModel();
                        quote.AUD = dat.AUD;
                        quote.CHF = dat.CHF;
                        quote.CNH = dat.CNH;
                        quote.EUR = dat.EUR;
                        quote.GBP = dat.GBP;
                        quote.IDR = dat.IDR;
                        quote.IQuoteID = dat.IQuoteID;
                        quote.JPY = dat.JPY;
                        quote.NZD = dat.NZD;
                        quote.SGD = dat.SGD;
                        quote.Tenor = dat.Tenor;
                        quote.USD = dat.USD;
                        IQuote.Add(quote);
                    }
                    isSuccess = true;
                }
            } catch (Exception ex){
                Message = ex.Message;
            }
            return isSuccess;
        }
    }
    #endregion
}