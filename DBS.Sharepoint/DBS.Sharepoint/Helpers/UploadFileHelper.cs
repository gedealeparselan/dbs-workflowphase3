﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBS.Sharepoint.Helpers
{
    public static class UploadFileHelper
    {
        /// <summary>
        /// Upload function for All Module Phase II
        /// Andiwijaya,23 November 2015
        /// Please use region based on Module
        /// Column From: Column name source
        /// Column To: Column name destination table
        /// Destination table: Staging table name
        /// SPName: Store Procedure Name
        /// </summary>
        public static string xlsxmimeType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        public static string xlsmimeType = "application/vnd.ms-excel";
        public static string ListName = "UploadedDocuments";
        public static string ActivityTypeisUpload = "Upload";
        public static string ActivityTypeisDownload = "Download";
        #region FD
        #region FD Interest Maintenance
        public static string[] ColumnFromFDInterest = {
                                                      "ActivityHistoryID","CreateDate","CreateBy","CUST_ID","CUST_NAME",	
	                                                  "SCHEME_TYPE","SCHEME_CODE","PRODUCT_TYPE","PROD_DESC",
                                                      "ACCT_NO","CCY",	"ORIGINAL_AMOUNT","ROLLOVER_DATE",	"MATURITY_DATE",	
                                                      "RM_NAME","BRANCH_CODE","INT_RATE_CODE","INTEREST_RATE","A/C_PREF_INT_CR",	
                                                      "NET_INT_RATE","INTEREST_MARGIN","FTP_RATE","SPREAD_RATE",	
                                                      "NETT INTEREST AMOUNT","MATURITY DATE INSTRUCTION","VALUE DATE INSTRUCTION",
                                                      "RENEWAL OPTION","Repayment Account" //,"Date"
                                                      };
        public static string[] ColumnToFDInterest = { 
                                                    "ActivityHistoryID","CreateDate","CreateBy","CUST_ID","CUST_NAME",	
	                                                "SCHEME_TYPE","SCHEME_CODE","PRODUCT_TYPE","PROD_DESC",
                                                    "ACCT_NO","CCY","ORIGINAL_AMOUNT","ROLLOVER_DATE",	"MATURITY_DATE",	
                                                    "RM_NAME","BRANCH_CODE","INT_RATE_CODE","INTEREST_RATE","A/C_PREF_INT_CR",	
                                                    "NET_INT_RATE","INTEREST_MARGIN","FTP_RATE","SPREAD_RATE",	
                                                    "NETT INTEREST AMOUNT","MATURITY DATE INSTRUCTION","VALUE DATE INSTRUCTION",
                                                    "RENEWAL OPTION","Repayment Account" //,"Date"
                                                    };
        public static string DestTableFDInterest = "[STG].[FDInterestMaintenance]";
        public static string SPNameFDInterest = "SP_UploadFDInterest";
        public static string UTypeFDInterest = "FD_UPLOAD_INTEREST";
        public static bool IsFirstRowFDInterest = false;
        public static int IndexHeaderFDInterest = 3;
        public static int IndexDataFDInterest = 4;
        #endregion

        #region FD Iquote
        public static string[] ColumnFromFDIquote = {
                                                      "ActivityHistoryID","CreateDate","CreateBy",
                                                      "Product","Tenor","IDR","USD","SGD","JPY",
                                                      "AUD","NZD","GBP","EUR","CHF","CNH"
                                                      };
        public static string[] ColumnToFDIquote = { 
                                                   "ActivityHistoryID","CreateDate","CreateBy",
                                                      "Product","Tenor","IDR","USD","SGD","JPY",
                                                      "AUD","NZD","GBP","EUR","CHF","CNH"
                                                    };
        public static string DestTableFDIquote = "[STG].[IQuote]";
        public static string SPNameFDIquote = "SP_UploadFDIQuote";
        public static string UTypeFDIquote = "FD_UPLOAD_IQUOTE";
        public static bool IsFirstRowFDIquote = false;
        public static int IndexHeaderFDIquote = 5;
        public static int IndexDataFDIquote = 7;
        #endregion
        #endregion

        #region TMO
        #region Murex
        public static string[] ColumnFromMurexNew = {
                                                      "ActivityHistoryID","CreateDate","CreateBy","NB","TRN.CREAT",	
	                                                  "FAMILY","GROUP","TYPE","TRN.DATE",
                                                      "Time transaction","START",	"Maturity","Counterparty",	"Buy/Sell",	
                                                      "CUR 0","NOMINAL 0","CUR 1","NOMINAL 1","RATE 0",	
                                                      "STP.STATUS","Mop_Type","S.COMMENT0","S.COMMENT1","S.COMMENT2",	
                                                      "B.COMMENT0","B.COMMENT1","B.COMMENT2","DSP_LABEL","Counterpart relation ship",
                                                      "COUNTRY","External Reference","counterpart type"
                                                      };
        public static string[] ColumnToMurexNew = { 
                                                      "ActivityHistoryID","CreateDate","CreateBy","NB","TRN.CREAT",	
	                                                  "FAMILY","GROUP","TYPE","TRN.DATE",
                                                      "Time transaction","START",	"Maturity","Counterparty",	"Buy/Sell",	
                                                      "CUR 0","NOMINAL 0","CUR 1","NOMINAL 1","RATE 0",	
                                                      "STP.STATUS","Mop_Type","S.COMMENT0","S.COMMENT1","S.COMMENT2",	
                                                      "B.COMMENT0","B.COMMENT1","B.COMMENT2","DSP_LABEL","Counterpart relation ship",
                                                      "COUNTRY","External Reference","counterpart type"
                                                    };
        public static string[] ColumnFromMurex = {
                                                      "ActivityHistoryID","CreateDate","CreateBy","NB.INT","NB.CREATOR",	
	                                                  "FAMILY","GROUP","TYPE","TRN.DATE",
                                                      "Time transaction","Start date",	"Maturity","COUNTERPART",	"Buy/Sell",	
                                                      "BRW_NOMU1","BRW_NOM1","BRW_NOMU2","BRW_NOM2","Price",	
                                                      "VALID","Mop_Type","S.COMMENT0","S.COMMENT1","S.COMMENT2",	
                                                      "B.COMMENT0","B.COMMENT1","B.COMMENT2","INSTRUMENT","Counterpart relation ship",
                                                      "COUNTRY","External Reference","counterpart type"
                                                      };
        public static string[] ColumnToMurex = { 
                                                      "ActivityHistoryID","CreateDate","CreateBy","NB","TRN.CREAT",	
	                                                  "FAMILY","GROUP","TYPE","TRN.DATE",
                                                      "Time transaction","START",	"Maturity","Counterparty",	"Buy/Sell",	
                                                      "CUR 0","NOMINAL 0","CUR 1","NOMINAL 1","RATE 0",	
                                                      "STP.STATUS","Mop_Type","S.COMMENT0","S.COMMENT1","S.COMMENT2",	
                                                      "B.COMMENT0","B.COMMENT1","B.COMMENT2","DSP_LABEL","Counterpart relation ship",
                                                      "COUNTRY","External Reference","counterpart type"
                                                    };
        public static string DestTableMurex = "[STG].[Murex]";
        public static string SPNameMurex = "SP_UploadMurex";
        public static string UTypeMurex = "TMO_MUREX";
        public static bool IsFirstRowMurex = true;
        public static int IndexHeaderMurex = 0;
        #endregion
        #region Murex Name Mapping
        public static string[] ColumnFromMurexName = {
                                                      "ActivityHistoryID","CreateDate","CreateBy","Murex Name","CIF",	
	                                                  "Customer Name"
                                                      };
        public static string[] ColumnToMurexName = { 
                                                    "ActivityHistoryID","CreateDate","CreateBy","Murex Name","CIF",	
	                                                  "Customer Name"
                                                    };
        public static string DestTableMurexName = "[STG].[MurexMapping]";
        public static string SPNameMurexName = "SP_UploadMurexMapping";
        public static string UTypeMurexName = "TMO_MUREXNAME";
        public static bool IsFirstRowMurexMapping = true;
        public static int IndexHeaderMurexMapping = 0;
        #endregion
        #endregion

        #region Payment
        #region Recon Payment
        public static string[] ColumnFromReconPayment = {
                                                    "ActivityHistoryID","CreateDate","CreateBy","Srl #","Payment Order#",	
	                                                "BOR","OSN","Sol Id","Date",
                                                    "Time","Transaction Mode","CIF","Remitter Account Id",	"Remitter Name",	
                                                    "Remitter Account Ccy","Transaction Amt based on Acct Ccy","Transaction Ccy","Transaction Amt on Transaction Ccy","Transaction channel (IDEAL,ATM,Paper)",	
                                                    "Bene. Bank Code","Bene. Bank Name","Bene. A/C No","Bene. Name",	
                                                    "PO Status","Remittance Status","FISC Reference #",
                                                    "RMT Type","Commission Fee based on Acct ccy","Telex Fee based on Acct ccy","Agent Bank Fee based on Acct ccy","Maker ID",
                                                    "Checker ID","Business Unit","RM Name","BATCH MANUAL IND ","IDEAL REFERENCE NUMBER","RECEIVER CORRESPONDENT BIC",
                                                    "SENDER CORRESPONDENT BIC","IN / OUT INDICATOR","NON RESIDENCE INDICATOR","NATIONALITY","COUNTRY OF INCORPORATION",
                                                    "PRINCIPAL PLACE OF OPERATION","FOREIGN PARTY","Customer Segment","Related Reference Number"
                                                      };
        public static string[] ColumnToReconPayment = { 
                                                    "ActivityHistoryID","CreateDate","CreateBy","Srl #","Payment Order#",	
	                                                "BOR","OSN","Sol Id","Date",
                                                    "Time","Transaction Mode","CIF","Remitter Account Id",	"Remitter Name",	
                                                    "Remitter Account Ccy","Transaction Amt based on Acct Ccy","Transaction Ccy","Transaction Amt on Transaction Ccy","Transaction channel (IDEAL,ATM,Paper)",	
                                                    "Bene. Bank Code","Bene. Bank Name","Bene. A/C No","Bene. Name",	
                                                    "PO Status","Remittance Status","FISC Reference #",
                                                    "RMT Type","Commission Fee based on Acct ccy","Telex Fee based on Acct ccy","Agent Bank Fee based","Maker ID",
                                                    "Checker ID","Business Unit","RM Name","BATCH MANUAL IND","IDEAL REFERENCE NUMBER","RECEIVER CORRESPONDENT BIC",
                                                    "SENDER CORRESPONDENT BIC","IN / OUT INDICATOR","NON RESIDENCE INDICATOR","NATIONALITY","COUNTRY OF INCORPORATION",
                                                    "PRINCIPAL PLACE OF OPERATION","FOREIGN PARTY","Customer Segment","Related Reference Number"
                                                    };
        public static string DestTableReconPayment = "[STG].[ReconPayment]";
        public static string SPNameReconPayment = "SP_UploadReconPayment";
        public static string UTypeReconPayment = "PAYMENT_UPLOAD_RECON";
        public static bool IsFirstRowReconPayment = false;
        public static int IndexHeaderReconPayment = 4;
        public static int IndexDataReconPayment = 5;
        #endregion
        #endregion

        #region Loan
        #region Loan Rollover
        public static string[] ColumnFromRollover = {
                                                      "ActivityHistoryID","CreateDate","CreateBy","NO","BRANCH ID",	
	                                                  "SCHEME CODE","CIF","CUSTOMER NAME","LOAN CONTRACT NO",
                                                      "DUE DATE","CCY",	"AMOUNT DUE","LIMIT ID","IBG SEGMENT","VALUE DATE(DD-MMM-YY)"
                                                      };
        public static string[] ColumnToRollover = { 
                                                     "ActivityHistoryID","CreateDate","CreateBy","NO","BRANCH ID",	
	                                                  "SCHEME CODE","CIF","CUSTOMER NAME","LOAN CONTRACT NO",
                                                      "DUE DATE","CCY",	"AMOUNT DUE","LIMIT ID","IBG SEGMENT","ValueDate"
                                                    };
        public static string DestTableRollover = "[STG].[LoanRollover]";
        public static string SPNameRollover = "SP_UploadLoanRollover";
        public static string UTypeRollover = "LOAN_UPLOAD_ROLLOVER";
        public static bool IsFirstRowRollover = false;
        public static int IndexHeaderRollover = 7;
        public static int IndexDataRollover = 10;
        #endregion
        #region Loan Interest Maintenance
        public static string[] ColumnFromLoanIM = {
                                                "ActivityHistoryID","CreateDate","CreateBy","NO","BRANCH ID",	
                                                "SCHEME CODE","CIF","CUSTOMER NAME","LOAN CONTRACT NO",
                                                "DUE DATE","CCY","PREVIOUS INT RATE","CURRENT INT RATE","IBG SEGMENT","Pegging Review Date","VALUE DATE(DD-MMM-YY)"
                                                };
        public static string[] ColumnToLoanIM = { 
                                                "ActivityHistoryID","CreateDate","CreateBy","NO","BRANCH ID",	
                                                "SCHEME CODE","CIF","CUSTOMER NAME","LOAN CONTRACT NO",
                                                "DUE DATE","CCY","PREVIOUS INT RATE","CURRENT INT RATE","IBG SEGMENT","Pegging Review Date","ValueDate"
                                            };
        public static string DestTableLoanIM = "[STG].[InterestMaintenance]";
        public static string SPNameLoanIM = "SP_UploadInterestMaintenance";
        public static string UTypeLoanIM = "LOAN_UPLOAD_INTEREST";
        public static bool IsFirstRowLoanIM = false;
        public static int IndexHeaderLoanIM = 7;
        public static int IndexDataLoanIM = 10;
        #endregion
        #region Loan SAM Account
        public static string[] ColumnFromSamAccount = {
                                                      "ActivityHistoryID","CreateDate","CreateBy","No","borrowersourceid",	
	                                                  "Liability Name"
                                                      };
        public static string[] ColumnToSamAccount = { 
                                                     "ActivityHistoryID","CreateDate","CreateBy","No","borrowersourceid",	
	                                                  "Liability Name"
                                                    };
        public static string DestTableSamAccount = "[STG].[SamAccount]";
        public static string SPNameSamAccount = "SP_UploadSamAccount";
        public static string UTypeSamAccount = "LOAN_UPLOAD_SAM";
        public static bool IsFirstRowSamAccount = true;
        public static int IndexHeaderSamAccount = 0;
        public static int IndexDataSamAccount = 0;
        #endregion
        #region Loan Matrix EOD
        public static string[] ColumnFromLoanEOD = {
                                                "ActivityHistoryID","CreateDate","CreateBy",	
                                                "Seq","Seq No","STAFF FLG","SOL ID","Customer","Account No",
                                                "Revolving / Non-","Product Code","Open Date","Activity",
                                                "Maturity Date","Currency","Contract Amount",
                                                "Interest rate code","FTP Rate","Margin",
                                                "All In Int Rate","Total Charges","Interest Paid",
                                                "Principal Paid","Trans ID","User ID"
                                                };
        public static string[] ColumnToLoanEOD = { 
                                                "ActivityHistoryID","CreateDate","CreateBy",	
                                                "Seq","Seq No","STAFF FLG","SOL ID","Customer","Account No",
                                                "Revolving / Non-","Product Code","Open Date","Activity",
                                                "Maturity Date","Currency","Contract Amount",
                                                "Interest rate code","FTP Rate","Margin",
                                                "All In Int Rate","Total Charges","Interest Paid",
                                                "Principal Paid","Trans ID","User ID"
                                            };
        public static string DestTableLoanEOD = "[STG].[LoanMetricsEOD]";
        public static string SPNameLoanEOD = "SP_UploadLoanMetricsEOD";
        public static string UTypeLoanEOD = "LOAN_UPLOAD_EOD";
        public static bool IsFirstRowLoanEOD = false;
        public static int IndexHeaderLoanEOD = 5;
        public static int IndexDataLoanEOD = 6;
        #endregion
        #region Loan Matrix SG
        public static string[] ColumnFromLoanSG = {
                                                "ActivityHistoryID","CreateDate","CreateBy",	
                                                "Seq","Seq No","STAFF FLG","SOL ID","Customer","Account No","Revolving / Non-",
                                                "Product Code","Open Date","Activity","Maturity Date","Currency",
                                                "Contract Amount","Interest rate code","FTP Rate","Margin",
                                                "All In Int Rate","Total Charges","Interest Paid",
                                                "Principal Paid","Trans ID","User ID"
                                                };
        public static string[] ColumnToLoanSG = { 
                                                "ActivityHistoryID","CreateDate","CreateBy",
                                                "Seq","Seq No","STAFF FLG","SOL ID","Customer","Account No","Revolving / Non-",
                                                "Product Code","Open Date","Activity","Maturity Date","Currency",
                                                "Contract Amount","Interest rate code","FTP Rate","Margin",
                                                "All In Int Rate","Total Charges","Interest Paid",
                                                "Principal Paid","Trans ID","User ID"
                                            };
        public static string DestTableLoanSG = "[STG].[LoanMetricsSG]";
        public static string SPNameLoanSG = "SP_UploadLoanMetricsSG";
        public static string UTypeLoanSG = "LOAN_UPLOAD_SG";
        public static bool IsFirstRowLoanSG = false;
        public static int IndexHeaderLoanSG = 5;
        public static int IndexDataLoanSG = 6;
        #endregion
        #region Loan Due
        public static string[] ColumnFromLoanDue = {
                                                "ActivityHistoryID","CreateDate","CreateBy",	
                                                "Branch ID","Scheme Code","Cust No","Customer Name",
                                                "Contract Ref No","Due Date","Component","Int Rate (%)","CCY","Amount Due"
                                                };
        public static string[] ColumnToLoanDue = { 
                                                "ActivityHistoryID","CreateDate","CreateBy",	
                                                "Branch ID","Scheme Code","Cust No","Customer Name",
                                                "Contract Ref No","Due Date","Component","Int Rate (%)","CCY","Amount Due"
                                            };
        public static string DestTableLoanDue = "[STG].[LoanDue]";
        public static string SPNameLoanDue = "SP_UploadLoanDue";
        public static string UTypeLoanDue = "LOAN_UPLOAD_DUE";
        public static bool IsFirstRowLoanDue = false;
        public static int IndexHeaderLoanDue = 3;
        public static int IndexDataLoanDue = 4;
        #endregion
        #region Loan Adhoc
        public static string[] ColumnFromLoanAdhoc = {
                                                "ActivityHistoryID","CreateDate","CreateBy",
                                                "No","Branch Name","BranchCode","Company","Category",
                                                "CIF","Credit Manager","Adhoc Required"
                                                };
        public static string[] ColumnToLoanAdhoc = { 
                                                "ActivityHistoryID","CreateDate","CreateBy",
                                                "No","Branch Name","BranchCode","Company","Category",
                                                "CIF","Credit Manager","Adhoc Required"
                                            };
        public static string DestTableLoanAdhoc = "[STG].[Adhoc]";
        public static string SPNameLoanAdhoc = "SP_UploadAdhoc";
        public static string UTypeLoanAdhoc = "LOAN_UPLOAD_ADHOC";
        public static bool IsFirstRowLoanAdhoc = false;
        public static int IndexHeaderLoanAdhoc = 2;
        public static int IndexDataLoanAdhoc = 3;
        #endregion
        #region Loan IQuote
        public static string[] ColumnFromLoanIQuote = {
                                                "ActivityHistoryID","CreateDate","CreateBy","Date",
                                                "Product","TenorCode","TenorName","CurrencyCode","BaseRate",
                                                "AllInLP","AllInFTPRate"
                                                };
        public static string[] ColumnToLoanIQuote = { 
                                                "ActivityHistoryID","CreateDate","CreateBy","LoaniQuotesDate",	
                                                "LoaniQuotesProduct","TenorCode","TenorName","CurrencyCode","BaseRate",
                                                "AllInLP","AllInFTPRate"
                                            };
        public static string DestTableLoanIQuote = "[STG].[LoaniQuotes]";
        public static string SPNameLoanIQuote = "SP_UploadiQuotes";// "SP_UploadIQuote";
        public static string UTypeLoanIQuote = "LOAN_UPLOAD_IQUOTE";
        public static bool IsFirstRowLoanIQuote = false;
        public static int IndexHeaderLoanIQuote = 6;
        public static int IndexDataLoanIQuote = 8;
        #endregion
        #endregion

        #region CIF
        #region CIF Change RM
        public static string[] ColumnFromCifRM = {
                                                    "ActivityHistoryID","CreateDate","CreateBy",
                                                    "NO","CIF","CUSTOMER NAME","CHANGE SEGMENT FROM",
                                                    "CHANGE SEGMENT TO","CHANGE FROM BRANCH CODE","CHANGE FROM RM CODE",
                                                    "CHANGE FROM BU","CHANGE TO BRANCH CODE",
                                                    "CHANGE TO RM CODE","CHANGE TO BU",
                                                    "ACCOUNT #\n( 10 Digit )","PC CODE\n( 1 Huruf 3 Angka )",
                                                    "FREE TEXT 5 (EA/TPB EA/TPB)","REMARKS / REASON",
                                                      };
        public static string[] ColumnToCifRM = { 
                                                    "ActivityHistoryID","CreateDate","CreateBy",	
                                                    "NO","CIF","CUSTOMER NAME","CHANGE SEGMENT FROM",
                                                    "CHANGE SEGMENT TO","CHANGE FROM BRANCH CODE","CHANGE FROM RM CODE",
                                                    "CHANGE FROM BU","CHANGE TO BRANCH CODE",
                                                    "CHANGE TO RM CODE","CHANGE TO BU",
                                                    "ACCOUNT #","PC CODE",
                                                    "FREE TEXT 5 (EA/TPB EA/TPB)","REMARKS / REASON",
                                                    };
        public static string DestTableCifRM = "[STG].[ChangeRM]";
        public static string SPNameCifRM = "SP_UploadChangeRM";
        public static string UTypeCifRM = "CIF_UPLOAD_CRM";
        public static bool IsFirstRowCifRM = false;
        public static int IndexHeaderCifRM = 2;
        public static int IndexDataCifRM = 3;
        #endregion
        #endregion

        #region IBG
        #region Annual Statement Letter
        public static string[] ColumnFromAnnual = {
                                                      "ActivityHistoryID","CreateDate","CreateBy","Customer Name","CIF",	
	                                                  "RM Name","TL"
                                                      };
        public static string[] ColumnToAnnual = { 
                                                    "ActivityHistoryID","CreateDate","CreateBy","Customer Name","CIF",	
	                                                  "RM Name","TL"
                                                    };
        public static string DestTableAnnual = "[STG].[AnnualStatementLetter]";
        public static string SPNameAnnual = "SP_UploadAnnual";
        public static string UTypeAnnual = "IBG_ANNUAL";
        public static bool IsFirstRowAnnual = true;
        public static int IndexHeaderAnnual = 0;
        #endregion
        #endregion

        #region CSO
        #region CSO Upload
        public static string[] ColumnFromCSO = {
                                                "ActivityHistoryID","CreateDate","CreateBy","CUST_ID","CUST_NAME","segment","SALES_CODE","Banker","Senior_Banker",
                                                "CUST_OPEN_DATE","SOL_ID","Cust_Branch_Name","CITY","CSO","CSO1","CSO2","CSO3","CSO4","CSO5","CSO6","CSO7","CSO8","CSO9","CSO10","HOME_ADDRESS","MAILING_ADDRESS",
                                                "CUST_TYPE","Nationality"
                                            };
        public static string[] ColumnToCSO = { 
                                                 "ActivityHistoryID","CreateDate","CreateBy","CUST_ID","CUST_NAME","segment","SALES_CODE","Banker","Senior_Banker",
                                                "CUST_OPEN_DATE","SOL_ID","Cust_Branch_Name","CITY","CSO","CSO1","CSO2","CSO3","CSO4","CSO5","CSO6","CSO7","CSO8","CSO9","CSO10","HOME_ADDRESS","MAILING_ADDRESS",
                                                "CUST_TYPE","Nationality"
                                            };
        public static string DestTableCSO = "[STG].[CSO]";
        public static string SPNameCSO = "SP_UploadCSO";
        public static string UTypeCSO = "CSO_UPLOAD";
        public static bool IsFirstRowCSO = false;
        public static int IndexHeaderCSO = 8;
        public static int IndexDataCSO = 9;
        #endregion
        #endregion
    }
}
