﻿using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Linq.Dynamic;

namespace DBS.WebAPI.Models
{
    #region property
    [ModelName("Fund")]
    public class CBOFundModel
    {
        public int ID { get; set; }
        public OffOnshoreModel OffOnshore { get; set; }
        public string FundCode { get; set; }
        public string FundName { get; set; }
        public string AccountNo { get; set; }
        public DateTime? SavingPlanDate { get; set; }
        public bool? IsSwitching { get; set; }
        public bool? IsSaving { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
    }

    public class OffOnshoreModel
    {
        public int ID { get; set; }
        public string Description { get; set; }
    }
    public class CBOFundRow : CBOFundModel
    {
        public int RowID { get; set; }
    }
    public class CBOFundGrid : Grid
    {
        public IList<CBOFundRow> Rows { get; set; }
    }
    #endregion
    #region filter
    public class CBOFundFilter : Filter
    {

    }
    #endregion
    #region interface
    public interface ICBOFundRepository : IDisposable
    {
        bool GetCBOFundByID(int Id, ref CBOFundModel CBOFund, ref string Message);
        bool GetCBOFund(ref IList<CBOFundModel> CBOFunds, int Limit, int Index, ref string Message);
        bool GetCBOFund(ref IList<CBOFundModel> CBOFunds, ref string Message);
        bool GetCBOFund(int Page, int Size, IList<CBOFundFilter> Filters, string sortColumn, string sortOrder, ref CBOFundGrid CBOFund, ref string Message);
        bool GetCBOFund(CBOFundFilter Filter, ref IList<CBOFundModel> CBOFunds, ref string Message);
        bool GetCBOFund(string Key, int Limit, ref IList<CBOFundModel> CBOFunds, ref string Message);
        bool AddCBOFund(CBOFundModel CBOFund, ref string Message);
        bool UpdateCBOFund(int Id, CBOFundModel CBOFund, ref string Message);
        bool DeleteCBOFund(int Id, ref string Message);
        bool GetCBOFundParams(string key, ref IList<CBOFundModel> CBOFunds, ref string Message);
        bool GetCBOFundParamsFund(string key, string pid, ref IList<CBOFundModel> CBOFunds, ref string Message);
    }
    #endregion
    #region repository
    public class CBOFundRepository : ICBOFundRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();
        public bool GetCBOFundByID(int Id, ref CBOFundModel CBOFund, ref string Message)
        {
            bool isSuccess = false;
            try
            {
                CBOFund = (from fund in context.Funds
                           where fund.IsDeleted.Equals(false) && fund.FundID.Equals(Id)
                           select new CBOFundModel
                           {
                               ID = fund.FundID,
                               OffOnshore = new OffOnshoreModel { ID = fund.OperationType, Description = fund.OperationType == 1 ? "Offshare" : (fund.OperationType == 2 ? "Onshare" : "CPF") },
                               FundCode = fund.FundCode,
                               FundName = fund.FundName,
                               AccountNo = fund.AccountNumber,
                               SavingPlanDate = fund.SavingPlanDate.Value,
                               IsSwitching = fund.IsSwitching,
                               IsSaving = fund.IsSaving,
                               LastModifiedBy = fund.UpdateDate == null ? fund.CreateBy : fund.UpdateBy,
                               LastModifiedDate = fund.UpdateDate == null ? fund.CreateDate : fund.UpdateDate.Value
                           }).SingleOrDefault();
                isSuccess = true;
            }
            catch (Exception e)
            {
                Message = e.Message;
            }
            return isSuccess;
        }

        public bool GetCBOFund(ref IList<CBOFundModel> CBOFunds, int Limit, int Index, ref string Message)
        {
            bool isSuccess = false;
            try
            {
                int skip = Limit * Index;
                using (DBSEntities cont = new DBSEntities())
                {
                    CBOFunds = (from fund in cont.Funds
                                where fund.IsDeleted.Equals(false)
                                orderby new { fund.OperationType, fund.FundName }
                                select new CBOFundModel
                                {
                                    ID = fund.FundID,
                                    OffOnshore = new OffOnshoreModel { ID = fund.OperationType, Description = fund.OperationType == 0 ? "Offshare" : "Onshare" },
                                    FundCode = fund.FundCode,
                                    FundName = fund.FundName,
                                    AccountNo = fund.AccountNumber,
                                    SavingPlanDate = fund.SavingPlanDate.Value,
                                    IsSwitching = fund.IsSwitching,
                                    IsSaving = fund.IsSaving,
                                    LastModifiedBy = fund.UpdateDate == null ? fund.CreateBy : fund.UpdateBy,
                                    LastModifiedDate = fund.UpdateDate == null ? fund.CreateDate : fund.UpdateDate.Value
                                }).Skip(skip).Take(Limit).ToList();
                }
                isSuccess = true;
            }
            catch (Exception e)
            {
                Message = e.Message;
            }
            return isSuccess;
        }

        public bool GetCBOFund(ref IList<CBOFundModel> CBOFunds, ref string Message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities cont = new DBSEntities())
                {
                    CBOFunds = (from fund in cont.Funds
                                where fund.IsDeleted.Equals(false)
                                orderby new { fund.OperationType, fund.FundName }
                                select new CBOFundModel
                                {
                                    ID = fund.FundID,
                                    OffOnshore = new OffOnshoreModel { ID = fund.OperationType, Description = fund.OperationType == 0 ? "Offshare" : "Onshare" },
                                    FundCode = fund.FundCode,
                                    FundName = fund.FundName,
                                    AccountNo = fund.AccountNumber,
                                    SavingPlanDate = fund.SavingPlanDate.Value,
                                    IsSwitching = fund.IsSwitching,
                                    IsSaving = fund.IsSaving,
                                    LastModifiedBy = fund.UpdateDate == null ? fund.CreateBy : fund.UpdateBy,
                                    LastModifiedDate = fund.UpdateDate == null ? fund.CreateDate : fund.UpdateDate.Value
                                }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception e)
            {
                Message = e.Message;
            }
            return isSuccess;
        }

        public bool GetCBOFund(int Page, int Size, IList<CBOFundFilter> Filters, string sortColumn, string sortOrder, ref CBOFundGrid CBOFund, ref string Message)
        {
            bool isSuccess = false;
            try
            {
                int skip = (Page - 1) * Size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                CBOFundModel filter = new CBOFundModel();
                if (Filters != null)
                {
                    filter.FundCode = Filters.Where(a => a.Field.Equals("FundCode")).Select(a => a.Value).SingleOrDefault();
                    filter.FundName = Filters.Where(a => a.Field.Equals("FundName")).Select(a => a.Value).SingleOrDefault();
                    //filter.OffOnshore = Int32.Parse(Filters.Where(a => a.Field.Equals("OffOnshore")).Select(a => a.Value).SingleOrDefault());
                    filter.AccountNo = Filters.Where(a => a.Field.Equals("AccountNo")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = Filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (Filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse(Filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }
                using (DBSEntities contex = new DBSEntities())
                {
                    var data = (from fund in contex.Funds
                                let isFundCode = !string.IsNullOrEmpty(filter.FundCode)
                                let isFundName = !string.IsNullOrEmpty(filter.FundName)
                                //let isOperationType = filter.OffOnshore <= 0 ? false : true
                                let isAccountNumber = !string.IsNullOrEmpty(filter.AccountNo)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where fund.IsDeleted.Equals(false) &&
                                (isFundCode ? fund.FundCode.Contains(filter.FundCode) : true) &&
                                (isFundName ? fund.FundName.Contains(filter.FundName) : true) &&
                                    //(isOperationType ? fund.OperationType == filter.OffOnshore : true) &&
                                (isAccountNumber ? fund.AccountNumber.Contains(filter.AccountNo) : true) &&
                                (isCreateBy ? (fund.UpdateDate == null ? fund.CreateBy.Contains(filter.LastModifiedBy) : fund.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                (isCreateDate ? ((fund.UpdateDate == null ? fund.CreateDate : fund.UpdateDate) > filter.LastModifiedDate.Value && ((fund.UpdateDate == null ? fund.CreateDate : fund.UpdateDate.Value) < maxDate)) : true)
                                select new CBOFundModel
                                {
                                    ID = fund.FundID,
                                    OffOnshore = new OffOnshoreModel { ID = fund.OperationType, Description = fund.OperationType == 0 ? "Offshare" : "Onshare" },
                                    FundCode = fund.FundCode,
                                    FundName = fund.FundName,
                                    AccountNo = fund.AccountNumber,
                                    SavingPlanDate = fund.SavingPlanDate.Value,
                                    IsSwitching = fund.IsSwitching,
                                    IsSaving = fund.IsSaving,
                                    LastModifiedBy = fund.UpdateDate == null ? fund.CreateBy : fund.UpdateBy,
                                    LastModifiedDate = fund.UpdateDate == null ? fund.CreateDate : fund.UpdateDate.Value
                                });
                    CBOFund.Page = Page;
                    CBOFund.Size = Size;
                    CBOFund.Total = data.Count();
                    CBOFund.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    CBOFund.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new CBOFundRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            OffOnshore = a.OffOnshore,
                            FundCode = a.FundCode,
                            FundName = a.FundName,
                            AccountNo = a.AccountNo,
                            SavingPlanDate = a.SavingPlanDate,
                            IsSwitching = a.IsSwitching,
                            IsSaving = a.IsSaving,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(Size)
                        .ToList();
                }
                isSuccess = true;
            }
            catch (Exception e)
            {
                Message = e.Message;
            }
            return isSuccess;
        }

        public bool GetCBOFund(CBOFundFilter Filter, ref IList<CBOFundModel> CBOFunds, ref string Message)
        {
            throw new NotImplementedException();
        }

        public bool GetCBOFund(string Key, int Limit, ref IList<CBOFundModel> CBOFunds, ref string Message)
        {
            bool isSuccess = false;

            try
            {
                CBOFunds = (from fund in context.Funds
                            where fund.IsDeleted.Equals(false) &&
                            fund.OperationType.ToString().Contains(Key) || fund.FundCode.Contains(Key) || fund.FundName.Contains(Key) || fund.AccountNumber.Contains(Key)
                            orderby new { fund.OperationType, fund.FundName }
                            select new CBOFundModel
                            {
                                ID = fund.FundID,
                                OffOnshore = new OffOnshoreModel { ID = fund.OperationType, Description = fund.OperationType == 0 ? "Offshare" : "Onshare" },
                                FundCode = fund.FundCode,
                                FundName = fund.FundName,
                                AccountNo = fund.AccountNumber,
                                SavingPlanDate = fund.SavingPlanDate.Value,
                                IsSwitching = fund.IsSwitching,
                                IsSaving = fund.IsSaving,
                                LastModifiedBy = fund.UpdateDate == null ? fund.CreateBy : fund.UpdateBy,
                                LastModifiedDate = fund.UpdateDate == null ? fund.CreateDate : fund.UpdateDate.Value
                            }).Take(Limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                Message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddCBOFund(CBOFundModel cboFund, ref string message)
        {
            bool isSuccess = false;
            try
            {

                if (!context.Funds.Where(a => a.OperationType == cboFund.OffOnshore.ID && a.FundCode.Equals(cboFund.FundCode.Trim()) && a.FundName.ToLower().Equals(cboFund.FundName.Trim().ToLower()) && a.IsDeleted.Equals(false)).Any())
                {
                    context.Funds.Add(new Entity.Fund()
                    {
                        OperationType = cboFund.OffOnshore.ID,
                        FundCode = cboFund.FundCode.Trim(),
                        FundName = cboFund.FundName.Trim(),
                        AccountNumber = cboFund.AccountNo.Trim(),
                        SavingPlanDate = cboFund.IsSaving == true ? cboFund.SavingPlanDate : null,
                        IsSwitching = cboFund.IsSwitching,
                        IsSaving = cboFund.IsSaving,
                        IsDeleted = false,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().LoginName
                    });

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("CBO Fund data for Operation Type {0}, Fund Code {1} and Fund Name {2} are already exist.", cboFund.OffOnshore.ID, cboFund.FundCode, cboFund.FundName);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }
            return isSuccess;
        }

        public bool UpdateCBOFund(int id, CBOFundModel cboFund, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.Fund data = context.Funds.Where(a => a.FundID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    if (context.Funds.Where(a => a.FundID != cboFund.ID && a.OperationType.Equals(cboFund.OffOnshore.ID) && a.FundCode.Equals(cboFund.FundCode) && a.FundName.Equals(cboFund.FundName) && a.IsDeleted.Equals(false)).Count() >= 1)
                    {
                        message = string.Format("Operation Type {0}, Fund Code {1} and Fund Name {2}  are exist.", cboFund.OffOnshore.ID, cboFund.FundCode, cboFund.FundName);
                    }
                    else
                    {
                        data.OperationType = cboFund.OffOnshore.ID;
                        data.FundCode = cboFund.FundCode.Trim();
                        data.FundName = cboFund.FundName.Trim();
                        data.AccountNumber = cboFund.AccountNo.Trim();
                        data.SavingPlanDate = cboFund.IsSaving == true ? cboFund.SavingPlanDate : null;
                        data.IsSwitching = cboFund.IsSwitching;
                        data.IsSaving = cboFund.IsSaving;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().LoginName;

                        context.SaveChanges();

                        isSuccess = true;
                    }
                }
                else
                {
                    message = string.Format("CBO Fund data for Fund ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeleteCBOFund(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.Fund data = context.Funds.Where(a => a.FundID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().LoginName;
                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("CBO Fund data for Fund ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        public bool GetCBOFundParams(string key, ref IList<CBOFundModel> CBOFunds, ref string Message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities cont = new DBSEntities())
                {
                    CBOFunds = (from fund in cont.Funds
                                where fund.IsDeleted.Equals(false) && (fund.FundName.Contains(key) || fund.FundCode.Contains(key))
                                orderby new { fund.OperationType, fund.FundName }
                                select new CBOFundModel
                                {
                                    ID = fund.FundID,
                                    OffOnshore = new OffOnshoreModel { ID = fund.OperationType, Description = fund.OperationType == 0 ? "Offshare" : "Onshare" },
                                    FundCode = fund.FundCode,
                                    FundName = fund.FundName,
                                    AccountNo = fund.AccountNumber,
                                    SavingPlanDate = fund.SavingPlanDate.Value,
                                    IsSwitching = fund.IsSwitching,
                                    IsSaving = fund.IsSaving,
                                    LastModifiedBy = fund.UpdateDate == null ? fund.CreateBy : fund.UpdateBy,
                                    LastModifiedDate = fund.UpdateDate == null ? fund.CreateDate : fund.UpdateDate.Value
                                }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception e)
            {
                Message = e.Message;
            }
            return isSuccess;
        }


        public bool GetCBOFundParamsFund(string key, string pid, ref IList<CBOFundModel> CBOFunds, ref string Message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities cont = new DBSEntities())
                {
                    int intPID = int.Parse(pid);
                    if (intPID == 0)
                    {
                        CBOFunds = (from fund in cont.Funds
                                    where fund.IsDeleted.Equals(false) && (fund.FundName.Contains(key) || fund.FundCode.Contains(key))
                                    orderby new { fund.OperationType, fund.FundName }
                                    select new CBOFundModel
                                    {
                                        ID = fund.FundID,
                                        OffOnshore = new OffOnshoreModel { ID = fund.OperationType, Description = fund.OperationType == (int)UTOperative.offshore ? "Offshare" : fund.OperationType == (int)UTOperative.onshore ? "Onshare" : "CPF" },
                                        FundCode = fund.FundCode,
                                        FundName = fund.FundName,
                                        AccountNo = fund.AccountNumber,
                                        SavingPlanDate = fund.SavingPlanDate.Value,
                                        IsSwitching = fund.IsSwitching,
                                        IsSaving = fund.IsSaving,
                                        LastModifiedBy = fund.UpdateDate == null ? fund.CreateBy : fund.UpdateBy,
                                        LastModifiedDate = fund.UpdateDate == null ? fund.CreateDate : fund.UpdateDate.Value
                                    }).ToList();
                    }
                    else
                    {
                        CBOFunds = (from fund in cont.Funds
                                    where fund.IsDeleted.Equals(false) && (fund.FundName.Contains(key) || fund.FundCode.Contains(key)) && fund.OperationType == intPID
                                    orderby new { fund.OperationType, fund.FundName }
                                    select new CBOFundModel
                                    {
                                        ID = fund.FundID,
                                        OffOnshore = new OffOnshoreModel { ID = fund.OperationType, Description = fund.OperationType == 1 ? "Offshare" : fund.OperationType == 2 ? "Onshare" : "CPF" },
                                        FundCode = fund.FundCode,
                                        FundName = fund.FundName,
                                        AccountNo = fund.AccountNumber,
                                        SavingPlanDate = fund.SavingPlanDate.Value,
                                        IsSwitching = fund.IsSwitching,
                                        IsSaving = fund.IsSaving,
                                        LastModifiedBy = fund.UpdateDate == null ? fund.CreateBy : fund.UpdateBy,
                                        LastModifiedDate = fund.UpdateDate == null ? fund.CreateDate : fund.UpdateDate.Value
                                    }).ToList();
                    }
                }
                isSuccess = true;
            }
            catch (Exception e)
            {
                Message = e.Message;
            }
            return isSuccess;
        }
    }
    #endregion

}