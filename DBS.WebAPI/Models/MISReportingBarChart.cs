﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using DBS.Entity;
using DBS.WebAPI.Models;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using System.Runtime.Serialization;
using System.Data.Entity;
using System.Data;
using System.Data.Common;
using System.Data.EntityClient;
using System.Data.SqlClient;

namespace DBS.WebAPI.Models
{
    #region property
    [DataContract]
    [ModelName("MISReportingBarChart")]
    public class MISReportingBarChartModel
    {

        /// <summary>
        /// Product Name
        /// </summary>
        [DataMember]
        public string productName { get; set; }

        /// <summary>
        /// Hour
        /// </summary>
        [DataMember]
        public int hour { get; set; }

        /// <summary>
        /// Completed Quantity
        /// </summary>
        [DataMember]
        public int qtyCompleted { get; set; }

        /// <summary>
        /// On Progress Quantity
        /// </summary>
        [DataMember]
        public int qtyOnProgress { get; set; }

        /// <summary>
        /// Cancelled Quantity
        /// </summary>
        [DataMember]
        public int qtyCancelled { get; set; }

        /// <summary>
        /// Created Date
        /// </summary>
        [DataMember]
        public DateTime createdDate { get; set; }


        /// <summary>
        /// Transaction ID
        /// </summary>
        [DataMember]
        public long transactionsID { get; set; }


    }

    [ModelName("MISReportingClearTableVolumeMatrix")]
    public class MISReportingClearTableVolumeMatrixModel
    {

        /// <summary>
        /// Product Name
        /// </summary>
        [DataMember]
        public Int64 ID { get; set; }

        /// <summary>
        /// Hour
        /// </summary>
        [DataMember]
        public Int64 WorkflowApproverID { get; set; }

        /// <summary>
        /// Completed Quantity
        /// </summary>
        [DataMember]
        public DateTime WorkflowTaskExitTime { get; set; }


    }

    #endregion

    #region interface
    public interface IMISReportingBarChart : IDisposable
    {
        bool GetMISReportingBarChart(int transactionType, int segmentation, int channel, int branch, DateTime startDate, DateTime endDate, DateTime clientDateTime, ref IList<MISReportingBarChartModel> resultOutput, ref string message);     

    }
    #endregion

    #region repository

    public class MISReportingBarChartRepository : IMISReportingBarChart
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();        

        public bool GetMISReportingBarChart(int transactionType, int segmentation, int channel, int branch, DateTime startDate, DateTime endDate, DateTime clientDateTime, ref IList<MISReportingBarChartModel> resultOutput, ref string message)
        {
            bool isSuccess = false;
            DateTime serverUTC = DateTime.UtcNow;
            int hourDiff, oneDayHour = 24;
            if (clientDateTime > serverUTC)
            {
                hourDiff = clientDateTime.Hour - serverUTC.Hour;
            }
            else if (clientDateTime < serverUTC)
            {
                hourDiff = oneDayHour + clientDateTime.Hour - serverUTC.Hour;
            }
            else
            {
                hourDiff = 0;
            }
            try
            {
                using (DBSEntities context = new DBSEntities())
                {

                    var ProductCount = (from a in context.Products select new { a.ProductID }).Count();
                    var BizSegmentCount = (from a in context.BizSegments select new { a.BizSegmentID }).Count();
                    var ChannelCount = (from a in context.Channels select new { a.ChannelID }).Count();
                    var BranchCount = (from a in context.Locations select new { a.LocationID }).Count();

                    bool isAllProduct = false; bool isAllSegment = false;
                    bool isAllChannel = false; bool isAllBranch = false;

                    bool isStartDateNull = false; bool isEndDateNull = false;

                    if (transactionType == ProductCount + 1)
                    {
                        isAllProduct = true;
                    }
                    if (segmentation == BizSegmentCount + 1)
                    {
                        isAllSegment = true;
                    }
                    if (channel == ChannelCount + 1)
                    {
                        isAllChannel = true;
                    }
                    if (branch == BranchCount + 1)
                    {
                        isAllBranch = true;
                    }
                    if (startDate == null)
                    {
                        isStartDateNull = true;
                    }
                    if (endDate == null)
                    {
                        isEndDateNull = true;
                    }

                    if ((startDate != null) && (endDate != null))
                    {
                        resultOutput = (from a in context.Products.AsNoTracking()
                                        join b in context.Transactions.AsNoTracking()
                                        on a.ProductID equals b.ProductID
                                        join c in context.BizSegments.AsNoTracking()
                                        on b.BizSegmentID equals c.BizSegmentID
                                        join d in context.ProgressStates.AsNoTracking()
                                        on b.StateID equals d.StateID
                                        join e in context.Channels.AsNoTracking()
                                        on b.ChannelID equals e.ChannelID
                                        join f in context.Customers.AsNoTracking()
                                        on b.CIF equals f.CIF                                      
                                        join g in context.Locations.AsNoTracking()
                                        on f.LocationID equals g.LocationID into group1
                                        from gr1 in group1.DefaultIfEmpty()

                                        let utcToLocalTransaction = DbFunctions.AddHours(b.CreateDate, hourDiff)
                                        let isAllQueryProduct = isAllProduct
                                        let isAllQuerySegment = isAllSegment
                                        let isAllQueryChannel = isAllChannel
                                        let isAllQueryBranch = isAllBranch
                                        let isAllQueryStartDate = isStartDateNull
                                        let isAllQueryEndDate = isEndDateNull

                                        where (a.IsDeleted.Equals(false))
                                          && (c.IsDeleted.Equals(false))
                                          && (e.IsDeleted.Equals(false))
                                          && (f.IsDeleted.Equals(false))                                              
                                          && (!isAllQueryProduct ? a.ProductID == transactionType : true)
                                          && (!isAllQuerySegment ? c.BizSegmentID == segmentation : true)
                                          && (!isAllQueryChannel ? e.ChannelID == channel : true)
                                          && (!isAllQueryBranch ? f.LocationID == branch : true)
                                          && (!isAllQueryStartDate ? (utcToLocalTransaction.Value >= startDate) : true)
                                          && (!isAllQueryEndDate ? (utcToLocalTransaction.Value <= endDate) : true)
                                          && (b.StateID == 1 || b.StateID == 2 || b.StateID == 3)
                                        select new
                                        {

                                            hour = utcToLocalTransaction.Value.Hour,
                                            stateID = b.StateID,


                                        } into jointable1
                                        group jointable1 by new
                                        {
                                            Hour = jointable1.hour,
                                            StateID = jointable1.stateID

                                        } into grouptable1
                                        select new
                                        {

                                            hourLast = grouptable1.Key.Hour,
                                            qtyOnProgressLast = (from grouptable in grouptable1 where grouptable.stateID == 1 select grouptable).Count(),
                                            qtyCompletedLast = (from grouptable in grouptable1 where grouptable.stateID == 2 select grouptable).Count(),
                                            qtyCancelledLast = (from grouptable in grouptable1 where grouptable.stateID == 3 select grouptable).Count()
                                        } into jointable2
                                        group jointable2 by new
                                        {
                                            Hour = jointable2.hourLast
                                        }
                                            into grouptable2
                                            select new MISReportingBarChartModel
                                            {
                                                hour = grouptable2.Key.Hour,
                                                qtyOnProgress = grouptable2.Sum(a => a.qtyOnProgressLast),
                                                qtyCompleted = grouptable2.Sum(a => a.qtyCompletedLast),
                                                qtyCancelled = grouptable2.Sum(a => a.qtyCancelledLast)

                                            }).ToList();
                    }


                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
    #endregion

}