﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace DBS.Sharepoint
{
    #region Interfaces

    [ServiceContract]
    public interface IGenerateFileV2Service
    {
        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Test HelloWorld();

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        GeneratedFileInfoV2 GenerateFileV2(DataInformationCollectionV2 data);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        GeneratedFileInfoV2 GenerateFileInfoV2_2(DataInformationCollectionV2Turunan data);
    }

    #endregion

    #region Properties
    [DataContract]
    public class DataInformationV2
    {
        [DataMember]
        public long ID { get; set; }

        [DataMember]
        public string TipeDesc { get; set; }

    }

    [DataContract]
    public class DataInformationCollectionV2
    {
        [DataMember]
        public string FileTipe { get; set; }

        [DataMember]
        public string dtimeFile { get; set; }

        [DataMember]
        public string CIF { get; set; }

        [DataMember]
        public IList<DataInformationV2> DataInformation { get; set; }
    }

    [DataContract]
    public class DataInformationCollectionV2Turunan : DataInformationCollectionV2
    {
        [DataMember]
        public long ID { get; set; }
    }

    [DataContract]
    public class GeneratedFileInfoV2
    {
        [DataMember]
        public long InsertedID { get; set; }

        [DataMember]
        public string InsertedURL { get; set; }

        [DataMember]
        public bool IsSuccess { get; set; }

        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public string FileName { get; set; }
    }
    #endregion
}
