﻿using DBS.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class ParameterSystemController : ApiController
    {
        private string message = string.Empty;

        private readonly IParameterSystemRepository repository = null;
        private readonly IParameterSystemRepository repoParameterSystem = null;
        private readonly IGeneralParameterRepository repoGeneralParam = null;
        public ParameterSystemController()
        {
            this.repository = new ParameterSystemRepository();
            this.repoGeneralParam = new GeneralParameterRepository();
        }
        public ParameterSystemController(IParameterSystemRepository repository)
        {
            this.repository = repository;
        }

        #region Collateral
        [ResponseType(typeof(IList<ParameterSystemModel>))]
        [Route("api/ParameterSystem/ParameterSystemCollateral")]
        public HttpResponseMessage GetAllCollateral()
        {
            IList<ParameterSystemModel> result = new List<ParameterSystemModel>();
            if (!this.repository.GetParamSystem(ref result, 10, 0, ref message, "collateral"))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<ParameterSystemModel>>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(IList<ParameterSystemModel>))]
        [Route("api/ParameterSystem/ParameterSystemCollateral")]
        public HttpResponseMessage GetAllCollateral(int? limit, int? index)
        {
            IList<ParameterSystemModel> result = new List<ParameterSystemModel>();
            if (!repository.GetParamSystem(ref result, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message, "collateral"))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<IList<ParameterSystemModel>>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(ParameterSystemGrid))]
        [AcceptVerbs("GET", "POST")]
        [Route("api/ParameterSystem/ParameterSystemCollateral")]
        public HttpResponseMessage GetCollateral(int? page, int? size, string sort_column, string sort_order, IList<ParameterSystemFilter> filters)
        {
            ParameterSystemGrid result = new ParameterSystemGrid();
            if (!repository.GetParamSystem(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "ParameterType" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref result, ref message, "collateral"))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<ParameterSystemGrid>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(IList<ParameterSystemModel>))]
        [HttpGet]
        [Route("api/ParameterSystem/ParameterSystemCollateral/Search")]
        public HttpResponseMessage SearchCollateral(string query, int? limit)
        {
            IList<ParameterSystemModel> result = new List<ParameterSystemModel>();

            if (!repository.GetParamSystem(query, limit.HasValue ? limit.Value : 10, ref result, ref message, "collateral"))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<ParameterSystemModel>>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(ParameterSystemModel))]
        [Route("api/ParameterSystem/ParameterSystemCollateral/{id}")]
        [HttpGet]
        public HttpResponseMessage GetByIdCollateral(int id)
        {
            ParameterSystemModel result = new ParameterSystemModel();

            if (!repository.GetParamSystemByID(id, ref result, ref message, "collateral"))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<ParameterSystemModel>(HttpStatusCode.OK, result);
            }
        }

        [HttpPost]
        [ResponseType(typeof(string))]
        [Route("api/ParameterSystem/ParameterSystemCollateral")]
        public HttpResponseMessage PostCollateral(ParameterSystemModel paramSystem)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddParamSystem(paramSystem, ref message, "collateral"))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse<string>(HttpStatusCode.Created, "New Parameter System Collateral data has been created.");
                }
            }
        }

        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/ParameterSystem/ParameterSystemCollateral/{id}")]
        public HttpResponseMessage PutCollateral(int id, ParameterSystemModel paramSystem)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateParamSystem(id, paramSystem, ref message, "collateral"))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Parameter System Collateral data has been updated.");
                }
            }
        }

        [HttpDelete]
        [Route("api/ParameterSystem/ParameterSystemCollateral/{id}")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage DeleteCollateral(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteParamSystem(id, ref message, "collateral"))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse<string>(HttpStatusCode.OK, "Parameter System Collateral data has been deleted.");
                }
            }
        }

        [ResponseType(typeof(StaticDropdownModel))]
        [Route("api/ParameterSystem/ParameterSystemCollateral/SD")]
        [HttpGet]
        public HttpResponseMessage GetDropdownCollateral()
        {
            IList<StaticDropdownModel> result = new List<StaticDropdownModel>();

            if (!repository.GetStaticDropdownByCategoryName(ref result, ref message, "collateral"))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<IList<StaticDropdownModel>>(HttpStatusCode.OK, result);
            }
        }
        #endregion

        #region Tmo
        [ResponseType(typeof(IList<ParameterSystemModel>))]
        [Route("api/ParameterSystem/ParameterSystemTmo")]
        public HttpResponseMessage GetAllTmo()
        {
            IList<ParameterSystemModel> result = new List<ParameterSystemModel>();
            if (!this.repository.GetParamSystem(ref result, 10, 0, ref message, "tmo"))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<ParameterSystemModel>>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(IList<ParameterSystemModel>))]
        [Route("api/ParameterSystem/ParameterSystemTmo")]
        public HttpResponseMessage GetAllTmo(int? limit, int? index)
        {
            IList<ParameterSystemModel> result = new List<ParameterSystemModel>();
            if (!repository.GetParamSystem(ref result, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message, "tmo"))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<IList<ParameterSystemModel>>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(ParameterSystemGrid))]
        [AcceptVerbs("GET", "POST")]
        [Route("api/ParameterSystem/ParameterSystemTmo")]
        public HttpResponseMessage GetTmo(int? page, int? size, string sort_column, string sort_order, IList<ParameterSystemFilter> filters)
        {
            ParameterSystemGrid result = new ParameterSystemGrid();
            if (!repository.GetParamSystem(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "ParameterType" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref result, ref message, "tmo"))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<ParameterSystemGrid>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(IList<ParameterSystemModel>))]
        [HttpGet]
        [Route("api/ParameterSystem/ParameterSystemTmo/Search")]
        public HttpResponseMessage SearchTmo(string query, int? limit)
        {
            IList<ParameterSystemModel> result = new List<ParameterSystemModel>();

            if (!repository.GetParamSystem(query, limit.HasValue ? limit.Value : 10, ref result, ref message, "tmo"))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<ParameterSystemModel>>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(ParameterSystemModel))]
        [Route("api/ParameterSystem/ParameterSystemTmo/{id}")]
        [HttpGet]
        public HttpResponseMessage GetByIdTmo(int id)
        {
            ParameterSystemModel result = new ParameterSystemModel();

            if (!repository.GetParamSystemByID(id, ref result, ref message, "tmo"))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<ParameterSystemModel>(HttpStatusCode.OK, result);
            }
        }

        [HttpPost]
        [ResponseType(typeof(string))]
        [Route("api/ParameterSystem/ParameterSystemTmo")]
        public HttpResponseMessage PostTmo(ParameterSystemModel paramSystem)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddParamSystem(paramSystem, ref message, "tmo"))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse<string>(HttpStatusCode.Created, "New Parameter System TMO data has been created.");
                }
            }
        }

        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/ParameterSystem/ParameterSystemTmo/{id}")]
        public HttpResponseMessage PutTmo(int id, ParameterSystemModel paramSystem)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateParamSystem(id, paramSystem, ref message, "tmo"))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Parameter System TMO data has been updated.");
                }
            }
        }

        [HttpDelete]
        [Route("api/ParameterSystem/ParameterSystemTmo/{id}")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage DeleteTmo(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteParamSystem(id, ref message, "tmo"))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse<string>(HttpStatusCode.OK, "Parameter System TMO data has been deleted.");
                }
            }
        }

        [ResponseType(typeof(StaticDropdownModel))]
        [Route("api/ParameterSystem/ParameterSystemTmo/SD")]
        [HttpGet]
        public HttpResponseMessage GetDropdownTmo()
        {
            IList<StaticDropdownModel> result = new List<StaticDropdownModel>();

            if (!repository.GetStaticDropdownByCategoryName(ref result, ref message, "tmo"))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<IList<StaticDropdownModel>>(HttpStatusCode.OK, result);
            }
        }
        #endregion

        #region Loan
        [ResponseType(typeof(IList<ParameterSystemModel>))]
        [Route("api/ParameterSystem/ParameterSystemLoan")]
        public HttpResponseMessage GetAllLoan()
        {
            IList<ParameterSystemModel> result = new List<ParameterSystemModel>();
            if (!this.repository.GetParamSystem(ref result, 10, 0, ref message, "loan"))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<ParameterSystemModel>>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(IList<ParameterSystemModel>))]
        [Route("api/ParameterSystem/ParameterSystemLoan")]
        public HttpResponseMessage GetAllLoan(int? limit, int? index)
        {
            IList<ParameterSystemModel> result = new List<ParameterSystemModel>();
            if (!repository.GetParamSystem(ref result, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message, "loan"))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<IList<ParameterSystemModel>>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(ParameterSystemGrid))]
        [AcceptVerbs("GET", "POST")]
        [Route("api/ParameterSystem/ParameterSystemLoan")]
        public HttpResponseMessage GetLoan(int? page, int? size, string sort_column, string sort_order, IList<ParameterSystemFilter> filters)
        {
            ParameterSystemGrid result = new ParameterSystemGrid();
            if (!repository.GetParamSystem(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "ParameterType" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref result, ref message, "loan"))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<ParameterSystemGrid>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(IList<ParameterSystemModel>))]
        [HttpGet]
        [Route("api/ParameterSystem/ParameterSystemLoan/Search")]
        public HttpResponseMessage SearchLoan(string query, int? limit)
        {
            IList<ParameterSystemModel> result = new List<ParameterSystemModel>();

            if (!repository.GetParamSystem(query, limit.HasValue ? limit.Value : 10, ref result, ref message, "loan"))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<ParameterSystemModel>>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(ParameterSystemModel))]
        [Route("api/ParameterSystem/ParameterSystemLoan/{id}")]
        [HttpGet]
        public HttpResponseMessage GetByIdLoan(int id)
        {
            ParameterSystemModel result = new ParameterSystemModel();

            if (!repository.GetParamSystemByID(id, ref result, ref message, "loan"))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<ParameterSystemModel>(HttpStatusCode.OK, result);
            }
        }

        [HttpPost]
        [ResponseType(typeof(string))]
        [Route("api/ParameterSystem/ParameterSystemLoan")]
        public HttpResponseMessage PostLoan(ParameterSystemModel paramSystem)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddParamSystem(paramSystem, ref message, "loan"))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse<string>(HttpStatusCode.Created, "New Parameter System Loan data has been created.");
                }
            }
        }

        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/ParameterSystem/ParameterSystemLoan/{id}")]
        public HttpResponseMessage PutLoan(int id, ParameterSystemModel paramSystem)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateParamSystem(id, paramSystem, ref message, "loan"))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Parameter System Loan data has been updated.");
                }
            }
        }

        [HttpDelete]
        [Route("api/ParameterSystem/ParameterSystemLoan/{id}")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage DeleteLoan(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteParamSystem(id, ref message, "loan"))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse<string>(HttpStatusCode.OK, "Parameter System Loan data has been deleted.");
                }
            }
        }

        [ResponseType(typeof(StaticDropdownModel))]
        [Route("api/ParameterSystem/ParameterSystemLoan/SD")]
        [HttpGet]
        public HttpResponseMessage GetDropdownLoan()
        {
            IList<StaticDropdownModel> result = new List<StaticDropdownModel>();

            if (!repository.GetStaticDropdownByCategoryName(ref result, ref message, "loan"))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<IList<StaticDropdownModel>>(HttpStatusCode.OK, result);
            }
        }
        #endregion

        #region Fd
        [ResponseType(typeof(IList<ParameterSystemModel>))]
        [Route("api/ParameterSystem/ParameterSystemFd")]
        public HttpResponseMessage GetAllFd()
        {
            IList<ParameterSystemModel> result = new List<ParameterSystemModel>();
            if (!this.repository.GetParamSystem(ref result, 10, 0, ref message, "fd"))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<ParameterSystemModel>>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(IList<ParameterSystemModel>))]
        [Route("api/ParameterSystem/ParameterSystemFd")]
        public HttpResponseMessage GetAllFd(int? limit, int? index)
        {
            IList<ParameterSystemModel> result = new List<ParameterSystemModel>();
            if (!repository.GetParamSystem(ref result, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message, "fd"))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<IList<ParameterSystemModel>>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(ParameterSystemGrid))]
        [AcceptVerbs("GET", "POST")]
        [Route("api/ParameterSystem/ParameterSystemFd")]
        public HttpResponseMessage GetFd(int? page, int? size, string sort_column, string sort_order, IList<ParameterSystemFilter> filters)
        {
            ParameterSystemGrid result = new ParameterSystemGrid();
            if (!repository.GetParamSystem(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "ParameterType" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref result, ref message, "fd"))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<ParameterSystemGrid>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(IList<ParameterSystemModel>))]
        [HttpGet]
        [Route("api/ParameterSystem/ParameterSystemFd/Search")]
        public HttpResponseMessage SearchFd(string query, int? limit)
        {
            IList<ParameterSystemModel> result = new List<ParameterSystemModel>();

            if (!repository.GetParamSystem(query, limit.HasValue ? limit.Value : 10, ref result, ref message, "fd"))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<ParameterSystemModel>>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(ParameterSystemModel))]
        [Route("api/ParameterSystem/ParameterSystemFd/{id}")]
        [HttpGet]
        public HttpResponseMessage GetByIdFd(int id)
        {
            ParameterSystemModel result = new ParameterSystemModel();

            if (!repository.GetParamSystemByID(id, ref result, ref message, "fd"))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<ParameterSystemModel>(HttpStatusCode.OK, result);
            }
        }
        //27 Juli 2016 by dani
        [HttpGet]
        [ResponseType(typeof(string))]
        [Route("api/ParameterSystem/ParameterSystemFd/ExistingMaxMinRate/{parameter}")]
        public HttpResponseMessage GetFdMinMaxRate(string parameter)
        {
            string result;
            if (!repository.GetExistingFdMaxMinRate(parameter, ref message, "fd"))
            {
                result = "false";
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                result = "true";
                return Request.CreateResponse<string>(HttpStatusCode.OK, result);
            }
        }
        //dani end.

        [HttpPost]
        [ResponseType(typeof(string))]
        [Route("api/ParameterSystem/ParameterSystemFd")]
        public HttpResponseMessage PostFd(ParameterSystemModel paramSystem)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddParamSystem(paramSystem, ref message, "fd"))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse<string>(HttpStatusCode.Created, "New Parameter System FD data has been created.");
                }
            }
        }

        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/ParameterSystem/ParameterSystemFd/{id}")]
        public HttpResponseMessage PutFd(int id, ParameterSystemModel paramSystem)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateParamSystem(id, paramSystem, ref message, "fd"))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Parameter System FD data has been updated.");
                }
            }
        }

        [HttpDelete]
        [Route("api/ParameterSystem/ParameterSystemFd/{id}")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage DeleteFd(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteParamSystem(id, ref message, "fd"))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse<string>(HttpStatusCode.OK, "Parameter System FD data has been deleted.");
                }
            }
        }

        [ResponseType(typeof(StaticDropdownModel))]
        [Route("api/ParameterSystem/ParameterSystemFd/SD")]
        [HttpGet]
        public HttpResponseMessage GetDropdownFd()
        {
            IList<StaticDropdownModel> result = new List<StaticDropdownModel>();

            if (!repository.GetStaticDropdownByCategoryName(ref result, ref message, "fd"))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<IList<StaticDropdownModel>>(HttpStatusCode.OK, result);
            }
        }
        #endregion

        #region Ut
        [ResponseType(typeof(IList<ParameterSystemModel>))]
        [Route("api/ParameterSystem/ParameterSystemUt")]
        public HttpResponseMessage GetAllUt()
        {
            IList<ParameterSystemModel> result = new List<ParameterSystemModel>();
            if (!this.repository.GetParamSystem(ref result, 10, 0, ref message, "ut"))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<ParameterSystemModel>>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(IList<ParameterSystemModel>))]
        [Route("api/ParameterSystem/ParameterSystemUt")]
        public HttpResponseMessage GetAllUt(int? limit, int? index)
        {
            IList<ParameterSystemModel> result = new List<ParameterSystemModel>();
            if (!repository.GetParamSystem(ref result, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message, "ut"))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<IList<ParameterSystemModel>>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(ParameterSystemGrid))]
        [AcceptVerbs("GET", "POST")]
        [Route("api/ParameterSystem/ParameterSystemUt")]
        public HttpResponseMessage GetUt(int? page, int? size, string sort_column, string sort_order, IList<ParameterSystemFilter> filters)
        {
            ParameterSystemGrid result = new ParameterSystemGrid();
            if (!repository.GetParamSystem(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "ParameterType" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref result, ref message, "ut"))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<ParameterSystemGrid>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(IList<ParameterSystemModel>))]
        [HttpGet]
        [Route("api/ParameterSystem/ParameterSystemUt/Search")]
        public HttpResponseMessage SearchUt(string query, int? limit)
        {
            IList<ParameterSystemModel> result = new List<ParameterSystemModel>();

            if (!repository.GetParamSystem(query, limit.HasValue ? limit.Value : 10, ref result, ref message, "ut"))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<ParameterSystemModel>>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(ParameterSystemModel))]
        [Route("api/ParameterSystem/ParameterSystemUt/{id}")]
        [HttpGet]
        public HttpResponseMessage GetByIdUt(int id)
        {
            ParameterSystemModel result = new ParameterSystemModel();

            if (!repository.GetParamSystemByID(id, ref result, ref message, "ut"))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<ParameterSystemModel>(HttpStatusCode.OK, result);
            }
        }

        [HttpPost]
        [ResponseType(typeof(string))]
        [Route("api/ParameterSystem/ParameterSystemUt")]
        public HttpResponseMessage PostUt(ParameterSystemModel paramSystem)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddParamSystem(paramSystem, ref message, "ut"))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse<string>(HttpStatusCode.Created, "New Parameter System UT data has been created.");
                }
            }
        }

        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/ParameterSystem/ParameterSystemUt/{id}")]
        public HttpResponseMessage PutUt(int id, ParameterSystemModel paramSystem)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateParamSystem(id, paramSystem, ref message, "ut"))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Parameter System UT data has been updated.");
                }
            }
        }

        [HttpDelete]
        [Route("api/ParameterSystem/ParameterSystemUt/{id}")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage DeleteUt(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteParamSystem(id, ref message, "ut"))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse<string>(HttpStatusCode.OK, "Parameter System UT data has been deleted.");
                }
            }
        }

        [ResponseType(typeof(StaticDropdownModel))]
        [Route("api/ParameterSystem/ParameterSystemUt/SD")]
        [HttpGet]
        public HttpResponseMessage GetDropdownUt()
        {
            IList<StaticDropdownModel> result = new List<StaticDropdownModel>();

            if (!repository.GetStaticDropdownByCategoryName(ref result, ref message, "ut"))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<IList<StaticDropdownModel>>(HttpStatusCode.OK, result);
            }
        }
        #endregion

        #region Cif
        [ResponseType(typeof(ParsysModel))]
        [Route("api/ParameterSystem/retailcif")]
        [HttpGet]
        public HttpResponseMessage GetAllParsysRetailCIF(string select = "CIFMaintencanceParam")
        {
            IDictionary<string, object> output = new Dictionary<string, object>();
            string messages = string.Empty;
            if (!string.IsNullOrEmpty(select))
            {
                string[] val = select.Split(',');
                foreach (string str in val)
                {
                    switch (str.ToLower())
                    {
                        case "cifmaintencanceparam":
                            IList<GeneralParameterModel> retailCifParSys = new List<GeneralParameterModel>();
                            if (!repoGeneralParam.GetAllParameterSystem(ref retailCifParSys, ref message))
                            {
                                message += string.Concat("Error Loan Parameter System : ", message);
                            }
                            else
                            {
                                output.Add("CIF_JENIS_IDENTITAS", retailCifParSys.Where(p => p.StringKey == "CIF_JENIS_IDENTITAS"));
                                output.Add("CIF_SUMBER_DANA", retailCifParSys.Where(p => p.StringKey == "CIF_SUMBER_DANA"));
                                output.Add("CIF_ASSET_BERSIH", retailCifParSys.Where(p => p.StringKey == "CIF_ASSET_BERSIH"));
                                output.Add("CIF_PENDAPATAN_BULANAN", retailCifParSys.Where(p => p.StringKey == "CIF_PENDAPATAN_BULANAN"));
                                output.Add("CIF_PENGHASILAN_TAMBAHAN", retailCifParSys.Where(p => p.StringKey == "CIF_PENGHASILAN_TAMBAHAN"));
                                output.Add("CIF_PEKERJAAN", retailCifParSys.Where(p => p.StringKey == "CIF_PEKERJAAN"));
                                output.Add("CIF_TUJUAN_PEMBUKAAN_REKENING", retailCifParSys.Where(p => p.StringKey == "CIF_TUJUAN_PEMBUKAAN_REKENING"));
                                output.Add("CIF_PERKIRAAN_DANA_MASUK", retailCifParSys.Where(p => p.StringKey == "CIF_PERKIRAAN_DANA_MASUK"));
                                output.Add("CIF_PERKIRAAN_DANA_KELUAR", retailCifParSys.Where(p => p.StringKey == "CIF_PERKIRAAN_DANA_KELUAR"));
                                output.Add("CIF_PERKIRAAN_TRANSAKSI_KELUAR", retailCifParSys.Where(p => p.StringKey == "CIF_PERKIRAAN_TRANSAKSI_KELUAR"));
                                output.Add("CIF_STAFF_TAGGING", retailCifParSys.Where(p => p.StringKey == "CIF_STAFF_TAGGING"));
                            }
                            retailCifParSys = null;
                            break;


                        default:
                            break;
                    }
                }
            }
            if (output == null)
                return Request.CreateResponse(HttpStatusCode.NoContent);
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, output);
            }
        }
        [ResponseType(typeof(IList<ParameterSystemModel>))]
        [Route("api/ParameterSystem/ParameterSystemCif")]
        public HttpResponseMessage GetAllCif()
        {
            IList<ParameterSystemModel> result = new List<ParameterSystemModel>();
            if (!this.repository.GetParamSystem(ref result, 10, 0, ref message, "cif"))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<ParameterSystemModel>>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(IList<ParameterSystemModel>))]
        [Route("api/ParameterSystem/ParameterSystemCif")]
        public HttpResponseMessage GetAllCif(int? limit, int? index)
        {
            IList<ParameterSystemModel> result = new List<ParameterSystemModel>();
            if (!repository.GetParamSystem(ref result, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message, "cif"))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<IList<ParameterSystemModel>>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(ParameterSystemGrid))]
        [AcceptVerbs("GET", "POST")]
        [Route("api/ParameterSystem/ParameterSystemCif")]
        public HttpResponseMessage GetCif(int? page, int? size, string sort_column, string sort_order, IList<ParameterSystemFilter> filters)
        {
            ParameterSystemGrid result = new ParameterSystemGrid();
            if (!repository.GetParamSystem(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "ParameterType" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref result, ref message, "cif"))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<ParameterSystemGrid>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(IList<ParameterSystemModel>))]
        [HttpGet]
        [Route("api/ParameterSystem/ParameterSystemCif/Search")]
        public HttpResponseMessage SearchCif(string query, int? limit)
        {
            IList<ParameterSystemModel> result = new List<ParameterSystemModel>();

            if (!repository.GetParamSystem(query, limit.HasValue ? limit.Value : 10, ref result, ref message, "cif"))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<ParameterSystemModel>>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(ParameterSystemModel))]
        [Route("api/ParameterSystem/ParameterSystemCif/{id}")]
        [HttpGet]
        public HttpResponseMessage GetByIdCif(int id)
        {
            ParameterSystemModel result = new ParameterSystemModel();

            if (!repository.GetParamSystemByID(id, ref result, ref message, "cif"))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<ParameterSystemModel>(HttpStatusCode.OK, result);
            }
        }

        [HttpPost]
        [ResponseType(typeof(string))]
        [Route("api/ParameterSystem/ParameterSystemCif")]
        public HttpResponseMessage PostCif(ParameterSystemModel paramSystem)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddParamSystem(paramSystem, ref message, "cif"))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse<string>(HttpStatusCode.Created, "New Parameter System CIF data has been created.");
                }
            }
        }

        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/ParameterSystem/ParameterSystemCif/{id}")]
        public HttpResponseMessage PutCif(int id, ParameterSystemModel paramSystem)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateParamSystem(id, paramSystem, ref message, "cif"))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Parameter System CIF data has been updated.");
                }
            }
        }

        [HttpDelete]
        [Route("api/ParameterSystem/ParameterSystemCif/{id}")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage DeleteCif(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteParamSystem(id, ref message, "cif"))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse<string>(HttpStatusCode.OK, "Parameter System CIF data has been deleted.");
                }
            }
        }

        [ResponseType(typeof(StaticDropdownModel))]
        [Route("api/ParameterSystem/ParameterSystemCif/SD")]
        [HttpGet]
        public HttpResponseMessage GetDropdownCif()
        {
            IList<StaticDropdownModel> result = new List<StaticDropdownModel>();

            if (!repository.GetStaticDropdownByCategoryName(ref result, ref message, "cif"))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<IList<StaticDropdownModel>>(HttpStatusCode.OK, result);
            }
        }
        #endregion

        [ResponseType(typeof(StaticDropdownModel))]
        [Route("api/ParameterSystem/SD")]
        [HttpGet]
        public HttpResponseMessage GetDropdown()
        {
            IList<StaticDropdownModel> result = new List<StaticDropdownModel>();

            if (!repository.GetStaticDropdownByCategoryName(ref result, ref message, "system"))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<IList<StaticDropdownModel>>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(ParameterSystemGrid))]
        [AcceptVerbs("GET", "POST")]
        [Route("api/ParameterSystem")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<ParameterSystemFilter> filters)
        {
            ParameterSystemGrid result = new ParameterSystemGrid();
            if (!repository.GetParamSystem(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "ParameterType" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref result, ref message, "system"))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<ParameterSystemGrid>(HttpStatusCode.OK, result);
            }
        }
        [ResponseType(typeof(ParsysModel))]
        [Route("api/ParameterSystem/partype/{parametertype}")]
        [HttpGet]
        public HttpResponseMessage GetByProductID(string parametertype)
        {
            IList<ParsysModel> outputRet = new List<ParsysModel>();
            IDictionary<string, object> output = new Dictionary<string, object>();

            if (!repository.GetParsysByCategory(parametertype, ref outputRet, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                output.Add("Parsys", outputRet);
            }
            if (output == null)
                return Request.CreateResponse(HttpStatusCode.NoContent);
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, output);
            }
        }
        [ResponseType(typeof(TransactionFDDraftModel))]
        [Route("api/ParameterSystem/Parsys/{type}")]
        [HttpGet]
        public HttpResponseMessage GetDraft(string type)
        {
            string partipe = string.Empty;
            string parval = string.Empty;
            if (!repository.GetSingleParys(type, ref partipe, ref parval, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.Created, new
                {
                    Name = partipe,
                    Value = parval
                });
            }
        }
        [ResponseType(typeof(CutOffModel))]
        [Route("api/ParameterSystem/IsCutOff/{type}")]
        [HttpGet]
        public HttpResponseMessage IsCutOff(string type)
        {
            CutOffModel cutoff = new CutOffModel();
            if (!repository.GetIsCuttOff(type, ref cutoff, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, cutoff);
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}