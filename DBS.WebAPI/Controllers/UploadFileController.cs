﻿using DBS.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class UploadFileController : ApiController
    {
        private string message = string.Empty;

        private readonly IUploadFileRepository repository = null;

        public UploadFileController()
        {
            this.repository = new UploadFileRepository();
        }

        public UploadFileController(IUploadFileRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Upload File.
        /// </summary>
        /// <param name="path">path.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(UploadFileModel model)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UploadFile(model, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "Data successfully uploaded." });
                }
            }
        }
    }
}