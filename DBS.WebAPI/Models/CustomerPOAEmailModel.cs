﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Web;
using DBS.Entity;
using DBS.WebAPI.Models;
using DBS.WebAPI.Helpers;

namespace DBS.WebAPI.Models
{
    #region Model
    public class CustomerPOAEmailModel
    {
        public int CustomerPOAEmailID { get; set; }
        public string POAEmail { get; set; }
        public string POAEmailOld { get; set; }
        public int? POAEmailProductID { get; set; }
        public string CIF { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }

    }
    public class CustomerPOAEmailModelFilter
    {
        public int CustomerPOAEmailID { get; set; }
        public string POAEmail { get; set; }
        public int? ProductID { get; set; }
        public List<POAEmailProductModel> POAEmailProduct { get; set; }
        public string CIF { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
    }

    public class CustomerPOAEmailGrid : Grid
    {
        public IList<CustomerPOAEmailRow> Rows { get; set; }
    }
    public class CustomerPOAEmailRow : CustomerPOAEmailModelFilter
    {
        public int RowID { get; set; }
    }
    public class POAEmailProductModel
    {
        public int POAEmailProductID { get; set; }
        public string ProductName { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        public string CreateBy { get; set; }
        public bool IsChecked { get; set; }
    }

    public class JSONSentModel
    {
        public List<CustomerPOAEmailModel> POAEmail { get; set; }
        public string Email { get; set; }
    }
    public class JSONSentUpdateModel
    {
        public List<CustomerPOAEmailModel> POAEmail { get; set; }
        public string Email { get; set; }
        public string OldEmail { get; set; }
    }
    public class JSONSentDeleteModel
    {
        public string Email { get; set; }
        public string CIF { get; set; }
        public List<CustomerPOAEmailModel> POAEmail { get; set; }

    }
    #endregion

    #region Filter
    public class CustomerPOAEmailFilter : Filter
    {
        //public string Field { get; set; }
        //public string Value { get; set; }
    }
    #endregion

    public interface ICustomerPOAEmailRepository : IDisposable
    {
        bool GetMasterPOAEmailProduct(ref IList<POAEmailProductModel> result, ref string message);
        bool AddCustomerPOAEmailWF(List<CustomerPOAEmailModel> POAEmail, string Email, ref string message);
        bool UpdateCustomerPOAEmailWF(string OldEmail, string Email, List<CustomerPOAEmailModel> POAEmail, ref string message);
        bool DeleteCustomerPOAEmailWF(string CIF, string Email, List<CustomerPOAEmailModel> POAEmail, ref string message);
        bool GetPOAEmailByID(int id, ref CustomerPOAEmailModel POAEmail, ref string message);
        bool GetCustomerPOAEmailGrid(string CIF, int page, int size, IList<CustomerPOAEmailFilter> filters, string sortColumn, string sortOrder, ref CustomerPOAEmailGrid CustomerPOAEmail, ref string message);
        bool GetDataCustomerPOAEmail(string CIF, ref IList<CustomerPOAEmailModel> CustomerPOAEmail, ref string message);
        bool GetSP(string cif, int page, int size, IList<WorkflowEntityFilter> filters, string sortColumn, string sortOrder, ref WorkflowEntityGrid roleGrid, ref string message);
    }

    public class CustomerPOAEmailRepository : ICustomerPOAEmailRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();
        public bool GetCustomerPOAEmailGrid(string CIF, int page, int size, IList<CustomerPOAEmailFilter> filters, string sortColumn, string sortOrder, ref CustomerPOAEmailGrid CustomerPOAEmail, ref string message)
        {
            bool isSuccess = false;
            try
            {
                int skip = (page - 1) * size;
                string orderBy = string.Format("UpdateDate DESC,{0} {1}", sortColumn, sortOrder);
                CustomerPOAEmailModelFilter filter = new CustomerPOAEmailModelFilter();

                if (filters != null)
                {
                    filter.POAEmail = filters.Where(a => a.Field.Equals("POAEmail")).Select(a => a.Value).SingleOrDefault();
                    filter.UpdateBy = filters.Where(a => a.Field.Equals("UpdateBy")).Select(a => a.Value).SingleOrDefault();
                    filter.CreateBy = filters.Where(a => a.Field.Equals("CreateBy")).Select(a => a.Value).SingleOrDefault();
                    filter.UpdateDate = DateTime.Parse(filters.Where(a => a.Field.Equals("UpdateDate")).Select(a => a.Value).SingleOrDefault());
                }
                List<CustomerPOAEmailModelFilter> POAEMailCollected = new List<CustomerPOAEmailModelFilter>();

                var POAEmail = context.CustomerPOAEmails.Where(a => a.CIF == CIF).ToList();
                //var POAEmailProduct = context.Customer.Where(a => a.CIF == CIF).ToList();
                foreach (var item in POAEmail)
                {
                    CustomerPOAEmailModelFilter ex = new CustomerPOAEmailModelFilter();
                    ex.POAEmail = item.POAEmail;
                    ex.ProductID = item.POAEmailProductID;
                    ex.UpdateBy = item.UpdateBy;
                    ex.UpdateDate = item.UpdateDate;
                    ex.CreateBy = item.CreateBy;
                    POAEMailCollected.Add(ex);
                }

                CustomerPOAEmail.Page = page;
                CustomerPOAEmail.Size = size;
                CustomerPOAEmail.Total = POAEMailCollected.Count();
                CustomerPOAEmail.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                CustomerPOAEmail.Rows = POAEMailCollected.AsQueryable().OrderBy(orderBy).Select((a, i) => new CustomerPOAEmailRow
                {
                    RowID = i + 1,
                    POAEmail = a.POAEmail,
                    ProductID = a.ProductID,
                    UpdateBy = a.UpdateBy,
                    UpdateDate = a.UpdateDate,
                    CreateBy = a.CreateBy
                })
                .Skip(skip)
                .Take(size)
                .Distinct()
                .ToList();

                isSuccess = true;
            }

            catch (Exception e)
            {
                message = e.Message;
            }

            return isSuccess;
        }
        public bool GetMasterPOAEmailProduct(ref IList<POAEmailProductModel> result, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                result = context.POAEmailProducts.Where(a => a.Show == true).Select(a => new POAEmailProductModel
                {
                    POAEmailProductID = a.POAEmailProductID,
                    ProductName = a.ProductName,
                    IsDeleted = a.IsDeleted,
                    CreateDate = a.CreateDate,
                    UpdateDate = a.UpdateDate,
                    UpdateBy = a.UpdateBy,
                    CreateBy = a.CreateBy,
                    IsChecked = false
                }).ToList();


                IsSuccess = true;
            }
            catch (Exception e)
            {
                message = e.Message;
            }


            return IsSuccess;
        }
        public bool GetDataCustomerPOAEmail(string CIF, ref IList<CustomerPOAEmailModel> CustomerPOAEmail, ref string message)
        {
            var isSuccess = false;
            try
            {
                CustomerPOAEmail = context.CustomerPOAEmails.Where(a => a.CIF == CIF).Select(a => new CustomerPOAEmailModel
                {
                    CustomerPOAEmailID = a.CustomerPOAEmailID,
                    POAEmail = a.POAEmail,
                    POAEmailOld = a.POAEmail,
                    POAEmailProductID = a.POAEmailProductID,
                    CIF = a.CIF,
                    UpdateDate = a.UpdateDate,
                    UpdateBy = a.UpdateBy,
                    CreateDate = a.CreateDate,
                    CreateBy = a.CreateBy

                }).ToList();
                isSuccess = true;
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            return isSuccess;
        }
        public bool AddCustomerPOAEmailWF(List<CustomerPOAEmailModel> POAEmail, string Email, ref string message)
        {
            bool isSuccess = false;

            try
            {
                DateTime createDate = DateTime.UtcNow;
                string currentUserTemp = currentUser.GetCurrentUser().DisplayName;

                if (POAEmail != null)
                {

                    foreach (var item in POAEmail)
                    {
                        CustomerPOAEmail xx = new CustomerPOAEmail();
                        xx.CustomerPOAEmailID = item.CustomerPOAEmailID;
                        xx.POAEmail = Email;
                        xx.POAEmailProductID = item.POAEmailProductID;
                        xx.CIF = item.CIF;
                        xx.UpdateDate = item.UpdateDate;
                        xx.UpdateBy = item.UpdateBy;
                        xx.CreateDate = createDate;
                        xx.CreateBy = currentUserTemp;
                        context.CustomerPOAEmails.Add(xx);
                    }
                    context.SaveChanges();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool DeleteCustomerPOAEmailWF(string CIF, string Email, List<CustomerPOAEmailModel> POAEmail, ref string message)
        {
            bool isSuccess = false;

            try
            {
                List<CustomerPOAEmail> data = context.CustomerPOAEmails.Where(a => a.CIF == CIF && a.POAEmail == Email).ToList();

                if (data != null)
                {

                    context.CustomerPOAEmails.RemoveRange(data);

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Customer data for POA Email is does not exist.", Email);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool UpdateCustomerPOAEmailWF(string OldEmail, string Email, List<CustomerPOAEmailModel> POAEmail, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Email = Email.Trim().ToLower();
                DateTime createDate = DateTime.UtcNow;
                string currentUserTemp = currentUser.GetCurrentUser().DisplayName;
                var oldData = context.CustomerPOAEmails.Where(a => a.POAEmail == Email).ToList();
                var createdByOld = oldData[0].CreateBy != null ? oldData[0].CreateBy : oldData[0].UpdateBy;
                var createdDateOld = oldData[0].CreateDate != null ? oldData[0].CreateDate : oldData[0].UpdateDate;
                var CIF = POAEmail[0].CIF.Trim();
                context.CustomerPOAEmails.RemoveRange(context.CustomerPOAEmails.Where(a => a.CIF.Trim() == CIF && a.POAEmail.Trim().ToLower() == Email).ToList());

                foreach (var data in POAEmail)
                {
                    CustomerPOAEmail NewData = new CustomerPOAEmail();
                    NewData.POAEmail = Email;
                    NewData.POAEmailProductID = data.POAEmailProductID;
                    NewData.CIF = data.CIF;
                    NewData.UpdateDate = createDate;
                    NewData.UpdateBy = currentUserTemp;
                    NewData.CreateDate = createdDateOld;
                    NewData.CreateBy = createdByOld;
                    context.CustomerPOAEmails.Add(NewData);
                    context.SaveChanges();

                }

                isSuccess = true;

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool GetPOAEmailByID(int id, ref CustomerPOAEmailModel POAEmail, ref string message)
        {
            bool isSuccess = false;

            try
            {
                CustomerPOAEmail CustomerPOAEmail = context.CustomerPOAEmails.Where(a => a.CustomerPOAEmailID == id).SingleOrDefault();
                CustomerPOAEmailModel aa = new CustomerPOAEmailModel();

                if (CustomerPOAEmail != null)
                {
                    POAEmail.POAEmailProductID = CustomerPOAEmail.POAEmailProductID;
                    POAEmail.CustomerPOAEmailID = CustomerPOAEmail.CustomerPOAEmailID;
                    POAEmail.POAEmail = CustomerPOAEmail.POAEmail;
                    POAEmail.CIF = CustomerPOAEmail.CIF;
                    POAEmail.UpdateDate = CustomerPOAEmail.UpdateDate;
                    POAEmail.UpdateBy = CustomerPOAEmail.UpdateBy;
                    POAEmail.CreateDate = CustomerPOAEmail.CreateDate;
                    POAEmail.CreateBy = CustomerPOAEmail.CreateBy;
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        private IQueryable<WorkflowEntityModel> GetSPAllJoin(DBSEntities context, string EntityName = null, string filterCIF = null)
        {
            List<WorkflowEntity> filterEntity = new List<WorkflowEntity>();
            string cifValue = null;
            if (EntityName != null && EntityName.Equals("CustomerPOAEmail"))
            {
                if (filterCIF != null)
                {
                    cifValue = filterCIF;
                }
            }
            string userName = currentUser.GetCurrentUser().LoginName;
            var data = (from a in context.SP_GetTaskWorkflow(EntityName, cifValue, null, null, null, null)
                        select new WorkflowEntityModel
                        {
                            ID = a.ID,
                            EntityName = a.EntityName,
                            TaskName = a.TaskName,
                            ActionType = a.ActionType,
                            //Initiator = a.,
                            //SPListItemID = a.SPListItemID,
                            //SPTaskListID = (Guid)a.SPTaskListID,
                            //SPTaskListItemID = (int)a.SPTaskListItemID,
                            WorkflowID = (Guid)a.WorkflowID,
                            LastModifiedDate = a.CreateDate,
                            LastModifiedBy = a.CreateBy,
                            Data = a.Data
                        }).ToList();

            return data.AsQueryable();
        }
        public bool GetSP(string cif, int page, int size, IList<WorkflowEntityFilter> filters, string sortColumn, string sortOrder, ref WorkflowEntityGrid roleGrid, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                WorkflowEntityModel filter = new WorkflowEntityModel();
                if (filters != null)
                {
                    filter.EntityName = (string)filters.Where(a => a.Field.Equals("EntityName")).Select(a => a.Value).SingleOrDefault();
                    filter.TaskName = (string)filters.Where(a => a.Field.Equals("TaskName")).Select(a => a.Value).SingleOrDefault();
                    filter.ActionType = (string)filters.Where(a => a.Field.Equals("Action")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
                    filter.FilterValue = (string)filters.Where(a => a.Field.Equals("FilterValue")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = from a in GetSPAllJoin(context, "CustomerPOAEmail", cif)
                               select a;
                    roleGrid.Page = page;
                    roleGrid.Size = size;
                    roleGrid.Total = data.Count();
                    roleGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    roleGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new WorkflowEntityRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            EntityName = a.EntityName,
                            TaskName = a.TaskName,
                            ActionType = a.ActionType,
                            Data = a.Data,
                            SPTaskListID = a.SPTaskListID,
                            SPTaskListItemID = a.SPTaskListItemID,
                            Initiator = a.Initiator,
                            WorkflowID = (Guid)a.WorkflowID == null ? Guid.Empty : (Guid)a.WorkflowID,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }



        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                        context.Dispose();
                }
            }
            this.disposed = true;
        }

        void IDisposable.Dispose()
        {

            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}