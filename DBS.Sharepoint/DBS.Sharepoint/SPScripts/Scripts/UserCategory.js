﻿function Comma(Num) { //function to add commas to textboxes
    Num += '';
    Num = Num.replace(',', ''); Num = Num.replace(',', ''); Num = Num.replace(',', '');
    Num = Num.replace(',', ''); Num = Num.replace(',', ''); Num = Num.replace(',', '');
    x = Num.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1))
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    return x1 + x2;
}


var ViewModel = function () {
    //Make the self as 'this' reference
    var self = this;
    var ProductModel = function (id, name) {
        var self = this;
        self.ID = ko.observable(id);
        self.Name = ko.observable(name);
    }
    var CCYModel = function (id, name) {
        var self = this;
        self.ID = ko.observable(id);
        self.Code = ko.observable(name);
    }


    self.Readonly = ko.observable(false);
    self.IsWorkflow = ko.observable(false);

    //Declare observable which will be bind with UI
   
    self.UserCategoryID = ko.observable(0);
    self.UserCategoryCode = ko.observable("");
    self.MaxTransaction = ko.observable(0);
    self.MinTransaction = ko.observable(0);
    self.Product = ko.observable(new ProductModel('', ''));
    self.Currency = ko.observable(new CCYModel('', ''));

    self.ddlProductName = ko.observableArray([]);
    self.ddlCCYName = ko.observableArray([]);
    self.ddlUserGroup = [
        new optionModel(1, "BranchChecker"),
        new optionModel(2, "PaymentChecker")
    ];
    self.UserGroup = ko.observable("");
    self.Unlimited = ko.observable(false);

    self.ModifiedBy = ko.observable("");
    self.ModifiedDate = ko.observable("");
    self.IsRoleMaker = ko.observable(false);
    self.IsPermited = ko.observable(false);
    // filter    
    self.FilterUserCategoryCode = ko.observable("");
    self.FilterMaxTransaction = ko.observable("");
    self.FilterMinTransaction = ko.observable("");
    self.FilterProduct = ko.observable("");
    self.FilterCurrency = ko.observable("");
    self.FilterModifiedBy = ko.observable("");
    self.FilterModifiedDate = ko.observable("");
    self.FilterUserGroup = ko.observable("");
    self.FilterUnlimited = ko.observable("");
  
    // New Data flag
    self.IsNewData = ko.observable(false);

    // Declare an ObservableArray for Storing the JSON Response
    self.UserCategories = ko.observableArray([]);

    // grid properties
    self.GridProperties = ko.observable();
    self.GridProperties(new GridPropertiesModel(GetData));

    // set default sorting
    self.GridProperties().SortColumn("UserCategoryCode");
    self.GridProperties().SortOrder("ASC");

    // bind clear filters
    self.ClearFilters = function () {
        self.FilterUserCategoryCode("");
        self.FilterMaxTransaction("");
        self.FilterProduct("");
        self.FilterCurrency("");
        self.FilterModifiedBy("");
        self.FilterModifiedDate("");
        self.FilterUserGroup("");
        self.FilterUnlimited("");
        GetData();
    };

    // bind dropdown
    self.GetDropdown = function () {
        GetDropdown();
    };

    self.GetData = function () {
        GetData();
    };

    var UserCategory = {
        UserCategoryID: self.UserCategoryID,
        UserCategoryCode: self.UserCategoryCode,
        MaxTransaction: self.MaxTransaction,
        MinTransaction: self.MinTransaction,
        Product: self.Product,
        Currency: self.Currency,
        ModifiedBy: self.ModifiedBy,
        ModifiedDate: self.ModifiedDate,
        UserGroup: self.UserGroup,
        Unlimited: self.Unlimited
    };
    self.IsPermited = function (IsPermitedResult) {
        //Ajax call to delete the Customer
        spUser = $.cookie(api.cookie.spUser);

        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/CheckUserPermited/" + _spFriendlyUrlPageContextInfo.title,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    //compare user role to page permission to get checker validation                    
                    for (i = 0; i < spUser.Roles.length; i++) {
                        for (j = 0; j < data.length; j++) {
                            if (Const_RoleName[spUser.Roles[i].ID] == data[j]) { //if (spUser.Roles[i].Name == data[j]) {
                                if (Const_RoleName[spUser.Roles[i].ID].endsWith('Maker')) { //if (spUser.Roles[i].Name.endsWith('Maker')) {
                                    self.IsRoleMaker(true);
                                    return;
                                }
                                else {
                                    self.IsRoleMaker(false);
                                }
                            }
                        }
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    self.save = function () {
        // validation
        var form = $("#aspnetForm");
        var ProductName = $('#product option:selected').text();
        var CurrencyName = $('#ccy option:selected').text();
        var UserGroup = $("#UserGroup option:selected").text();
        var dataCount = self.UserCategories();
        var matchValue = '';
        var errorisNaNMaxTrx = '';
        var errorisNaNMinTrx = '';

        var vMaxTrx = document.getElementById('maxTrx').value;
        var vMinTrx = document.getElementById('minTrx').value;

        var resMax = vMaxTrx.replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "");
        var resMin = vMinTrx.replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "");
        self.MaxTransaction(resMax);
        self.MinTransaction(resMin);

        for (x = 0; x < dataCount.length; x++) {
            if (dataCount[x].UserCategoryCode = self.UserCategoryCode() && dataCount[x].Product.Name == ProductName && dataCount[x].UserGroup == UserGroup) {
                matchValue = 'duplicate';
            }
        }

        form.validate();

        if (form.valid()) {
            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {
                    //Ajax call to insert the Branchs
                    $.ajax({
                        type: "POST",
                        url: api.server + api.url.usercategory,
                        data: ko.toJSON(UserCategory), //Convert the Observable Data into JSON
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {
                                // send notification
                                ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // hide current popup window
                                $("#modal-form").modal('hide');

                                // refresh data
                                GetData();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-success', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-success', true);
                        }
                    });
                }
                $("#modal-form").modal('hide');
            });
        }
    };

    self.update = function () {
        // validation
        var form = $("#aspnetForm");
        var ProductName = $('#product option:selected').text();
        var CurrencyName = $('#ccy option:selected').text();
        var UserGroup = $("#UserGroup option:selected").text();
        var dataCount = self.UserCategories();
        var matchValue = '';
        var errorisNaNMaxTrx = '';
        var errorisNaNMinTrx = '';

        var vMaxTrx = document.getElementById('maxTrx').value;
        var vMinTrx = document.getElementById('minTrx').value;

        var resMax = vMaxTrx.replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "");
        var resMin = vMinTrx.replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "");
        self.MaxTransaction(resMax);
        self.MinTransaction(resMin);

        for (x = 0; x < dataCount.length; x++) {
            if (dataCount[x].UserCategoryCode = self.UserCategoryCode() && dataCount[x].Product.Name == ProductName && dataCount[x].UserGroup == UserGroup) {
                matchValue = 'duplicate';
            }
        }

        if (form.valid()) {
            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {

                    //Ajax call to update the Customer
                    $.ajax({
                        type: "PUT",
                        url: api.server + api.url.usercategory + "/" + UserCategory.UserCategoryID(),
                        data: ko.toJSON(UserCategory),
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {

                            if (jqXHR.status = 200) {
                                // send notification
                                ShowNotification('Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // hide current popup window
                                $("#modal-form").modal('hide');

                                // refresh data
                                GetData();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-success', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-success', true);
                        }
                    });
                }
                $("#modal-form").modal('hide');
            });
        }
    };

    self.delete = function () {
        // hide current popup window
        //$("#modal-form").modal('hide');
        var vMaxTrx = document.getElementById('maxTrx').value;
        var vMinTrx = document.getElementById('minTrx').value;

        var resMax = vMaxTrx.replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "");
        var resMin = vMinTrx.replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "").replace(",", "");
        self.MaxTransaction(resMax);
        self.MinTransaction(resMin);

        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                $("#modal-form").modal('show');
            } else {
                //Ajax call to delete the Customer
                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.usercategory + "/" + UserCategory.UserCategoryID(),
                    data: ko.toJSON(UserCategory),
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if (jqXHR.status = 200) {
                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                            // refresh data
                            GetData();
                        } else
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
            }
        });
    };
    // Local Date
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return "";
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-18
    };
    //Function to Display record to be updated
    self.GetSelectedRow = function (data) {
        self.IsNewData(false);

        if (self.IsWorkflow()) $("#modal-form-workflow").modal('show');
        else $("#modal-form").modal('show');

        self.UserCategoryID(data.UserCategoryID);
        self.UserCategoryCode(data.UserCategoryCode);
        self.MaxTransaction(formatNumber(data.MaxTransaction));
        self.MinTransaction(formatNumber(data.MinTransaction));
        self.Product(new ProductModel(data.Product.ID, data.Product.Name));
        self.Currency(new CCYModel(data.Currency.ID, data.Currency.Code));
        self.ModifiedBy(data.ModifiedBy);
        self.ModifiedDate(data.ModifiedDate);
        var modeNameID = 0;
        if (data.UserGroup == 'BranchChecker') {
            modeNameID = 1;
        }
        else {
            modeNameID = 2;
        }
        self.UserGroup(modeNameID);
        self.Unlimited(data.Unlimited);
    };

    //insert new
    self.NewData = function () {
        // flag as new Data
        self.IsNewData(true);
        self.Readonly(false);
        // bind empty data
        self.UserCategoryID(0);
        self.UserCategoryCode('')
        self.MaxTransaction(0)
        self.MinTransaction(0)
        self.Unlimited(false)
        self.Product(new ProductModel('', ''));
        self.Currency(new CCYModel('13', 'IDR'));
        self.ModifiedBy,
        self.ModifiedDate
        self.UserGroup('')
    };

    function GetData() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.usercategory,
            params: {
                page: self.GridProperties().Page(),
                size: self.GridProperties().Size(),
                sort_column: self.GridProperties().SortColumn(),
                sort_order: self.GridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetData, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetData, OnError, OnAlways);
        }
    }

    function GetDropdown() {
        var url = api.server + api.url.parameter + '?select=Product,Currency';

        $.ajax({
            async: false,
            type: "GET",
            url: url,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },

            success: function (dataValue, textStatus, jqXHR) {
                if (jqXHR.status = 200) {                  
                    if (dataValue['Product'] != null) {
                        self.ddlProductName(dataValue['Product']);
                        self.ddlCCYName(dataValue['Currency']);
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);

                }
            },

            error: function (jqXHR, textStatus, errorThrown) {

                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }

        });

    }

    self.SetMaxUnlimited = ko.computed(function () {
        if (self.Unlimited()) {
            self.MaxTransaction(formatNumber(0));
        }
    }); 

    self.OnChangeMode = function () {      
        var modeNameID = $("#UserGroup option:selected").text();
        self.UserGroup(modeNameID);
    };
    self.OnChangeModeProduct = function () {
        var ProductName = $('#product option:selected').text();
        self.Product().Name(ProductName);
    };
    self.OnChangeModeCCY = function () {
        var CurrencyName = $('#ccy option:selected').text();
        self.Currency().Code(CurrencyName);
    };

    function IsvalidField() { /*
     $("[name^=days]").each(function () {
     $(this).rules('add', {
     required: true,
     maxlength: 2,
     number: true,
     messages: {
     required: "Please provide a valid Day.",
     maxlength: "Please provide maximum 2 number valid day value.",
     number: "Please provide number"
     }
     });
     }); */
        return true;
    }

    // Get filtered columns value
    function GetFilteredColumns() {
        // define filter
        var filters = [];

        if (self.FilterUserCategoryCode() != "") filters.push({ Field: 'UserCategoryCode', Value: self.FilterUserCategoryCode() });
        if (self.FilterMaxTransaction() != "") filters.push({ Field: 'MaxTransaction', Value: self.FilterMaxTransaction() });
        if (self.FilterMinTransaction() != "") filters.push({ Field: 'MinTransaction', Value: self.FilterMinTransaction() });
        if (self.FilterProduct() != "") filters.push({ Field: 'Product', Value: self.FilterProduct() });
        if (self.FilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.FilterCurrency() });
        if (self.FilterModifiedBy() != "") filters.push({ Field: 'ModifiedBy', Value: self.FilterModifiedBy() });
        if (self.FilterModifiedDate() != "") filters.push({ Field: 'ModifiedDate', Value: self.FilterModifiedDate() });
        if (self.FilterUserGroup() != "") filters.push({ Field: 'UserGroup', Value: self.FilterUserGroup() });
        if (self.FilterUnlimited() != "") filters.push({ Field: 'Unlimited', Value: self.FilterUnlimited() });
        return filters;
    };

    // On success GetData callback
    function OnSuccessGetData(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.UserCategories(data.Rows);

            self.GridProperties().Page(data['Page']);
            self.GridProperties().Size(data['Size']);
            self.GridProperties().Total(data['Total']);
            self.GridProperties().TotalPages(Math.ceil(self.GridProperties().Total() / self.GridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    // On Error callback
    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    // On Always callback
    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }

    //$("#aspnetForm").validate({onsubmit: false});
    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }

    });
};


