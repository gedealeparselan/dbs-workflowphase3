﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class CustomerTypeController : ApiController
    {
        private string message = string.Empty;

        private readonly ICustomerTypeRepository repository = null;

        public CustomerTypeController()
        {
            this.repository = new CustomerTypeRepository();
        }

        public CustomerTypeController(ICustomerTypeRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all Customer Type.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<CustomerTypeModel>))]
        public HttpResponseMessage Get()
        {
            IList<CustomerTypeModel> output = new List<CustomerTypeModel>();

            if (!repository.GetCustomerType(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<CustomerTypeModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Customer Type with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<CustomerTypeModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<CustomerTypeModel> output = new List<CustomerTypeModel>();

            if (!repository.GetCustomerType(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<CustomerTypeModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Customer Type for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(CustomerTypeGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<CustomerTypeFilter> filters)
        {
            CustomerTypeGrid output = new CustomerTypeGrid();

            if (!repository.GetCustomerType(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<CustomerTypeGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find Customer Type using criterias.
        /// </summary>
        /// <param name="query">Code or Description.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<CustomerTypeModel>))]
        [HttpGet]
        [Route("api/CustomerType/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<CustomerTypeModel> output = new List<CustomerTypeModel>();

            if (!repository.GetCustomerType(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<CustomerTypeModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get bene segment by ID.
        /// </summary>
        /// <param name="id">Bene Segment ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(CustomerTypeModel))]
        [Route("api/CustomerType/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            CustomerTypeModel output = new CustomerTypeModel();

            if (!repository.GetCustomerTypeByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<CustomerTypeModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new Customer Type .
        /// </summary>
        /// <param name="CustomerType">Customer Type data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(CustomerTypeModel CustomerType)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddCustomerType(CustomerType, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New Bene Segment data has been created." });
                }
            }
        }

        /// <summary>
        /// Modify existing bene segment.
        /// </summary>
        /// <param name="id">ID of CustomerType to be modify.</param>
        /// <param name="CustomerType">Bene segment data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/CustomerType/{id}")]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(int id, CustomerTypeModel CustomerType)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateCustomerType(id, CustomerType, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Bene Segment data has been updated." });
                }
            }
        }

        /// <summary>
        /// Remove exisiting bene segment.
        /// </summary>
        /// <param name="id">ID of bene segment to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/CustomerType/{id}")]
        [Authorize(Roles = "DBS Admin")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteCustomerType(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "CustomerType data has been deleted." });
                }
            }
        }
        
        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}