﻿using DBS.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class CBOFundController : ApiController
    {
        private string message = string.Empty;

        private readonly ICBOFundRepository repository = null;
        public CBOFundController()
        {
            this.repository = new CBOFundRepository();
        }
        public CBOFundController(ICBOFundRepository repository)
        {
            this.repository = repository;
        }

        [ResponseType(typeof(IList<CBOFundModel>))]
        public HttpResponseMessage GetAll()
        {
            IList<CBOFundModel> result = new List<CBOFundModel>();
            if (!this.repository.GetCBOFund(ref result, 10, 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<CBOFundModel>>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(IList<CBOFundModel>))]
        public HttpResponseMessage GetAll(int? limit, int? index)
        {
            IList<CBOFundModel> result = new List<CBOFundModel>();
            if (!repository.GetCBOFund(ref result, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<IList<CBOFundModel>>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(CBOFundGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<CBOFundFilter> filters)
        {
            CBOFundGrid result = new CBOFundGrid();
            if (!repository.GetCBOFund(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "FundCode" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref result, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<CBOFundGrid>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(IList<CBOFundModel>))]
        [HttpGet]
        [Route("api/CBOFund/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<CBOFundModel> result = new List<CBOFundModel>();

            if (!repository.GetCBOFund(query, limit.HasValue ? limit.Value : 10, ref result, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<CBOFundModel>>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(CBOFundModel))]
        [Route("api/CBOFund/{id}")]
        [HttpGet]
        public HttpResponseMessage GetById(int id)
        {
            CBOFundModel result = new CBOFundModel();

            if (!repository.GetCBOFundByID(id, ref result, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<CBOFundModel>(HttpStatusCode.OK, result);
            }
        }

        [HttpPost]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Post(CBOFundModel CBOFund)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddCBOFund(CBOFund, ref message))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse<string>(HttpStatusCode.Created, "New CBO Fund data has been created.");
                }
            }
        }

        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/CBOFund/{id}")]
        public HttpResponseMessage Put(int id, CBOFundModel CBOFund)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateCBOFund(id, CBOFund, ref message))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "CBO Fund data has been updated.");
                }
            }
        }

        [HttpDelete]
        [Route("api/CBOFund/{id}")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteCBOFund(id, ref message))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse<string>(HttpStatusCode.OK, "CBO Fund data has been deleted.");
                }
            }
        }

        [ResponseType(typeof(CBOFundModel))]
        [Route("api/CBOFund/Parameter")]
        [HttpGet]
        public HttpResponseMessage GetParam(string query)
        {
            IList<CBOFundModel> result = new List<CBOFundModel>();

            if (!repository.GetCBOFundParams(query, ref result, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<IList<CBOFundModel>>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(CBOFundModel))]
        [Route("api/CBOFund/ParameterFund")]
        [HttpGet]
        public HttpResponseMessage GetParam(string query, string pid)
        {
            IList<CBOFundModel> result = new List<CBOFundModel>();

            if (!repository.GetCBOFundParamsFund(query, pid, ref result, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<IList<CBOFundModel>>(HttpStatusCode.OK, result);
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}