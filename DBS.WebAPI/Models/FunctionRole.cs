﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Models
{
    #region Property
    [ModelName("FunctionRole")]
    public class FunctionRoleModel
    {
        /// <summary>
        /// FunctionRole ID.
        /// </summary>
        public int? ID { get; set; }

        /// <summary>
        /// Curreny Name.
        /// </summary>
        //[MaxLength(50, ErrorMessage = "FunctionRole Name is must be in {1} characters.")]
        public string Name { get; set; }

        /// <summary>
        /// Currency Description.
        /// </summary>
        //[MaxLength(255, ErrorMessage = "Maximum length is {1} characters.")]
        public string Description { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }
    }

    public class FunctionRoleRow : FunctionRoleModel
    {
        public int RowID { get; set; }

    }

    public class FunctionRoleGrid : Grid
    {
        public IList<FunctionRoleRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class FunctionRoleFilter : Filter { }
    #endregion

    #region Interface
    public interface IFunctionRoleRepository : IDisposable
    {
        bool GetFunctionRoleByID(int id, ref FunctionRoleModel FunctionRole, ref string message);
        bool GetFunctionRole(ref IList<FunctionRoleModel> FunctionRoles, int limit, int index, ref string message);
        bool GetFunctionRole(int page, int size, IList<FunctionRoleFilter> filters, string sortColumn, string sortOrder, ref FunctionRoleGrid FunctionRole, ref string message);
        bool GetFunctionRole(ref IList<FunctionRoleModel> FunctionRoles, ref string message);
        bool GetFunctionRole(FunctionRoleFilter filter, ref IList<FunctionRoleModel> FunctionRoles, ref string message);
        bool GetFunctionRole(string key, int limit, ref IList<FunctionRoleModel> FunctionRoles, ref string message);
        bool AddFunctionRole(FunctionRoleModel FunctionRole, ref string message);
        bool UpdateFunctionRole(int id, FunctionRoleModel FunctionRole, ref string message);
        bool DeleteFunctionRole(int id, ref string message);
    }
    #endregion

    #region Repository
    public class FunctionRoleRepository : IFunctionRoleRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetFunctionRoleByID(int id, ref FunctionRoleModel FunctionRole, ref string message)
        {
            bool isSuccess = false;

            try
            {
                FunctionRole = (from a in context.FunctionRoles
                              where a.IsDeleted.Equals(false) && a.FunctionRoleID.Equals(id)
                              select new FunctionRoleModel
                              {
                                  ID = a.FunctionRoleID,
                                  Name = a.FunctionRoleName,
                                  Description = a.FunctionRoleDescription,
                                  LastModifiedDate = a.CreateDate,
                                  LastModifiedBy = a.CreateBy
                              }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetFunctionRole(ref IList<FunctionRoleModel> FunctionRoles, ref string message)
        {
            bool isSuccess = false;

            try
            {

                using (DBSEntities context = new DBSEntities())
                {
                    FunctionRoles = (from a in context.FunctionRoles
                                   where a.IsDeleted.Equals(false)
                                   orderby new { a.FunctionRoleName, a.FunctionRoleDescription }
                                   select new FunctionRoleModel
                                   {
                                       ID = a.FunctionRoleID,
                                       Name = a.FunctionRoleName,
                                       Description = a.FunctionRoleDescription,
                                       LastModifiedBy = a.CreateBy,
                                       LastModifiedDate = a.CreateDate
                                   }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool GetFunctionRole(ref IList<FunctionRoleModel> FunctionRole, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    FunctionRole = (from a in context.FunctionRoles
                                  where a.IsDeleted.Equals(false)
                                  orderby new { a.FunctionRoleName, a.FunctionRoleDescription }
                                  select new FunctionRoleModel
                                  {
                                      ID = a.FunctionRoleID,
                                      Name = a.FunctionRoleName,
                                      Description = a.FunctionRoleDescription,
                                      LastModifiedBy = a.CreateBy,
                                      LastModifiedDate = a.CreateDate
                                  }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetFunctionRole(int page, int size, IList<FunctionRoleFilter> filters, string sortColumn, string sortOrder, ref FunctionRoleGrid customer, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                FunctionRoleModel filter = new FunctionRoleModel();
                if (filters != null)
                {
                    filter.Name = (string)filters.Where(a => a.Field.Equals("Name")).Select(a => a.Value).SingleOrDefault();
                    filter.Description = (string)filters.Where(a => a.Field.Equals("Description")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.FunctionRoles
                                let isCode = !string.IsNullOrEmpty(filter.Name)
                                let isDesc = !string.IsNullOrEmpty(filter.Description)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) &&
                                    (isCode ? a.FunctionRoleName.Contains(filter.Name) : true) &&
                                    (isDesc ? a.FunctionRoleDescription.Contains(filter.Description) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new FunctionRoleModel
                                {
                                    ID = a.FunctionRoleID,
                                    Name = a.FunctionRoleName,
                                    Description = a.FunctionRoleDescription,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    customer.Page = page;
                    customer.Size = size;
                    customer.Total = data.Count();
                    customer.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    customer.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new FunctionRoleRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            Name = a.Name,
                            Description = a.Description,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetFunctionRole(FunctionRoleFilter filter, ref IList<FunctionRoleModel> customer, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetFunctionRole(string key, int limit, ref IList<FunctionRoleModel> FunctionRole, ref string message)
        {
            bool isSuccess = false;

            try
            {
                FunctionRole = (from a in context.FunctionRoles
                              where a.IsDeleted.Equals(false) &&
                                  a.FunctionRoleName.Contains(key) || a.FunctionRoleDescription.Contains(key)
                              orderby new { a.FunctionRoleName, a.FunctionRoleDescription }
                              select new FunctionRoleModel
                              {
                                  Name = a.FunctionRoleName,
                                  Description = a.FunctionRoleDescription
                              }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddFunctionRole(FunctionRoleModel FunctionRole, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (!context.FunctionRoles.Where(a => a.FunctionRoleName.Equals(FunctionRole.Name) && a.IsDeleted.Equals(false)).Any())
                {
                    context.FunctionRoles.Add(new Entity.FunctionRole()
                    {
                        FunctionRoleName = FunctionRole.Name,
                        FunctionRoleDescription = FunctionRole.Description,
                        IsDeleted = false,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    });

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("FunctionRole data for FunctionRole Name {0} is already exist.", FunctionRole.Name);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateFunctionRole(int id, FunctionRoleModel FunctionRole, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.FunctionRole data = context.FunctionRoles.Where(a => a.FunctionRoleID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    if (context.FunctionRoles.Where(a => a.FunctionRoleID != FunctionRole.ID && a.FunctionRoleName.Equals(FunctionRole.Name) && a.IsDeleted.Equals(false)).Count() >= 1)
                    {
                        message = string.Format("Function Role Name {0} is exist.", FunctionRole.Name);
                    }
                    else
                    {
                        data.FunctionRoleName = FunctionRole.Name;
                        data.FunctionRoleDescription = FunctionRole.Description;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        context.SaveChanges();

                        isSuccess = true;
                    }
                }
                else
                {
                    message = string.Format("Function Role data for Biz Segment ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeleteFunctionRole(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.FunctionRole data = context.FunctionRoles.Where(a => a.FunctionRoleID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Fungtion Role data for Fungtion Role ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}