﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;
using DBS.Entity;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class NotificationEmailController : ApiController
    {
        private string message = string.Empty;

        private readonly INotificationEmailRepository repository = null;

        public NotificationEmailController()
        {
            this.repository = new NotificationEmailRepository();
        }

        public NotificationEmailController(INotificationEmailRepository repository)
        {
            this.repository = repository;
        }
        /// <summary>
        /// Get all NotificationEmail s.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<NotificationEmailModel>))]
        public HttpResponseMessage Get(string modul)
        {
            IList<NotificationEmailModel> output = new List<NotificationEmailModel>();

            if (!repository.GetNotificationEmail(modul, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<NotificationEmailModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get NotificationEmail with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<NotificationEmailModel>))]
        public HttpResponseMessage Get(string modul, int? limit, int? index)
        {
            IList<NotificationEmailModel> output = new List<NotificationEmailModel>();

            if (!repository.GetNotificationEmail(modul, ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<NotificationEmailModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Notification Email for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(NotificationEmailGrid))]
        [AcceptVerbs("GET", "POST")]
        [Route("api/NotificationEmail/CBONotificationEmail")]
        public HttpResponseMessage GetCBO(int? page, int? size, string sort_column, string sort_order, IList<NotificationEmailFilter> filters)
        {
            string modul = "CBO";
            NotificationEmailGrid output = new NotificationEmailGrid();

            if (!repository.GetNotificationEmail(modul, page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "TypeOfEmail" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<NotificationEmailGrid>(HttpStatusCode.OK, output);
            }
        }
        /// <summary>
        /// Get Notification Email for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(NotificationEmailGrid))]
        [AcceptVerbs("GET", "POST")]
        [Route("api/NotificationEmail/CollateralNotificationEmail")]
        public HttpResponseMessage GetCollateral(int? page, int? size, string sort_column, string sort_order, IList<NotificationEmailFilter> filters)
        {
            string modul = "Collateral";
            NotificationEmailGrid output = new NotificationEmailGrid();

            if (!repository.GetNotificationEmail(modul, page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "TypeOfEmail" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<NotificationEmailGrid>(HttpStatusCode.OK, output);
            }
        }
        /// <summary>
        /// Get Notification Email for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(NotificationEmailGrid))]
        [AcceptVerbs("GET", "POST")]
        [Route("api/NotificationEmail/FDNotificationEmail")]
        public HttpResponseMessage GetFD(int? page, int? size, string sort_column, string sort_order, IList<NotificationEmailFilter> filters)
        {
            string modul = "FD";
            NotificationEmailGrid output = new NotificationEmailGrid();

            if (!repository.GetNotificationEmail(modul, page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "TypeOfEmail" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<NotificationEmailGrid>(HttpStatusCode.OK, output);
            }
        }
        /// <summary>
        /// Get Notification Email for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(NotificationEmailGrid))]
        [AcceptVerbs("GET", "POST")]
        [Route("api/NotificationEmail/UTNotificationEmail")]
        public HttpResponseMessage GetUT(int? page, int? size, string sort_column, string sort_order, IList<NotificationEmailFilter> filters)
        {
            string modul = "UT";
            NotificationEmailGrid output = new NotificationEmailGrid();

            if (!repository.GetNotificationEmail(modul, page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "TypeOfEmail" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<NotificationEmailGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find NotificationEmail s using criterias.
        /// </summary>
        /// <param name="query">Code or Description.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<NotificationEmailModel>))]
        [HttpGet]
        [Route("api/NotificationEmail/Search")]
        public HttpResponseMessage Search(string modul, string query, int? limit)
        {
            IList<NotificationEmailModel> output = new List<NotificationEmailModel>();

            if (!repository.GetNotificationEmail(modul, query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<NotificationEmailModel>>(HttpStatusCode.OK, output);
            }
        }

        
        /// <summary>
        /// Get Notification Email by ID.
        /// </summary>
        /// <param name="id">Notification Email  ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(NotificationEmailModel))]
        [Route("api/NotificationEmail/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(string modul, int id)
        {
            NotificationEmailModel output = new NotificationEmailModel();

            if (!repository.GetNotificationEmailByID(modul, id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<NotificationEmailModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new NotificationEmail .
        /// </summary>
        /// <param name="NotificationEmail">NotificationEmail  data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(string modul, NotificationEmailModel notificationemail)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddNotificationEmail(modul, notificationemail, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New Notification Email data has been created." });
                }
            }
        }

        /// <summary>
        /// Modify existing Notification Email.
        /// </summary>
        /// <param name="id">ID of Notification Email to be modify.</param>
        /// <param name="NotificationEmail">Notification Email data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/NotificationEmail/{id}")]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(string modul, int id, NotificationEmailModel notificationemail)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateNotificationEmail(modul, id, notificationemail, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Notification Email data has been updated." });
                }
            }
        }

        /// <summary>
        /// Remove exisiting Notification Email.
        /// </summary>
        /// <param name="id">ID of Notification Email to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/NotificationEmail/{id}")]
        [Authorize(Roles = "DBS Admin")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Delete(string modul, int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteNotificationEmail(modul, id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Notification Email data has been deleted." });
                }
            }
        }
        [ResponseType(typeof(IList<StaticDropdownParameterModel>))]
        [HttpGet]
        [Route("api/NotificationEmail/Parameters")]
        public HttpResponseMessage Search(string modul)
        {
            IList<StaticDropdownParameterModel> output = new List<StaticDropdownParameterModel>();

            if (!repository.GetParameters(modul, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<StaticDropdownParameterModel>>(HttpStatusCode.OK, output);
            }
        }     
        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }

    }
}
