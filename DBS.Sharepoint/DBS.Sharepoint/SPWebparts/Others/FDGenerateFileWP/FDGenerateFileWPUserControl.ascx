﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FDGenerateFileWPUserControl.ascx.cs" Inherits="DBS.Sharepoint.SPWebparts.Others.FDGenerateFileWP.FDGenerateFileWPUserControl" %>

<div id="new-transaction" class="modal-body overflow-visible">
    <div class="row">
        <div class="col-xs-12">
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div id="upload-form" class="form-horizontal" role="form">
                <div class="form-group">
                    <label class="col-sm-2 control-label no-padding-right" for="upload-type">Format</label>
                    <div class="col-sm-10">
                        <div class="clearfix">
                            <select id="filetype" name="filetype" data-rule-required="true" data-rule-value="true" data-bind="options: $root.Parameter().Filetipes, optionsText: 'TipeDesc', optionsValue: 'ID', optionsCaption: 'Please Select...', value: $root.Selected().Filetipes, event: { change: $root.OnUploadChange }"></select>
                        </div>
                    </div>
                </div>
                <div class="form-group no-margin-bottom no-padding-bottom">
                    <label class="col-sm-2 control-label no-padding-right" for="upload-type">Date</label>
                    <div class="col-sm-3">
                        <div class="clearfix input-group col-sm-10 no-padding">
                            <input class="form-control date-picker" type="text" autocomplete="off" data-date-format="dd-M-yyyy" id="value-date" name="value-date" data-rule-required="false" data-rule-value="true" data-bind="datepicker: $root.SearchDate, value: $root.SearchDate" />
                            <span class="input-group-addon">
                                <i class="icon-calendar bigger-100"></i>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                </div>
                <div class="form-group">
                    <div class="col-sm-2">
                        <div class="clearfix">
                        </div>
                    </div>
                    <div class="col-sm-10">
                        <div class="clearfix">
                            <button class="btn btn-sm btn-primary" data-bind="visible: $root.IsIM() || $root.IsLoanIM() || $root.IsLoanRO(), click: $root.FDBindIM"><i class="icon-edit"></i>Search</button>
                            <button class="btn btn-sm btn-primary" data-bind="click: $root.FDGenerate"><i class="icon-edit"></i>Generate File</button>
                        </div>
                    </div>
                </div>
                <div class="form-group" data-bind="visible: $root.IsSuccess()">
                    <label class="col-sm-2 control-label no-padding-right" for="uri">File Name : </label>
                    <div class="col-sm-10">
                        <div class="clearfix">
                            <a data-bind="attr: { href: $root.InsertedURL, target: '_blank' }, text: $root.FileName"></a>
                        </div>
                    </div>
                </div>
                <br />

                <!-- widget box start -->
                <div id="widget-box" class="widget-box" data-bind="visible: $root.IsIM() || $root.IsLoanIM() || $root.IsLoanRO()">
                    <!-- widget header start -->
                    <div class="widget-header widget-header-small header-color-dark">
                        <h6>Transaction</h6>
                        <div class="widget-toolbar">
                            <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                <i class="blue icon-filter"></i>
                                <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: GridProperties().AllowFilter" />
                                <span class="lbl"></span>
                            </label>
                            <a href="#" data-bind="click: FDBindIM" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                <i class="blue icon-refresh"></i>
                            </a>
                        </div>
                    </div>
                    <!-- widget header end -->

                    <!-- widget body start -->
                    <div class="widget-body">
                        <!-- widget main start -->
                        <div class="widget-main padding-0">
                            <!-- widget slim control start -->
                            <div class="slim-scroll" data-height="400">
                                <!-- widget content start -->
                                <div class="content">
                                    <!-- table responsive start -->
                                    <div class="table-responsive">
                                        <div class="dataTables_wrapper" role="grid">
                                            <table id="workflow-task-table" class="table table-striped table-bordered table-hover dataTable">
                                                <thead>
                                                    <tr data-bind="with: GridProperties">
                                                        <th>No.</th>
                                                        <th>Select<input name="check-all" id="check-all" type="checkbox" data-bind="checked: $root.CheckAll" /></th>
                                                        <th data-bind="click: function () { Sorting('SchemeType') }, css: GetSortedColumn('SchemeType')">Scheme Type</th>
                                                        <th data-bind="click: function () { Sorting('AccNumber') }, css: GetSortedColumn('AccNumber')">Account Number</th>
                                                        <th data-bind="click: function () { Sorting('IntTableCode') }, css: GetSortedColumn('IntTableCode')">Int. Table Code</th>
                                                        <th data-bind="click: function () { Sorting('Version') }, css: GetSortedColumn('Version')">Version</th>
                                                        <th data-bind="click: function () { Sorting('IntCr') }, css: GetSortedColumn('IntCr')">A/c. Pref. Int. (Cr.)</th>
                                                        <th data-bind="click: function () { Sorting('IntDr') }, css: GetSortedColumn('IntDr')">A/c. Pref. Int. (Dr.)</th>
                                                        <th data-bind="click: function () { Sorting('IntPegged') }, css: GetSortedColumn('IntPegged')">Int. Pegged</th>
                                                        <th data-bind="click: function () { Sorting('PegFreq') }, css: GetSortedColumn('PegFreq')">Pegging Freq. (Months/Days)</th>
                                                        <th data-bind="click: function () { Sorting('PegReviewDate') }, css: GetSortedColumn('PegReviewDate')">Peg Review Date</th>
                                                        <th data-bind="click: function () { Sorting('StartDate') }, css: GetSortedColumn('StartDate')">Start Date</th>
                                                        <th data-bind="click: function () { Sorting('EndDate') }, css: GetSortedColumn('EndDate')">End Date</th>
                                                        <th data-bind="click: function () { Sorting('IntMargin') }, css: GetSortedColumn('IntMargin')">InterestMargin</th>
                                                    </tr>
                                                </thead>
                                                <thead data-bind="visible: GridProperties().AllowFilter" class="table-filter">
                                                    <tr>
                                                        <th class="clear-filter">
                                                            <a href="#" data-bind="click: ClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                <i class="green icon-trash"></i>
                                                            </a>
                                                        </th>
                                                        <th></th>
                                                        <th>
                                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterSchemeType, event: { change: GridProperties().Filter }" /></th>
                                                        <th>
                                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterAccountNumber, event: { change: GridProperties().Filter }" /></th>
                                                        <th>
                                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterIntTableCode, event: { change: GridProperties().Filter }" /></th>
                                                        <th>
                                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterVersion, event: { change: GridProperties().Filter }" /></th>
                                                        <th>
                                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterPrefIntCr, event: { change: GridProperties().Filter }" /></th>
                                                        <th>
                                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterPrefIntDr, event: { change: GridProperties().Filter }" /></th>
                                                        <th>
                                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterIntPegged, event: { change: GridProperties().Filter }" /></th>
                                                        <th>
                                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterPeggingFreq, event: { change: GridProperties().Filter }" /></th>
                                                        <th>
                                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterPegReviewDate, event: { change: GridProperties().Filter }" /></th>
                                                        <th>
                                                            <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.FilterStartDate, event: { change: GridProperties().Filter }" /></th>
                                                        <th>
                                                            <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.FilterEndDate, event: { change: GridProperties().Filter }" /></th>
                                                        <th>
                                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterInterestMargin, event: { change: GridProperties().Filter }" /></th>
                                                    </tr>
                                                </thead>

                                                <tbody data-bind="foreach: FDInterest, visible: FDInterest().length > 0">
                                                    <tr>
                                                        <td><span data-bind="text: RowID"></span></td>
                                                        <td>
                                                            <input type="checkbox" id="isSelect" name="isSelect" data-bind="checked: FDIMSelect" />
                                                        </td>
                                                        <td><span data-bind="text: SchemeType"></span></td>
                                                        <td><span data-bind="text: AccNumber"></span></td>
                                                        <td><span data-bind="text: IntTableCode"></span></td>
                                                        <td><span data-bind="text: Version"></span></td>
                                                        <td><span data-bind="text: IntCr"></span></td>
                                                        <td><span data-bind="text: IntDr"></span></td>
                                                        <td><span data-bind="text: IntPegged"></span></td>
                                                        <td><span data-bind="text: PegFreq"></span></td>
                                                        <td><span data-bind="text: PegReviewDate"></span></td>
                                                        <td><span data-bind="text: $root.LocalDate(StartDate, true, false)"></span></td>
                                                        <td><span data-bind="text: $root.LocalDate(EndDate, true, false)"></span></td>
                                                        <td><span data-bind="text: IntMargin"></span></td>
                                                    </tr>
                                                </tbody>
                                                <tbody data-bind="visible: FDInterest().length == 0">
                                                    <tr>
                                                        <td colspan="14" align="center">No entries available.</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- table responsive end -->
                                </div>
                                <!-- widget content end -->
                            </div>
                            <!-- widget slim control end -->

                            <!-- widget footer start -->
                            <div class="widget-toolbox padding-8 clearfix">
                                <div class="row" data-bind="with: GridProperties">
                                    <!-- pagination size start -->
                                    <div class="col-sm-6">
                                        <div class="dataTables_paginate paging_bootstrap pull-left">
                                            Showing
                                            <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                            rows of <span data-bind="text: Total"></span>
                                            entries
                                        </div>
                                    </div>
                                    <!-- pagination size end -->

                                    <!-- pagination page jump start -->
                                    <div class="col-sm-3">
                                        <div class="dataTables_paginate paging_bootstrap">
                                            Page
                                            <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                            of <span data-bind="text: TotalPages"></span>
                                        </div>
                                    </div>
                                    <!-- pagination page jump end -->

                                    <!-- pagination navigation start -->
                                    <div class="col-sm-3">
                                        <div class="dataTables_paginate paging_bootstrap">
                                            <ul class="pagination">
                                                <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                        <i class="icon-double-angle-left"></i>
                                                    </a>
                                                </li>
                                                <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                        <i class="icon-angle-left"></i>
                                                    </a>
                                                </li>
                                                <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                        <i class="icon-angle-right"></i>
                                                    </a>
                                                </li>
                                                <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                        <i class="icon-double-angle-right"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- pagination navigation end -->

                                </div>
                            </div>
                            <!-- widget footer end -->

                        </div>
                        <!-- widget main end -->
                    </div>
                    <!-- widget body end -->
                </div>
                <!-- widget box end -->

            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="/SiteAssets/Scripts/Helper.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/Knockout/knockout.mapping-latest.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/FDGenerateFile.js"></script>
