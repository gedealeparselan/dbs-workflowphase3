﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class PurposeDocsController : ApiController
    {
        private string message = string.Empty;

        private readonly IDocumentPurposeRepository repository = null;

        public PurposeDocsController()
        {
            this.repository = new PurposeDocsRepository();
        }

        public PurposeDocsController(IDocumentPurposeRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all Purpose of Docs.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<DocumentPurposeModel>))]
        [Route("api/PurPosedocs/Get")]
        public HttpResponseMessage Get()
        {
            IList<DocumentPurposeModel> output = new List<DocumentPurposeModel>();

            if (!repository.GetPurposeDocs(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<DocumentPurposeModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Purpose of Docs with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<DocumentPurposeModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<DocumentPurposeModel> output = new List<DocumentPurposeModel>();

            if (!repository.GetPurposeDocs(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<DocumentPurposeModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Purpose of Docs for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(PurposeDocsGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<PurposeDocsFilter> filters)
        {
            PurposeDocsGrid output = new PurposeDocsGrid();

            if (!repository.GetPurposeDocs(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<PurposeDocsGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find Purpose of Docs using criterias.
        /// </summary>
        /// <param name="query">Code or Description.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<DocumentPurposeModel>))]
        [HttpGet]
        [Route("api/PurposeDocs/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<DocumentPurposeModel> output = new List<DocumentPurposeModel>();

            if (!repository.GetPurposeDocs(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<DocumentPurposeModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Purpose of Doc by ID.
        /// </summary>
        /// <param name="id">Purpose of Doc ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(DocumentPurposeModel))]
        [Route("api/PurposeDocs/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            DocumentPurposeModel output = new DocumentPurposeModel();

            if (!repository.GetPurposeDocsByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<DocumentPurposeModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new Purpose of Doc.
        /// </summary>
        /// <param name="PurposeDocs">Purpose of Doc data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(DocumentPurposeModel PurposeDocs)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddPurposeDocs(PurposeDocs, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New Purpose of Doc data has been created." });
                }
            }
        }

        /// <summary>
        /// Modify existing Purpose of Doc.
        /// </summary>
        /// <param name="id">ID of PurposeDocs to be modify.</param>
        /// <param name="PurposeDocs">Purpose of Doc data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/PurposeDocs/{id}")]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(int id, DocumentPurposeModel PurposeDocs)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdatePurposeDocs(id, PurposeDocs, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Purpose of Doc data has been updated." });
                }
            }
        }

        /// <summary>
        /// Remove exisiting Purpose of Doc.
        /// </summary>
        /// <param name="id">ID of Purpose of Doc to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/PurposeDocs/{id}")]
        [Authorize(Roles = "DBS Admin")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeletePurposeDocs(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "PurposeDocs data has been deleted." });
                }
            }
        }
        
        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}