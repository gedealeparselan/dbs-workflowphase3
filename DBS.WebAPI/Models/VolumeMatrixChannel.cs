﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using System.Data.Entity;

namespace DBS.WebAPI.Models
{
    [ModelName("VolumeMatrixChannel")]
    public class VolumeMatrixChannelModel
    {
        /// <summary>
        /// channel's model
        /// </summary>

        /// <summary>
        ///  Channel Name
        /// </summary>
        public string channelName { get; set; }

        /// <summary>
        ///  Quantity On Progress
        /// </summary>
        public int qtyOnProgress { get; set; }

        /// <summary>
        ///  Quantity Completed
        /// </summary>
        public int qtyCompleted { get; set; }

        /// <summary>
        ///  Quantity Completed
        /// </summary>
        public int qtyCancelled { get; set; }

        /// <summary>
        /// Quantity On Progress 
        /// </summary>        
        public int qtyWaitingACK1 { get; set; }

        /// <summary>
        /// Quantity On Progress 
        /// </summary>        
        public int qtyWaitingACK2 { get; set; }

        /// <summary>
        ///  Creted Date
        /// </summary>
        public DateTime createdDate { get; set; }

        /// <summary>
        /// State ID (In Progress, Completed, Cancelled)
        /// </summary>
        public int stateID { get; set; }



    }
    public interface IVolumeMatrixChannel : IDisposable
    {
        bool GetVolumeMatrixDashboardChannel(string productTypeDetailID, Nullable<DateTime> startDate, Nullable<DateTime> endDate, DateTime clientDateTime, string paymentMode, ref IList<VolumeMatrixChannelModel> allDataChannel, ref string message);
    }
    public class VolumeMatrixDashboardChannelRepository : IVolumeMatrixChannel
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();
        public bool GetVolumeMatrixDashboardChannel(string productTypeDetailID, Nullable<DateTime> startDate, Nullable<DateTime> endDate, DateTime clientDateTime, string paymentMode, ref IList<VolumeMatrixChannelModel> allDataChannel, ref string message)
        {
            bool isSuccess = false;
            DateTime serverUTC = DateTime.UtcNow;
            int hourDiff, oneDayHour = 24;
            string[] productTypeDetail = productTypeDetailID.Split(',');
            IList<int> productTypeDetailInt = new List<int>();

            if (clientDateTime.Hour > serverUTC.Hour)
            {
                hourDiff = clientDateTime.Hour - serverUTC.Hour;
            }
            else if (clientDateTime.Hour < serverUTC.Hour)
            {
                hourDiff = oneDayHour + clientDateTime.Hour - serverUTC.Hour;
            }
            else
            {
                hourDiff = 0;
            }

            foreach (string id in productTypeDetail)
            {
                productTypeDetailInt.Add(int.Parse(id));
            }

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    //20160913 add aridya for payment IPE
                    if (paymentMode == "BCP2")
                    {
                        allDataChannel = (from channels in context.Channels
                                          join transactions in context.Transactions
                                          on channels.ChannelID equals transactions.ChannelID
                                          join states in context.ProgressStates
                                          on transactions.StateID equals states.StateID
                                          join products in context.Products
                                          on transactions.ProductID equals products.ProductID
                                          let utcToLocalTransaction = DbFunctions.AddHours(transactions.CreateDate, hourDiff)
                                          where (channels.IsDeleted.Equals(false))
                                                  && (transactions.StateID == 1 || transactions.StateID == 2 || transactions.StateID == 3)
                                                  && ((utcToLocalTransaction >= startDate)
                                                  && (utcToLocalTransaction <= endDate))
                                                  && (channels.IsDeleted.Equals(false))
                                                  && (productTypeDetailInt.Contains(products.ProductID))
                                                  && (transactions.Mode.Equals("BCP2"))

                                          select new
                                          {
                                              channelname = channels.ChannelName,
                                              stateID = transactions.StateID,
                                              TransactionStatus = transactions.Transaction_Status

                                          } into jointable1
                                          group jointable1 by new
                                          {
                                              ChannelName = jointable1.channelname

                                          } into grouptable1
                                          select new VolumeMatrixChannelModel
                                          {

                                              channelName = grouptable1.Key.ChannelName,
                                              qtyOnProgress = (from grouptable in grouptable1
                                                               where (grouptable.stateID == 1 && grouptable.TransactionStatus.Trim() != "IPE ACK1 Task" && grouptable.TransactionStatus.Trim() != "IPE ACK2 Task")
                                                               select grouptable).Count(),

                                              qtyCompleted = (from grouptable in grouptable1
                                                              where (grouptable.stateID == 2)
                                                              select grouptable).Count(),
                                              qtyCancelled = (from grouptable in grouptable1
                                                              where (grouptable.stateID == 3)
                                                              select grouptable).Count(),
                                              qtyWaitingACK1 = (from grouptable in grouptable1
                                                                where grouptable.TransactionStatus == "IPE ACK1 Task"
                                                                select grouptable).Count(),
                                              qtyWaitingACK2 = (from grouptable in grouptable1
                                                                where grouptable.TransactionStatus == "IPE ACK2 Task"
                                                                select grouptable).Count()


                                          }).ToList();
                    }
                    else if (paymentMode == "IPE")
                    {
                        allDataChannel = (from channels in context.Channels
                                          join transactions in context.Transactions
                                          on channels.ChannelID equals transactions.ChannelID
                                          join states in context.ProgressStates
                                          on transactions.StateID equals states.StateID
                                          join products in context.Products
                                          on transactions.ProductID equals products.ProductID
                                          let utcToLocalTransaction = DbFunctions.AddHours(transactions.CreateDate, hourDiff)
                                          where (channels.IsDeleted.Equals(false))
                                                  && (transactions.StateID == 1 || transactions.StateID == 2 || transactions.StateID == 3)
                                                  && ((utcToLocalTransaction >= startDate)
                                                  && (utcToLocalTransaction <= endDate))
                                                  && (channels.IsDeleted.Equals(false))
                                                  && (productTypeDetailInt.Contains(products.ProductID))
                                                  && ((!transactions.Mode.Equals("BCP2")) || transactions.Mode.Equals(null))

                                          select new
                                          {
                                              channelname = channels.ChannelName,
                                              stateID = transactions.StateID,
                                              TransactionStatus = transactions.Transaction_Status

                                          } into jointable1
                                          group jointable1 by new
                                          {
                                              ChannelName = jointable1.channelname

                                          } into grouptable1
                                          select new VolumeMatrixChannelModel
                                          {

                                              channelName = grouptable1.Key.ChannelName,
                                              qtyOnProgress = (from grouptable in grouptable1
                                                               where (grouptable.stateID == 1 && grouptable.TransactionStatus.Trim() != "IPE ACK1 Task" && grouptable.TransactionStatus.Trim() != "IPE ACK2 Task")
                                                               select grouptable).Count(),

                                              qtyCompleted = (from grouptable in grouptable1
                                                              where (grouptable.stateID == 2)
                                                              select grouptable).Count(),
                                              qtyCancelled = (from grouptable in grouptable1
                                                              where (grouptable.stateID == 3)
                                                              select grouptable).Count(),
                                              qtyWaitingACK1 = (from grouptable in grouptable1
                                                                where grouptable.TransactionStatus == "IPE ACK1 Task"
                                                                select grouptable).Count(),
                                              qtyWaitingACK2 = (from grouptable in grouptable1
                                                                where grouptable.TransactionStatus == "IPE ACK2 Task"
                                                                select grouptable).Count()


                                          }).ToList();
                    }
                    else
                    {
                        allDataChannel = (from channels in context.Channels
                                          join transactions in context.Transactions
                                          on channels.ChannelID equals transactions.ChannelID
                                          join states in context.ProgressStates
                                          on transactions.StateID equals states.StateID
                                          join products in context.Products
                                          on transactions.ProductID equals products.ProductID
                                          let utcToLocalTransaction = DbFunctions.AddHours(transactions.CreateDate, hourDiff)
                                          where (channels.IsDeleted.Equals(false))
                                                  && (transactions.StateID == 1 || transactions.StateID == 2 || transactions.StateID == 3)
                                                  && ((utcToLocalTransaction >= startDate)
                                                  && (utcToLocalTransaction <= endDate))
                                                  && (channels.IsDeleted.Equals(false))
                                                  && (productTypeDetailInt.Contains(products.ProductID))

                                          select new
                                          {
                                              channelname = channels.ChannelName,
                                              stateID = transactions.StateID,
                                              TransactionStatus = transactions.Transaction_Status

                                          } into jointable1
                                          group jointable1 by new
                                          {
                                              ChannelName = jointable1.channelname

                                          } into grouptable1
                                          select new VolumeMatrixChannelModel
                                          {

                                              channelName = grouptable1.Key.ChannelName,
                                              qtyOnProgress = (from grouptable in grouptable1
                                                               where (grouptable.stateID == 1 && grouptable.TransactionStatus.Trim() != "IPE ACK1 Task" && grouptable.TransactionStatus.Trim() != "IPE ACK2 Task")
                                                               select grouptable).Count(),

                                              qtyCompleted = (from grouptable in grouptable1
                                                              where (grouptable.stateID == 2)
                                                              select grouptable).Count(),
                                              qtyCancelled = (from grouptable in grouptable1
                                                              where (grouptable.stateID == 3)
                                                              select grouptable).Count(),
                                              qtyWaitingACK1 = (from grouptable in grouptable1
                                                                where grouptable.TransactionStatus == "IPE ACK1 Task"
                                                                select grouptable).Count(),
                                              qtyWaitingACK2 = (from grouptable in grouptable1
                                                                where grouptable.TransactionStatus == "IPE ACK2 Task"
                                                                select grouptable).Count()


                                          }).ToList();
                    }
                    //end add aridya
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}