﻿using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin.Cors;
using Owin;

namespace DBS.SignalR
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseCors(CorsOptions.AllowAll)
                .MapSignalR<EchoConnection>("/echo")
                .MapSignalR("/hub", new HubConfiguration()
                {
                    EnableJavaScriptProxies = false,
                    EnableDetailedErrors = true
                });
            //app.MapSignalR<EchoConnection>("/echo");
        }

        public class EchoConnection : PersistentConnection
        {
            protected override async Task OnConnected(IRequest request, string connectionId)
            {
                //return base.OnConnected(request, connectionId);
                // Notify all connection clients

                //await this.Connection.Broadcast("New connection " + username);
                var data = new
                {
                    ConnectionID = connectionId,
                    Sender = "DBS Server",
                    Message = "Welcome to DBS Workflow, Have a nice day!"
                };

                await this.Connection.Send(connectionId, data);
            }

            protected override async Task OnDisconnected(IRequest request, string connectionId, bool stop)
            {
                // Notify all connected clients
                await this.Connection.Broadcast("Bye bye " + connectionId);
            }

            protected override Task OnReceived(IRequest request, string connectionId, string message)
            {
                var data = new { 
                    ConnectionID = connectionId,
                    Message = message,
                    Sender = "Admin"
                };
                
                return Connection.Broadcast(data);
            }
        }

        

    }
}