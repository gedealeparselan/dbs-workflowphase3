﻿
var ViewModel = function () {

    var self = this;
    self.Readonly = ko.observable(false);
    self.IsWorkflow = ko.observable(false);


    // self.SavePriority = ko.observable("");
    self.InformationTypeDrop = ko.observableArray([]);
    self.MaintenanceCCDrop = ko.observableArray([]);
    //add by fandi
    self.MaintenanceCCTemp = ko.observableArray([]);


    self.IsRoleMaker = ko.observable(false);
    self.IsPermited = ko.observable(false);

    // New Data flag
    self.IsNewData = ko.observable(false);
    //Data
    var MainDataCustomer = {
        MainDataCustomer: self.MaintenanceCCDrop
    };
    // Declare an ObservableArray for Storing the JSON Response
    self.CBOMaintainSubID = ko.observableArray([]);
    self.SubTypeName = ko.observableArray([]);
    self.InformationType = ko.observableArray([]);
    self.MaintenanceCC = ko.observableArray([]);
    self.SelectedInformationType = ko.observableArray([]);
    self.SelectedMaintenanceCC = ko.observableArray([]);
    self.MainDataCustomerApproval = ko.observableArray([]);


    self.searchParameter = ko.observable("");

    // bind get data function to viewget
    //self.GetData = function () {
    //    GetData();
    //};

    self.IsPermited = function (IsPermitedResult) {

        spUser = $.cookie(api.cookie.spUser);

        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/CheckUserPermited/" + _spFriendlyUrlPageContextInfo.title,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    //compare user role to page permission to get checker validation
                    for (i = 0; i < spUser.Roles.length; i++) {
                        for (j = 0; j < data.length; j++) {
                            if (Const_RoleName[spUser.Roles[i].ID] == data[j]) { //if (spUser.Roles[i].Name == data[j]) {
                                if (Const_RoleName[spUser.Roles[i].ID].endsWith('Maker')) { //if (spUser.Roles[i].Name.endsWith('Maker')) {
                                    self.IsRoleMaker(true);
                                    return;
                                }
                                else {
                                    self.IsRoleMaker(false);
                                }
                            }
                        }
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    };
    self.GetDropdown = function () {

        GetDropdown();
    };

    function GetDropdown() {
        $.ajax({
            async: false,
            type: "GET",
            url: api.server + api.url.parameter + '?select=InformationType,MaintenanceCC',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status == 200) {
                    //console.log(data['Currency']);					
                    self.InformationTypeDrop(data['InformationType']);
                    self.MaintenanceCCDrop(data['MaintenanceCC']);
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }

        });

    }

    //    self.pullUp = function () {
    //        var sel = self.SelectedMaintenanceTypeNamePriority();
    //        for (var i = 0; i < sel.length; i++) {
    //        if(sel <= 0)return;
    //        var selCat = sel[i]-1;
    //        var result = self.MaintenanceTypeNamePriority.remove(function (item) {
    //            return item.CBOMaintainID == selCat;
    //        });
    //        if (result && result.length <= 0) {
    //            self.MaintenanceTypeNamePriority.push(result[0]);
    //            console.log(ko.toJSON(self.MaintenanceTypeNamePriority));
    //        }
    //    }
    //    self.SelectedMaintenanceTypeNamePriority.removeAll();
    //    console.log(ko.toJSON(self.MaintenanceTypeName));


    //}.bind(this);
    self.save = function () {
        // validation

        var form = $("#aspnetForm");
        form.validate();
        if (form.valid) {
            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {
                    console.log(ko.toJSON(self.MaintenanceCCDrop));
                    //add by fandi
                    if (self.MaintenanceCCDrop().length == 0)
                    {
                        self.MaintenanceCCDrop.push(self.MaintenanceCCTemp()[0]);
                    }
                    //end
                    $.ajax({
                        type: "POST",
                        url: api.server + api.url.maindatacustomer,
                        data: ko.toJSON(MainDataCustomer.MainDataCustomer), //Convert the Observable Data into JSON
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {
                                // send notification                                
                                ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // hide current popup window
                                $("#modal-form").modal('hide');

                                // refresh data
                                GetData();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });


                }

            });



        }

    };


    self.pullLeft = function () {
        var sel = self.SelectedMaintenanceCC();
        for (var i = 0; i < sel.length; i++) {
            var selCat = sel[i];
            //add by fandi
            var selTemp = sel[i];
            ko.utils.arrayForEach(self.MaintenanceCCDrop(), function (item) {
                //return item.CBOMaintainSubID == selTemp;
                if (item.CBOMaintainSubID == selTemp) {
                    self.MaintenanceCCTemp.push(item);
                }
            });
            

            //end
            var result = self.MaintenanceCCDrop.remove(function (item) {
                return item.CBOMaintainSubID == selCat;
            });
            //  $('.MaintenanceTypeNamePriority' + "option:selCat").clone().appendTo('.MaintenanceTypeName');
            if (result && result.length > 0) {
                self.InformationTypeDrop.push(result[0]);
                console.log(ko.toJSON(self.InformationTypeDrop));
            }
        }
        self.SelectedMaintenanceCC.removeAll();
        console.log(ko.toJSON(self.InformationTypeDrop));


    }.bind(this);
    self.pullRight = function () {
        var sel = self.SelectedInformationType();
        for (var i = 0; i < sel.length; i++) {
            var selCat = sel[i];
            var result = self.InformationTypeDrop.remove(function (item) {
                return item.CBOMaintainSubID == selCat;
            });
            // $('.MaintenanceTypeName' + "option:selCat").appendTo('.MaintenanceTypeNamePriority');
            if (result && result.length > 0) {
                self.MaintenanceCCDrop.push(result[0]);
                console.log(ko.toJSON(self.MaintenanceCCDrop));
            }
        }
        self.SelectedInformationType.removeAll();
        console.log(ko.toJSON(self.MaintenanceCCDrop));



    }.bind(this);

    $.holdReady(true);
    var intervalRoleName = setInterval(function () {
        if (Object.keys(Const_RoleName).length != 0) {
            $.holdReady(false);
            clearInterval(intervalRoleName);
        }
    }, 100);

    //Pull UP / D
    //$(document).on("RoleNameReady", function() {
    $(document).ready(function () {
        $('input[type="button"]').click(function () {
            var $op = $('#MaintenanceTypeNamePriority option:selected'),
                $this = $(this);
            if ($op.length) {
                ($this.val() == '?') ?
                    $op.first().prev().before($op) :
                    $op.last().next().after($op);
                //self.MaintenanceTypeNamesPriorityDrop = $this;

            }
        });
    });



    self.UpdateMaintenanceTypeName = function () {
        // validation
        IsvalidField();
        var form = $("#aspnetForm");
        form.validate();

        if (self.selectedDays().length == 0) {
            alert('Days of Ageing must be selected');
            return;
        }

        self.Days(self.selectedDays().join(","));

        if (form.valid()) {
            //Ajax call to insert the AgeingReportParameter
            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {

                    $.ajax({
                        type: "POST",
                        url: api.server + api.url.ageingreport,
                        data: ko.toJSON(AgeingReportParameter), //Convert the Observable Data into JSON
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        contentType: "application/json",
                        success: function (data, textStatus, jqXHR) {
                            // send notification
                            ShowNotification('Create Success', jqXHR.responseText, 'gritter-success', false);

                            // hide current popup window
                            $("#modal-form").modal('hide');

                            // refresh data
                            GetData();
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification('An Error Occured', jqXHR.responseText, 'gritter-error', false);
                        }
                    });
                }
            });
        }
    };


    // Local Date
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return ""
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-17
    };

    //Function to Display record to be updated
    //self.GetSelectedRow = function (data) {
    //    //fandi
    //    //var count = data.length();

    //    self.IsNewData(false);
    //    if (self.IsWorkflow()) $("#modal-form-workflow").modal('show');
    //    else $("#modal-form").modal('show');

    //    for (i = 0; i < data.length; i++)
    //    {
    //       // text += self.MaintenanceTypeName(data[i].MaintenanceTypeName) + "<br>";
    //       self.MaintenanceTypeName(data[i].MaintenanceTypeName);
    //        //document.getElementById("MaintenanceTypeName").innerHTML = text;

    //        //console.log(data[i].MaintenanceTypeName);
    //      //  self.MaintenanceTypeName(data.MaintenanceTypeName);
    //    }

    //};
    self.GetSelectedRow = function (data) {

        self.IsNewData(false);
        if (self.IsWorkflow()) $("#modal-form-workflow").modal('show');
        else $("#modal-form").modal('show');

        if (ko.toJSON(self.Readonly() == false)) {
            self.MainDataCustomerApproval(data);
            console.log(data);

        }

    };

}


// On Error callback
function OnError(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}

// On Always callback
function OnAlways() {
    $box.trigger('reloaded.ace.widget');
}
$('#aspnetForm').validate({
    errorElement: 'div',
    errorClass: 'help-block',
    focusInvalid: true,
    highlight: function (e) {
        $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
    },

    success: function (e) {
        $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
        $(e).remove();
    },

    errorPlacement: function (error, element) {
        if (element.is(':checkbox') || element.is(':radio')) {
            var controls = element.closest('div[class*="col-"]');
            if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
            else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
        }
        else if (element.is('.select2')) {
            error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
        }
        else if (element.is('.chosen-select')) {
            error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
        }
        else error.insertAfter(element.parent());
    }

});


//$(document).ready(function () {
//    window.onmousedown = function (e) {
//        var el = e.target;
//        if (el.tagName.toLowerCase() == 'option' && el.parentNode.hasAttribute('multiple')) {
//            e.preventDefault();

//            // toggle selection
//            if (el.hasAttribute('selected')) el.removeAttribute('selected');
//            else el.setAttribute('selected', '');

//            // hack to correct buggy behavior
//            var select = el.parentNode.cloneNode(true);
//            el.parentNode.parentNode.replaceChild(select, el.parentNode);
//        }
//    }


//});
