﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
﻿using DBS.WebAPI.Models;
using System.Web.Http;
using System.Web.Http.Description;
using System.Net.Http;
using System.Net;

namespace DBS.WebAPI.Controllers
{
    public class TransactionBringupTaskController : ApiController
    {
        private string message = string.Empty;

        private readonly ITransactionBringupTaskRepository repository = null;
        public TransactionBringupTaskController (){
            this.repository = new TransactionBringupTaskRepository();
        }

        public TransactionBringupTaskController(ITransactionBringupTaskRepository repo)
        {
            this.repository = repo;
        }

        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }

        [ResponseType(typeof(TransactionBringupTaskModel))]
        [Route("api/TransactionBringupTask/{id}")]
        [HttpGet]
        public HttpResponseMessage GetBringupTask(Int64 id)
        {
            TransactionBringupTaskModel output = new TransactionBringupTaskModel();

            if (!repository.GetTransactionBringupTaskById(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<TransactionBringupTaskModel>(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(TransactionBringupTaskGrid))]
        [Route("api/TransactionBringupTask")]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetDraft(int? page, int? size, string sort_column, string sort_order, IList<TransactionBringupTaskFilter> filters)
        {
            TransactionBringupTaskGrid output = new TransactionBringupTaskGrid();

            if (!repository.GetTransactionBringupTask(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "ApplicationID" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<TransactionBringupTaskGrid>(HttpStatusCode.OK, output);
            }
        }
    }
}