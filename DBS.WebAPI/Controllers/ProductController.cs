﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class ProductController : ApiController
    {
        private string message = string.Empty;

        private readonly IProductRepository repository = null;

        public ProductController()
        {
            this.repository = new ProductRepository();
        }

        public ProductController(IProductRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all products.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<ProductModel>))]
        public HttpResponseMessage Get()
        {
            IList<ProductModel> output = new List<ProductModel>();

            if (!repository.GetProduct(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<ProductModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get products with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<ProductModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<ProductModel> output = new List<ProductModel>();

            if (!repository.GetProduct(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<IList<ProductModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get products for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(ProductGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<ProductFilter> filters)
        {
            ProductGrid output = new ProductGrid();

            if (!repository.GetProduct(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Code" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<ProductGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find products using criterias Code or Description.
        /// </summary>
        /// <param name="query">Code or Description.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<ProductModel>))]
        [HttpGet]
        [Route("api/Product/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<ProductModel> output = new List<ProductModel>();

            if (!repository.GetProduct(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<ProductModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Product by ID.
        /// </summary>
        /// <param name="id">Product ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(ProductModel))]
        [Route("api/Product/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            ProductModel output = new ProductModel();

            if (!repository.GetProductByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<ProductModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new Product.
        /// </summary>
        /// <param name="product">Product data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Post(ProductModel product)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddProduct(product, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New Product data has been created." });
                }
            }
        }

        /// <summary>
        /// Modify existing Product.
        /// </summary>
        /// <param name="id">ID of Product to be modify.</param>
        /// <param name="product">Product data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/Product/{id}")]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(int id, ProductModel product)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateProduct(id, product, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Product data has been updated." });
                }
            }
        }

        /// <summary>
        /// Remove exisiting Product.
        /// </summary>
        /// <param name="id">ID of Product to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/Product/{id}")]
        [ResponseType(typeof(string))]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteProduct(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Product data has been deleted." });
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}