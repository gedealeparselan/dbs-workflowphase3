﻿<%@ assembly name="DBS.Sharepoint, Version=1.0.0.0, Culture=neutral, PublicKeyToken=f75c72caf4809f4b" %>
<%@ assembly name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ register tagprefix="SharePoint" namespace="Microsoft.SharePoint.WebControls" assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ register tagprefix="Utilities" namespace="Microsoft.SharePoint.Utilities" assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ register tagprefix="asp" namespace="System.Web.UI" assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ import namespace="Microsoft.SharePoint" %>
<%@ register tagprefix="WebPartPages" namespace="Microsoft.SharePoint.WebPartPages" assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ control language="C#" autoeventwireup="true" codebehind="CSOTaskWPUserControl.ascx.cs" inherits="DBS.Sharepoint.SPWebparts.Others.CSOTaskWP.CSOTaskWPUserControl" %>

<style>
	#container.handsontable table{
		width:95%;
	}
</style>

<div class="row">
    <div class="col-xs-12">
        <div id="transaction-form" class="form-horizontal" role="form">
            <div class="form-group">
                <label class="col-sm-3" for="cso-task-type">CSO Task Type</label>
                <div class="col-sm-9">
                    <select id="cso-task-type" name="cso-task-type" class="col-sm-3" style="width: 180px">
                        <option value="0">--Please Select--</option>
                        <option value="1">Interest Maintenance</option>
                        <option value="2">Rollover</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3" for="TaskDate" id="taskDateLabel">Date</label>
                <div class="col-sm-9">
                    <div class="input-group col-sm-3" style="right: 11px">
                        <input class="form-control date-picker" id="TaskDate" name="TaskDate" data-date-format="dd-M-yyyy" type="text" />
                        <span class="input-group-addon">
                            <i class="icon-calendar bigger-110"></i>
                        </span>
                    </div>
                    <div class="clearfix">
                        <button class="btn btn-sm btn-primary" onclick="GenerateExcel();return false;"><i class="icon-edit"></i>Select</button>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div id="dataTable" style="overflow-x: auto"></div>
            </div>
        </div>
        <div class="form-group">
            <b>
                <label class="col-sm-3">
                    Attachment
                </label>
            </b>
            <label class="col-sm-9 pull-right">
                <button class="btn btn-sm btn-primary pull-right" data-bind="click: $root.UploadDocument">
                    <i class="icon-plus"></i>
                    Attach
                </button>
            </label>
            <table class="table table-striped table-bordered table-hover dataTable">
                <thead>
                    <tr>
                        <th style="width: 50px">No.</th>
                        <th>File Name</th>
                        <th>Document Type</th>
                        <th>Modified</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody data-bind="foreach: $root.Documents, visible: $root.Documents().length > 0">
                    <tr>
                        <td><span data-bind="text: $index() + 1"></span></td>
                        <td><a data-bind="attr: { href: DocumentPath.DocPath, target: '_blank' }, text: DocumentPath.name, disable: (ID > 0)"></a></td>
                        <td><span data-bind="text: Type.Name"></span></td>
                        <td><span data-bind="text: moment(DocumentPath.lastModifiedDate).format(config.format.dateTime)"></span></td>
                        <td><a href="#" data-bind="click: $root.RemoveDocument">Remove</a></td>
                    </tr>
                </tbody>
                <tbody data-bind="visible: $root.Documents().length == 0">
                    <tr>
                        <td colspan="6" align="center">No entries available.</td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>
</div>

<div class="modal-footer">
    <button class="btn btn-sm btn-primary" id="btnsave" data-bind="click: $root.save, disable: $root.IsSubmitted()==true">
        <i class="icon-save"></i>
        Submit
    </button>
    <button class="btn btn-sm" data-dismiss="modal">
        <i class="icon-remove"></i>
        Close
    </button>
</div>

<!-- Modal upload form start -->
<div id="modal-form-upload" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body overflow-visible">
                <div class="row">
                    <div class="col-xs-12">
                        <div id="customer-form" class="form-horizontal" role="form">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="document-type">Document Type</label>

                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <select id="document-type" name="document-type" data-rule-required="true" data-rule-value="true"
                                            data-bind="options: $root.ddlDocumentTypes,
    optionsText: function (item) { if (item.ID != null) return item.Name },
    optionsValue: 'ID',
    optionsCaption: 'Please Select...',
    value: $root.Selected().DocumentType">
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="space-4"></div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="document-path">Document</label>

                                <div class="col-sm-8">
                                    <div class="clearfix">
                                        <input type="file" id="document-path" name="document-path" data-bind="file: DocumentPath" />
                                    </div>
                                </div>
                                <label class="control-label bolder text-danger">*</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button class="btn btn-sm btn-primary" data-bind="click: $root.AddDocument">
                    <i class="icon-save"></i>
                    Save
                </button>
                <button class="btn btn-sm" data-dismiss="modal">
                    <i class="icon-remove"></i>
                    Cancel
                </button>
            </div>
        </div>
    </div>
</div>

<div id="modal-form-uploadAddHoc" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body overflow-visible">
                <div class="row">
                    <div class="col-xs-12">
                        <div id="customer-formAddHoc" class="form-horizontal" role="form">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="document-type">Document Type</label>

                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <select id="document-typeaddhoc" name="document-typeaddhoc" data-rule-required="true" data-rule-value="true"
                                            data-bind="options: $root.ddlDocumentTypes,
    optionsText: function (item) { if (item.ID != null) return item.Name },
    optionsValue: 'ID',
    optionsCaption: 'Please Select...',
    value: $root.Selected().DocumentType">
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="space-4"></div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="document-path">Document</label>

                                <div class="col-sm-8">
                                    <div class="clearfix">
                                        <input type="file" id="document-pathaddhoc" name="document-pathaddhoc" data-bind="file: DocumentPath" />
                                    </div>
                                </div>
                                <label class="control-label bolder text-danger">*</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button class="btn btn-sm btn-primary" data-bind="click: $root.AddDocumentAddHoc">
                    <i class="icon-save"></i>
                    Save
                </button>
                <button class="btn btn-sm" data-dismiss="modal">
                    <i class="icon-remove"></i>
                    Cancel
                </button>
            </div>
        </div>
    </div>
</div>

<div id="modal-form-instruction" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width: 95%">
        <div class="modal-content" style="top: 20px">
            <div class="modal-body overflow-visible">
                <div class="widget-box">
                    <div class="widget-header widget-hea1der-small header-color-dark">
                        <h6>Instruction List Table</h6>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main padding-0">
                            <div class="slim-scroll">
                                <div class="content">
                                    <div class="table-responsive">
                                        <div class="dataTables_wrapper" role="grid">
                                            <table class="table table-striped table-bordered table-hover dataTable">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Application ID</th>
                                                        <th>Customer Name</th>
                                                        <th>CIF</th>
                                                        <th>Product</th>
                                                        <th>IBG Segment</th>
                                                        <th>Currency</th>
                                                        <th>Amount</th>
                                                    </tr>
                                                </thead>
                                                <tbody data-bind="foreach: $root.ChooseInstruction()">
                                                    <tr data-bind="click: $root.onSelectionChoseeFile">
                                                        <td><span data-bind="text: $index() + 1"></span></td>
                                                        <td><span data-bind="text: ApplicationID"></span></td>
                                                        <td><span data-bind="text: CustomerName"></span></td>
                                                        <td><span data-bind="text: CIF"></span></td>
                                                        <td><span data-bind="text: ProductName"></span></td>
                                                        <td><span data-bind="text: BizSegment.Description"></span></td>
                                                        <td><span data-bind="text: CurrencyName"></span></td>
                                                        <td align="right"><span data-bind="text: $root.FormatNumber(Transaction.transactionAmount)"></span></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>    
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-sm" data-dismiss="modal">
                            <i class="icon-remove"></i>
                            Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modal-form-detail-loan" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
    <div class="modal-dialog" style="height: 100%; width: 100%;">
        <div id="backDrop" class="absCustomBackDrop topCustomBackDrop darkCustomBackDrop hideCustomBackDrop fullHeightCustomBackDrop fullWidthCustomBackDrop centerCustomBackDrop"></div>
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="blue bigger"></h4>
            </div>
            <div class="modal-body overflow-visible">
                <div class="row" data-bind="with: $root.CSODetails()">
                    <div class="row" data-bind="ifnot: $data == null">
                        <div class="col-xs-12">
                            <div id="transaction-form2" class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label bolder no-padding-right" for="top-urgent">Top Urgent</label>

                                    <label class="col-sm-1">
                                        <input id="top-urgent1" name="top-urgent" class="ace ace-switch ace-switch-6" type="checkbox" disabled="disabled" />
                                        <span class="lbl"></span>
                                    </label>

                                    <label class="col-sm-2 control-label bolder no-padding-right" for="top-urgent">Top Urgent Chain</label>

                                    <label class="col-sm-1">
                                        <input id="top-urgent2" name="top-urgent" class="ace ace-switch ace-switch-6" type="checkbox" disabled="disabled" />
                                        <span class="lbl"></span>
                                    </label>
                                    <label class="col-sm-2 pull-left">
                                            <b>Application ID</b> : <span data-bind="text: Transaction.ApplicationID"></span>
                                    </label>
                                </div>


                                <div class="form-group">
                                    <label class="col-sm-3 control-label bolder no-padding-right">Customer Name</label>

                                    <div class="col-sm-6">
                                        <div class="clearfix">
                                            <input type="text" autocomplete="off" class="col-sm-12" name="customer-name" id="customer-name" disabled="disabled" data-rule-required="true" data-bind="value: Transaction.Customer.Name" />
                                        </div>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label bolder no-padding-right">CIF</label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" autocomplete="off" class="col-sm-3" name="cif" id="cif" disabled="disabled" data-bind="value: Transaction.Customer.CIF" />
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label bolder no-padding-right">Product</label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="product" name="product" data-rule-required="true" disabled="disabled" data-bind="options: $root.Products, optionsText: function (item) { if (item.Code != null) return item.Code + ' (' + item.Name + ')' }, optionsValue: 'ID', optionsCaption: 'Please Select...', value: Transaction.Product.ID"></select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label bolder no-padding-right" for="biz-segment">IBG Segment</label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="bizsegment" name="bizsegment" disabled="disabled" data-bind="options: $root.BizSegments, optionsText: function (item) { if (item.Name != null) return item.Name + ' (' + item.Description + ')'; }, optionsValue: 'ID', optionsCaption: 'Please Select...', value: Transaction.BizSegment.ID"></select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label bolder no-padding-right" for="currency">Transaction Currency</label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="currency" name="currency" disabled="disabled" data-bind="options: $root.Currencies, optionsText: 'Code', optionsValue: 'ID', optionsCaption: 'Please Select...', value: Transaction.Currency.ID"></select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label bolder no-padding-right" for="trxn-amount">Transaction Amount</label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" autocomplete="off" class="col-sm-4 align-right" name="trxn-amount" id="trxn-amount" disabled="disabled" data-rule-required="true" data-rule-number="true" data-bind="value: Transaction.Amount" />
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label bolder no-padding-right" for="channel">Channel</label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="channel" name="channel" disabled="disabled" data-bind="options: $root.Channels, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Please Select...', value: Transaction.Channel.ID"></select>
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div>
                                    <h3 class="header smaller lighter dark">Attachment</h3>

                                    <div class="dataTables_wrapper" role="grid">
                                        <table class="table table-striped table-bordered table-hover dataTable">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>File Name</th>
                                                    <th>Document Type</th>
                                                    <th>Modified</th>
                                                </tr>
                                            </thead>
                                            <tbody data-bind="foreach: Transaction.Documents, visible: Transaction.Documents.length > 0">
                                                <tr>
                                                    <td><span data-bind="text: $index() + 1"></span></td>
                                                    <td><a data-bind="attr: { href: DocumentPath, target: '_blank' }, text: FileName"></a></td>
                                                    <td><span data-bind="text: Type.Name"></span></td>
                                                    <td><span data-bind="text: moment(LastModifiedDate).format(config.format.dateTime)"></span></td>
                                                </tr>
                                            </tbody>
                                            <tbody data-bind="visible: Transaction.Documents.length == 0">
                                                <tr>
                                                    <td colspan="6" align="center">No entries available.</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-sm" data-dismiss="modal">
                        <i class="icon-remove"></i>
                        Cancel
                    </button>
                </div>
            </div>
        </div>
        <!-- modal footer end -->

    </div>
</div>

<style>
    .handsontable .htDimmed {
        color: #020202 !important;
    }
</style>

<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/Knockout/knockout.mapping-latest.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/CSOTask.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/Helper.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/Knockout/knockout.mapping-latest.js"></script>

<link rel="stylesheet" media="screen" href="/SiteAssets/Scripts/handsontable/dist/handsontable.full.css">
<script src="/SiteAssets/Scripts/handsontable/dist/handsontable.full.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/parser.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/ruleJS.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/lodash.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/underscore.string.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/moment.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/numeral.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/md5.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/jstat.js"></script>
