﻿using DBS.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class NostroController : ApiController
    {
        private string message = string.Empty;

        private readonly INostroRepository repository = null;
        public NostroController()
        {
            this.repository = new NostroRepository();
        }
        public NostroController(INostroRepository repository)
        {
            this.repository = repository;
        }
        
        [ResponseType(typeof(IList<NostroModel>))]
        public HttpResponseMessage GetAll()
        {
            IList<NostroModel> result = new List<NostroModel>();
            if (!this.repository.GetNostro(ref result, 10, 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            } else {
                return Request.CreateResponse<IList<NostroModel>>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(IList<NostroModel>))]
        public HttpResponseMessage GetAll(int? limit, int? index)
        {
            IList<NostroModel> result = new List<NostroModel>();
            if (!repository.GetNostro(ref result, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<IList<NostroModel>>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(NostroGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<NostroFilter> filters)
        {
            NostroGrid result = new NostroGrid();
            if (!repository.GetNostro(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref result, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<NostroGrid>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(IList<NostroModel>))]
        [HttpGet]
        [Route("api/Nostro/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<NostroModel> result = new List<NostroModel>();

            if (!repository.GetNostro(query, limit.HasValue ? limit.Value : 10, ref result, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<NostroModel>>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(NostroModel))]
        [Route("api/Nostro/{id}")]
        [HttpGet]
        public HttpResponseMessage GetById(int id)
        {
            NostroModel result = new NostroModel();

            if (!repository.GetNostroByID(id, ref result, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<NostroModel>(HttpStatusCode.OK, result);
            }
        }

        [HttpPost]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Post(NostroModel nostro)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddNostro(nostro, ref message))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse<string>(HttpStatusCode.Created, "New nostro data has been created.");
                }
            }
        }

        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/Nostro/{id}")]
        public HttpResponseMessage Put(int id, NostroModel nostro)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateNostro(id, nostro, ref message))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Nostro data has been updated.");
                }
            }
        }

        [HttpDelete]
        [Route("api/Nostro/{id}")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteNostro(id, ref message))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse<string>(HttpStatusCode.OK, "Nostro data has been deleted.");
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}