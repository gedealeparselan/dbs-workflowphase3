﻿using System;
using DBS.WebAPI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Description;
using System.Web;
using System.Web.Http;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class BeneficiaryBusinessTypeController : ApiController
    {
        private string message = string.Empty;
        private readonly IBeneficiaryBusinessType repo = null;
        public BeneficiaryBusinessTypeController()
        {
            this.repo = new BeneficiaryRepository();
        }
        public BeneficiaryBusinessTypeController(BeneficiaryRepository repo)
        {
            this.repo = repo;
        }

        //Get All
        [ResponseType(typeof(IList<BeneficiaryBusinessTypeModel>))]
        public HttpResponseMessage GetAll()
        {
            IList<BeneficiaryBusinessTypeModel> result = new List<BeneficiaryBusinessTypeModel>();
            if (!this.repo.GetBeneficiary(ref result, 10, 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<BeneficiaryBusinessTypeModel>>(HttpStatusCode.OK, result);
            }
        }

        //Get ALL by Filter
        [ResponseType(typeof(BeneficiaryBusinessTypeGrid))]
        [AcceptVerbs("GET", "POST")]
        [Route("api/BeneficiaryBusinessType")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<BeneficiaryBusinessTypeFilter> filters)
        {
            BeneficiaryBusinessTypeGrid result = new BeneficiaryBusinessTypeGrid();
            if (!repo.GetBeneficiary(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "BeneficiaryBusinessTypeCode" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref result, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<BeneficiaryBusinessTypeGrid>(HttpStatusCode.OK, result);
            }
        }

        //Cari 
        [ResponseType(typeof(IList<BeneficiaryBusinessTypeModel>))]
        [HttpGet]
        [Route("api/BeneficiaryBusinessType/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<BeneficiaryBusinessTypeModel> result = new List<BeneficiaryBusinessTypeModel>();

            if (!repo.GetBeneficiary(query, limit.HasValue ? limit.Value : 10, ref result, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<BeneficiaryBusinessTypeModel>>(HttpStatusCode.OK, result);
            }
        }

        // Get By ID
        [ResponseType(typeof(BeneficiaryBusinessTypeModel))]
        [Route("api/BeneficiaryBusinessType/{id}")]
        [HttpGet]
        public HttpResponseMessage GetById(int id)
        {
            BeneficiaryBusinessTypeModel result = new BeneficiaryBusinessTypeModel();

            if (!repo.GetBeneficiaryByID(id, ref result, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<BeneficiaryBusinessTypeModel>(HttpStatusCode.OK, result);
            }
        }

        [HttpPost]
        [ResponseType(typeof(string))]
        [Route("api/BeneficiaryBusinessType")]
        public HttpResponseMessage Post(BeneficiaryBusinessTypeModel CBOFund)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repo.AddBeneficiary(CBOFund, ref message))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse<string>(HttpStatusCode.Created, "New BeneficiaryBusinessTypeModel data has been created.");
                }
            }
        }

        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/BeneficiaryBusinessType/{id}")]
        public HttpResponseMessage Put(int id, BeneficiaryBusinessTypeModel CBOFund)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repo.UpdateBeneficiary(id, CBOFund, ref message))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "BeneficiaryBusinessType data has been updated.");
                }
            }
        }

        [HttpDelete]
        [Route("api/BeneficiaryBusinessType/{id}")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repo.DeleteBeneficiary(id, ref message))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse<string>(HttpStatusCode.OK, "BeneficiaryBusinessType data has been deleted.");
                }
            }
        }

        [ResponseType(typeof(CBOFundModel))]
        [Route("api/BeneficiaryBusinessType/Parameter")]
        [HttpGet]
        public HttpResponseMessage GetParam(string query)
        {
            IList<BeneficiaryBusinessTypeModel> result = new List<BeneficiaryBusinessTypeModel>();

            if (!repo.GetBeneficiaryBusinessParams(query, ref result, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<IList<BeneficiaryBusinessTypeModel>>(HttpStatusCode.OK, result);
            }
        }

        [ResponseType(typeof(CBOFundModel))]
        [Route("api/BeneficiaryBusinessType/ParameterBusiness")]
        [HttpGet]
        public HttpResponseMessage GetParam(string query, string pid)
        {
            IList<BeneficiaryBusinessTypeModel> result = new List<BeneficiaryBusinessTypeModel>();

            if (!repo.GetBeneficiaryBusinessParamsBusiness(query, pid, ref result, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<IList<BeneficiaryBusinessTypeModel>>(HttpStatusCode.OK, result);
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (repo != null)
                repo.Dispose();
            base.Dispose(disposing);
        }

    }
}