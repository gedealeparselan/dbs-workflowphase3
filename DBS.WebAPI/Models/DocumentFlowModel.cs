﻿using DBS.Entity;
using DBS.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.EntityClient;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Transactions;

namespace DBS.WebAPI.Models
{
    #region Property
    public class RecipientModel
    {
        public int DFRecipientID {set; get;}
        public string DFRecipientName {set; get;}
        public bool IsDeleted {set; get;}
        public DateTime? LastModifiedDate { set; get; }
        public string LastModifiedBy { set; get; }
    }

    //start david
    public class RecipientRow : RecipientModel
    {
        public int RowID { get; set; }
    }
    public class RecipientGrid : Grid
    {
        public IList<RecipientRow> Rows { get; set; }
    }

    //end david

    public class RecipientRoleMappingModel
    {
        public long DFRecipientRoleMappingID {set; get;}
	    public int DFRecipientID {set; get;}
	    public int RoleID {set; get;}
	    public bool IsDeleted {set; get;}
        public DateTime? LastModifiedDate { set; get; }
        public string LastModifiedBy { set; get; }
    }

    public class TransactionDFModel 
    {
        public long TransactionDFID { set; get; }
        public long TransactionID { set; get; }
        public string Client { set; get; }
        public int? TransactionTypeID { set; get; }
        public string RefNumber { set; get; }
        public int? DFRecipientID { set; get; }
        public int? Sheets { set; get; }
        public  string Remarks { set; get; }
        public string Others { set; get; }
        public DateTime? LastModifiedDate { set; get; }
        public string LastModifiedBy { set; get; }

    }

    public class DocumentModel
    {
        public long ID { get; set; }
        public DocumentTypeModel Type { get; set; }
        public DocumentPurposeModel Purpose { get; set; }
        public string FileName { get; set; }
        public DocumentPathModel DocumentPath { get; set; }
        //public string DocumentPath { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
    }

    public class TransactionTypeDFModel
    {
        public long TransactionTypeDFID { set; get; }
        public string TypeName { set; get; }
        public bool IsDeleted { set; get; }
        public DateTime? LastModifiedDate { set; get; }
        public string LastModifiedBy { set; get; }
    }

    public class DocumentFlowModel
    {
        public long TransactionDFID { set; get; }
        public long TransactionID { set; get; }
        public string ApplicationID { set; get; }
        public string Client { set; get; }
        public int? TransactionTypeID { set; get; }
        public string RefNumber { set; get; }
        public int? DFRecipientID { set; get; }
        public int? Sheets { set; get; }
        public string Remarks { set; get; }
        public string Others { set; get; }
        public DateTime? LastModifiedDate { set; get; }
        public string LastModifiedBy { set; get; }
        public bool IsDraft { set; get; }
        public int ProductID { set; get; }
        public IList<DocumentModel> Documents { set; get; }
        public bool? IsTopUrgent { set; get; }
        public bool? IsUrgent { set; get; }
        public bool? IsNormal { set; get; }
        public bool? IsSignatureVerification { set; get; }
    }

    public class DocumentFlowDataModel : DocumentFlowModel
    {
        public Guid? WorkflowInstanceID { get; set; }
        public RecipientModel DFRecipient { get; set; }
        public TransactionTypeModel TransactionType { get; set; }
        public ProductModel Product { get; set; }

    }
    public class TransactionDocumentFlowModel
    {
        public DocumentFlowDataModel Transaction { set; get; }
        public IList<TransactionTimelineModel> Timelines { get; set; }
        public IList<VerifyModel> Verify { get; set; }

    }
    #endregion

    #region Filter
    public class RecipientFilter : Filter { }
    #endregion

    #region Interface
    public interface IDocumentFlowRepository : IDisposable
    {
        bool GetRecepient(ref IList<RecipientModel> datas, ref string message);
        //bool GetTransactionType(ref IList<TransactionTypeDFModel> datas, ref string message);
        bool GetTransactionDF(ref IList<TransactionDFModel> datas, ref string message);
        bool GetTransactionDFByClient(string key, int limit, ref IList<TransactionDFModel> datas, ref string message);
        bool GetTransactionDFById(long Id, ref TransactionDFModel data, ref string message);
        bool AddTransactionDF(DocumentFlowModel data, ref long TransactionId, ref string ApplicationId, ref string message);
        bool DeleteTransactionDraftByID(int id, ref string Message);
        bool GetTransactionTimeline(Guid workflowInstanceID, ref IList<TransactionTimelineModel> timelines, ref string message);
        bool GetTransactionDF(Guid instanceID, ref DocumentFlowDataModel transaction, ref string message);
        //bool GetCheckerDF(Guid instanceID, long approverID, ref TransactionDocumentFlowModel output, ref string message);
        bool AddTransactionChecker(Guid workflowInstanceID, long approverID, TransactionDocumentFlowModel transactionChecker, ref string message);
        bool AddTransactionDFMaker(Guid workflowInstanceID, long approverID, DocumentFlowModel transaction, ref string message);
        bool GetDocumentFlowDraft(long id, ref DocumentFlowDataModel output, ref string message);
    }

    public interface IRecipientRepository : IDisposable
    {
        bool GetRecipientByID(int id, ref RecipientModel recipient, ref string message);
        bool GetRecipient(ref IList<RecipientModel> recipients, int limit, int index, ref string message);
        bool GetRecipient(ref IList<RecipientModel> recipients, ref string message);
        bool GetRecipient(int page, int size, IList<RecipientFilter> filters, string sortColumn, string sortOrder, ref RecipientGrid Recipient, ref string message);
        bool GetRecipient(RecipientFilter filter, ref IList<RecipientModel> recipients, ref string message);
        bool GetRecipient(string key, int limit, ref IList<RecipientModel> recipients, ref string message);
        bool AddRecipient(RecipientModel recipient, ref string message);
        bool UpdateRecipient(int id, RecipientModel recipient, ref string message);
        bool DeleteRecipient(int id, ref string message);
    }
    #endregion

    #region Repository
    public class DocumentFlowRepository : IDocumentFlowRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();
        private bool disposed = false;

        public bool GetRecepient(ref IList<RecipientModel> datas, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    datas = (from a in context.DFRecipients
                             where a.IsDeleted.Equals(false)
                             select new RecipientModel
                             {
                                 DFRecipientID = a.DFRecipientID,
                                 DFRecipientName = a.DFRecipientName,
                                 IsDeleted = a.IsDeleted,                                 
                                 LastModifiedBy = string.IsNullOrEmpty(a.UpdateBy) ? a.CreateBy : a.UpdateBy,
                                 LastModifiedDate = a.UpdateDate.HasValue ? a.UpdateDate : a.CreateDate
                             }).ToList();
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }

        //public bool GetTransactionType(ref IList<TransactionTypeDFModel> datas, ref string message)
        //{
        //    bool IsSuccess = false;
        //    try
        //    {
        //        using (DBSEntities context = new DBSEntities())
        //        {
        //            datas = (from a in context.DFTransactionTypes
        //                     where a.IsDeleted.Equals(false)
        //                     select new TransactionTypeDFModel
        //                     {
        //                         TransactionTypeDFID = a.TransactionTypeDFID,
        //                         TypeName = a.TypeName,
        //                         IsDeleted = a.IsDeleted,
        //                         LastModifiedBy = string.IsNullOrEmpty(a.UpdateBy) ? a.CreateBy : a.UpdateBy,
        //                         LastModifiedDate = a.UpdateDate.HasValue ? a.UpdateDate : a.CreateDate
        //                     }).ToList();
        //        }

        //        IsSuccess = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        message = ex.Message;
        //    }

        //    return IsSuccess;
        //}

        public bool GetTransactionDF(ref IList<TransactionDFModel> datas, ref string message)
        {
            bool IsSuccess = false;
            return IsSuccess;
        }

        public bool GetTransactionDFByClient(string key, int limit, ref IList<TransactionDFModel> datas, ref string message)
        {
            bool isSuccess = false;

            try
            {
                datas = (from a in context.TransactionDFs
                             where a.Client.Contains(key.Trim())
                             orderby new { a.Client }
                             select new TransactionDFModel
                             {
                                 TransactionDFID = a.TransactionDFID,
                                 TransactionID = a.TransactionID,
                                 TransactionTypeID = a.TransactionTypeID,
                                 Client = a.Client
                             }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetTransactionDFById(long Id, ref TransactionDFModel data, ref string message)
        {
            bool IsSuccess = false;
            return IsSuccess;
        }

        public bool AddTransactionDF(DocumentFlowModel data, ref long TransactionId, ref string ApplicationId, ref string message)
        {
            bool IsSuccess = false;
            long TrIDInserted = 0;
            var loginName = currentUser.GetCurrentUser().LoginName;
            using (DBSEntities context_ts = new DBSEntities())
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    try
                    {
                        if (data.IsDraft)
                        {
                            #region Save draft
                            if (data.TransactionID != null && data.TransactionID > 0)//dulu draft sekarang draft lagi
                            {
                                Entity.TransactionDraft transDraft = context.TransactionDrafts.Where(a => a.TransactionID.Equals(data.TransactionID)).SingleOrDefault();
                                if (transDraft != null)
                                {
                                    #region Unused field but Not Null
                                    transDraft.BeneName = "-";
                                    transDraft.BankID = 1;
                                    transDraft.IsResident = false;
                                    transDraft.IsCitizen = false;
                                    transDraft.AmountUSD = 0.00M;
                                    transDraft.Rate = 0.00M;
                                    transDraft.DebitCurrencyID = 1;
                                    transDraft.BizSegmentID = 1;
                                    transDraft.ChannelID = 1;
                                    transDraft.CIF = "DUMMYCIF";
                                    #endregion

                                    #region Primary Field                                    
                                    transDraft.ProductID = data.ProductID;
                                    transDraft.TransactionID = data.TransactionID;
                                    transDraft.ApplicationID = data.ApplicationID;
                                    transDraft.IsDraft = data.IsDraft;
                                    transDraft.IsNewCustomer = null;
                                    transDraft.CreateDate = DateTime.UtcNow;
                                    transDraft.CreateBy = loginName;
                                    transDraft.ApplicationDate = DateTime.UtcNow;
                                    transDraft.CurrencyID = (int)CurrencyID.NullCurrency;
                                    transDraft.Amount = 0;
                                    transDraft.IsTopUrgent = 0;
                                    transDraft.StateID = (int)StateID.OnProgress;
                                    transDraft.StaffID = null;
                                    transDraft.StaffTaggingID = null;
                                    transDraft.ATMNumber = null;
                                    transDraft.IsLOI = null;
                                    transDraft.IsPOI = null;
                                    transDraft.IsPOA = null;
                                    transDraft.LocationName = null;
                                    transDraft.AccountNumber = null;
                                    #endregion
                                    context_ts.SaveChanges();

                                    #region document flow draft
                                    Entity.TransactionDFDraft transDFDraft = context_ts.TransactionDFDrafts.Where(a => a.TransactionID.Equals(data.TransactionID)).SingleOrDefault();
                                    if (transDFDraft != null)
                                    {
                                        transDFDraft.Client = data.Client;
                                        transDFDraft.DFRecipientID = data.DFRecipientID;
                                        transDFDraft.Others = data.Others;
                                        transDFDraft.RefNumber = data.RefNumber;
                                        transDFDraft.Remarks = data.Remarks;
                                        transDFDraft.Sheets = data.Sheets;
                                        transDFDraft.TransactionDFID = data.TransactionDFID;
                                        transDFDraft.TransactionID = data.TransactionID;
                                        transDFDraft.TransactionTypeID = data.TransactionTypeID;
                                        transDFDraft.UpdateBy = loginName;
                                        transDFDraft.IsSignatureVerification = data.IsSignatureVerification;
                                        bool topUrgent = data.IsTopUrgent ?? false;
                                        bool urgent = data.IsUrgent ?? false;
                                        bool normal = data.IsNormal ?? false;
                                        transDFDraft.Urgent = topUrgent == true ? "1" : (urgent == true ? "2" : (normal == true ? "3" : null));
                                        transDFDraft.UpdateDate = DateTime.UtcNow; 
                                        context_ts.SaveChanges();
                                    }                                    
                                    #endregion

                                    #region Save Document
                                    var existingDocs = context.TransactionDocumentDrafts.Where(a => a.TransactionID.Equals(data.TransactionID)).ToList();
                                    if (existingDocs != null)
                                    {
                                        if (existingDocs.Count() > 0)
                                        {
                                            foreach (var item in existingDocs)
                                            {
                                                var deleteDoc = context.TransactionDocumentDrafts.Where(a => a.TransactionDocumentID.Equals(item.TransactionDocumentID)).SingleOrDefault();
                                                deleteDoc.IsDeleted = true;
                                                context.SaveChanges();
                                            }
                                        }
                                    }
                                    if (data.Documents != null)
                                    {
                                        if (data.Documents.Count > 0)
                                        {
                                            foreach (var item in data.Documents)
                                            {
                                                Entity.TransactionDocumentDraft document = new Entity.TransactionDocumentDraft()
                                                {
                                                    TransactionID = data.TransactionID,
                                                    DocTypeID = item.Type.ID,
                                                    PurposeID = item.Purpose.ID,
                                                    Filename = item.FileName,
                                                    DocumentPath = item.DocumentPath.DocPath,
                                                    CreateDate = DateTime.UtcNow,
                                                    CreateBy = loginName
                                                };
                                                context_ts.TransactionDocumentDrafts.Add(document);
                                            }
                                            context_ts.SaveChanges();
                                        }
                                    }
                                    #endregion
                                }
                            }
                            else
                            {//draft baru
                                bool isBringup = false;
                                //if (data.IsBringupTask == true)
                                    //isBringup = true;
                                DBS.Entity.TransactionDraft transDraft = new DBS.Entity.TransactionDraft();
                                transDraft.BeneName = "-";
                                transDraft.BankID = 1;
                                transDraft.IsResident = false;
                                transDraft.IsCitizen = false;
                                transDraft.AmountUSD = 0.00M;
                                transDraft.Rate = 0.00M;
                                transDraft.DebitCurrencyID = 1;
                                transDraft.BizSegmentID = 1;
                                transDraft.ChannelID = 1;
                                transDraft.CIF = "DUMMYCIF";

                                transDraft.ProductID = data.ProductID;
                                transDraft.IsDraft = data.IsDraft;
                                    transDraft.IsNewCustomer = null;
                                    transDraft.CreateDate = DateTime.UtcNow;
                                    transDraft.CreateBy = loginName;
                                    transDraft.ApplicationDate = DateTime.UtcNow;
                                    transDraft.CurrencyID = (int)CurrencyID.NullCurrency;
                                    transDraft.Amount = 0;
                                    transDraft.IsTopUrgent = 0;
                                    transDraft.StateID = (int)StateID.OnProgress;
                                    transDraft.StaffID = null;
                                    transDraft.StaffTaggingID = null;
                                    transDraft.ATMNumber = null;
                                    transDraft.IsLOI = null;
                                    transDraft.IsPOI = null;
                                    transDraft.IsPOA = null;
                                    transDraft.LocationName = null;
                                    transDraft.AccountNumber = null;

                                //data.IsBringUp = isBringup;

                                context_ts.TransactionDrafts.Add(transDraft);
                                context_ts.SaveChanges();

                                TransactionId = transDraft.TransactionID;

                                #region Submit Transaction DF
                                Entity.TransactionDFDraft transDFDraft = new Entity.TransactionDFDraft();
                                transDFDraft.TransactionID = TransactionId;
                                transDFDraft.Client = data.Client;
                                transDFDraft.CreateBy = loginName;
                                transDFDraft.CreateDate = DateTime.UtcNow;
                                transDFDraft.DFRecipientID = data.DFRecipientID;
                                transDFDraft.RefNumber = data.RefNumber;
                                transDFDraft.Remarks = data.Remarks;
                                transDFDraft.Sheets = data.Sheets;
                                transDFDraft.TransactionTypeID = data.TransactionTypeID;
                                transDFDraft.Others = data.Others;
                                transDFDraft.IsSignatureVerification = data.IsSignatureVerification;
                                bool topUrgent = data.IsTopUrgent ?? false;
                                bool urgent = data.IsUrgent ?? false;
                                bool normal = data.IsNormal ?? false;
                                transDFDraft.Urgent = topUrgent == true ? "1" : (urgent == true ? "2" : (normal == true ? "3" : null));
                                context_ts.TransactionDFDrafts.Add(transDFDraft);
                                context_ts.SaveChanges();
                                #endregion


                                #region Save Document
                                var existingDocs = context.TransactionDocumentDrafts.Where(a => a.TransactionID.Equals(data.TransactionID)).ToList();
                                if (existingDocs != null)
                                {
                                    if (existingDocs.Count() > 0)
                                    {
                                        foreach (var item in existingDocs)
                                        {
                                            var deleteDoc = context.TransactionDocumentDrafts.Where(a => a.TransactionDocumentID.Equals(item.TransactionDocumentID)).SingleOrDefault();
                                            deleteDoc.IsDeleted = true;
                                            context.SaveChanges();
                                        }
                                    }
                                }
                                if (data.Documents != null)
                                {
                                    if (data.Documents.Count > 0)
                                    {
                                        foreach (var item in data.Documents)
                                        {
                                            Entity.TransactionDocumentDraft document = new Entity.TransactionDocumentDraft()
                                            {
                                                TransactionID = TransactionId,
                                                DocTypeID = item.Type.ID,
                                                PurposeID = item.Purpose.ID,
                                                Filename = item.FileName,
                                                DocumentPath = item.DocumentPath.DocPath,
                                                CreateDate = DateTime.UtcNow,
                                                CreateBy = loginName
                                            };
                                            context_ts.TransactionDocumentDrafts.Add(document);
                                        }
                                        context_ts.SaveChanges();
                                    }
                                }
                                #endregion

                                #region BringUp
                                //if (data.IsBringupTask == true)
                                //{
                                //    Entity.TransactionBringupTask Bringup = new Entity.TransactionBringupTask()
                                //    {
                                //        TransactionID = TrIDInserted,
                                //        TaskName = "Bring Up Retail CIF",
                                //        AssignedTo = currentUser.GetCurrentUser().LoginName,
                                //        AssignedFrom = currentUser.GetCurrentUser().LoginName,
                                //        DateIn = DateTime.UtcNow,
                                //        IsSubmitted = false,
                                //        CreateDate = DateTime.UtcNow,
                                //        CreateBy = currentUser.GetCurrentUser().LoginName,
                                //    };
                                //    context_ts.TransactionBringupTasks.Add(Bringup);
                                //    context_ts.SaveChanges();
                                //}
                                #endregion
                                ts.Complete();
                                IsSuccess = true;
                            }
                            #endregion
                        }
                        else
                        {                                
                            #region Submit Transaction
                            Entity.Transaction trans = new Entity.Transaction();
                            trans.IsSignatureVerified = false;
                            trans.IsDormantAccount = false;
                            trans.IsFrezeAccount = false;
                            trans.BeneName = "-";
                            trans.BankID = 1;
                            trans.IsResident = false;
                            trans.IsCitizen = false;
                            trans.AmountUSD = 0.00M;
                            trans.Rate = 0.00M;
                            trans.DebitCurrencyID = 1;
                            trans.BizSegmentID = 1;
                            trans.ChannelID = 1;
                            trans.IsNewCustomer = false;
                            trans.CIF = "DUMMYCIF";
                            trans.CurrencyID = (int)CurrencyID.NullCurrency;
                            trans.Amount = 0;
                            trans.IsTopUrgent = 0;
                            trans.IsDocumentComplete = false;
                            trans.StaffID = null;
                            trans.LocationID = context_ts.Employees.Where(x => x.EmployeeUsername.Equals(loginName) && x.IsDeleted.Equals(false)).Select(x => x.LocationID).FirstOrDefault();
                            trans.ProductID = data.ProductID;
                            trans.IsDraft = data.IsDraft;                                
                            trans.CreateDate = DateTime.UtcNow;
                            trans.CreateBy = loginName;
                            trans.ApplicationDate = DateTime.UtcNow;                                
                            trans.StateID = (int)StateID.OnProgress;
                            trans.StaffTaggingID = null;
                            trans.ATMNumber = null;
                            trans.IsLOI = null;
                            trans.IsPOI = null;
                            trans.IsPOA = null;
                            trans.LocationName = null;
                            trans.AccountNumber = null;
                            trans.BranchName = null;
                            trans.TouchTimeStartDate = null;
                            trans.TransactionTypeID = data.TransactionTypeID;       
                            trans.CBOMaintainID = null;
                            trans.TransactionSubTypeID = null;

                            context_ts.Transactions.Add(trans);
                            context_ts.SaveChanges();
                            #endregion

                            #region add transaction id
                            TransactionId = trans.TransactionID;
                            TrIDInserted = TransactionId;
                            #endregion

                            #region Submit Transaction DF
                            Entity.TransactionDF transDF = new Entity.TransactionDF();
                            transDF.TransactionID = TransactionId;
                            transDF.Client = data.Client;
                            transDF.CreateBy = loginName;
                            transDF.CreateDate = DateTime.UtcNow;
                            transDF.DFRecipientID = data.DFRecipientID;
                            transDF.RefNumber = data.RefNumber;
                            transDF.Remarks = data.Remarks;
                            transDF.Sheets = data.Sheets;
                            transDF.TransactionTypeID = data.TransactionTypeID;
                            transDF.Others = data.Others;
                            transDF.IsSignatureVerification = data.IsSignatureVerification;
                            bool topUrgent = data.IsTopUrgent ?? false;
                            bool urgent = data.IsUrgent ?? false;
                            bool normal = data.IsNormal ?? false;
                            transDF.Urgent = topUrgent == true ? "1" : (urgent == true ? "2" : ( normal == true ? "3" : null ));
                            context_ts.TransactionDFs.Add(transDF);
                            context_ts.SaveChanges();
                            #endregion
                            
                            #region Document
                            if (data.Documents != null)
                            {
                                if (data.Documents.Count > 0)
                                {
                                    foreach (var item in data.Documents)
                                    {
                                        Entity.TransactionDocument document = new Entity.TransactionDocument()
                                        {
                                            TransactionID = TransactionId,
                                            DocTypeID = item.Type.ID,
                                            PurposeID = item.Purpose.ID,
                                            Filename = item.FileName,
                                            DocumentPath = item.DocumentPath.DocPath,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = loginName
                                        };

                                        context_ts.TransactionDocuments.Add(document);
                                    }

                                    context_ts.SaveChanges();
                                }
                            }

                            #endregion                            

                            ts.Complete();
                            IsSuccess = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        ts.Dispose();
                        message = ex.Message;
                    }

                }
            }
            if (!data.IsDraft)
            {
                using (DBSEntities context = new DBSEntities())
                {
                    var getTR = (from TR in context.Transactions
                                 where TR.TransactionID == TrIDInserted
                                 select TR).SingleOrDefault();
                    if(getTR != null) {
                        ApplicationId = getTR.ApplicationID;
                    }
                    
                    IsSuccess = true;
                }
            }
            return IsSuccess;
        }

        public bool DeleteTransactionDraftByID(int id, ref string Message)
        {
            bool isDelete = false;

            try
            {
                Entity.TransactionDraft data = context.TransactionDrafts.Where(a => a.TransactionID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    var existingDocs = context.TransactionDocumentDrafts.Where(a => a.TransactionID.Equals(id)).ToList();
                    if (existingDocs != null)
                    {
                        if (existingDocs.Count() > 0)
                        {
                            foreach (var item in existingDocs)
                            {
                                var deleteDoc = context.TransactionDocumentDrafts.Where(a => a.TransactionDocumentID.Equals(item.TransactionDocumentID)).SingleOrDefault();
                                context.TransactionDocumentDrafts.Remove(deleteDoc);
                                context.SaveChanges();
                            }
                        }
                    }

                    context.TransactionDrafts.Remove(data);
                    context.SaveChanges();

                    isDelete = true;
                }
                else
                {
                    Message = string.Format("Draft ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }

            return isDelete;
        }
        public bool GetTransactionTimeline(Guid workflowInstanceID, ref IList<TransactionTimelineModel> timelines, ref string message)
        {
            bool IsSuccess = false;

            try
            {

                timelines = (from a in context.SP_GetNintexTaskTimeline(workflowInstanceID)
                             select new TransactionTimelineModel
                             {
                                 ApproverID = a.WorkflowApproverID,
                                 Activity = a.ActivityTitle,
                                 Time = a.ActivityTime,
                                 UserOrGroup = a.UserOrGroup,
                                 IsGroup = a.IsSPGroup.Value,
                                 Outcome = a.Outcome,
                                 DisplayName = a.DisplayName,
                                 Comment = a.Comment
                             }).ToList();

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);

            GC.Collect();
            GC.SuppressFinalize(this);
        }
        public bool GetTransactionDF(Guid instanceID, ref DocumentFlowDataModel transaction, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                context.Transactions.AsNoTracking();
                var trans = (from a in context.Transactions
                               where a.WorkflowInstanceID.Value.Equals(instanceID)
                               select new DocumentFlowDataModel {
                                TransactionID = a.TransactionID,
                                TransactionTypeID = a.TransactionTypeID,
                                ApplicationID = a.ApplicationID,
                                IsDraft = a.IsDraft,
                                ProductID = a.ProductID,
                                WorkflowInstanceID = a.WorkflowInstanceID,
                                LastModifiedBy = string.IsNullOrEmpty(a.UpdateBy) ? a.CreateBy : a.UpdateBy,
                                LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                               }).SingleOrDefault();
                if (trans != null)
                {
                    long ID = trans.TransactionID;
                    var tdf = context.TransactionDFs.Where(b => b.TransactionID == ID).Select(x => x).SingleOrDefault();
                    var dm = context.TransactionDocuments.Where(c => c.TransactionID == ID).Where(x=>x.IsDeleted == false).Select(xx => new DocumentModel()
                    {
                        ID = xx.TransactionDocumentID,
                        FileName = xx.Filename,
                        DocumentPath = new DocumentPathModel { name = xx.Filename, lastModified = string.Empty, lastModifiedDate = DateTime.Now, DocPath = xx.DocumentPath, type = Util.ExistingDoctype },
                        Purpose = new DocumentPurposeModel() { ID = xx.DocumentPurpose.PurposeID, Name = xx.DocumentPurpose.PurposeName, Description = xx.DocumentPurpose.PurposeDescription},
                        Type = new DocumentTypeModel() { ID = xx.DocumentType.DocTypeID, Name = xx.DocumentType.DocTypeName, Description = xx.DocumentType.DocTypeDescription },
                        LastModifiedBy = string.IsNullOrEmpty(xx.UpdateBy) ? xx.CreateBy : xx.UpdateBy,
                        LastModifiedDate = xx.UpdateDate == null ? xx.CreateDate : xx.UpdateDate.Value
                    }).ToList();
                    if(tdf != null) {
                        trans.Client = tdf.Client;
                        trans.DFRecipientID = tdf.DFRecipientID;
                        trans.Others = tdf.Others;
                        trans.RefNumber = tdf.RefNumber;
                        trans.Remarks = tdf.Remarks;
                        trans.Sheets = tdf.Sheets;
                        trans.TransactionDFID = tdf.TransactionDFID;
                        trans.TransactionTypeID = tdf.TransactionTypeID;
                        trans.IsTopUrgent = tdf.Urgent.Trim() == "1" ? true : false;
                        trans.IsUrgent = tdf.Urgent.Trim() == "2" ? true : false;
                        trans.IsNormal = tdf.Urgent.Trim() == "3" ? true : false;
                        trans.IsSignatureVerification = tdf.IsSignatureVerification;
                        if (dm != null && dm.Count > 0)
                        {
                            trans.Documents = dm;
                        } else
                        {
                            trans.Documents = new List<DocumentModel>();
                        }
                        if (trans.TransactionTypeID.HasValue)
                        {
                            var transType = context.TransactionTypes.Where(d => d.TransTypeID == trans.TransactionTypeID.Value).Select(xxx => new TransactionTypeModel()
                            {
                                ID = xxx.TransTypeID,
                                ParameterTypeDisplay = xxx.TransactionType1
                            }).SingleOrDefault();
                            if (transType != null) { trans.TransactionType = transType; } else { trans.TransactionType = new TransactionTypeModel(); }
                        }
                        if(trans.DFRecipientID.HasValue) {
                            var recipient = context.DFRecipients.Where(e => e.DFRecipientID == trans.DFRecipientID.Value).Select(z=> new RecipientModel(){
                                DFRecipientID = z.DFRecipientID,
                                DFRecipientName = z.DFRecipientName
                            }).SingleOrDefault();
                            if(recipient != null) {trans.DFRecipient = recipient;} else {trans.DFRecipient = new RecipientModel();}
                        }
                        if(trans.ProductID > 0) {
                            var product = context.Products.Where(f=>f.ProductID == trans.ProductID).Select(zz=>new ProductModel(){
                                ID = zz.ProductID,
                                Code = zz.ProductCode,
                                Name = zz.ProductName
                            }).SingleOrDefault();
                            if (product != null) { trans.Product = product; } else { trans.Product = new ProductModel(); }
                        }
                        
                    }
                    transaction = trans;
                    IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }
            return IsSuccess;
        }
        //public bool GetCheckerDF(Guid instanceID, long approverID, ref TransactionDocumentFlowModel output, ref string message)
        //{
        //    bool IsSuccess = false;
        //    try
        //    {
        //        output = (from trans in context.Transactions.AsNoTracking()
        //                  where trans.WorkflowInstanceID.Value == instanceID
        //                  select new TransactionDocumentFlowModel
        //                  {
        //                      Verify = trans.TransactionDFCheckers
        //                          .Where(x => x.TransactionID.Equals(trans.TransactionID) && x.IsDeleted == false)
        //                          .Select(x => new VerifyModel()
        //                          {
        //                              ID = x.TransactionColumnID,
        //                              Name = x.TransactionColumn.ColumnName,
        //                              IsVerified = x.IsVerified
        //                          }).ToList()
        //                  }).SingleOrDefault();
        //        if (output != null)
        //        {
        //            if (!output.Verify.Any())
        //            {
        //                output.Verify = context.TransactionColumns
        //                    .Where(x => x.IsCheckerDF.Value.Equals(true))
        //                    .Where(x => x.IsDeleted.Equals(false))
        //                    .Select(x => new VerifyModel()
        //                    {
        //                        ID = x.TransactionColumnID,
        //                        Name = x.ColumnName,
        //                        IsVerified = false
        //                    }).ToList();
        //            }
        //        }
        //        IsSuccess = true;
        //    } catch(Exception ex){
        //        message = ex.Message;
        //    }
        //    return IsSuccess;
        //}
        public bool AddTransactionChecker(Guid workflowInstanceID, long approverID, TransactionDocumentFlowModel transactionChecker, ref string message) {
            bool IsSuccess = false;
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    Entity.Transaction dataTrans = context.Transactions.Where(x => x.WorkflowInstanceID.Value == workflowInstanceID).SingleOrDefault();
                    if (dataTrans != null)
                    {
                        //var checker = context.TransactionDFCheckers
                        //   .Where(a => a.TransactionID == dataTrans.TransactionID && a.IsDeleted == false)
                        //   .ToList();

                        //if (checker.Count > 0)
                        //{
                        //    var checkerdata = context.TransactionDFCheckers.Where(a => a.TransactionID == dataTrans.TransactionID).ToList();
                        //    foreach (var item in checkerdata)
                        //    {
                        //        context.TransactionDFCheckers.Remove(item);
                        //    }
                        //    context.SaveChanges();

                        //    foreach (var item in transactionChecker.Verify)
                        //    {
                        //        context.TransactionDFCheckers.Add(new TransactionDFChecker()
                        //        {
                        //            ApproverID = approverID,
                        //            TransactionID = dataTrans.TransactionID,
                        //            TransactionColumnID = item.ID,
                        //            IsVerified = item.IsVerified,
                        //            IsDeleted = false,
                        //            CreateDate = DateTime.UtcNow,
                        //            CreateBy = currentUser.GetCurrentUser().LoginName
                        //        });

                        //        context.SaveChanges();
                        //    }
                        //}
                        //else
                        //{
                        //    foreach (var item in transactionChecker.Verify)
                        //    {
                        //        context.TransactionDFCheckers.Add(new TransactionDFChecker()
                        //        {
                        //            ApproverID = approverID,
                        //            TransactionID = dataTrans.TransactionID,
                        //            TransactionColumnID = item.ID,
                        //            IsVerified = item.IsVerified,
                        //            IsDeleted = false,
                        //            CreateDate = DateTime.UtcNow,
                        //            CreateBy = currentUser.GetCurrentUser().LoginName
                        //        });

                        //        context.SaveChanges();
                        //    }
                        //}
                        //checker = null;
                    }                    

                    ts.Complete();

                    IsSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    ts.Dispose();
                }
            }
            return IsSuccess;
        }
        public bool AddTransactionDFMaker(Guid workflowInstanceID, long approverID, DocumentFlowModel transaction, ref string message) {
            var IsSuccess = false;
            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    var data = context.Transactions.Where(a => a.WorkflowInstanceID.Value == workflowInstanceID).SingleOrDefault();
                    if (data != null)
                    {
                        
                        data.CIF = "DUMMYCIF";
                        context.SaveChanges();
                        long TransID = transaction.TransactionID;
                        var transDF = context.TransactionDFs.Where(a => a.TransactionID == TransID).SingleOrDefault();
                        if (transDF != null)
                        {
                            transDF.Client = transaction.Client;
                            transDF.CreateBy = currentUser.GetCurrentUser().LoginName;
                            transDF.CreateDate = DateTime.UtcNow;
                            transDF.DFRecipientID = transaction.DFRecipientID;
                            transDF.RefNumber = transaction.RefNumber;
                            transDF.Remarks = transaction.Remarks;
                            transDF.Sheets = transaction.Sheets;
                            transDF.TransactionTypeID = transaction.TransactionTypeID;
                            transDF.Others = transaction.Others;
                             bool topUrgent = transaction.IsTopUrgent ?? false;
                            bool urgent = transaction.IsUrgent ?? false;
                            bool normal = transaction.IsNormal ?? false;
                            transDF.Urgent = topUrgent == true ? "1" : (urgent == true ? "2" : (normal == true ? "3" : null));
                            transDF.IsSignatureVerification = transaction.IsSignatureVerification;
                            context.SaveChanges();
                        }                       
                        
                        #region Save Document
                        var existingDocs = context.TransactionDocuments.Where(a => a.TransactionID.Equals(TransID) && a.IsDeleted == false).ToList();
                        if (existingDocs != null)
                        {
                            if (existingDocs.Count > 0)
                            {
                                foreach (var item2 in existingDocs)
                                {
                                    var deleteDoc = context.TransactionDocuments.Where(a => a.TransactionDocumentID.Equals(item2.TransactionDocumentID)).SingleOrDefault();
                                    deleteDoc.IsDeleted = true;
                                    context.SaveChanges();
                                }
                            }
                        }
                        if (transaction.Documents != null)
                        {
                            if (transaction.Documents.Count > 0)
                            {
                                foreach (var item3 in transaction.Documents)
                                {
                                    Entity.TransactionDocument document = new Entity.TransactionDocument()
                                    {
                                        TransactionID = TransID,
                                        DocTypeID = item3.Type.ID,
                                        PurposeID = item3.Purpose.ID,
                                        Filename = item3.FileName,
                                        DocumentPath = item3.DocumentPath.DocPath,
                                        CreateDate = DateTime.UtcNow,
                                        CreateBy = currentUser.GetCurrentUser().LoginName
                                    };
                                    context.TransactionDocuments.Add(document);
                                }
                                context.SaveChanges();
                            }
                        }
                        
                        #endregion

                    }
                    ts.Complete();
                    IsSuccess = true;
                }

                catch (Exception ex)
                {
                    ts.Dispose();
                    message = ex.Message;
                }
            }
            return IsSuccess;
        }
        public bool GetDocumentFlowDraft(long id, ref DocumentFlowDataModel output, ref string message) {
            bool IsSuccess = false;

            try
            {
                context.Transactions.AsNoTracking();

                DocumentFlowDataModel data = (from a in context.TransactionDrafts
                                                         where a.TransactionID.Equals(id)
                                                         select new DocumentFlowDataModel
                                                         {
                                                             IsDraft = a.IsDraft.Value,
                                                             ProductID = a.ProductID.Value,
                                                             TransactionID = a.TransactionID
                                                         }).SingleOrDefault();

                // fill CIF Retail data
                if (data != null)
                {
                    var dataDFDraft = (from x in context.TransactionDFDrafts
                                       where x.TransactionID == data.TransactionID
                                       select x).SingleOrDefault();
                    if (dataDFDraft != null)
                    {
                        data.Client = dataDFDraft.Client;
                        data.DFRecipientID = dataDFDraft.DFRecipientID;
                        data.Others = dataDFDraft.Others;
                        data.RefNumber = dataDFDraft.RefNumber;
                        data.Remarks = dataDFDraft.Remarks;
                        data.Sheets = dataDFDraft.Sheets;
                        data.TransactionTypeID = dataDFDraft.TransactionTypeID;
                        data.IsTopUrgent = dataDFDraft.Urgent.Trim() == "1" ? true : false;
                        data.IsUrgent = dataDFDraft.Urgent.Trim() == "2" ? true : false;
                        data.IsNormal = dataDFDraft.Urgent.Trim() == "3" ? true : false;
                        data.IsSignatureVerification = dataDFDraft.IsSignatureVerification;
                    }



                    IList<DocumentModel> DocDraft = (from doc in context.TransactionDocumentDrafts
                                                     where doc.TransactionID == id && doc.IsDeleted.Equals(false)
                                                     select new DocumentModel
                                                     {                                                        
                                                        ID = doc.TransactionDocumentID,
                                                        LastModifiedBy = doc.UpdateBy == null ? doc.CreateBy : doc.UpdateBy,
                                                        LastModifiedDate = doc.UpdateDate == null ? doc.CreateDate : doc.UpdateDate.Value,
                                                        Purpose = new DocumentPurposeModel { ID = doc.DocumentPurpose.PurposeID, Name = doc.DocumentPurpose.PurposeName },
                                                        Type = new DocumentTypeModel { ID = doc.DocumentType.DocTypeID, Name = doc.DocumentType.DocTypeName },
                                                        FileName = doc.Filename,
                                                        DocumentPath = new DocumentPathModel { name = doc.Filename, lastModified = string.Empty, lastModifiedDate = DateTime.Now, DocPath = doc.DocumentPath, type = Util.ExistingDoctype }
                                                     }).ToList();
                    if (DocDraft != null)
                    {
                        data.Documents = DocDraft;
                    }

                    output = data;
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }
            return IsSuccess;
        }
    }

    // start david
    public class RepicientRepository : IRecipientRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetRecipientByID(int id, ref RecipientModel recipient, ref string message)
        {
            bool isSucces = false;
            try
            {
                recipient = (from a in context.DFRecipients
                             where a.IsDeleted.Equals(false) && a.DFRecipientID.Equals(id)
                             select new RecipientModel
                             {
                                 DFRecipientID = a.DFRecipientID,
                                 DFRecipientName = a.DFRecipientName,
                                 LastModifiedDate = a.CreateDate,
                                 LastModifiedBy = a.CreateBy
                             }).SingleOrDefault();
                isSucces = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSucces;
        }
        public bool GetRecipient(ref IList<RecipientModel> recipients, int limit, int index, ref string message)
        {
            bool isSuccess = false;
            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    recipients = (from a in context.DFRecipients
                                  where a.IsDeleted.Equals(false)
                                  orderby new { a.DFRecipientName }
                                  select new RecipientModel
                                  {
                                      DFRecipientID = a.DFRecipientID,
                                      DFRecipientName = a.DFRecipientName,
                                      LastModifiedDate = a.CreateDate,
                                      LastModifiedBy = a.CreateBy
                                  }).Skip(skip).Take(limit).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool GetRecipient(ref IList<RecipientModel> recipients, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    recipients = (from a in context.DFRecipients
                                  where a.IsDeleted.Equals(false)
                                  orderby new { a.DFRecipientName }
                                  select new RecipientModel
                                  {
                                      DFRecipientID = a.DFRecipientID,
                                      DFRecipientName = a.DFRecipientName,
                                      LastModifiedDate = a.CreateDate,
                                      LastModifiedBy = a.CreateBy
                                  }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool GetRecipient(int page, int size, IList<RecipientFilter> filters, string sortColumn, string sortOrder, ref RecipientGrid customer, ref string message)
        {
            bool isSucces = false;
            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                RecipientModel filter = new RecipientModel();

                if (filters != null)
                {
                    filter.DFRecipientName = filters.Where(a => a.Field.Equals("")).Select(a => a.Value).SingleOrDefault(); // TODO
                    filter.LastModifiedBy = filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse(filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.DFRecipients
                                let isDFRecipientName = !string.IsNullOrEmpty(filter.DFRecipientName)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false

                                where a.IsDeleted.Equals(false) &&
                                (isDFRecipientName ? a.DFRecipientName.Contains(filter.DFRecipientName) : true) &&
                                (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new RecipientModel
                                {
                                    DFRecipientID = a.DFRecipientID,
                                    DFRecipientName = a.DFRecipientName,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });
                    customer.Page = page;
                    customer.Size = size;
                    customer.Total = data.Count();
                    customer.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    customer.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new RecipientRow
                        {
                            RowID = i + 1,
                            DFRecipientID = a.DFRecipientID,
                            DFRecipientName = a.DFRecipientName,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }
                isSucces = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSucces;
        }
        public bool GetRecipient(RecipientFilter filter, ref IList<RecipientModel> recipients, ref string message)
        {
            throw new NotImplementedException();
        }
        public bool GetRecipient(string key, int limit, ref IList<RecipientModel> recipients, ref string message)
        {
            bool isSuccess = false;
            try
            {
                recipients = (from a in context.DFRecipients
                              where a.DFRecipientID.Equals(false) &&
                              a.DFRecipientName.Contains(key)
                              orderby new { a.DFRecipientName }
                              select new RecipientModel
                              {
                                  DFRecipientName = a.DFRecipientName
                              }).Take(limit).ToList();
                isSuccess = true;
            }
            catch (Exception ex)
            { message = ex.Message; }


            return isSuccess;
        }
        public bool AddRecipient(RecipientModel recipient, ref string message)
        {
            bool isSuccess = false;
            try
            {
                if (!context.DFRecipients.Where(a => a.DFRecipientName.Equals(recipient.DFRecipientName) && a.IsDeleted.Equals(false)).Any())
                {
                    context.DFRecipients.Add(new Entity.DFRecipient()
                    {
                        DFRecipientName = recipient.DFRecipientName,
                        IsDeleted = false,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    });
                    context.SaveChanges();
                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Recipient data for recipient name {0} is already exist.", recipient.DFRecipientName);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool UpdateRecipient(int id, RecipientModel recipient, ref string message)
        {
            bool isSuccess = false;
            try
            {
                Entity.DFRecipient data = context.DFRecipients.Where(a => a.DFRecipientID.Equals(id)).SingleOrDefault();
                if (data != null)
                {
                    if (context.DFRecipients.Where(a => a.DFRecipientID != recipient.DFRecipientID && a.DFRecipientName.Equals(recipient.DFRecipientName) && a.IsDeleted.Equals(false)).Count() >= 1)
                    {
                        message = string.Format("recipient name {0} is exist.", recipient.DFRecipientName);
                    }
                    else
                    {
                        data.DFRecipientName = recipient.DFRecipientName;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                        context.SaveChanges();
                        isSuccess = true;
                    }
                }
                else
                {
                    message = string.Format("Recipient data for Recipient ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool DeleteRecipient(int id, ref string message)
        {
            bool isSuccess = false;
            try
            {
                Entity.DFRecipient data = context.DFRecipients.Where(a => a.DFRecipientID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Recipient data for Recipient ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    //end david
    #endregion
    }
