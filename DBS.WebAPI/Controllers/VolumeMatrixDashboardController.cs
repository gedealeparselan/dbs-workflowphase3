﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;

namespace DBS.WebAPI.Controllers
{
    //[Authorize]
    public class VolumeMatrixDashboardController : ApiController
    {
        private string message = string.Empty;

        private readonly IVolumeMatrixDashboardChart repositoryChart = null;

        public VolumeMatrixDashboardController()
        {
            this.repositoryChart = new VolumeMatrixDashboardRepository();

        }
        public VolumeMatrixDashboardController(IVolumeMatrixDashboardChart repositoryChart)
        {
            this.repositoryChart = repositoryChart;

        }
        [ResponseType(typeof(IList<VolumeMatrixDashboardModel>))]

        public HttpResponseMessage Get(string productTypeDetailID, DateTime startDate, DateTime endDate, DateTime clientDateTime, string paymentMode)
        {
            IList<VolumeMatrixDashboardModel> output = new List<VolumeMatrixDashboardModel>();

            if (!repositoryChart.GetVolumeMatrixDashboardChart(productTypeDetailID, startDate, endDate, clientDateTime, paymentMode, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<VolumeMatrixDashboardModel>>(HttpStatusCode.OK, output);
            }

        }
    }

}
