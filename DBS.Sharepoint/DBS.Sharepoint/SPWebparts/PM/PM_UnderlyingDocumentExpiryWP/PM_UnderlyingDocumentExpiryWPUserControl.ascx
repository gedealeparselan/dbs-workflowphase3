﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PM_UnderlyingDocumentExpiryWPUserControl.ascx.cs" Inherits="DBS.Sharepoint.SPWebparts.PM.PM_UnderlyingDocumentExpiryWP.PM_UnderlyingDocumentExpiryWPUserControl" %>


<h1 class="header smaller lighter dark">Underlying Documents Expiry</h1>

<div class="modal-body overflow-visible">
        <div class="row">
            <div class="col-xs-12">
                <div id="customer-form" class="form-horizontal" role="form">

                    <div class="form-group no-margin-bottom no-padding-bottom">
                        <label class="col-sm-2" for="Expiry">
                            Underlying Documents Expiry (Months)
                        </label>

                        <div class="col-sm-5">
                            <div class="clearfix">
                                <input class="col-sm-12" type="text" name="Expiry" id="Expiry" data-in="" data-bind="value: $root.Expiry" data-rule-required = "true" data-rule-value = "true" data-rule-number="true" />
                                <label class="control-label bolder text-danger" style="position:absolute;" for="Name">*</label>
                            </div>
                        </div>

                        <div>
                            <button id="btnUpdateExpiry" class="btn btn-sm btn-primary" data-bind="click: $root.UpdateExpiry, visible: $root.IsRoleMaker()">
                                <i class="icon-save"></i>
                                Save
                            </button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/additional-methods.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.slimscroll.min.js"></script>