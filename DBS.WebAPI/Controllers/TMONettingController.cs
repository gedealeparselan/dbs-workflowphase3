﻿using DBS.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace DBS.WebAPI.Controllers
{
    public class TMONettingController : ApiController
    {
        private string message = string.Empty;
        private readonly ITMONettingRepository repoTMONetting = null;
        public TMONettingController()
        {
            this.repoTMONetting = new TMONettingRepository();
        }

        [HttpPost]
        [ResponseType(typeof(string))]
        [Route("api/TMONetting/Add")]
        public HttpResponseMessage AddTMONettingTransaction(TMONettingModel transaction)
        {
            long transactionID = 0;
            string ApplicationID = string.Empty;
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoTMONetting.AddTMONetting(transaction, ref transactionID, ref ApplicationID, ref message))
                {
                    if (message.ToLower().EndsWith("already exist on workflow"))
                    {
                        return Request.CreateResponse(HttpStatusCode.PreconditionFailed, new { Message = message });
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new
                    {
                        ID = transactionID,
                        AppID = ApplicationID,
                        Message = transaction.IsDraft.Value ? "Transaction has been saved as draft." : "New Transaction has been submitted."
                    });
                }
            }
        }

        [ResponseType(typeof(TMONettingDetailModel))]
        [HttpGet]
        [Route("api/WorkflowTMONetting/{workflowInstanceID}/Transaction/Checker/{approverID}")]
        public HttpResponseMessage GetTMONetting(Guid workflowInstanceID, long approverID)
        {
            TMONettingModel transaction = new TMONettingModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TMONettingDetailModel output = new TMONettingDetailModel();
            TMONettingDetailModel outputChecker = new TMONettingDetailModel();

            try
            {
                if (!repoTMONetting.GetTMONettingDetails(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    if (!repoTMONetting.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        if (!repoTMONetting.GetTMONettingCheckerDetails(workflowInstanceID, approverID, ref outputChecker, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Transaction = transaction;
                            output.Timelines = timelines;
                            output.Checker = outputChecker.Checker;
                            output.Verify = outputChecker.Verify;

                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {
                transaction = null;
                timelines = null;
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowTMONetting/{workflowInstanceID}/Transaction/Checker/{approverID}/{typeapproval}")]
        public HttpResponseMessage AddOrReviseTMONettingCheckerDetails(Guid workflowInstanceID, long approverID, string typeapproval, TMONettingDetailModel transactionChecker)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoTMONetting.AddOrReviseTMONettingChecker(workflowInstanceID, approverID, typeapproval, transactionChecker, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been saved." });
                }
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowTMONetting/{workflowInstanceID}/Transaction/Maker/{approverID}")]
        public HttpResponseMessage SubmitTMONettingMakers(Guid workflowInstanceID, long approverID, TMONettingModel transaction)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoTMONetting.UpdateTMONetting(workflowInstanceID, approverID, transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been saved." });
                }
            }
        }

    }
}