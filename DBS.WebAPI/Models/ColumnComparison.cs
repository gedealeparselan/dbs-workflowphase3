﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using System.Linq.Dynamic;


namespace DBS.WebAPI.Models
{
    #region property
    public class ColumnComparisonModel
    {
        public int FilterID { get; set; }
        public string ColumnName { get; set; }
        public string TableName { get; set; }
        public int? OrdinalPosition { get; set; }

        public Nullable<bool> IsCompare { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
    }

    public class ColumnComparisonModelRow : ColumnComparisonModel
    {
        public int RowID { get; set; }
    }
    public class ColumnComparisonModelGrid : Grid
    {
        public IList<ColumnComparisonModelRow> Rows { get; set; }
    }
    #endregion

    #region Filter
    public class ColumnComparisonModelFilter : Filter { }
    #endregion

    #region interface
    public interface IColumnComparisonModel : IDisposable
    {
        bool GetColumnComparisonByID(int ID, ref ColumnComparisonModel columnComparison, ref string message);
        bool GetColumnComparison(ref IList<ColumnComparisonModel> columnComparison, ref string message);
        //bool GetColumnComparison(string key, int limit, ref IList<ProvinsiModel> provinsi, ref string message);
        bool GetColumnComparison(int page, int size, IList<ColumnComparisonModelFilter> filters, string sortColumn, string sortOrder, ref ColumnComparisonModelGrid columnComparison, ref string message);
        bool AddColumnComparison(ColumnComparisonModel columnComparison, ref string message);

        bool UpdateColumnComparison(int ID, ref ColumnComparisonModel columnComparison, ref string message);

        bool DeleteColumnComparison(int id, ref string message);
        bool GetColumnComparison(string key, int limit, ref IList<ColumnComparisonModel> ColumnComparisons, ref string message);
        bool GetColumnComparisonByTableName(ref IList<ColumnComparisonModel> output, string tableName, ref string message);
    }
    #endregion

    #region Repository
    public class ColumnComparisonRepository : IColumnComparisonModel
    {


        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        currentUser = null;
                        context.Dispose();
                    }
                }
            }
            this.disposed = true;
        }
   
        public bool GetColumnComparisonByID(int ID, ref ColumnComparisonModel columnComparison, ref string message)
        {
            bool isSuccess = false;

            try
            {
                columnComparison = (from a in context.ColumnComparisons                        
                        select new ColumnComparisonModel
                        {
                            FilterID = a.FilterID,
                            ColumnName = a.ColumnName,
                            TableName = a.TableName,
                            OrdinalPosition = a.OrdinalPosition,
                            IsCompare = a.IsCompare,
                            LastModifiedBy = a.CreateBy,
                            LastModifiedDate = a.CreateDate
                        }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetColumnComparison(ref IList<ColumnComparisonModel> columnComparison, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    columnComparison = (from a in context.ColumnComparisons

                            where a.IsDeleted.Equals(false)
                            orderby new { a.ColumnName }
                            select new ColumnComparisonModel
                            {
                                FilterID = a.FilterID,
                                ColumnName = a.ColumnName,
                                TableName = a.TableName,
                                OrdinalPosition = a.OrdinalPosition,
                                IsCompare = a.IsCompare,
                                LastModifiedBy = a.CreateBy,
                                LastModifiedDate = a.CreateDate
                            }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetColumnComparison(int page, int size, IList<ColumnComparisonModelFilter> filters, string sortColumn, string sortOrder, ref ColumnComparisonModelGrid columnComparison, ref string message)
        {
            bool isSucces = false;
            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                ColumnComparisonModel filter = new ColumnComparisonModel();
                if (filters != null)
                {

                    filter.ColumnName = filters.Where(a => a.Field.Equals("ColumnName")).Select(a => a.Value).SingleOrDefault();
                    filter.TableName = filters.Where(a => a.Field.Equals("TableName")).Select(a => a.Value).SingleOrDefault();
                    filter.OrdinalPosition = Convert.ToInt32((string) filters.Where(a => a.Field.Equals("OrdinalPosition")).Select(a => a.Value).SingleOrDefault());
                    if (filter.OrdinalPosition == 0) { filter.OrdinalPosition = null; }
                    // filter.IsCompare = Convert.ToBoolean((string)filters.Where(a => a.Field.Equals("IsCompare")).Select(a => a.Value).SingleOrDefault());
                  

                    filter.LastModifiedBy = filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse(filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }
                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.ColumnComparisons                                

                                let isColumnName = !string.IsNullOrEmpty(filter.ColumnName)
                                let isTableName = !string.IsNullOrEmpty(filter.TableName)
                                let isOrdinaryPosition = filter.OrdinalPosition.HasValue ? true: false
                               // let isCompare = filter.IsCompare.HasValue ? false : true
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false

                                where a.IsDeleted == false &&
                                 (isColumnName ? a.ColumnName.Contains(filter.ColumnName) : true) &&
                                 (isTableName ? a.TableName.Contains(filter.TableName) : true) &&
                                 (isOrdinaryPosition ?( a.OrdinalPosition == filter.OrdinalPosition) : true) &&
                               //  (isCompare ? a.IsCompare.Equals(filter.IsCompare) : true) &&
                                 (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                 (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)


                                select new ColumnComparisonModel
                                {
                                    FilterID = a.FilterID,
                                    ColumnName = a.ColumnName,
                                    TableName = a.TableName,
                                    OrdinalPosition=a.OrdinalPosition,
                                    IsCompare = a.IsCompare,
                                    LastModifiedBy = a.CreateBy,
                                    LastModifiedDate = a.CreateDate
                                });

                    columnComparison.Page = page;
                    columnComparison.Size = size;
                    columnComparison.Total = data.Count();
                    columnComparison.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    columnComparison.Rows = data.OrderBy(orderBy).AsEnumerable().Select((a, i) => new ColumnComparisonModelRow
                    {
                        RowID = i + 1,
                        FilterID = a.FilterID,
                        ColumnName = a.ColumnName,
                        TableName = a.TableName,
                        OrdinalPosition = a.OrdinalPosition,
                        IsCompare = a.IsCompare,
                        LastModifiedBy = a.LastModifiedBy,
                        LastModifiedDate = a.LastModifiedDate,
                    }).Skip(skip).Take(size).ToList();
                }
                isSucces = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSucces;
        }

        public bool AddColumnComparison(ColumnComparisonModel columnComparison, ref string message)
        {
          
            bool isSuccess = false;

            try
            {
                if (!context.ColumnComparisons.Where(a => a.FilterID == columnComparison.FilterID && a.IsDeleted == false).Any())
                {
                    ColumnComparison colum = new ColumnComparison();

                    colum.ColumnName = columnComparison.ColumnName;
                    colum.TableName = columnComparison.TableName;
                    colum.OrdinalPosition = columnComparison.OrdinalPosition;
                    colum.IsCompare = columnComparison.IsCompare;
                    colum.IsDeleted = false;
                    colum.CreateDate = DateTime.UtcNow;
                    colum.CreateBy = currentUser.GetCurrentUser().DisplayName;


                    context.ColumnComparisons.Add(colum);
                    context.SaveChanges();
                    isSuccess = true;


                }
                else
                {
                    message = string.Format("Colum Comparison  {0} is already exist.", columnComparison.FilterID);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateColumnComparison(int ID, ref ColumnComparisonModel columnComparison, ref string message)
        {
            bool isSucces = false;
            try
            {
                Entity.ColumnComparison data = context.ColumnComparisons.Where(a => a.FilterID.Equals(ID)).SingleOrDefault();
                if (data != null)
                {

                    data.ColumnName = columnComparison.ColumnName;
                    data.TableName = columnComparison.TableName;
                    data.OrdinalPosition = columnComparison.OrdinalPosition;
                    data.IsCompare = columnComparison.IsCompare;                    
                    data.CreateBy = currentUser.GetCurrentUser().DisplayName;
                    data.CreateDate = DateTime.UtcNow;
                    context.SaveChanges();
                    isSucces = true;
                }
                else
                {
                    message = string.Format("Data Colum Comparison {0} is does not exist.", ID);
                }
                isSucces = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSucces;
        }

        public bool DeleteColumnComparison(int id, ref string message)
        {
            bool isSucces = false;
            try
            {
                Entity.ColumnComparison data = context.ColumnComparisons.Where(a => a.FilterID.Equals(id)).SingleOrDefault();
                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                    context.SaveChanges();
                    isSucces = true;
                }
                else
                {
                    message = string.Format("Colum Comparison data for  {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSucces;
        }

        public bool GetColumnComparison(string key, int limit, ref IList<ColumnComparisonModel> ColumnComparisons, ref string message)
        {
            bool isSuccess = false;
            try
            {
                ColumnComparisons = (from a in context.ColumnComparisons
                                      where a.IsDeleted.Equals(false) &&
                             a.CreateBy.Contains(key)
                            select new ColumnComparisonModel
                            {
                                FilterID = a.FilterID,
                                ColumnName = a.ColumnName,
                                TableName = a.TableName,
                                OrdinalPosition = a.OrdinalPosition,
                                IsCompare = a.IsCompare,
                            }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetColumnComparisonByTableName(ref IList<ColumnComparisonModel> output, string tableName, ref string message)
        {
            bool isSuccess = false;

            try
            {
                output = context.ColumnComparisons.Where(t => t.TableName == tableName && t.IsCompare.Value == true && t.IsDeleted.Value == false).
                    Select(a => new ColumnComparisonModel
                    {
                        ColumnName = a.ColumnName,
                        OrdinalPosition = a.OrdinalPosition
                    }).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public void Dispose()
        {

            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}