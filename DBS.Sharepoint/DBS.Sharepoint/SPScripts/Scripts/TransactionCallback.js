/**
 * Created by Administrator on 9/18/2014.
 */

var accessToken;
var callBackTimeCount = ko.observable();
var CallBackTimeModels = ko.observableArray([]);

function initCallBackTime() {
    callBackTimeCount = ko.observable(4);
    $.when(addTempModel(callBackTimeCount()))
        .done(
            function() {
                helperCallBackTime(callBackTimeCount());
            }
        );
}

function addTempModel(callBackTimeCount) {
    if (callBackTimeCount > 1) {
        for ( var i = 0; i <= callBackTimeCount - 1; i++ ) {
            CallBackTimeModels.push({
                callBackTime: ""
            });
        }
    }
}

function helperCallBackTime(callBackTimeCount) {
    if (callBackTimeCount > 1) {
        for ( var i = 0; i <= callBackTimeCount - 1; i++ ) {
            $('#timepicker'+ i).timepicker({
                defaultTime: "",
                minuteStep: 1,
                showSeconds: false,
                showMeridian: false
            }).next().on(ace.click_event, function(){
                $(this).prev().focus();
            });
        }
    }
}

function OnSuccessAutoComplete(response, data, textStatus, jqXHR) {
    response($.map(data, function (item) {
            return {
                // default autocomplete object
                label: item.CIF + " - " + item.Name,
                value: item.Name,

                // custom object binding
                ID: item.CIF,
                Name: item.Name
            }
        })
    );
}

var ViewModel = function () {
    //Make the self as 'this' reference
    var self = this;

    //Declare Variables
    //self.CallBackTimeModels = ko.observableArray([]);
    self.CustomerContact = ko.observableArray([]);
    self.CustomerContactName = ko.observable();
    self.CustomerContactPhone = ko.observable();
    self.CustomerContactDob = ko.observable();
    self.CustomerContactAddress = ko.observable();
    self.CustomerContactIdNumber = ko.observable();
    self.CustomerContactSource = ko.observable();
    self.CustomerContactIsSelected = ko.observable();

    //Test Data Grid
    self.AccountNumber = ko.observable();

    GetAccount();

    function GetAccount() {
        $.ajax({
            type: "GET",
            url: api.server + api.url.customeraccount + "/089850390031", //+ cif,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    console.log(ko.toJSON(data));
                    self.CustomerContact(data);
                } else {
                    //ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                //ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    self.onSaveDraft = function()
    {
        getLastInputTime();
    }

    function getLastInputTime(){
        for ( var i = callBackTimeCount() - 1; i >= 0; i-- ){
            if ($("#timepicker" + i ).val() != "00:00") {
                self.CallBackTimeModelsTemp = ko.observableArray([]);
                self.CallBackTimeModelsTemp.push({
                    callBackTime: $("#timepicker" + i ).val()
                });
                return;
            }
        }
    }
}

$(document).ready(function () {
    accessToken = $.cookie(api.cookie.name);
    ko.applyBindings(new ViewModel());

    initCallBackTime();

    /*for ( var i = 0; i <= TestModel.length; i++ ) {
        $('#timepicker'+ i).timepicker({
            defaultTime: "",
            minuteStep: 1,
            showSeconds: false,
            showMeridian: false
        }).next().on(ace.click_event, function(){
            $(this).prev().focus();
        });
    };*/
});
