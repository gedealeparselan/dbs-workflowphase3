/**
 * Created by Roman Bangkit S on 10/14/2014.
 */
var ViewModel = function () {
    var self = this;
    self.Readonly = ko.observable(false);
    self.IsWorkflow = ko.observable(false);
    // New Data flag
    self.IsNewData = ko.observable(false);
    //Parameter
    self.IsCutOffApplied = ko.observable();
    self.CutOffTime = ko.observable();
    self.LastModifiedBy = ko.observable("");
    self.LastModifiedDate = ko.observable("");

    self.IsRoleMaker = ko.observable(false);
    self.IsPermited = ko.observable(false);

    var IsCutOffAppliedData = {
        ID: 0,
        Name: "IS_CUT_OFF_TIME_APPLIED",
        //Value: self.IsCutOffApplied() === true ? 1 : 0,
        Value: ko.computed({
            read: function () {
                return self.IsCutOffApplied() ? 1 : 0;
            }
        }),
        LastModifiedDate: new Date(),
        LastModifiedBy: "Unknown"
    }

    var CutOffTimeData = {
        ID: 0,
        Name: "CUT_OFF_TIME",
        Value: self.CutOffTime,
        LastModifiedDate: new Date(),
        LastModifiedBy: "Unknown"
    };

    //Body
    self.IsCutOffAppliedData = ko.observable(new SingleValueParameter(IsCutOffAppliedData));
    self.CutOffTimeData = ko.observable(new SingleValueParameter(CutOffTimeData));

    //Get Data
    self.GetData2 = function () { GetData2(); }
    self.GetData = function () { GetData(); };

    //Get IsCutOffApplied
    function GetData2() {
        var options = {
            url: api.server + api.url.iscutofftimeapplied,
            params: {
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetIsCutOffApplied, OnError, OnAlways);
    }
    self.GetSelectedRow = function (data) {
        self.IsNewData(false);
        if (self.IsWorkflow()) {
            $("#modal-form-workflow").modal('show');
            $.each(data, function (index, item) {
                switch (item.Name) {
                    case "IS_CUT_OFF_TIME_APPLIED":
                        self.IsCutOffApplied(item.Value);
                        break;
                    case "CUT_OFF_TIME":
                        self.CutOffTime(item.Value);
                        break;
                }
                self.LastModifiedBy(item.LastModifiedBy);
                self.LastModifiedDate(item.LastModifiedDate);
            });
            $('#IsCutOffApproval').prop('checked', self.IsCutOffApplied());
            $('#timepickerapproval').timepicker({
                defaultTime: self.CutOffTime(),
                minuteStep: 1,
                showSeconds: false,
                showMeridian: false
            }).next().on(ace.click_event, function () {
                $(this).prev().focus();
            });

        } else {
            $("#modal-form").modal('show');
        }
    };
    function OnSuccessGetIsCutOffApplied(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            // bind result to observable array
            if (data.Message == 0) self.IsCutOffApplied(false);
            if (data.Message == 1) self.IsCutOffApplied(true);
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }

    //Get CutOffTime
    function GetData() {
        GetData2();
        var options = {
            url: api.server + api.url.cutofftime,
            params: {
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetCutOffTime, OnError, OnAlways);
    }

    function OnSuccessGetCutOffTime(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            // bind result to observable array
            if (data.Message == null) { self.CutOffTime('00:00'); }
            else { self.CutOffTime(data.Message); }

            $('#timepicker').timepicker({
                time: self.CutOffTime(),
                minuteStep: 1,
                showSeconds: false,
                showMeridian: false
            }).next().on(ace.click_event, function () {
                $(this).prev().focus();
            });
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }

    //Update Cut Off Time
    self.UpdateCutOffTime = function () { UpdateCutOffTime(); }

    self.IsPermited = function (IsPermitedResult) {
        //Ajax call to delete the Customer
        spUser = $.cookie(api.cookie.spUser);

        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/CheckUserPermited/" + _spFriendlyUrlPageContextInfo.title,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    //compare user role to page permission to get checker validation
                    for (i = 0; i < spUser.Roles.length; i++) {
                        for (j = 0; j < data.length; j++) {
                            if (Const_RoleName[spUser.Roles[i].ID] == data[j]) { //if (spUser.Roles[i].Name == data[j]) {
                                if (Const_RoleName[spUser.Roles[i].ID].endsWith('Maker')) { //if (spUser.Roles[i].Name.endsWith('Maker')) {
                                    self.IsRoleMaker(true);
                                    return;
                                }
                                else {
                                    self.IsRoleMaker(false);
                                }
                            }
                        }
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    function UpdateCutOffTime() {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        //alert(ko.toJSON(self.IsCutOffAppliedData()));

        if (form.valid()) {
            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {

                    var adata = new Array();
                    adata.push(self.IsCutOffAppliedData());
                    adata.push(self.CutOffTimeData());
                    /*
		            var iscutoffapplied = {
		                url: api.server + api.url.singlevalueparameter + "/" + self.IsCutOffAppliedData().Name(),
		                data: ko.toJSON(self.IsCutOffAppliedData()),
		                params: {
		                },
		                token: accessToken
		            };
		
		            Helper.Ajax.Put(iscutoffapplied, OnSuccessUpdateIsCutOffApplied, OnError, OnAlways);
					*/
                    var cutofftime = {
                        url: api.server + api.url.singlevalueparameter + "/" + self.CutOffTimeData().Name(),
                        //data: ko.toJSON(self.CutOffTimeData()),
                        data: ko.toJSON(adata),
                        params: {
                        },
                        token: accessToken
                    };

                    Helper.Ajax.Put(cutofftime, OnSuccessUpdateCutOffTime, OnError, OnAlways);
                    GetData();
                }
            });
        }
    }

    function OnSuccessUpdateIsCutOffApplied(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            // bind result to observable array
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-success', false);
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }

    function OnSuccessUpdateCutOffTime(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            // bind result to observable array
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-success', false);
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }
    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }
    });
}
