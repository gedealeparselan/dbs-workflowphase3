﻿<%@ Assembly Name="DBS.Sharepoint, Version=1.0.0.0, Culture=neutral, PublicKeyToken=f75c72caf4809f4b" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChangeRMSolIDUserControl.ascx.cs" Inherits="DBS.Sharepoint.SPWebparts.PM.ChangeRMSolID.ChangeRMSolIDUserControl" %>

<div id="home-ChageRM">
    <h1 class="header smaller lighter dark">Change RM/Segment/SOL ID</h1>
        <div class="col-xs-12">
            <div id="transaction-form" class="form-horizontal" role="form">
                <div class="form-group">
                    <label class="col-sm-3 control-label bolder no-padding-right">
                        <span data-bind="text: $root.LocalDate(TransactionMakerCBO().CreateDate, true, true)"></span>
                    </label>

                    <label class="col-sm-4 pull-right">
                        <b>Application ID</b> : <span data-bind="text: TransactionMakerCBO().ApplicationID"></span><br />
                        <b>User Name</b> : <span data-bind="text: spUser.DisplayName"></span>
                    </label>

                    <div class="form-group no-padding-bottom">
                        
                    </div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label bolder no-padding-right">Product</label>

                        <label class="col-sm-4">
                            <select class="col-sm-11" id="Product" name="Product" class="col-sm-11"
                                    data-bind="options: $root.ddlProducts,
    optionsText: function (item) { if (item.ID != null) return item.Code + ' (' + item.Name + ')' },
    optionsValue: 'ID',
    value: $root.Selected().Product,
    optionsCaption: 'Please Select...',
    event: { change: $root.OnChangeRMProductChange }, disable:true" data-rule-required="true" data-rule-value="true"></select>
                        </label>

                    </div>

                     <div class="form-group">
                        <label class="col-sm-3 control-label bolder no-padding-right">Request Type</label>
                        <label class="col-sm-4">
                            <select class="col-sm-11" id="RequestType" name="RequestType" class="col-sm-11"
                                    data-bind="options: $root.ddlRequestTypes,
    optionsText: function (item) { if (item.ID != null) return item.Name },
    optionsValue: 'ID',
    value: $root.Selected().RequestType,
    optionsCaption: 'Please Select...',
    event: { change: $root.OnChangeRMRequestTypeChange }, disable:true" data-rule-required="true" data-rule-value="true"></select>
                                                         </label>
                    </div>

                    <div id="lblmaintenanceid" class="form-group">
                        <label class="col-sm-3 control-label bolder no-padding-right">Maintenance Type</label>
                        <label class="col-sm-4">
                            <select class="col-sm-11" id="MaintenanceType" name="MaintenanceType" class="col-sm-11"
                                    data-bind="options: $root.ddlMaintenanceTypes,
    optionsText: function (item) { if (item.ID != null) return item.Name },
    optionsValue: 'ID',
    value: $root.Selected().MaintenanceType,
    optionsCaption: 'Please Select...',
    event: { change: $root.OnChangeRMMaintenanceTypeChange }, disable:true" data-rule-required="true" data-rule-value="true"></select>
                        </label>
                    </div>
                    <div class="space-4"></div>
                    <div id="lblChkChangeRM" class="form-group no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right">Change RM</label>
                        <label class="col-xs-1" style="width:3%">
                            <input type="checkbox" id="isChangeRM" name="isChangeRM" data-bind="checked: $root.CheckboxGroup().IsChangeRM"></span>
                        </label>
                    </div>
                    <div class="space-4"></div>
                    <div id="lblChkSegment" class="form-group no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right">Segment</label>
                        <label class="col-xs-1" style="width:3%">
                            <input type="checkbox" id="isSegment" name="isSegment" data-bind="checked: $root.CheckboxGroup().IsSegment "></span>
                        </label>
                    </div>
                    <div class="space-4"></div>
                    <div id="lblChkSolID" class="form-group no-margin-bottom no-padding-bottom">
                        <label class="col-sm-3 control-label bolder no-padding-right">SOL ID</label>
                        <label class="col-xs-1" style="width:3%">
                            <input type="checkbox" id="isSolID" name="isSolID" data-bind="checked: $root.CheckboxGroup().IsSOLID"></span>
                        </label>
                    </div>
                <div class="space-6"></div>
                    <%-- Grid Upload --%>
                <div class="padding-4">                                                    
                    <button class="btn btn-sm btn-primary pull-right" data-bind="click: $root.UploadExcel">
                        <i class="icon-plus"></i>
                        Upload
                    </button>                       
                </div>  
                      
                <%--</div>
                   <div class="padding-4">
                    <div id="Documents_Upload" class="dataTables_wrapper" role="grid">
                        <table class="table table-striped table-bordered table-hover dataTable">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>No.</th>
                                    <th>CIF</th>
                                    <th>Customer Name</th>
                                    <th>Change Segment Form</th>
                                    <th>Change SegmentTo</th>
                                    <th>Change From BranchCode</th>
                                    <th>ChangeFromRMCode</th>
                                    <th>ChangeFromBU</th>
                                    <th>ChangeToBranchCode</th>
                                    <th>ChangeToRMCode</th>
                                    <th>ChangeToBU</th>
                                    <th>Account</th>
                                    <th>PCCode</th>
                                    <th>EA/TPB</th>
                                    <th>Remarks</th>
                                </tr>
                            </thead>
                            
                            <tbody data-bind="foreach: $root.Documents_Upload">
                                <tr>
                                    <td align="center"><input type="checkbox" id="isUtilize" name="isUtilize"/></td>
                                    <td><span data-bind="text: $index() + 1"></span></td>
                                    <td><span data-bind="text: CIF"></span></td>
                                    <td><span data-bind="text: CustomerName"></<span></td>
                                    <td><span data-bind="text: ChangeSegmentForm"></span></td>
                                    <td><span data-bind="text: ChangeSegmentTo"></span></td>
                                    <td><span data-bind="text: ChangeFromBranchCode"></span></td>
                                    <td><span data-bind="text: ChangeFromRMCode"></span></td>
                                    <td><span data-bind="text: ChangeFromBU"></span></td>
                                    <td><span data-bind="text: ChangeToBranchCode"></span></td>
                                    <td><span data-bind="text: ChangeToRMCode"></span></td>
                                    <td><span data-bind="text: ChangeToBU"></span></td>
                                    <td><span data-bind="text: Account"></span></td>
                                    <td><span data-bind="text: PCCode"></span></td>
                                    <td><span data-bind="text: EATPB"></span></td>
                                    <td><span data-bind="text: Remarks"></span></td>
                                </tr>
                            </tbody>
                            <tbody">
                                <tr>
                                    <td colspan="16" align="center">No entries available.</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>--%>
                            
                            <%-- Hands On Table --%>
                                <div class="form-group">
                                    <div class="col-sm-12">                                        
                                        <div id="dataTable"></div>                        
                                    </div>
                                </div>
                            <%-- Attachment --%>                                   
                              <div class="padding-4">
                                <h3 class="header smaller lighter dark">
                                    Attachment
                                    <button class="btn btn-sm btn-primary pull-right" data-bind="click: $root.UploadDocument">
                                        <i class="icon-plus"></i>
                                        Attach
                                    </button>
                                </h3>                            
                                <div id="Documents" class="dataTables_wrapper" role="grid">
                                    <table class="table table-striped table-bordered table-hover dataTable">
                                        <thead>
                                            <tr>
                                                <th style="width:50px">No.</th>
                                                <th>File Name</th>
                                                <th>Type</th>
                                                <th>Modified</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                            
                                        <tbody data-bind="foreach: $root.Documents, visible: $root.Documents().length > 0">
                                            <tr>
                                                <td><span data-bind="text: $index() + 1"></span></td>
                                                <td><a data-bind="attr: { href: DocumentPath.DocPath, target: '_blank' }, text: DocumentPath.name"></a></td>
                                                <td><span data-bind="text: Type.Name"></span></td>
                                                <td><span data-bind="text: $root.LocalDate(LastModifiedDate)"></span></td>
                                                <td><a href="#" data-bind="click: $root.RemoveDocument">Remove</a></td>
                                            </tr>
                                        </tbody>
                                        <tbody data-bind="visible: $root.Documents().length == 0">
                                            <tr>
                                                <td colspan="5" align="center">No entries available.</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            
                            
                            <div id="modal-form-upload" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="0" role="dialog">
                                    <div class="modal-dialog upload">
                                        <div class="modal-content">
                                            <!-- modal header start Attach FIle Form -->
                                            <div class="modal-header">
                                                <h4 class="blue bigger">
                                                    <span data-bind="visible: $root.IsUploadAttachment()==true">Add Attachment File</span>
                                                    <span data-bind="visible: $root.IsUploadExcel()==true">Upload File</span> 
                                                </h4>
                                            </div>
                                            <!-- modal header end Attach file -->
                                            <!-- modal body start Attach File -->
                                            <div class="modal-body overflow-visible" id="upload-form">
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <!-- modal body form start -->
                                                        <div class="form-group" data-bind="visible: $root.IsUploadAttachment()==true">
                                                            <label class="col-sm-3 control-label no-padding-right" for="document-type">Document Type</label>                                                            
                                                            <div class="col-sm-9">
                                                                <div class="clearfix">
                                                                    <select id="document-type" name="document-type" data-rule-required="true" data-rule-value="true"
                                                                            data-bind="options: $root.ddlDocumentTypes,
    optionsText: function (item) { if (item.ID != null) return item.Name },
    optionsValue: 'ID',
    optionsCaption: 'Please Select...',
    value: $root.Selected().DocumentType,
    visible: $root.IsUploadAttachment()==true"></select>
                                                                </div>
                                                            </div>
                                                        </div>                                                       

                                                        <div class="space-4"></div>

                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label no-padding-right" for="document-path" data-bind="visible: $root.IsUploadAttachment() == true">Document</label>
                                                            <label class="col-sm-3 control-label no-padding-right" for="file-name" data-bind="visible: $root.IsUploadExcel()==true">File Name</label>
                                                            <div class="col-sm-9 ace-file-input">
                                                                <div class="clearfix">
                                                                    <input type="file" id="document-path" name="document-path" data-bind="file: DocumentPath" data-rule-required="true" data-rule-value="true" /><!---->
                                                                    <!--<span data-bind="text: DocumentPath"></span>-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-body overflow-visible">
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <div id="transaction-progress" class="col-sm-12" style="display: none">
                                                            <h4 class="smaller blue">Upload in progress, please wait...</h4>
                                                            <span class="space-4"></span>
                                                            <div class="progress progress-small progress-striped active">
                                                                <div class="progress-bar progress-bar-info" style="width: 100%"></div>
                                                            </div>
                                                        </div>
                                                        <div id="transaction-result" class="col-sm-12" style="display: none">
                                                            <div class="dataTables_wrapper" role="grid">
                                                                <h4><span data-bind="text: $root.resultHeader()" /></h4>
                                                                <span data-bind="text: $root.resultMessage()" />
                                                            </div>
                                                        </div>
                                                        <div id="transaction-grid" class="col-sm-12" style="display: none">
                                                            <div class="dataTables_wrapper" role="grid">
                                                                <table class="table table-striped table-bordered table-hover dataTable">
                                                                    <thead>
                                                                        <tr>
                                                                            <th style="width: 50px">No.</th>
                                                                            <th><span data-bind="text: $root.Column1Name"></th>
                                                                            <th><span data-bind="text: $root.Column2Name"></th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody data-bind="foreach: $root.RowFailed, visible: $root.RowFailed().length > 0">
                                                                        <tr>
                                                                            <!--data-bind="click: $root.EditDocument"-->
                                                                            <td><span data-bind="text: $index() + 1"></span></td>
                                                                            <td><span data-bind="text: Column1"></span></td>
                                                                            <td><span data-bind="text: Column2"></span></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-sm btn-primary" data-bind="click: $root.AddDocument, visible: $root.IsUploadAttachment() == true">
                                                    <i class="icon-save"></i>
                                                    Save
                                                </button>
                                                <button class="btn btn-sm btn-primary" data-bind="click: $root.UploadProcessExcel, visible: $root.IsUploadExcel() == true, disable: $root.IsUploading() == true">
                                                    <i class="icon-save"></i>
                                                    Upload
                                                </button>
                                                <button class="btn btn-sm" data-dismiss="modal" data-bind="visible: $root.IsUploadAttachment() == true">
                                                    <i class="icon-remove"></i>
                                                    Close
                                                </button>
                                                <button class="btn btn-sm" data-dismiss="modal" data-bind="visible: $root.IsUploadExcel() == true">                                                    
                                                    Close
                                                </button>
                                            </div>

                                        </div>                                        
                                    </div>
                            </div>
                            <!-- Modal application ID List start-->
                            <div id="modal-form-applicationID" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="blue bigger">Application ID List
                                            </h4>
                                        </div>
                                        <div class="modal-body overflow-visible">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div id="customer-form" class="form-horizontal" role="form">
                                                        <div class="dataTables_wrapper" role="grid">
                                                            <table class="table table-striped table-bordered table-hover dataTable">
                                                                <thead>
                                                                    <tr>
                                                                        <th style="width: 50px">No.</th>
                                                                        <th>Application ID</th>                                                                       
                                                                    </tr>
                                                                </thead>
                                                                <tbody data-bind="foreach: $root.ApplicationIDColl, visible: $root.ApplicationIDColl().length > 0">
                                                                    <tr data-toggle="modal">
                                                                        <td><span data-bind="text: $index() + 1"></span></td>
                                                                        <td><span data-bind="text: ApplicationID"></span></td>                                                                        
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <br />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-sm btn-primary" data-bind="click: $root.RedirectHome">
                                                <i class="icon-save"></i>
                                                OK
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Modal application ID List end-->
                            <!-- Modal Cut Off notification start-->
                            <div id="modal-form-Notif" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h3 class="blue bigger"><span data-bind="text: $root.NotifTitle()" />
                                            </h3>
                                        </div>
                                        <div class="modal-body overflow-visible">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div id="customer-form" class="form-horizontal" role="form">
                                                        <div class="dataTables_wrapper" role="grid">
                                                            <h2><span data-bind="text: $root.NotifHeader()" /></h2>
                                                            <span data-bind="text: $root.NotifMessage()" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-sm btn-primary" data-bind="click: $root.NotifOK">
                                                <i class="icon-save"></i>
                                                OK
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Modal Cut Off notification end-->
                            <div class="space-4"></div>
                            <%-- Button Submit --%>
                            <div class="col-sm-12 align-right">
                                <!--<button class="btn btn-sm btn-primary" data-bind="click: $root.SaveDraftChangeRM">
                                    <i class="icon-save"></i>
                                    Draft
                                </button>-->
                                <button class="btn btn-sm btn-primary"data-bind="click: $root.SaveChangeRM, disable: $root.IsSubmitted()==true" >
                                    <i class="icon-save"></i>
                                    Submit
                                </button>
                                <button class="btn btn-sm" data-bind="click: $root.RedirectHome">
                                    <i class="icon-remove"></i>
                                    Close
                                </button>
                            </div>

            </div>
        </div>



    <!-- widget box start --> 
    <%--<div id="widget-box" class="widget-box">
        <!-- widget header start -->
        <div class="widget-header">
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="IMDate">
                    Start Date</label>
                <div class="col-sm-2">
                    <div class="clearfix">
                        <div class="input-group">
                            <input id="IMDate" name="IMDate"  data-date-format="dd-M-yyyy" class="form-control date-picker col-xs-6" type="text" />
                            <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <div id="dataTable" class="widget-box"></div>
                </div>
            </div>
        </div>
    </div>--%>
</div>




<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/additional-methods.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/Knockout/knockout.mapping-latest.js"></script>
<script src="/SiteAssets/Scripts/ChangeRMSolID.js"></script>

<link rel="stylesheet" media="screen" href="/SiteAssets/Scripts/handsontable/dist/handsontable.full.css">
<script src="/SiteAssets/Scripts/handsontable/dist/handsontable.full.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/parser.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/ruleJS.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/lodash.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/underscore.string.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/moment.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/numeral.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/md5.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/jstat.js"></script>
