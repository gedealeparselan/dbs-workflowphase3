﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class BeneSegmentController : ApiController
    {
        private string message = string.Empty;

        private readonly IBeneSegmentRepository repository = null;

        public BeneSegmentController()
        {
            this.repository = new BeneSegmentRepository();
        }

        public BeneSegmentController(IBeneSegmentRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all bene segments.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<BeneSegmentModel>))]
        public HttpResponseMessage Get()
        {
            IList<BeneSegmentModel> output = new List<BeneSegmentModel>();

            if (!repository.GetBeneSegment(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<BeneSegmentModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get bene segments with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<BeneSegmentModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<BeneSegmentModel> output = new List<BeneSegmentModel>();

            if (!repository.GetBeneSegment(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<BeneSegmentModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get bene segments for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(BeneSegmentGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<BeneSegmentFilter> filters)
        {
            BeneSegmentGrid output = new BeneSegmentGrid();

            if (!repository.GetBeneSegment(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<BeneSegmentGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find bene segments using criterias.
        /// </summary>
        /// <param name="query">Code or Description.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<BeneSegmentModel>))]
        [HttpGet]
        [Route("api/BeneSegment/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<BeneSegmentModel> output = new List<BeneSegmentModel>();

            if (!repository.GetBeneSegment(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<BeneSegmentModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get bene segment by ID.
        /// </summary>
        /// <param name="id">Bene Segment ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(BeneSegmentModel))]
        [Route("api/BeneSegment/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            BeneSegmentModel output = new BeneSegmentModel();

            if (!repository.GetBeneSegmentByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<BeneSegmentModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new bene segment.
        /// </summary>
        /// <param name="beneSegment">Bene Segment data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(BeneSegmentModel beneSegment)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddBeneSegment(beneSegment, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New Bene Segment data has been created." });
                }
            }
        }

        /// <summary>
        /// Modify existing bene segment.
        /// </summary>
        /// <param name="id">ID of BeneSegment to be modify.</param>
        /// <param name="beneSegment">Bene segment data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/BeneSegment/{id}")]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(int id, BeneSegmentModel beneSegment)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateBeneSegment(id, beneSegment, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Bene Segment data has been updated." });
                }
            }
        }

        /// <summary>
        /// Remove exisiting bene segment.
        /// </summary>
        /// <param name="id">ID of bene segment to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/BeneSegment/{id}")]
        //[Authorize(Roles = "DBS Admin")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteBeneSegment(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "BeneSegment data has been deleted." });
                }
            }
        }
        
        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}