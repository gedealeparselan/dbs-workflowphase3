﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class WorkflowCIFController : ApiController
    {
        private string message = string.Empty;
        /// <summary>
        /// Define repo here
        /// </summary>
        private readonly IWorkflowCIFRepository repoCIF = null;
        public WorkflowCIFController()
        {
            this.repoCIF = new WorkflowCIFRepository();
        }

        protected override void Dispose(bool disposing)
        {
            if (repoCIF != null)
                repoCIF.Dispose();


            base.Dispose(disposing);
        }
        #region Affandy
        [ResponseType(typeof(IList<ChangeRMModel>))]
        [AcceptVerbs("POST")]
        [Route("api/WorkflowCIF/Transaction/GetTransactionDetailsChangeRM")]
        public HttpResponseMessage GetTransactionDetailsChangeRM(IList<int> ActivityHistoryID)
        {
            IList<ChangeRMModel> output = new List<ChangeRMModel>();
            if (!repoCIF.GetTransactionDetailsChangeRMUploaded(ActivityHistoryID, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<ChangeRMModel>>(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(IList<ChangeRMModel>))]
        [AcceptVerbs("GET")]
        [Route("api/WorkflowCIF/Transaction/{ActivityHistoryID}/GetTransactionDetailsChangeRMReupload/{TransactionID}/{IsBringUp}")]
        public HttpResponseMessage GetTransactionDetailsChangeRM(int ActivityHistoryID, long TransactionID, bool IsBringUp)
        {
            IList<ChangeRMModel> output = new List<ChangeRMModel>();
            if (!repoCIF.GetTransactionDetailsChangeRMReuploaded(TransactionID, ActivityHistoryID, IsBringUp, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<ChangeRMModel>>(HttpStatusCode.OK, output);
            }
        }
        [ResponseType(typeof(TransactionContactDetailModel))]
        [HttpGet]
        [Route("api/WorkflowCIF/{workflowInstanceID}/Transaction/Contact/{approverID}")]
        public HttpResponseMessage GetTransactionContactDetails(Guid workflowInstanceID, long approverID)
        {
            TransactionCBODetailModel transaction = new TransactionCBODetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            IList<CustomerContactModel> contact = new List<CustomerContactModel>();
            TransactionCBOContactDetailModel output = new TransactionCBOContactDetailModel();

            try
            {
                // #1 Get Transaction Details
                if (!repoCIF.GetTransactionDetails(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoCIF.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        if (!repoCIF.GetTransactionCheckerDetails(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Transaction = transaction;
                            output.Timelines = timelines;

                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                        // #3 Get Transaction Contact Details
                        if (!repoCIF.GetTransactionContactDetails(workflowInstanceID, approverID, ref contact, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Contacts = contact;
                            output.Transaction = transaction;
                            output.Timelines = timelines;

                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }

                }
            }
            finally
            {

                transaction = null;
                timelines = null;
                contact = null;
            }
        }


        [ResponseType(typeof(TransactionContactDetailModel))]
        [HttpGet]
        [Route("api/WorkflowCIF/{workflowInstanceID}/TransactionChangeRM/Contact/{approverID}")]
        public HttpResponseMessage GetTransactionChangeRM(Guid workflowInstanceID, long approverID)
        {
            TransactionCBODetailModel transaction = new TransactionCBODetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            IList<CustomerContactModel> contact = new List<CustomerContactModel>();
            TransactionCBOContactDetailModel output = new TransactionCBOContactDetailModel();

            try
            {
                // #1 Get Transaction Details
                if (!repoCIF.GetTransactionDetailsChangeRM(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoCIF.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        //if (!repoCIF.GetTransactionCheckerDetails(workflowInstanceID, approverID, ref output, ref message))
                        //{
                        //    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        //}
                        //else
                        //{
                        //    output.Transaction = transaction;
                        //    output.Timelines = timelines;

                        //    return Request.CreateResponse(HttpStatusCode.OK, output);
                        //}
                        // #3 Get Transaction Contact Details
                        if (!repoCIF.GetTransactionContactDetails(workflowInstanceID, approverID, ref contact, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Contacts = contact;
                            output.Transaction = transaction;
                            output.Timelines = timelines;

                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }

                }
            }
            finally
            {

                transaction = null;
                timelines = null;
            }
        }

        [ResponseType(typeof(TransactionCallbackDetailModel))]
        [HttpGet]
        [Route("api/workflowCIF/{workflowInstanceID}/Transaction/CallBack/{approverID}")]
        public HttpResponseMessage GetTransactionCallbackDetails(Guid workflowInstanceID, long approverID)
        {
            TransactionCBODetailModel transaction = new TransactionCBODetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            IList<CustomerContactModel> contact = new List<CustomerContactModel>();
            TransactionCBOContactDetailModel output = new TransactionCBOContactDetailModel();
            //TransactionCallbackDetailModel output = new TransactionCallbackDetailModel();

            try
            {
                // #1 Get Transaction Details
                if (!repoCIF.GetTransactionDetails(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "1" });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoCIF.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "2" });
                    }
                    else
                    {
                        // #3 Get Transaction Checker Details getutc
                        if (!repoCIF.GetTransactionCallbackDetails(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "3" });
                        }
                        else
                        {
                            output.Transaction = transaction;
                            output.Timelines = timelines;

                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {

                transaction = null;
                timelines = null;
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/workflowCIF/{workflowInstanceID}/Transaction/SaveCallback/{approverID}")]
        public HttpResponseMessage AddTransactionCallbackDetails(Guid workflowInstanceID, long approverID, TransactionCallbackDetailModel data)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoCIF.AddTransactionCallback(workflowInstanceID, approverID, data, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "transaction Customer Contact callback data has been saved." });
                }
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/workflowCIF/{workflowInstanceID}/Transaction/UpdateSubmitRevise/{approverID}")]
        public HttpResponseMessage UpdateTransactionBranchMaker(Guid workflowInstanceID, long approverID, TransactionCBOContactDetailModel data)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoCIF.UpdateTransactionForBranchMaker(workflowInstanceID, approverID, data, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "data has been update." });
                }
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/workflowCIF/{workflowInstanceID}/Transaction/Checker/{approverID}")]
        public HttpResponseMessage AddTransactionCheckerDetails(Guid workflowInstanceID, long approverID, TransactionCheckerDetailModel transactionChecker)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoCIF.AddTransactionCheckerDetails(workflowInstanceID, approverID, transactionChecker, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been saved." });
                }
            }
        }

        [ResponseType(typeof(TransactionCallbackDetailModel))]
        [HttpGet]
        [Route("api/workflowCIF/{workflowInstanceID}/Transaction/CBOChecker/{approverID}")]
        public HttpResponseMessage GetransactionCBOCheckerDetails(Guid workflowInstanceID, long approverID)
        {
            TransactionCBODetailModel transaction = new TransactionCBODetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TransactionCBOContactDetailModel output = new TransactionCBOContactDetailModel();
            try
            {
                if (!repoCIF.GetTransactionDetails(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    if (!repoCIF.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        if (!repoCIF.GetTransactionCBOCheckerDetails(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Contacts = null;
                            output.Transaction = transaction;
                            output.Timelines = timelines;

                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }

                    }
                }

            }
            finally
            {
                transaction = null;
                timelines = null;
            }


        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/workflowCIF/{workflowInstanceID}/Transaction/CBOChecker/{approverID}")]
        public HttpResponseMessage AddTransactionCBOCheckerDetails(Guid workflowInstanceID, long approverID, TransactionCheckerDetailModel transactionChecker)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoCIF.AddTransactionCBOCheckerDetails(workflowInstanceID, approverID, transactionChecker, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction CBO checker has been saved." });
                }
            }
        }
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/workflowCIF/{workflowInstanceID}/Transaction/CBOCheckerRevise/{approverID}")]
        public HttpResponseMessage AddTransactionCBOCheckerDetailsRevise(Guid workflowInstanceID, long approverID, TransactionCheckerDetailModel transactionChecker)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoCIF.AddTransactionCBOCheckerDetailsRevise(workflowInstanceID, approverID, transactionChecker, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction CBO checker has been saved." });
                }
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/workflowCIF/{workflowInstanceID}/Transaction/Checker/Update/{approverID}")]
        public HttpResponseMessage UpdateTransactionCheckerDetails(Guid workflowInstanceID, long approverID, TransactionCheckerDetailModel transactionChecker)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoCIF.UpdateTransactionCheckerDetails(workflowInstanceID, approverID, transactionChecker, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been updated." });
                }
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/workflowCIF/{workflowInstanceID}/Transaction/CBOMaker/Update/ATM/{approverID}")]
        public HttpResponseMessage UpdateTransactionCBOMakerATMDetails(Guid workflowInstanceID, long approverID, TransactionCBOContactDetailModel transaction)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoCIF.UpdateTransactionCBOMakerATMDetails(workflowInstanceID, approverID, transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction CBO Maker has been updated." });
                }
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/workflowCIF/{workflowInstanceID}/Transaction/CBOMaker/Update/Dispatch/{approverID}")]
        public HttpResponseMessage UpdateTransactionCBOMakerDispatchDetails(Guid workflowInstanceID, long approverID, TransactionCBOContactDetailModel transaction)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoCIF.UpdateTransactionCBOMakerDispatchDetails(workflowInstanceID, approverID, transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction CBO Maker has been updated." });
                }
            }
        }

        #endregion



    }
}
