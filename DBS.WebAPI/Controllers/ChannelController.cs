﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class ChannelController : ApiController
    {
         private string message = string.Empty;

        private readonly IChannelRepository repository = null;

        public ChannelController()
        {
            this.repository = new ChannelRepository();
        }

        public ChannelController(IChannelRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all currencies.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<ChannelModel>))]
        public HttpResponseMessage Get()
        {
            IList<ChannelModel> output = new List<ChannelModel>();

            if (!repository.GetChannel(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<ChannelModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get channels with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<ChannelModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<ChannelModel> output = new List<ChannelModel>();

            if (!repository.GetChannel(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<ChannelModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get channels for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(ChannelGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<ChannelFilter> filters)
        {
            ChannelGrid output = new ChannelGrid();

            if (!repository.GetChannel(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Code" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<ChannelGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find channels using criterias Name.
        /// </summary>
        /// <param name="query">Name </param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<ChannelModel>))]
        [HttpGet]
        [Route("api/Channel/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<ChannelModel> output = new List<ChannelModel>();

            if (!repository.GetChannel(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<ChannelModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Channel by ID.
        /// </summary>
        /// <param name="id">Channel ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(ChannelModel))]
        [Route("api/Channel/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            ChannelModel output = new ChannelModel();

            if (!repository.GetChannelByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<ChannelModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new Channel.
        /// </summary>
        /// <param name="Channel">Channel data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(ChannelModel channel)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddChannel(channel, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New channel data has been created." });
                }
            }
        }

        /// <summary>
        /// Modify existing Channel.
        /// </summary>
        /// <param name="id">ID of channel to be modify.</param>
        /// <param name="customer">Channel data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/Channel/{id}")]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(int id, ChannelModel channel)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateChannel(id, channel, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Channel data has been updated." });
                }
            }
        }

        /// <summary>
        /// Remove exisiting channel.
        /// </summary>
        /// <param name="id">ID of channel to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/Channel/{id}")]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteChannel(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Channel data has been deleted." });
                }
            }
        }
        
        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}