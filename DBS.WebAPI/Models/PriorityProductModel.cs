﻿using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;

namespace DBS.WebAPI.Models
{
    #region Property
    [ModelName("PriorityProductModel")]
    public class PriorityProductModel
    {
        public int ID { get; set; }
        public int ProductID { get; set; }
        public ProductModel Product { get; set; }
        public string Location { get; set; }
        public string Name { get; set; }
        public bool isDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime? LastModifiedDate { get; set; }
    }

    public class PriorityProductModelRow : PriorityProductModel
    {
        public int RowID { get; set; }
    }

    public class PriorityProductModelGrid : Grid
    {
        public IList<PriorityProductModelRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class PriorityProductModelFilter : Filter
    {
    }
    #endregion

    #region Interface
    public interface IPriorityProductModelInterface : IDisposable
    {
        bool GetPriorityByID(int id, ref PriorityProductModel Priority, ref string message);
        bool GetPriority(ref IList<PriorityProductModel> Priority, int limit, int index, ref string message);
        bool GetPriority(ref IList<PriorityProductModel> Priority, ref string message);
        bool GetPriority(int page, int size, IList<PriorityProductModelFilter> filters, string sortColumn, string sortOrder, ref PriorityProductModelGrid Priority, ref string message);
        bool GetPriority(PriorityProductModelFilter filter, ref IList<PriorityProductModel> Priority, ref string message);
        bool AddPriorityProduct(PriorityProductModel Priority, ref string message);
        bool UpdatePriorityProduct(int id, PriorityProductModel Priority, ref string message);
        bool DeletePriorityProduct(int id, ref string message);
    }
    #endregion

    #region Repository
    public class PriorityProductModelRepository : IPriorityProductModelInterface
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetPriorityByID(int id, ref PriorityProductModel Priority, ref string message)
        {
            bool isSucces = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                    Priority = (from a in context.PriorityProducts
                                join b in context.Products on a.ProductID equals b.ProductID
                                where a.isDeleted.Equals(false) && a.PriorityProductID.Equals(id)
                                select new PriorityProductModel
                                {
                                    ID = a.PriorityProductID,
                                    ProductID = b.ProductID,
                                    Location = a.TypeLocation,
                                    Name = b.ProductName,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreatedBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreatedDate : a.UpdateDate.Value,

                                    Product = new ProductModel() { ID = b.ProductID, Name = b.ProductName }
                                }).SingleOrDefault();

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSucces;
        }

        public bool GetPriority(ref IList<PriorityProductModel> Priority, int limit, int index, ref string message)
        {
            bool isSucces = false;
            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    Priority = (from a in context.PriorityProducts
                                join b in context.Products on a.ProductID equals b.ProductID
                                where a.isDeleted.Equals(false)
                                orderby new { a.TypeLocation, b.ProductName }
                                select new PriorityProductModel
                                {
                                    ID = a.PriorityProductID,
                                    ProductID = b.ProductID,
                                    Location = a.TypeLocation,
                                    Name = b.ProductName,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreatedBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreatedDate : a.UpdateDate.Value,

                                    Product = new ProductModel() { ID = b.ProductID, Name = b.ProductName }
                                }).Skip(skip).Take(limit).ToList();
                }
                isSucces = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSucces;
        }

        public bool GetPriority(ref IList<PriorityProductModel> Priority, ref string message)
        {
            bool isSucces = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    Priority = (from a in context.PriorityProducts
                                join b in context.Products on a.ProductID equals b.ProductID
                                where a.isDeleted.Equals(false)
                                orderby new { a.TypeLocation }
                                select new PriorityProductModel
                                {
                                    ID = a.PriorityProductID,
                                    ProductID = b.ProductID,
                                    Location = a.TypeLocation,
                                    Name = b.ProductName,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreatedBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreatedDate : a.UpdateDate.Value,

                                    Product = new ProductModel() { ID = b.ProductID, Name = b.ProductName }
                                }).ToList();
                }
                isSucces = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSucces;
        }

        public bool GetPriority(int page, int size, IList<PriorityProductModelFilter> filters, string sortColumn, string sortOrder, ref PriorityProductModelGrid Priority, ref string message)
        {
            bool isSucces = false;
            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                PriorityProductModel filter = new PriorityProductModel();
                if (filters != null)
                {
                    filter.Location = filters.Where(a => a.Field.Equals("Location")).Select(a => a.Value).SingleOrDefault();
                    filter.Name = filters.Where(a => a.Field.Equals("Name")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse(filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }
                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.PriorityProducts
                                join b in context.Products on a.ProductID equals b.ProductID

                                let isLocation = !string.IsNullOrEmpty(filter.Location)
                                let isName = !string.IsNullOrEmpty(filter.Name)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.isDeleted.Equals(false) &&
                                    (isLocation ? a.TypeLocation.Contains(filter.Location) : true) &&
                                    (isName ? b.ProductName.Contains(filter.Name) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreatedBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreatedDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreatedDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new PriorityProductModel
                                {
                                    ID = a.PriorityProductID,
                                    ProductID = b.ProductID,
                                    Location = a.TypeLocation,
                                    Name = b.ProductName,
                                    CreatedBy = a.CreatedBy,
                                    CreatedDate = a.CreatedDate,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreatedBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreatedDate : a.UpdateDate.Value,
                                    //model dropdown
                                    Product = new ProductModel() { ID = b.ProductID, Name = b.ProductName }
                                });

                    Priority.Page = page;
                    Priority.Size = size;
                    Priority.Total = data.Count();
                    Priority.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    Priority.Rows = data.OrderBy(orderBy).AsEnumerable().Select((a, i) => new PriorityProductModelRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            ProductID = a.ProductID,
                            Location = a.Location,
                            Name = a.Name,
                            CreatedBy = a.CreatedBy,
                            CreatedDate = a.CreatedDate,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate,
                            Product = a.Product
                        }).Skip(skip).Take(size).ToList();
                }
                isSucces = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSucces;
        }

        public bool GetPriority(PriorityProductModelFilter filter, ref IList<PriorityProductModel> Priority, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool UpdatePriorityProduct(int id, PriorityProductModel Priority, ref string message)
        {
            bool isSuccess = false;
            try
            {
                Entity.PriorityProduct data = context.PriorityProducts.Where(a => a.PriorityProductID.Equals(id)).SingleOrDefault();
                int IDUpdate = Priority.Product.ID;
                if (data != null)
                {
                    data.ProductID = IDUpdate;
                    data.isDeleted = Priority.isDeleted;
                    data.CreatedBy = data.CreatedBy;
                    data.CreatedDate = data.CreatedDate;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();
                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Product Name for Priority Product ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }


        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        public bool AddPriorityProduct(PriorityProductModel Priority, ref string message)
        {
            bool isSuccess = false;

            if(Priority != null)
            {
            try
            {
                if (!context.PriorityProducts.Where(a => a.TypeLocation.Equals(Priority.Location) && a.isDeleted.Equals(false)).Any())
                {
                    Entity.PriorityProduct data = new Entity.PriorityProduct()     
                    {
                        ProductID       = Priority.Product.ID,
                        TypeLocation    = Priority.Location,
                        isDeleted       = Priority.isDeleted,
                        CreatedDate     = DateTime.UtcNow,
                        CreatedBy       = currentUser.GetCurrentUser().DisplayName
                    };
                    context.PriorityProducts.Add(data);
                    context.SaveChanges();
                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Priority product data for type location {0} is already exist.", Priority.Location);
                }
            }
            catch (Exception ex)
                {
                message = ex.Message;
                }
            }
            return isSuccess;
        }


        public bool DeletePriorityProduct(int id, ref string message)
        {
            bool isSuccess = false;
            try
            {
                Entity.PriorityProduct data = context.PriorityProducts.Where(a => a.PriorityProductID.Equals(id)).SingleOrDefault();
                if (data != null)
                {
                    data.isDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Priority product data for priority ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
    }
}
    #endregion