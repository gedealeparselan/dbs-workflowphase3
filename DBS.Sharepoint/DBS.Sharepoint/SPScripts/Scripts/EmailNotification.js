var accessToken;

var SPRole = {
    ID: ko.observable(),
    Name: ko.observable()
};

var SPUser = {
    DisplayName: ko.observable(),
    Email: ko.observable(),
    ID: ko.observable(),
    LoginName: ko.observable(),
    Roles: ko.observableArray([SPRole])
};

var ApplicationModel = {
    TransactionID: ko.observable(),
    AppID: ko.observable(),
    MatrixID: ko.observable(),
    CustomerName: ko.observable(),
    Currency: ko.observable(),
    Product: ko.observable(),
    TransactionAmount: ko.observable(),
    EqvUsd: ko.observable(),
    FXTransaction: ko.observable(),
    IsTopUrgent: ko.observable(),
    TranstactionStatus: ko.observable(),
    LastSentBy: ko.observable(),
    LastSentDate: ko.observable(),
    CurrentUser: ko.observable()
};

var NotificationModel = {
    Application: ko.observable(ApplicationModel)
};

var ViewModel = function () {
    //Make the self as 'this' reference
    var self = this;

    // Sharepoint User
    self.SPUser = ko.observable(SPUser);

    //Declare an ObservableArray for Storing the JSON Response
    self.ddlMatrix = ko.observableArray([]);
    self.selectedChoice = ko.observable();

    self.NotificationModel = ko.observable(NotificationModel);

    self.GetMatrixDropdown = function () {
        GetMatrixDropdown();
    };

    self.ValidationProcess = function () {
        var matrixID = ko.toJSON(this.selectedChoice());
        GetEmailProperties(matrixID, self.NotificationModel().Application().TransactionID);
    };

    // Function to get Matrix for Dropdownlist
    function GetMatrixDropdown() {
        $.ajax({
            type: "GET",
            url: api.server + api.url.parameter + '?select=Matrix',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.ddlMatrix(data['Matrix']);
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    // Local Date
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);

        if(date = '1900/01/01 00:00:00') {
            return "";
        }
        else if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else
        {
            return "";
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-17
    };
};

// Knockout View Model
var viewModel = new ViewModel();

$(document).ready(function () {

    $('#matrix').attr("disabled", true);
    $('#isentries').hide();
    $('#resultfor').hide();

    // autocomplete
    $("#application-name").autocomplete({
        source: function (request, response) {
            // declare options variable for ajax get request
            var options = {
                url: api.server + api.url.emailnotification + "/Search",
                data: {
                    query: request.term,
                    limit: 10
                },
                token: accessToken
            };

            // exec ajax request
            Helper.Ajax.AutoComplete(options, response, OnSuccessAutoComplete, OnError);
        },
        minLength: 2,
        select: function (event, ui) {
            // set Application data model
            if (ui.item.data != undefined || ui.item.data != null) {
                var mapping = {
                    'ignore': ["LastModifiedDate", "LastModifiedBy"]
                };
                viewModel.NotificationModel().Application(ko.mapping.toJS(ui.item.data, mapping));
                $('#matrix').attr("disabled", false);
                $('#isentries').show();
                $('#noentries').hide();
                $('#resultfor').show();
            }
            else {
                viewModel.NotificationModel().Application(null);
            }
        }
    });

    // Knockout Bindings
    var viewModel = new ViewModel();
    ko.applyBindings(viewModel);

    // Token Validation
    if ($.cookie(api.cookie.name) == undefined) {
        Helper.Token.Request(TokenOnSuccess, TokenOnError);
    } else {
        // read token from cookie
        accessToken = $.cookie(api.cookie.name);

        // read spuser from cookie
        if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
            spUser = $.cookie(api.cookie.spUser);

            viewModel.SPUser(ko.mapping.toJS(spUser));
        }
        viewModel.GetMatrixDropdown();
    }

    // Token validation On Success function
    function TokenOnSuccess(data, textStatus, jqXHR) {
        // store token on browser cookies
        $.cookie(api.cookie.name, data.AccessToken);
        accessToken = $.cookie(api.cookie.name);

        // read spuser from cookie
        if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
            spUser = $.cookie(api.cookie.spUser);

            viewModel.SPUser(ko.mapping.toJS(spUser));
        }

        // call get data inside view model
        viewModel.GetMatrixDropdown();
    }

    // Token validation On Error function
    function TokenOnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
});

// Autocomplete
function OnSuccessAutoComplete(response, data, textStatus, jqXHR) {
    //get Application ID to dropdown and table
    response($.map(data, function (item) {
            return {
                // default autocomplete object
                label: item.ApplicationID,
                value: item.ApplicationID,

                // custom object binding
                data: {
                    TransactionID: item.TransactionID,
                    AppID: item.ApplicationID,
                    CustomerName: item.CustomerName,
                    Product: item.Product,
                    Currency: item.Currency,
                    TransactionAmount: item.TransactionAmount,
                    EqvUsd: item.EqvUsd,
                    FXTransaction: item.FXTransaction,
                    IsTopUrgent: item.IsTopUrgent,
                    TranstactionStatus: item.TranstactionStatus,
                    LastSentBy: item.LastSentBy,
                    LastSentDate: item.LastSentDate,
                    CurrentUser: spUser.DisplayName
                }
            }
        })
    );
}

//Function to Get All Email Properties
function GetEmailProperties(_MatrixID, _TransactionID) {
    var options = {
        url: api.server + api.url.emailnotification + "/Properties",
        params: {
            MatrixID: _MatrixID,
            TransactionID: _TransactionID
        },
        token: accessToken
    };

    Helper.Ajax.Get(options, OnSuccessAutoProperties, OnError);
}

function OnSuccessAutoProperties(data, textStatus, jqXHR) {
    SendEmailNotification(data);
}

function OnError(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}

// Function to display notification.
// class name : gritter-success, gritter-warning, gritter-error, gritter-info
function ShowNotification(title, text, className, isSticky) {
    $.gritter.add({
        title: title,
        text: text,
        class_name: className,
        sticky: isSticky
    });
}

function SaveTransaction(MatrixID) {
    //Ajax call to update the Customer
    $.ajax({
        type: "PUT",
        url: api.server + api.url.emailnotification + "/" + MatrixID,
        data: ko.toJSON(viewModel.NotificationModel().Application()),
        contentType: "application/json",
        headers: {
            "Authorization": "Bearer " + accessToken
        },
        success: function (data, textStatus, jqXHR) {
            ShowNotification(jqXHR.status + " Email Sent", "Email Notification has been sent.", 'gritter-success', true);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // send notification
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-success', true);
        }
    });
}

//Function to send email based on Transaction ID properties result
function SendEmailNotification(values) {

    var properties = {
        properties: {
            "EmailBody": values.EmailBody,
            "Subject": values.Subject,
            "UserTo": values.UserTo,
            "MatrixID": values.MatrixID
        }
    };

    $.ajax({
        type: "POST",
        url: encodeURI(_spPageContextInfo.siteAbsoluteUrl) + api.url.wcfemailnotification,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(properties),
        processdata: true,
        success: function (data, textStatus, jqXHR) {
            SaveTransaction(values.MatrixID); // start email and submit input TransactionEmailNotification
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // send notification
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-success', true);
        }
    });
}