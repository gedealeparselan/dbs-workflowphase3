var ViewModel = function () {
    //Make the self as 'this' reference
    var self = this;

    // Sharepoint User
    self.SPUser = ko.observable();

    self.GetDefaultDate = function () { GetDefaultDate(); };
    function GetDefaultDate() {
        var newDateTime = new Date();
        var newMonth; var newDate;
        var newHour; var newMinute; var newSecond;

        newMonth = newDateTime.getMonth() + 1;
        newMonth = newMonth.toString().length == 2 ? newMonth : "0" + newMonth;
        newDate = newDateTime.getDate();
        newDate = newDate.toString().length == 2 ? newDate : "0" + newDate;
        newHour = newDateTime.getHours();
        newHour = newHour.toString().length == 2 ? newHour : "0" + newHour;
        newMinute = newDateTime.getMinutes();
        newMinute = newMinute.toString().length == 2 ? newMinute : "0" + newMinute;
        newSecond = newDateTime.getSeconds();
        newSecond = newSecond.toString().length == 2 ? newSecond : "0" + newSecond;

        var IsTransactionMonitoring = $('#DefaultDateMonitoring');
        if (IsTransactionMonitoring.length > 0) {
            self.GridProperties().SortColumn("ExitTime");
            self.GridProperties().SortOrder("DESC");
            self.GridProperties().AllowFilter(true);
            self.FilterEntryTime(newDateTime.getFullYear() + "/" + newMonth + "/" + newDate);
        }

    }
    // rate param
    self.Rate = ko.observable(0);
    self.Amount = ko.observable(0);
    self.BeneAccNumberMask = ko.observable();
    self.AmountUSD_r = ko.observable(0);

    //check tz number exist
    self.IsSearchTZ = ko.observable(false);
    self.TZModel = ko.observable(TZModel);

    self.instanceIdpayment = ko.observable();
    self.approverIdpayment = ko.observable();
    self.GetPaymentURL = ko.observable();

    // count of document
    self.CountDoc = 0;
    // Workflow task config
    self.WorkflowConfig = ko.observable();

    //aailable timelines tab
    self.IsTimelines = ko.observable(false);

    // utilize amount from underlying
    self.DealUnderlying = ko.observable(DealUnderlyingModel);
    self.UtilizationAmounts = ko.observable(0);

    //other account number selected
    self.IsOtherAccountNumber = ko.observable(false);

    // counter upload file ppu maker
    self.counterUpload = ko.observable(0);

    //underlying declare
    self.AmountModel = ko.observable(AmountModel);

    //file attachment edit
    self.SelectingUnderlying = ko.observable(true);

    //is fx transaction
    self.t_IsFxTransaction = ko.observable(false);
    self.t_IsFxTransactionToIDR = ko.observable(false);
    self.t_IsFxTransactionToIDRB = ko.observable(false);

    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerAttachUnderlyings = ko.observableArray([]);

    // parameters
    self.Products = ko.observableArray([]);
    self.Currencies = ko.observableArray([]);
    self.DebitCurrencies = ko.observableArray([]);
    self.Channels = ko.observableArray([]);
    self.BizSegments = ko.observableArray([]);
    self.ProductTypes = ko.observableArray([]);
    self.LLDs = ko.observableArray([]);
    self.Banks = ko.observableArray([]);
    self.BankCharges = ko.observableArray([]);
    self.AgentCharges = ko.observableArray([]);
    self.FXCompliances = ko.observableArray([]);
    self.UnderlyingDocs = ko.observableArray([]);
    self.POAFunctions = ko.observableArray([]);
    self.DocumentTypes = ko.observableArray([]);
    self.DocumentPurposes = ko.observableArray([]);

    //decrale other condition
    self.isNewUnderlying = ko.observable(false);
    self.IsOtherBank = ko.observable(false);
    self.IsUploading = ko.observable(false);

    // task properties
    self.ApproverId = ko.observable();
    self.SPWebID = ko.observable();
    self.SPSiteID = ko.observable();
    self.SPListID = ko.observable();
    self.SPListItemID = ko.observable();
    self.SPTaskListID = ko.observable();
    self.SPTaskListItemID = ko.observable();
    self.Initiator = ko.observable();
    self.WorkflowInstanceID = ko.observable();
    self.WorkflowID = ko.observable();
    self.WorkflowName = ko.observable();
    self.StartTime = ko.observable();
    self.StateID = ko.observable();
    self.StateDescription = ko.observable();
    self.IsAuthorized = ko.observable();
    self.TaskTypeID = ko.observable();
    self.TaskTypeDescription = ko.observable();
    self.Title = ko.observable();
    self.ActivityTitle = ko.observable();
    self.User = ko.observable();
    self.IsSPGroup = ko.observable();
    self.EntryTime = ko.observable();
    self.ExitTime = ko.observable();
    self.OutcomeID = ko.observable();
    self.OutcomeDescription = ko.observable();
    self.CustomOutcomeID = ko.observable();
    self.CustomOutcomeDescription = ko.observable();
    self.Comments = ko.observable();

    // value CIF underlying
    self.Treshold = ko.observable(0);
    self.FCYIDRTreshold = ko.observable(0);
    self.TotalRemainigByCIF = ko.observable(0);

    // Transaction Details
    self.TransactionAllNormal = ko.observable(TransactionAllNormalModel);

    self.TransactionMaker = ko.observable(TransactionMakerModel);
    self.TransactionChecker = ko.observable(TransactionCheckerModel);
    self.TransactionCheckerCallback = ko.observable(TransactionCheckerCallbackModel);
    self.TransactionContact = ko.observable(TransactionContactModel);
    self.TransactionCallback = ko.observable(TransactionCallbackModel);
    self.PaymentMaker = ko.observable(PaymentMakerModel);
    self.PaymentChecker = ko.observable(PaymentCheckerModel);
    self.TransactionDeal = ko.observable(TransactionDealModel);

    // filter Attach Underlying File
    self.AttachFilterFileName = ko.observable("");
    self.AttachFilterDocumentPurpose = ko.observable("");
    self.AttachFilterModifiedDate = ko.observable("");
    self.AttachFilterDocumentRefNumber = ko.observable("");

    // filter underlying
    self.UnderlyingFilterCIF = ko.observable("");
    self.UnderlyingFilterParentID = ko.observable("");
    self.UnderlyingFilterIsSelected = ko.observable(false);
    self.UnderlyingFilterUnderlyingDocument = ko.observable("");
    self.UnderlyingFilterDocumentType = ko.observable("");
    self.UnderlyingFilterCurrency = ko.observable("");
    self.UnderlyingFilterStatementLetter = ko.observable("");
    self.UnderlyingFilterAmount = ko.observable("");
    self.UnderlyingFilterDateOfUnderlying = ko.observable("");
    self.UnderlyingFilterExpiredDate = ko.observable("");
    self.UnderlyingFilterReferenceNumber = ko.observable("");
    self.UnderlyingFilterSupplierName = ko.observable("");
    self.UnderlyingFilterIsProforma = ko.observable("");

    // Task status from Nintex custom REST API
    self.IsPendingNintex = ko.observable();
    self.IsAuthorizedNintex = ko.observable();
    self.MessageNintex = ko.observable();

    // Task activity properties
    self.PPUChecker = ko.observable();

    //// start Checked Calculate for Attach Grid and Underlying Grid
    self.TempSelectedAttachUnderlying = ko.observableArray([]);

    //is fx transaction
    self.t_IsFxTransaction = ko.observable(false);
    self.t_IsFxTransactionToIDR = ko.observable(false);
    self.t_IsFxTransactionToIDRB = ko.observable(false);

    //properties
    self.LLDCode = ko.observable();
    // New Data flag
    self.IsNewData = ko.observable(false);

    // check all data
    self.CheckAll = ko.observable(false);

    // temp payment maker confirm
    self.BeneBanktmp = ko.observable('');
    self.SwiftCodetmp = ko.observable('');
    self.AccNumbertmp = ko.observable('');

    self.IsResidenttmp = ko.observable('');
    self.IsCitizentmp = ko.observable('');

    self.agentChargestmp = ko.observable('');
    self.bankChargestmp = ko.observable('');

    self.CargerBankOrig = ko.observable('');
    self.AgentChargesOrig = ko.observable('');

    // customer contact
    self.ID_c = ko.observable();
    self.CIF_c = ko.observable("");
    self.SourceID_c = ko.observable("2");
    self.Name_c = ko.observable("");
    self.PhoneNumber_c = ko.observable("");
    self.DateOfBirth_c = ko.observable('');
    self.Address_c = ko.observable("");
    self.IDNumber_c = ko.observable("");
    self.POAFunction_c = ko.observable();
    self.OccupationInID_c = ko.observable("");
    self.PlaceOfBirth_c = ko.observable("");
    self.EffectiveDate_c = ko.observable("");
    self.CancellationDate_c = ko.observable("");
    self.POAFunctionOther_c = ko.observable("");
    self.LastModifiedBy_c = ko.observable("");
    self.LastModifiedDate_c = ko.observable('');

    // customer underlying
    self.ID_u = ko.observable("");
    self.UnderlyingDocument_u = ko.observable('');
    self.DocumentType_u = ko.observable('');
    self.AmountUSD_u = ko.observable(0);
    self.Currency_u = ko.observable('');
    self.Rate_u = ko.observable(0);
    self.StatementLetter_u = ko.observable('');
    self.Amount_u = ko.observable(0);
    self.AttachmentNo_u = ko.observable("");
    self.IsDeclarationOfException_u = ko.observable(false);
    self.StartDate_u = ko.observable('');
    self.EndDate_u = ko.observable('');
    self.DateOfUnderlying_u = ko.observable("");
    self.ExpiredDate_u = ko.observable("");
    self.ReferenceNumber_u = ko.observable("");
    self.SupplierName_u = ko.observable("");
    self.InvoiceNumber_u = ko.observable("");
    self.IsProforma_u = ko.observable(false);
    self.IsBulkUnderlying_u = ko.observable(false);
    self.Proformas = ko.observableArray([]);
    self.BulkUnderlyings = ko.observableArray([]);
    self.ProformaDetails = ko.observableArray([]);
    self.BulkDetails = ko.observableArray([]);

    // Temporary documents
    self.Documents = ko.observableArray([]);
    self.DocumentPath = ko.observable();
    self.DocumentType = ko.observable();
    self.DocumentPurpose = ko.observable();
    self.DocumentFileName = ko.observable();

    self.CurrencyCode = ko.observable("");
    self.CurrencyAccCode = ko.observable("");

    // Original value chandra
    self.OriginalValue = ko.observable((new OriginalValueModel(new statusValueModel('', false),
        new statusValueModel('', false),
        new statusValueModel('', false),
        new statusValueModel('', false),
        new statusValueModel('', false),
        new statusValueModel('', false),
        new statusValueModel('', false),
        new statusValueModel('', false),
        new statusValueModel('', false),
        new statusValueModel('', false),
        new statusValueModel('', false)

    )));

    // Chandra 2015.02.24 attachment properties for maker checker
    // Attach Variable
    self.ID_a = ko.observable(0);
    self.ddlDocumentPurpose_a = ko.observableArray([]);
    self.tempddlDocumentPurpose_a = ko.observableArray([]);
    self.ddlDocumentType_a = ko.observableArray([]);
    self.DocumentPurpose_a = ko.observable();
    self.DocumentPath_a = ko.observable();
    self.DocumentType_a = ko.observable();
    self.CustomerUnderlyingMappings_a = ko.observableArray([CustomerUnderlyingMappingModel]);
    self.MakerDocuments = ko.observableArray([]);
    self.MakerUnderlyings = ko.observableArray([]);
    self.NewDocuments = ko.observableArray([]);

    var AttachDocuments = {
        Documents: self.Documents,
        DocumentPath: self.DocumentPath_a,
        Type: self.DocumentType_a,
        Purpose: self.DocumentPurpose_a
    }

    var CustomerUnderlyingFile = {
        ID: self.ID_a,
        FileName: DocumentModels().FileName,
        DocumentPath: DocumentModels().DocumentPath,
        DocumentPurpose: self.DocumentPurpose_a,
        DocumentType: self.DocumentType_a,
        DocumentRefNumber: self.DocumentRefNumber_a,
        LastModifiedBy: self.LastModifiedBy_a,
        LastModifiedDate: self.LastModifiedDate_a,
        CustomerUnderlyingMappings: self.CustomerUnderlyingMappings_a
    };

    //Bangkit 20141119
    self.Contact = ko.observable();
    self.Contacts = ko.observableArray([]);
    self.CalledContacts = ko.observableArray([]);

    self.OnCloseApproval = function () {
        self.TransactionMaker(TransactionMakerModel);
        self.TransactionAllNormal(TransactionAllNormalModel); //addall
        self.TransactionChecker(TransactionCheckerModel);
        self.TransactionCheckerCallback(TransactionCheckerCallbackModel);
        self.TransactionContact(TransactionContactModel);
        self.TransactionCallback(TransactionCallbackModel);
        self.PaymentMaker(PaymentMakerModel);
        self.PaymentChecker(PaymentCheckerModel);
        self.TransactionDeal(TransactionDealModel);
        self.IsOtherAccountNumber(false);
    }

    self.SelectedContacts = function (contact) {
        if (self.TransactionCallback().Callbacks.length == self.UtcAttemp()) {
            ShowNotification("Warning", "You can only tick " + self.UtcAttemp() + " contacts.", "gritter-warning", false);
            //document.getElementById('checkbox' + selectedIndex).checked = false;
            return;
        }

        $("#modal-form-callbacktime").modal('show');

        //set selected contact
        var tanggal = new Date();
        var jam = tanggal.getHours() > 9 ? tanggal.getHours() : '0' + tanggal.getHours();
        var menit = tanggal.getMinutes() > 9 ? tanggal.getMinutes() : '0' + tanggal.getMinutes();
        $('#TimePickerCallBack').val(jam + ':' + menit);
        $('#NameCallback').val(contact.Name);
        $('.bootstrap-timepicker-hour').val("00");
        $('.bootstrap-timepicker-minute').val("00");
        document.getElementById('IsUtc').checked = false;

        $('#backDrop').show();

        $('.time-picker').timepicker({
            defaultTime: "",
            minuteStep: 1,
            showSeconds: false,
            showMeridian: false
        }).next().on(ace.click_event, function () {
            $(this).prev().focus();
        });

        // validation
        $('#aspnetForm').validate({
            errorElement: 'div',
            errorClass: 'help-block',
            focusInvalid: true,

            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
            },

            success: function (e) {
                $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
                $(e).remove();
            },

            errorPlacement: function (error, element) {
                if (element.is(':checkbox') || element.is(':radio')) {
                    var controls = element.closest('div[class*="col-"]');
                    if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                    else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                }
                else if (element.is('.select2')) {
                    error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                }
                else if (element.is('.chosen-select')) {
                    error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                }
                else error.insertAfter(element.parent());
            }
        });

        self.Contact(contact);
    }

    self.TempCallBackTime = ko.observableArray([]);

    self.CloseCallBackTime = function () {
        $("#modal-form-callbacktime").modal('hide');

        $('#backDrop').hide();
    }

    self.TimeFormat = function (data) {
        if (data.length > 5) {
            var tanggal = new Date(data);
            return tanggal.format('HH:mm');
            // var jam = tanggal.getUTCHours() > 9 ? tanggal.getUTCHours() : '0' + tanggal.getUTCHours();
            // var menit = tanggal.getUTCMinutes() > 9 ? tanggal.getUTCMinutes() : '0' + tanggal.getUTCMinutes();
            // var result = jam + ':' + menit;
            // return result;
        } else {
            return data;
        }
    }

    self.SearchTZ = function () {
        viewModel.IsSearchTZ(true);
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/CheckAvailbaleTZ/" + viewModel.PaymentMaker().Payment.TZNumber,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                viewModel.TZModel(ko.mapping.toJS(data));
                viewModel.IsSearchTZ(false);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                viewModel.IsSearchTZ(false);
            }
        });
    }

    self.ApprovalButton = function (name) {
        if (name == 'Continue') {
            return 'Submit';
        } else {
            return name;
        }
    }

    // bind clear filters
    self.UnderlyingClearFilters = function () {
        //self.UnderlyingFilterParentID("");
        self.UnderlyingFilterIsSelected(false);
        self.UnderlyingFilterUnderlyingDocument("");
        self.UnderlyingFilterDocumentType("");
        self.UnderlyingFilterCurrency("");
        self.UnderlyingFilterStatementLetter("");
        self.UnderlyingFilterAmount("");
        self.UnderlyingFilterDateOfUnderlying("");
        self.UnderlyingFilterExpiredDate("");
        self.UnderlyingFilterReferenceNumber("");
        self.UnderlyingFilterSupplierName("");
        self.UnderlyingFilterIsProforma("");

        GetDataUnderlying();
    };

    self.PendingDocumentsClearFilters = function () {
        self.FilterApplicationID("");
        self.FilterSignatureVerified("");
        self.FilterDocument("");
        self.FilterCustomerName("");
        self.FilterProductName("");
        self.FilterCurrencyDesc("");
        self.FilterTransactionAmount("");
        self.FilterEqvUSD("");
        self.FilterFxTransaction("");
        self.FilterActivityTitle("");
        self.FilterLastModifiedBy("");
        self.FilterLastModifiedDate("");

        GetPendingDocuments();
    }

    //init transaction callback ( set data if exist )
    self.initTransactionCallback = function () {
        for (var i = 0; i <= self.TransactionCallback().Callbacks.length - 1; i++) {

            //set grid contact
            var isSelected = ko.utils.arrayFirst(self.TransactionCallback().Transaction.Customer.Contacts, function (data) {

                var indexOfContact = self.TransactionCallback().Transaction.Customer.Contacts.indexOf(self.TransactionCallback().Callbacks[i].Contact);

                if (data.ID == self.TransactionCallback().Callbacks[i].Contact.ID) {
                    document.getElementById('checkbox' + indexOfContact).checked = true;
                } else {
                    document.getElementById('checkbox' + indexOfContact).checked = false;
                }
            });
        }
    }

    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerUnderlyings = ko.observableArray([]);

    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerUnderlyingFiles = ko.observableArray([]);

    // grid properties Underlying
    self.UnderlyingGridProperties = ko.observable();
    self.UnderlyingGridProperties(new GridPropertiesModel(GetDataUnderlying));

    // grid properties Attach Grid
    self.AttachGridProperties = ko.observable();
    self.AttachGridProperties(new GridPropertiesModel(GetDataAttachFile));

    // set default sorting for Underlying Grid
    self.UnderlyingGridProperties().SortColumn("ID");
    self.UnderlyingGridProperties().SortOrder("ASC");

    // set default sorting for Underlying File Grid
    self.AttachGridProperties().SortColumn("FileName");
    self.AttachGridProperties().SortOrder("ASC");

    // Get filtered columns value
    function GetUnderlyingFilteredColumns() {
        var filters = [];

        //if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.UnderlyingFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.UnderlyingFilterIsSelected() });
        if (self.UnderlyingFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.UnderlyingFilterUnderlyingDocument() });
        if (self.UnderlyingFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.UnderlyingFilterDocumentType() });
        if (self.UnderlyingFilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.UnderlyingFilterCurrency() });
        if (self.UnderlyingFilterStatementLetter() != "") filters.push({ Field: 'StatementLetter', Value: self.UnderlyingFilterStatementLetter() });
        if (self.UnderlyingFilterAmount() != "") filters.push({ Field: 'Amount', Value: self.UnderlyingFilterAmount() });
        if (self.UnderlyingFilterDateOfUnderlying() != "") filters.push({ Field: 'DateOfUnderlying', Value: self.UnderlyingFilterDateOfUnderlying() });
        if (self.UnderlyingFilterExpiredDate() != "") filters.push({ Field: 'ExpiredDate', Value: self.UnderlyingFilterExpiredDate() });
        if (self.UnderlyingFilterReferenceNumber() != "") filters.push({ Field: 'ReferenceNumber', Value: self.UnderlyingFilterReferenceNumber() });
        if (self.UnderlyingFilterSupplierName() != "") filters.push({ Field: 'SupplierName', Value: self.UnderlyingFilterSupplierName() });

        return filters;
    };

    self.GetCalculateFX = function (amountUSD) {
        GetCalculateFX(amountUSD);
    }

    self.GetDataUnderlying = function () {
        GetDataUnderlying();
    };

    self.GetDataAttachFile = function () {
        GetDataAttachFile();
    }

    self.GetTotalAmount = function (CIFData, AmountUSD) {
        GetTotalAmount(CIFData, AmountUSD);
    }

    self.GetTotalAmountFX = function (CIFData, currentAmount) {
        GetTotalAmountFX(CIFData, currentAmount);
    }

    self.GetFCYIDRTreshold = function () {
        GetFCYIDRTreshold();
    }

    function ResetDataAttachment(index, isSelect) {
        self.CustomerAttachUnderlyings()[index].IsSelectedAttach = isSelect;
        var dataRows = self.CustomerAttachUnderlyings().slice(0);
        self.CustomerAttachUnderlyings([]);
        self.CustomerAttachUnderlyings(dataRows);
    }

    function GetCalculateFX(amountUSD) {
        AmountModel.TotalPPUAmountsUSD(TotalPPUModel.Total.IDRFCYPPU);
        AmountModel.TotalDealAmountsUSD(TotalDealModel.Total.IDRFCYDeal)
        AmountModel.RemainingBalance(TotalDealModel.Total.RemainingBalance);
        AmountModel.TotalPPUUtilization(TotalPPUModel.Total.UtilizationPPU);
        AmountModel.TotalDealUtilization(TotalDealModel.Total.UtilizationDeal);
    }

    self.DealCalculate = function () {

        var t_IsFxTransaction = (viewModel.TransactionDeal().Transaction.Currency.Code != 'IDR' && viewModel.TransactionDeal().Transaction.AccountCurrencyCode == 'IDR') ? true : false;
        var t_IsFxTransactionToIDR = (viewModel.TransactionDeal().Transaction.Currency.Code == 'IDR' && viewModel.TransactionDeal().Transaction.AccountCurrencyCode != 'IDR') ? true : false;
        var t_IsFxTransactionToIDRB = (viewModel.TransactionDeal().Transaction.Currency.Code == 'IDR' && viewModel.TransactionDeal().Transaction.AccountCurrencyCode != 'IDR' && viewModel.TransactionDeal().Transaction.ProductType.IsFlowValas == true) ? true : false;

        viewModel.t_IsFxTransaction(t_IsFxTransaction);
        viewModel.t_IsFxTransactionToIDR(t_IsFxTransactionToIDR);
        viewModel.t_IsFxTransactionToIDRB(t_IsFXTransactionToIDRB);

        var t_TotalAmountsUSD = 0;
        var t_TotalAmountsUSD_stB = 0;

        var t_RemainingBalance = 0;

        var t_TotalDealTransaction = parseFloat(viewModel.AmountModel().TotalAmounts());
        var t_TotalDealTransactionB = isNaN(parseFloat(viewModel.AmountModel().TotalAmountsStatementB())) ? 0 : parseFloat(viewModel.AmountModel().TotalAmountsStatementB()); //avoid NaN

        var t_FcyAmount = parseFloat(viewModel.TransactionDeal().Transaction.AmountUSD);
        var t_IsFxTransaction = viewModel.t_IsFxTransaction();
        var t_IsFxTransactionToIDR = viewModel.t_IsFxTransactionToIDR();
        var t_IsFXTransactionToIDRB = viewModel.t_IsFxTransactionToIDRB();

        var t_Treshold = parseFloat(viewModel.AmountModel().TreshHold());
        var t_RemainingBalanceByCIF = viewModel.AmountModel().TotalRemainigByCIF();

        /* START get Amount USD Trx Total */
        if (t_IsFxTransaction) {
            t_TotalAmountsUSD = t_TotalDealTransaction;
            t_TotalAmountsUSD_stB = t_TotalDealTransactionB;
        }
        else if (t_IsFxTransactionToIDR) {
            t_TotalAmountsUSD = t_TotalDealTransaction;
            t_TotalAmountsUSD_stB = t_TotalDealTransactionB;
        }
        else {
            t_TotalAmountsUSD = t_TotalDealTransaction;
            t_TotalAmountsUSD_stB = t_TotalDealTransactionB;
        }
        viewModel.AmountModel().TotalDealAmountsUSD(t_TotalAmountsUSD);
        /* END OF get Amount USD Trx Total */

        /* START get Remaining Underlying Balance */
        if (viewModel.TransactionDeal().Transaction.StatementLetter != undefined) {
            if (viewModel.TransactionDeal().Transaction.StatementLetter.ID == 1) {

                // begin if statement A
                if (t_TotalAmountsUSD > t_Treshold) {
                    //t_RemainingBalance = t_Treshold - t_TotalAmountsUSD;
                    t_RemainingBalance = t_RemainingBalanceByCIF - t_TotalAmountsUSD_stB;
                }
                else {
                    t_RemainingBalance = 0.00;
                }
            }
            else {
                // begin if statement B
                t_RemainingBalance = t_RemainingBalanceByCIF - t_TotalAmountsUSD_stB;
            }
            if (t_IsFxTransactionToIDR) {
                if (t_FcyAmount >= self.FCYIDRTreshold() && t_IsFxTransactionToIDRB == true) {
                    t_RemainingBalance = t_RemainingBalance - t_FcyAmount;
                } else {
                    t_RemainingBalance = t_RemainingBalance;
                }
            }

            viewModel.AmountModel().RemainingBalance(t_RemainingBalance);
        }
    }

    // Get filtered columns value
    function GetAttachFilteredColumns() {
        var filters = [];

        if (self.AttachFilterFileName() != "") filters.push({ Field: 'FileName', Value: self.AttachFilterFileName() });
        if (self.AttachFilterDocumentPurpose() != "") filters.push({ Field: 'DocumentPurpose', Value: self.AttachFilterDocumentPurpose() });
        if (self.AttachFilterModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.AttachFilterModifiedDate() });
        if (self.AttachFilterDocumentRefNumber() != "") filters.push({ Field: 'DocumentRefNumber', Value: self.AttachFilterDocumentRefNumber() });

        return filters;
    };

    function GetCustomerUnderlyingFilters() {
        var filters = [];
        var _cif = viewModel.TransactionAllNormal().Transaction.Customer.CIF;
        filters.push({ Field: 'CIF', Value: _cif });
        return filters;
    }

    function GetTreshold(cif_d) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.singlevalueparameter + "/FX_TRANSACTION_TRESHOLD",
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            }, success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.Treshold(data);
                    GetAvailableStatementB(cif_d);
                }
            }
        });
    }

    function GetFCYIDRTreshold() {
        $.ajax({
            type: "GET",
            url: api.server + api.url.singlevalueparameter + "/FCY_IDR_TRANSACTION_TRESHOLD",
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            }, success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.FCYIDRTreshold(data);
                }
            }
        });
    }

    function GetTotalAmountFX(cif, currentAmountUSD) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.transaction + "/GetAmountTransaction=" + cif,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            }, success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    if (data == undefined) {
                        viewModel.AmountModel().TotalDealAmountsUSD(0);
                    } else {
                        //self.TransactionModel().TotalTransFX(parseFloat(data));
                        viewModel.AmountModel().TotalDealAmountsUSD(parseFloat(data) - parseFloat(currentAmountUSD));
                    }
                }
            }
        });

    }

    function GetTotalAmount(CIFData, AmountUSD) {
        var totalValue1 = $.ajax({
            type: "GET",
            url: api.server + api.url.transactiondeal + "/GetAmountTransaction=" + CIFData + "?isStatementB=false",
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            }
        });
        var totalValue2 = $.ajax({
            type: "GET",
            url: api.server + api.url.transactiondeal + "/GetAmountTransaction=" + CIFData + "?isStatementB=true",
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            }
        });
        $.when(totalValue1, totalValue2).done(function (data1, data2) {
            var t_TotalAmounts = 0;
            if (data1 != null || data1 != undefined) {
                var t_TotalAmounts1 = parseFloat(data1);
                var t_TotalAmounts2 = parseFloat(data2);

                self.AmountModel().TotalAmounts(t_TotalAmounts2);
                self.AmountModel().TotalAmountsStatementB(t_TotalAmounts1);
                self.DealCalculate();
            }
            else {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    function GetTotalAmount(CIFData, IsStatementB) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.transactiondeal + "/GetAmountTransaction=" + CIFData + "?isStatementB=" + IsStatementB,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {

                var t_TotalAmounts = 0;
                if (jqXHR.status = 200) {
                    var t_TotalAmounts = parseFloat(data);
                    if (IsStatementB == true) {
                        self.AmountModel().TotalAmounts(t_TotalAmounts);
                    }
                    else {
                        self.AmountModel().TotalAmountsStatementB(t_TotalAmounts);
                    }
                }
                else if (jqXHR.status = 204) {
                    if (IsStatementB == true) {
                        self.AmountModel().TotalAmounts(t_TotalAmounts);
                    }
                    else {
                        self.AmountModel().TotalAmountsStatementB(t_TotalAmounts);
                    }
                }
                else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    function GetAvailableStatementB(cif_d) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/GetAvailableStatementB/" + cif_d,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                var t_TotalRemainig = 0;
                if (jqXHR.status = 200) {
                    t_TotalRemainig = data;
                    viewModel.AmountModel().TotalRemainigByCIF(t_TotalRemainig);

                } else {

                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    function GetUnderlyingFile() {
        var options = {
            url: api.server + api.url.customerunderlyingfile + "/TransactionDeal/" + DealID,
            token: accessToken
        };

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataAttachFile, OnError, OnAlways);

        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataAttachFile, OnError, OnAlways);
        }
    }

    function GetDataAttachFile() {
        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlyingfile + "/TransactionDeal/" + DealID,
            params: {
                page: self.AttachGridProperties().Page(),
                size: self.AttachGridProperties().Size(),
                sort_column: self.AttachGridProperties().SortColumn(),
                sort_order: self.AttachGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetAttachFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataAttachFile, OnError, OnAlways);

        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataAttachFile, OnError, OnAlways);
        }
    }

    // On success GetData callback
    function OnSuccessGetDataAttachFile(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.CustomerUnderlyingFiles(data.Rows);

            self.AttachGridProperties().Page(data['Page']);
            self.AttachGridProperties().Size(data['Size']);
            self.AttachGridProperties().Total(data['Total']);
            self.AttachGridProperties().TotalPages(Math.ceil(self.AttachGridProperties().Total() / self.AttachGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    function GetDataUnderlying() {
        // widget reloader function start
        var options = {
            url: api.server + api.url.customerunderlying + "/TransactionDeal/" + DealID,
            params: {
                page: self.UnderlyingGridProperties().Page(),
                size: self.UnderlyingGridProperties().Size(),
                sort_column: self.UnderlyingGridProperties().SortColumn(),
                sort_order: self.UnderlyingGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetUnderlyingFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataUnderlying, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataUnderlying, OnError, OnAlways);
        }
    }

    // On success GetData callback 1
    function OnSuccessGetDataUnderlying(data, textStatus, jqXHR) {

        if (jqXHR.status = 200) {

            var UtilizationAmount = 0;
            for (i = 0; i < data['Total']; i++) {
                UtilizationAmount = UtilizationAmount + data.Rows[i].UtilizationAmount;

            }

            self.CustomerUnderlyings(data.Rows);
            self.UnderlyingGridProperties().Page(data['Page']);
            self.UnderlyingGridProperties().Size(data['Size']);
            self.UnderlyingGridProperties().Total(data['Total']);
            self.UnderlyingGridProperties().TotalPages(Math.ceil(self.UnderlyingGridProperties().Total() / self.UnderlyingGridProperties().Size()));

            self.UtilizationAmounts(UtilizationAmount);
            GetDataAttachFile();
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    //added chandra
    self.GetUnderlyingSelectedRow = function (data) {

        $("#modal-form-Underlying").modal('show');

        self.ID_u(data.ID);
        self.Amount_u(data.Amount);
        self.UnderlyingDocument_u(data.UnderlyingDocument.Code + " (" + data.UnderlyingDocument.Name + ")");
        //self.OtherUnderlying_u(data.OtherUnderlying);
        self.DocumentType_u(data.DocumentType.Name);

        //self.Currency_u(data.Currency);
        self.Currency_u(data.Currency.Code + " (" + data.Currency.Description + ")");

        self.Rate_u(data.Rate);
        self.AmountUSD_u(data.AmountUSD);
        self.AttachmentNo_u(data.AttachmentNo);

        //self.StatementLetter_u(data.StatementLetter);
        self.StatementLetter_u(data.StatementLetter.Name);

        self.IsDeclarationOfException_u(data.IsDeclarationOfException);
        self.StartDate_u(self.LocalDate(data.StartDate, true, false));
        self.EndDate_u(self.LocalDate(data.EndDate, true, false));
        self.DateOfUnderlying_u(self.LocalDate(data.DateOfUnderlying, true, false));
        self.ExpiredDate_u(self.LocalDate(data.ExpiredDate, true, false));
        self.ReferenceNumber_u(data.ReferenceNumber);
        self.SupplierName_u(data.SupplierName);
        self.InvoiceNumber_u(data.InvoiceNumber);
        self.IsProforma_u(data.IsProforma);
        self.IsBulkUnderlying_u(data.IsBulkUnderlying);
        self.Proformas(data.Proformas);
        self.BulkUnderlyings(data.BulkUnderlyings);
        self.ProformaDetails(data.ProformaDetails);
        self.BulkDetails(data.BulkDetails);

        $('#backDrop').show();
    }


    self.ReplaceBoolean = function (value) {
        if (value == true) return "yes";
        else return "no";
    }

    self.GridProperties = ko.observable();
    self.GridProperties(new GridPropertiesModel(GetData));

    self.GridProperties().SortColumn("EntryTime");
    self.GridProperties().SortOrder("DESC");

    // Options selected value
    self.Selected = ko.observable(SelectedModel);

    // filters
    self.FilterWorkflowName = ko.observable("");
    self.FilterInitiator = ko.observable("");
    self.FilterStateDescription = ko.observable("");
    self.FilterStartTime = ko.observable("");
    self.FilterTitle = ko.observable("");
    self.FilterActivityTitle = ko.observable("");
    self.FilterUser = ko.observable("");
    self.FilterUserApprover = ko.observable("");
    self.FilterEntryTime = ko.observable("");
    self.FilterExitTime = ko.observable("");
    self.FilterOutcomeDescription = ko.observable("");
    self.FilterCustomOutcomeDescription = ko.observable("");
    self.FilterComments = ko.observable("");

    // filters transaction
    self.FilterApplicationID = ko.observable("");
    self.FilterCustomer = ko.observable("");
    self.FilterProduct = ko.observable("");
    self.FilterCurrency = ko.observable("");
    self.FilterAmount = ko.observable("");
    self.FilterAmountUSD = ko.observable("");
    self.FilterDebitAccNumber = ko.observable("");
    self.FilterFXTransaction = ko.observable("");
    self.FilterTopUrgent = ko.observable("");

    //Declare an ObservableArray for Storing the JSON Response
    self.Tasks = ko.observableArray([]);
    self.Outcomes = ko.observableArray([]);

    // customer contact
    var CustomerContact = {
        ID: self.ID_c,
        CIF: self.CIF_c,
        SourceID: self.SourceID_c,
        Name: self.Name_c,
        PhoneNumber: self.PhoneNumber_c,
        DateOfBirth: self.DateOfBirth_c,
        Address: self.Address_c,
        IDNumber: self.IDNumber_c,
        POAFunction: self.POAFunction_c,
        OccupationInID: self.OccupationInID_c,
        PlaceOfBirth: self.PlaceOfBirth_c,
        EffectiveDate: self.EffectiveDate_c,
        CancellationDate: self.CancellationDate_c,
        POAFunctionOther: self.POAFunctionOther_c,
        LastModifiedBy: self.LastModifiedBy_c,
        LastModifiedDate: self.LastModifiedDate_c
    };

    self.AddContact = function () {
        self.IsNewData(true);
        self.ClearData();

        $("#modal-form-child").modal('show');

        $('#backDrop').show();

        // datepicker
        $('.date-picker').datepicker({ autoclose: true }).next().on(ace.click_event, function () {
            $(this).prev().focus();
        });

        // validation
        $('#aspnetForm').validate({
            errorElement: 'div',
            errorClass: 'help-block',
            focusInvalid: true,

            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
            },

            success: function (e) {
                $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
                $(e).remove();
            },

            errorPlacement: function (error, element) {
                if (element.is(':checkbox') || element.is(':radio')) {
                    var controls = element.closest('div[class*="col-"]');
                    if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                    else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                }
                else if (element.is('.select2')) {
                    error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                }
                else if (element.is('.chosen-select')) {
                    error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                }
                else error.insertAfter(element.parent());
            }
        });
    }

    self.ClearData = function () {
        self.ID_c(0);
        self.Name_c('');
        self.PhoneNumber_c('');
        self.DateOfBirth_c('');
        self.Address_c('');
        self.IDNumber_c('');
        self.Selected().POAFunction('');
        self.OccupationInID_c('');
        self.PlaceOfBirth_c('');
        self.EffectiveDate_c('');
        self.CancellationDate_c('');
        self.POAFunctionOther_c('');
    }

    self.Close = function () {
        $("#modal-form-child").modal('hide');
        $("#modal-form-Attach1").modal('hide');
        $('#backDrop').hide();
    }
    self.CloseUnderlying = function () {
        $("#modal-form-Underlying").modal('hide');
        //$("#modal-form-upload").modal('hide');
        $('#backDrop').hide();
    }
    self.CloseDocument = function () {
        $("#modal-form-Attach1").modal('hide');
        $('#backDrop').hide();
        $('.remove').click();
    }

    self.GetSelectedContactRow = function (contact) {
        self.IsNewData(false);

        self.ID_c(contact.ID);
        self.SourceID_c('2');
        self.Name_c(contact.Name);
        self.PhoneNumber_c(contact.PhoneNumber);

        self.Address_c(contact.Address);
        self.IDNumber_c(contact.IDNumber);
        self.Selected().POAFunction(contact.POAFunction.ID);
        self.POAFunction_c(contact.POAFunction);
        self.OccupationInID_c(contact.OccupationInID);
        self.PlaceOfBirth_c(contact.PlaceOfBirth);        
        /*self.DateOfBirth_c(moment(contact.DateOfBirth).format('DD-M-YYYY'));
        self.EffectiveDate_c(moment(contact.EffectiveDate).format('DD-M-YYYY'));
        self.CancellationDate_c(moment(contact.CancellationDate).format('DD-M-YYYY'));*/
        self.DateOfBirth_c(self.LocalDate(contact.DateOfBirth, true, false));
        self.EffectiveDate_c(self.LocalDate(contact.EffectiveDate, true, false));
        self.CancellationDate_c(self.LocalDate(contact.CancellationDate, true, false));
        self.POAFunctionOther_c(contact.POAFunctionOther);
        $("#modal-form-child").modal('show');
        $('#backDrop').show();
    }

    function getStyle(el, prop) {
        var x = document.getElementById(el);

        if (window.getComputedStyle) {
            var y = document.defaultView.getComputedStyle(x, null).getPropertyValue(prop);
        } else if (x.currentStyle) {
            var y = x.currentStyle[styleProp];
        }

        return y;
    }

    self.save_c = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();
        SetPOAFunctionOther();
        if (form.valid()) {
            //Ajax call to insert the CustomerContacts
            $.ajax({
                type: "POST",
                url: api.server + api.url.customercontact + '/Add/Workflow/' + self.WorkflowInstanceID() + '/ApproverID/' + self.ApproverId(),
                data: ko.toJSON(CustomerContact), //Convert the Observable Data into JSON
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + accessToken
                },
                success: function (data, textStatus, jqXHR) {
                    if (jqXHR.status = 200) {
                        // send notification
                        ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                        // hide current popup window
                        $("#modal-form-child").modal('hide');
                        $('#backDrop').hide();

                        // refresh data
                        GetTransactionData(self.WorkflowInstanceID(), self.ApproverId());
                    } else {
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // send notification
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            });
        }
    };

    self.update_c = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();
        SetPOAFunctionOther();
        if (form.valid()) {
            //Ajax call to insert the CustomerContacts
            $.ajax({
                type: "PUT",
                url: api.server + api.url.customercontact + '/Update/Workflow/' + self.WorkflowInstanceID() + '/ApproverID/' + self.ApproverId(),
                data: ko.toJSON(CustomerContact), //Convert the Observable Data into JSON
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + accessToken
                },
                success: function (data, textStatus, jqXHR) {
                    if (jqXHR.status = 200) {
                        // send notification
                        ShowNotification('Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                        // hide current popup window
                        $("#modal-form-child").modal('hide');
                        $('#backDrop').hide();

                        // refresh data
                        GetTransactionData(self.WorkflowInstanceID(), self.ApproverId());
                    } else {
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // send notification
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            });
        }
    };

    self.delete_c = function () {
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            //Ajax call to insert the CustomerContacts
            $.ajax({
                type: "PUT",
                url: api.server + api.url.customercontact + '/Delete/Workflow/' + self.WorkflowInstanceID() + '/ApproverID/' + self.ApproverId(),
                data: ko.toJSON(CustomerContact), //Convert the Observable Data into JSON
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + accessToken
                },
                success: function (data, textStatus, jqXHR) {
                    if (jqXHR.status = 200) {
                        // send notification
                        ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                        // hide current popup window
                        $("#modal-form-child").modal('hide');
                        $('#backDrop').hide();

                        // refresh data
                        GetTransactionData(self.WorkflowInstanceID(), self.ApproverId());
                    } else {
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // send notification
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            });
        }
    };

    //set POA Function Other Other
    function SetPOAFunctionOther() {
        if (self.Selected().POAFunction() != 9) {
            self.POAFunctionOther_c('');
        }
    }
    // upload document
    self.UploadDocument = function () {
        self.IsUploading(false);
        $("#modal-form-Attach1").modal('show');
        $('.remove').click();
        var item = ko.utils.arrayFilter(self.DocumentPurposes(), function (dta) { return dta.ID != 2 });
        self.ddlDocumentPurpose_a(item);
        self.ID_a(0);
        self.DocumentPurpose_a(null);
        self.Selected().DocumentPurpose_a(null);
        self.DocumentType_a(null);
        self.Selected().DocumentType_a(null);
        self.DocumentPath_a(null);

        $('#backDrop').show();
    }

    // Add new document handler
    self.AddDocument = function () {
        var form = $("#aspnetForm");
        form.validate();

        if (form.valid()) {
            var vDocumentType = ko.utils.arrayFirst(self.DocumentTypes(), function (item) {
                return item.ID == self.Selected().DocumentType();
            });

            var vDocumentPurpose = ko.utils.arrayFirst(self.DocumentPurposes(), function (item) {
                return item.ID == self.Selected().DocumentPurpose();
            });

            var file = ko.toJS(self.DocumentPath())

            var doc = {
                ID: 0,
                Type: vDocumentType,
                Purpose: vDocumentPurpose,
                FileName: file.name,
                DocumentPath: self.DocumentPath(),
                LastModifiedDate: new Date(),
                LastModifiedBy: null
            };

            var isDocExist = ko.utils.arrayFirst(self.TransactionAllNormal().Transaction.Documents, function (item) {
                return item.FileName == doc.FileName && item.Purpose.Name == doc.Purpose.Name && item.Type.Name == doc.Type.Name;
            });

            if (isDocExist) {
                ShowNotification('Warning', 'Data already exist.', 'gritter-warning', false);
            } else {
                self.TransactionAllNormal().Transaction.Documents.push(doc);

                var update = viewModel.TransactionAllNormal();
                viewModel.TransactionAllNormal(ko.mapping.toJS(update));

                // hide upload dialog
                $("#modal-form-Attach1").modal('hide');
                $('#backDrop').hide();
            }
        }

        $('.remove').click();
    };

    //is deleted document
    self.IsDeleteAllowed = function (id) {
        if (id != 0) {
            return false;
        } else {
            return true;
        }
    };

    //delete document
    self.RemoveDocument = function (data) {
        var result = ko.utils.arrayFilter(self.TransactionAllNormal().Transaction.Documents, function (item) {
            return ((item.DocumentPath != data.DocumentPath));
        });

        self.TransactionAllNormal().Transaction.Documents = [];

        for (var i = 0; i <= result.length - 1; i++) {
            self.TransactionAllNormal().Transaction.Documents.push(result[i]);
        }

        var update = viewModel.TransactionAllNormal();
        viewModel.TransactionAllNormal(ko.mapping.toJS(update));
        //console.log(self.TransactionAllNormal().Transaction.Documents);
    };

    // delete document on ppumaker
    self.RemoveDocumentData = function (data) {
        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (result) {
                if (data.Purpose.ID == 2) {
                    // delete underlying document
                    DeleteUnderlyingAttachment(data);
                    var item = ko.utils.arrayFilter(viewModel.TransactionAllNormal().Transaction.Documents, function (dta) { return dta.DocumentPath != data.DocumentPath; });
                    if (item != null) {
                        viewModel.TransactionAllNormal().Transaction.Documents = item;
                    }
                } else {
                    if (data.IsNewDocument == false) {
                        var item = ko.utils.arrayFilter(viewModel.TransactionAllNormal().Transaction.Documents, function (dta) { return dta.ID != data.ID; });
                        if (item != null) {
                            viewModel.TransactionAllNormal().Transaction.Documents = item;
                        }
                    }
                }
                self.MakerDocuments.remove(data);
            }
        });
    }

    function DeleteUnderlyingAttachment(item) {
        //Ajax call to delete the Customer

        $.ajax({
            type: "DELETE",
            url: api.server + api.url.customerunderlyingfile + "/" + item.ID,
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                // send notification
                if (jqXHR.status = 200) {
                    DeleteFile(item.DocumentPath);
                    if (!item.IsNewDocument) {
                        DeleteTransactionDocumentUnderlying(item);
                    }
                    ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);
                    // refresh data
                    //GetDataAttachFile();
                } else
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    function DeleteTransactionDocumentUnderlying(item) {
        //Ajax call to delete the Customer
        var pathdocs = item.DocumentPath.split('/');
        var file = pathdocs[pathdocs.length - 1].split('.');
        var fileName = file[0];
        $.ajax({
            type: "DELETE",
            url: api.server + api.url.transaction + "/DocumentPath/Delete/" + fileName,
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                // send notification
                if (jqXHR.status = 200) {
                    //DeleteFile(item.DocumentPath);
                    //ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);
                    // refresh data
                    //GetDataAttachFile();
                } else
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    function DeleteFile(documentPath) {
        // Get the server URL.
        var serverUrl = _spPageContextInfo.webAbsoluteUrl;
        // output variable
        var output;

        // Delete the file to the SharePoint folder.
        var deleteFile = deleteFileToFolder();
        deleteFile.done(function (file, status, xhr) {
            output = file.d;
        });
        deleteFile.fail(OnError);
        // Call function delete File Shrepoint Folder
        function deleteFileToFolder() {
            return $.ajax({
                url: serverUrl + "/_api/web/GetFileByServerRelativeUrl('" + documentPath + "')",
                type: "POST",
                headers: {
                    "accept": "application/json;odata=verbose",
                    "X-RequestDigest": $("#__REQUESTDIGEST").val(),
                    "X-HTTP-Method": "DELETE",
                    "IF-MATCH": "*"
                    //"content-length": arrayBuffer.byteLength
                }
            });
        }
    }

    // bind get data function to view
    self.GetData = function () { GetData(); };
    self.GetParameters = function () { GetParameters(); };
    self.GetCustomerUnderlying = function () { GetCustomerUnderlying(); }
    // bind clear filters
    self.ClearFilters = function () {
        self.FilterWorkflowName("");
        self.FilterInitiator("");
        self.FilterStateDescription("");
        self.FilterStartTime("");
        self.FilterTitle("");
        self.FilterActivityTitle("");
        self.FilterUser("");
        self.FilterUserApprover("");
        self.FilterEntryTime("");
        self.FilterExitTime("");
        self.FilterOutcomeDescription("");
        self.FilterCustomOutcomeDescription("");
        self.FilterComments("");

        self.FilterApplicationID("");
        self.FilterCustomer("");
        self.FilterProduct("");
        self.FilterCurrency("");
        self.FilterAmount("");
        self.FilterAmountUSD("");
        self.FilterDebitAccNumber("");
        self.FilterFXTransaction("");
        self.FilterTopUrgent("");

        GetData();
    };

    // Color Status
    // Update Rizki 2015-03-06
    self.SetColorStatus = function (id, applicationID, outcomeDescription, customOutcomeDescription) {
        if (outcomeDescription == null) {
            outcomeDescription = '';
        }

        if (customOutcomeDescription == null) {
            customOutcomeDescription = '';
        }

        if (id == 2) {
            return "active";
        }
        else if (id == 4) {
            //Untuk handle FX Checker reject dan PPU Checker Approve Cancellation
            if ((applicationID.toLowerCase().startsWith('fx') && outcomeDescription.toLowerCase() == 'rejected') || (customOutcomeDescription.toLowerCase() == 'approve cancellation')) {
                return "warning";
            }
            else {
                return "success";
            }
        }
        else if (id == 8) {
            return "warning";
        }
        else if (id == 64) {
            return "danger";
        }
        else {
            return "";
        }
    };

    // Local Date
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);
        if (moment(localDate).isValid()) {
            if (date == '1970/01/01 00:00:00' || date == '1970-01-01T00:00:00' || date == null) {
                return "";
            }
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return ""
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-17
    };

    self.FormatNumber = function (num) {
        var n = num.toString(), p = n.indexOf('.');
        return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
            return p < 0 || i < p ? ($0 + ',') : $0;
        });
    }

    //Function to Display record to be updated
    self.GetSelectedRow = function (data) {
        self.ApproverId(data.WorkflowContext.ApproverID);
        self.SPWebID(data.WorkflowContext.SPWebID);
        self.SPSiteID(data.WorkflowContext.SPSiteID);
        self.SPListID(data.WorkflowContext.SPListID);
        self.SPListItemID(data.WorkflowContext.SPListItemID);
        self.SPTaskListID(data.WorkflowContext.SPTaskListID);
        self.SPTaskListItemID(data.WorkflowContext.SPTaskListItemID);
        self.Initiator(data.WorkflowContext.Initiator);
        self.WorkflowInstanceID(data.WorkflowContext.WorkflowInstanceID);
        self.WorkflowID(data.WorkflowContext.WorkflowID);
        self.WorkflowName(data.WorkflowContext.WorkflowName);
        self.StartTime(data.WorkflowContext.StartTime);
        self.StateID(data.WorkflowContext.StateID);
        self.StateDescription(data.WorkflowContext.StateDescription);
        self.IsAuthorized(data.WorkflowContext.IsAuthorized);

        self.TaskTypeID(data.CurrentTask.TaskTypeID);
        self.TaskTypeDescription(data.CurrentTask.TaskTypeDescription);
        self.Title(data.CurrentTask.Title);
        self.ActivityTitle(data.CurrentTask.ActivityTitle);
        self.User(data.CurrentTask.User);
        self.IsSPGroup(data.CurrentTask.IsSPGroup);
        self.EntryTime(data.CurrentTask.EntryTime);
        self.ExitTime(data.CurrentTask.ExitTime);
        self.OutcomeID(data.CurrentTask.OutcomeID);
        self.OutcomeDescription(data.CurrentTask.OutcomeDescription);
        self.CustomOutcomeID(data.CurrentTask.CustomOutcomeID);
        self.CustomOutcomeDescription(data.CurrentTask.CustomOutcomeDescription);
        self.Comments(data.CurrentTask.Comments);

        self.Outcomes.removeAll();
        // add chandra set original value

        // show the dialog task form
        $("#modal-form").modal('show');

        // while is pending task, we need to get outcomes
        if (data.CurrentTask.OutcomeID == 2) {
            // lock current task for current user only
            //LockAndUnlockTaskHub("Lock", self.WorkflowInstanceID(), self.ApproverId(), self.SPTaskListItemID(), self.ActivityTitle());

            // get nintex outcomes
            //GetTaskOutcomes(data.WorkflowContext.SPTaskListID, data.WorkflowContext.SPTaskListItemID);
        }

        // ruddy : by pass to check pending outcome
        // get nintex outcomes


        GetTaskOutcomes(data.WorkflowContext.SPTaskListID, data.WorkflowContext.SPTaskListItemID);
    };

    self.OpenTask = function (data) {

    };

    // set approval ui template
    self.ApprovalTemplate = function () {
        var output;

        switch (self.ActivityTitle()) {
            case "CBO Maker Task":
                output = "TaskCBOMaker";
                break;
            case "CBO Checker Task":
                output = "TaskCBOChecker";
                break;
                /*case "CBO Maker Reactivation Task":
                    output = "TaskCBOMakerReactivation";
                    break;
                case "CBO Checker Reactivation Task":
                    output = "TaskCBOCheckerReactivation";
                    break; */
            case "FX Deal Checker Task":
                output = "TaskFXDealChecker";
                break;
            case "Pending Documents Checker Task":
                output = "TaskPendingDocumentsChecker";
                break;
            default:
                output = "TransactionAll";
        }

        return output;
    };

    self.UtcAttemp = ko.observable();
    function GetUtcAttemp() {
        var options = {
            url: api.server + api.url.utcattemps,
            params: {
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetUtcAttemp, OnError, OnAlways);
    }

    function OnSuccessGetUtcAttemp(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            // bind result to observable array
            self.UtcAttemp(data);
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }

    // set approval data template
    self.ApprovalData = function () {
        var output;

        switch (self.ActivityTitle()) {
            case "CBO Maker Task":
                output = self.TransactionContact();
                break;
            case "CBO Checker Task":
                output = self.TransactionContact();
                break;
                /*case "CBO Maker Reactivation Task":
                    output = self.TransactionChecker();
                    break;
                case "CBO Checker Reactivation Task":
                    output = self.TransactionChecker();
                    break; */
            case "FX Deal Checker Task":
                output = self.TransactionDeal(); //ganti di sini
                break;
            case "Pending Documents Checker Task":
                output = self.TransactionAllNormal(); // addall
            default:
                output = self.TransactionAllNormal();
        }

        return output;
    };
    function EnableMessageApprover() {
        self.OriginalValue().BeneAccNumberOri().Status(false);
        self.OriginalValue().AmountOri().Status(false);
        self.OriginalValue().RateOri().Status(false);
        self.OriginalValue().AmountUSDOri().Status(false);
        self.OriginalValue().BeneNameOri().Status(false);
        self.OriginalValue().BanksOri().Status(false);
        //self.OriginalValue().AccountOri().Status(false);

        self.OriginalValue().SwiftCodeOri().Status(false);
        self.OriginalValue().BankChargesOri().Status(false);
        self.OriginalValue().AgentChargesOri().Status(false);
        self.OriginalValue().IsCitizenOri().Status(false);
        self.OriginalValue().IsResidentOri().Status(false);
    }

    // rename status fx transaction & top urgent
    self.RenameStatus = function (type, value) {
        if (type == "FX Transaction") {
            if (value) {
                return "Yes";
            } else {
                return "No";
            }
        } else if (type == "Urgency") {
            if (value) {
                return "Top Urgent";
            } else {
                return "Normal";
            }
        } else {
            return "Unknown";
        }
    };

    // rename status filter fx Transaction & top urgent : add chandra
    self.RenameStatusFilter = function (type, value) {
        if (type == "FX Transaction") {
            if (value != "") {
                if (("yes").indexOf(value.toLowerCase()) > -1) {
                    return true;
                } else if (("no").indexOf(value.toLowerCase()) > -1) {
                    return false;
                } else {
                    return "-";
                }
            } else { return "" };
        } else if (type == "Urgency") {
            if (value != "") {
                if (("top urgent").indexOf(value.toLowerCase()) > -1) {
                    return true;
                } else if (("normal").indexOf(value.toLowerCase()) > -1) {
                    return false;
                } else {
                    return "-";
                }
            } else { return "" };
        } else {
            // unknown parameter
            return false;
        }
    }
    // rename status fx transaction & top urgent
    self.CallbackStatus = function (type, value) {
        if (type == "FX Transaction") {
            if (value == "Yes") {
                return true;
            } else {
                return false;
            }
        } else if (type == "Urgency") {
            if (value == "Top Urgent") {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    };

    // Ruddy 2014.11.09 : Generate Transaction Status
    // Update Rizki 2015-03-06
    self.FormatTransactionStatus = function (workflowStateDesc, activityTitle, applicationID, outcomeDescription, customOutcomeDescription) {
        var output;

        if (outcomeDescription == null) {
            outcomeDescription = '';
        }

        if (customOutcomeDescription == null) {
            customOutcomeDescription = '';
        }

        if (workflowStateDesc.toLowerCase() == 'running') {
            output = activityTitle;
        }
        else if ((workflowStateDesc.toLowerCase() == 'complete') && viewModel.GridProperties().SortColumn() !== 'ActivityTitle') {
            //Untuk handle FX Checker reject dan PPU Checker Approve Cancellation
            if ((applicationID.toLowerCase().startsWith('fx') && outcomeDescription.toLowerCase() == 'rejected') || (customOutcomeDescription.toLowerCase() == 'approve cancellation')) {
                output = 'Cancelled'
            }
            else {
                output = 'Completed';
            }
        }
        else {
            output = workflowStateDesc;
        }

        //if (outcome != 'Custom') {
        //output = String.format("{0} by {1}", outcome, user);
        //} else {
        //output = String.format("{0} by {1}", customOutcome, user);
        //}

        return output;
    };

    // Chandra 2015.02.24 : Get Underlying data for attach files
    function GetCustomerUnderlying() {
        var _cif = viewModel.TransactionAllNormal().Transaction.Customer.CIF;
        var options = {
            url: api.server + api.url.customerunderlying + "/Attachment",
            params: { cif: _cif },
            token: accessToken
        };
        // get filtered columns
        var filters = self.MakerUnderlyings();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataAttachFile1, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataAttachFile1, OnError, OnAlways);
        }
    }

    // On success GetData Underlying for AttachFile
    function OnSuccessGetDataAttachFile1(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            for (var i = 0 ; i < data.length; i++) {
                var selected = ko.utils.arrayFirst(self.TempSelectedAttachUnderlying(), function (item) {
                    return item.ID == data[i].ID;
                });
                if (selected != null) {
                    data[i].IsSelectedAttach = true;
                }
                else {
                    data[i].IsSelectedAttach = false;
                }
            }
            self.CustomerAttachUnderlyings(data);

        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }
    // chandra 2015.02.26 : upload attachment of ppu maker task
    self.cancel_a = function () {
        $("#modal-form-Attach").modal('hide');
        $('#backDrop').hide();
    }

    // Get filtered columns value
    function GetFilteredColumns() {
        // define filter
        var filters = [];

        if (self.FilterWorkflowName() != "") filters.push({ Field: 'WorkflowName', Value: self.FilterWorkflowName() });
        if (self.FilterInitiator() != "") filters.push({ Field: 'Initiator', Value: self.FilterInitiator() });
        if (self.FilterStateDescription() != "") filters.push({ Field: 'StateDescription', Value: self.FilterStateDescription() });
        if (self.FilterStartTime() != "") filters.push({ Field: 'StartTime', Value: self.FilterStartTime() });
        if (self.FilterTitle() != "") filters.push({ Field: 'Title', Value: self.FilterTitle() });
        if (self.FilterActivityTitle() != "") filters.push({ Field: 'ActivityTitle', Value: self.FilterActivityTitle() });
        if (self.FilterUser() != "") filters.push({ Field: 'LastModifiedBy', Value: self.FilterUser() });
        if (self.FilterUserApprover() != "") filters.push({ Field: 'UserApprover', Value: self.FilterUserApprover() });
        if (self.FilterEntryTime() != "") filters.push({ Field: 'EntryTime', Value: self.FilterEntryTime() });
        if (self.FilterExitTime() != "") filters.push({ Field: 'ExitTime', Value: self.FilterExitTime() });
        if (self.FilterOutcomeDescription() != "") filters.push({ Field: 'OutcomeDescription', Value: self.FilterOutcomeDescription() });
        if (self.FilterCustomOutcomeDescription() != "") filters.push({ Field: 'CustomOutcomeDescription', Value: self.FilterCustomOutcomeDescription() });
        if (self.FilterComments() != "") filters.push({ Field: 'Comments', Value: self.FilterComments() });


        if (self.FilterApplicationID() != "") filters.push({ Field: 'ApplicationID', Value: self.FilterApplicationID() });
        if (self.FilterCustomer() != "") filters.push({ Field: 'Customer', Value: self.FilterCustomer() });
        if (self.FilterProduct() != "") filters.push({ Field: 'Product', Value: self.FilterProduct() });
        if (self.FilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.FilterCurrency() });
        if (self.FilterAmount() != "") filters.push({ Field: 'Amount', Value: self.FilterAmount() });
        if (self.FilterAmountUSD() != "") filters.push({ Field: 'AmountUSD', Value: self.FilterAmountUSD() });
        if (self.FilterDebitAccNumber() != "") filters.push({ Field: 'DebitAccNumber', Value: self.FilterDebitAccNumber() });
        if (self.FilterFXTransaction() != "") filters.push({ Field: 'IsFXTransaction', Value: self.RenameStatusFilter('FX Transaction', self.FilterFXTransaction()) });
        if (self.FilterTopUrgent() != "") filters.push({ Field: 'IsTopUrgent', Value: self.RenameStatusFilter('Urgency', self.FilterTopUrgent()) });

        return filters;
    };

    // get all parameters need
    function GetParameters() {
        var options = {
            url: api.server + api.url.parameter,
            params: {
                select: "Product,Currency,Channel,BizSegment,Bank,BankCharge,AgentCharge,FXCompliance,ChargesType,ProductType,lld,UnderlyingDoc,POAFunction,DocType,PurposeDoc"
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetParameters, OnError, OnAlways);
    }

    // Get data / refresh data
    function GetData() {
        // widget reloader function start
        if ($box.css('position') == 'static') { $remove = true; $box.addClass('position-relative'); }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });

        // widget reloader function end

        // declare options variable for ajax get request
        var options = null;
        options = {
            url: api.server + api.url.task,
            params: {
                webid: config.sharepoint.webId,
                siteid: config.sharepoint.siteId,
                workflowids: config.sharepoint.workflowId,
                state: self.WorkflowConfig().State,
                outcome: self.WorkflowConfig().Outcome,
                //customOutcome:  "1,2,3,4,5,6,7,8",
                customOutcome: self.WorkflowConfig().CustomOutcome,
                showContribute: self.WorkflowConfig().ShowContribute,
                showActiveTask: self.WorkflowConfig().ShowActiveTask,
                page: self.GridProperties().Page(),
                size: self.GridProperties().Size(),
                sort_column: self.GridProperties().SortColumn(),
                sort_order: self.GridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetData, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetData, OnError, OnAlways);
        }
    }

    // get Transaction data
    function GetTransactionData(instanceId, approverId) {
        var endPointURL;

        switch (self.ActivityTitle()) {
            case "CBO Maker Task":
                endPointURL = api.server + api.url.workflow.transactionContact(instanceId, approverId);
                self.IsTimelines(true);
                break;
            case "CBO Checker Task":
                endPointURL = api.server + api.url.workflow.transactionContact(instanceId, approverId);
                self.IsTimelines(true);
                break;
                /*case "CBO Maker Reactivation Task":
                    endPointURL = api.server + api.url.workflow.transactionChecker(instanceId, approverId);
                    self.IsTimelines(true);
                    break;
                case "CBO Checker Reactivation Task":
                    endPointURL = api.server + api.url.workflow.transactionChecker(instanceId, approverId);
                    self.IsTimelines(true);
                    break; */
            case "FX Deal Checker Task":
                endPointURL = api.server + api.url.workflow.transactionDeal(instanceId, approverId);
                self.IsTimelines(true);
                break;
            case "Pending Documents Checker Task":
                endPointURL = api.server + api.url.workflow.transactionDocuments(instanceId, approverId);
                self.IsTimelines(false);
                break;
            default:
                endPointURL = api.server + api.url.workflow.transactionAllNormal(instanceId, approverId);
                self.IsTimelines(true);
        }

        // declare options variable for ajax get request
        var options = {
            url: endPointURL,
            token: accessToken
        };

        // show progress bar
        $("#transaction-progress").show();

        // hide transaction data
        $("#transaction-data").hide();

        // call ajax
        Helper.Ajax.Get(options, OnSuccessGetTransactionData, OnError, function () {
            // hide progress bar
            $("#transaction-progress").hide();

            // show transaction data
            $("#transaction-data").show();
        });
    }

    // Get Nintex task outcomes
    function GetTaskOutcomes(taskListID, taskListItemID) {
        var task = {
            TaskListID: taskListID,
            TaskListItemID: taskListItemID
        };

        var options = {
            url: "/_vti_bin/DBSNintex/Services.svc/GetTaskOutcomesByListID",
            data: ko.toJSON(task)
        };

        Helper.Sharepoint.Nintex.Post(options, OnSuccessTaskOutcomes, OnError, OnAlways);
    }

    // Event handlers declaration start
    function OnSuccessGetParameters(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            // bind result to observable array
            self.Products(data.Product);
            self.Channels(data.Channel);
            self.Currencies(data.Currency);
            self.DebitCurrencies(data.Currency);
            self.BizSegments(data.BizSegment);
            self.Banks(data.Bank);
            self.FXCompliances(data.FXCompliance);
            self.BankCharges(data.ChargesType);
            self.AgentCharges(data.ChargesType);
            self.ProductTypes(data.ProductType);
            self.LLDs(data.LLD);
            self.UnderlyingDocs(data.UnderltyingDoc);
            self.POAFunctions(data.POAFunction);
            self.DocumentTypes(data.DocType);
            self.DocumentPurposes(data.PurposeDoc);
            self.ddlDocumentPurpose_a(data.PurposeDoc);

            self.ddlDocumentType_a(data.DocType);
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }

    function OnSuccessGetData(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.Tasks(data.Rows); //Put the response in ObservableArray

            self.GridProperties().Page(data['Page']);
            self.GridProperties().Size(data['Size']);
            self.GridProperties().Total(data['Total']);
            self.GridProperties().TotalPages(Math.ceil(self.GridProperties().Total() / self.GridProperties().Size()));

            //$("#transaction-progress").hide();
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    function GetIsFlowValas(FlowValas) {
        return (FlowValas == null ? false : FlowValas.IsFlowValas);
    }

    self.LLDFlag = ko.observable();

    function OnSuccessGetTransactionData(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            var mapping = {
                //'ignore': ["LastModifiedDate", "LastModifiedBy"]
            };
            // Chandra (get Total IDR_FCY & utilization Amount)
            SetFXTotalTransaction(self.ActivityTitle(), data);
            switch (self.ActivityTitle()) {
                case "Pending Documents Checker Task":
                    if (data.Transaction.Bank.Code == '999') {
                        data.Transaction.Bank.Description = data.Transaction.OtherBeneBankName;
                        data.Transaction.Bank.SwiftCode = data.Transaction.OtherBeneBankSwift;
                    }
                    else {
                        $('#bank-code').prop('disabled', true);
                    }

                    self.TransactionAllNormal(ko.mapping.toJS(data, mapping));

                    break;
                case "CBO Maker Task":
                    self.TransactionContact(ko.mapping.toJS(data, mapping));
                    self.CIF_c(self.TransactionContact().Transaction.Customer.CIF);
                    break;
                case "CBO Checker Task":
                    self.TransactionContact(ko.mapping.toJS(data, mapping));
                    break;
                case "FX Deal Checker Task":

                    if (data.Transaction.IsOtherAccountNumber) {
                        data.Transaction.Account.AccountNumber = data.Transaction.OtherAccountNumber;
                    }

                    if (data.Transaction.DealUnderlying == null) {
                        self.DealUnderlying.ID = '-';
                        self.DealUnderlying.Description = '-';
                    }
                    else {
                        self.DealUnderlying.ID = data.Transaction.DealUnderlying.ID;
                        self.DealUnderlying.Description = data.Transaction.DealUnderlying.Description;
                    }

                    self.TransactionDeal(ko.mapping.toJS(data, mapping));
                    DealID = data.Transaction.ID;
                    break;
                default:
                    //TRANSACTION================================
                    if (data.Transaction.IsOtherAccountNumber) {
                        data.Transaction.Account.AccountNumber = data.Transaction.OtherAccountNumber;
                    }

                    if (data.Transaction.LLD != null) {
                        self.Selected().LLD(data.Transaction.LLD.ID);
                        self.LLDCode(data.Transaction.LLD.Description);
                    }
                    else {
                        self.Selected().LLD(null);
                    }

                    if (data.Transaction.Bank.Code == '999') {
                        data.Transaction.Bank.Description = data.Transaction.OtherBeneBankName;
                        data.Transaction.Bank.SwiftCode = data.Transaction.OtherBeneBankSwift;
                    }
                    else {
                        $('#bank-code').prop('disabled', true);
                    }

                    // PAYMENT===================================
                    if (data.Payment != null) {
                        if (data.Payment.LLD != null) {
                            self.Selected().LLD(data.Payment.LLD.ID);
                            self.LLDCode(data.Payment.LLD.Description);
                        }
                        else {
                            self.Selected().LLD(null);
                        }
                        if (data.Payment.Bank != null && data.Payment.Bank.Code == '999') {
                            data.Payment.Bank.Description = data.Payment.OtherBeneBankName;
                            data.Payment.Bank.SwiftCode = data.Payment.OtherBeneBankSwift;
                        }
                    }

                    self.TransactionAllNormal(ko.mapping.toJS(data, mapping));
            }

            // disable form if this task is not authorized
            /*if (self.IsPendingNintex()) {
             if (!self.IsAuthorizedNintex()) {
             DisableForm();
             }
             } else {
             DisableForm();
             }*/
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    function SetMappingAttachment(customerName) {
        CustomerUnderlyingFile.CustomerUnderlyingMappings([]);
        for (var i = 0 ; i < self.TempSelectedAttachUnderlying().length; i++) {
            var sValue = {
                ID: 0,
                UnderlyingID: self.TempSelectedAttachUnderlying()[i].ID,
                UnderlyingFileID: 0,
                AttachmentNo: customerName + '-(A)'
            }
            CustomerUnderlyingFile.CustomerUnderlyingMappings.push(sValue);
        }
    }

    function SetFXTotalTransaction(title, data) {
        switch (title) {
            case "PPU Maker Task":
            case "PPU Maker After Checker and Caller Task":
            case "PPU Checker Task":
            case "Pending Documents Checker Task":
            case "PPU Checker After PPU Caller Task":
            case "PPU Checker Cancellation Task":
            case "PPU Caller Task":
            case "CBO Maker Task":
            case "CBO Checker Task":
            case "Branch Maker Reactivation Task":
            case "CBO Maker Reactivation Task":
            case "CBO Checker Reactivation Task":
            case "Branch Checker Reactivation Task":
            case "Lazy Approval Task for Exceptional Handling Case":
            case "FX Deal Checker Task":
                cifData = data.Transaction.Customer.CIF;
                var t_IsFxTransaction = (data.Transaction.Currency.Code != 'IDR' && data.Transaction.DebitCurrency.Code == 'IDR');
                var t_IsFxTransactionToIDRB = (data.Transaction.Currency.Code == 'IDR' &&
                    data.Transaction.DebitCurrency.Code != 'IDR' && GetIsFlowValas(data.Transaction.ProductType) == true && data.Transaction.AmountUSD >= self.FCYIDRTreshold());

                self.t_IsFxTransaction(t_IsFxTransaction);
                self.t_IsFxTransactionToIDRB(t_IsFxTransactionToIDRB);
                break;
            case "Payment Maker Task":
            case "Payment Checker Task":
            case "Payment Maker Revise Task":
                cifData = data.Transaction.Customer.CIF;
                var t_IsFxTransaction = (data.Transaction.Currency.Code != 'IDR' && data.Transaction.DebitCurrency.Code == 'IDR');
                var t_IsFxTransactionToIDRB = (data.Transaction.Currency.Code == 'IDR' &&
                    data.Transaction.DebitCurrency.Code != 'IDR' && data.Transaction.ProductType.IsFlowValas == true && data.Transaction.AmountUSD >= self.FCYIDRTreshold());

                self.t_IsFxTransaction(t_IsFxTransaction);
                self.t_IsFxTransactionToIDRB(t_IsFxTransactionToIDRB);
                break;
            case "FX Deal Checker Task":

                // set value of transaction 'IS FX TRANSACTION'
                var t_IsFxTransaction = (data.Transaction.Currency.Code != 'IDR' && data.Transaction.AccountCurrencyCode == 'IDR') ? true : false;
                var t_IsFxTransactionToIDR = (data.Transaction.Currency.Code == 'IDR' && data.Transaction.AccountCurrencyCode != 'IDR') ? true : false;
                var t_IsFxTransactionToIDRB = (data.Transaction.Currency.Code == 'IDR' && data.Transaction.AccountCurrencyCode != 'IDR' && data.Transaction.ProductType.IsFlowValas == true) ? true : false;
                self.t_IsFxTransaction(t_IsFxTransaction);
                self.t_IsFxTransactionToIDR(t_IsFxTransactionToIDR);
                self.t_IsFxTransactionToIDRB(t_IsFxTransactionToIDRB);
                break;
        }

        FXModel.cif = cifData;
        FXModel.token = accessToken;
        GetTotalDeal(FXModel, OnSuccessTotal, OnError);
        GetTotalPPU(FXModel, OnSuccessTotal, OnError);
    }

    function SetVerifiedData(data, isVerified) {
        for (i = 0; i <= data.length - 1; i++) {
            data[i].IsVerified = isVerified;
        }

        self.Callback = function () {
            data;
        };
        self.Callback();
    }

    function OnSuccessSaveApprovalData(outcomeId) {
        // completing task
        CompletingTask(self.SPTaskListID(), self.SPTaskListItemID(), self.TaskTypeID(), outcomeId, self.Comments());
    }

    function DisableForm() {
        $("#transaction-form").find(" input, select, textarea, button").prop("disabled", true);
    }

    function OnSuccessTaskOutcomes(data, textStatus, jqXHR) {
        if (data.IsSuccess) {
            self.Outcomes(data.Outcomes);

            self.IsPendingNintex(data.IsPending);
            self.IsAuthorizedNintex(data.IsAuthorized);
            self.MessageNintex(data.Message);

            // Get transaction Data
            GetTransactionData(self.WorkflowInstanceID(), self.ApproverId());

            // lock current task only if user had permission
            /*if (self.IsPendingNintex() == true && self.IsAuthorizedNintex() == true) {
             LockAndUnlockTaskHub("Lock", self.WorkflowInstanceID(), self.ApproverId(), self.SPTaskListItemID(), self.ActivityTitle());
             }*/
        } else {
            if (data.Outcomes == null) {
                self.MessageNintex(data.Message);
                GetTransactionData(self.WorkflowInstanceID(), self.ApproverId());
            }
            else {
                ShowNotification("An error Occured", jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        }
    }

    function OnSuccessCompletingTask(data, textStatus, jqXHR) {
        if (data.IsSuccess) {
            // send notification
            ShowNotification('Completing Task Success', jqXHR.responseJSON.Message, 'gritter-success', false);

            // close dialog task
            $("#modal-form").modal('hide');

            // reload tasks & show progress bar
            $("#transaction-progress").show();

            self.IsOtherAccountNumber(false);

            self.GetData();
        } else {
            ShowNotification('Completing Task Failed', jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    //bangkit
    function GetRateAmount(form, CurrencyID) {
        var options = {
            url: api.server + api.url.currency + "/CurrencyRate/" + CurrencyID,
            params: {
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, function OnSuccessGetRateAmount(data, textStatus, jqXHR) {
            if (jqXHR.status = 200) {
                // nothing todo
            } else {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
            }
        }, OnError, OnAlways);
    }

    self.idrrate = ko.observable();

    self.GetRateIDR = function () { GetRateIDR() };

    function GetRateIDR() {
        var options = {
            url: api.server + api.url.currency + "/CurrencyRate/" + "1",
            params: {
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetRateIDR, OnError, OnAlways);
    }


    function OnSuccessGetRateIDR(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            // bind result to observable array
            self.idrrate(data.RupiahRate);
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }

    //Apply bank auto complete

    //Apply auto complete

    self.UploadReactivationDoc = ko.observable();

    function UploadReactivationDoc(SaveApprovalData, instanceId, approverId, outcomeId) {
        // uploading documents. after upload completed, see SaveTransaction()
        var data = {
            ApplicationID: self.TransactionAllNormal().Transaction.ApplicationID,
            CIF: self.TransactionAllNormal().Transaction.Customer.CIF,
            Name: self.TransactionAllNormal().Transaction.Customer.Name
        };

        //bangkit
        if (self.TransactionAllNormal().Transaction.Documents.length > 0) {
            for (var i = 0; i < self.TransactionAllNormal().Transaction.Documents.length; i++) {
                UploadFile(i, data, self.TransactionAllNormal().Transaction.Documents[i], SaveApprovalData, instanceId, approverId, outcomeId);
            }
        } else {
            SaveApprovalData(instanceId, approverId, outcomeId);
        }
    }

    // save underlying file

    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }
    // Event handlers declaration end
};

// View Model
var viewModel = new ViewModel();

// jQuery Init
$(document).ready(function () {

    // get table parameter
    var workflow = {
        State: $("#workflow-task-table").attr("workflow-state"),
        Outcome: $("#workflow-task-table").attr("workflow-outcome"),
        CustomOutcome: $("#workflow-task-table").attr("workflow-custom-outcome"),
        ShowContribute: $("#workflow-task-table").attr("workflow-show-contribute"),
        ShowActiveTask: $("#workflow-task-table").attr("workflow-show-active-task")
    };

    // set workflow inside vm
    viewModel.WorkflowConfig(workflow);
    // $('#aspnetForm').after('<form id="frmUnderlying"></form>');
    // $("#modal-form-Attach").appendTo("#frmUnderlying");
    // $("#modal-form-Attach").appendTo("#frmUnderlying");
    // widget loader
    $box = $('#widget-box');
    var event;
    $box.trigger(event = $.Event('reload.ace.widget'))
    if (event.isDefaultPrevented()) return
    $box.blur();

    // block enter key from user to prevent submitted data.
    $(this).keypress(function (event) {
        var code = event.charCode || event.keyCode;
        if (code == 13) {
            ShowNotification("Page Information", "Enter key is disabled for this form.", "gritter-warning", false);

            return false;
        }
    });

    // Load all templates
    $('script[src][type="text/html"]').each(function () {
        //alert($(this).attr("src"))
        var src = $(this).attr("src");
        if (src != undefined) {
            $(this).load(src);
        }
    });

    // scrollables
    $('.slim-scroll').each(function () {
        var $this = $(this);
        $this.slimscroll({
            height: $this.data('height') || 100,
            railVisible: true,
            alwaysVisible: true,
            color: '#D15B47'
        });
    });

    // tooltip
    $('[data-rel=tooltip]').tooltip();

    // datepicker
    $('.date-picker').datepicker({ autoclose: true }).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });

    $('.time-picker').timepicker({
        defaultTime: "",
        minuteStep: 1,
        showSeconds: false,
        showMeridian: false
    }).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });
    // ace file upload
    $('#document-path-upload').ace_file_input({
        no_file: 'No File ...',
        btn_choose: 'Choose',
        btn_change: 'Change',
        droppable: false,
        //onchange:null,
        thumbnail: false, //| true | large
        //whitelist:'gif|png|jpg|jpeg'
        blacklist: 'exe|dll'
        //onchange:''
        //
    });
    // ace file upload
    $('#document-path-upload1').ace_file_input({
        no_file: 'No File ...',
        btn_choose: 'Choose',
        btn_change: 'Change',
        droppable: false,
        //onchange:null,
        thumbnail: false, //| true | large
        //whitelist:'gif|png|jpg|jpeg'
        blacklist: 'exe|dll'
        //onchange:''
        //
    });
    // ace file upload
    $('#document-path').ace_file_input({
        no_file: 'No File ...',
        btn_choose: 'Choose',
        btn_change: 'Change',
        droppable: false,
        //onchange:null,
        thumbnail: false, //| true | large
        //whitelist:'gif|png|jpg|jpeg'
        blacklist: 'exe|dll'
        //onchange:''
        //
    });

    // Knockout custom file handler
    ko.bindingHandlers.file = {
        init: function (element, valueAccessor) {
            $(element).change(function () {
                var file = this.files[0];

                if (ko.isObservable(valueAccessor()))
                    valueAccessor()(file);
            });
        }
    };

    // validation
    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,

        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }
    });

    // Knockout Datepicker custom binding handler
    ko.bindingHandlers.datepicker = {
        init: function (element) {
            $(element).datepicker({ autoclose: true }).next().on(ace.click_event, function () {
                $(this).prev().focus();
            });
        },
        update: function (element) {
            $(element).datepicker("refresh");
        }
    };

    // Knockout Timepicker custom binding handler
    ko.bindingHandlers.timepicker = {
        init: function (element) {
            $(element).timepicker({
                defaultTime: "",
                minuteStep: 1,
                showSeconds: false,
                showMeridian: false
            }).next().on(ace.click_event, function () {
                $(this).prev().focus();
            });
        },
        update: function (element) {
            $(element).timepicker('showWidget');
        }
    };

    // Knockout Bindings
    //bangkit
    //ko.applyBindings(viewModel);
    ko.applyBindings(viewModel, document.getElementById('home-transaction'));
    // -- test error login
    if ($.cookie(api.cookie.name) == undefined) {
        Helper.Token.Request(TokenOnSuccess, TokenOnError);
    } else {
        accessToken = $.cookie(api.cookie.name);
        StartTaskHub();
        GetCurrentUser(viewModel);
        FXModel.token = accessToken;
        GetParameterData(FXModel, OnSuccessGetTotal, OnError);
        viewModel.GetDefaultDate();
        viewModel.GetParameters();
        viewModel.GetData();
        viewModel.GetRateIDR();
    }

    // Modal form on close handler
    $("#modal-form").on('hidden.bs.modal', function () {
        //alert("close")

        LockAndUnlockTaskHub("unlock", viewModel.WorkflowInstanceID(), viewModel.ApproverId(), viewModel.SPTaskListItemID(), viewModel.ActivityTitle());
    });
});

function ResetTZNumber() {
    viewModel.TZModel().Message = '';
    $('#tz-message').text('');
}

function IsDocumentCompleteCheck(data) {
    //IsDocumentCompleteCheck

    if (viewModel.TransactionAllNormal().Verify != undefined) {

        var verify = ko.utils.arrayFilter(viewModel.TransactionAllNormal().Verify, function (item) {
            return item.Name == "Document Completeness";
        });

        if (verify != null) {
            var index = viewModel.TransactionAllNormal().Verify.indexOf(verify[0]);

            if (verify[0].IsVerified) {
                viewModel.TransactionAllNormal().Transaction.IsDocumentComplete = true;
            } else {
                viewModel.TransactionAllNormal().Transaction.IsDocumentComplete = false;
            }
        }
    }
}

// Function to display notification.
// class name : gritter-success, gritter-warning, gritter-error, gritter-info
function ShowNotification(title, text, className, isSticky) {
    $.gritter.add({
        title: title,
        text: text,
        class_name: className,
        sticky: isSticky
    });
}
// chandra
function UpdatePaymentModel(data) {
    switch (data.name) {
        case "bene-name":
            viewModel.PaymentMaker().Payment.BeneName = data.value;
            break;
        case "bene-acc-number":
            viewModel.PaymentMaker().Payment.BeneAccNumber = data.value;
            break;
        default:
            break;
    }
    var update = viewModel.PaymentMaker();
    viewModel.PaymentMaker(ko.mapping.toJS(update));
    //bind auto complete
}

// Get SPUser from cookies
function GetCurrentUser() {
    //if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
    if ($.cookie(api.cookie.spUser) != undefined) {
        spUser = $.cookie(api.cookie.spUser);
        viewModel.SPUser = spUser;
    }
}

// Token validation On Success function
function TokenOnSuccess(data, textStatus, jqXHR) {
    // store token on browser cookies
    $.cookie(api.cookie.name, data.AccessToken);
    accessToken = $.cookie(api.cookie.name);

    // call spuser function
    GetCurrentUser(viewModel);

    // call get data inside view model
    FXModel.token = accessToken;
    GetParameterData(FXModel, OnSuccessGetTotal, OnError);
    viewModel.GetDefaultDate();
    viewModel.GetParameters();
    viewModel.GetData();
    viewModel.GetRateIDR();
    viewModel.GetFCYIDRTreshold();
}

// Token validation On Error function
function TokenOnError(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}

//
function OnSuccessSignal() {
    //ShowNotification("Connection Success", "Connected to signal server", "gritter-success", false);
    /*    var hub = $.connection.task;
     $.connection.hub.url = config.signal.server+ "task";

     $.connection.hub.start()
     .done(function () {
     alert("Connected");
     })
     .fail(function() {
     alert("Connection failed!");
     });*/
}

function OnErrorSignal() {
    ShowNotification("Connection Failed", "Failed connected to signal server", "gritter-error", true);
}

function OnReceivedSignal(data) {
    ShowNotification(data.Sender, data.Message, "gritter-info", false);
}

// SignalR ----------------------
var taskHub = $.hubConnection(config.signal.server + "hub");
var taskProxy = taskHub.createHubProxy("TaskService");
var IsTaskHubConnected = false;

function StartTaskHub() {
    taskHub.start()
        .done(function () {
            IsTaskHubConnected = true;
        })
        .fail(function () {
            //alert('fail');
            ShowNotification("Hub Connection Failed", "Fail while try connecting to hub server", "gritter-error", true);
        }
    );
}

function LockAndUnlockTaskHub(lockType, instanceId, approverID, taskId, activityTitle) {
    if (IsTaskHubConnected) {
        var data = {
            WorkflowInstanceID: instanceId,
            ApproverID: approverID,
            TaskID: taskId,
            ActivityTitle: activityTitle,
            LoginName: spUser.LoginName,
            DisplayName: spUser.DisplayName
        };

        var LockType = lockType.toLowerCase() == "lock" ? "LockTask" : "UnlockTask";

        // try lock current task
        taskProxy.invoke(LockType, data)
            .fail(function (err) {
                ShowNotification("LockType Task Failed", err, "gritter-error", true);
            }
        );
    } else {
        ShowNotification("Task Manager", "There is no active connection to task server manager.", "gritter-error", true);
    }
}


taskProxy.on("Message", function (data) {
    ShowNotification(data.From, data.Message, "gritter-info", false);
});

taskProxy.on("NotifyInfo", function (data) {
    //alert(JSON.stringify(data))
    ShowNotification(data.From, data.Message, "gritter-info", false);
});

taskProxy.on("NotifyWarning", function (data) {
    //alert(JSON.stringify(data))
    ShowNotification(data.From, data.Message, "gritter-warning", false);
});

taskProxy.on("NotifyError", function (data) {
    //alert(JSON.stringify(data))
    ShowNotification(data.From, data.Message, "gritter-error", true);
});

function OnSuccessGetTotal(dataCall) {
    idrrate = dataCall.RateIDR;
    viewModel.FCYIDRTreshold(dataCall.FCYIDRTrashHold);
    viewModel.Treshold(dataCall.TreshHold);
}
function OnError(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}
function OnSuccessTotal() {
    viewModel.GetCalculateFX(0);
}