﻿using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Validation;
using System.Transactions;
using System.Data;
using System.Data.SqlClient;
using System.Data.Entity.Core.Objects;
using System.Text;


namespace DBS.WebAPI.Models
{
    #region property
    [ModelName("InterestMaintenance")]
    public class DataID
    {
        public long transactionID { get; set; }
        public string ApplicationID { get; set; }

        public string Title { get; set; }
    }

    public class TransactionData
    {
        public IList<InterestMaintenanceModel> TransactionDataIM { get; set; }
        public IList<RolloverSettlementModel> TransactionDataRO { get; set; }
        public IList<TransactionDocumentModel> Documents { get; set; }
        public IList<TransactionDocumentModel> DocumentsAddHoc { get; set; }


    }
    public class InterestMaintenanceModel
    {
        public long CSOInterestMaintenanceID { get; set; }
        public long InterestMaintenanceID { get; set; }
        public long? InstructionID { get; set; }
        public long TransactionInterestMaintenanceID { get; set; }
        public string SolID { get; set; }
        public string SchemeCode { get; set; }
        public string CIF { get; set; }
        public string CustomerName { get; set; }
        public string LoanContractNo { get; set; }
        public DateTime DueDate { get; set; }
        public int CurrencyID { get; set; }
        public string CurrencyName { get; set; }
        public decimal PreviousInterestRate { get; set; }
        public decimal CurrentInterestRate { get; set; }
        public int BizSegmentID { get; set; }
        public string BizSegmentName { get; set; }
        public long EmployeeID { get; set; }
        public string RM { get; set; }
        public string CSO { get; set; }
        public string IsSBLCSecured { get; set; }
        public string IsHeavyEquipment { get; set; }
        public Nullable<DateTime> ValueDate { get; set; }
        public Nullable<DateTime> NextPrincipalDate { get; set; }
        public Nullable<DateTime> NextInterestDate { get; set; }
        public decimal ApprovedMarginAsPerCM { get; set; }
        public decimal BaseRate { get; set; }
        public decimal AccountPreferencial { get; set; }
        public decimal SpecialFTP { get; set; }
        public decimal SpecialRate { get; set; }
        public decimal AllinRate { get; set; }
        public string ApprovalDOA { get; set; }
        public string ApprovedBy { get; set; }
        public string Remarks { get; set; }
        public string OtherRemarks { get; set; }
        public Nullable<int> IsAdhoc { get; set; }
        public string CSOName { get; set; }
        public Nullable<DateTime> CreateDate { get; set; }
        public string CreateBy { get; set; }
        public Nullable<DateTime> UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        public bool Selected { get; set; }
        public Nullable<int> MaintenanceTypeID { get; set; }
        public Nullable<decimal> AmountRollover { get; set; }
        public Nullable<decimal> AllinFTPRate { get; set; }
        public string InterestRateCodeID { get; set; }
        public string InterestRateCodeName { get; set; }
        public DateTime? PeggingReviewDate { get; set; }
        //add aridya 20161205 add currency model
        public CurrencyModel Currency { get; set; }
        //end add
    }

    public class TransactionCSOTaskModel
    {

        public long ApproverID { get; set; }

        public Guid WorkflowInstanceID { get; set; }

        public long ID { get; set; }

        public string ApplicationID { get; set; }

        public string CustomerName { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public int ProductID { get; set; }

        public string ProductName { get; set; }

        public string TransactionStatus { get; set; }

        public string CIF { get; set; }

        public string Amount { get; set; }

        public TransactionModel Transaction { get; set; }

        public string CurrencyName { get; set; }

        public BizSegmentModel BizSegment { get; set; }
    }

    public class CSOAllTaskModel
    {
        public IList<TransactionCSOTaskModel> CSOAllTrans { set; get; }
    }

    public class RolloverSettlementModel
    {
        public long CSORolloverID { get; set; }
        public long RolloverID { get; set; }
        public long TransactionRolloverID { get; set; }
        public string CIF { get; set; }
        public string CustomerName { get; set; }
        public string LoanContractNo { get; set; }
        public Nullable<DateTime> DueDate { get; set; }
        public int CurrencyID { get; set; }
        public string CurrencyName { get; set; }
        public string SolID { get; set; }
        public decimal PreviousInterestRate { get; set; }
        public decimal CurrentInterestRate { get; set; }
        public int BizSegmentID { get; set; }
        public string BizSegmentName { get; set; }
        public long EmployeeID { get; set; }
        public string RM { get; set; }
        public string CSO { get; set; }
        public string IsSBLCSecured { get; set; }
        public string IsHeavyEquipment { get; set; }
        public Nullable<DateTime> ValueDate { get; set; }
        public Nullable<DateTime> NextPrincipalDate { get; set; }
        public Nullable<DateTime> NextInterestDate { get; set; }
        public decimal ApprovedMarginAsPerCM { get; set; }
        public int InterestRateCodeID { get; set; }
        public string InterestRateCodeName { get; set; }
        public decimal BaseRate { get; set; }
        public decimal AccountPreferencial { get; set; }
        public decimal SpecialFTP { get; set; }
        public decimal SpecialRateMargin { get; set; }
        public decimal AllinRate { get; set; }
        public string ApprovalDOA { get; set; }
        public string ApprovedBy { get; set; }
        public string Remarks { get; set; }
        public string OtherRemarks { get; set; }
        public string IsAdhoc { get; set; }
        public string CSOName { get; set; }
        public Nullable<DateTime> CreateDate { get; set; }
        public string CreateBy { get; set; }
        public Nullable<DateTime> UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        public bool Selected { get; set; }
        public string SchemeCode { get; set; }
        public decimal AmountDue { get; set; }
        public string LimitID { get; set; }
        public int MaintenanceTypeID { get; set; }
        public decimal AmountRollover { get; set; }
        public Nullable<decimal> AllInLP { get; set; }
        public Nullable<decimal> AllInFTPRate { get; set; }
        public long InterestMaintenanceID { get; set; }
        public long? InstructionID { get; set; }
        public string MaintenanceTypeName { get; set; }
        public string Attachment { get; set; }
        //add aridya 20161205 add currency model
        public CurrencyModel Currency { get; set; }
        //end add
    }

    public class ApprovalDOA
    {
        public int RowID { get; set; }
        public int MatrixID { get; set; }
        public long WorksheetID { get; set; }
        public string LoanContractNo { get; set; }
        public string MatrixName { get; set; }
        public string ApprovalDOAString { get; set; }
        public string MatrixCondition { get; set; }
    }

    public class BaseRate
    {
        public int RowID { get; set; }
        public long WorksheetID { get; set; }
        public string LoanContractNo { get; set; }

    }

    public class InterestRateModel
    {
        public int InterestRateCodeID { get; set; }
        public string InterestRateCodeName { get; set; }
    }

    public class MaintenanceTypeLoanModel
    {
        public int MaintenanceTypeID { get; set; }
        public string MaintenanceTypeName { get; set; }
    }
    #endregion

    #region interface
    public interface ICSOTask : IDisposable
    {
        #region Interest Maintenance
        bool GetInterestMaintenance(string logname, ref IList<InterestMaintenanceModel> InterestMaintenances, DateTime _date, ref string message);
        bool AddCSOInterestMaintenance(TransactionData InterestMaintenances, ref List<DataID> dataID, ref string message);
        #endregion

        #region Rollover
        bool GetRollover(string logname, ref IList<RolloverSettlementModel> InterestMaintenances, DateTime _date, ref string message);
        bool GetSettlement(string logname, ref IList<RolloverSettlementModel> InterestMaintenances, DateTime _date, ref string message);
        bool AddCSORolloverSettlement(TransactionData RolloverSettlements, ref List<DataID> dataID, ref string message);
        #endregion

        #region CSO Task And Detail
        bool GetTransactionCSOTask(ref IList<TransactionCSOTaskModel> TransactionCSO, string CIF, string ProductName, ref string message);
        bool GetLoanCSODetails(long instructionID, ref TransactionLoanModel transaction, ref string message);
        #endregion

        #region Parameter
        bool GetApprovalDOA(ref RolloverSettlementModel RolloverSettlements, ref InterestMaintenanceModel InterestMaintenances, ref ApprovalDOA ApprovalList, int productID, ref string message);
        bool GetBaseRate(ref IList<RolloverSettlementModel> RolloverSettlements, ref IList<InterestMaintenanceModel> InterestMaintenances, ref IList<BaseRate> BaseRateList, int productID, ref string message);
        bool GetInterestRates(ref IList<InterestRateModel> InterstRateList, ref string message);
        bool GetMaintenanceTypes(ref IList<MaintenanceTypeLoanModel> MaintenanceTypeList, ref string message);

        #endregion
        bool ReleaseWorksheetItem(string loanContract, ref string message);
    }
    #endregion
    public class CSOTask : ICSOTask
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        #region Parameter
        public bool GetApprovalDOA(ref RolloverSettlementModel RolloverSettlements, ref InterestMaintenanceModel InterestMaintenances, ref ApprovalDOA ApprovalList, int productID, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                InterestMaintenanceModel imCurr;
                RolloverSettlementModel roCurr;

                if (productID == (int)DBSProductID.LoanIMProductIDCons)
                {
                    roCurr = new RolloverSettlementModel();
                    imCurr = new InterestMaintenanceModel();
                    if (InterestMaintenances.IsSBLCSecured == "Yes")
                        InterestMaintenances.IsSBLCSecured = "1";
                    else
                        InterestMaintenances.IsSBLCSecured = "0";
                    if (InterestMaintenances.IsHeavyEquipment == "Yes")
                        InterestMaintenances.IsHeavyEquipment = "1";
                    else
                        InterestMaintenances.IsHeavyEquipment = "0";

                    InterestMaintenances.CreateDate = DateTime.Now;
                    InterestMaintenances.UpdateDate = DateTime.Now;

                    imCurr = InterestMaintenances;
                    SqlParameter param1 = new SqlParameter();
                    GenerateSQLParamIM(imCurr, param1);
                    SqlParameter param2 = new SqlParameter();
                    GenerateSQLParamRO(roCurr, param2);
                    ApprovalDOA doaMatrixIM = context.Database.SqlQuery<ApprovalDOA>(@"EXECUTE [dbo].[SP_GetMatrixApprovalDOA] @ProductID, @tvpIM, @tvpRO", new SqlParameter("ProductID", productID), param1, param2).FirstOrDefault();
                    if (doaMatrixIM != null)
                        ApprovalList = doaMatrixIM;
                }
                else
                {
                    roCurr = new RolloverSettlementModel();
                    imCurr = new InterestMaintenanceModel();
                    if (RolloverSettlements.IsSBLCSecured == "Yes")
                        RolloverSettlements.IsSBLCSecured = "1";
                    else
                        RolloverSettlements.IsSBLCSecured = "0";
                    if (RolloverSettlements.IsHeavyEquipment == "Yes")
                        RolloverSettlements.IsHeavyEquipment = "1";
                    else
                        RolloverSettlements.IsHeavyEquipment = "0";
                    if (RolloverSettlements.MaintenanceTypeName == "Rollover")
                        RolloverSettlements.MaintenanceTypeName = "1";
                    else
                        RolloverSettlements.MaintenanceTypeName = "0";
                    if (RolloverSettlements.IsAdhoc == "Yes")
                        RolloverSettlements.IsAdhoc = "1";
                    else
                        RolloverSettlements.IsAdhoc = "0";

                    imCurr.DueDate = DateTime.Now;
                    RolloverSettlements.CreateDate = DateTime.Now;

                    roCurr = RolloverSettlements;
                    SqlParameter param1 = new SqlParameter();
                    GenerateSQLParamIM(imCurr, param1);
                    SqlParameter param2 = new SqlParameter();
                    GenerateSQLParamRO(roCurr, param2);
                    ApprovalDOA doaMatrixRO = context.Database.SqlQuery<ApprovalDOA>(@"EXECUTE [dbo].[SP_GetMatrixApprovalDOA] @ProductID, @tvpIM, @tvpRO", new SqlParameter("ProductID", productID), param1, param2).FirstOrDefault();
                    if (doaMatrixRO != null)
                        ApprovalList = doaMatrixRO;
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }
            return IsSuccess;
        }
        public bool GetBaseRate(ref IList<RolloverSettlementModel> RolloverSettlements, ref IList<InterestMaintenanceModel> InterestMaintenances, ref IList<BaseRate> BaseRateList, int productID, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                int ListCount = 0;
                if (productID == (int)DBSProductID.LoanIMProductIDCons) ListCount = InterestMaintenances.Count;
                else ListCount = RolloverSettlements.Count;

                for (int i = 0; i < ListCount; i++)
                {
                    if (productID == (int)DBSProductID.LoanIMProductIDCons)
                    {

                        // aridya 20161205 change to get currency name
                        string CurrCode = "";
                        if (InterestMaintenances[i].CurrencyName == null)
                        {
                            CurrCode = InterestMaintenances[i].Currency.Code.Trim();
                        }
                        else
                        {
                            CurrCode = InterestMaintenances[i].CurrencyName;
                        }
                        string IntCode = InterestMaintenances[i].InterestRateCodeName;
                        SP_GetIQuoteRate_Result rate = context.SP_GetIQuoteRate(CurrCode, IntCode).FirstOrDefault();
                        if (rate != null && rate.BaseRate > 0)
                        {
                            InterestMaintenances[i].BaseRate = rate.BaseRate.Value;
                            InterestMaintenances[i].AllinFTPRate = rate.AllInFTPRate.Value;
                        }
                        else
                        {
                            InterestMaintenances[i].BaseRate = 0;
                        }
                    }
                    else
                    {
                        // aridya 20161205 change to get currency name
                        string CurrCode = "";
                        if (RolloverSettlements[i].CurrencyName == null)
                        {
                            CurrCode = RolloverSettlements[i].Currency.Code.Trim();
                        }
                        else
                        {
                            CurrCode = RolloverSettlements[i].CurrencyName;
                        }

                        string IntCode = RolloverSettlements[i].InterestRateCodeName;
                        SP_GetIQuoteRate_Result rate = context.SP_GetIQuoteRate(CurrCode, IntCode).FirstOrDefault();
                        if (rate != null && rate.BaseRate > 0)
                        {
                            RolloverSettlements[i].BaseRate = rate.BaseRate.Value;
                        }
                        else
                        {
                            RolloverSettlements[i].BaseRate = 0;
                        }
                    }
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }
            return IsSuccess;
        }
        public bool GetInterestRates(ref IList<InterestRateModel> InterestRateList, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                InterestRateList = (from a in context.ParameterSystems
                                    where a.ParameterType == "LOAN_INTEREST_RATE_CODE"
                                    select new InterestRateModel
                                    {
                                        InterestRateCodeID = a.ParsysID,
                                        InterestRateCodeName = a.ParameterValue
                                    }).ToList();
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }

            return IsSuccess;
        }
        public bool GetMaintenanceTypes(ref IList<MaintenanceTypeLoanModel> MaintenanceTypeList, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                MaintenanceTypeList = (from a in context.ParameterSystems
                                       where a.ParameterType == "LOAN_MAINTENANCE_TYPE"
                                       select new MaintenanceTypeLoanModel
                                       {
                                           MaintenanceTypeID = a.ParsysID,
                                           MaintenanceTypeName = a.ParameterValue
                                       }).ToList();
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }

            return IsSuccess;
        }
        private void GenerateSQLParamRO(RolloverSettlementModel roCurr, SqlParameter param2)
        {
            param2.SqlDbType = SqlDbType.Structured;
            param2.SqlValue = GenerateRODataTable(roCurr);
            param2.ParameterName = "@tvpRO";
            param2.TypeName = "dbo.tvp_Rollover";
        }
        private void GenerateSQLParamIM(InterestMaintenanceModel imCurr, SqlParameter param1)
        {
            param1.SqlDbType = SqlDbType.Structured;
            param1.SqlValue = GenerateIMDataTable(imCurr);
            param1.ParameterName = "@tvpIM";
            param1.TypeName = "dbo.tvp_CSOInterestMaintenance";
        }
        private DataTable GenerateIMDataTable(InterestMaintenanceModel roCurr)
        {
            List<InterestMaintenanceModel> imList = new List<InterestMaintenanceModel>();
            imList.Add(roCurr);
            DataTable IMDataTable = ToDataTable<InterestMaintenanceModel>(imList);
            IMDataTable.Columns.Remove("InterestMaintenanceID");
            IMDataTable.Columns.Remove("CustomerName");
            IMDataTable.Columns.Remove("CurrencyName");
            IMDataTable.Columns.Remove("BizSegmentName");
            IMDataTable.Columns.Remove("RM");
            IMDataTable.Columns.Remove("PeggingReviewDate");
            //IMDataTable.Columns.Remove("InterestRateCodeName");
            IMDataTable.Columns.Remove("Selected");
            IMDataTable.Columns.Remove("Currency");
            ReorderTable(ref IMDataTable, "CSOInterestMaintenanceID", "TransactionInterestMaintenanceID", "CIF", "LoanContractNo", "CurrencyID", "SolID", "SchemeCode", "DueDate", "PreviousInterestRate",
                            "CurrentInterestRate", "BizSegmentID", "EmployeeID", "CSO", "IsSBLCSecured", "IsHeavyEquipment", "MaintenanceTypeID", "AmountRollover", "NextPrincipalDate",
                            "NextInterestDate", "ApprovedMarginAsPerCM", "BaseRate", "AllinFTPRate", "AccountPreferencial", "SpecialFTP", "SpecialRate",
                            "AllinRate", "ApprovalDOA", "ApprovedBy", "Remarks", "OtherRemarks", "IsAdhoc", "CSOName", "CreateDate", "CreateBy", "UpdateDate", "UpdateBy", "ValueDate", "InstructionID", "InterestRateCodeID", "InterestRateCodeName");
            return IMDataTable;
        }
        private DataTable GenerateRODataTable(RolloverSettlementModel roCurr)
        {
            List<RolloverSettlementModel> roList = new List<RolloverSettlementModel>();
            roList.Add(roCurr);
            DataTable RODataTable = ToDataTable<RolloverSettlementModel>(roList);
            RODataTable.Columns.Remove("RolloverID");
            RODataTable.Columns.Remove("CustomerName");
            RODataTable.Columns.Remove("CurrencyName");
            RODataTable.Columns.Remove("PreviousInterestRate");
            RODataTable.Columns.Remove("CurrentInterestRate");
            RODataTable.Columns.Remove("BizSegmentName");
            RODataTable.Columns.Remove("RM");
            RODataTable.Columns.Remove("ValueDate");
            RODataTable.Columns.Remove("InterestRateCodeName");
            RODataTable.Columns.Remove("OtherRemarks");
            RODataTable.Columns.Remove("Selected");
            RODataTable.Columns.Remove("Attachment");
            RODataTable.Columns.Remove("Currency");
            //RODataTable.Columns.Remove("PeggingReviewDate");
            ReorderTable(ref RODataTable, "CSORolloverID", "TransactionRolloverID", "CIF", "LoanContractNo", "CurrencyID", "SOLID", "SchemeCode", "DueDate", "AmountDue", "LimitID", "BizSegmentID",
                            "EmployeeID", "CSO", "IsSBLCSecured", "IsHeavyEquipment", "MaintenanceTypeID", "AmountRollover", "NextPrincipalDate", "NextInterestDate", "ApprovedMarginAsPerCM",
                            "InterestRateCodeID", "BaseRate", "AllInLP", "AllInFTPRate", "AccountPreferencial", "SpecialFTP", "SpecialRateMargin", "AllInrate", "ApprovalDOA", "ApprovedBy",
                            "Remarks", "IsAdhoc", "CSOName", "CreateDate", "CreateBy", "Updatedate", "UpdateBy", "MaintenanceTypeName");
            return RODataTable;
        }

        public bool ReleaseWorksheetItem(string loanContract, ref string message)
        {
            bool IsSuccess = false;
            using (DBSEntities context_ts = new DBSEntities())
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    try
                    {
                        char[] delimiterChars = { ',', ';' };
                        string[] loanList = loanContract.Split(delimiterChars);

                        foreach (string no in loanList)
                        {
                            List<Rollover> roList = (from r in context_ts.Rollovers
                                                     where r.LoanContractNo == no
                                                     select r).ToList();
                            if (roList != null && roList.Count > 0)
                            {
                                foreach (Rollover ro in roList)
                                {
                                    ro.IsSelected = false;
                                    context_ts.SaveChanges();
                                }
                            }
                            List<InterestMaintenance> imList = (from r in context_ts.InterestMaintenances
                                                                where r.LoanContractNo == no
                                                                select r).ToList();
                            if (imList != null && imList.Count > 0)
                            {
                                foreach (InterestMaintenance im in imList)
                                {
                                    im.IsSelected = false;
                                    context_ts.SaveChanges();
                                }
                            }
                        }
                    }
                    catch (DbEntityValidationException e)
                    {
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            message += string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                eve.Entry.Entity.GetType().Name, eve.Entry.State);
                            foreach (var ve in eve.ValidationErrors)
                            {
                                message += string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                    ve.PropertyName, ve.ErrorMessage);
                            }
                        }
                        return IsSuccess;
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                        return IsSuccess;
                    }
                    ts.Complete();

                    IsSuccess = true;
                }
            }
            return IsSuccess;
        }

        #endregion

        #region Get And Add Data Excel

        #region Interest Maintenance
        public bool GetInterestMaintenance(string logname, ref IList<InterestMaintenanceModel> InterestMaintenances, DateTime _date, ref string message)
        {
            bool IsSuccess = false;
            _date = _date.Date;
            try
            {
                InterestMaintenances = (from a in context.SP_GetCSOTaskWorksheet("IM", _date, logname)
                                        select new InterestMaintenanceModel
                                        {
                                            TransactionInterestMaintenanceID = a.InterestMaintenanceID,
                                            InterestMaintenanceID = a.InterestMaintenanceID,
                                            SolID = a.SOLID,
                                            SchemeCode = a.SchemeCode,
                                            CIF = a.CIF,
                                            CustomerName = a.CustomerName,
                                            LoanContractNo = a.LoanContractNo,
                                            DueDate = a.DueDate.Value,
                                            CurrencyID = a.CurrencyID.Value,
                                            CurrencyName = a.CurrencyCode,
                                            PreviousInterestRate = a.PreviousInterestRate ?? 0,
                                            CurrentInterestRate = a.CurrentInterestRate ?? 0,
                                            BizSegmentID = a.BizSegmentID ?? 1,
                                            BizSegmentName = a.BizSegmentName,
                                            EmployeeID = a.EmployeeID ?? 1,
                                            RM = a.EmployeeName,
                                            CSO = a.CSO,
                                            IsSBLCSecured = a.IsSBLCSecured.HasValue ? (a.IsSBLCSecured.Value == false ? "No" : "Yes") : "No",
                                            IsHeavyEquipment = a.IsHeavyEquipment.HasValue ? (a.IsHeavyEquipment.Value == false ? "No" : "Yes") : "No",
                                            //valuedate
                                            ValueDate = a.ValueDate,
                                            NextInterestDate = a.NextInterestDate,
                                            NextPrincipalDate = a.NextPrincipalDate,
                                            ApprovedMarginAsPerCM = a.ApprovedMarginAsPerCM ?? 0,
                                            InterestRateCodeName = "",
                                            BaseRate = a.BaseRate ?? 0,
                                            AccountPreferencial = a.IM_AccountPreferencial ?? 0,
                                            //AllFTPRate
                                            SpecialFTP = a.IM_SpecialFTP ?? 0,
                                            SpecialRate = a.IM_SpecialRate ?? 0,
                                            AllinRate = a.IM_AllinRate ?? 0,
                                            ApprovalDOA = a.ApprovalDOA,
                                            ApprovedBy = a.ApprovedBy,
                                            Remarks = a.Remarks,
                                            PeggingReviewDate = a.PeggingReviewDate ?? null,
                                            Selected = false
                                        }).ToList();


                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }

            return IsSuccess;
        }
        public bool AddCSOInterestMaintenance(TransactionData InterestMaintenances, ref List<DataID> dataID, ref string message)
        {
            bool IsSuccess = false;
            long TrIDInserted = 0;

            using (DBSEntities context_ts = new DBSEntities())
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    try
                    {
                        var selectedData = InterestMaintenances.TransactionDataIM.Where(a => a.Selected.Equals(true)).ToList();

                        if (selectedData.Count > 0)
                        {
                            foreach (var tr in selectedData)
                            {
                                DBS.Entity.Transaction data = new DBS.Entity.Transaction()
                                {
                                    IsDocumentComplete = false,
                                    IsDraft = false,
                                    IsTopUrgent = 0,
                                    CIF = tr.CIF,
                                    ProductID = (int)DBSProductID.LoanIMProductIDCons,
                                    CurrencyID = tr.CurrencyID,
                                    Amount = 0,
                                    Rate = 0,
                                    AmountUSD = 0,
                                    ChannelID = 1,
                                    ApplicationDate = tr.DueDate,
                                    BizSegmentID = tr.BizSegmentID,
                                    DebitCurrencyID = 1,
                                    BeneName = "",
                                    BankID = 1,
                                    IsSignatureVerified = false,
                                    IsDormantAccount = false,
                                    IsFrezeAccount = false,
                                    IsNewCustomer = false,
                                    StateID = 1,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName,
                                    IsResident = false,
                                    IsCitizen = false,
                                    TouchTimeStartDate = DateTime.UtcNow,
                                    Mode = "SCHEDULED" //Rizki - 2017-07-02
                                };

                                context_ts.Transactions.Add(data);
                                context_ts.SaveChanges();

                                long transactionIDTemp = data.TransactionID;

                                DBS.Entity.TransactionInterestMaintenance TransIM = new DBS.Entity.TransactionInterestMaintenance()
                                {
                                    TransactionID = transactionIDTemp,
                                    InterestMaintenanceID = tr.InterestMaintenanceID,
                                    InstructionID = tr.InstructionID,
                                    CreateBy = currentUser.GetCurrentUser().LoginName,
                                    CreateDate = DateTime.Now
                                };
                                context_ts.TransactionInterestMaintenances.Add(TransIM);
                                context_ts.SaveChanges();

                                DBS.Entity.CSOInterestMaintenance CSOIM = new DBS.Entity.CSOInterestMaintenance()
                                {
                                    TransactionInterestMaintenanceID = TransIM.TransactionInterestMaintenanceID,
                                    SolID = tr.SolID,
                                    SchemeCode = tr.SchemeCode,
                                    CIF = tr.CIF,
                                    DueDate = tr.DueDate,
                                    LoanContractNo = tr.LoanContractNo,
                                    CurrencyID = tr.CurrencyID,
                                    PreviousInterestRate = tr.PreviousInterestRate,
                                    CurrentInterestRate = tr.CurrentInterestRate,
                                    BizSegmentID = tr.BizSegmentID,
                                    EmployeeID = tr.EmployeeID,
                                    CSO = tr.CSO,
                                    CSOName = tr.CSOName,
                                    IsSBLCSecured = tr.IsSBLCSecured == "Yes" ? true : false,
                                    IsHeavyEquipment = tr.IsHeavyEquipment == "Yes" ? true : false,
                                    ValueDate = tr.ValueDate,
                                    NextInterestDate = tr.NextInterestDate,
                                    NextPrincipalDate = tr.NextPrincipalDate,
                                    ApprovedMarginAsPerCM = tr.ApprovedMarginAsPerCM,
                                    InterestRateCodeID = tr.InterestRateCodeName != null ? context_ts.ParameterSystems.Where(k => k.ParameterValue.Equals(tr.InterestRateCodeName)).Select(d => d.ParsysID).FirstOrDefault() : 0,
                                    BaseRate = tr.BaseRate,
                                    AccountPreferencial = tr.AccountPreferencial,
                                    SpecialFTP = tr.SpecialFTP,
                                    SpecialRate = tr.SpecialRate,
                                    AllinRate = tr.AllinRate,
                                    ApprovalDOA = tr.ApprovalDOA,
                                    ApprovedBy = tr.ApprovedBy,
                                    Remarks = tr.Remarks,
                                    CreateBy = currentUser.GetCurrentUser().LoginName,
                                    CreateDate = DateTime.Now,
                                    //PeggingReviewDate = context_ts.InterestMaintenances.Where(x => x.LoanContractNo.Equals(tr.LoanContractNo) && (x.DueDate.Value.Year == tr.DueDate.Year && x.DueDate.Value.Month == tr.DueDate.Month && x.DueDate.Value.Day == tr.DueDate.Day)).Select(y => y.PeggingReviewDate).FirstOrDefault()

                                };
                                //add henggar condition when pegging date data.
                                if (tr.PeggingReviewDate.HasValue)
                                {
                                    CSOIM.PeggingReviewDate = tr.PeggingReviewDate;
                                }
                                else
                                {
                                    CSOIM.PeggingReviewDate = context_ts.InterestMaintenances.Where(x => x.LoanContractNo.Equals(tr.LoanContractNo) && (x.DueDate.Value.Year == tr.DueDate.Year && x.DueDate.Value.Month == tr.DueDate.Month && x.DueDate.Value.Day == tr.DueDate.Day)).Select(y => y.PeggingReviewDate).FirstOrDefault();
                                }

                                context_ts.CSOInterestMaintenances.Add(CSOIM);
                                context_ts.SaveChanges();

                                var updateIsSelected = context_ts.InterestMaintenances.Where(a => a.InterestMaintenanceID.Equals(TransIM.InterestMaintenanceID)).SingleOrDefault();
                                updateIsSelected.IsSelected = true;
                                context_ts.SaveChanges();

                                if (InterestMaintenances.Documents.Count > 0)
                                {
                                    foreach (var doc in InterestMaintenances.Documents)
                                    {
                                        DBS.Entity.TransactionDocument document = new Entity.TransactionDocument()
                                        {
                                            TransactionID = transactionIDTemp,
                                            DocTypeID = doc.Type.ID,
                                            PurposeID = 1,
                                            Filename = doc.FileName,
                                            DocumentPath = doc.DocumentPath,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().LoginName,
                                            IsDeleted = false
                                        };
                                        context_ts.TransactionDocuments.Add(document);
                                    }
                                    context_ts.SaveChanges();
                                }
                                var datalist = new DataID();
                                datalist.transactionID = transactionIDTemp;
                                TrIDInserted = transactionIDTemp;

                                using (DBSEntities context = new DBSEntities())
                                {
                                    var getTR = (from TR in context.Transactions
                                                 where TR.TransactionID == TrIDInserted
                                                 select TR).SingleOrDefault();
                                    datalist.ApplicationID = getTR.ApplicationID;
                                    datalist.Title = getTR.ApplicationID + "-" + getTR.CIF;
                                    IsSuccess = true;
                                }
                                dataID.Add(datalist);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ts.Dispose();
                        message = ex.Message + ex.InnerException.ToString();
                    }
                    ts.Complete();

                    IsSuccess = true;
                }
            }

            return IsSuccess;
        }
        #endregion

        #region Rollover
        public bool GetRollover(string logname, ref IList<RolloverSettlementModel> Rollovers, DateTime _date, ref string message)
        {
            bool IsSuccess = false;
            _date = _date.Date;
            try
            {
                Rollovers = (from a in context.SP_GetCSOTaskWorksheetRollover("RO", _date, logname)
                             select new RolloverSettlementModel
                             {
                                 TransactionRolloverID = a.RolloverID,
                                 SolID = a.SOLID,
                                 SchemeCode = a.SchemeCode,
                                 CIF = a.CIF,
                                 CustomerName = a.CustomerName,
                                 DueDate = a.DueDate.HasValue ? a.DueDate.Value : DateTime.Now,
                                 LoanContractNo = a.LoanContractNo,
                                 CurrencyID = a.CurrencyID.HasValue ? a.CurrencyID.Value : 1,
                                 CurrencyName = a.CurrencyCode,
                                 LimitID = a.LimitID,
                                 BizSegmentID = a.BizSegmentID.HasValue ? a.BizSegmentID.Value : 1,
                                 BizSegmentName = a.BizSegmentName,
                                 EmployeeID = a.EmployeeID.HasValue ? a.EmployeeID.Value : 1,
                                 RM = a.EmployeeName,
                                 CSO = a.CSO,
                                 IsSBLCSecured = a.IsSBLCSecured.Value == false ? "No" : "Yes",
                                 IsHeavyEquipment = a.IsHeavyEquipment.Value == false ? "No" : "Yes",
                                 //MaintenanceID
                                 MaintenanceTypeName = "",
                                 AmountRollover = a.AmountRollover.HasValue ? a.AmountRollover.Value : 0,
                                 NextInterestDate = a.NextInterestDate,
                                 NextPrincipalDate = a.NextPrincipalDate,
                                 ApprovedMarginAsPerCM = a.ApprovedMarginAsPerCM.HasValue ? a.ApprovedMarginAsPerCM.Value : 0,
                                 //InterestRateCodeID = a.InterestRateCodeID,
                                 InterestRateCodeName = "",
                                 BaseRate = a.BaseRate.HasValue ? a.BaseRate.Value : 0,
                                 AccountPreferencial = a.RO_AccountPreferencial.HasValue ? a.RO_AccountPreferencial.Value : 0,
                                 SpecialFTP = a.RO_SpecialFTP.HasValue ? a.RO_SpecialFTP.Value : 0,
                                 SpecialRateMargin = a.RO_SpecialRateMargin.HasValue ? a.RO_SpecialRateMargin.Value : 0,
                                 AllinRate = a.RO_AllInrate.HasValue ? a.RO_AllInrate.Value : 0,
                                 ApprovalDOA = a.ApprovalDOA,
                                 ApprovedBy = a.ApprovedBy,
                                 Remarks = a.Remarks,
                                 Selected = false,
                                 AmountDue = a.AmountDue.HasValue ? a.AmountDue.Value : 0,
                                 IsAdhoc = a.IsAdhoc.HasValue ? (a.IsAdhoc.Value == false ? "NO" : "Yes") : "No",
                                 ValueDate = a.ValueDate.HasValue ? a.ValueDate : null
                             }).ToList();
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }

            return IsSuccess;
        }
        public bool GetSettlement(string logname, ref IList<RolloverSettlementModel> Settlements, DateTime _date, ref string message)
        {
            bool IsSuccess = false;
            _date = _date.Date;
            try
            {
                Settlements = (from a in context.SP_GetCSOTaskWorksheet("SE", _date, logname)
                               select new RolloverSettlementModel
                               {
                                   TransactionRolloverID = a.RolloverID.Value,
                                   SolID = a.SOLID,
                                   CIF = a.CIF,
                                   CustomerName = a.CustomerName,
                                   DueDate = a.DueDate.Value,
                                   LoanContractNo = a.LoanContractNo,
                                   CurrencyID = a.CurrencyID.Value,
                                   CurrencyName = a.CurrencyCode,
                                   PreviousInterestRate = a.PreviousInterestRate.Value,
                                   CurrentInterestRate = a.CurrentInterestRate.Value,
                                   BizSegmentID = a.BizSegmentID.Value,
                                   BizSegmentName = a.BizSegmentName,
                                   EmployeeID = a.EmployeeID.Value,
                                   RM = a.EmployeeName,
                                   CSO = a.CSO,
                                   IsSBLCSecured = a.IsSBLCSecured.Value == false ? "No" : "Yes",
                                   IsHeavyEquipment = a.IsHeavyEquipment.Value == false ? "No" : "Yes",
                                   NextInterestDate = a.NextInterestDate.Value,
                                   NextPrincipalDate = a.NextInterestDate.Value,
                                   ApprovedMarginAsPerCM = a.ApprovedMarginAsPerCM.Value,
                                   //InterestRateCodeID = a.InterestRateCodeID,
                                   BaseRate = a.BaseRate.Value,
                                   AccountPreferencial = a.RO_AccountPreferencial.Value,
                                   SpecialFTP = a.RO_SpecialFTP.Value,
                                   SpecialRateMargin = a.RO_SpecialRateMargin.Value,
                                   AllinRate = a.RO_AllInrate.Value,
                                   ApprovalDOA = a.ApprovalDOA,
                                   ApprovedBy = a.ApprovedBy,
                                   Remarks = a.Remarks,
                                   Selected = false
                               }).ToList();
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }

            return IsSuccess;
        }
        public bool AddCSORolloverSettlement(TransactionData RolloverSettlements, ref List<DataID> dataID, ref string message)
        {
            bool IsSuccess = false;
            long TrIDInserted = 0;

            using (DBSEntities context_ts = new DBSEntities())
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    try
                    {
                        var selectedData = RolloverSettlements.TransactionDataRO.Where(a => a.Selected.Equals(true)).ToList();

                        if (selectedData.Count > 0)
                        {
                            foreach (var t in selectedData)
                            {
                                DBS.Entity.Transaction data = new DBS.Entity.Transaction()
                                {
                                    IsDocumentComplete = false,
                                    IsDraft = false,
                                    IsTopUrgent = 0,
                                    CIF = t.CIF,
                                    ProductID = t.MaintenanceTypeName == "Rollover" ? (int)DBSProductID.LoanRolloverProductIDCons : (int)DBSProductID.LoanSettlementProductIDCons,
                                    CurrencyID = t.CurrencyID,
                                    Amount = 0,
                                    Rate = 0,
                                    AmountUSD = 0,
                                    ChannelID = 1,
                                    ApplicationDate = DateTime.UtcNow,
                                    BizSegmentID = t.BizSegmentID,
                                    DebitCurrencyID = 1,
                                    BeneName = "",
                                    BankID = 1,
                                    IsSignatureVerified = false,
                                    IsDormantAccount = false,
                                    IsFrezeAccount = false,
                                    IsNewCustomer = false,
                                    StateID = 1,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName,
                                    IsResident = false,
                                    IsCitizen = false,
                                    TouchTimeStartDate = DateTime.UtcNow,
                                    Remarks = t.Remarks,
                                    Mode = "SCHEDULED" //Rizki - 2017-07-02
                                };

                                context_ts.Transactions.Add(data);
                                context_ts.SaveChanges();

                                long transactionIDTemp = data.TransactionID;

                                DBS.Entity.TransactionRollover TranROList = new Entity.TransactionRollover()
                                {
                                    TransactionID = transactionIDTemp,
                                    RolloverID = t.TransactionRolloverID,
                                    Instruction = t.InstructionID,
                                    CreateBy = currentUser.GetCurrentUser().LoginName,
                                    CreateDate = DateTime.Now
                                };
                                context_ts.TransactionRollovers.Add(TranROList);
                                context_ts.SaveChanges();

                                if (t.Attachment != null && t.Attachment != "")
                                {
                                    if (RolloverSettlements.DocumentsAddHoc.Count > 0)
                                    {
                                        foreach (var doc in RolloverSettlements.DocumentsAddHoc)
                                        {
                                            if (t.Attachment == doc.FileName)
                                            {
                                                DBS.Entity.TransactionDocumentLoan document = new Entity.TransactionDocumentLoan()
                                                {
                                                    TransactionID = TranROList.TransactionRolloverID,
                                                    DocTypeID = doc.Type.ID,
                                                    PurposeID = 1,
                                                    Filename = doc.FileName,
                                                    DocumentPath = doc.DocumentPath,
                                                    CreateDate = DateTime.UtcNow,
                                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                                };
                                                context_ts.TransactionDocumentLoans.Add(document);
                                            }
                                        }
                                        context_ts.SaveChanges();
                                    }
                                }

                                if (t.MaintenanceTypeName == "Rollover")
                                {
                                    DBS.Entity.CSORollover CSOROList = new Entity.CSORollover()
                                    {
                                        TransactionRolloverID = TranROList.TransactionRolloverID,
                                        SOLID = t.SolID,
                                        SchemeCode = t.SchemeCode,
                                        CIF = t.CIF,
                                        DueDate = t.DueDate,
                                        LoanContractNo = t.LoanContractNo,
                                        CurrencyID = t.CurrencyID,
                                        AmountDue = t.AmountDue,
                                        LimitID = t.LimitID,
                                        BizSegmentID = t.BizSegmentID,
                                        EmployeeID = t.EmployeeID,
                                        CSO = t.CSO,
                                        CSOName = t.CSOName,
                                        IsSBLCSecured = t.IsSBLCSecured == "Yes" ? true : false,
                                        IsHeavyEquipment = t.IsHeavyEquipment == "Yes" ? true : false,
                                        MaintenanceTypeID = t.MaintenanceTypeName != null ? context_ts.ParameterSystems.Where(j => j.ParameterValue.Equals(t.MaintenanceTypeName)).Select(j => j.ParsysID).FirstOrDefault() : 0,
                                        AmountRollover = t.AmountRollover,
                                        NextInterestDate = t.NextInterestDate,
                                        NextPrincipalDate = t.NextPrincipalDate,
                                        ApprovedMarginAsPerCM = t.ApprovedMarginAsPerCM,
                                        InterestRateCodeID = t.InterestRateCodeName != null ? context_ts.ParameterSystems.Where(k => k.ParameterValue.Equals(t.InterestRateCodeName)).Select(d => d.ParsysID).FirstOrDefault() : 0,
                                        BaseRate = t.BaseRate,
                                        AccountPreferencial = t.AccountPreferencial,
                                        SpecialFTP = t.SpecialFTP,
                                        SpecialRateMargin = t.SpecialRateMargin,
                                        AllInrate = t.AllinRate,
                                        ApprovalDOA = t.ApprovalDOA,
                                        ApprovedBy = t.ApprovedBy,
                                        Remarks = t.Remarks,
                                        CreateBy = currentUser.GetCurrentUser().LoginName,
                                        CreateDate = DateTime.Now,
                                        IsAdhoc = t.IsAdhoc == "Yes" ? true : false,
                                        ValueDate = t.ValueDate
                                    };
                                    context_ts.CSORollovers.Add(CSOROList);
                                }
                                else
                                {
                                    DBS.Entity.CSORollover CSOROList = new Entity.CSORollover()
                                    {
                                        TransactionRolloverID = TranROList.TransactionRolloverID,
                                        SOLID = t.SolID,
                                        SchemeCode = t.SchemeCode,
                                        CIF = t.CIF,
                                        DueDate = t.DueDate,
                                        LoanContractNo = t.LoanContractNo,
                                        CurrencyID = t.CurrencyID,
                                        AmountDue = t.AmountDue,
                                        LimitID = t.LimitID,
                                        BizSegmentID = t.BizSegmentID,
                                        EmployeeID = t.EmployeeID,
                                        CSO = t.CSO,
                                        CSOName = t.CSOName,
                                        IsSBLCSecured = t.IsSBLCSecured == "Yes" ? true : false,
                                        IsHeavyEquipment = t.IsHeavyEquipment == "Yes" ? true : false,
                                        MaintenanceTypeID = t.MaintenanceTypeName != null ? context_ts.ParameterSystems.Where(j => j.ParameterValue.Equals(t.MaintenanceTypeName)).Select(j => j.ParsysID).FirstOrDefault() : 0,
                                        AmountRollover = t.AmountRollover,
                                        //NextInterestDate = t.NextInterestDate,
                                        //NextPrincipalDate = t.NextPrincipalDate,
                                        //ApprovedMarginAsPerCM = t.ApprovedMarginAsPerCM,
                                        //InterestRateCodeID = t.InterestRateCodeName != null ? context_ts.ParameterSystems.Where(k => k.ParameterValue.Equals(t.InterestRateCodeName)).Select(d => d.ParsysID).FirstOrDefault() : 0,
                                        //BaseRate = t.BaseRate,
                                        //AccountPreferencial = t.AccountPreferencial,
                                        //SpecialFTP = t.SpecialFTP,
                                        //SpecialRateMargin = t.SpecialRateMargin,
                                        //AllInrate = t.AllinRate,
                                        //ApprovalDOA = t.ApprovalDOA,
                                        //ApprovedBy = t.ApprovedBy,
                                        Remarks = t.Remarks,
                                        CreateBy = currentUser.GetCurrentUser().LoginName,
                                        CreateDate = DateTime.Now,
                                        IsAdhoc = t.IsAdhoc == "Yes" ? true : false,
                                        ValueDate = t.ValueDate
                                    };
                                    context_ts.CSORollovers.Add(CSOROList);
                                }
                                context_ts.SaveChanges();

                                var updateIsSelected = context_ts.Rollovers.Where(a => a.RolloverID.Equals(t.TransactionRolloverID)).SingleOrDefault();
                                updateIsSelected.IsSelected = true;
                                context_ts.SaveChanges();

                                if (RolloverSettlements.Documents.Count > 0)
                                {
                                    foreach (var doc in RolloverSettlements.Documents)
                                    {
                                        DBS.Entity.TransactionDocument document = new Entity.TransactionDocument()
                                        {
                                            TransactionID = transactionIDTemp,
                                            DocTypeID = doc.Type.ID,
                                            PurposeID = 1,
                                            Filename = doc.FileName,
                                            DocumentPath = doc.DocumentPath,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().LoginName,
                                            IsDeleted = false
                                        };
                                        context_ts.TransactionDocuments.Add(document);
                                    }
                                    context_ts.SaveChanges();
                                }
                                context_ts.SaveChanges();

                                var datalist = new DataID();
                                datalist.transactionID = transactionIDTemp;
                                TrIDInserted = transactionIDTemp;

                                using (DBSEntities context = new DBSEntities())
                                {
                                    var getTR = (from TR in context.Transactions
                                                 where TR.TransactionID == TrIDInserted
                                                 select TR).SingleOrDefault();
                                    datalist.ApplicationID = getTR.ApplicationID;
                                    datalist.Title = getTR.ApplicationID + "-" + getTR.CIF;
                                    IsSuccess = true;
                                }
                                dataID.Add(datalist);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ts.Dispose();
                        message = ex.Message + ex.InnerException.ToString();
                    }
                    ts.Complete();

                    IsSuccess = true;
                }
            }
            return IsSuccess;
        }
        #endregion

        #endregion

        #region CSO Task And Detail
        public bool GetLoanCSODetails(long instructionID, ref TransactionLoanModel transaction, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                context.Transactions.AsNoTracking();


                TransactionLoanModel datatransaction = (from a in context.Transactions
                                                        where a.TransactionID.Equals(instructionID)
                                                        select new TransactionLoanModel
                                                        {
                                                            ID = a.TransactionID,
                                                            WorkflowInstanceID = a.WorkflowInstanceID.Value,
                                                            ApplicationID = a.ApplicationID,
                                                            ApproverID = a.ApproverID != null ? a.ApproverID : 0,

                                                            Product = new ProductModel()
                                                            {
                                                                ID = a.Product.ProductID,
                                                                Code = a.Product.ProductCode,
                                                                Name = a.Product.ProductName,
                                                                WorkflowID = a.Product.ProductWorkflow.WorkflowID,
                                                                Workflow = a.Product.ProductWorkflow.WorkflowName,
                                                                LastModifiedBy = a.Product.UpdateDate == null ? a.Product.CreateBy : a.Product.UpdateBy,
                                                                LastModifiedDate = a.Product.UpdateDate == null ? a.Product.CreateDate : a.Product.UpdateDate.Value
                                                            },
                                                            Currency = new CurrencyModel()
                                                            {
                                                                ID = a.Currency.CurrencyID,
                                                                Code = a.Currency.CurrencyCode,
                                                                Description = a.Currency.CurrencyDescription,
                                                                LastModifiedBy = a.Currency.UpdateDate == null ? a.Currency.CreateBy : a.Currency.UpdateBy,
                                                                LastModifiedDate = a.Currency.UpdateDate == null ? a.Currency.CreateDate : a.Currency.UpdateDate.Value
                                                            },
                                                            Amount = a.Amount,
                                                            AmountUSD = a.AmountUSD,
                                                            Rate = a.Rate,
                                                            Channel = new ChannelModel()
                                                            {
                                                                ID = a.Channel.ChannelID,
                                                                Name = a.Channel.ChannelName,
                                                                LastModifiedBy = a.Channel.UpdateDate == null ? a.Channel.CreateBy : a.Channel.UpdateBy,
                                                                LastModifiedDate = a.Channel.UpdateDate == null ? a.Channel.CreateDate : a.Channel.UpdateDate.Value
                                                            },

                                                            BizSegment = new BizSegmentModel()
                                                            {
                                                                ID = a.BizSegment.BizSegmentID,
                                                                Name = a.BizSegment.BizSegmentName,
                                                                Description = a.BizSegment.BizSegmentDescription,
                                                                LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                                                LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                                            },
                                                            LoanContractNo = a.LoanContractNo,
                                                            IsTopUrgent = a.IsTopUrgent,
                                                            Documents = a.TransactionDocuments.Where(x => x.IsDeleted.Equals(false)).Select(x => new TransactionDocumentModel()
                                                            {
                                                                ID = x.TransactionDocumentID,
                                                                Type = new DocumentTypeModel()
                                                                {
                                                                    ID = x.DocumentType.DocTypeID,
                                                                    Name = x.DocumentType.DocTypeName,
                                                                    Description = x.DocumentType.DocTypeDescription,
                                                                    LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                                                    LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                                                },
                                                                Purpose = new DocumentPurposeModel()
                                                                {
                                                                    ID = x.DocumentPurpose.PurposeID,
                                                                    Name = x.DocumentPurpose.PurposeName,
                                                                    Description = x.DocumentPurpose.PurposeDescription,
                                                                    LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                                                    LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                                                },
                                                                FileName = x.Filename,
                                                                DocumentPath = x.DocumentPath,
                                                                LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                                                LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value,
                                                                IsDormant = x.IsDormant == null ? false : x.IsDormant
                                                            }).ToList(),
                                                            CreateDate = a.CreateDate,
                                                            LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                            LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                                        }).SingleOrDefault();

                if (datatransaction != null)
                {
                    //datatransaction.IsOtherBeneBank = datatransaction.Bank.Code != "999" ? false : true;
                    string cif = context.Transactions.Where(a => a.TransactionID.Equals(instructionID)).Select(a => a.Customer.CIF).FirstOrDefault();
                    ICustomerRepository customerRepo = new CustomerRepository();
                    CustomerModel customer = new CustomerModel();
                    // changed, chandra : 2015.03.23
                    if (customerRepo.GetCustomerByTransaction(datatransaction.ID, ref customer, ref message))
                    {
                        datatransaction.Customer = customer;
                    }
                    var data = context.Transactions.Where(x => x.TransactionID.Equals(datatransaction.ID)).Select(x => x).FirstOrDefault();
                    customerRepo.Dispose();
                    customer = null;
                    long IDTrns = datatransaction.ID;
                    var checker = context.TransactionCheckerDatas.Where(x => x.TransactionID.Equals(IDTrns)).Select(x => x).OrderByDescending(b => b.ApproverID).FirstOrDefault();
                    if (checker != null)
                    {
                        datatransaction.IsSignatureVerified = false;
                        datatransaction.IsDocumentComplete = false;
                        datatransaction.IsCallbackRequired = false;
                        datatransaction.IsLOIAvailable = false;
                        datatransaction.IsDormantAccount = false;
                        datatransaction.IsFreezeAccount = false;
                        datatransaction.IsSyndication = false;
                    }
                }
                {
                    transaction = datatransaction;
                }
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }

            return IsSuccess;
        }
        public bool GetTransactionCSOTask(ref IList<TransactionCSOTaskModel> TransactionCSO, string CIF, string ProductName, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                TransactionCSO = (from a in context.SP_GetTaskUserLoan(currentUser.GetCurrentUser().LoginName, null, CIF, null, ProductName, null, null, null, null, null, null, null, null, null, null)
                                  select new TransactionCSOTaskModel
                                  {
                                      #region Workflow Context
                                      ApproverID = a.WorkflowApproverID,
                                      WorkflowInstanceID = a.WorkflowInstanceID,
                                      #endregion

                                      #region Transaction
                                      ID = a.TransactionID,
                                      ApplicationID = a.ApplicationID,
                                      LastModifiedBy = a.UpdateBy,
                                      LastModifiedDate = a.UpdateDate,
                                      TransactionStatus = a.TransactionStatus,
                                      #endregion

                                      #region Detail
                                      CIF = a.CIF,
                                      CustomerName = a.CustomerName,
                                      CurrencyName = a.CurrencyCode,
                                      ProductName = a.ProductName,
                                      Transaction = context.Transactions.Where(x => x.TransactionID.Equals(a.TransactionID)).Select(x => new TransactionModel()
                                      {
                                          ID = x.TransactionID,
                                          bizSegmentID = x.BizSegmentID,
                                          transactionAmount = x.Amount
                                      }).FirstOrDefault(),
                                      BizSegment = (from x in context.Transactions.Where(x => x.TransactionID.Equals(a.TransactionID))
                                                    join y in context.BizSegments on x.BizSegmentID equals y.BizSegmentID
                                                    select new BizSegmentModel 
                                                    { 
                                                        ID = y.BizSegmentID,
                                                        Name = y.BizSegmentName,
                                                        Description = y.BizSegmentDescription
                                                    }).FirstOrDefault()
                                      #endregion
                                  }).ToList();

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }
            return IsSuccess;
        }

        #endregion

        public DataTable ToDataTable<T>(IEnumerable<T> list) where T : new()
        {
            Type elementType = typeof(T);
            using (DataTable dt = new DataTable())
            {
                System.Reflection.PropertyInfo[] _props = elementType.GetProperties();
                IEnumerable<string> basePropName = from b in elementType.BaseType.GetProperties()
                                                   select b.Name;
                System.Reflection.PropertyInfo[] TProps = (from p in _props
                                                           where !basePropName.Contains(p.Name)
                                                           select p).ToArray();
                foreach (System.Reflection.PropertyInfo propInfo in TProps)
                {
                    Type _pi = propInfo.PropertyType;
                    Type ColType = Nullable.GetUnderlyingType(_pi) ?? _pi;

                    dt.Columns.Add(propInfo.Name, ColType);
                }
                foreach (T item in list)
                {
                    DataRow row = dt.NewRow();
                    foreach (System.Reflection.PropertyInfo propInfo in TProps)
                    {
                        if (propInfo.PropertyType == typeof(Nullable<DateTime>))
                        {
                            DateTime _date;
                            if (!DateTime.TryParse(propInfo.GetValue(item, null) == null ? "0" : propInfo.GetValue(item, null).ToString(), out _date))
                            {
                                row[propInfo.Name] = new DateTime(1900, 1, 1);
                            }
                        }
                        row[propInfo.Name] = propInfo.GetValue(item, null) ?? DBNull.Value;
                    }
                    dt.Rows.Add(row);
                }
                return dt;
            }
        }
        public void ReorderTable(ref DataTable table, params String[] columns)
        {
            for (int i = 0; i < columns.Length; i++)
            {
                table.Columns[columns[i]].SetOrdinal(i);
            }
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }

    public static class ObjectContextExt
    {
        public static List<T> ExecuteStoredProcedure<T>(this ObjectContext dbContext, string storedProcedureName, params SqlParameter[] parameters)
        {
            var spSignature = new StringBuilder();
            object[] spParameters;
            bool hasTableVariables = parameters.Any(p => p.SqlDbType == SqlDbType.Structured);

            spSignature.AppendFormat("EXECUTE {0}", storedProcedureName);
            var length = parameters.Count() - 1;

            if (hasTableVariables)
            {
                var tableValueParameters = new List<SqlParameter>();

                for (int i = 0; i < parameters.Count(); i++)
                {
                    switch (parameters[i].SqlDbType)
                    {
                        case SqlDbType.Structured:
                            spSignature.AppendFormat(" @{0}", parameters[i].ParameterName);
                            tableValueParameters.Add(parameters[i]);
                            break;
                        case SqlDbType.VarChar:
                        case SqlDbType.Char:
                        case SqlDbType.Text:
                        case SqlDbType.NVarChar:
                        case SqlDbType.NChar:
                        case SqlDbType.NText:
                        case SqlDbType.Xml:
                        case SqlDbType.UniqueIdentifier:
                        case SqlDbType.Time:
                        case SqlDbType.Date:
                        case SqlDbType.DateTime:
                        case SqlDbType.DateTime2:
                        case SqlDbType.DateTimeOffset:
                        case SqlDbType.SmallDateTime:
                            // TODO: some magic here to avoid SQL injections
                            spSignature.AppendFormat(" '{0}'", parameters[i].Value.ToString());
                            break;
                        default:
                            spSignature.AppendFormat(" {0}", parameters[i].Value.ToString());
                            break;
                    }

                    if (i != length) spSignature.Append(",");
                }
                spParameters = tableValueParameters.Cast<object>().ToArray();
            }
            else
            {
                for (int i = 0; i < parameters.Count(); i++)
                {
                    spSignature.AppendFormat(" @{0}", parameters[i].ParameterName);
                    if (i != length) spSignature.Append(",");
                }
                spParameters = parameters.Cast<object>().ToArray();
            }

            var query = dbContext.ExecuteStoreQuery<T>(spSignature.ToString(), spParameters);


            var list = query.ToList();
            return list;
        }
    }
}