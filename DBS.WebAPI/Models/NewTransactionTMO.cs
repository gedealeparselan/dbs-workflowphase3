﻿using DBS.Entity;
using DBS.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;

namespace DBS.WebAPI.Models
{
    #region Property
    public class TransactionDraftTMOModel
    {
        public long ID { get; set; }
        public string ApplicationID { get; set; }
        public int? IsTopUrgent { get; set; }
        public CustomerModel Customer { get; set; }
        public bool? IsNewCustomer { get; set; }
        public ProductModel Product { get; set; }
        public CurrencyModel Currency { get; set; }
        public decimal? Amount { get; set; }
        public ChannelModel Channel { get; set; }
        public CustomerAccountModel Account { get; set; }
        public DateTime? ApplicationDate { get; set; }
        public string CIF { get; set; }
        public string AccountNumber { get; set; }
        public BizSegmentModel BizSegment { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public string BeneName { get; set; }
        public string BeneAccNumber { get; set; }
        public string CreateBy { get; set; }
        public string DraftCIF { get; set; }
        public string DraftCustomerName { get; set; }
        public string DraftAccountNumber { get; set; }
        public int? DraftCurrencyID { get; set; }
        public string Remarks { get; set; }
        public string DealNumber { get; set; }
        public IList<TransactionDocumentDraftModel> Documents { get; set; }
        public int? ChannelID { get; set; }
    }
    #endregion
    #region Interface
    public interface INewTransactionTMORepository : IDisposable
    {
        bool AddTransactionTMO(TransactionDetailModel transaction, ref long transactionID, ref string ApplicationID, ref string message);
        bool GetCustomerByCIF(string cif, ref CustomerModel customer, ref string message);
        bool UpdateTransactionDraft(long id, TransactionDetailModel transaction, ref string message);
        bool GetTransactionDraftByID(long id, ref TransactionDraftTMOModel transaction, ref string message);
        bool DeleteTransactionDraftByID(int id, ref string Message);
    }
    #endregion

    #region Repository
    public class NewTransactionTMORepository : INewTransactionTMORepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();
        public bool AddTransactionTMO(TransactionDetailModel transaction, ref long transactionID, ref string ApplicationID, ref string message)
        {
            bool IsSuccess = false;
            long TrIDInserted = 0;
            using (DBSEntities context_ts = new DBSEntities())
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    try
                    {
                        if (transaction.IsDraft)
                        {
                            #region Saving to Draft
                            if (transaction.ID != null && transaction.ID > 0)
                            {
                                Entity.TransactionDraft data = context.TransactionDrafts.Where(a => a.TransactionID.Equals(transaction.ID)).SingleOrDefault();
                                if (data != null)
                                {
                                    data.ApplicationID = transaction.ApplicationID;
                                    data.Amount = transaction.Amount == null ? 0 : transaction.Amount;
                                    data.ChannelID = (transaction.Channel != null && transaction.Channel.ID != null && transaction.Channel.ID > 0) ? transaction.Channel.ID : 1;
                                    data.CurrencyID = (transaction.Currency != null && transaction.Currency.ID != null && transaction.Currency.ID > 0) ? transaction.Currency.ID : (int)CurrencyID.NullCurrency;
                                    data.Remarks = transaction.Remarks;
                                    data.ValueDate = transaction.ValueDate == null ? DateTime.Now : transaction.ValueDate;
                                    data.DealNumber = transaction.DealNumber;
                                    context.SaveChanges();
                                    #region Save Document
                                    var existingDocs = context.TransactionDocumentDrafts.Where(a => a.TransactionID.Equals(transaction.ID)).ToList();
                                    if (existingDocs != null)
                                    {
                                        if (existingDocs.Count() > 0)
                                        {
                                            foreach (var item in existingDocs)
                                            {
                                                var deleteDoc = context.TransactionDocumentDrafts.Where(a => a.TransactionDocumentID.Equals(item.TransactionDocumentID)).SingleOrDefault();
                                                deleteDoc.IsDeleted = true;
                                                context.SaveChanges();
                                            }
                                        }
                                    }
                                    if (transaction.Documents != null)
                                    {
                                        if (transaction.Documents.Count > 0)
                                        {
                                            foreach (var item in transaction.Documents)
                                            {
                                                Entity.TransactionDocumentDraft document = new Entity.TransactionDocumentDraft()
                                                {
                                                    TransactionID = data.TransactionID,
                                                    DocTypeID = item.Type.ID,
                                                    PurposeID = item.Purpose.ID,
                                                    Filename = item.FileName,
                                                    DocumentPath = item.DocumentPath,
                                                    CreateDate = DateTime.UtcNow,
                                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                                };
                                                context_ts.TransactionDocumentDrafts.Add(document);
                                            }
                                            context_ts.SaveChanges();
                                        }
                                    }
                                    #endregion
                                }
                                ts.Complete();
                                IsSuccess = true;
                            }
                            else
                            {
                                DBS.Entity.TransactionDraft data = new DBS.Entity.TransactionDraft()
                                {
                                    #region Non Used Field Not Null
                                    ApplicationDate = DateTime.Now,
                                    AmountUSD = 0.00M,
                                    Rate = 0.00M,
                                    DebitCurrencyID = 1,
                                    BizSegmentID = 1,
                                    BeneName = "-",
                                    BankID = 1,
                                    IsResident = false,
                                    IsCitizen = false,
                                    IsTopUrgent = 0,
                                    #endregion

                                    IsNewCustomer = transaction.IsNewCustomer,
                                    CIF = transaction.Customer.CIF,
                                    ProductID = transaction.Product.ID,
                                    IsDraft = transaction.IsDraft,
                                    ApplicationID = transaction.ApplicationID,
                                    StateID = (int)StateID.OnProgress,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName,
                                    Amount = transaction.Amount == null ? 0 : transaction.Amount,
                                    ChannelID = (transaction.Channel != null && transaction.Channel.ID != null && transaction.Channel.ID > 0) ? transaction.Channel.ID : 1,
                                    CurrencyID = (transaction.Currency != null && transaction.Currency.ID != null && transaction.Currency.ID > 0) ? transaction.Currency.ID : (int)CurrencyID.NullCurrency,
                                    Remarks = transaction.Remarks,
                                    ValueDate = transaction.ValueDate == null ? DateTime.Now : transaction.ValueDate,
                                    DealNumber = transaction.DealNumber,
                                };

                                context_ts.TransactionDrafts.Add(data);
                                context_ts.SaveChanges();

                                #region Save Document
                                if (transaction.Documents.Count > 0)
                                {
                                    foreach (var item in transaction.Documents)
                                    {
                                        Entity.TransactionDocumentDraft document = new Entity.TransactionDocumentDraft()
                                        {
                                            TransactionID = data.TransactionID,
                                            DocTypeID = item.Type.ID,
                                            PurposeID = item.Purpose.ID,
                                            Filename = item.FileName,
                                            DocumentPath = item.DocumentPath,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().LoginName
                                        };
                                        context_ts.TransactionDocumentDrafts.Add(document);
                                    }
                                    context_ts.SaveChanges();
                                }
                                #endregion
                                ts.Complete();
                                IsSuccess = true;
                            }
                            #endregion
                        }
                        else
                        {
                            #region New Customer
                            if (transaction.IsNewCustomer)
                            {
                                context_ts.Customers.Add(new Customer()
                                {
                                    CIF = transaction.Customer.CIF,
                                    CustomerName = transaction.Customer.Name,
                                    BizSegmentID = 1,
                                    LocationID = 1,
                                    CustomerTypeID = 1,
                                    SourceID = 1,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                });
                                context_ts.SaveChanges();

                                context_ts.CustomerAccounts.Add(new CustomerAccount()
                                {
                                    CIF = transaction.Customer.CIF,
                                    AccountNumber = transaction.Account.AccountNumber,
                                    CurrencyID = transaction.Account.Currency.ID,
                                    IsDeleted = false,
                                    CreateDate = DateTime.UtcNow,
                                    CreateBy = currentUser.GetCurrentUser().LoginName
                                });
                                context_ts.SaveChanges();
                            }
                            #endregion

                            #region Submit new Transaction
                            DBS.Entity.Transaction data = new DBS.Entity.Transaction()
                            {
                                #region Non Used Field Not Null
                                ApplicationDate = DateTime.Now,
                                AmountUSD = 0.00M,
                                Rate = 0.00M,
                                DebitCurrencyID = 1,
                                BizSegmentID = 1,
                                BeneName = "-",
                                BankID = 1,
                                IsResident = false,
                                IsCitizen = false,
                                IsTopUrgent = 0,
                                IsSignatureVerified = false,
                                IsDormantAccount = false,
                                IsFrezeAccount = false,
                                #endregion

                                IsNewCustomer = transaction.IsNewCustomer,
                                CIF = transaction.Customer.CIF,
                                ProductID = transaction.Product.ID,
                                IsDocumentComplete = false,
                                IsDraft = transaction.IsDraft,
                                ApplicationID = transaction.ApplicationID,
                                StateID = (int)StateID.OnProgress, // 1: On Progress, 2: Completed, 3: Canceled 
                                CreateDate = DateTime.UtcNow,
                                CreateBy = currentUser.GetCurrentUser().LoginName,
                                Remarks = transaction.Remarks,
                                ValueDate = transaction.ValueDate == null ? DateTime.UtcNow : transaction.ValueDate,
                                DealNumber = transaction.DealNumber,
                                Amount = transaction.Amount,
                                ChannelID = (transaction.Channel != null && transaction.Channel.ID != null && transaction.Channel.ID > 0) ? transaction.Channel.ID : 1,
                                CurrencyID = (transaction.Currency != null && transaction.Currency.ID != null && transaction.Currency.ID > 0) ? transaction.Currency.ID : (int)CurrencyID.NullCurrency,
                                TouchTimeStartDate = transaction.CreateDate
                            };
                            context_ts.Transactions.Add(data);
                            context_ts.SaveChanges();

                            transactionID = data.TransactionID;
                            TrIDInserted = transactionID;

                            #region Saving Document
                            if (transaction.Documents != null)
                            {
                                if (transaction.Documents.Count > 0)
                                {
                                    foreach (var item in transaction.Documents)
                                    {
                                        Entity.TransactionDocument document = new Entity.TransactionDocument()
                                        {
                                            TransactionID = data.TransactionID,
                                            DocTypeID = item.Type.ID,
                                            PurposeID = item.Purpose.ID,
                                            Filename = item.FileName,
                                            DocumentPath = item.DocumentPath,
                                            CreateDate = DateTime.UtcNow,
                                            CreateBy = currentUser.GetCurrentUser().LoginName
                                        };

                                        context_ts.TransactionDocuments.Add(document);
                                    }

                                    context_ts.SaveChanges();
                                }
                            }
                            #endregion

                            ts.Complete();
                            #endregion
                        }
                    }
                    catch (Exception ex)
                    {
                        ts.Dispose();
                        message = ex.Message;
                    }
                }
            }
            if (!transaction.IsDraft)
            {
                using (DBSEntities context = new DBSEntities())
                {
                    var getTR = (from TR in context.Transactions
                                 where TR.TransactionID == TrIDInserted
                                 select TR).SingleOrDefault();
                    ApplicationID = getTR.ApplicationID;
                    IsSuccess = true;
                }
            }
            return IsSuccess;
        }
        public bool GetCustomerByCIF(string cif, ref CustomerModel customer, ref string message)
        {
            bool isSuccess = false;
            try
            {
                customer = (from a in context.Customers
                            where a.IsDeleted.Equals(false)
                            && a.CIF.Equals(cif)
                            select new CustomerModel
                            {
                                CIF = a.CIF,
                                Name = a.CustomerName,
                                POAName = a.POAName,
                                CustomerTypeID = a.CustomerTypeID,

                                BizSegment = new BizSegmentModel()
                                {
                                    ID = a.BizSegment.BizSegmentID,
                                    Name = a.BizSegment.BizSegmentName,
                                    Description = a.BizSegment.BizSegmentDescription,
                                    LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                    LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                },

                                Type = a.CustomerType != null ? new CustomerTypeModel()
                                {
                                    ID = a.CustomerType.CustomerTypeID,
                                    Name = a.CustomerType.CustomerTypeName,
                                    Description = a.CustomerType.CustomerTypeDescription,
                                    LastModifiedBy = a.CustomerType.UpdateDate == null ? a.CustomerType.CreateBy : a.CustomerType.UpdateBy,
                                    LastModifiedDate = a.CustomerType.UpdateDate == null ? a.CustomerType.CreateDate : a.CustomerType.UpdateDate.Value
                                } : new CustomerTypeModel()
                                {
                                    ID = 0,
                                    Name = "",
                                    Description = "",
                                    LastModifiedBy = a.CustomerType.UpdateDate == null ? a.CustomerType.CreateBy : a.CustomerType.UpdateBy,
                                    LastModifiedDate = a.CustomerType.UpdateDate == null ? a.CustomerType.CreateDate : a.CustomerType.UpdateDate.Value
                                },
                                RM = a.Employee != null ? new RMModel()
                                {

                                    ID = a.Employee.EmployeeID,
                                    Name = a.Employee.EmployeeName,
                                    RankID = a.Employee.RankID,
                                    BranchID = a.Employee.LocationID,
                                    SegmentID = a.Employee.BizSegmentID,
                                    Email = a.Employee.EmployeeEmail,
                                    LastModifiedBy = a.Employee.UpdateDate == null ? a.Employee.CreateBy : a.Employee.UpdateBy,
                                    LastModifiedDate = a.Employee.UpdateDate == null ? a.Employee.CreateDate : a.Employee.UpdateDate.Value
                                } : new RMModel()
                                {
                                    ID = 0,
                                    Name = "",
                                    RankID = 0,
                                    BranchID = 0,
                                    SegmentID = 0,
                                    Email = "",
                                    LastModifiedBy = a.Employee.CreateBy,
                                    LastModifiedDate = a.Employee.CreateDate
                                },
                                Accounts = a.CustomerAccounts.Where(x => x.IsDeleted.Equals(false)).Select(x => new CustomerAccountModel()
                                {
                                    AccountNumber = x.AccountNumber,
                                    Currency = new CurrencyModel()
                                    {
                                        ID = x.Currency.CurrencyID,
                                        Code = x.Currency.CurrencyCode,
                                        Description = x.Currency.CurrencyDescription,
                                        LastModifiedBy = x.Currency.UpdateDate == null ? x.Currency.CreateBy : x.Currency.UpdateBy,
                                        LastModifiedDate = x.Currency.UpdateDate == null ? x.Currency.CreateDate : x.Currency.UpdateDate.Value
                                    },
                                    LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                    LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                                }).ToList(),
                                Functions = a.CustomerFunctions.Where(x => x.IsDeleted.Equals(false)).Select(x => new POAFunctionModel()
                                {
                                    ID = x.POAFunction.POAFunctionID,
                                    Name = x.POAFunction.POAFunctionName,
                                    Description = x.POAFunction.POAFunctionDescription,
                                    LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                    LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                                }).ToList(),
                                Underlyings = a.CustomerUnderlyings.Select(x => new CustomerUnderlyingModel()
                                {
                                    ID = x.UnderlyingID,
                                    StatementLetter = new StatementLetterModel()
                                    {
                                        ID = x.StatementLetter.StatementLetterID,
                                        Name = x.StatementLetter.StatementLetterName,
                                        LastModifiedBy = x.StatementLetter.UpdateDate == null ? x.StatementLetter.CreateBy : x.StatementLetter.UpdateBy,
                                        LastModifiedDate = x.StatementLetter.UpdateDate == null ? x.StatementLetter.CreateDate : x.StatementLetter.UpdateDate.Value
                                    },
                                    Amount = x.Amount,
                                    Rate = x.Rate,
                                    AmountUSD = x.AmountUSD,
                                    AvailableAmount = x.AvailableAmountUSD,
                                    AttachmentNo = x.AttachmentNo,
                                    Currency = new CurrencyModel()
                                    {
                                        ID = x.Currency.CurrencyID,
                                        Code = x.Currency.CurrencyCode,
                                        Description = x.Currency.CurrencyDescription,
                                        LastModifiedBy = x.Currency.UpdateDate == null ? x.Currency.CreateBy : x.Currency.UpdateBy,
                                        LastModifiedDate = x.Currency.UpdateDate == null ? x.Currency.CreateDate : x.Currency.UpdateDate.Value
                                    },
                                    DateOfUnderlying = x.DateOfUnderlying,
                                    DocumentType = new DocumentTypeModel()
                                    {
                                        ID = x.DocumentType.DocTypeID,
                                        Name = x.DocumentType.DocTypeName,
                                        Description = x.DocumentType.DocTypeDescription,
                                        LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                        LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                    },
                                    EndDate = x.EndDate,
                                    ExpiredDate = x.ExpiredDate,
                                    InvoiceNumber = x.InvoiceNumber,
                                    ReferenceNumber = x.ReferenceNumber,
                                    StartDate = x.StartDate,
                                    SupplierName = x.SupplierName,
                                    UnderlyingDocument = new UnderlyingDocModel()
                                    {
                                        ID = x.UnderlyingDocument.UnderlyingDocID,
                                        Name = x.UnderlyingDocument.UnderlyingDocName,
                                        LastModifiedBy = x.UnderlyingDocument.UpdateDate == null ? x.UnderlyingDocument.CreateBy : x.UnderlyingDocument.UpdateBy,
                                        LastModifiedDate = x.UnderlyingDocument.UpdateDate == null ? x.UnderlyingDocument.CreateDate : x.UnderlyingDocument.UpdateDate.Value
                                    },
                                    IsDeclarationOfException = x.IsDeclarationOfExecption,
                                    IsUtilize = x.IsUtilize,
                                    IsEnable = false,
                                    LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                    LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                                }).ToList(),
                                LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                            }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool UpdateTransactionDraft(long id, TransactionDetailModel transaction, ref string message)
        {
            bool isSuccess = false;

            using (TransactionScope ts = new TransactionScope())
            {
                try
                {
                    Entity.TransactionDraft data = context.TransactionDrafts.Where(a => a.TransactionID.Equals(id)).SingleOrDefault();
                    if (data != null)
                    {
                        data.IsDraft = transaction.IsDraft;
                        data.TransactionID = transaction.ID;
                        data.ApplicationID = transaction.ApplicationID;
                        data.IsTopUrgent = transaction.IsTopUrgent;
                        data.Amount = transaction.Amount;
                        data.Rate = 0.00M;
                        data.AmountUSD = 0.00M;
                        data.BeneName = "-";
                        data.ApplicationDate = transaction.ApplicationDate;
                        data.IsNewCustomer = transaction.IsNewCustomer;
                        data.StateID = 1;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().LoginName;
                        data.IsResident = transaction.IsResident;
                        data.IsCitizen = transaction.IsResident;
                        data.BankID = 1;
                        data.Remarks = transaction.Remarks;
                        data.ValueDate = transaction.ValueDate;
                        data.DealNumber = transaction.DealNumber;
                        if (transaction.Channel != null && transaction.Channel.Name != null)
                        {
                            data.ChannelID = transaction.Channel.ID;
                        }
                        data.BizSegmentID = 1;
                        if (transaction.Product != null && transaction.Product.Code != null)
                        {
                            data.ProductID = transaction.Product.ID;
                        }

                        if (!transaction.IsNewCustomer)
                        {
                            data.CIF = transaction.Customer.CIF;

                            if (transaction.Currency != null && transaction.Currency.Code != null)
                            {
                                data.CurrencyID = transaction.Currency.ID;
                            }

                            data.DebitCurrencyID = 1;
                        }
                        else
                        {
                            data.DraftCIF = transaction.CustomerDraft.DraftCIF;
                            data.DraftAccountNumber = transaction.CustomerDraft.DraftAccountNumber.AccountNumber;
                            data.DraftCustomerName = transaction.CustomerDraft.DraftCustomerName;
                            data.DraftCurrencyID = transaction.CustomerDraft.DraftAccountNumber.Currency.ID;

                            if (transaction.Currency != null && transaction.Currency.Code != null)
                            {
                                data.CurrencyID = transaction.Currency.ID;
                            }
                        }
                    }
                    context.SaveChanges();
                    #region Save Document
                    var existingDocs = context.TransactionDocumentDrafts.Where(a => a.TransactionID.Equals(transaction.ID)).ToList();
                    if (existingDocs != null)
                    {
                        if (existingDocs.Count() > 0)
                        {
                            foreach (var item in existingDocs)
                            {
                                var deleteDoc = context.TransactionDocumentDrafts.Where(a => a.TransactionDocumentID.Equals(item.TransactionDocumentID)).SingleOrDefault();
                                deleteDoc.IsDeleted = true;
                                context.SaveChanges();
                            }
                        }
                    }
                    if (transaction.Documents.Count > 0)
                    {
                        foreach (var item in transaction.Documents)
                        {
                            Entity.TransactionDocumentDraft document = new Entity.TransactionDocumentDraft()
                            {
                                TransactionID = data.TransactionID,
                                DocTypeID = item.Type.ID,
                                PurposeID = item.Purpose.ID,
                                Filename = item.FileName,
                                DocumentPath = item.DocumentPath,
                                CreateDate = DateTime.UtcNow,
                                CreateBy = currentUser.GetCurrentUser().LoginName
                            };
                            context.TransactionDocumentDrafts.Add(document);
                        }
                        context.SaveChanges();
                    }
                    #endregion
                    ts.Complete();
                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    ts.Dispose();
                    message = ex.Message;
                }
            }

            return isSuccess;
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);

            GC.Collect();
            GC.SuppressFinalize(this);
        }
        public bool GetTransactionDraftByID(long id, ref TransactionDraftTMOModel transaction, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                context.Transactions.AsNoTracking();
                TransactionDraftTMOModel datatransaction = (from a in context.TransactionDrafts
                                                            where a.TransactionID.Equals(id)
                                                            select new TransactionDraftTMOModel
                                                            {
                                                                ID = a.TransactionID,
                                                                ApplicationID = a.ApplicationID,
                                                                ApplicationDate = a.ApplicationDate,
                                                                Amount = a.Amount,
                                                                BeneName = a.BeneName,
                                                                BeneAccNumber = a.BeneAccNumber,
                                                                IsTopUrgent = a.IsTopUrgent,
                                                                IsNewCustomer = a.IsNewCustomer,
                                                                CreateDate = a.CreateDate,
                                                                LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                                LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,
                                                                CIF = a.CIF,
                                                                Remarks = a.Remarks,
                                                                DealNumber = a.DealNumber,
                                                                ChannelID = a.ChannelID
                                                            }).SingleOrDefault();

                if (datatransaction != null)
                {
                    var data = context.TransactionDrafts.Where(x => x.TransactionID.Equals(datatransaction.ID)).Select(x => x).FirstOrDefault();
                    if (datatransaction.IsNewCustomer != null && datatransaction.IsNewCustomer != true)
                    {
                        string cif = context.TransactionDrafts.Where(a => a.TransactionID.Equals(id)).Select(a => a.CIF).FirstOrDefault();
                        if (!string.IsNullOrEmpty(cif))
                        {
                            ICustomerRepository customerRepo = new CustomerRepository();
                            CustomerModel customer = new CustomerModel();
                            if (customerRepo.GetCustomerByCIF(cif, ref customer, ref message))
                            {
                                datatransaction.Customer = customer;
                            }

                            customerRepo.Dispose();
                            customer = null;
                        }
                    }
                    else
                    {
                        datatransaction.Customer = new CustomerModel();

                        datatransaction.DraftCIF = data.DraftCIF;
                        datatransaction.DraftCustomerName = data.DraftCustomerName;
                        datatransaction.DraftAccountNumber = data.DraftAccountNumber;
                        if (data.DraftCurrencyID != null)
                        {
                            datatransaction.DraftCurrencyID = data.DraftCurrencyID;
                        }
                    }

                    if (data.Product != null)
                    {
                        ProductModel p = new ProductModel();

                        p.ID = data.Product.ProductID;
                        p.Code = data.Product.ProductCode;
                        p.Name = data.Product.ProductName;

                        datatransaction.Product = p;
                    }
                    //add change by adi
                    /*
                    if (data.Channel != null)
                    {
                        ChannelModel ch = new ChannelModel();
                            ch.ID = data.Channel.ChannelID;
                            ch.Name = data.Channel.ChannelName;
                            datatransaction.Channel = ch;
                    }
                     * */
                    //end

                    if (data.Currency != null)
                    {
                        CurrencyModel c = new CurrencyModel();

                        c.ID = data.Currency.CurrencyID;
                        c.Code = data.Currency.CurrencyCode;
                        c.Description = data.Currency.CurrencyDescription;

                        datatransaction.Currency = c;
                    }

                    var docDraft = (from doc in context.TransactionDocumentDrafts
                                    where doc.TransactionID == id && doc.IsDeleted.Equals(false)
                                    select new TransactionDocumentDraftModel
                                    {
                                        ID = doc.TransactionDocumentID,
                                        IsDormant = doc.IsDormant,
                                        LastModifiedBy = doc.UpdateBy == null ? doc.CreateBy : doc.UpdateBy,
                                        LastModifiedDate = doc.UpdateDate == null ? doc.CreateDate : doc.CreateDate,
                                        Purpose = new DocumentPurposeModel { ID = doc.DocumentPurpose.PurposeID, Name = doc.DocumentPurpose.PurposeName },
                                        Type = new DocumentTypeModel { ID = doc.DocumentType.DocTypeID, Name = doc.DocumentType.DocTypeName },
                                        FileName = doc.Filename,
                                        DocumentPath = new DocumentPathModel { name = doc.Filename, lastModified = string.Empty, lastModifiedDate = DateTime.Now, DocPath = doc.DocumentPath, type = Util.ExistingDoctype }
                                    }).ToList();
                    if (docDraft != null)
                    {
                        datatransaction.Documents = docDraft;
                    }
                    transaction = datatransaction;
                }
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }

            return IsSuccess;
        }

        public bool DeleteTransactionDraftByID(int id, ref string Message)
        {
            bool isDelete = false;
            using (DBSEntities context_ts = new DBSEntities())
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    try
                    {
                        Entity.TransactionDraft data = context.TransactionDrafts.Where(a => a.TransactionID.Equals(id)).SingleOrDefault();
                        if (data != null)
                        {
                            var existingDocs = context.TransactionDocumentDrafts.Where(a => a.TransactionID.Equals(id)).ToList();
                            if (existingDocs != null)
                            {
                                if (existingDocs.Count() > 0)
                                {
                                    foreach (var item in existingDocs)
                                    {
                                        var deleteDoc = context.TransactionDocumentDrafts.Where(a => a.TransactionDocumentID.Equals(item.TransactionDocumentID)).SingleOrDefault();
                                        context.TransactionDocumentDrafts.Remove(deleteDoc);
                                        context.SaveChanges();
                                    }
                                }
                            }
                            context.TransactionDrafts.Remove(data);
                            context.SaveChanges();
                            isDelete = true;
                        }
                        else
                        {
                            Message = string.Format("Draft ID {0} is does not exist.", id);
                            isDelete = false;
                        }
                        ts.Complete();
                    }
                    catch (Exception ex)
                    {
                        ts.Dispose();
                        Message = ex.Message;
                    }
                }
            }
            return isDelete;
        }
    }
    #endregion
}