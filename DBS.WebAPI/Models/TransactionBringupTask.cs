﻿using DBS.Entity;
using DBS.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Data.Common;
using System.Data.EntityClient;
using System.Data.SqlClient;
using System.Linq.Dynamic;
using System.Transactions;

namespace DBS.WebAPI.Models
{
    #region Property
    public class TransactionBringupTaskModel
    {
        /// <summary>
        /// Transaction ID.
        /// </summary>
        public long ID { get; set; }

        /// <summary>
        /// Application ID
        /// </summary>
        public string ApplicationID { get; set; }

        /// <summary>
        /// Top Urgent
        /// </summary>
        public int? IsTopUrgent { get; set; }

        /// <summary>
        /// Customer Data Details
        /// </summary>
        public CustomerModel Customer { get; set; }

        /// <summary>
        /// New Customer
        /// </summary>
        public bool? IsNewCustomer { get; set; }

        /// <summary>
        /// Product Details
        /// </summary>
        public ProductModel Product { get; set; }

        /// <summary>
        /// Product Type Details
        /// </summary>
        public ProductTypeModel ProductType { get; set; }

        /// <summary>
        /// LLD Details
        /// </summary>
        public LLDModel LLD { get; set; }


        /// <summary>
        /// Currency Details
        /// </summary>
        public CurrencyModel Currency { get; set; }

        /// <summary>
        /// Transaction Amount
        /// </summary>
        public decimal? Amount { get; set; }



        /// <summary>
        /// Channel Details
        /// </summary>
        public ChannelModel Channel { get; set; }

        /// <summary>
        /// Debit Acc Number
        /// </summary>
        public CustomerAccountModel Account { get; set; }

        /// <summary>
        /// Application Date
        /// </summary>
        public DateTime? ApplicationDate { get; set; }

        public string CIF { get; set; }
        public string AccountNumber { get; set; }

        /// <summary>
        /// Bene Segment Details
        /// </summary>
        public BizSegmentModel BizSegment { get; set; }

        /// <summary>
        /// Bank Details
        /// </summary>
        public BankModel Bank { get; set; }

        /// <summary>
        /// Bank Charges
        /// </summary>
        public ChargesTypeModel BankCharges { get; set; }

        /// <summary>
        /// Agent Charges
        /// </summary>
        public ChargesTypeModel AgentCharges { get; set; }

        /// <summary>
        /// Type
        /// </summary>
        public decimal? Type { get; set; }

        /// <summary>
        /// Is Citizen
        /// </summary>
        public bool? IsCitizen { get; set; }

        /// <summary>
        /// Is Resident
        /// </summary>
        public bool? IsResident { get; set; }

        /// <summary>
        /// Signature Verification
        /// </summary>
        public bool? IsSignatureVerified { get; set; }

        /// <summary>
        /// Dormant Account
        /// </summary>
        public bool? IsDormantAccount { get; set; }

        /// <summary>
        /// Freeze Account
        /// </summary>
        public bool? IsFreezeAccount { get; set; }

        /// <summary>
        /// Others
        /// </summary>
        public string Others { get; set; }


        /// <summary>
        /// Create Date
        /// </summary>
        public DateTime? CreateDate { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }


        /// <summary>
        /// Purpose detail of compliance
        /// </summary>
        public string DetailCompliance { get; set; }

        /// <summary>
        /// Bene Name
        /// </summary>
        public string BeneName { get; set; }

        /// <summary>
        /// Bene Name
        /// </summary>
        public string BeneAccNumber { get; set; }

        /// <summary>
        /// TZ Number
        /// </summary>
        public string TZNumber { get; set; }

        public string PaymentDetails { get; set; }

        /// <summary>
        /// Rate
        /// </summary>
        public decimal? Rate { get; set; }


        /// <summary>
        /// Transaction Create By
        /// </summary> //add reizvan
        public string CreateBy { get; set; }

        /// <summary>
        /// Transaction Is Other Bene Bank
        /// </summary> //add reizvan
        public bool IsOtherBeneBank { get; set; }

        /// <summary>
        /// Transaction Other Bene Bank Name
        /// </summary> //add reizvan
        public string OtherBeneBankName { get; set; }

        /// <summary>
        /// Transaction Other Bene Bank Swift
        /// </summary> //add reizvan
        public string OtherBeneBankSwift { get; set; }
        //public CustomerDraftModel CustomerDraft { get; set; }

        public string DraftCIF { get; set; }
        public string DraftCustomerName { get; set; }
        public string DraftAccountNumber { get; set; }
        public int? DraftCurrencyID { get; set; }
        public bool? IsOtherAccountNumber { get; set; }
        public string OtherAccountNumber { get; set; }
        public CurrencyModel DebitCurrency { get; set; }
        public bool? IsJointAccount { get; set; }
        public IList<TransactionDocumentDraftModel> Documents { get; set; }
    }
    public class TransactionBringupModel
    {
        /// <summary>
        /// Workflow Instance ID
        /// </summary>
        public Guid workflowInstanceID { get; set; }

        /// <summary>
        /// Transaction ID.
        /// </summary>
        public Int64 ID { get; set; }

        /// <summary>
        /// Application ID
        /// </summary>
        public string applicationID { get; set; }

        /// <summary>
        /// Top Urgent
        /// </summary>
        public int? topUrgent { get; set; }

        /// <summary>
        /// Top Urgent Desc
        /// </summary>
        public string topUrgentDesc { get; set; }

        /// <summary>
        /// Customer Name
        /// </summary>
        public string customerName { get; set; }

        /// <summary>
        /// New Customer
        /// </summary>
        public bool newCustomer { get; set; }

        /// <summary>
        /// CIF
        /// </summary>
        public string cif { get; set; }

        /// <summary>
        /// Product
        /// </summary>
        //public DBS.PaymentAPI.Models.Product product { get; set; }
        public int productID { get; set; }

        /// <summary>
        /// Product Name
        /// </summary>
        public string productName { get; set; }

        /// <summary>
        /// Currency
        /// </summary>
        //public DBS.PaymentAPI.Models.Currency currency { get; set; }
        public int currencyID { get; set; }

        /// <summary>
        /// Currency Description
        /// </summary>
        public string currencyDesc { get; set; }

        /// <summary>
        /// Currency Code
        /// </summary>
        public string currencyCode { get; set; }

        /// <summary>
        /// Transaction Amount
        /// </summary>
        public decimal transactionAmount { get; set; }

        /// <summary>
        /// Rate
        /// </summary>
        public decimal rate { get; set; }

        /// <summary>
        /// Eqv.USD
        /// </summary>
        public decimal eqvUsd { get; set; }

        /// <summary>
        /// Channel
        /// </summary>
        //public DBS.PaymentAPI.Models.Channel channel { get; set; }
        public int channelID { get; set; }

        /// <summary>
        /// Debit Acc Number
        /// </summary>
        public string debitAccNumber { get; set; }

        public string beneName { get; set; }

        /// <summary>
        /// Debit Acc Ccy
        /// </summary>
        public string debitAccCcy { get; set; }

        /// <summary>
        /// Application Date
        /// </summary>
        public DateTime? applicationDate { get; set; }

        /// <summary>
        /// Bene Segment
        /// </summary>
        //public DBS.PaymentAPI.Models.BeneSegment beneSegment { get; set; }
        public int bizSegmentID { get; set; }

        ///// <summary>
        ///// Lld Code
        ///// </summary>
        //public string lldCode { get; set; }

        ///// <summary>
        ///// Lld Info
        ///// </summary>
        //public string lldInfo { get; set; }

        /// <summary>
        /// LLD Details
        /// </summary>
        public int? LLDID { get; set; }

        /// <summary>
        /// Signature Verification
        /// </summary>
        public bool signatureVerification { get; set; }

        /// <summary>
        /// Dormant Account
        /// </summary>
        public bool dormantAccount { get; set; }

        /// <summary>
        /// Freeze Account
        /// </summary>
        public bool freezeAccount { get; set; }

        /// <summary>
        /// Others
        /// </summary>
        public string others { get; set; }

        /// <summary>
        /// State ID
        /// </summary>
        public int stateID { get; set; }

        /// <summary>
        /// Is Draft
        /// </summary>
        public bool isDraft { get; set; }

        /// <summary>
        /// TZ Number
        /// </summary>
        public string tzNumber { get; set; }

        /// <summary>
        /// FX Transaction
        /// </summary>
        public string fxTransaction { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }

        /// <summary>
        /// List Transaction Document
        /// </summary>
        public IList<TransactionDocument> transactionDocument { get; set; }

        /// <summary>
        /// Signatured Verified
        /// </summary>
        public string signatureVerified { get; set; }

        /// <summary>
        /// Document Pending Type
        /// </summary>
        public string Document { get; set; }

        /// <summary>
        /// Transaction Status
        /// </summary>
        public string transactionStatus { get; set; }

        public bool? IsNewCustomer { get; set; }
        public string DraftCustomerName { get; set; }
        public bool? IsChangeRMTransaction { get; set; }
        public Int64? SubmitedTransactionID { get; set; }
        public Int64 approvalID { get; set; }
    }
    public class TransactionBringupTaskRow : TransactionBringupModel
    {
        public int RowID { get; set; }

    }
    public class TransactionBringupTaskGrid : Grid
    {
        public IList<TransactionBringupTaskRow> Rows { get; set; }
    }
    #endregion

    #region Filter
    public class TransactionBringupTaskFilter : Filter { }
    #endregion

    #region Interface
    public interface ITransactionBringupTaskRepository : IDisposable
    {
        bool GetTransactionBringupTaskById(long id, ref TransactionBringupTaskModel transaction, ref string message);
        bool GetTransactionBringupTask(int page, int size, IList<TransactionBringupTaskFilter> filters, string sortColumn, string sortOrder, ref TransactionBringupTaskGrid transaction, ref string message);
        bool GetTransactionBringupTask(ref IList<TransactionBringupTaskModel> transactions, int limit, int index, ref string message);
        bool GetTransactionBringupTask(ref IList<TransactionBringupTaskModel> transactions, ref string message);
        bool GetTransactionBringupTask(string key, int limit, ref IList<TransactionBringupTaskModel> transactions, ref string message);
    }
    #endregion

    #region Repository
    public class TransactionBringupTaskRepository : ITransactionBringupTaskRepository
    {
        private bool disposed = false;
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();
        public bool GetTransactionBringupTaskById(long id, ref TransactionBringupTaskModel transaction, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                context.Transactions.AsNoTracking();

                TransactionBringupTaskModel datatransaction = (from a in context.TransactionDrafts
                                                               join b in context.TransactionBringupTasks on a.TransactionID equals b.TransactionID
                                                               where a.TransactionID.Equals(id)
                                                               select new TransactionBringupTaskModel
                                                               {
                                                                   ID = a.TransactionID,
                                                                   ApplicationID = a.ApplicationID,
                                                                   ApplicationDate = a.ApplicationDate,
                                                                   Amount = a.Amount,
                                                                   BeneName = a.BeneName,
                                                                   BeneAccNumber = a.BeneAccNumber,
                                                                   IsTopUrgent = a.IsTopUrgent,

                                                                   IsNewCustomer = a.IsNewCustomer,
                                                                   DetailCompliance = a.DetailCompliance,
                                                                   TZNumber = a.TZNumber,

                                                                   CreateDate = a.CreateDate,
                                                                   LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                                                   LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,
                                                                   OtherBeneBankSwift = a.OtherBeneBankSwift,
                                                                   OtherBeneBankName = a.OtherBeneBankName,

                                                                   IsCitizen = a.IsCitizen,
                                                                   IsResident = a.IsResident,
                                                                   Type = a.Type,
                                                                   PaymentDetails = a.PaymentDetails,
                                                                   Rate = a.Rate,
                                                                   IsOtherAccountNumber = a.IsOtherAccountNumber,
                                                                   CIF = a.CIF,//dani dp
                                                                   IsJointAccount = a.IsJointAccount
                                                               }).SingleOrDefault();

                // fill payment data
                if (datatransaction != null)
                {
                    var data = context.TransactionDrafts.Where(x => x.TransactionID.Equals(datatransaction.ID)).Select(x => x).FirstOrDefault();

                    if (datatransaction.IsNewCustomer != null && datatransaction.IsNewCustomer != true)
                    {
                        string cif = context.TransactionDrafts.Where(a => a.TransactionID.Equals(id)).Select(a => a.CIF).FirstOrDefault();
                        if (!string.IsNullOrEmpty(cif))
                        {
                            ICustomerRepository customerRepo = new CustomerRepository();
                            CustomerModel customer = new CustomerModel();
                            if (customerRepo.GetCustomerByCIF(cif, ref customer, ref message))
                            {
                                datatransaction.Customer = customer;
                            }

                            customerRepo.Dispose();
                            customer = null;
                        }
                    }
                    else
                    {
                        datatransaction.Customer = new CustomerModel();

                        datatransaction.DraftCIF = data.DraftCIF;
                        datatransaction.DraftCustomerName = data.DraftCustomerName;
                        datatransaction.DraftAccountNumber = data.DraftAccountNumber;
                        if (data.DraftCurrencyID != null)
                        {
                            datatransaction.DraftCurrencyID = data.DraftCurrencyID;
                        }
                    }

                    //if (data.AccountNumber != null)
                    //{
                    //    CustomerAccountModel ctm = new CustomerAccountModel()
                    //    {
                    //        AccountNumber = data.CustomerAccount.AccountNumber,
                    //        Currency = new CurrencyModel()
                    //        {
                    //            ID = data.CustomerAccount.Currency.CurrencyID,
                    //            Code = data.CustomerAccount.Currency.CurrencyCode,
                    //            Description = data.CustomerAccount.Currency.CurrencyDescription,
                    //            LastModifiedBy = data.CustomerAccount.Currency.UpdateDate == null ? data.CustomerAccount.Currency.CreateBy : data.CustomerAccount.Currency.UpdateBy,
                    //            LastModifiedDate = data.CustomerAccount.Currency.UpdateDate == null ? data.CustomerAccount.Currency.CreateDate : data.CustomerAccount.Currency.UpdateDate.Value
                    //        },
                    //        LastModifiedBy = data.CustomerAccount.UpdateDate == null ? data.CustomerAccount.CreateBy : data.CustomerAccount.UpdateBy,
                    //        LastModifiedDate = data.CustomerAccount.UpdateDate == null ? data.CustomerAccount.CreateDate : data.CustomerAccount.UpdateDate.Value
                    //    };

                    //    datatransaction.Account = ctm;
                    //}

                    if (data.IsOtherAccountNumber != null && data.IsOtherAccountNumber == true)
                    {
                        if (data.OtherAccountNumber != null)
                        {
                            CustomerAccountModel Account = new CustomerAccountModel();
                            datatransaction.Account = Account;
                            datatransaction.OtherAccountNumber = data.OtherAccountNumber;
                        }
                    }
                    else
                    {
                        if (data.AccountNumber != null)
                        {
                            /*CustomerAccountModel Account = new CustomerAccountModel();
                            Account.AccountNumber = data.CustomerAccount.AccountNumber;
                            Account.LastModifiedBy = data.CustomerAccount.UpdateDate == null ? data.CustomerAccount.CreateBy : data.CustomerAccount.UpdateBy;
                            Account.LastModifiedDate = data.CustomerAccount.UpdateDate == null ? data.CustomerAccount.CreateDate : data.CustomerAccount.UpdateDate.Value;

                            datatransaction.Account = Account;*/

                            datatransaction.Account = (from j in context.SP_GETJointAccount(data.CIF)
                                                       where j.AccountNumber.Equals(data.AccountNumber)
                                                       select new CustomerAccountModel
                                                       {
                                                           AccountNumber = j.AccountNumber,
                                                           Currency = new CurrencyModel
                                                           {
                                                               ID = j.CurrencyID.Value,
                                                               Code = j.CurrencyCode,
                                                               Description = j.CurrencyDescription
                                                           },
                                                           CustomerName = j.CustomerName,
                                                           IsJointAccount = j.IsJointAccount
                                                       }).FirstOrDefault();
                            datatransaction.AccountNumber = data.AccountNumber;

                        }
                    }
                    if (data.DebitCurrencyID != null)
                    {
                        CurrencyModel AccountCurrency = new CurrencyModel();
                        AccountCurrency.ID = data.Currency1.CurrencyID;
                        AccountCurrency.Code = data.Currency1.CurrencyCode;
                        AccountCurrency.Description = data.Currency1.CurrencyDescription;

                        datatransaction.DebitCurrency = AccountCurrency;
                    }
                    if (data.ProductType != null)
                    {
                        ProductTypeModel ptm = new ProductTypeModel();

                        ptm.ID = data.ProductType.ProductTypeID;
                        ptm.Code = data.ProductType.ProductTypeCode;
                        ptm.Description = data.ProductType.ProductTypeDesc;
                        ptm.IsFlowValas = data.ProductType.IsFlowValas;

                        datatransaction.ProductType = ptm;
                    }

                    if (data.LLD != null)
                    {
                        LLDModel lm = new LLDModel();

                        lm.ID = data.LLD.LLDID;
                        lm.Code = data.LLD.LLDCode;
                        lm.Description = data.LLD.LLDDescription;

                        datatransaction.LLD = lm;
                    }

                    if (data.Product != null)
                    {
                        ProductModel p = new ProductModel();

                        p.ID = data.Product.ProductID;
                        p.Code = data.Product.ProductCode;
                        p.Name = data.Product.ProductName;

                        datatransaction.Product = p;
                    }

                    if (data.Currency != null)
                    {
                        CurrencyModel c = new CurrencyModel();

                        c.ID = data.Currency.CurrencyID;
                        c.Code = data.Currency.CurrencyCode;
                        c.Description = data.Currency.CurrencyDescription;

                        datatransaction.Currency = c;
                    }

                    if (data.Channel != null)
                    {
                        ChannelModel cn = new ChannelModel();

                        cn.ID = data.Channel.ChannelID;
                        cn.Name = data.Channel.ChannelName;

                        datatransaction.Channel = cn;
                    }

                    if (data.BizSegment != null)
                    {
                        BizSegmentModel bz = new BizSegmentModel();

                        bz.ID = data.BizSegment.BizSegmentID;
                        bz.Name = data.BizSegment.BizSegmentName;
                        bz.Description = data.BizSegment.BizSegmentDescription;

                        datatransaction.BizSegment = bz;
                    }

                    datatransaction.Bank = new BankModel();

                    if (data.Bank != null)
                    {
                        BankModel bn = new BankModel();

                        bn.ID = data.Bank.BankID;
                        bn.Code = data.Bank.BankCode;
                        bn.BankAccount = data.Bank.BankAccount;
                        bn.BranchCode = data.Bank.BranchCode;
                        bn.CommonName = data.Bank.CommonName;
                        bn.Currency = data.Bank.Currency;
                        bn.PGSL = data.Bank.Currency;
                        bn.SwiftCode = data.Bank.SwiftCode;
                        bn.Description = data.Bank.BankDescription;

                        datatransaction.Bank = bn;
                    }

                    //Add by Rizki 2015-12-31
                    if (data.BankChargesID != null)
                    {
                        IChargesTypeRepository chargesTypeRepo = new ChargesTypeRepository();
                        ChargesTypeModel chargesType = new ChargesTypeModel();
                        if (chargesTypeRepo.GetChargesTypeByID((int)data.BankChargesID, ref chargesType, ref message))
                        {
                            datatransaction.BankCharges = chargesType;
                        }

                        chargesTypeRepo.Dispose();
                        chargesType = null;
                    }

                    if (data.AgentChargesID != null)
                    {
                        IChargesTypeRepository chargesTypeRepo = new ChargesTypeRepository();
                        ChargesTypeModel chargesType = new ChargesTypeModel();
                        if (chargesTypeRepo.GetChargesTypeByID((int)data.AgentChargesID, ref chargesType, ref message))
                        {
                            datatransaction.AgentCharges = chargesType;
                        }

                        chargesTypeRepo.Dispose();
                        chargesType = null;
                    }

                    ///Andi,24 October 2015
                    var docDraft = (from doc in context.TransactionDocumentDrafts
                                    where doc.TransactionID == id && doc.IsDeleted.Equals(false)
                                    select new TransactionDocumentDraftModel
                                    {
                                        ID = doc.TransactionDocumentID,
                                        IsDormant = doc.IsDormant,
                                        LastModifiedBy = doc.UpdateBy == null ? doc.CreateBy : doc.UpdateBy,
                                        LastModifiedDate = doc.UpdateDate == null ? doc.CreateDate : doc.CreateDate,
                                        Purpose = new DocumentPurposeModel { ID = doc.DocumentPurpose.PurposeID, Name = doc.DocumentPurpose.PurposeName },
                                        Type = new DocumentTypeModel { ID = doc.DocumentType.DocTypeID, Name = doc.DocumentType.DocTypeName },
                                        FileName = doc.Filename,
                                        DocumentPath = new DocumentPathModel { name = doc.Filename, lastModified = string.Empty, lastModifiedDate = DateTime.Now, DocPath = doc.DocumentPath, type = Util.ExistingDoctype }
                                    }).ToList();
                    if (docDraft != null)
                    {
                        datatransaction.Documents = docDraft;
                    }
                    ///End Andi
                    transaction = datatransaction;
                }



                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }

            return IsSuccess;
        }

        public bool GetTransactionBringupTask(int page, int size, IList<TransactionBringupTaskFilter> filters, string sortColumn, string sortOrder, ref TransactionBringupTaskGrid transaction, ref string message)
        {
            bool isSuccess = false;
            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                TransactionBringupModel filter = new TransactionBringupModel();
                if (filters != null)
                {
                    filter.applicationID = (string)filters.Where(a => a.Field.Equals("applicationID")).Select(a => a.Value).SingleOrDefault();
                    filter.customerName = (string)filters.Where(a => a.Field.Equals("customerName")).Select(a => a.Value).SingleOrDefault();
                    filter.productName = (string)filters.Where(a => a.Field.Equals("productName")).Select(a => a.Value).SingleOrDefault();
                    filter.currencyCode = (string)filters.Where(a => a.Field.Equals("currencyCode")).Select(a => a.Value).SingleOrDefault();
                    filter.transactionAmount = Convert.ToDecimal((string)filters.Where(a => a.Field.Equals("transactionAmount")).Select(a => a.Value == null ? "0" : a.Value).SingleOrDefault());
                    filter.eqvUsd = Convert.ToDecimal((string)filters.Where(a => a.Field.Equals("eqvUsd")).Select(a => a.Value == null ? "0" : a.Value).SingleOrDefault());
                    filter.fxTransaction = (string)filters.Where(a => a.Field.Equals("fxTransaction")).Select(a => a.Value).SingleOrDefault();
                    filter.topUrgentDesc = (string)filters.Where(a => a.Field.Equals("topUrgentDesc")).Select(a => a.Value).SingleOrDefault();
                    filter.stateID = Convert.ToInt32((string)filters.Where(a => a.Field.Equals("stateID")).Select(a => a.Value == null ? "0" : a.Value).SingleOrDefault());
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
                    filter.debitAccNumber = (string)filters.Where(a => a.Field.Equals("debitAccNumber")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("ApplicationDate")).Any())
                    {
                        filter.applicationDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("ApplicationDate")).Select(a => a.Value).SingleOrDefault());
                    }
                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var cUser = currentUser.GetCurrentUser().LoginName;

                    var data = (from a in context.TransactionDrafts
                                join br in context.TransactionBringupTasks on a.TransactionID equals br.TransactionID into lbr
                                from br in lbr
                                join b in context.Customers on a.CIF equals b.CIF into lb
                                from b in lb.DefaultIfEmpty()
                                join c in context.Products on a.ProductID equals c.ProductID into lc
                                from c in lc.DefaultIfEmpty()
                                join d in context.Currencies on a.CurrencyID equals d.CurrencyID into ld
                                from d in ld.DefaultIfEmpty()
                                join e in context.Transactions on br.SubmitedTransactionID equals e.TransactionID into le//added by dani
                                from e in le.DefaultIfEmpty()//added by dani

                                let isApplicationID = !string.IsNullOrEmpty(filter.applicationID)
                                let isCustomerName = !string.IsNullOrEmpty(filter.customerName)
                                let isProduct = !string.IsNullOrEmpty(filter.productName)
                                let isCurrency = !string.IsNullOrEmpty(filter.currencyCode)
                                let isTrxAmount = filter.transactionAmount > 0 ? true : false
                                let isEqvUsd = filter.eqvUsd > 0 ? true : false
                                let isFxTransaction = !string.IsNullOrEmpty(filter.fxTransaction)
                                let isTopUrgent = filter.topUrgent == null ? false : true
                                let isTransactionStatus = filter.stateID > 0 ? true : false
                                let isApplicationDate = filter.applicationDate.HasValue ? true : false
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                let isDebitAccNumber = !string.IsNullOrEmpty(filter.debitAccNumber)

                                where
                                    (isApplicationID ? a.ApplicationID.Contains(filter.applicationID) : true) &&
                                    ((isCustomerName ? b.CustomerName.Contains(filter.customerName) || a.DraftCustomerName.Contains(filter.customerName) : true) ||
                                    (isCustomerName ? b.CIF.Contains(filter.customerName) || a.DraftCIF.Contains(filter.customerName) : true)) &&
                                    (isProduct ? c.ProductName.Contains(filter.productName) : true) &&
                                    (isCurrency ? d.CurrencyCode.Contains(filter.currencyCode) : true) &&
                                    (isTrxAmount ? a.Amount == filter.transactionAmount : true) &&
                                    (isEqvUsd ? a.AmountUSD == filter.eqvUsd : true) &&
                                    (isFxTransaction ? filter.fxTransaction.Contains("Yes") ? a.TZNumber != null : (filter.fxTransaction.Contains("No") ? a.TZNumber == null : false) : true) &&
                                    (isTopUrgent ? a.IsTopUrgent == filter.topUrgent : true) &&
                                    //(filter.fxTransaction.Contains("Top Urgent") ? a.IsTopUrgent == true : false) : true) &&
                                    (isTransactionStatus ? a.StateID == filter.stateID : true) &&
                                    (isDebitAccNumber ? a.IsNewCustomer == true ? a.DraftAccountNumber.Contains(filter.debitAccNumber) : a.AccountNumber.Contains(filter.debitAccNumber) : true) &&
                                    (isApplicationDate ? filter.applicationDate.Value == a.ApplicationDate.Value : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true) &&

                                    (br.CreateBy.Equals(cUser)) &&
                                    ((br.IsSubmitted == false) || (br.IsSubmitted == true && br.SubmitedTransactionID != null && br.DateOut > DateTime.Now))

                                select new TransactionBringupModel
                                {
                                    ID = a.TransactionID,
                                    applicationID = a.ApplicationID,
                                    customerName = b == null ? "" : b.CustomerName,
                                    IsNewCustomer = a.IsNewCustomer,
                                    DraftCustomerName = a.DraftCustomerName,
                                    productName = c == null ? "" : c.ProductName,
                                    currencyCode = a.CurrencyID.HasValue ? (a.CurrencyID.Value == 25 ? "" : (d == null ? "" : d.CurrencyCode)) : "",
                                    transactionAmount = a.Amount == null ? 0 : (decimal)a.Amount,
                                    eqvUsd = a.AmountUSD == null ? 0 : (decimal)a.AmountUSD,
                                    tzNumber = a.TZNumber,
                                    topUrgent = a.IsTopUrgent,
                                    stateID = a.StateID == null ? 0 : (int)a.StateID,
                                    applicationDate = a.ApplicationDate,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,
                                    debitAccNumber = a.IsNewCustomer == true ? (a.IsOtherAccountNumber.Value == true ? a.OtherAccountNumber : a.DraftAccountNumber) : (a.IsOtherAccountNumber.Value == true ? a.OtherAccountNumber : a.AccountNumber),
                                    fxTransaction = a.TZNumber != null ? "Yes" : "No",
                                    productID = c == null ? 0 : c.ProductID,
                                    IsChangeRMTransaction = a.IsChangeRMTransaction,
                                    SubmitedTransactionID = br.SubmitedTransactionID ?? 0,//added by dani
                                    workflowInstanceID = e.WorkflowInstanceID == null ? new Guid() : e.WorkflowInstanceID.Value,//added by dani
                                    //approvalID = e.ApproverID.Value,
                                    transactionStatus = e.Transaction_Status                     //added by dani               
                                });

                    transaction.Page = page;
                    transaction.Size = size;
                    transaction.Total = data.Count();
                    transaction.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    transaction.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new TransactionBringupTaskRow
                        {
                            ID = a.ID,
                            RowID = i + 1,
                            applicationID = a.applicationID,
                            customerName = (a.IsNewCustomer == true) ? a.DraftCustomerName : a.customerName,
                            productName = a.productName,
                            currencyCode = a.currencyCode,
                            transactionAmount = a.transactionAmount,
                            eqvUsd = a.eqvUsd,
                            tzNumber = a.tzNumber,
                            topUrgent = a.topUrgent,
                            stateID = a.stateID,
                            applicationDate = a.applicationDate,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate,
                            debitAccNumber = a.debitAccNumber,
                            fxTransaction = a.fxTransaction,
                            productID = a.productID,
                            IsChangeRMTransaction = a.IsChangeRMTransaction,
                            SubmitedTransactionID = a.SubmitedTransactionID ?? 0,//added by dani
                            workflowInstanceID = a.workflowInstanceID,//added by dani
                            //approvalID = a.approvalID,
                            transactionStatus = a.transactionStatus //added by dani
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool GetTransactionBringupTask(ref IList<TransactionBringupTaskModel> transactions, int limit, int index, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetTransactionBringupTask(ref IList<TransactionBringupTaskModel> transactions, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetTransactionBringupTask(string key, int limit, ref IList<TransactionBringupTaskModel> transactions, ref string message)
        {
            throw new NotImplementedException();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);

            GC.Collect();
            GC.SuppressFinalize(this);
        }
    }
    #endregion

}