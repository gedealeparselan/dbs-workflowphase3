﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Protocols.WSTrust;
using System.Linq;
using System.Web;
using DBS.Entity;

namespace DBS.WebAPI.Models
{
    public class SharepointUserModel
    {
        public int ID { get; set; }
        public string LoginName { get; set; }
        public string DisplayName { get; set; }
        public string Email { get; set; }
        public IList<UserRole> Roles { get; set; }
    }

    public class UserRole
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }

    public class APITokenModel
    {
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public string AccessToken { get;set; }
        public Lifetime LifeTime { get; set; }
        public SharepointUserModel User { get; set; }
    }

    #region Interface
    public interface ISharepointUserRepository : IDisposable
    {
        bool StoreClaims(SharepointUserModel spUser, ref string message);
    }
    #endregion

    #region Repository
    public class SharepointUserRepository : ISharepointUserRepository
    {
        private DBSEntities context = new DBSEntities();
        private bool disposed = false;

        public bool StoreClaims(SharepointUserModel spUser, ref string message)
        {
            bool isSuccess = false;

            try
            {
                // groups to string
                string groups = string.Empty;
                foreach (var item in spUser.Roles)
	            {
		            groups += item.Name + ", ";                    
	            }

                //groups += groups + ", TestGroupUpdate"; 

                var user = context.Users.Where(u => u.UserName.Equals(spUser.LoginName)).SingleOrDefault();

                // check user loginname is exist
                if (user == null)
                {
                    // insert new one
                    context.Users.Add(new User { 
                        UserName = spUser.LoginName,
                        UserDisplayName = spUser.DisplayName,
                        UserEmail = spUser.Email,
                        UserGroups = groups,
                        IsDeleted = false,
                        UserTypeID = 1,
                        CreateDate = DateTime.Now,
                        CreateBy = "JWT Auth."
                    });

                    context.SaveChanges();
                }
                else
                {
                    // update only
                    user.UpdateBy = "JWT Auth.";
                    user.UpdateDate = DateTime.Now;
                    user.UserDisplayName = spUser.DisplayName;
                    user.UserEmail = spUser.Email;
                    user.UserGroups = groups ;

                    context.SaveChanges();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}