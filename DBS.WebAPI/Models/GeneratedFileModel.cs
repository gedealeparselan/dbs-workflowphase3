﻿using DBS.Entity;
using DBS.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Linq.Dynamic;
using System.Xml.Linq;

namespace DBS.WebAPI.Models
{
    #region Property
    public class GeneratedFileModel
    {
        public long ItemID { get; set; }
        public string FileName { get; set; }
        public string URL { get; set; }
        public string ApplicationID { get; set; }
        public DateTime CreateDate { get; set; }
    }

    #region IPE
    public class GenerateFileXMLModel
    {
        public long ItemID { get; set; }
        public string FileName { get; set; }
        public string URL { get; set; }
        public string ApplicationID { get; set; }
        public DateTime CreateDate { get; set; }
    }

    public class MasterConfigModel
    {
        public int ConfigID { get; set; }
        public string Parameter { get; set; }
        public string Value { get; set; }
        public bool IsDelete { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateBy { get; set; }

    }

    public class GeneratedFileXMLRow : GenerateFileXMLModel
    {
        public int RowID { get; set; }
    }

    public class GeneratedFileXMLGrid : Grid
    {
        public IList<GeneratedFileXMLRow> Rows { get; set; }
    }
    #endregion

    public class GeneratedFileRow : GeneratedFileModel
    {
        public int RowID { get; set; }
    }

    public class GeneratedFileGrid : Grid
    {
        public IList<GeneratedFileRow> Rows { get; set; }
    }
    #endregion

    #region Filter
    public class GeneratedFileFilter : Filter { }
    #region IPE
    public class GeneratedFileXMLFilter : Filter { }
    #endregion
    #endregion

    #region Interface
    public interface IGeneratedFileModelRepository : IDisposable
    {
        bool GetCompletedTransaction(int page, int size, IList<GeneratedFileFilter> filters, string sortColumn, string sortOrder, ref GeneratedFileGrid file, ref string message);

        bool GetGenerateFileName(string ApplicationID, ref IList<GeneratedFileModel> GenerateFileName, ref string message);
        #region IPE
        bool GetGeneratedFileXML(int page, int size, IList<GeneratedFileXMLFilter> filters, string sortColumn, string sortOrder, ref GeneratedFileXMLGrid filexml, ref string message);
        bool GetTransactionMode(int ID, ref MasterConfigModel data, ref string message);
        #endregion
    }
    #endregion

    #region Repository
    public class GeneratedFileModelRepository : IGeneratedFileModelRepository
    {

        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetCompletedTransaction(int page, int size, IList<GeneratedFileFilter> filters, string sortColumn, string sortOrder, ref GeneratedFileGrid file, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;

                GeneratedFileModel filter = new GeneratedFileModel();
                if (filters != null)
                {
                    filter.FileName = (string)filters.Where(a => a.Field.Equals("FileName")).Select(a => a.Value).SingleOrDefault();
                    filter.ApplicationID = (string)filters.Where(a => a.Field.Equals("ApplicationID")).Select(a => a.Value).SingleOrDefault();
                }

                using (DBSEntities context = new DBSEntities())
                {
                    //var tApplicationID = (from a in context.GeneratedTransactions)
                    var tFileTemp = (from a in context.GeneratedTransactions
                                     where a.FileName.Contains(".txt")
                                     group a by a.ItemID into ga
                                     select ga).AsEnumerable()
                                     .Select(x => new GeneratedFileModel()
                                     {
                                         ItemID = x.FirstOrDefault().ItemID,
                                         FileName = x.FirstOrDefault().FileName,
                                         URL = x.FirstOrDefault().URL,
                                         ApplicationID = string.Join(", ", (x.Select(y => y.ApplicationID)).ToArray()),
                                         CreateDate = x.FirstOrDefault().CreateDate
                                     });

                    if (tFileTemp != null)
                    {
                        var tFile = (from a in tFileTemp.ToList()
                                     let isFileName = !string.IsNullOrEmpty(filter.FileName)
                                     let isApplicationID = !string.IsNullOrEmpty(filter.ApplicationID)
                                     where (isFileName ? a.FileName.Contains(filter.FileName) : true) &&
                                           (isApplicationID ? a.ApplicationID.Contains(filter.ApplicationID) : true)
                                     select new GeneratedFileModel
                                     {
                                         ItemID = a.ItemID,
                                         FileName = a.FileName,
                                         URL = a.URL,
                                         ApplicationID = a.ApplicationID,
                                         CreateDate = a.CreateDate
                                     });

                        file.Page = page;
                        file.Size = size;
                        file.Total = tFile.Count();
                        file.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                        file.Rows = tFile.AsQueryable().OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new GeneratedFileRow
                        {
                            RowID = i + 1,
                            ItemID = a.ItemID,
                            FileName = a.FileName,
                            URL = a.URL,
                            ApplicationID = a.ApplicationID,
                            CreateDate = a.CreateDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                    }
                    isSuccess = true;
                }


            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetGenerateFileName(string ApplicationID, ref IList<GeneratedFileModel> GenerateFileName, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    GenerateFileName = (from a in context.GeneratedTransactions
                                        where a.ApplicationID.Equals(ApplicationID)
                                        && a.FileName.Contains(".txt")
                                        select new GeneratedFileModel
                                        {
                                            ItemID = a.ItemID,
                                            FileName = a.FileName,
                                            URL = a.URL,
                                            ApplicationID = a.ApplicationID,
                                            CreateDate = a.CreateDate
                                        }).ToList();

                    isSuccess = true;
                }

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        #region IPE
        public bool GetGeneratedFileXML(int page, int size, IList<GeneratedFileXMLFilter> filters, string sortColumn, string sortOrder, ref GeneratedFileXMLGrid filexml, ref string message)
        {
            bool isSuccess = false;
            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;

                GenerateFileXMLModel filter = new GenerateFileXMLModel();
                if (filters != null)
                {
                    filter.FileName = (string)filters.Where(a => a.Field.Equals("FileName")).Select(a => a.Value).SingleOrDefault();
                    filter.ApplicationID = (string)filters.Where(a => a.Field.Equals("ApplicationID")).Select(a => a.Value).SingleOrDefault();
                }

                using (DBSEntities context = new DBSEntities())
                {
                    //var tApplicationID = (from a in context.GeneratedTransactions)
                    var tFileTemp = (from a in context.GeneratedTransactions
                                     where a.FileName.Contains(".xml")
                                     group a by a.ItemID into ga
                                     select ga).AsEnumerable()
                                     .Select(x => new GenerateFileXMLModel()
                                     {
                                         ItemID = x.FirstOrDefault().ItemID,
                                         FileName = x.FirstOrDefault().FileName,
                                         URL = x.FirstOrDefault().URL,
                                         ApplicationID = string.Join(", ", (x.Select(y => y.ApplicationID)).ToArray()),
                                         CreateDate = x.FirstOrDefault().CreateDate
                                     }).OrderByDescending(x => x.CreateDate);

                    if (tFileTemp != null)
                    {
                        var tFile = (from a in tFileTemp.ToList()
                                     let isFileName = !string.IsNullOrEmpty(filter.FileName)
                                     let isApplicationID = !string.IsNullOrEmpty(filter.ApplicationID)
                                     where (isFileName ? a.FileName.Contains(filter.FileName) : true) &&
                                           (isApplicationID ? a.ApplicationID.Contains(filter.ApplicationID) : true)
                                     select new GenerateFileXMLModel
                                     {
                                         ItemID = a.ItemID,
                                         FileName = a.FileName,
                                         URL = a.URL,
                                         ApplicationID = a.ApplicationID,
                                         CreateDate = a.CreateDate
                                     });

                        filexml.Page = page;
                        filexml.Size = size;
                        filexml.Total = tFile.Count();
                        filexml.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                        filexml.Rows = tFile.AsQueryable().OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new GeneratedFileXMLRow
                        {
                            RowID = i + 1,
                            ItemID = a.ItemID,
                            FileName = a.FileName,
                            URL = a.URL,
                            ApplicationID = a.ApplicationID,
                            CreateDate = a.CreateDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                    }
                    isSuccess = true;
                }


            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;

        }
        public bool GetTransactionMode(int ID, ref MasterConfigModel data, ref string message)
        {
            bool IsSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    data = (from a in context.Configs
                            where a.Parameter.Equals("Paymenot_Mode")
                            select new MasterConfigModel
                            {
                                Value = a.Value
                            }).SingleOrDefault();
                    IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return IsSuccess;
        }
        #endregion
    }
    #endregion
}