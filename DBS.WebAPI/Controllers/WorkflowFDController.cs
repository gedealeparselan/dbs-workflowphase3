﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

//=======================================================================================//
///FOR ALL DEVELOPER...
///PLEASE ADD NEW REGION BY YOUR NAME TO KEEP CLEAN !!! DONT BE A DIRTY PROGRAMMER!!!
///ANDIWIJAYA
//=======================================================================================//

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class WorkflowFDController : ApiController
    {
        private string message = string.Empty;
        /// <summary>
        /// Define repo here
        /// </summary>
        private readonly IWorkflowFDRepository repoFD = null;
        public WorkflowFDController()
        {
            this.repoFD = new WorkflowFDRepository();
        }
        /// <summary>
        /// Add Controller below...
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (repoFD != null)
                repoFD.Dispose();


            base.Dispose(disposing);
        }

        #region Andi
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowFD/{workflowInstanceID}/FD/CSO/{approverID}")]
        public HttpResponseMessage AddFDCSO(Guid workflowInstanceID, long approverID, FDDetailModel fd)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoFD.AddTransactionFDCSO(workflowInstanceID, approverID, fd, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "FD maker data has been saved." });
                }
            }
        }
        #endregion

        #region Chandra
        #endregion

        #region Agung
        [ResponseType(typeof(string))]
        [HttpPost]
        [Route("api/SaveFDCSOInterestMaintenace")]
        public HttpResponseMessage SaveFDAddInterestMaintenanceDetails(IList<FDInterestMaintenanceModel> FDIntMaintenance)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoFD.FDCSOInterestMaintenance(FDIntMaintenance, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "Create Data has been created." });
                }
            }

        }

        [ResponseType(typeof(FDInterestMaintenanceModel))]
        [HttpGet]
        [Route("api/FDInterestMaintenace/{logname}/{searchBy}/excel/{tanggal}")]
        public HttpResponseMessage FDAddInterestMaintenanceDetails(string logname, int searchBy, DateTime? tanggal)
        {
            IList<FDInterestMaintenanceModel> output = new List<FDInterestMaintenanceModel>();
            if (!repoFD.FDInterestMaintenance(logname,searchBy,tanggal,ref output,ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<FDInterestMaintenanceModel>>(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(FTPRateModel))]
        [HttpGet]
        [Route("api/GetFTPRateData")]
        public HttpResponseMessage GetFTPRateData()
        {
            IList<FTPRateModel> output = new List<FTPRateModel>();
            if (!repoFD.GetFTPRate(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<FTPRateModel>>(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(MatrixFDDoa))]
        [HttpGet]
        [Route("api/GetFDMatrixDOA")]
        public HttpResponseMessage GetFDMatrixDOA()
        {
            IList<MatrixFDDoa> output = new List<MatrixFDDoa>();
            if (!repoFD.GetMatrixFdDOA(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<MatrixFDDoa>>(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(string))]
        [HttpGet]
        [Route("api/GetTenorData")]
        public HttpResponseMessage GetTenor()
        {
            IList<string> output = new List<string>();
            if (!repoFD.GetTenorData(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<string>>(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowFD/{workflowInstanceID}/Transaction/FDChecker/{approverID}")]
        public HttpResponseMessage FDAddTransactionCheckerDetails(Guid workflowInstanceID, long approverID, TransactionFDCheckerDetailModel transactionChecker)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoFD.FDAddTransactionChecker(workflowInstanceID, approverID, transactionChecker, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been saved." });
                }
            }
        }

        [ResponseType(typeof(TransactionFDCheckerDetailModel))]
        [HttpGet]
        [Route("api/WorkflowFD/{workflowInstanceID}/Transaction/BranchMakerReview/{approverID}")]
        public HttpResponseMessage GetBranchMakerReviewDetails(Guid workflowInstanceID, long approverID)
        {
            FDDetailModel fd = new FDDetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TransactionPPUCheckerFDDetailModel output = new TransactionPPUCheckerFDDetailModel();

            try
            {
                if (!repoFD.GetFDDetails(workflowInstanceID, ref fd, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoFD.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {// #3 Get Transaction Checker Details
                        if (!repoFD.GetTransactionFDPPUCheckerDetails(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Transaction = fd;
                            output.Timelines = timelines;

                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {
                fd = null;
                timelines = null;
            }
        }

        [ResponseType(typeof(TransactionFDCheckerDetailModel))]
        [HttpGet]
        [Route("api/WorkflowFD/{workflowInstanceID}/FD/Checker/{approverID}")]
        public HttpResponseMessage GetFDCheckerDetails(Guid workflowInstanceID, long approverID)
        {
            FDDetailModel fd = new FDDetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TransactionFDCheckerDetailModel output = new TransactionFDCheckerDetailModel();

            try
            {
                if (!repoFD.GetFDDetails(workflowInstanceID, ref fd, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoFD.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {// #3 Get Transaction Checker Details
                        if (!repoFD.GetFDCheckerDetails(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Transaction = fd;
                            output.Timelines = timelines;

                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {
                fd = null;
                timelines = null;
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowFD/{workflowInstanceID}/FD/Maker/{approverID}")]
        public HttpResponseMessage AddFDMakerDetails(Guid workflowInstanceID, long approverID, TransactionFDCheckerDetailModel fd)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoFD.AddTransactionFDMaker(workflowInstanceID, approverID, fd, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "FD maker data has been saved." });
                }
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowFD/{workflowInstanceID}/PPU/Maker/{approverID}")]
        public HttpResponseMessage UpdatePPUMakerFDDetails(Guid workflowInstanceID, long approverID, TransactionPPUCheckerFDDetailModel fd)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoFD.UpdateTransactionPPUMakerFD(fd, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "PPU Maker data has been saved." });
                }
            }
        }

        #endregion

        #region Dani
        [ResponseType(typeof(TransactionPPUCheckerFDDetailModel))]
        [HttpGet]
        [Route("api/WorkflowFD/{workflowInstanceID}/Transaction/PPUChecker/{approverID}")]
        public HttpResponseMessage GetDataFDCheckerDetails(Guid workflowInstanceID, long approverID)
        {
            FDDetailModel fd = new FDDetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TransactionPPUCheckerFDDetailModel output = new TransactionPPUCheckerFDDetailModel();

            try
            {
                // #1 Get FD Details
                if (!repoFD.GetTransactionFDDetails(workflowInstanceID, ref fd, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoFD.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        //#3 Get Transaction Checker 
                        if (!repoFD.GetTransactionFDPPUCheckerDetails(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Transaction = fd;
                            output.Timelines = timelines;

                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {
                fd = null;
                timelines = null;
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowFD/{workflowInstanceID}/Transaction/PPUCheckerUpdate/{approverID}")]
        public HttpResponseMessage UpdateFDCheckerDetails(Guid workflowInstanceID, long approverID, TransactionPPUCheckerFDDetailModel data)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoFD.UpdateTransactionChecker(workflowInstanceID, approverID, data, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been updated." });
                }
            }
        }

        [ResponseType(typeof(TransactionPPUCheckerFDDetailModel))]
        [HttpGet]
        [Route("api/WorkflowFD/{workflowInstanceID}/Transaction/FDMakerReview/{approverID}")]
        public HttpResponseMessage GetDataFDMakerReview(Guid workflowInstanceID, long approverID)
        {
            FDDetailModel fd = new FDDetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TransactionFDCheckerDetailModel output = new TransactionFDCheckerDetailModel();

            try
            {
                // #1 Get FD Details
                if (!repoFD.GetTransactionFDMakerReviewDetails(workflowInstanceID, ref fd, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoFD.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        //#3 Get Transaction Checker 
                        if (!repoFD.GetFDCheckerDetails(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Transaction = fd;
                            output.Timelines = timelines;
                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {
                fd = null;
                timelines = null;
            }
        }

        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowFD/{workflowInstanceID}/Transaction/FDMakerReview/{approverID}")]
        public HttpResponseMessage UpdateFDMakerReview(Guid workflowInstanceID, long approverID, TransactionPPUCheckerFDDetailModel data)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoFD.UpdateTransactionFDMakerReview(workflowInstanceID, approverID, data, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction FD Maker Review has been updated." });
                }
            }
        }
        #endregion

        #region Basri
        /// <summary>
        /// Save Transaction Caller from PPU Caller
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approver ID from Nintex WF</param>
        /// <param name="data">Payment Maker data</param>
        /// <returns></returns>
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowFD/{workflowInstanceID}/Transaction/Callback/{approverID}")]
        public HttpResponseMessage AddTransactionCallbackDetails(Guid workflowInstanceID, long approverID, TransactionFDCallbackDetailModel data)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoFD.AddTransactionCallback(workflowInstanceID, approverID, data, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "transaction callback data has been saved." });
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="workflowInstanceID"></param>
        /// <param name="approverID"></param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionCallbackDetailModel))]
        [HttpGet]
        [Route("api/WorkflowFD/{workflowInstanceID}/Transaction/Callback/{approverID}")]
        public HttpResponseMessage GetTransactionCallbackDetails(Guid workflowInstanceID, long approverID)
        {
            TransactionFDDetailModel transaction = new TransactionFDDetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TransactionFDCallbackDetailModel output = new TransactionFDCallbackDetailModel();

            try
            {
                // #1 Get Transaction Details
                if (!repoFD.GetTransactionDetails(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "1" });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoFD.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "2" });
                    }
                    else
                    {
                        // #3 Get Transaction Checker Details getutc
                        if (!repoFD.GetTransactionCallbackDetails(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "3" });
                        }
                        else
                        {
                            output.Transaction = transaction;
                            output.Timelines = timelines;

                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {

                transaction = null;
                timelines = null;
            }
        }
        /// <summary>
        /// Get transaction details for checker afte caller by WF Instance ID
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approve ID from Nintex WF</param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionFDCheckerAfterCallbackDetailModel))]
        [HttpGet]
        [Route("api/WorkflowFD/{workflowInstanceID}/Transaction/Checker/Callback/{approverID}")]
        public HttpResponseMessage GetTransactionCheckCallbackDetails(Guid workflowInstanceID, long approverID)
        {
            TransactionFDDetailModel transaction = new TransactionFDDetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TransactionFDCheckerAfterCallbackDetailModel output = new TransactionFDCheckerAfterCallbackDetailModel();

            try
            {
                // #1 Get Transaction Details
                if (!repoFD.GetTransactionDetails(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoFD.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        // #3 Get Transaction Checker Details
                        if (!repoFD.GetTransactionCheckerAfterCallbackDetails(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Transaction = transaction;
                            output.Timelines = timelines;

                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {

                transaction = null;
                timelines = null;
            }
        }
        /// <summary>
        /// Save approval data from checker after callback
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approver ID from Nintex WF</param>
        /// <param name="transactionChecker">Transaction checker data</param>
        /// <returns></returns>
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowFD/{workflowInstanceID}/Transaction/Checker/Callback/{approverID}")]
        public HttpResponseMessage AddTransactionCheckerCallbackDetails(Guid workflowInstanceID, long approverID, TransactionFDCheckerDetailModel data)
        {
            // Forward to existing method: same as url "api/WorkflowFD/{workflowInstanceID}/Transaction/Checker/{approverID}"
            return AddTransactionCheckerDetails(workflowInstanceID, approverID, data);
        }

        /// <summary>
        /// Save approval data from checker after callback
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approver ID from Nintex WF</param>
        /// <param name="transactionChecker">Transaction checker data</param>
        /// <returns></returns>
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowFD/{workflowInstanceID}/Transaction/MakerReviseAfterCaller/{approverID}")]
        public HttpResponseMessage AddTransactionMakerAfterReviseCallerDetails(Guid workflowInstanceID, long approverID, TransactionFDDetailModel transaction)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoFD.AddTransactionMakerAfterReviseCallerDetails(workflowInstanceID, approverID, transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "PPU Maker data has been updated." });
                }
            }
        }

        /// <summary>
        /// Get transaction details for checker afte caller by WF Instance ID
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approve ID from Nintex WF</param>
        /// <returns></returns>
        [ResponseType(typeof(TransactionFDCheckerAfterCallbackDetailModel))]
        [HttpGet]
        [Route("api/WorkflowFD/{workflowInstanceID}/Transaction/MakerReviseAfterCaller/{approverID}")]
        public HttpResponseMessage GetTransactionMakerAfterReviseCallerDetails(Guid workflowInstanceID, long approverID)
        {
            TransactionFDDetailModel transaction = new TransactionFDDetailModel();
            IList<TransactionTimelineModel> timelines = new List<TransactionTimelineModel>();
            TransactionFDCheckerAfterCallbackDetailModel output = new TransactionFDCheckerAfterCallbackDetailModel();

            try
            {
                // #1 Get Transaction Details
                if (!repoFD.GetTransactionMakerReviseAfterCheckerDetails(workflowInstanceID, ref transaction, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    // #2 Get Transaction Timeline
                    if (!repoFD.GetTransactionTimeline(workflowInstanceID, ref timelines, ref message))
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                    }
                    else
                    {
                        // #3 Get Transaction Checker Details
                        if (!repoFD.GetTransactionCheckerAfterCallbackDetails(workflowInstanceID, approverID, ref output, ref message))
                        {
                            return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                        }
                        else
                        {
                            output.Transaction = transaction;
                            output.Timelines = timelines;

                            return Request.CreateResponse(HttpStatusCode.OK, output);
                        }
                    }
                }
            }
            finally
            {

                transaction = null;
                timelines = null;
            }
        }
        /// <summary>
        /// Save approval data from checker
        /// </summary>
        /// <param name="workflowInstanceID">Workflow Instance ID from Nintex WF</param>
        /// <param name="approverID">Approver ID from Nintex WF</param>
        /// <param name="transactionChecker">Transaction checker data</param>
        /// <returns></returns>
        [ResponseType(typeof(object))]
        [HttpPost]
        [Route("api/WorkflowFD/{workflowInstanceID}/Transaction/Checker/{approverID}")]
        public HttpResponseMessage AddTransactionCheckerDetails(Guid workflowInstanceID, long approverID, TransactionFDCheckerDetailModel transactionChecker)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repoFD.AddTransactionChecker(workflowInstanceID, approverID, transactionChecker, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Transaction checker has been saved." });
                }
            }
        }
        #endregion

        #region Aridya
        //aridya 20161220 get fd maker history
        [ResponseType(typeof(IList<FDMakerHistoryModel>))]
        [HttpPost]
        [Route("api/WorkflowFD/{transactionId}/TransactionFDMakerHistory/{approverId}")]
        public HttpResponseMessage GetTransactionFDMakerHistory(long transactionId, long approverId, FDMakerUserModel fdMakerUserModel)
        {
            IList<FDMakerHistoryModel> output = new List<FDMakerHistoryModel>();
            if (!repoFD.GetTransactionFDMakerHistory(transactionId, approverId, fdMakerUserModel, ref output, ref message))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, output);
            }
        }
        #endregion
    }
}

//=======================================================================================//
///FOR ALL DEVELOPER...
///PLEASE ADD NEW REGION BY YOUR NAME TO KEEP CLEAN !!! DONT BE A DIRTY PROGRAMMER!!!
///ANDIWIJAYA
//=======================================================================================//