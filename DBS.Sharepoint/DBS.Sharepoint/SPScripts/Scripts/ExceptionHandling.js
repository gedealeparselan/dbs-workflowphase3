/*
var accessToken;
var $box;
var $remove = false;


var GridPropertiesModel = function (callback) {
    var self = this;	
    self.SortColumn = ko.observable();
    self.SortOrder = ko.observable();
    self.AllowFilter = ko.observable(false);
    self.AvailableSizes = ko.observableArray(config.sizes);
    self.Page = ko.observable(1);
    self.Size = ko.observable(config.sizeDefault);
    self.Total = ko.observable(0);
    self.TotalPages = ko.observable(0);

    self.Callback = function () {
        callback();
    };

    self.OnPageSizeChange = function () {
        self.Page(1);

        self.Callback();
    };

    self.OnPageChange = function () {
        if (self.Page() < 1) {
            self.Page(1);
        } else {
            if (self.Page() > self.TotalPages())
                self.Page(self.TotalPages());
        }

        self.Callback();
    };

    self.NextPage = function () {
        var page = self.Page();
        if (page < self.TotalPages()) {
            self.Page(page + 1);

            self.Callback();
        }
    };

    self.PreviousPage = function () {
        var page = self.Page();
        if (page > 1) {
            self.Page(page - 1);

            self.Callback();
        }
    };

    self.FirstPage = function () {
        self.Page(1);

        self.Callback();
    };

    self.LastPage = function () {
        self.Page(self.TotalPages());

        self.Callback();
    };

    self.Filter = function (data) {
        self.Page(1);

        self.Callback();
    };

    // get sorted column
    self.GetSortedColumn = function (columnName) {
        var sort = "sorting";

        if (self.SortColumn() == columnName) {
            if (self.SortOrder() == "DESC")
                sort = sort + "_asc";
            else
                sort = sort + "_desc";
        }

        return sort;
    };

    // Sorting data
    self.Sorting = function (column) {
        if (self.SortColumn() == column) {
            if (self.SortOrder() == "ASC")
                self.SortOrder("DESC");
            else
                self.SortOrder("ASC");
        } else {
            self.SortOrder("ASC");
        }

        self.SortColumn(column);

        self.Page(1);

        self.Callback();
    };
};



var DetailModel = function(selParameter,selCondition,value,ddlValues)
{
    var self = this;
    self.selectParameter = ko.observable(selParameter);
    self.ddlParameter = ko.observableArray(ddlParameters);

    self.selectCondition = ko.observable(selCondition);
    self.ddlCondition = ko.observableArray(ddlConditions);

    self.value = ko.observable(value);
    self.ddlValue = ko.observableArray(ddlValues);
    self.IsddlValue = ko.observable(false);
    self.selectValue = ko.observable(value);

}

var resultModel = function(ID,parameterID,conditionID,value)
{
    var self = this;
    self.ID = ko.observable(ID);
    self.ParameterField = ko.observable(parameterID);
    self.Condition =ko.observable(conditionID);
    self.Value = ko.observable(value);

}

var CurrencyModel = function(id,code){
    var self = this;
    self.ID=ko.observable(id);
    self.Name =ko.observable(code);
}
var MatrixModel = function(id,name,desc)
{
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
    self.Description = ko.observable(desc);
}

var SegmentModel = function(id,name,desc)
{
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
    self.Description = ko.observable(desc);
}

var BranchModel = function(id,name)
{
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
}*/


var ddlInternalConditions = [{ ID: 1, Name: "AND" }, { ID: 2, Name: "OR" }];
var ddlExternalConditions = [{ ID: 1, Name: "AND" }, { ID: 2, Name: "OR" }];
var ddlValueTypes = [{ ID: 0, Name: "Value" }, { ID: 1, Name: "Parameter" }];

var ViewModel = function () {
    //Make the self as 'this' reference
    var self = this;
    self.Readonly = ko.observable(false);
    self.IsWorkflow = ko.observable(false);

    //Declare observable which will be bind with UI 
    self.ID = ko.observable("");
    self.CaseName = ko.observable("");
    self.Matrix = ko.observable(new MatrixModel('', '', ''));
    self.ParamName = ko.observable("");
    self.OperatorName = ko.observable("");
    self.ValueTypeName = ko.observable("");
    self.ValueName = ko.observable("");
    self.LastModifiedBy = ko.observable("");
    self.LastModifiedDate = ko.observable("");
    self.IsddlValue = ko.observable(false);

    //Dropdown
    self.ddlMatrix = ko.observableArray([]);
    self.ddlSubParameters = ko.observableArray([]);
    self.ddlSubConditions = ko.observableArray([]);
    self.ddlSubValuetypes = ko.observableArray([]);
    self.ddlSubValues = ko.observableArray([]);
    self.ddlValue = ko.observableArray([]);

    // filter
    self.FilterCaseName = ko.observable("");
    self.FilterMatrix = ko.observable("");
    self.FilterModifiedBy = ko.observable("");
    self.FilterModifiedDate = ko.observable("");

    self.Details = ko.observableArray([new DetailModel([new SubDetailModel('', '', '', '', '', '', '', [], true)], '', '', [], [])]);
    self.Subdetails = ko.observableArray([new SubDetailModel('', '', '', '', '', '', '', [], true)]);

    // New Data flag
    self.IsNewData = ko.observable(false);
    self.IsValueTypeParameter = ko.observable(false);

    self.IsRoleMaker = ko.observable(false);
    self.IsPermited = ko.observable(false);

    self.ExceptionHandlings = ko.observableArray([]);
    self.ExceptionHandlingsDetail = ko.observableArray([new resultDetailModel(0, '', '', [])]);
    self.ExceptionHandlingsSubDetail = ko.observableArray([new resultSubdetailModel(0, '', '', '', '', '', '', '')]);

    // grid properties
    self.GridProperties = ko.observable();
    self.GridProperties(new GridPropertiesModel(GetData));

    // set default sorting
    self.GridProperties().SortColumn("CaseName");
    self.GridProperties().SortOrder("ASC");

    // bind clear filters
    self.ClearFilters = function () {
        self.FilterCaseName("");
        self.FilterMatrix("");
        self.FilterModifiedBy("");
        self.FilterModifiedDate("");

        GetData();
    };


    // bind get data function to view
    self.GetData = function () {
        GetData();
    };
    self.GetDropdown = function () {
        GetDropdown();
    };
    //The Object which stored data entered in the observables
    var ExceptionHandling = {
        ID: self.ID,
        CaseName: self.CaseName,
        Matrix: self.Matrix,
        ExceptionHandlingsDetail: self.ExceptionHandlingsDetail,
        LastModifiedBy: self.LastModifiedBy,
        LastModifiedDate: self.LastModifiedDate
    };

    self.AddGroup = function () {

        self.Details.push(new DetailModel([new SubDetailModel('', '', '', '', '', '', '', ddlValueTypes, true)], '', '', ddlInternalConditions, ddlExternalConditions));

    };

    self.RemoveGroup = function () {
        self.Details.remove(this);
    };

    self.IsPermited = function (IsPermitedResult) {
        //Ajax call to delete the Customer
        spUser = $.cookie(api.cookie.spUser);

        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/CheckUserPermited/" + _spFriendlyUrlPageContextInfo.title,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    //compare user role to page permission to get checker validation
                    console.log("spUser Roles : " + ko.toJSON(spUser.Roles));
                    console.log("spUser Permited : " + data);
                    for (i = 0; i < spUser.Roles.length; i++) {
                        for (j = 0; j < data.length; j++) {
                            if (Const_RoleName[spUser.Roles[i].ID] == data[j]) { //if (spUser.Roles[i].Name == data[j]) {
                                if (Const_RoleName[spUser.Roles[i].ID].endsWith('Maker')) { //if (spUser.Roles[i].Name.endsWith('Maker')) {
                                    self.IsRoleMaker(true);
                                    return;
                                }
                                else {
                                    self.IsRoleMaker(false);
                                }
                            }
                        }
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    self.save = function () {
        // validation

        var form = $("#aspnetForm");
        form.validate();
        changeObject(self.Details());

        if (form.valid()) {
            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {

                    //console.log(ko.toJSON(ExceptionHandling));
                    //Ajax call to insert the ExceptionHandlings
                    $.ajax({
                        type: "POST",
                        url: api.server + api.url.exceptionHandling,
                        data: ko.toJSON(ExceptionHandling), //Convert the Observable Data into JSON
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {
                                // send notification
                                ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // hide current popup window
                                $("#modal-form").modal('hide');

                                // refresh data
                                GetData();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });
                }
            });
        }
    };

    self.update = function () {
        // validation

        //ExceptionHandling.MatrixName($('#matrix option:selected').text());

        var form = $("#aspnetForm");
        form.validate();
        changeObject(self.Details());
        if (form.valid()) {

            // hide current popup window
            //$("#modal-form").modal('hide');

            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {
                    //Ajax call to update the Customer
                    $.ajax({
                        type: "PUT",
                        url: api.server + api.url.exceptionHandling + "/" + ExceptionHandling.ID(),
                        data: ko.toJSON(ExceptionHandling),
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {
                                // send notification
                                ShowNotification('Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // hide current popup window
                                $("#modal-form").modal('hide');

                                // refresh data
                                GetData();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });
                }
            });
        }
    };

    self.delete = function () {

        changeObject(self.Details());
        //ExceptionHandling.MatrixName($('#matrix option:selected').text());
        // hide current popup window
        //$("#modal-form").modal('hide');

        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                $("#modal-form").modal('show');
            } else {
                //Ajax call to delete the Customer
                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.exceptionHandling + "/" + ExceptionHandling.ID(),
                    data: ko.toJSON(ExceptionHandling),
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if (jqXHR.status = 200) {
                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                            // refresh data
                            GetData();
                        } else
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
            }
        });
    };

    // Local Date
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return "";
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-17
    };

    //Function to Display record to be updated
    self.GetSelectedRow = function (data) {

        self.IsNewData(false);


        if (self.IsWorkflow()) $("#modal-form-workflow").modal('show');
        else $("#modal-form").modal('show');

        self.IsddlValue(false);
        self.ID(data.ID);
        self.CaseName(data.CaseName);
        self.Matrix(new MatrixModel(data.Matrix.ID, data.Matrix.Name, data.Matrix.Description));

        self.LastModifiedBy(data.LastModifiedBy);
        self.LastModifiedDate(data.LastModifiedDate);
        self.Details([]);

        //console.log(data.ExceptionHandlingsDetail);
        if (data.ExceptionHandlingsDetail.length > 0) {
            for (var i = 0; i < data.ExceptionHandlingsDetail.length; i++) {
                var internalCondition = data.ExceptionHandlingsDetail[i].InternalCondition;
                var externalCondition = data.ExceptionHandlingsDetail[i].ExternalCondition;

                self.Details.push(new DetailModel([], internalCondition, externalCondition, ddlInternalConditions, ddlExternalConditions));

                if (data.ExceptionHandlingsDetail[i].ExceptionHandlingsSubDetail.length > 0) {
                    for (var j = 0; j < data.ExceptionHandlingsDetail[i].ExceptionHandlingsSubDetail.length; j++) {

                        var parameterID = data.ExceptionHandlingsDetail[i].ExceptionHandlingsSubDetail[j].ParamID;
                        var parameterName = data.ExceptionHandlingsDetail[i].ExceptionHandlingsSubDetail[j].ParamName;
                        var operatorID = data.ExceptionHandlingsDetail[i].ExceptionHandlingsSubDetail[j].OperatorID;
                        var operatorName = data.ExceptionHandlingsDetail[i].ExceptionHandlingsSubDetail[j].OperatorName;
                        var valueType = data.ExceptionHandlingsDetail[i].ExceptionHandlingsSubDetail[j].ValueType;
                        var valueTypeName = data.ExceptionHandlingsDetail[i].ExceptionHandlingsSubDetail[j].ValueTypeName;
                        var rawValue = data.ExceptionHandlingsDetail[i].ExceptionHandlingsSubDetail[j].Value;
                        var value = '';

                        if (data.ExceptionHandlingsDetail[i].ExceptionHandlingsSubDetail[j].ValueType == 1) {
                            var posStart = rawValue.indexOf('(') + 1;
                            var posEnd = rawValue.length;
                            value = (data.ExceptionHandlingsDetail[i].ExceptionHandlingsSubDetail[j].Value).substring(posStart, posEnd);
                            value = value.replace('(', '').replace(')', '');
                        } else {
                            value = data.ExceptionHandlingsDetail[i].ExceptionHandlingsSubDetail[j].Value;
                        }

                        var IsVisible = true;
                        if (valueType == undefined) {

                            IsVisible = false;
                            valueTypeName = '';
                            value = '';

                            $('#lblvaluetype' + i + j).hide(); $('#lblvalue' + i + j).hide();
                            $('#ValueType' + i + j).hide(); $('#Value' + i + j).hide();

                        }
                        self.Details()[i].Subdetails.push(new SubDetailModel(parameterID, parameterName, operatorID, operatorName, valueType, valueTypeName, value, ddlValueTypes, IsVisible));
                        //console.log(ko.toJSON(self.Details()[i].Subdetails));
                        var suspected = $('#lblParameter' + i + j).text().trim();

                        if ((data.ExceptionHandlingsDetail[i].ExceptionHandlingsSubDetail[j].ValueType == 1) &&
			           		(suspected == ': undefined')) {
                            $('#Value' + i + j).hide();
                            $('#ValueTypeParameter' + i + j).show();
                            $('#Value' + i + j).val('');
                        } else if ((data.ExceptionHandlingsDetail[i].ExceptionHandlingsSubDetail[j].ValueType == 1) &&
							(suspected != ': undefined')) {
                            $('#Value' + i + j).hide();
                            $('#ValueTypeParameter' + i + j).hide();
                        } else if ((data.ExceptionHandlingsDetail[i].ExceptionHandlingsSubDetail[j].ValueType != 1) &&
							(suspected == ': undefined')) {
                            var conditionOperator = $('#ConditionOpr' + i + j + ' option:selected').text();
                            if ((conditionOperator.toLowerCase().trim() == 'is empty') ||
								(conditionOperator.toLowerCase().trim() == 'is not empty')) {
                                $('#Value' + i + j).hide();
                                $('#lblstarvaluetypename' + i + j).hide();
                                $('#lblstarvalueparameter' + i + j).hide();
                            } else {
                                $('#Value' + i + j).show();
                                $('#lblstarvaluetypename' + i + j).show();
                                $('#lblstarvalueparameter' + i + j).show();

                            }
                            $('#ValueTypeParameter' + i + j).hide();
                        } else if ((data.ExceptionHandlingsDetail[i].ExceptionHandlingsSubDetail[j].ValueType != 1) &&
							(suspected != ': undefined')) {
                            $('#Value' + i + j).hide();
                            $('#ValueTypeParameter' + i + j).hide();
                            $('#lblstarvaluetypename' + i + j).hide();
                            $('#lblstarvalueparameter' + i + j).hide();


                        }
                    }
                }

                //SetControlValue(parameterID,i);
                //self.Details()[i].SubDetailExceptionalHandling[i].value);
                //self.Details()[i].SubDetailExceptionalHandling[i].selectValue(value);
            }

        }
        $(".lblforhidden").each(
	        function () {
	            var content = $(this).text().trim();

	            //Get the length of the content;
	            //var contentLength = $.trim( content ).length;
	            if ((content == ":")) {
	                $(this).hide();
	            }
	        }
    	);
    };
    function IsEmptyOrIsNotEmpty(parentindex, index) {

        var Condition = $('#ConditionOpr' + parentindex + index + ' option:selected').text();
        var ValueType = $('#ValueType' + parentindex + index + ' option:selected').text();
        if ((Condition.toLowerCase().trim() == 'is empty') ||
			(Condition.toLowerCase().trim() == 'is not empty')) {

            $('#lblvaluetype' + parentindex + index).hide(); $('#lblvalue' + parentindex + index).hide();
            $('#ValueType' + parentindex + index).hide(); $('#Value' + parentindex + index).hide();
            $('#ValueTypeParameter' + parentindex + index).hide();
            $('#lblstarvaluetypename' + parentindex + index).hide();
            $('#lblstarvalueparameter' + parentindex + index).hide();


        } else {

            $('#lblvaluetype' + parentindex + index).show(); $('#lblvalue' + parentindex + index).show();
            $('#ValueType' + parentindex + index).show(); $('#Value' + parentindex + index).show();
            $('#lblstarvaluetypename' + parentindex + index).show();
            $('#lblstarvalueparameter' + parentindex + index).show();

            if (ValueType.toLowerCase().trim() == 'parameter') {
                $('#ValueTypeParameter' + parentindex + index).show(); $('#Value' + parentindex + index).hide();
            } else {
                $('#ValueTypeParameter' + parentindex + index).hide(); $('#Value' + parentindex + index).show();

            }



        }

    }
    self.onChangeCondition = function (parentindex, index) {

        IsEmptyOrIsNotEmpty(parentindex, index);

    };

    function IsValueTypeParameter(parentindex, index) {

        var ValueType = $('#ValueType' + parentindex + index + ' option:selected').text();
        if ((ValueType.toLowerCase().trim() == 'parameter')) {

            //self.IsValueTypeParameter(true);
            $('#ValueTypeParameter' + parentindex + index).show();
            $('#Value' + parentindex + index).hide();


        } else {
            //self.IsValueTypeParameter(false);
            $('#ValueTypeParameter' + parentindex + index).hide();
            $('#Value' + parentindex + index).show();

        }

    }

    self.onChangeValueType = function (parentindex, index) {

        IsValueTypeParameter(parentindex, index);

    };

    //insert new
    self.NewData = function () {
        // flag as new Data
        self.IsNewData(true);
        self.Readonly(false);
        self.ID(0);
        self.CaseName('');
        self.Matrix(new MatrixModel('', '', ''));

        self.Details.removeAll();
        self.Details([new DetailModel([new SubDetailModel('', '', '', '', '', '', '', ddlValueTypes, true)], '', '', ddlInternalConditions, ddlExternalConditions)]);
        //console.log(ko.toJS(self.IsNewData));
    };

    // Function to get Biz Segment for Dropdownlist
    self.OnAfterGetDropdown = null;
    function GetDropdown() {
        $.ajax({

            type: "GET",
            url: api.server + api.url.parameter + '?select=Matrix,ExceptionHandlingParameter,ExceptionHandlingOperator',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {

                if (jqXHR.status = 200) {
                    self.ddlMatrix(data['Matrix']);
                    self.ddlSubParameters(data['ExceptionHandlingParameter']);
                    self.ddlSubConditions(data['ExceptionHandlingOperator']);
                    if (self.OnAfterGetDropdown) self.OnAfterGetDropdown();
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    //Function to Read All Employee
    function GetData() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.exceptionHandling,
            params: {
                page: self.GridProperties().Page(),
                size: self.GridProperties().Size(),
                sort_column: self.GridProperties().SortColumn(),
                sort_order: self.GridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetData, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetData, OnError, OnAlways);
        }
    }


    // Get filtered columns value
    function GetFilteredColumns() {
        // define filter
        var filters = [];
        if (self.FilterCaseName() != "") filters.push({ Field: 'CaseName', Value: self.FilterCaseName() });
        if (self.FilterMatrix() != "") filters.push({ Field: 'Matrix', Value: self.FilterMatrix() });
        if (self.FilterModifiedBy() != "") filters.push({ Field: 'LastModifiedBy', Value: self.FilterModifiedBy() });
        if (self.FilterModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.FilterModifiedDate() });

        return filters;
    };

    // On success GetData callback
    function OnSuccessGetData(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.ExceptionHandlings(data.Rows);

            self.GridProperties().Page(data['Page']);
            self.GridProperties().Size(data['Size']);
            self.GridProperties().Total(data['Total']);
            self.GridProperties().TotalPages(Math.ceil(self.GridProperties().Total() / self.GridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    // On Error callback
    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    // On Always callback
    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }


    // Custom Function
    function changeObject(Details) {
        var value = "";
        self.ExceptionHandlingsDetail([]);

        if (ko.toJSON(Details.length) > 0) {

            for (var i = 0; i < Details.length; i++) {
                var internalCondition = Details[i].selectInternalCondition();
                var externalCondition = Details[i].selectExternalCondition();

                self.ExceptionHandlingsDetail.push(new resultDetailModel(0, internalCondition, externalCondition, []));
                //console.log(ko.toJSON(Details[i]));				
                if (ko.toJSON(Details[i].Subdetails().length) > 0) {

                    for (var j = 0; j < Details[i].Subdetails().length; j++) {

                        var parameterID = Details[i].Subdetails()[j].selectParameter();
                        var parameterName = $('#Parameter' + i + j + ' option:selected').text();
                        var operatorID = Details[i].Subdetails()[j].selectCondition();
                        var operatorName = $('#ConditionOpr' + i + j + ' option:selected').text();
                        var valueType = Details[i].Subdetails()[j].selectValueType();
                        var valueTypeName = $('#ValueType' + i + j + ' option:selected').text();
                        var valueTypeParameterName = $('#ValueTypeParameter' + i + j + ' option:selected').text();
                        var value = ''
                        if (valueTypeName.toLowerCase().trim() == 'parameter') {
                            value = Details[i].Subdetails()[j].selectValueParameter() + ' (' + valueTypeParameterName + ')';
                        } else {
                            value = Details[i].Subdetails()[j].selectValue();
                        }


                        if ((valueType == undefined) || (operatorName.toLowerCase().trim() == 'is empty') ||
							(operatorName.toLowerCase().trim() == 'is not empty')) {

                            valueType = undefined;
                            valueTypeName = '';
                            value = '';

                        }

                        self.ExceptionHandlingsDetail()[i].ExceptionHandlingsSubDetail.push(new resultSubdetailModel(0, parameterID, parameterName, operatorID, operatorName, valueType, valueTypeName, value));


                    }
                }

            }


        }
    }
    /*function SetControlValue(parameterID,index)
    {
        self.Details()[index].ddlCondition(ddlEquals);
        switch(parameterID)
        {
            case 1://Product
                self.Details()[index].IsddlValue(true);
                self.Details()[index].ddlValue(self.tmpProduct());
                //self.Details()[index].ddlCondition(ddlEquals);
                break;
            case 2://Currency
                self.Details()[index].IsddlValue(true);
                self.Details()[index].ddlValue(self.tmpCurrency());
                //self.Details()[index].ddlCondition(ddlEquals);
                break;
            case 3://Eqv USD
                self.Details()[index].IsddlValue(false);
                //self.Details()[index].ddlCondition(ddlEquals);
                self.Details()[index].selectValue('');
                break;
            case 4://Channel
                self.Details()[index].IsddlValue(true);
                self.Details()[index].ddlValue(self.tmpChannel());
                //self.Details()[index].ddlCondition(ddlEquals);
                break;
            case 5://Biz Segment
                self.Details()[index].IsddlValue(true);
                self.Details()[index].ddlValue(self.ddlSegment());
                //self.Details()[index].ddlCondition(ddlEquals);
                break;
            case 6: // Dormant Account
                self.Details()[index].IsddlValue(true);
                self.Details()[index].ddlValue([{ID:1,Name:"True"},{ID:2,Name:"False"}]);
                //self.Details()[index].ddlCondition(ddlEquals);
                break;
            case 7: //Hubungan Keuangan antar pelaku transaksi
                self.Details()[index].IsddlValue(true);
                self.Details()[index].ddlValue([{ID:1,Name:"True"},{ID:2,Name:"False"}]);
                //self.Details()[index].ddlCondition(ddlEquals);
                break;
            case 8: //Sandi Tujuan Transaksi
                self.Details()[index].IsddlValue(true);
                self.Details()[index].ddlValue([{ID:1,Name:"True"},{ID:2,Name:"False"}]);
                //self.Details()[index].ddlCondition(ddlEquals);
                break;
            default:
                break;
        }
    }*/
    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }

    });

    // text editor (Body Email from Exception Handling)
    function showErrorAlert(reason, detail) {
        var msg = '';
        if (reason === 'unsupported-file-type') { msg = "Unsupported format " + detail; }
        else {
            console.log("error uploading file", reason, detail);
        }
        $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
            '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
    }

    //$('#editor1').ace_wysiwyg();//this will create the default editor will all buttons
    //but we want to change a few buttons colors for the third style
    /*$('#editor1').ace_wysiwyg({
        toolbar:
            [
                'font',
                null,
                'fontSize',
                null,
                {name:'bold', className:'btn-info'},
                {name:'italic', className:'btn-info'},
                {name:'strikethrough', className:'btn-info'},
                {name:'underline', className:'btn-info'},
                null,
                {name:'insertunorderedlist', className:'btn-success'},
                {name:'insertorderedlist', className:'btn-success'},
                {name:'outdent', className:'btn-purple'},
                {name:'indent', className:'btn-purple'},
                null,
                {name:'justifyleft', className:'btn-primary'},
                {name:'justifycenter', className:'btn-primary'},
                {name:'justifyright', className:'btn-primary'},
                {name:'justifyfull', className:'btn-inverse'},
                null,
                {name:'createLink', className:'btn-pink'},
                {name:'unlink', className:'btn-pink'},
                null,
                null,//{name:'insertImage', className:'btn-success'},
                null,
                'foreColor',
                null,
                {name:'undo', className:'btn-grey'},
                {name:'redo', className:'btn-grey'}
            ],
        'wysiwyg': {
            fileUploadError: showErrorAlert
        }
    }).prev().addClass('wysiwyg-style2');


    $('[data-toggle="buttons"] .btn').on('click', function(e){
        var target = $(this).find('input[type=radio]');
        var which = parseInt(target.val());
        var toolbar = $('#editor1').prev().get(0);
        if(which == 1 || which == 2 || which == 3) {
            toolbar.className = toolbar.className.replace(/wysiwyg\-style(1|2)/g , '');
            if(which == 1) $(toolbar).addClass('wysiwyg-style1');
            else if(which == 2) $(toolbar).addClass('wysiwyg-style2');
        }
    });


    if ( typeof jQuery.ui !== 'undefined' && /applewebkit/.test(navigator.userAgent.toLowerCase()) ) {

        var lastResizableImg = null;
        function destroyResizable() {
            if(lastResizableImg == null) return;
            lastResizableImg.resizable( "destroy" );
            lastResizableImg.removeData('resizable');
            lastResizableImg = null;
        }

        var enableImageResize = function() {
            $('.wysiwyg-editor')
                .on('mousedown', function(e) {
                    var target = $(e.target);
                    if( e.target instanceof HTMLImageElement ) {
                        if( !target.data('resizable') ) {
                            target.resizable({
                                aspectRatio: e.target.width / e.target.height,
                            });
                            target.data('resizable', true);

                            if( lastResizableImg != null ) {//disable previous resizable image
                                lastResizableImg.resizable( "destroy" );
                                lastResizableImg.removeData('resizable');
                            }
                            lastResizableImg = target;
                        }
                    }
                })
                .on('click', function(e) {
                    if( lastResizableImg != null && !(e.target instanceof HTMLImageElement) ) {
                        destroyResizable();
                    }
                })
                .on('keydown', function() {
                    destroyResizable();
                });
        }
        enableImageResize();
    }*/
    // text editor end
};
/*
$(document).ready(function () {
	$('#modal-form > .modal-dialog > .modal-content > .modal-body').load('/Pages/Dialogs/ExceptionHandling.html',function(){
    // widget loader
    $box = $('#widget-box');
    var event;
    $box.trigger(event = $.Event('reload.ace.widget'))
    if (event.isDefaultPrevented()) return
    $box.blur();

    /// block enter key from user to prevent submitted data.
    $("input").keypress(function (event) {
        var code = event.charCode || event.keyCode;
        if (code == 13) {
            ShowNotification("Page Information", "Enter key is disabled for this form.", "gritter-warning", false);

            return false;
        }
    });

    // scrollables
    $('.slim-scroll').each(function () {
        var $this = $(this);
        $this.slimscroll({
            height: $this.data('height') || 100,
            //width: $this.data('width') || 100,
            railVisible: true,
            alwaysVisible: true,
            color: '#D15B47'
        });
    });

    var viewModel = new ViewModel();
    ko.applyBindings(viewModel);

    // Token Validation
    if($.cookie(api.cookie.name) == undefined){
        Helper.Token.Request(TokenOnSuccess, TokenOnError);
    }else{
        // read token from cookie
        accessToken = $.cookie(api.cookie.name);

        // call spuser function
        GetCurrentUser(viewModel);

        // call get data inside view model
        viewModel.GetData();
		
		viewModel.GetDropdown();
        
    }

    $('.date-picker').datepicker({autoclose: true}).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });

    //$("#aspnetForm").validate({onsubmit: false});

		});			
});

// Get SPUser from cookies
function GetCurrentUser(vm) {
    if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
        spUser = $.cookie(api.cookie.spUser);

        vm.SPUser = spUser;
    }
}


// Token validation On Success function
function TokenOnSuccess(data, textStatus, jqXHR){
    // store token on browser cookies
    $.cookie(api.cookie.name, data.AccessToken);
    accessToken = $.cookie(api.cookie.name);

    // call spuser function
    GetCurrentUser();

    // call get data inside view model
    viewModel.GetParameters();
    viewModel.GetData();
}

// Token validation On Error function
function TokenOnError(jqXHR, textStatus, errorThrown){
    ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}*/