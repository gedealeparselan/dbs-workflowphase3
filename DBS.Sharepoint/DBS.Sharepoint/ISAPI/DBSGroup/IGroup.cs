﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace DBS.Sharepoint
{
    #region Interfaces

    [ServiceContract]
    public interface IGroup
    {
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]        
        string RemoveUserFromGroup(Remove vRemove);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]        
        string AddUserToAGroup(Add vAdd);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]        
        string DeleteUserFromSite(Delete vDelete);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]        
        string UpdateUserName(Update vUpdate);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string CreateGroup(AddGroup vAdd);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string UpdateGroup(UpdateGroup vUpdate);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string DeleteUserFromSiteCollection(Delete vDelete);
    }

    #endregion

    #region Properties
    [DataContract]
    public class Remove
    {
        [DataMember]
        public string sGroupName { get; set; }
        [DataMember]
        public string sLoginName { get; set; }
        [DataMember]
        public string sWeb { get; set; }
        //[DataMember]
        //public string sGroupNameRemove { get; set; }
    }

    [DataContract]
    public class Delete
    {
        [DataMember]
        public string sLoginName { get; set; }
        [DataMember]
        public string sWeb { get; set; }
        
    }
    [DataContract]
    public class Update
    {
        [DataMember]
        public string sLoginName { get; set; }
        [DataMember]
        public string sWeb { get; set; }
        [DataMember]
        public string userFullName { get; set; }
    }

    [DataContract]
    public class Add 
    {
        [DataMember]
        public string userLoginName { get; set; }
        [DataMember]
        public string userGroupName { get; set; }
        [DataMember]
        public string sWeb { get; set; }
        [DataMember]
        public string userFullName { get; set; }
        [DataMember]
        public string userGroupNameRemove { get; set; }
    }

    [DataContract]
    public class AddGroup
    {
        [DataMember]
        public string NamaGroupOld { get; set; }
        [DataMember]
        public string NamaGroup { get; set; }
        [DataMember]
        public string DescGroup { get; set; }
        [DataMember]
        public string Type { get; set; }
        
    }

    [DataContract]
    public class UpdateGroup
    {
        [DataMember]
        public string NamaGroupOld { get; set; }
        [DataMember]
        public string NamaGroup { get; set; }
        [DataMember]
        public string DescGroup { get; set; }

    }

    #endregion
}
