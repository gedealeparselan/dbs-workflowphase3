﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using System.Net.Http;
using System.Web.Script.Serialization;
using System.Transactions;

namespace DBS.WebAPI.Models
{
    #region Property
    public class TransactionFDCallbackCompleteModel
    {
        public long? ApproverID { get; set; }
        public long ID { get; set; }

        /// <summary>
        /// Product Details
        /// </summary>
        public ProductModel Product { get; set; }

        /// <summary>
        /// Channel Details
        /// </summary>
        public ChannelModel Channel { get; set; }

        /// <summary>
        /// Others information
        /// </summary>
        public string Others { get; set; }

        /// <summary>
        /// Remarks
        /// </summary>
        public string Remarks { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }

        public bool IsSignatureVerified { get; set; }

        public bool IsDormantAccount { get; set; }

        public bool IsFreezeAccount { get; set; }

        public bool IsLOIAvailable { get; set; }

    }
    public class FDDetailCompleteModel
    {
        public Guid? WorkflowInstanceID { get; set; }
        public long ID { get; set; }
        public long TransactionID { get; set; }
        public string ApplicationID { get; set; }
        public bool IsTopUrgent { get; set; }
        public bool IsTopUrgentChain { get; set; }
        public CustomerModel Customer { get; set; }
        public bool IsNewCustomer { get; set; }
        public ProductModel Product { get; set; }
        public CurrencyModel Currency { get; set; }
        public decimal Amount { get; set; }
        public decimal Rate { get; set; }
        public DateTime ApplicationDate { get; set; }
        public bool IsDraft { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public IList<TransactionDocumentModel> Documents { get; set; }
        public string CreateBy { get; set; }
        public TransactionTypeParameterModel TransactionType { get; set; }
        public int TransactionTypeID { get; set; }
        public string FDAccNumber { get; set; }
        public string FDBankName { get; set; }
        public string CreditAccNumber { get; set; }
        public string DebitAccNumber { get; set; }
        public decimal? InterestRate { get; set; }
        public string Tenor { get; set; }
        public DateTime? ValueDate { get; set; }
        public DateTime? MaturityDate { get; set; }
        public ParsysModel Remarks { get; set; }
        //public string Remarks { get; set; }
        public long? ApproverID { get; set; }
        public ChannelModel Channel { get; set; }
        //add by adi 07/12/2016
        public bool IsCallbackRequired { get; set; }
        //end adi
        public bool IsFrezeAccount { get; set; }
        public bool IsDocumentComplete { get; set; }
        public bool IsDormantAccount { get; set; }
        public bool IsSignatureVerified { get; set; }
        public string BankNameAccNumber { get; set; }
        public string AttachmentRemarks { get; set; }
        public string TransactionRemarks { get; set; }
        public int SourceID { get; set; }
        public string FTPRate { set; get; }
        public bool? DocsComplete { set; get; }
        public decimal? AllInRate { set; get; }
        public int? ApprovalName { set; get; }

    }
    public class TransactionFDCallbackTimeCompleteModel
    {
        public long ID { get; set; }
        public long ApproverID { get; set; }
        public CustomerContactModel Contact { get; set; }
        public DateTime Time { get; set; }
        public bool? IsUTC { get; set; }
        public string Remark { get; set; }

    }
    public class TransactionFDCompleteDetailModel
    {
        public FDDetailCompleteModel Transaction { get; set; }
        public IList<TransactionTimelineModel> Timelines { get; set; }
        public IList<TransactionFDCallbackTimeCompleteModel> Callbacks { get; set; }
        public IList<VerifyModel> Verify { get; set; }

    }
    #endregion
    #region Interface
    public interface IFDCompleteRepository : IDisposable
    {
        bool GetFDCompleteDetails(Guid workflowInstanceId, ref FDDetailCompleteModel output, ref string message);
        bool GetTransactionTimeline(Guid workflowInstanceID, ref IList<TransactionTimelineModel> timelines, ref string message);
        bool GetTransactionCallbackCompleteDetails(Guid instanceID, long approverID, ref TransactionFDCompleteDetailModel output, ref string message);
    }
    #endregion

    #region repository
    public class WorkflowFDCompleteRepository : IFDCompleteRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        #region Andri
        public bool GetFDCompleteDetails(Guid workflowInstanceID, ref FDDetailCompleteModel output, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                context.Transactions.AsNoTracking();
                // get main transaction data

                output = (from a in context.Transactions
                          where a.WorkflowInstanceID.Value.Equals(workflowInstanceID)
                          select new FDDetailCompleteModel
                          {
                              ID = a.TransactionID,
                              WorkflowInstanceID = a.WorkflowInstanceID.Value,
                              ApplicationID = a.ApplicationID,
                              ApproverID = a.ApproverID != null ? a.ApproverID : 0,
                              Amount = a.Amount,
                              DebitAccNumber = a.DebitAccNumber,
                              FDAccNumber = a.FDAccNumber,
                              AttachmentRemarks = a.AttachmentRemarks,
                              //TransactionRemarks = context.ParameterSystems.Where(c => c.ParsysID.Equals(a.Remarks)).Select(c => c.ParameterValue).FirstOrDefault(),
                              TransactionType = new TransactionTypeParameterModel()
                              {
                                  TransTypeID = a.TransactionType.TransTypeID,
                                  TransactionTypeName = a.TransactionType.TransactionType1,
                                  ProductID = a.TransactionType.ProductID
                              },
                              Currency = new CurrencyModel()
                              {
                                  ID = a.Currency.CurrencyID,//updated by dani 20-6-2016
                                  Code = a.Currency.CurrencyCode,//updated by dani 20-6-2016
                                  Description = a.Currency.CurrencyDescription,//updated by dani 20-6-2016
                                  CodeDescription = a.Currency.CurrencyCode + " (" + a.Currency.CurrencyDescription + ")" //updated by dani 20-6-2016
                              },
                              Product = new ProductModel()
                              {
                                  ID = a.Product.ProductID,
                                  Code = a.Product.ProductCode,
                                  Name = a.Product.ProductName,
                                  WorkflowID = a.Product.ProductWorkflow.WorkflowID,
                                  Workflow = a.Product.ProductWorkflow.WorkflowName,
                                  LastModifiedBy = a.Product.UpdateDate == null ? a.Product.CreateBy : a.Product.UpdateBy,
                                  LastModifiedDate = a.Product.UpdateDate == null ? a.Product.CreateDate : a.Product.UpdateDate.Value
                              },
                              Channel = new ChannelModel()
                              {
                                  ID = a.Channel.ChannelID,
                                  Name = a.Channel.ChannelName,
                                  LastModifiedBy = a.Channel.UpdateDate == null ? a.Channel.CreateBy : a.Channel.UpdateBy,
                                  LastModifiedDate = a.Channel.UpdateDate == null ? a.Channel.CreateDate : a.Channel.UpdateDate.Value
                              },
                              Documents = a.TransactionDocuments.Where(x => x.IsDeleted.Equals(false)).Select(x => new TransactionDocumentModel()
                              {
                                  ID = x.TransactionDocumentID,
                                  Type = new DocumentTypeModel()
                                  {
                                      ID = x.DocumentType.DocTypeID,
                                      Name = x.DocumentType.DocTypeName,
                                      Description = x.DocumentType.DocTypeDescription,
                                      LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                      LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                  },
                                  Purpose = new DocumentPurposeModel()
                                  {
                                      ID = x.DocumentPurpose.PurposeID,
                                      Name = x.DocumentPurpose.PurposeName,
                                      Description = x.DocumentPurpose.PurposeDescription,
                                      LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                      LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                  },
                                  FileName = x.Filename,
                                  DocumentPath = x.DocumentPath,
                                  LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                  LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                              }).ToList(),
                              SourceID = a.SourceID.HasValue ? a.SourceID.Value : 0,
                              CreateDate = a.CreateDate,
                              LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                              LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value,
                              IsTopUrgent = a.IsTopUrgent == 1 ? true : false,//updated by dani 20-6-2016
                              IsTopUrgentChain = a.IsTopUrgent == 2 ? true : false,//updated by dani 20-6-2016
                              InterestRate = a.InterestRate, //updated by dani 20-6-2016
                              Tenor = a.Tenor, //updated by dani 20-6-2016
                              ValueDate = a.ValueDate ?? null, //updated by dani 20-6-2016
                              MaturityDate = a.MaturityDate ?? null, //updated by dani 20-6-2016
                              BankNameAccNumber = a.FDBankName,//updated by dani 20-6-2016                              
                              CreditAccNumber = a.CreditAccNumber, //updated by dani 20-6-2016
                              DocsComplete = a.DocsComplete,
                              FTPRate = a.FTPRate,
                              AllInRate = a.AllInRate,
                              ApprovalName = a.FdMatrixDoaID
                          }).SingleOrDefault();

                if (output != null)
                {
                    //updated by dani 20-6-2016 start
                    int IdRemarks = 0;
                    string IdRemarkString = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID)).Select(a => a.Remarks).SingleOrDefault();
                    bool IdRemarksBool = string.IsNullOrEmpty(IdRemarkString) ? false : int.TryParse(IdRemarkString.Trim(), out IdRemarks);
                    if (IdRemarksBool)
                    {
                        output.TransactionRemarks = context.ParameterSystems.Where(a => a.ParsysID.Equals(IdRemarks)).Select(a => a.ParameterValue).SingleOrDefault();
                        output.Remarks = new ParsysModel() { ID = IdRemarks, Name = context.ParameterSystems.Where(a => a.ParsysID.Equals(IdRemarks)).Select(a => a.ParameterValue).SingleOrDefault() };
                    }
                    else
                    {
                        output.TransactionRemarks = "";
                        output.Remarks = new ParsysModel() { ID = IdRemarks, Name = "" };
                    }
                    //updated by dani 20-6-2016 end
                    string cif = context.Transactions.Where(a => a.WorkflowInstanceID.Value.Equals(workflowInstanceID)).Select(a => a.Customer.CIF).FirstOrDefault();
                    ICustomerRepository customerRepo = new CustomerRepository();
                    CustomerModel customer = new CustomerModel();
                    long TransactionID = output.ID;

                    //add by adi
                    var checker = context.TransactionCheckerDatas.Where(x => x.TransactionID.Equals(TransactionID)).Select(x => x).OrderByDescending(b => b.ApproverID).FirstOrDefault();
                    if (checker != null)
                    {
                        output.IsSignatureVerified = checker.IsSignatureVerified;
                        output.IsCallbackRequired = checker.IsCallbackRequired;
                        output.IsDormantAccount = checker.IsDormantAccount;
                    }
                    //end adi

                    bool isUnderlying = context.TransactionUnderlyings.Any(x => x.TransactionID.Equals(TransactionID));
                    if (isUnderlying)
                    {
                        if (customerRepo.GetCustomerByTransaction(TransactionID, ref customer, ref message))
                        {
                            output.Customer = customer;
                        }
                    }
                    else
                    {
                        if (customerRepo.GetCustomerByCIF(cif, ref customer, ref message))
                        {
                            output.Customer = customer;
                        }
                    }
                    customerRepo.Dispose();
                    customer = null;
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }
        public bool GetTransactionTimeline(Guid workflowInstanceID, ref IList<TransactionTimelineModel> timelines, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                //Remark Rizki 2015-04-21, ganti dari View ke SP             
                timelines = (from a in context.SP_GetNintexTaskTimeline(workflowInstanceID)
                             select new TransactionTimelineModel
                             {
                                 ApproverID = a.WorkflowApproverID,
                                 Activity = a.ActivityTitle,
                                 Time = a.ActivityTime,
                                 UserOrGroup = a.UserOrGroup,
                                 IsGroup = a.IsSPGroup.Value,
                                 Outcome = a.Outcome,
                                 DisplayName = a.DisplayName,
                                 Comment = a.Comment
                             }).ToList();

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }
        public bool GetTransactionCallbackCompleteDetails(Guid instanceID, long approverID, ref TransactionFDCompleteDetailModel output, ref string message)
        {
            bool IsSuccess = false;

            try
            {
                var transactionID = context.Transactions
                       .Where(a => a.WorkflowInstanceID.Value.Equals(instanceID))
                       .Select(a => a.TransactionID)
                       .SingleOrDefault();

                var callbackID = context.TransactionCallbacks
                    .Where(a => a.IsUTC == false && a.TransactionID.Equals(transactionID))
                    .OrderByDescending(a => a.TransactionCallbackID)
                    .Select(a => a.TransactionCallbackID)
                    .FirstOrDefault();

                output = (from a in context.Transactions.AsNoTracking()
                          where a.WorkflowInstanceID.Value.Equals(instanceID)
                          select new TransactionFDCompleteDetailModel
                          {
                              Callbacks = (from b in context.TransactionCallbacks
                                           where b.TransactionID.Equals(transactionID)
                                           select new TransactionFDCallbackTimeCompleteModel
                                           {
                                               ID = b.TransactionCallbackID,
                                               ApproverID = b.ApproverID,
                                               Contact = b.TransactionCallbackContacts.Select(y => new CustomerContactModel()
                                               {
                                                   ID = y.CustomerContact.ContactID,
                                                   Name = y.CustomerContact.ContactName,
                                                   PhoneNumber = y.CustomerContact.PhoneNumber,
                                                   DateOfBirth = y.CustomerContact.DateOfBirth,
                                                   IDNumber = y.CustomerContact.IdentificationNumber,
                                               }).FirstOrDefault(),
                                               Time = b.Time,
                                               IsUTC = b.IsUTC,
                                               Remark = b.Remark
                                           }).ToList(),
                              Verify = a.TransactionCallbackCheckers
                                .Select(z => new VerifyModel()
                                {
                                    ID = z.TransactionColumnID,
                                    Name = z.TransactionColumn.ColumnName,
                                    IsVerified = z.IsVerified
                                }).ToList()
                          }).SingleOrDefault();

                // replace verify list while empty
                if (output != null)
                {

                    output.Verify = (from a in context.TransactionColumns
                                     where a.IsDeleted.Equals(false) && a.IsCallBack.Equals(true)
                                     select new VerifyModel()
                                     {
                                         ID = a.TransactionColumnID,
                                         Name = a.ColumnName,
                                         IsVerified = false
                                     }).ToList();


                    var documentCompleteness = (from a in context.TransactionCheckers
                                                join b in context.TransactionColumns
                                                on a.TransactionColumnID equals b.TransactionColumnID
                                                where a.TransactionID.Equals(transactionID) && b.ColumnName.Equals("Document Completeness") && a.IsDeleted == false
                                                select new VerifyModel()
                                                {
                                                    ID = a.TransactionColumnID,
                                                    Name = b.ColumnName,
                                                    IsVerified = a.IsVerified
                                                }).SingleOrDefault();

                    if (documentCompleteness != null)
                    {
                        output.Verify.Add(documentCompleteness);
                    }
                }

                IsSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return IsSuccess;
        }
        #endregion

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {

            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}