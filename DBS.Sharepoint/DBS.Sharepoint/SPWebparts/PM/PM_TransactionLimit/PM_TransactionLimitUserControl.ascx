﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PM_TransactionLimitUserControl.ascx.cs" Inherits="DBS.Sharepoint.SPWebparts.PM.PM_TransactionLimit.PM_TransactionLimitUserControl" %>




<h1 class="header smaller lighter dark">Transaction Limit</h1>

<!-- widget box start -->
<div id="widget-box" class="widget-box">
    <!-- widget header start -->
    <div class="widget-header widget-hea1der-small header-color-dark">
        <h6>Transaction Limit Table</h6>

        <div class="widget-toolbar">
            <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                <i class="blue icon-filter"></i>
                <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: GridProperties().AllowFilter" />
                <span class="lbl"></span>
            </label>
            <a href="#" data-bind="click: GetData" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                <i class="blue icon-refresh"></i>
            </a>
        </div>
    </div>
    <!-- widget header end -->

    <!-- widget body start -->
    <div class="widget-body">
        <!-- widget main start -->
        <div class="widget-main padding-0">
            <!-- widget slim control start -->
            <div class="slim-scroll" data-height="400">
                <!-- widget content start -->
                <div class="content">

                    <!-- table responsive start -->
                    <div class="table-responsive">
                        <div class="dataTables_wrapper" role="grid">
                            <table id="FXComplianceCode-table" class="table table-striped table-bordered table-hover dataTable">
                                <thead>
                                    <tr data-bind="with: GridProperties">
                                        <th style="width: 50px">No.</th>
                                        <th data-bind="click: function () { Sorting('Product.Name'); }, css: GetSortedColumn('Product.Name')">Product</th>
                                        <th data-bind="click: function () { Sorting('MinAmount'); }, css: GetSortedColumn('MinAmount')">Min Amount</th>
                                        <th data-bind="click: function () { Sorting('MaxAmount'); }, css: GetSortedColumn('MaxAmount')">Max Amount</th>
                                        <th data-bind="click: function () { Sorting('LastModifiedBy'); }, css: GetSortedColumn('LastModifiedBy')">Modified By</th>
                                        <th data-bind="click: function () { Sorting('LastModifiedDate'); }, css: GetSortedColumn('LastModifiedDate')">Modified Date</th>
                                    </tr>
                                </thead>
                                <thead data-bind="visible: GridProperties().AllowFilter" class="table-filter">
                                    <tr>
                                        <th class="clear-filter">
                                            <a href="#" data-bind="click: ClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                <i class="green icon-trash"></i>
                                            </a>
                                        </th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterProduct, event: { change: GridProperties().Filter }" /></th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterMinAmount, event: { change: GridProperties().Filter }" /></th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterMaxAmount, event: { change: GridProperties().Filter }" /></th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterModifiedBy, event: { change: GridProperties().Filter }" /></th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.FilterModifiedDate, event: { change: GridProperties().Filter }" /></th>
                                    </tr>
                                </thead>
                                <tbody data-bind="foreach: TransactionLimits, visible: TransactionLimits().length > 0">
                                    <tr data-bind="click: $root.GetSelectedRow" data-toggle="modal">
                                        <td><span data-bind="text: RowID"></span></td>
                                        <td><span data-bind="text: Product.Name"></span></td>
                                        <td><span data-bind="text:formatNumber(MinAmount)"></span></td>
                                        <td><span data-bind="text:formatNumber(MaxAmount)"></span></td>
                                        <td><span data-bind="text: LastModifiedBy"></span></td>
                                        <td><span data-bind="text: $root.LocalDate(LastModifiedDate)"></span></td>
                                    </tr>
                                </tbody>
                                <tbody data-bind="visible: TransactionLimits().length == 0">
                                    <tr>
                                        <td colspan="6" class="text-center">No entries available.
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- table responsive end -->
                </div>
                <!-- widget content end -->
            </div>
            <!-- widget slim control end -->

            <!-- widget footer start -->
            <div class="widget-toolbox padding-8 clearfix">
                <div class="row" data-bind="with: GridProperties">
                    <!-- pagination size start -->
                    <div class="col-sm-6">
                        <div class="dataTables_paginate paging_bootstrap pull-left">
                            Showing
                            <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                            rows of <span data-bind="text: Total"></span>
                            entries
                        </div>
                    </div>
                    <!-- pagination size end -->

                    <!-- pagination page jump start -->
                    <div class="col-sm-3">
                        <div class="dataTables_paginate paging_bootstrap">
                            Page
                            <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                            of <span data-bind="text: TotalPages"></span>
                        </div>
                    </div>
                    <!-- pagination page jump end -->

                    <!-- pagination navigation start -->
                    <div class="col-sm-3">
                        <div class="dataTables_paginate paging_bootstrap">
                            <ul class="pagination">
                                <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                        <i class="icon-double-angle-left"></i>
                                    </a>
                                </li>
                                <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                        <i class="icon-angle-left"></i>
                                    </a>
                                </li>
                                <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                        <i class="icon-angle-right"></i>
                                    </a>
                                </li>
                                <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                        <i class="icon-double-angle-right"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- pagination navigation end -->

                </div>
            </div>
            <!-- widget footer end -->

        </div>
        <!-- widget main end -->
    </div>
    <!-- widget body end -->
</div>
<!-- widget box end -->

<div data-bind="if: $root.IsRoleMaker()">
    <h4 class="pink">
        <i class="icon-hand-right green"></i>
        <a href="#modal-form" role="button" class="blue" data-toggle="modal" data-bind="click: NewData">Transaction Limit</a>
    </h4>
</div>

<!-- modal form start -->
<div id="modal-form" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- modal header start -->
            <div class="modal-header">
                <h4 class="blue bigger">
                    <span data-bind="if: $root.IsNewData()">Create a new Transaction Limit</span>
                    <span data-bind="if: !$root.IsNewData()">Modify a Transaction Limit</span>
                </h4>
            </div>
            <!-- modal header end -->

            <!-- modal body start -->
            <div class="modal-body overflow-visible">
                <div class="row">
                    <div class="col-xs-12">
                        <!-- modal body form start -->
                        <div id="customer-form" class="form-horizontal" role="form">
                            <div class="space-4"></div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="product">
                                    Product</label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
                                    <select id="Product" name="Product" class="valid" data-bind="value: $root.Product().ID, options: ddlProduct, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Please Select...', event: { change: $root.OnChangeProduct }, disable: $root.Readonly"></select>
                                    </div>
                                </div>
                            </div>
                            <div class="space-4"></div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="MinAmount">
                                    Min Amount</label>

                                <div class="col-sm-9">
                                    <div class="clearfix">
                                        <input type="text" id="MinAmount" name="MinAmount" data-bind="value: $root.MinAmount, disable: $root.Readonly" class="col-xs-12" data-rule-required="true" data-rule-maxlength="255" />
                                        <label class="control-label bolder text-danger" for="MinAmount">*</label>
                                    </div>
                                </div>
                            </div>
                            <div class="space-4"></div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="MaxAmount">
                                    Maxn Amount</label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
									
									   <input type="text" id="MaxAmount" name="MaxAmount" data-bind="value: $root.MaxAmount, disable: $root.Readonly" class="col-xs-12" data-rule-required="true" data-rule-maxlength="255" />
                                        <label class="control-label bolder text-danger" for="MaxAmount">*</label>
									
                                    </div>
                                </div>
                            </div>
                            <div class="space-4"></div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="Currency">
                                  Currency
                                </label>
                                <div class="col-sm-9">
                                    <div class="clearfix">
									<select id="Currency" name="Currency" class="valid" data-bind="value: $root.Currency().ID, options: ddlCurrency, optionsText: 'Code', optionsValue: 'ID', optionsCaption: 'Please Select...', event: { change: $root.OnChangeCurrency }, disable: $root.Readonly"></select>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <!-- modal body form end -->
                    </div>
                </div>
            </div>
            <!-- modal body end -->

            <!-- modal footer start -->
            <div class="modal-footer">
                <button class="btn btn-sm btn-primary" data-bind="click: $root.save, visible: $root.IsNewData() && $root.IsRoleMaker()">
                    <i class="icon-save"></i>
                    Save
                </button>
                <button class="btn btn-sm btn-primary" data-bind="click: $root.update, visible: !$root.IsNewData() && $root.IsRoleMaker()">
                    <i class="icon-edit"></i>
                    Update
                </button>
                <button class="btn btn-sm btn-warning" data-bind="click: $root.delete, visible: !$root.IsNewData() && $root.IsRoleMaker()">
                    <i class="icon-trash"></i>
                    Delete
                </button>
                <button class="btn btn-sm" data-dismiss="modal">
                    <i class="icon-remove"></i>
                    Close
                </button>
            </div>
            <!-- modal footer end -->

        </div>
    </div>
</div>
<!-- modal form end -->

<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/additional-methods.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/TransactionLimit.js"></script>