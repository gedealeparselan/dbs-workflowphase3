$(document).ready(function(){
	//TIMEOUT //
	$('ul.ace-nav>li>ul.user-menu').prepend('<li><a href="#"><i class="icon-time"></i><span id="TIMEOUT_COUNTER"></span></a></li>');
	api.timeout.interval = setInterval(function(){
		api.timeout.counter++;
		$('#TIMEOUT_COUNTER').html(api.timeout.value - api.timeout.counter);
		if(api.timeout.counter>api.timeout.value){
			window.location.href = '/_layouts/15/SignOut.aspx';
		}
	},1000);
		$.ajaxSetup({
			beforeSend: function(jqXHR,data){
				api.timeout.counter = 0;
			},
			complete: function(jqXHR) {
				api.timeout.counter = 0;
			}
		});

	//console.log('DBS Module started!');
	// AUTO LOAD WORKFLOW FEATURE ON REGISTERED MODULES IN APIConfig.js
	console.log(api.workflow.entity.modules);
	var module = api.workflow.entity.modules[window.location.pathname];
	if(module!=null){
		console.log('Workflow Module started...');
		// widget loader
		$box = $('#widget-box');
		var event;
		$box.trigger(event = $.Event('reload.ace.widget'))
		if (event.isDefaultPrevented()) return
		$box.blur();	

		$.getScript('/SiteAssets/Scripts/System.js',function(){		
			$.getScript('/SiteAssets/Scripts/Models.js',function(){
				$.getScript('/SiteAssets/Scripts/WorkflowEntity.js',function(){	
					if(module.file==''){
						// CONVERT CONFIGURATION INTO DIALOG CONFIGURATION FOR AUTOMATIC LOAD MODULES 
						var dialogs = new Array();
						$.each(api.workflow.entity.modules,function(index,item){
							name = item.url.substr(item.url.lastIndexOf('/') + 1);

							if(name!='')
								if(dialogs[name]==null){
									dialogs[name] = {file : item.file};
								}
						});

						module.dialogs = dialogs;
						module.columns = {sort : '', filter : '', row : '', fields : []};
						new WorkflowEntityModel(module);
					}else{
						function LoadModule(){
							$.getScript(module.file,function(){
								var tableColumns = GetMainTableItems(); // Get fields from main table to add on workflow table
					
								var viewModel = new ViewModel();
								// ADD Readonly observable property if not exitst
					
								ko.applyBindings(viewModel);
								
								// Token Validation
								if($.cookie(api.cookie.name) == undefined){
									Helper.Token.Request(function(data, textStatus, jqXHR){
									// store token on browser cookies
									$.cookie(api.cookie.name, data.AccessToken);
									accessToken = $.cookie(api.cookie.name);

									// call spuser function
									GetCurrentUser();

									// call get data inside view model
									if(viewModel.GetDropdown!=null)viewModel.GetDropdown();
									if(viewModel.GetParameters!=null)viewModel.GetParameters();
									viewModel.GetData();
									if(!(module.disable!=null&&module.disable==true)){
										module.view = viewModel; // set current view to config
										new WorkflowEntityModel(module);
									}										
								}, TokenOnError);
								}else{
									// read token from cookie
									accessToken = $.cookie(api.cookie.name);
									// call spuser function
									GetCurrentUser(viewModel);
									// call get data inside view model
									if(viewModel.GetParameters!=null)viewModel.GetParameters();
									if(viewModel.GetDropdown!=null)viewModel.GetDropdown();
									if(viewModel.GetData!=null)viewModel.GetData();
									
									if(!(module.disable!=null&&module.disable==true)){
										module.view = viewModel; // set current view to config
										new WorkflowEntityModel(module);
									}
								}
							});
						}
						var dialog = $('#modal-form > .modal-dialog > .modal-content > .modal-body');
						console.log(dialog);
						if(dialog.length>0){
							dialog.load('/Pages/Dialogs/' + module.url.substr(module.url.lastIndexOf('/') + 1) + '.html',function(){
								$('button',$('#modal-form')).each(function(){
									b = $(this).attr('data-bind');
									if(b)$(this).attr('data-bind',b+'&& !$root.Readonly()');
								});
								module.columns = GetMainTableItems();
								LoadModule();
							});
						}else{ // SINGLE VALUE
							module.columns = {
								sort : '<th>Value</th>', 
								filter : '<th></th>', 
								row : '<td><span data-bind="text: Value"></span></td>', 
								fields : ['Value']};
							LoadModule();
						}
					}
				});				
			});
		});	
	}
});