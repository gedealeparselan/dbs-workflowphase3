﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Web;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using System.Data.Entity.Validation;
using System.Diagnostics;


namespace DBS.WebAPI.Models
{
    #region property
    [ModelName("MatrixDOAParameter")]
    public class MatrixDOAParameterModel
    {
        /// <summary>
        /// Exception Handling Parameter ID
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// MatrixDOAProduct
        /// </summary>
        public int ProductID { get; set; }



        /// <summary>
        /// Exception Handling Parameter Code
        /// </summary>
        public string Code { get; set; }


        /// <summary>
        /// Exception Handling Parameter Name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Matrix DOA Name
        /// </summary>
        public string DataType { get; set; }


        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>

        public string LastModifiedBy { get; set; }
    }

    public class MatrixDOAParameterRow : MatrixDOAParameterModel
    {
        public int RowID { get; set; }
    }

    public class MatrixDOAParameterGrid : Grid
    {
        public IList<MatrixDOAParameterRow> Rows { get; set; }
    }
    #endregion

    #region Filter

    public class MatrixDOAParameterFilter : Filter { }
    #endregion

    #region Interface
    public interface IMatrixDOAParameterRepository : IDisposable
    {
        bool GetMatrixDOAParameter(ref IList<MatrixDOAParameterModel> MatrixDOAparameter, ref string message);
    }
    #endregion

    #region Repository
    public class MatrixDOAParameterRepository : IMatrixDOAParameterRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();
        public bool GetMatrixDOAParameter(ref IList<MatrixDOAParameterModel> MatrixDOAparameter, ref string message)
        {
            bool isSuccess = false;
            try
            {

                using (DBSEntities context = new DBSEntities())
                {
                    MatrixDOAparameter = (from a in context.GeneralMatrixFields
                                                  orderby new { a.FieldName }
                                                  select new MatrixDOAParameterModel
                                                  {
                                                      ID = a.MatrixFieldID,
                                                      
                                                      Code = a.FieldCode,
                                                      Name = a.FieldName,
                                                      DataType = a.FieldDataType,
                                                      LastModifiedBy = a.CreateBy,
                                                      LastModifiedDate = a.CreateDate
                                                  }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;

        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        currentUser = null;
                        context.Dispose();
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}