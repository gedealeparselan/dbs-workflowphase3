﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TBOToolTMOWPUserControl.ascx.cs" Inherits="DBS.Sharepoint.SPWebparts.Others.TBOToolTMOWP.TBOToolTMOWPUserControl" %>


<div id="gridTBOTMOList">
    <h1 class="header smaller lighter dark">TBO Tool</h1>
    <!-- widget box start -->
    <div id="widget-box" class="widget-box">
        <!-- widget header start -->
        <div class="widget-header widget-hea1der-small header-color-dark">
            <h6>TBO TMO</h6>

            <div class="widget-toolbar">
                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                    <i class="blue icon-filter"></i>
                    <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: GridProperties().AllowFilter" />
                    <span class="lbl"></span>
                </label>
                <a href="#" data-bind="click: GetData" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                    <i class="blue icon-refresh"></i>
                </a>
            </div>
        </div>
        <!-- widget header end -->

        <!-- widget body start -->
        <div class="widget-body">
            <!-- widget main start -->
            <div class="widget-main padding-0">
                <!-- widget slim control start -->
                <div class="slim-scroll" data-height="400">
                    <!-- widget content start -->
                    <div class="content">

                        <!-- table responsive start -->
                        <div class="table-responsive">
                            <div class="dataTables_wrapper" role="grid">
                                <table id="Tbo-tool" class="table table-striped table-bordered table-hover dataTable">
                                    <thead>
                                        <tr data-bind="with: GridProperties">
                                            <th style="width: 50px">No.</th>
                                            <th data-bind="click: function () { Sorting('Name'); }, css: GetSortedColumn('Name')">Customer Name</th>
                                            <th data-bind="click: function () { Sorting('CIF'); }, css: GetSortedColumn('CIF')">CIF</th>
                                            <th data-bind="click: function () { Sorting('TotalTBODocument'); }, css: GetSortedColumn('TotalTBODocument')">Total TBO Document</th>
                                            <th data-bind="click: function () { Sorting('LastModifiedBy'); }, css: GetSortedColumn('LastModifiedBy')">Last Modified By</th>
                                            <th data-bind="click: function () { Sorting('LastModifiedDate'); }, css: GetSortedColumn('LastModifiedDate')">Last Modified Date</th>
                                        </tr>
                                    </thead>
                                    <thead data-bind="visible: GridProperties().AllowFilter" class="table-filter">
                                        <tr>
                                            <th class="clear-filter">
                                                <a href="#" data-bind="click: ClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                    <i class="green icon-trash"></i>
                                                </a>
                                            </th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCustomerName, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCif, event: { change: GridProperties().Filter }" /></th>                                            
                                            <th></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterLastModifiedBy, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.FilterLastModifiedDate, event: { change: GridProperties().Filter }" /></th>
                                        </tr>
                                    </thead>
                                    <tbody data-bind="foreach: TBOTools, visible: TBOTools != null">
                                        <tr data-bind="click: $root.GetSelectedRow" data-toggle="modal">
                                            <td><span data-bind="text: $index() + 1"></span></td>
                                            <td><span data-bind="text: Name"></span></td>
                                            <td><span data-bind="text: CIF"></span></td>
                                            <td><span data-bind="text: DocumentCount"></span></td>
                                            <td><span data-bind="text: LastModifiedBy"></span></td>
                                            <td><span data-bind="text: $root.LocalDate(LastModifiedDate)"></span></td>
                                        </tr>
                                    </tbody>
                                    <tbody data-bind="visible: TBOTools == null">
                                        <tr>
                                            <td colspan="9" class="text-center">No entries available.
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- table responsive end -->
                    </div>
                    <!-- widget content end -->
                </div>
                <!-- widget slim control end -->

                <!-- widget footer start -->
                <div class="widget-toolbox padding-8 clearfix">
                    <div class="row" data-bind="with: GridProperties">
                        <!-- pagination size start -->
                        <div class="col-sm-6">
                            <div class="dataTables_paginate paging_bootstrap pull-left">
                                Showing
                                <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                rows of <span data-bind="text: Total"></span>
                                entries
                            </div>
                        </div>
                        <!-- pagination size end -->

                        <!-- pagination page jump start -->
                        <div class="col-sm-3">
                            <div class="dataTables_paginate paging_bootstrap">
                                Page
                                <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                of <span data-bind="text: TotalPages"></span>
                            </div>
                        </div>
                        <!-- pagination page jump end -->

                        <!-- pagination navigation start -->
                        <div class="col-sm-3">
                            <div class="dataTables_paginate paging_bootstrap">
                                <ul class="pagination">
                                    <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                            <i class="icon-double-angle-left"></i>
                                        </a>
                                    </li>
                                    <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                            <i class="icon-angle-left"></i>
                                        </a>
                                    </li>
                                    <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                            <i class="icon-angle-right"></i>
                                        </a>
                                    </li>
                                    <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                            <i class="icon-double-angle-right"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- pagination navigation end -->
                    </div>
                </div>
                <!-- widget footer end -->
            </div>
            <!-- widget main end -->
        </div>
        <!-- widget body end -->
    </div>
    <!-- widget box end -->
</div>

<div class="modal-body overflow-visible" id="newtransaction-tbotmo-form" style="display: none">
        <div class="row">
            <div class="col-xs-12">
                <div id="transaction-tbotmo-data">
					<div>
			<!-- modal header start -->
			<div>
                <h1 class="header smaller lighter dark">To Be Obtained</h1>
            </div>
			<!-- modal header end -->
				<!-- modal body start -->
		<div class="col-xs-12">
            <!-- modal body form start -->
			   <div class="form-group">
                    <label class="col-sm-3 control-label bolder no-padding-right">
                        <span data-bind="text: $root.LocalDate($root.DateNow, true)"></span>
                    </label>

                    <label class="col-sm-4 pull-right">
                        <b>User Name</b> : <span data-bind="text: $root.LoginName"></span>
                    </label>
                </div>
				
            <div id="customer-form" class="form-horizontal" role="form">
                <div class="space-4"></div>
                <div class="form-group">
                    <label class="col-sm-2 control-label no-padding-right" for="CustomerName">
                        Customer Name
                    </label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <input type="text" id="CustomerName" name="CustomerName" data-bind="value: $root.Name, disable: true" class="col-xs-10 col-sm-7" data-rule-required="true" />
                        </div>
                    </div>
                </div>
				
				<div class="form-group">
                    <label class="col-sm-2 control-label no-padding-right" for="Cif">
                        CIF
                    </label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <input type="text" id="Cif" name="Cif" data-bind="value: $root.Cif, disable: true" class="col-xs-10 col-sm-6" data-rule-required="true" />
                        </div>
                    </div>
                </div>
               
                <div class="space-4"></div>
				
				 <div class="padding-4">
            <h3 class="header smaller lighter dark">Attachment</h3>
        <div class="widget-header widget-hea1der-small header-color-dark">
            <h6>Attachment</h6>
        </div>
			<div class="table-responsive">
            <div class="dataTables_wrapper" role="grid">
                <table class="table table-striped table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>ApplicationID</th>
                            <th>TBO Type</th>
                            <th>Expected Submission Date</th>
                            <th>Actual Submission Date</th>
							<th>File Name</th>
							<th>Purpose of Doc</th>
							<th>Type of Doc</th>
							<th>LastModified Date</th>
							<th>Status</th>
							<th>Check</th>
                        </tr>
                    </thead>
                    <tbody data-bind="foreach: $root.DocumentTBO(), visible: $root.DocumentTBO().length > 0">
                        <tr>
                            <td><span data-bind="text: $index() + 1"></span></td>
							<td><span data-bind="text: ApplicationID"></span></td>
							<td><span data-bind="text: Name"></span></td>
							<td><span data-bind="text: $root.LocalDate(ExptDate, true)"></span></td>
							<td><span data-bind="text: ActDate"></span></td>
							<td><span data-bind="text: FileName.name"></span></td>							
							<td><span data-bind="text: PurposeofDoc.Name"></span></td>
							<td><span data-bind="text: TypeofDoc.Name"></span></td>
							<td><span data-bind="text: Modified"></span></td>
							<td><span data-bind="text: Status"></span></td>
							<td align="center">
                                        <div">                                         
                                            <button class="btn-link" data-bind="click: function (data) { $root.GetSelectedRowTBOData($index(), $data) }, disable: IsDisableTBO">
                                       <i class="icon-edit"></i>
                                    </button>
                            </td>
                        </tr>
                    </tbody>
                    <tbody data-bind="visible: $root.DocumentTBO().length == 0">
                        <tr>
                            <td colspan="13" class="text-center">
                                No entries available.
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
		 </div>
       </div>
	   
	    <div class="space-4"></div>
        <div class="row">
            <div class="col-sm-12 align-right">
                <button class="btn btn-sm btn-primary" data-bind="click: $root.save, visible: $root.IsMessage()"><i class="icon-approve"></i>Submit</button> 
                <button class="btn btn-sm" data-bind="click: $root.close" data-dismiss="modal">
                    <i class="icon-remove"></i>
                    Close
                </button>
            </div>
        </div>
	   
	   <div class="space-4"></div>
	   <div class="space-4"></div>	   
	   
	   <div id="transaction-tbotmo-data-wf">
	       <div id="widget-box" class="widget-box">
	           <div class="widget-header widget-hea1der-small header-color-dark">
            <h6>Workflow</h6>

            <div class="widget-toolbar">
                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                    <i class="blue icon-filter"></i>
                    <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: GridPropertiesWF().AllowFilter" />
                    <span class="lbl"></span>
                </label>
                <a href="#" data-bind="click: $root.GetDataWF" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                    <i class="blue icon-refresh"></i>
                </a>
            </div>
        </div>
	   <div class="widget-body">
            <!-- widget main start -->
            <div class="widget-main padding-0">
                <!-- widget slim control start -->
                <div class="slim-scroll" data-height="200">
                    <!-- widget content start -->
                    <div class="content">

                        <!-- table responsive start -->
                        <div class="table-responsive">
                            <div class="dataTables_wrapper" role="grid">
                                <table id="Tbo-tool" class="table table-striped table-bordered table-hover dataTable">
                                    <thead>
                                        <tr data-bind="with: GridPropertiesWF">
                                            <th style="width: 50px">No.</th>
											<th data-bind="click: function () { Sorting('CustomerName'); }, css: GetSortedColumn('CustomerName')">Customer Name</th>
                                            <th data-bind="click: function () { Sorting('ApplicationID'); }, css: GetSortedColumn('ApplicationID')">CIF</th>
                                            <th data-bind="click: function () { Sorting('Status'); }, css: GetSortedColumn('Status')">Status</th>
                                            <th data-bind="click: function () { Sorting('LastModifiedDate'); }, css: GetSortedColumn('LastModifiedDate')">LastModified Date</th>
                                        </tr>
                                    </thead>
                                    <thead data-bind="visible: GridPropertiesWF().AllowFilter" class="table-filter">
                                        <tr>
                                            <th class="clear-filter">
                                                <a href="#" data-bind="click: ClearFiltersWF" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                    <i class="green icon-trash"></i>
                                                </a>
                                            </th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterApplicationID, event: { change: GridPropertiesWF().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCustomerName, event: { change: GridPropertiesWF().Filter }" /></th>                                            
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterStatus, event: { change: GridPropertiesWF().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.FilterLastModifiedDate, event: { change: GridPropertiesWF().Filter }" /></th>
                                        </tr>
                                    </thead>
                                    <tbody data-bind="foreach: $root.TBOToolsWF(), visible: $root.TBOToolsWF().length > 0">
										<tr data-bind="click: $root.GetSelectedRowWF($data)" data-toggle="modal">
											<td><span data-bind="text: $index() + 1"></span></td>
											<td><span data-bind="text: CustomerName + ' - ' + TBOApplicationID "></span></td>
											<td><span data-bind="text: CIF"></span></td>
											<td><span data-bind="text: Status"></span></td>
											<td><span data-bind="text: $root.LocalDate(LastModifiedDate, true)"></span></td>
										</tr>
                                    </tbody>
                                    <tbody data-bind="visible: $root.TBOToolsWF().length == 0">
                                        <tr>
                                            <td colspan="9" class="text-center">No entries available.
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- table responsive end -->
                    </div>
                    <!-- widget content end -->
                </div>
                <!-- widget slim control end -->

                <!-- widget footer start -->
                <div class="widget-toolbox padding-8 clearfix">
                    <div class="row" data-bind="with: GridPropertiesWF">
                        <!-- pagination size start -->
                        <div class="col-sm-6">
                            <div class="dataTables_paginate paging_bootstrap pull-left">
                                Showing
                                <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                rows of <span data-bind="text: Total"></span>
                                entries
                            </div>
                        </div>
                        <!-- pagination size end -->

                        <!-- pagination page jump start -->
                        <div class="col-sm-3">
                            <div class="dataTables_paginate paging_bootstrap">
                                Page
                                <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                of <span data-bind="text: TotalPages"></span>
                            </div>
                        </div>
                        <!-- pagination page jump end -->

                        <!-- pagination navigation start -->
                        <div class="col-sm-3">
                            <div class="dataTables_paginate paging_bootstrap">
                                <ul class="pagination">
                                    <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                            <i class="icon-double-angle-left"></i>
                                        </a>
                                    </li>
                                    <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                            <i class="icon-angle-left"></i>
                                        </a>
                                    </li>
                                    <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                            <i class="icon-angle-right"></i>
                                        </a>
                                    </li>
                                    <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                            <i class="icon-double-angle-right"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- pagination navigation end -->
                    </div>
                </div>
                <!-- widget footer end -->
            </div>
            <!-- widget main end -->
        </div>
	   </div>
	   </div>	
            </div>
            <!-- modal body form end -->
		</div>

            <!-- modal body form end -->
					</div>
				</div>
            </div>
        </div>
</div>

<div id="tbotmo-doc-detail" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">
        <div class="modal-dialog" style="height: 100%; width: 70%;" data-bind="with: $root">
            <div id="backDrop2" class="absCustomBackDrop topCustomBackDrop darkCustomBackDrop hideCustomBackDrop fullHeightCustomBackDrop fullWidthCustomBackDrop centerCustomBackDrop"></div>
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="blue bigger">
                        <span data-bind="if: $root.IsNewTBO()">Add TBO Document</span>
                        <span data-bind="if: !$root.IsNewTBO()">Edit TBO Document</span>
                    </h4>
                </div>
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="tbo-form" class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label bolder no-padding-right" for="tbotype">TBO Type</label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" class="col-sm-6 autocomplete" disabled="disabled" name="TBOType" data-bind="value: $root.NameSelected()" id="TBOType" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label bolder no-padding-right" for="expdate">Expected Submission Date</label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" autocomplete="off" class="col-sm-3" name="expdate" id="expdate" data-bind="value: $root.ExpectedSUbDate()" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label bolder no-padding-right" for="document-type">Document Type</label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="document-type-tbo" name="document-type-tbo" data-rule-value="true" data-rule-required="true" data-bind="options: $root.ParameterTBO().DocumentTypes, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Options..', value: $root.SelectedTBO().DocumentType"></select>
                                        <label class="control-label bolder text-danger" for="document-type">*</label>
										</div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label bolder no-padding-right" for="document-purpose">Purpose of Docs</label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="document-purpose-tbo" name="document-purpose-tbo" data-rule-required="true" data-rule-value="true" data-bind="options: $root.ParameterTBO().DocumentPurposes, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Options..', value: $root.SelectedTBO().DocumentPurpose"></select>
                                           <label class="control-label bolder text-danger" for="document-purpose">*</label>
										</div>
                                    </div>
                                </div>
								
							   <div class="form-group">
                                    <label class="col-sm-3 control-label bolder no-padding-right" for="actdate">Actual Submission Date</label>
                                           <div class="col-sm-2">
                                                <div class="clearfix input-group col-sm-12 no-padding">
                                                <input class="form-control date-picker" type="text" autocomplete="off" data-date-format="dd-M-yyyy" data-rule-required="true" onchange="formatCheckerDateApplicationTBO('ActualSubmissionDate')" id="application-date-actsubmissiondate" name="application-date-actsubmissiondate" data-rule-required="true" data-rule-date="true" data-bind="datepicker: true, value: $root.ActSubmissionDate()">
                                                <span class="input-group-addon">
                                                    <i class="icon-calendar bigger-110"></i>
                                                </span>
												<label class="control-label bolder text-danger" for="actdate">*</label>
                                        </div>
                                      </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label bolder no-padding-right" for="document-path">Document</label>

                                    <div class="col-sm-6">
                                        <div class="clearfix">
                                            <input type="file" id="document-path-tbo" data-rule-required="true" name="document-path-tbo" data-bind="file: DocumentPath" />											
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.AddDocument, visible: $root.IsNewTBO()">
                        Save
                    </button>
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.AddDocument, visible: !$root.IsNewTBO()">
                        <i class="icon-edit"></i>
                        Update
                    </button>
                    <button class="btn btn-sm" data-bind="click: $root.CloseTBODetail">
                        <i class="icon-remove"></i>
                        Close
                    </button>
                </div>
            </div>
        </div>
    </div>
	
<div id="modaltbotmo-form" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" style="width:100%;">
            <div id="backDrop" class="absCustomBackDrop topCustomBackDrop darkCustomBackDrop hideCustomBackDrop fullHeightCustomBackDrop fullWidthCustomBackDrop centerCustomBackDrop"></div>
            <div class="modal-content">
                <!-- modal header end -->
                <!-- modal body start -->
                <div class="modal-body overflow-visible">  
					<button type="button" class="bootbox-close-button close" data-dismiss="modal" style="margin-top: -10px;font-size:30px;margin-right: -8px;" data-bind:"$root.OnCloseApproval">×</button>
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- modal body form start -->
                            <div class="tabbable">
                                <ul class="nav nav-tabs" id="myTab">
                                    <li class="active">
                                        <a data-toggle="tab" href="#transactiontbo">
                                            <i class="blue icon-credit-card bigger-110"></i>
											PPU Checker TBO Approval Task
                                        </a>
                                    </li>
                                    <li>
                                        <a data-toggle="tab" href="#history">
                                            <i class="blue icon-comments bigger-110"></i>
                                            Workflow History
                                        </a>
                                    </li>
                                </ul>

                                <div class="tab-content">
<div id="transactiontbo" class="tab-pane in active">
	    <div id="transactiontmo-progress" class="col-sm-4 col-sm-offset-4" style="display: none">
                                     <h4 class="smaller blue">Please wait...</h4>
                                        <span class="space-4"></span>
                                            <div class="progress progress-small progress-striped active">
                                      <div class="progress-bar progress-bar-info" style="width:100%"></div>
               </div>
        </div>
		<div id="tbotmo-form-wf" class="form-horizontal" role="form">										
					<div class="form-group">
                    <label class="col-sm-3 control-label bolder no-padding-right">
                        <span data-bind="text: $root.LocalDate($root.LastModifiedDate, true)"></span>
                    </label>

                    <label class="col-sm-4 pull-right">
                        <b>User Name</b> : <span data-bind="text: $root.LastModifiedBy"></span>
                    </label>
                </div>
										 <div class="space-4"></div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="CustomerName">
                        Customer Name
                    </label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <input type="text" id="CustomerName" name="CustomerName" data-bind="value: $root.NameWF, disable: true" class="col-xs-10 col-sm-5" data-rule-required="true" />
                        </div>
                    </div>
                </div>
				
				<div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="Cif">
                        CIF
                    </label>
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <input type="text" id="Cif" name="Cif" data-bind="value: $root.CifWF, disable: true" class="col-xs-10 col-sm-4" data-rule-required="true" />
                        </div>
                    </div>
                </div>
               
                <div class="space-2"></div>
				
									    <div class="modal-body overflow-visible">
											<div class="row">
												<div class="col-xs-12">
												<div id="tbo-form" class="form-horizontal" role="form">
													<div class="padding-4">
										<h3 class="header smaller lighter dark">Attachment</h3>
										<div data-bind="visible: $root.IsMessage">
										<h5><b style='color:#FF0000;'><span data-bind="text: $root.Message"></span></b></h5>
										</div>
			<div class="form-group form-line no-margin-bottom no-padding-bottom">
                <label class="col-sm-3 control-label bolder no-padding-right" for="check-all"></label>

                <label class="col-sm-8 control-label bolder no-padding-right" for="product-code">Check All</label>

                <label class="col-sm-1">
					<input id="check-all" class="ace ace-switch ace-switch-6" type="checkbox" data-bind="checked: $root.CheckAll, event: { change: $root.CheckAllData('TBO') }, disable: $root.IsMessage">					
					<span class="lbl"></span>
                </label>
            </div>
			<div class="widget-header widget-hea1der-small header-color-dark">
            <h6>Attachment</h6>
			</div>
				<div class="table-responsive">
				<div class="dataTables_wrapper" role="grid">
                <table class="table table-striped table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>ApplicationID</th>
							<th>Customer Name</th>
                            <th>TBO Type</th>
                            <th>Expected Submission Date</th>
                            <th>Actual Submission Date</th>
							<th>File Name</th>
							<th>Purpose of Doc</th>
							<th>Type of Doc</th>
							<th>LastModified Date</th>
							<th>Status</th>
							<th>Check</th>
                        </tr>
                    </thead>
                    <tbody data-bind="foreach: $root.DocumentTBOWFAfterSubmit(), visible: $root.DocumentTBOWFAfterSubmit().length > 0" id="TabelTBO">
                        <tr>
                            <td><span data-bind="text: $index() + 1"></span></td>
							<td><span data-bind="text: ApplicationID"></span></td>
							<td><span data-bind="text: CustomerName"></span></td>
							<td><span data-bind="text: Name"></span></td>
							<td><span data-bind="text: $root.LocalDate(ExptDate, true)"></span></td>
							<td><span data-bind="text: $root.LocalDate(ActDate, true)"></span></td>
							<td><span data-bind="text: FileName.name"></span></td>							
							<td><span data-bind="text: PurposeofDoc.Name"></span></td>
							<td><span data-bind="text: TypeofDoc.Name"></span></td>
							<td><span data-bind="text: $root.LocalDate(Modified, true)"></span></td>
							<td><span data-bind="text: Status"></span></td>
							<td align="center">                        
								 <div class="col-xs-1">
									<label>
									<input class="ace ace-switch ace-switch-6" type="checkbox" data-bind="checked: IsChecklistSubmit, click: function () { $root.SetChecklistSubmit($index(), $data) }, disable: $root.IsMessage">								
									<span class="lbl"></span>
									</label>
								</div>
                            </td>
                        </tr>
                    </tbody>
                    <tbody data-bind="visible: $root.DocumentTBOWFAfterSubmit().length == 0">
                        <tr>
                            <td colspan="13" class="text-center">
                                No entries available.
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
		 </div>
		                 <div class="modal-footer">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-3 control-label bolder no-padding-right" for="workflow-comments">Comments</label>

                            <div class="col-xs-9 align-left">
                                <div class="clearfix">
                                    <textarea class="col-lg-8" id="workflow-comments" name="workflow-comments" rows="4" data-bind="value: $root.Comments" placeholder="Type any comment here..."></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="space-8"></div>

                    <!-- dynamic outcomes button start -->
                        	<button class="btn btn-sm btn-primary" data-bind="click: $root.savewf, visible: !$root.IsMessage()">
							<i class="icon-approve"></i>
							Submit
							</button>	

                    <!-- dynamic outcomes button end -->
                    <button class="btn btn-sm" data-bind="click: $root.CloseTBOWFDetail">
                        <i class="icon-remove"></i>
                        Close
                    </button>
                </div>
       </div>
							</div> 
							</div> 
						</div> 
					</div>                                 
             </div>
</div>
									<div id="history" class="tab-pane">
									 <div id="timeline-2" data-bind="with: $root.TransactionTBOModelTimeLine()">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                                                    <div class="timeline-container timeline-style2">
                                                        <!--<span class="timeline-label">
                                                            <b>Today</b>
                                                        </span>-->

                                                        <div data-bind="if: $root.TransactionTBOModelTimeLine().length > 0">

                                                            <div class="timeline-items" data-bind="foreach: $root.TransactionTBOModelTimeLine()">
                                                                <div class="timeline-item clearfix">
                                                                    <div class="timeline-info">
                                                                        <span class="timeline-date" data-bind="text: $root.LocalDate(Time)"></span>

                                                                        <i class="timeline-indicator btn btn-info no-hover"></i>
                                                                    </div>

                                                                    <div class="widget-box transparent">
                                                                        <div class="widget-body">
                                                                            <div class="widget-main no-padding">
                                                                                <span class="bigger-110">
                                                                                    <span class="blue bolder" data-bind="text: DisplayName"></span>
                                                                                    <!-- ko if: IsGroup -->
                                                                                    (<span data-bind="text: UserOrGroup"></span>)
                                                                                    <!-- /ko -->
                                                                                </span>

                                                                                <span class="bolder" data-bind="text: Outcome"></span>
                                                                                <span data-bind="text: Activity"></span>

                                                                                <!-- ko ifnot: ApproverID == 0 -->
                                                                                <br />
                                                                                <i class="icon-comments-alt bigger-110 blue"></i>
                                                                                <span data-bind="text: Comment"></span>
                                                                                <!-- /ko -->
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
														
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
									</div >                                       
                                    </div>
                                </div>
                            </div>
                            <!-- modal body form end -->
                        </div>
                    </div>
                </div>
                <!-- modal body end -->
            </div>
        </div>

<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/Knockout/knockout.mapping-latest.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/DBS.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/Helper.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/TBOTMO.js"></script>

<link rel="stylesheet" media="screen" href="/SiteAssets/Scripts/handsontable/dist/handsontable.full.css">
<script src="/SiteAssets/Scripts/handsontable/dist/handsontable.full.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/parser.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/ruleJS.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/lodash.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/underscore.string.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/moment.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/numeral.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/md5.js"></script>
<script src="/SiteAssets/Scripts/handsontable/ruleJS/jstat.js"></script>