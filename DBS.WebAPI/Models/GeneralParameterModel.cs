﻿using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DBS.WebAPI.Models
{
    public interface IGeneralParameterRepository : IDisposable
    {
        bool GetAllLoanParameterSystem(ref IList<GeneralParameterModel> Param, ref string Message);
        bool GetParameterSystemByKey(string Key, ref IList<GeneralParameterModel> Param, ref string Message);
        bool GetAllParameterSystem(ref IList<GeneralParameterModel> Param, ref string Message);
    }

    public class GeneralParameterRepository : IGeneralParameterRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetAllLoanParameterSystem(ref IList<GeneralParameterModel> Param, ref string Message)
        {
            bool isSuccess = false;
            try
            {
                Param = (from p in context.ParameterSystems
                         select new GeneralParameterModel()
                         {
                             ID = p.ParsysID,
                             Name = p.ParameterValue,
                             StringKey = p.ParameterType
                         }).ToList();
                if (Param != null) isSuccess = true;
                else Message = "Parameter System Not Found";
            }
            catch (Exception ex)
            {
                Message = ex.Message;
            }
            return isSuccess;
        }
    

        public bool GetParameterSystemByKey(string Key, ref IList<GeneralParameterModel> Param, ref string Message)
        {
 	        bool isSuccess = false;
            try
            {
                Param = (from p in context.ParameterSystems
                         where p.ParameterType == Key
                         select new GeneralParameterModel()
                         {
                             ID = p.ParsysID,
                             Name = p.ParameterValue,
                             StringKey = p.ParameterType
                         }).ToList();
                if (Param != null) isSuccess = true;
                else Message = "Parameter System Not Found";
            }
            catch (Exception ex)
            {
                Message = ex.Message;
            }
            return isSuccess;
        }

        public bool GetAllParameterSystem(ref IList<GeneralParameterModel> Param, ref string Message)
        {
            bool isSuccess = false;
            try
            {
                Param = (from p in context.ParameterSystems
                         select new GeneralParameterModel()
                         {
                             ID = p.ParsysID,
                             Name = p.ParameterValue,
                             StringKey = p.ParameterType
                         }).ToList();
                if (Param != null) isSuccess = true;
                else Message = "Parameter System Not Found";
            }
            catch (Exception ex)
            {
                Message = ex.Message;
            }
            return isSuccess;
        }

        public void Dispose()
        {
 	        throw new NotImplementedException();
        }
    }
    [ModelName("GeneralParameterModel")]
    public class GeneralParameterModel
    {
        /// <summary>
        /// ID.
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Code.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Key for String Data Type
        /// </summary>
        public string StringKey { get; set; }
        /// <summary>
        /// Key for Integer Data Type
        /// </summary>
        public int IntKey { get; set; }
    }
}