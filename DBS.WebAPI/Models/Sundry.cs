﻿using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;

namespace DBS.WebAPI.Models
{
    #region property
    [ModelName("Sundry")]
    public class SundryModel
    {
        public int? ID { get; set; }
        public string UnitName { get; set; }
        public string OABAccNo { get; set; }
        public string OABAccName { get; set; }        
        public bool IsDeleted { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }  
    }

    public class SundryRow : SundryModel
    {
        public int RowID { get; set; }
    }

    public class SundryGrid : Grid
    {
        public IList<SundryRow> Rows { get; set; }
    }
    #endregion

    #region filter
    public class SundryFilter : Filter
    {

    }
    #endregion

    #region interface
    public interface ISundryRepository : IDisposable
    {
        bool GetSundryByID(int Id, ref SundryModel Sundry, ref string Message);
        bool GetSundry(ref IList<SundryModel> Sundrys, int Limit, int Index, ref string Message);
        bool GetSundry(ref IList<SundryModel> Sundrys, ref string Message);
        bool GetSundry(int Page, int Size, IList<SundryFilter> Filters, string sortColumn, string sortOrder, ref SundryGrid Sundry, ref string Message);
        bool GetSundry(SundryFilter Filter, ref IList<SundryModel> Sundrys, ref string Message);
        bool GetSundry(string Key, int Limit, ref IList<SundryModel> Sundrys, ref string Message);
        bool AddSundry(SundryModel Sundry, ref string Message);
        bool UpdateSundry(int Id, SundryModel Sundry, ref string Message);
        bool DeleteSundry(int Id, ref string Message);
    }
    #endregion

    #region repository
    public class SundryRepository : ISundryRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();
        public bool GetSundryByID(int id, ref SundryModel sundry, ref string message)
        {
            bool isSuccess = false;
            try
            {
                sundry = (from sun in context.Sundries
                          where sun.IsDeleted.Equals(false) && sun.SundryID.Equals(id)
                          select new SundryModel
                          {
                              ID = sun.SundryID,
                              UnitName = sun.UnitName,
                              OABAccNo = sun.OABAccNo,
                              OABAccName = sun.OABAccName,
                              LastModifiedDate = sun.UpdateDate == null ? sun.CreateDate : sun.UpdateDate.Value,
                              LastModifiedBy = sun.UpdateDate == null ? sun.CreateBy : sun.UpdateBy
                          }).SingleOrDefault();
                isSuccess = true;
            }
            catch(Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool GetSundry(ref IList<SundryModel> sundrys, int limit, int index, ref string message)
        {
            bool isSuccess = false;
            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    sundrys = (from sundry in context.Sundries
                               where sundry.IsDeleted.Equals(false)
                               orderby new { sundry.UnitName, sundry.OABAccNo, sundry.OABAccName }
                               select new SundryModel
                               {
                                   ID = sundry.SundryID,
                                   UnitName = sundry.UnitName,
                                   OABAccNo = sundry.OABAccNo,
                                   OABAccName = sundry.OABAccName,
                                   LastModifiedDate = sundry.UpdateDate == null ? sundry.CreateDate : sundry.UpdateDate.Value,
                                   LastModifiedBy = sundry.UpdateDate == null ? sundry.CreateBy : sundry.UpdateBy
                               }).Skip(skip).Take(limit).ToList();
                }
                isSuccess = true;
            }
            catch(Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool GetSundry(ref IList<SundryModel> sundrys, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    sundrys = (from sundry in context.Sundries
                               where sundry.IsDeleted.Equals(false)
                               orderby new { sundry.UnitName, sundry.OABAccNo, sundry.OABAccName }
                               select new SundryModel
                               {
                                   ID = sundry.SundryID,
                                   UnitName = sundry.UnitName,
                                   OABAccNo = sundry.OABAccNo,
                                   OABAccName = sundry.OABAccName,
                                   LastModifiedDate = sundry.UpdateDate == null ? sundry.CreateDate : sundry.UpdateDate.Value,
                                   LastModifiedBy = sundry.UpdateDate == null ? sundry.CreateBy : sundry.UpdateBy
                               }).ToList();
                }
                isSuccess = true;
            }
            catch(Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool GetSundry(int page, int size, IList<SundryFilter> filters, string sortColumn, string sortOrder, ref SundryGrid sundry, ref string message)
        {
            bool isSuccess = false;
            try
            {
                SundryModel filter = new SundryModel();
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                if (filters != null)
                {
                    filter.UnitName = filters.Where(a => a.Field.Equals("UnitName")).Select(a => a.Value).SingleOrDefault();
                    filter.OABAccNo = filters.Where(a => a.Field.Equals("OABAccNo")).Select(a => a.Value).SingleOrDefault();
                    filter.OABAccName = filters.Where(a => a.Field.Equals("OABAccName")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }
                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from sun in context.Sundries 
                                join emp in context.Employees on sun.CreateBy equals emp.EmployeeUsername into joinEmp
                                from emp in joinEmp.DefaultIfEmpty()
                                let isUnitName = !string.IsNullOrEmpty(filter.UnitName)
                                let isOABAccNo = !string.IsNullOrEmpty(filter.OABAccNo)
                                let isOABAccName = !string.IsNullOrEmpty(filter.OABAccName)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where sun.IsDeleted.Equals(false)
                                //&& sun.CreateBy.Equals("i:0#.f|dbsmembership|Admin1") &&
                                && (isUnitName ? sun.UnitName.Contains(filter.UnitName) : true)
                                && (isOABAccNo ? sun.OABAccNo.Contains(filter.OABAccNo) : true)
                                && (isOABAccName ? sun.OABAccName.Contains(filter.OABAccName) : true)
                                && (isCreateBy ? (sun.UpdateDate == null ? sun.CreateBy.Contains(filter.LastModifiedBy) : sun.UpdateBy.Contains(filter.LastModifiedBy)) : true)
                                && (isCreateDate ? ((sun.UpdateDate == null ? sun.CreateDate : sun.UpdateDate.Value) > filter.LastModifiedDate.Value && ((sun.UpdateDate == null ? sun.CreateDate : sun.UpdateDate.Value) < maxDate)) : true)

                                
                                select new SundryModel {
                                    ID = sun.SundryID,
                                    UnitName = sun.UnitName,
                                    OABAccNo = sun.OABAccNo,
                                    OABAccName = sun.OABAccName,
                                    LastModifiedDate = sun.UpdateDate == null ? sun.CreateDate : sun.UpdateDate.Value,
                                    LastModifiedBy = (emp.EmployeeUsername == null? sun.CreateBy : emp.EmployeeName)
                                    // LastModifiedBy = sun.UpdateDate == null ? sun.CreateBy : sun.UpdateBy
                                });
                    sundry.Page = page;
                    sundry.Size = size;
                    sundry.Total = data.Count();
                    sundry.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    sundry.Rows = data.OrderBy(orderBy).AsEnumerable().Select((a, i) => new SundryRow {
                        RowID = i +1,
                        ID = a.ID,
                        UnitName = a.UnitName,
                        OABAccNo = a.OABAccNo,
                        OABAccName = a.OABAccName,
                        LastModifiedBy = a.LastModifiedBy,
                        LastModifiedDate = a.LastModifiedDate
                    }).Skip(skip).Take(size).ToList();
                }
                isSuccess = true;
            }
            catch(Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool GetSundry(SundryFilter Filter, ref IList<SundryModel> Sundrys, ref string Message)
        {
            bool isSuccess = false;
            try
            {
                isSuccess = true;
            }
            catch
            {

            }
            return isSuccess;
        }

        public bool GetSundry(string key, int limit, ref IList<SundryModel> sundrys, ref string message)
        {
            bool isSuccess = false;
            try
            {
                sundrys = (from sundry in context.Sundries
                           where sundry.IsDeleted.Equals(false) && (sundry.UnitName.Contains(key) || sundry.OABAccNo.Contains(key) || sundry.OABAccName.Contains(key))
                           orderby new {sundry.UnitName}
                           select new SundryModel {
                                ID = sundry.SundryID,
                                UnitName = sundry.UnitName,
                                OABAccNo = sundry.OABAccNo,
                                OABAccName = sundry.OABAccName,
                                LastModifiedDate = sundry.UpdateDate == null ? sundry.CreateDate : sundry.UpdateDate.Value,
                                LastModifiedBy = sundry.UpdateDate == null ? sundry.CreateBy : sundry.UpdateBy
                           }).Take(limit).ToList();
                isSuccess = true;
            }
            catch(Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool AddSundry(SundryModel sundry, ref string message)
        {
            bool isSuccess = false;
            if (sundry != null)
            {
                try
                {
                    if (!context.Sundries.Where(a => a.UnitName.ToLower() == sundry.UnitName.Trim().ToLower() && a.OABAccNo == sundry.OABAccNo.Trim() && a.IsDeleted.Equals(false)).Any())
                    {
                        context.Sundries.Add(new Entity.Sundry()
                        {
                            UnitName = sundry.UnitName.Trim(),
                            OABAccNo = sundry.OABAccNo.Trim(),
                            OABAccName = sundry.OABAccName.Trim(),
                            IsDeleted = false,
                            CreateDate = DateTime.UtcNow,
                            //CreateBy = currentUser.GetCurrentUser().DisplayName
                            CreateBy = currentUser.GetCurrentUser().LoginName
                        });
                        context.SaveChanges();
                        isSuccess = true;
                    } else {
                        message = string.Format("Sundry data for Unit Name {0}, OABAcc No {1} and OABAcc Name {2}  is already exist.", sundry.UnitName, sundry.OABAccNo, sundry.OABAccName);
                    }                    
                }
                catch(Exception ex)
                {
                    message = ex.Message;
                }
            }
            
            return isSuccess;
        }

        public bool UpdateSundry(int id, SundryModel sundry, ref string message)
        {
            bool isSuccess = false;
            try
            {
                Entity.Sundry data = context.Sundries.Where(a => a.SundryID.Equals(id)).SingleOrDefault();
                if (data != null)
                {
                    if (!context.Sundries.Where(a => a.SundryID != sundry.ID && a.UnitName.ToLower() == sundry.UnitName.Trim().ToLower() && a.OABAccNo == sundry.OABAccNo.Trim() && a.IsDeleted.Equals(false)).Any())
                    {
                        data.UnitName = sundry.UnitName.Trim();
                        data.OABAccNo = sundry.OABAccNo.Trim();
                        data.OABAccName = sundry.OABAccName.Trim();
                        data.UpdateDate = DateTime.UtcNow;
                        //data.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                        data.UpdateBy = currentUser.GetCurrentUser().LoginName;
                        context.SaveChanges();
                        isSuccess = true;
                    }
                    else
                    {
                        message = string.Format("Sundry data for Unit Name {0} and OABAcc No {1} is already exist.", sundry.UnitName, sundry.OABAccNo);
                    }
                }
                else
                {
                    message = string.Format("Sundry data for Sundry ID {0} is does not exist.", id);
                }
            }
            catch(Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }

        public bool DeleteSundry(int id, ref string Message)
        {
            bool isSuccess = false;
            try
            {
                Entity.Sundry data = context.Sundries.Where(a => a.SundryID.Equals(id)).SingleOrDefault();
                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().LoginName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    Message = string.Format("Sundry data for Sundry ID {0} is does not exist.", id);
                }
            }
            catch(Exception ex)
            {
                Message = ex.Message;
            }
            return isSuccess;
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}