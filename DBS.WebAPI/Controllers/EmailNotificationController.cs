﻿using DBS.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class EmailNotificationController : ApiController
    {
        private string message = string.Empty;

        private readonly IEmailAlertRepository repository = null;

        public EmailNotificationController()
        {
            this.repository = new EmailNotificationRepository();
        }

        public EmailNotificationController(IEmailAlertRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get transaction data based on ApplicationID.
        /// </summary>
        /// <param name="query">Search Keyword.</param>
        /// <param name="limit">Total rows will be retreive</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<EmailNotificationModel>))]
        [HttpGet]
        [Route("api/emailnotification/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<EmailNotificationModel> output = new List<EmailNotificationModel>();

            if (!repository.GetEmailNotificationByID(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<EmailNotificationModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Save history of notification alert.
        /// </summary>
        /// <param name="id">ID of Matrix.</param>
        /// <param name="EmailNotification">Notivication data to be saved.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/emailnotification/{id}")]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(EmailNotificationModel EmailNotification, int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.SaveEmailNotification(EmailNotification, id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "Email Notification has been saved." });
                }
            }
        }

        /// <summary>
        /// Get Email properties.
        /// </summary>
        /// <param name="MatrixID">Matrix cases selected</param>
        /// <param name="TransactionID">Transaction ID of notification.</param>
        /// <returns></returns>
        [ResponseType(typeof(EmailProperty))]
        [HttpGet]
        [Route("api/emailnotification/Properties")]
        public HttpResponseMessage Properties(int MatrixID, int TransactionID)
        {
            EmailProperty output = new EmailProperty();

            if (!repository.GetEmailNotificationProperties(MatrixID, TransactionID, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<EmailProperty>(HttpStatusCode.OK, output);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}
