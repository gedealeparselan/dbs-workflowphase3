﻿using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Data;

namespace DBS.WebAPI.Models
{
    #region property
    public class CustomerLoanModel : CustomerModel
    {
        public bool? IsAdhoc { get; set; }
    }
    public class FundingMemoTranModel
    {
        public Nullable<int> InterestFrequencyID { get; set; }
        public BizSegmentModel BizSegment { get; set; }
        public FinacleScheme FinacleScheme { get; set; }
        public string BizName { get; set; }
        public string DoAName { get; set; }
        public ProgramType ProgramType { get; set; }
        public long? CSOFundingMemoID { get; set; }
        public string CodeAmount { get; set; }
        public string DoaName { get; set; }
        public string CIF { get; set; }
        public bool? isAdhoc { get; set; }
        public string CustomerName { get; set; }

        public int? BizSegmentID { get; set; }

        public string BizSegmentName { get; set; }

        public int? LocationID { get; set; }

        public string SolID { get; set; }

        public int? CustomerCategoryID { get; set; }

        public string CustomerCategoryName { get; set; }

        public int? LoanTypeID { get; set; }

        public string LoanTypeName { get; set; }

        public int? ProgramTypeID { get; set; }

        public string ProgramTypeName { get; set; }

        public int? FinacleSchemeCodeID { get; set; }

        public string FinacleSchemeCodeName { get; set; }

        public bool? IsAdhoc { get; set; }

        public int? AmountDisburseID { get; set; }

        public decimal? AmountDisburse { get; set; }

        public int? CreditingOperativeID { get; set; }

        public string CreditingOperative { get; set; }

        public int? DebitingOperativeID { get; set; }

        public string DebitingOperative { get; set; }

        public DateTime? MaturityDate { get; set; }

        public string InterestCodeID { get; set; }

        public decimal? BaseRate { get; set; }

        public decimal? AccountPreferencial { get; set; }

        public decimal? AllInRate { get; set; }

        public int? RepricingPlanID { get; set; }

        public DateTime? PeggingDate { get; set; }

        public decimal? ApprovedMarginAsPerCM { get; set; }

        public decimal? SpecialFTP { get; set; }

        public decimal? Margin { get; set; }

        public decimal? AllInSpecialRate { get; set; }

        public string PrincipalFrequencyName { get; set; }

        public int? PeggingFrequencyID { get; set; }

        public DateTime? PrincipalStartDate { get; set; }

        public int? PrincipalFrequencyID { get; set; }

        public string NoOfInstallment { get; set; }

        public DateTime? InterestStartDate { get; set; }

        public string LimitIDinFin10 { get; set; }

        public DateTime? LimitExpiryDate { get; set; }

        public int? SanctionLimitCurrID { get; set; }

        public string SactionLimitCurrCode { get; set; }
        public decimal? utilazition { get; set; }
        public DateTime? ValueDateFun { get; set; }
        public decimal? sactionlimit { get; set; }
        public decimal? outstanding { get; set; }

        public int? UtilizationCurrID { get; set; }

        public string UtilizationCurrCode { get; set; }

        public int? OutstandingCurrID { get; set; }

        public string OutstandingCurrCode { get; set; }

        public string CSOName { get; set; }
        public long FundingMemoDocumentID { get; set; }
    }
    public class EmployeeFundingMemoLTModel
    {
        public EmployeeModel Employee { get; set; }
        public int CustCSOID { get; set; }
        public int CIF { get; set; }
        public int EmployeeID { get; set; }
        public string nameEmp { get; set; }
    }
    public class DocumentFundingModel
    {
        public IList<FundingMemoDocumentModel> Documents { get; set; }
    }

    public class FundingMemoTVP
    {
        public string CIF { get; set; }
        public int CurrencyID { get; set; }
        public int? BizSegmentID { get; set; }
        public DateTime? ValueDate { get; set; }
        public int? CustomerCategoryID { get; set; }
        public int? LoanTypeID { get; set; }
        public string SolID { get; set; }
        public int? ProgramTypeID { get; set; }
        public int? FinacleSchemeCodeID { get; set; }
        public bool? IsAdhoc { get; set; }
        public string CreditingOperativeID { get; set; }
        public string DebitingOperativeID { get; set; }
        public DateTime? MaturityDate { get; set; }
        public string InterestCodeID { get; set; }
        public decimal? BaseRate { get; set; }
        public decimal? AccountPreferencial { get; set; }
        public decimal? AllInRate { get; set; }
        public int? RepricingPlanID { get; set; }
        public decimal? ApprovedMarginAsPerCM { get; set; }
        public decimal? SpecialFTP { get; set; }
        public decimal? Margin { get; set; }
        public decimal? AllInSpecialRate { get; set; }
        public DateTime? PeggingDate { get; set; }
        public int? PeggingFrequencyID { get; set; }
        public DateTime? PrincipalStartDate { get; set; }
        public DateTime? InterestStartDate { get; set; }
        public string NoOfInstallment { get; set; }
        public int? PrincipalFrequencyID { get; set; }
        public int? InterestFrequencyID { get; set; }
        public string LimitIDinFin10 { get; set; }
        public DateTime? LimitExpiryDate { get; set; }
        public decimal? SanctionLimit { get; set; }
        public int? SanctionLimitCurrID { get; set; }
        public decimal? utilization { get; set; }
        public int? UtilizationCurrID { get; set; }
        public decimal? Outstanding { get; set; }
        public int? OutstandingCurrID { get; set; }
        public string CSOName { get; set; }
        public string Remarks { get; set; }
        public string OtherRemarks { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        public int? AmountDisburseID { get; set; }
        public decimal? AmountDisburse { get; set; }
        public decimal? CreditingOperative { get; set; }
        public string DebitingOperative { get; set; }
        public string BizName { get; set; }

    }
    public class FundingMemoLTModel
    {

        public long? CSOID { get; set; }
        public long? CSOFundingMemoID { get; set; }
        public long? TransactionFundingMemoID { get; set; }
        public string CIF { get; set; }
        public string Name { get; set; }
        // public Nullable<int> CurrencyID { get; set; }
        public Nullable<int> BizSegmentID { get; set; }
        public DateTime? ValueDate { get; set; }
        public Nullable<int> CustomerCategoryID { get; set; }
        public string CustomerCategoryName { get; set; }
        public Nullable<int> LoanTypeID { get; set; }
        public string LoanTypeName { get; set; }
        public string SolID { get; set; }
        public Nullable<int> ProgramTypeID { get; set; }
        public Nullable<int> FinacleSchemeCodeID { get; set; }
        public Nullable<bool> IsAdhoc { get; set; }
        public string CreditingOperative { get; set; }
        public string DebitingOperative { get; set; }
        public DateTime? MaturityDate { get; set; }
        public string InterestCodeID { get; set; }
        public decimal? BaseRate { get; set; }
        public decimal? AccountPreferencial { get; set; }
        public decimal? AllInRate { get; set; }
        public Nullable<int> RepricingPlanID { get; set; }
        public Nullable<decimal> ApprovedMarginAsPerCM { get; set; }
        public decimal? SpecialFTP { get; set; }
        public decimal? Margin { get; set; }
        public decimal? AllInSpecialRate { get; set; }
        public DateTime? PeggingDate { get; set; }
        public Nullable<int> PeggingFrequencyID { get; set; }
        public DateTime? PrincipalStartDate { get; set; }
        public DateTime? InterestStartDate { get; set; }
        public string NoOfInstallment { get; set; }
        public PrincipalFrequencyModel PrincipalFrequency { get; set; }
        public Nullable<int> InterestFrequencyID { get; set; }
        public string LimitIDinFin10 { get; set; }
        public DateTime? LimitExpiryDate { get; set; }
        public decimal? SanctionLimitAmount { get; set; }
        public decimal? AmountDisburse { get; set; }
        public Nullable<int> SanctionLimitCurrID { get; set; }
        public decimal? utilizationAmount { get; set; }
        public Nullable<int> UtilizationCurrID { get; set; }
        public decimal? OutstandingAmount { get; set; }
        public Nullable<int> OutstandingCurrID { get; set; }
        public string CSOName { get; set; }
        public string Remarks { get; set; }
        public string OtherRemarks { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public string BizSegmentName { get; set; }

        public CustomerModel Customer { get; set; }
        public BizSegmentModel BizSegment { get; set; }
        public string BizName { get; set; }
        public CurrencyModel Currency { get; set; }
        public LocationModel Location { get; set; }

        public CSOModel CSO { get; set; }
        public ParameterSystemModelFunding ParameterSystemModel { get; set; }
        public ProgramType ProgramType { get; set; }
        public ParameterSystemModelFunding LoanID { get; set; }

        public AmountDisburseIDModel AmountDisburseID { get; set; }
        public OutstandingIDModel OutstandingID { get; set; }
        public utilizationIDModel utilizationID { get; set; }
        public SanctionLimitCurrModel SanctionLimitCurr { get; set; }

        public FinacleScheme FinacleScheme { get; set; }
        public Interest Interest { get; set; }
        public RepricingPlan RepricingPlan { get; set; }
        public PeggingFrequency PeggingFrequency { get; set; }
        public InterestFrequency InterestFrequency { get; set; }
        public int? PrincipalFrequencyID { get; set; }
        public IList<FundingMemoDocumentModel> fundingmemodocument { get; set; }
        public IList<EmployeeFundingMemoLTModel> Employee { get; set; }
        public IList<TransactionDocumentFundingMemoModel> DocumentsFunding { get; set; }
        public IList<TransactionDocumentFundingMemoPopUpModel> DocumentsFundingPopUp { get; set; }
        public TransactionFundingMemo TransactionFundingMemo { get; set; }
        public string EmployeeFunding { get; set; }
        public string ApprovalDOA { get; set; }
        public string DoAName { get; set; }

    }

    public class DocPathModel
    {
        public string name { get; set; }

    }
    public class CSOModel
    {
        public string Name { get; set; }
    }
    public class LocationModel
    {
        public int? ID { get; set; }
        public string SolId { get; set; }

    }

    public class ParameterSystemModelFunding
    {
        public int ID { get; set; }
        public int? LoanTypeID { get; set; }
        public string LoanTypeName { get; set; }
        public ProgramType ProgramType { get; set; }
        public FinacleScheme FinacleScheme { get; set; }
        public Interest Interest { get; set; }
        public RepricingPlan RepricingPlan { get; set; }
        public PeggingFrequency PeggingFrequency { get; set; }
        public InterestFrequency InterestFrequency { get; set; }
        public CustomerCategory CustomerCategory { get; set; }
        public PrincipalFrequency PrincipalFrequency { get; set; }
        public LoanMaintenanceType MaintenanceType { get; set; }

    }
    public class PrincipalFrequency
    {
        public int? PrincipalFrequencyID { get; set; }
        public string PricipalFrequencyName { get; set; }
    }

    public class LoanMaintenanceType
    {
        public int? MaintenanceTypeID { get; set; }
        public string MaintenanceTypeName { get; set; }
    }
    public class TransactionDocumentFundingMemoModel
    {
        /// <summary>
        /// Transaction Document ID
        /// </summary>
        public long? ID { get; set; }

        /// <summary>
        /// Document Type details
        /// </summary>
        public DocumentTypeModel Type { get; set; }

        /// <summary>
        /// Document Type details
        /// </summary>
        public DocumentPurposeModel Purpose { get; set; }

        /// <summary>
        /// Document filename
        /// </summary>
        public string FileName { get; set; }
        public string DocumentPath { get; set; }

        /// <summary>
        /// DocumentPath
        /// </summary>


        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }

        public bool? IsDormant { get; set; }
    }
    public class TransactionDocumentFundingMemoPopUpModel
    {
        /// <summary>
        /// Transaction Document ID
        /// </summary>
        public long? ID { get; set; }

        /// <summary>
        /// Document Type details
        /// </summary>
        public DocumentTypeModel Type { get; set; }

        /// <summary>
        /// Document Type details
        /// </summary>
        public DocumentPurposeModel Purpose { get; set; }

        /// <summary>
        /// Document filename
        /// </summary>
        public string FileName { get; set; }
        public string DocumentPath { get; set; }

        /// <summary>
        /// DocumentPath
        /// </summary>


        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }

        public bool? IsDormant { get; set; }
    }
    public class CustomerCategory
    {
        public int? CustomerCategoryID { get; set; }
        public string CustomerCategoryName { get; set; }
    }
    public class ProgramType
    {
        public int? ProgramTypeID { get; set; }
        public string ProgramTypeName { get; set; }
    }
    public class FinacleScheme
    {
        public int? FinacleSchemeCodeID { get; set; }
        public string FinacleSchemeCodeName { get; set; }
    }
    public class Interest
    {
        public int? InterestID { get; set; }
        public string InterestCodeID { get; set; }
        public string InterestCodeName { get; set; }
    }
    public class RepricingPlan
    {
        public int? RepricingPlanID { get; set; }
        public string RepricingPlanName { get; set; }
    }
    public class PeggingFrequency
    {
        public int? PeggingFrequencyID { get; set; }
        public string PeggingFrequencyName { get; set; }
    }
    public class PrincipalFrequencyModel
    {
        public int? PrincipalFrequencyID { get; set; }
        public string PrincipalFrequencyName { get; set; }
    }
    public class InterestFrequency
    {
        public int? InterestFrequencyID { get; set; }
        public string InterestFrequencyName { get; set; }
    }

    public class AmountDisburseIDModel
    {

        public int? ID { get; set; }

        public int? DraftCurrencyID { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }

        public string CodeDescription { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public string LastModifiedBy { get; set; }
    }
    public class SanctionLimitCurrModel
    {
        public int? ID { get; set; }

        public int? DraftCurrencyID { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }

        public string CodeDescription { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public string LastModifiedBy { get; set; }
    }
    public class OutstandingIDModel
    {
        public int? ID { get; set; }

        public int? DraftCurrencyID { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }

        public string CodeDescription { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public string LastModifiedBy { get; set; }
    }
    public class utilizationIDModel
    {
        public int? ID { get; set; }

        public int? DraftCurrencyID { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }

        public string CodeDescription { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public string LastModifiedBy { get; set; }
    }

    public class FundingMemoDocumentModel
    {
        public Int64 ID { get; set; }
        public long? CSOfundingMemoID { get; set; }

        public long? FundingMemoDocumentID { get; set; }

        public long? FundingMemoID { get; set; }

        public int? TypeID { get; set; }

        public string DocumentPath { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public string FileName { get; set; }
        public int? PurposeID { get; set; }


        /// <summary>
        /// Document Type details
        /// </summary>
        public DocumentPurposeModel Purpose { get; set; }
        public DocumentTypeModel Type { get; set; }


    }


    public class FundingMemoLTRow : FundingMemoLTModel
    {
        public int RowID { get; set; }
    }

    public class FundingMemoLTGrid : Grid
    {
        public IList<FundingMemoLTRow> Rows { get; set; }
        public IList<EmployeeFundingMemoLTModel> Employee { get; set; }
        public FundingMemoLTModel Document = new FundingMemoLTModel();
    }
    #endregion

    #region filter
    public class FundingMemoLTFilter : Filter
    {

    }
    #endregion

    #region Interface
    public interface IFundingMemoLTRepository : IDisposable
    {
        bool GetCustomerLoan(string key, int limit, ref IList<CustomerLoanModel> customers, ref string message);
        bool GetFundingMemoTLCSOApp(int page, int size, IList<FundingMemoLTFilter> filters, string sortColumn, string sortOrder, ref FundingMemoLTGrid Funding, ref string message, String cif);
        bool GetRepricingFrequency(ref IList<PeggingFrequency> funding, ref string message);
        bool GetInterestCode(ref IList<Interest> funding, ref string message);
        bool GetRepricing(ref IList<RepricingPlan> funding, ref string message);
        //Andri
        bool GetEmployeeCSO(ref IList<EmployeeFundingMemoLTModel> Employee, ref string message);
        bool GetLoanType(ref IList<ParameterSystemModelFunding> funding, ref string message);
        bool GetProgramType(ref IList<ProgramType> funding, ref string message);
        bool GetSchemeCode(ref IList<FinacleScheme> funding, ref string message);
        bool GetFundingMemoTLByID(ref FundingMemoLTModel FundingMemo, ref string Message);
        bool GetFundingMemoTL(ref IList<FundingMemoLTModel> Funding, int limit, int index, ref string message);
        bool GetFundingMemoTL(int page, int size, IList<FundingMemoLTFilter> filters, string sortColumn, string sortOrder, ref FundingMemoLTGrid Funding, ref string message);
        bool GetFundingMemoTL(FundingMemoLTFilter filter, ref IList<FundingMemoLTModel> fundings, ref string message);
        bool GetFundingMemoTL(string key, int limit, ref IList<FundingMemoLTModel> fundings, ref string message);
        bool AddFundingMemoTL(FundingMemoLTModel funding, ref string message);
        bool UpdateFundingMemoTL(int id, FundingMemoLTModel funding, ref string message);
        bool UpdateFundingMemoDocument(long id, IList<FundingMemoDocumentModel> documents, ref string message);
        bool DeleteFundingMemoTL(int id, ref string message);
        bool GetCSOName(string key, ref IList<EmployeeFundingMemoLTModel> EmployeeFunding, ref string Message);

        bool GetBaseRate(string currency, string tenorCode, ref FundingMemoLTModel Funding, ref string Message);
        bool GetApprovalDOA(ref FundingMemoLTModel Funding, ref string Message);

    }
    #endregion

    #region repository
    public class FundingMemoLTRepository : IFundingMemoLTRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();


        public bool GetCustomerLoan(string key, int limit, ref IList<CustomerLoanModel> customers, ref string message)
        {
            bool isSuccess = false;

            try
            {
                customers = (from a in context.Customers
                             join b in context.Adhocs on a.CIF equals b.CIF into ps
                             from b in ps.DefaultIfEmpty()
                             where a.IsDeleted.Equals(false)
                                 && (a.CIF.Contains(key) || a.CustomerName.Contains(key))
                             orderby new { a.CIF, a.CustomerName }
                             select new CustomerLoanModel
                             {
                                 IsAdhoc = b.IsAdhocRequired.HasValue ? b.IsAdhocRequired.Value : false,
                                 CIF = a.CIF,
                                 Name = a.CustomerName,
                                 POAName = a.POAName,
                                 Branch = a.Location != null ? new BranchModel()
                                 {
                                     ID = a.Location.LocationID,
                                     Name = a.Location.LocationName,
                                     LastModifiedBy = a.Location.UpdateDate == null ? a.Location.CreateBy : a.Location.UpdateBy,
                                     LastModifiedDate = a.Location.UpdateDate == null ? a.Location.CreateDate : a.Location.UpdateDate.Value
                                 } : new BranchModel()
                                 {
                                     ID = 0,
                                     Name = "",
                                     LastModifiedBy = a.Location.UpdateDate == null ? a.Location.CreateBy : a.Location.UpdateBy,
                                     LastModifiedDate = a.Location.UpdateDate == null ? a.Location.CreateDate : a.Location.UpdateDate.Value
                                 },
                                 BizSegment = new BizSegmentModel()
                                 {
                                     ID = a.BizSegment.BizSegmentID,
                                     Name = a.BizSegment.BizSegmentName,
                                     Description = a.BizSegment.BizSegmentDescription,
                                     LastModifiedBy = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateBy : a.BizSegment.UpdateBy,
                                     LastModifiedDate = a.BizSegment.UpdateDate == null ? a.BizSegment.CreateDate : a.BizSegment.UpdateDate.Value
                                 },

                                 Type = a.CustomerType != null ? new CustomerTypeModel()
                                 {
                                     ID = a.CustomerType.CustomerTypeID,
                                     Name = a.CustomerType.CustomerTypeName,
                                     Description = a.CustomerType.CustomerTypeDescription,
                                     LastModifiedBy = a.CustomerType.UpdateDate == null ? a.CustomerType.CreateBy : a.CustomerType.UpdateBy,
                                     LastModifiedDate = a.CustomerType.UpdateDate == null ? a.CustomerType.CreateDate : a.CustomerType.UpdateDate.Value
                                 } : new CustomerTypeModel()
                                 {
                                     ID = 0,
                                     Name = "",
                                     Description = "",
                                     LastModifiedBy = a.CustomerType.UpdateDate == null ? a.CustomerType.CreateBy : a.CustomerType.UpdateBy,
                                     LastModifiedDate = a.CustomerType.UpdateDate == null ? a.CustomerType.CreateDate : a.CustomerType.UpdateDate.Value
                                 },
                                 RM = a.Employee != null ? new RMModel()
                                 {

                                     ID = a.Employee.EmployeeID,
                                     Name = a.Employee.EmployeeName,
                                     RankID = a.Employee.RankID,
                                     BranchID = a.Employee.LocationID,
                                     SegmentID = a.Employee.BizSegmentID,
                                     Email = a.Employee.EmployeeEmail,
                                     LastModifiedBy = a.Employee.UpdateDate == null ? a.Employee.CreateBy : a.Employee.UpdateBy,
                                     LastModifiedDate = a.Employee.UpdateDate == null ? a.Employee.CreateDate : a.Employee.UpdateDate.Value
                                 } : new RMModel()
                                 {
                                     ID = 0,
                                     Name = "",
                                     RankID = 0,
                                     BranchID = 0,
                                     SegmentID = 0,
                                     Email = "",
                                     LastModifiedBy = a.Employee.CreateBy,
                                     LastModifiedDate = a.Employee.CreateDate
                                 },
                                 Accounts = a.CustomerAccounts.Where(x => x.IsDeleted.Equals(false)).Select(x => new CustomerAccountModel()
                                 {
                                     AccountNumber = x.AccountNumber,
                                     Currency = new CurrencyModel()
                                     {
                                         ID = x.Currency.CurrencyID,
                                         Code = x.Currency.CurrencyCode,
                                         Description = x.Currency.CurrencyDescription,
                                         LastModifiedBy = x.Currency.UpdateDate == null ? x.Currency.CreateBy : x.Currency.UpdateBy,
                                         LastModifiedDate = x.Currency.UpdateDate == null ? x.Currency.CreateDate : x.Currency.UpdateDate.Value
                                     },
                                     LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                     LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                                 }).ToList(),
                                 /* Accounts = (from x in context.CustomerAccounts
                                             join y in context.CustomerAccountMappings.Where(xy => xy.IsDeleted == false) on x.AccountNumber equals y.AccountNumber into temp
                                             from z in temp.DefaultIfEmpty()
                                             where (z.CIF == a.CIF)
                                             select new CustomerAccountModel
                                             {
                                                 AccountNumber = x.AccountNumber,
                                                 IsJointAccount = x.IsJointAccount,
                                                 CIFJoint = (from cus in context.Customers
                                                                    join cusm in context.CustomerAccountMappings on cus.CIF equals cusm.CIF
                                                                    where cusm.AccountNumber.Equals(x.AccountNumber) && !cus.CIF.Equals(x.CIF)
                                                             select cus.CIF).FirstOrDefault(),
                                                 Currency = new CurrencyModel()
                                                 {
                                                     ID = x.Currency.CurrencyID,
                                                     Code = x.Currency.CurrencyCode,
                                                     Description = x.Currency.CurrencyDescription,
                                                     LastModifiedBy = x.Currency.UpdateDate == null ? x.Currency.CreateBy : x.Currency.UpdateBy,
                                                     LastModifiedDate = x.Currency.UpdateDate == null ? x.Currency.CreateDate : x.Currency.UpdateDate.Value
                                                 },
                                                 LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                                 LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value
                                              }).ToList(),*/
                             }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool GetLoanType(ref IList<ParameterSystemModelFunding> funding, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    funding = (from a in context.ParameterSystems
                               where a.ParameterType == "LOAN_TYPE"
                               orderby new { a.ParameterValue }
                               select new ParameterSystemModelFunding
                               {
                                   //ParameterSystemModel = a.ParsysID != null ? new ParameterSystemModelFunding { LoanTypeID = a.ParsysID, LoanTypeName = a.ParameterValue } : new ParameterSystemModelFunding { LoanTypeID = 0, LoanTypeName = "" }
                                   LoanTypeID = a.ParsysID,
                                   LoanTypeName = a.ParameterValue
                               }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool GetProgramType(ref IList<ProgramType> funding, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    funding = (from a in context.ParameterSystems
                               where a.ParameterType == "LOAN_PROGRAM_TYPE"
                               orderby new { a.ParameterValue }
                               select new ProgramType
                               {
                                   ProgramTypeID = a.ParsysID,
                                   ProgramTypeName = a.ParameterValue
                                   //ProgramType = a.ParsysID != null ? new ProgramType { ProgramTypeID = a.ParsysID, ProgramTypeName = a.ParameterValue } : new ProgramType { ProgramTypeID = 0, ProgramTypeName = "" }
                               }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool GetSchemeCode(ref IList<FinacleScheme> funding, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    funding = (from a in context.ParameterSystems
                               where a.ParameterType == "LOAN_SCHEME_CODE"
                               orderby new { a.ParameterValue }
                               select new FinacleScheme
                               {
                                   FinacleSchemeCodeID = a.ParsysID,
                                   FinacleSchemeCodeName = a.ParameterValue
                                   //FinacleScheme = a.ParsysID != null ? new FinacleScheme { FinacleSchemeCodeID = a.ParsysID, FinacleSchemeCodeName = a.ParameterValue } : new FinacleScheme { FinacleSchemeCodeID = 0, FinacleSchemeCodeName = "" }
                               }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool GetRepricingFrequency(ref IList<PeggingFrequency> funding, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    funding = (from a in context.ParameterSystems
                               where a.ParameterType == "LOAN_PEGGING_FREQ"
                               orderby new { a.ParameterValue }
                               select new PeggingFrequency
                               {
                                   PeggingFrequencyID = a.ParsysID,
                                   PeggingFrequencyName = a.ParameterValue
                               }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool GetRepricing(ref IList<RepricingPlan> funding, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    funding = (from a in context.ParameterSystems
                               where a.ParameterType == "LOAN_REPRICING_PLAN"
                               orderby new { a.ParameterValue }
                               select new RepricingPlan
                               {
                                   //RepricingPlan = a.ParsysID != null ? new RepricingPlan { RepricingPlanID = a.ParsysID, RepricingPlanName = a.ParameterValue } : new RepricingPlan { RepricingPlanID = 0, RepricingPlanName = "" }
                                   RepricingPlanID = a.ParsysID,
                                   RepricingPlanName = a.ParameterValue
                               }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool GetInterestCode(ref IList<Interest> funding, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    funding = (from a in context.ParameterSystems
                               where a.ParameterType == "LOAN_INTEREST_RATE_CODE"
                               orderby new { a.ParameterValue }
                               select new Interest
                               {
                                   //Interest = a.ParsysID != null ? new Interest { InterestCodeID = Convert.ToString(a.ParsysID), InterestCodeName = a.ParameterValue } : new Interest { InterestCodeID = "", InterestCodeName = "" }
                                   InterestID = a.ParsysID,
                                   InterestCodeName = a.ParameterValue
                               }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        //Andriyanto 05 Desember 2015
        public bool GetEmployeeCSO(ref IList<EmployeeFundingMemoLTModel> Employee, ref string message)
        {
            bool isSuccess = false;
            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    Employee = (from a in context.CSOes
                                join b in context.Employees on a.EmployeeID equals b.EmployeeID
                                select new EmployeeFundingMemoLTModel
                                {

                                    Employee = new EmployeeModel()
                                    {
                                        EmployeeID = b.EmployeeID == null ? 1 : b.EmployeeID,
                                        EmployeeName = b.EmployeeID == null ? "" : context.Employees.Where(x => x.EmployeeID.Equals(b.EmployeeID)).Select(x => x.EmployeeName).FirstOrDefault()
                                    },
                                }).ToList();
                }
                isSuccess = true;
            }

            catch (Exception ex)
            {
                message = ex.InnerException.Message;
            }


            return isSuccess;
        }

        public bool GetApprovalDOA(ref FundingMemoLTModel Funding, ref string Message)
        {
            bool IsSuccess = false;
            try
            {
                string _cif = Funding.CIF;
                Customer cust = (from c in context.Customers
                                 where c.CIF == _cif
                                 select c).FirstOrDefault();
                if (cust != null)
                {
                    FundingMemoTVP tvp = new FundingMemoTVP()
                    {
                        CIF = Funding.CIF,
                        CurrencyID = Funding.Currency == null ? 0 : Funding.Currency.ID,
                        BizSegmentID = cust.BizSegmentID,
                        ValueDate = Funding.ValueDate,
                        CustomerCategoryID = Funding.CustomerCategoryID,
                        LoanTypeID = Funding.LoanTypeID,
                        SolID = Funding.SolID,
                        ProgramTypeID = Funding.ProgramTypeID != null ? Funding.ProgramTypeID : 0,
                        FinacleSchemeCodeID = Funding.FinacleSchemeCodeID,
                        IsAdhoc = Funding.IsAdhoc,
                        CreditingOperativeID = Funding.CreditingOperative,
                        DebitingOperativeID = Funding.DebitingOperative,
                        MaturityDate = Funding.MaturityDate,
                        InterestCodeID = Funding.InterestCodeID,
                        BaseRate = Funding.BaseRate,
                        AccountPreferencial = Funding.AccountPreferencial,
                        AllInRate = Funding.AllInRate,
                        ApprovedMarginAsPerCM = Funding.ApprovedMarginAsPerCM,
                        SpecialFTP = Funding.SpecialFTP,
                        Margin = Funding.Margin,
                        AllInSpecialRate = Funding.AllInSpecialRate,
                        PeggingDate = Funding.PeggingDate,
                        PeggingFrequencyID = Funding.PeggingFrequencyID,
                        InterestStartDate = Funding.InterestStartDate,
                        NoOfInstallment = Funding.NoOfInstallment,
                        PrincipalFrequencyID = Funding.PrincipalFrequency == null ? 0 : Funding.PrincipalFrequency.PrincipalFrequencyID,
                        InterestFrequencyID = Funding.InterestFrequencyID,
                        LimitIDinFin10 = Funding.LimitIDinFin10,
                        SanctionLimit = Funding.SanctionLimitAmount,
                        SanctionLimitCurrID = Funding.SanctionLimitCurrID,
                        utilization = Funding.utilizationAmount,
                        UtilizationCurrID = Funding.UtilizationCurrID,
                        Outstanding = Funding.OutstandingAmount,
                        OutstandingCurrID = Funding.OutstandingCurrID,
                        Remarks = Funding.Remarks,
                        OtherRemarks = Funding.OtherRemarks,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = string.Empty,
                        UpdateDate = DateTime.UtcNow,
                        UpdateBy = string.Empty,
                        AmountDisburseID = Funding.AmountDisburseID == null ? 0 : Funding.AmountDisburseID.ID,
                        AmountDisburse = Funding.AmountDisburse,
                        DebitingOperative = Funding.DebitingOperative,
                        BizName = Funding.BizName

                    };

                    List<FundingMemoTVP> fundList = new List<FundingMemoTVP>();
                    fundList.Add(tvp);
                    SqlParameter param1 = new SqlParameter();
                    GenerateSQLParamFunding(fundList, param1);
                    Funding.ApprovalDOA = context.Database.SqlQuery<string>(@"EXECUTE [dbo].[SP_GetMatrixApprovalDOADisbursement] @tvpFunding", param1).FirstOrDefault();
                }
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                Message = ex.Message;
            }
            return IsSuccess;
        }

        private void GenerateSQLParamFunding(List<FundingMemoTVP> Funding, SqlParameter param1)
        {
            param1.SqlDbType = SqlDbType.Structured;
            param1.SqlValue = GenerateFundingDataTable(Funding);
            param1.ParameterName = "@tvpFunding";
            param1.TypeName = "dbo.tvp_CSOFundingMemo";
        }

        private DataTable GenerateFundingDataTable(List<FundingMemoTVP> Funding)
        {
            DataTable fundDataTable = ToDataTable<FundingMemoTVP>(Funding);
            //fundDataTable.Columns.Remove("CSOID");
            //fundDataTable.Columns.Remove("CSOFundingMemoID");
            //fundDataTable.Columns.Remove("TransactionFundingMemoID");
            //fundDataTable.Columns.Remove("SanctionLimitAmount");
            //fundDataTable.Columns.Remove("utilizationAmount");
            //fundDataTable.Columns.Remove("OutstandingAmount");
            //fundDataTable.Columns.Remove("LastModifiedDate");
            //fundDataTable.Columns.Remove("LastModifiedBy");
            //fundDataTable.Columns.Remove("BizSegmentName");
            //fundDataTable.Columns.Remove("Customer");
            //fundDataTable.Columns.Remove("BizSegment");
            //fundDataTable.Columns.Remove("Currency");
            //fundDataTable.Columns.Remove("Location");
            //fundDataTable.Columns.Remove("CSO");
            //fundDataTable.Columns.Remove("ParameterSystemModel");
            //fundDataTable.Columns.Remove("ProgramType");
            //fundDataTable.Columns.Remove("LoanID");
            //fundDataTable.Columns.Remove("AmountDisburseID");
            //fundDataTable.Columns.Remove("OutstandingID");
            //fundDataTable.Columns.Remove("utilizationID");
            //fundDataTable.Columns.Remove("SanctionLimitCurr");
            //fundDataTable.Columns.Remove("FinacleScheme");
            //fundDataTable.Columns.Remove("Interest");
            //fundDataTable.Columns.Remove("RepricingPlan");
            //fundDataTable.Columns.Remove("PeggingFrequency");
            //fundDataTable.Columns.Remove("InterestFrequency");
            //fundDataTable.Columns.Remove("fundingmemodocument");
            //fundDataTable.Columns.Remove("Employee");
            //fundDataTable.Columns.Remove("DocumentsFunding");
            //fundDataTable.Columns.Remove("TransactionFundingMemo");
            //fundDataTable.Columns.Remove("EmployeeFunding");
            //fundDataTable.Columns.Remove("ApprovalDOA");
            ReorderTable(ref fundDataTable, "CIF", "CurrencyID", "BizSegmentID", "ValueDate", "CustomerCategoryID", "LoanTypeID", "SolID", "ProgramTypeID", "FinacleSchemeCodeID",
                            "IsAdhoc", "CreditingOperativeID", "DebitingOperativeID", "MaturityDate", "InterestCodeID", "BaseRate", "AccountPreferencial", "AllInRate",
                            "RepricingPlanID", "ApprovedMarginAsPerCM", "SpecialFTP", "Margin", "AllInSpecialRate", "PeggingDate", "PeggingFrequencyID",
                            "PrincipalStartDate", "InterestStartDate", "NoOfInstallment", "PrincipalFrequencyID", "InterestFrequencyID", "LimitIDinFin10",
                            "LimitExpiryDate", "SanctionLimit", "SanctionLimitCurrID", "utilization", "UtilizationCurrID", "Outstanding", "OutstandingCurrID",
                            "CSOName", "Remarks", "OtherRemarks", "CreateDate", "CreateBy", "UpdateDate", "UpdateBy", "AmountDisburseID", "AmountDisburse", "DebitingOperative",
                            "CreditingOperative", "BizName");
            return fundDataTable;
        }

        public DataTable ToDataTable<T>(IEnumerable<T> list) where T : new()
        {
            Type elementType = typeof(T);
            using (DataTable dt = new DataTable())
            {
                System.Reflection.PropertyInfo[] _props = elementType.GetProperties();
                IEnumerable<string> basePropName = from b in elementType.BaseType.GetProperties()
                                                   select b.Name;
                System.Reflection.PropertyInfo[] TProps = (from p in _props
                                                           where !basePropName.Contains(p.Name)
                                                           select p).ToArray();
                foreach (System.Reflection.PropertyInfo propInfo in TProps)
                {
                    Type _pi = propInfo.PropertyType;
                    Type ColType = Nullable.GetUnderlyingType(_pi) ?? _pi;

                    dt.Columns.Add(propInfo.Name, ColType);
                }
                foreach (T item in list)
                {
                    DataRow row = dt.NewRow();
                    foreach (System.Reflection.PropertyInfo propInfo in TProps)
                    {
                        if (propInfo.PropertyType == typeof(Nullable<DateTime>))
                        {
                            DateTime _date;
                            if (!DateTime.TryParse(propInfo.GetValue(item, null) == null ? "0" : propInfo.GetValue(item, null).ToString(), out _date))
                            {
                                row[propInfo.Name] = new DateTime(1900, 1, 1);
                            }
                        }
                        row[propInfo.Name] = propInfo.GetValue(item, null) ?? DBNull.Value;
                    }
                    dt.Rows.Add(row);
                }
                return dt;
            }
        }
        public void ReorderTable(ref DataTable table, params String[] columns)
        {
            for (int i = 0; i < columns.Length; i++)
            {
                table.Columns[columns[i]].SetOrdinal(i);
            }
        }
        //End

        public bool GetFundingMemoTLByID(ref FundingMemoLTModel Funding, ref string message)
        {
            bool isSuccess = false;
            try
            {
                Funding = (from funding in context.CSOFundingMemoes
                           join cso in context.CSOes on funding.CIF equals cso.CIF
                           join fundingmemodocument in context.FundingMemoDocuments on funding.CSOFundingMemoID equals fundingmemodocument.FundingMemoID
                           join parametersysmodel5 in context.ParameterSystems on funding.PeggingFrequencyID equals parametersysmodel5.ParsysID
                           join parametersysmodel6 in context.ParameterSystems on funding.InterestFrequencyID equals parametersysmodel6.ParsysID
                           select new FundingMemoLTModel
                           {

                               OutstandingAmount = funding.Outstanding,

                           }).SingleOrDefault();
                if (Funding != null)
                {
                    long? CIFa = Funding.CSOFundingMemoID;
                    List<FundingMemoDocument> fundingDoc = context.FundingMemoDocuments.Where(x => x.FundingMemoID == CIFa).ToList();
                    if (fundingDoc != null)
                    {
                        Funding.fundingmemodocument = new List<FundingMemoDocumentModel>();
                        foreach (var item in fundingDoc)
                        {
                            FundingMemoDocumentModel Doc = new FundingMemoDocumentModel();
                            {
                                Doc.FundingMemoID = item.FundingMemoID;
                                Doc.DocumentPath = item.DocumentPath;
                                Doc.FileName = item.Filename;
                                Doc.CreatedBy = item.CreateBy;
                                Doc.CreatedDate = item.CreateDate;
                            };
                            Funding.fundingmemodocument.Add(Doc);
                        }
                    }

                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool GetFundingMemoTL(ref IList<FundingMemoLTModel> Funding, int limit, int index, ref string message)
        {
            bool isSuccess = false;
            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    Funding = (from funding in context.CSOFundingMemoes
                               join customer in context.Customers on funding.Customer.CIF equals customer.CIF
                               join bizsegment in context.BizSegments on funding.BizSegmentID equals bizsegment.BizSegmentID
                               join currency in context.Currencies on funding.Currency.CurrencyID equals currency.CurrencyID
                               join cso in context.CSOes on funding.CIF equals cso.CIF
                               join location in context.Locations on customer.LocationID equals location.LocationID
                               join parametersysmodel in context.ParameterSystems on funding.LoanTypeID equals parametersysmodel.ParsysID
                               join parametersysmodel1 in context.ParameterSystems on funding.ProgramTypeID equals parametersysmodel1.ParsysID
                               join FundDoc in context.FundingMemoDocuments on funding.CSOFundingMemoID equals FundDoc.FundingMemoID
                               join parametersysmodel2 in context.ParameterSystems on funding.FinacleSchemeCodeID equals parametersysmodel2.ParsysID
                               join parametersysmodel3 in context.ParameterSystems on funding.InterestFrequencyID equals parametersysmodel3.ParsysID
                               join parametersysmodel4 in context.ParameterSystems on funding.RepricingPlanID equals parametersysmodel4.ParsysID
                               join parametersysmodel5 in context.ParameterSystems on funding.PeggingFrequencyID equals parametersysmodel5.ParsysID
                               join parametersysmodel6 in context.ParameterSystems on funding.InterestFrequencyID equals parametersysmodel6.ParsysID
                               select new FundingMemoLTModel
                               {
                                   Customer = funding.Customer != null ? new CustomerModel { CIF = funding.Customer.CIF, Name = funding.Customer.CustomerName } : new CustomerModel { CIF = "", Name = "" },
                                   BizSegment = funding.BizSegment != null ? new BizSegmentModel { ID = funding.BizSegment.BizSegmentID, Name = funding.BizSegment.BizSegmentName, Description = funding.BizSegment.BizSegmentDescription } : new BizSegmentModel { ID = 0, Name = "", Description = "" },
                                   Currency = funding.Currency != null ? new CurrencyModel { ID = funding.Currency.CurrencyID, CodeDescription = funding.Currency.CurrencyCode + "(" + funding.Currency.CurrencyDescription + ")" } : new CurrencyModel { ID = 0, CodeDescription = "" },
                                   Location = customer != null ? new LocationModel { ID = customer.LocationID, SolId = location.SolID } : new LocationModel { ID = 0, SolId = "" },
                                   CSOName = funding.CSOName,
                                   CSOFundingMemoID = Convert.ToInt32(funding.CSOFundingMemoID),
                                   CSOID = funding.CSOFundingMemoID,
                                   TransactionFundingMemoID = Convert.ToInt32(funding.TransactionFundingMemoID),
                                   ValueDate = funding.ValueDate,
                                   IsAdhoc = funding.IsAdhoc,
                                   //CreditingOperative = funding.CreditingOperativeID,
                                   //DebitingOperative = funding.DebitingOperativeID,
                                   CreditingOperative = funding.CreditingOperative,
                                   DebitingOperative = funding.DebitingOperative,
                                   MaturityDate = funding.MaturityDate,
                                   BaseRate = funding.BaseRate,
                                   AccountPreferencial = funding.AccountPreferencial,
                                   AllInRate = funding.AllInRate,
                                   ApprovedMarginAsPerCM = funding.ApprovedMarginAsPerCM,
                                   SpecialFTP = funding.SpecialFTP,
                                   Margin = funding.Margin,
                                   AllInSpecialRate = funding.AllInSpecialRate,
                                   PeggingDate = funding.PeggingDate,
                                   NoOfInstallment = funding.NoOfInstallment,
                                   LimitIDinFin10 = funding.LimitIDinFin10,
                                   LimitExpiryDate = funding.LimitExpiryDate,
                                   SanctionLimitAmount = funding.SanctionLimit,
                                   utilizationAmount = funding.utilization,
                                   OutstandingAmount = funding.Outstanding,
                                   OutstandingCurrID = funding.OutstandingCurrID,
                                   Remarks = funding.Remarks,
                                   OtherRemarks = funding.OtherRemarks,
                                   LastModifiedDate = funding.UpdateDate == null ? funding.CreateDate : funding.UpdateDate.Value,
                                   LastModifiedBy = funding.UpdateDate == null ? funding.CreateBy : funding.UpdateBy,
                                   ParameterSystemModel = funding.Customer != null ? new ParameterSystemModelFunding { LoanTypeID = funding.LoanTypeID, LoanTypeName = parametersysmodel.ParameterValue } : new ParameterSystemModelFunding { LoanTypeID = 0, LoanTypeName = "" },
                                   //fundingmemodocument = funding.FundDoc.Select(x => new FundingMemoDocumentModel() 


                                   //{ 
                                   //    CSOfundingMemoID = x.FundingMemoID,
                                   //    TypeModel = new DocumentTypeModel()
                                   //    {
                                   //        ID = x.DocumentType.DocTypeID,
                                   //        Name = x.DocumentType.DocTypeName,
                                   //        Description = x.DocumentType.DocTypeDescription,
                                   //        LastModifiedBy = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateBy : x.DocumentType.UpdateBy,
                                   //        LastModifiedDate = x.DocumentType.UpdateDate == null ? x.DocumentType.CreateDate : x.DocumentType.UpdateDate.Value
                                   //    },
                                   //    PurposeModel = new DocumentPurposeModel()
                                   //    {
                                   //        ID = x.DocumentPurpose.PurposeID,
                                   //        Name = x.DocumentPurpose.PurposeName,
                                   //        Description = x.DocumentPurpose.PurposeDescription,
                                   //        LastModifiedBy = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateBy : x.DocumentPurpose.UpdateBy,
                                   //        LastModifiedDate = x.DocumentPurpose.UpdateDate == null ? x.DocumentPurpose.CreateDate : x.DocumentPurpose.UpdateDate.Value
                                   //    },
                                   //     FileName = x.Filename,
                                   //                             DocumentPath = x.DocumentPath,
                                   //                             LastModifiedBy = x.UpdateDate == null ? x.CreateBy : x.UpdateBy,
                                   //                             LastModifiedDate = x.UpdateDate == null ? x.CreateDate : x.UpdateDate.Value,
                                   //})

                                   //fundingmemodocument = new List<FundingMemoDocumentModel>  { CSOfundingMemoID = fundingmemodocument.FundingMemoID, Type = fundingmemodocument.DocTypeID, Purpose = fundingmemodocument.PurposeID }
                               }).Skip(skip).Take(limit).ToList();


                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool GetFundingMemoTL(int page, int size, IList<FundingMemoLTFilter> filters, string sortColumn, string sortOrder, ref FundingMemoLTGrid Funding, ref string message)
        {
            {
                bool isSuccess = false;
                //IList<IList<FundingMemoDocumentModel>> Ret = new List<IList<FundingMemoDocumentModel>>();
                try
                {
                    FundingMemoLTModel filter = new FundingMemoLTModel();
                    int skip = (page - 1) * size;
                    string orderBy = sortColumn + " " + sortOrder;
                    DateTime maxDate = new DateTime();

                    if (filters != null)
                    {
                        filter.CIF = filters.Where(a => a.Field.Equals("CIF")).Select(a => a.Value).SingleOrDefault();                        
                        filter.Name = filters.Where(a => a.Field.Equals("Name")).Select(a => a.Value).SingleOrDefault();                      
                        filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
                        if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                        {
                            filter.LastModifiedDate = DateTime.Parse(filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                            maxDate = filter.LastModifiedDate.Value.AddDays(1);
                        }
                    }
                    using (DBSEntities context = new DBSEntities())
                    {



                      // var data = context.SP_GetFundingMemo().ToList(); //comment azam
                        //start add azam
                        var data = (from a in context.SP_GetFundingMemo()
                                   

                                    let isCif = !string.IsNullOrEmpty(filter.CIF)
                                    let isName = !string.IsNullOrEmpty(filter.Name)
                                    let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy) 
                                    let isCreateDate = filter.LastModifiedDate.HasValue ? true : false 
                                   
                                    where isCif ? a.CIF.Contains(filter.CIF) : true &&
                                     (isName ? a.CustomerName.Contains(filter.Name) : true) &&
                                     (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&                                     
                                     (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)


                                    select a).ToList();
                        //end azam

                        Funding.Page = page;
                        Funding.Size = size;
                        Funding.Total = data.Count();
                        Funding.Sort = new Sort { Column = sortColumn, Order = sortOrder };

                        Funding.Rows = data.Select((a, i) => new FundingMemoLTRow()
                        {
                            RowID = i + 1,
                            CIF = a.CIF,
                            Customer = new CustomerModel() { CIF = a.CIF, Name = a.CustomerName },
                            SolID = a.SolID,
                            CustomerCategoryID = a.CustomerCategoryID,
                            BizName = a.BizName,
                            BizSegment = new BizSegmentModel() { Name = a.BizName },
                            ParameterSystemModel = new ParameterSystemModelFunding() { LoanTypeID = a.LoanTypeID, LoanTypeName = a.LoanTypeValue },
                            ProgramType = new ProgramType() { ProgramTypeID = a.ProgramTypeID, ProgramTypeName = a.ProgramTypeValue },
                            LoanID = new ParameterSystemModelFunding() { LoanTypeID = a.LoanTypeID, LoanTypeName = a.LoanTypeValue },
                            FinacleScheme = new FinacleScheme { FinacleSchemeCodeID = a.FinacleSchemeCodeID, FinacleSchemeCodeName = a.FinacleSchemeValue },
                            AmountDisburseID = new AmountDisburseIDModel { ID = a.AmountDisburseID, CodeDescription = a.AmountDisbursCode + "(" + a.AmountDisburDesc + ")" },
                            SanctionLimitCurr = new SanctionLimitCurrModel { ID = a.SanctionLimitCurrID, CodeDescription = a.SanctionLimitCurrCode + "(" + a.SanctionLimitCurrDesc + ")" },
                            utilizationID = new utilizationIDModel { ID = a.UtilizationCurrID, CodeDescription = a.UtilizationCurrCode + "(" + a.UtilizationCurrDesc + ")" },
                            OutstandingID = new OutstandingIDModel { ID = a.OutstandingCurrID, CodeDescription = a.OutstandingCurrCode + "(" + a.OutstandingCurrDesc + ")" },
                            Interest = new Interest { InterestCodeID = a.InterestCodeID, InterestCodeName = a.InterestCodeValue },
                            RepricingPlan = new RepricingPlan { RepricingPlanID = a.RepricingPlanID, RepricingPlanName = a.RepricingPlanValue },
                            PeggingFrequency = new PeggingFrequency { PeggingFrequencyID = a.PeggingFrequencyID, PeggingFrequencyName = a.PeggingFrequencyValue },
                            PrincipalFrequency = new PrincipalFrequencyModel { PrincipalFrequencyID = a.PrincipalFrequencyID, PrincipalFrequencyName = a.PrincipalFrequencyName },
                            InterestFrequency = new InterestFrequency { InterestFrequencyID = a.InterestFrequencyID, InterestFrequencyName = a.InterestFrequencyValue },
                            AmountDisburse = a.AmountDisburse,
                            CSOName = a.CSOName ?? "-",
                            CSOFundingMemoID = a.CSOFundingMemoID,//Convert.ToInt32(a.CSOFundingMemoID),
                            TransactionFundingMemoID = a.TransactionFundingMemoID,//Convert.ToInt32(a.TransactionFundingMemoID),
                            DoAName = a.DoAName,

                            DocumentsFunding = context.FundingMemoDocuments.Where(x => x.FundingMemoID == a.CSOFundingMemoID).Select(x => new TransactionDocumentFundingMemoModel()
                            {
                                FileName = x.Filename,
                                LastModifiedDate = x.CreateDate,
                                ID = x.FundingMemoID,
                                DocumentPath = x.DocumentPath,


                                Type = new DocumentTypeModel()
                                {
                                    Name = x.DocumentType.DocTypeName,
                                    ID = x.DocumentType.DocTypeID
                                },
                                Purpose = new DocumentPurposeModel()
                                {
                                    ID = x.DocumentPurpose.PurposeID,
                                    Name = x.DocumentPurpose.PurposeName
                                }

                            }).ToList(),
                            EmployeeFunding = a.CSOName,

                            //fundingmemodocument = context.FundingMemoDocuments.Where(x => x.FundingMemoID.Equals(a.FundingMemoID)).Single(),

                            ApprovalDOA = a.DoAName,
                            ValueDate = a.ValueDate,
                            IsAdhoc = a.IsAdhoc,
                            CreditingOperative = a.CreditingOperative,
                            DebitingOperative = a.DebitingOperative,
                            MaturityDate = a.MaturityDate,
                            BaseRate = a.BaseRate,
                            AccountPreferencial = a.AccountPreferencial,
                            AllInRate = a.AllInRate,
                            ApprovedMarginAsPerCM = a.ApprovedMarginAsPerCM,
                            SpecialFTP = a.SpecialFTP,
                            Margin = a.Margin,
                            PrincipalStartDate = a.PrincipalStartDate,
                            InterestStartDate = a.InterestStartDate,
                            AllInSpecialRate = a.AllInSpecialRate,
                            PeggingDate = a.PeggingDate,
                            NoOfInstallment = a.NoOfInstallment ?? "-",
                            LimitIDinFin10 = a.LimitIDinFin10 ?? "-",
                            LimitExpiryDate = a.LimitExpiryDate,
                            SanctionLimitAmount = a.SanctionLimitAmount,
                            utilizationAmount = a.utilizationAmount,
                            OutstandingAmount = a.OutstandingAmount,
                            OutstandingCurrID = a.OutstandingCurrID,
                            Remarks = a.Remarks ?? "-",
                            OtherRemarks = a.OtherRemarks ?? "-",
                            LastModifiedBy = a.LastModifiedBy ?? "-",
                            LastModifiedDate = a.LastModifiedDate
                        })
                         .Skip(skip)
                         .Take(size)
                         .ToList();


                    }
                    isSuccess = true;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string _message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(_message, raise);
                        }
                    }
                    throw raise;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
                return isSuccess;
            }

        }
        public bool GetFundingMemoTLCSOApp(int page, int size, IList<FundingMemoLTFilter> filters, string sortColumn, string sortOrder, ref FundingMemoLTGrid Funding, ref string message, String cif)
        {
            {
                bool isSuccess = false;
                //IList<IList<FundingMemoDocumentModel>> Ret = new List<IList<FundingMemoDocumentModel>>();
                try
                {
                    FundingMemoLTModel filter = new FundingMemoLTModel();
                    int skip = (page - 1) * size;
                    string orderBy = sortColumn + " " + sortOrder;
                    DateTime maxDate = new DateTime();

                    if (filters != null)
                    {
                        filter.CIF = filters.Where(a => a.Field.Equals("CIF")).Select(a => a.Value).SingleOrDefault();
                        filter.Customer.Name = filters.Where(a => a.Field.Equals("Customer")).Select(a => a.Value).SingleOrDefault();
                        filter.BizSegment.Name = filters.Where(a => a.Field.Equals("BizSegment")).Select(a => a.Value).SingleOrDefault();
                        filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();
                        if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                        {
                            filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                            maxDate = filter.LastModifiedDate.Value.AddDays(1);
                        }
                    }
                    using (DBSEntities context = new DBSEntities())
                    {



                        var CustCif = context.SP_GetFundingMemoPopUp(cif).ToList();
                        var data = (from a in CustCif
                                    where a.CIF == cif
                                    select a).ToList();


                        Funding.Page = page;
                        Funding.Size = size;
                        Funding.Total = data.Count();
                        Funding.Sort = new Sort { Column = sortColumn, Order = sortOrder };

                        Funding.Rows = data.Select((a, i) => new FundingMemoLTRow()
                        {
                            RowID = i + 1,
                            CIF = a.CIF,
                            Customer = new CustomerModel() { CIF = a.CIF, Name = a.CustomerName },
                            SolID = a.SolID,
                            CustomerCategoryID = a.CustomerCategoryID,
                            CustomerCategoryName = a.CustomerCategoryName,
                            BizName = a.BizName,
                            BizSegment = new BizSegmentModel() { Name = a.BizName },
                            ParameterSystemModel = new ParameterSystemModelFunding() { LoanTypeID = a.LoanTypeID, LoanTypeName = a.LoanTypeValue },
                            ProgramType = new ProgramType() { ProgramTypeID = a.ProgramTypeID, ProgramTypeName = a.ProgramTypeValue },
                            LoanID = new ParameterSystemModelFunding() { LoanTypeID = a.LoanTypeID, LoanTypeName = a.LoanTypeValue },
                            FinacleScheme = new FinacleScheme { FinacleSchemeCodeID = a.FinacleSchemeCodeID, FinacleSchemeCodeName = a.FinacleSchemeValue },
                            AmountDisburseID = new AmountDisburseIDModel { ID = a.AmountDisburseID, CodeDescription = a.AmountDisbursCode + "(" + a.AmountDisburDesc + ")" },
                            SanctionLimitCurr = new SanctionLimitCurrModel { ID = a.SanctionLimitCurrID, CodeDescription = a.SanctionLimitCurrCode + "(" + a.SanctionLimitCurrDesc + ")" },
                            utilizationID = new utilizationIDModel { ID = a.UtilizationCurrID, CodeDescription = a.UtilizationCurrCode + "(" + a.UtilizationCurrDesc + ")" },
                            OutstandingID = new OutstandingIDModel { ID = a.OutstandingCurrID, CodeDescription = a.OutstandingCurrCode + "(" + a.OutstandingCurrDesc + ")" },
                            Interest = new Interest { InterestCodeID = a.InterestCodeID, InterestCodeName = a.InterestCodeValue, InterestID = a.InterestFrequencyID },
                            RepricingPlan = new RepricingPlan { RepricingPlanID = a.RepricingPlanID, RepricingPlanName = a.RepricingPlanValue },
                            PeggingFrequency = new PeggingFrequency { PeggingFrequencyID = a.PeggingFrequencyID, PeggingFrequencyName = a.PeggingFrequencyValue },
                            InterestFrequency = new InterestFrequency { InterestFrequencyID = a.InterestFrequencyID, InterestFrequencyName = a.InterestFrequencyValue },
                            AmountDisburse = a.AmountDisburse,
                            CSOName = a.CSOName,
                            CSOFundingMemoID = Convert.ToInt32(a.CSOFundingMemoID),
                            TransactionFundingMemoID = a.TransactionFundingMemoID,//Convert.ToInt32(a.TransactionFundingMemoID),
                            DoAName = a.DoAName,

                            DocumentsFunding = context.FundingMemoDocuments.Where(x => x.FundingMemoID == a.CSOFundingMemoID).Select(x => new TransactionDocumentFundingMemoModel()
                            {
                                FileName = x.Filename,
                                LastModifiedDate = x.CreateDate,
                                ID = x.FundingMemoID,
                                DocumentPath = x.DocumentPath,
                                Type = new DocumentTypeModel()
                                {
                                    Name = x.DocumentType.DocTypeName,
                                    ID = x.DocumentType.DocTypeID
                                },
                                Purpose = new DocumentPurposeModel()
                                {
                                    ID = x.DocumentPurpose.PurposeID,
                                    Name = x.DocumentPurpose.PurposeName
                                },

                            }).ToList(),
                            EmployeeFunding = a.CSOName,

                            //fundingmemodocument = context.FundingMemoDocuments.Where(x => x.FundingMemoID.Equals(a.FundingMemoID)).Single(),


                            ValueDate = a.ValueDate,
                            IsAdhoc = a.IsAdhoc,
                            //CreditingOperative = a.CreditingOperativeID,
                            //DebitingOperative = a.DebitingOperativeID,
                            CreditingOperative = a.CreditingOperative,
                            DebitingOperative = a.DebitingOperative,
                            MaturityDate = a.MaturityDate,
                            BaseRate = a.BaseRate,
                            AccountPreferencial = a.AccountPreferencial,
                            AllInRate = a.AllInRate,
                            ApprovedMarginAsPerCM = a.ApprovedMarginAsPerCM,
                            SpecialFTP = a.SpecialFTP,
                            Margin = a.Margin,
                            PrincipalStartDate = a.PrincipalStartDate,
                            InterestStartDate = a.InterestStartDate,
                            AllInSpecialRate = a.AllInSpecialRate,
                            PeggingDate = a.PeggingDate,
                            NoOfInstallment = a.NoOfInstallment,
                            LimitIDinFin10 = a.LimitIDinFin10,
                            LimitExpiryDate = a.LimitExpiryDate,
                            SanctionLimitAmount = a.SanctionLimitAmount,
                            utilizationAmount = a.utilizationAmount,
                            OutstandingAmount = a.OutstandingAmount,
                            OutstandingCurrID = a.OutstandingCurrID,
                            Remarks = a.Remarks,
                            OtherRemarks = a.OtherRemarks,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                         .Skip(skip)
                         .Take(size)
                         .ToList();


                    }
                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
                return isSuccess;
            }

        }

        public bool GetFundingMemoTL(FundingMemoLTFilter filter, ref IList<FundingMemoLTModel> fundings, ref string message)
        {
            throw new NotImplementedException();
        }
        public bool GetFundingMemoTL(string key, int limit, ref IList<FundingMemoLTModel> fundings, ref string message)
        {
            bool isSuccess = false;
            try
            {
                fundings = (from funding in context.CSOFundingMemoes
                            join customer in context.Customers on funding.Customer.CIF equals customer.CIF
                            join bizsegment in context.BizSegments on funding.BizSegmentID equals bizsegment.BizSegmentID
                            join currency in context.Currencies on funding.Currency.CurrencyID equals currency.CurrencyID
                            join cso in context.CSOes on funding.CIF equals cso.CIF
                            join location in context.Locations on customer.LocationID equals location.LocationID
                            join parametersysmodel in context.ParameterSystems on funding.LoanTypeID equals parametersysmodel.ParsysID
                            join parametersysmodel1 in context.ParameterSystems on funding.ProgramTypeID equals parametersysmodel1.ParsysID
                            join parametersysmodel2 in context.ParameterSystems on funding.FinacleSchemeCodeID equals parametersysmodel2.ParsysID
                            join parametersysmodel3 in context.ParameterSystems on funding.InterestFrequencyID equals parametersysmodel3.ParsysID
                            join parametersysmodel4 in context.ParameterSystems on funding.RepricingPlanID equals parametersysmodel4.ParsysID
                            join parametersysmodel5 in context.ParameterSystems on funding.PeggingFrequencyID equals parametersysmodel5.ParsysID
                            join parametersysmodel6 in context.ParameterSystems on funding.InterestFrequencyID equals parametersysmodel6.ParsysID
                            select new FundingMemoLTModel
                            {
                                Customer = funding.Customer != null ? new CustomerModel { CIF = funding.Customer.CIF, Name = funding.Customer.CustomerName } : new CustomerModel { CIF = "", Name = "" },
                                BizSegment = funding.BizSegment != null ? new BizSegmentModel { ID = funding.BizSegment.BizSegmentID, Name = funding.BizSegment.BizSegmentName, Description = funding.BizSegment.BizSegmentDescription } : new BizSegmentModel { ID = 0, Name = "", Description = "" },
                                Currency = funding.Currency != null ? new CurrencyModel { ID = funding.Currency.CurrencyID, CodeDescription = funding.Currency.CurrencyCode + "(" + funding.Currency.CurrencyDescription + ")" } : new CurrencyModel { ID = 0, CodeDescription = "" },
                                Location = customer != null ? new LocationModel { ID = customer.LocationID, SolId = location.SolID } : new LocationModel { ID = 0, SolId = "" },
                                CSOName = funding.CSOName,
                                CSOFundingMemoID = Convert.ToInt32(funding.CSOFundingMemoID),
                                TransactionFundingMemoID = Convert.ToInt32(funding.TransactionFundingMemoID),
                                ValueDate = funding.ValueDate,
                                IsAdhoc = funding.IsAdhoc,
                                //CreditingOperative = funding.CreditingOperativeID,
                                //DebitingOperative = funding.DebitingOperativeID,
                                CreditingOperative = funding.CreditingOperative,
                                DebitingOperative = funding.DebitingOperative,
                                MaturityDate = funding.MaturityDate,
                                BaseRate = funding.BaseRate,
                                AccountPreferencial = funding.AccountPreferencial,
                                AllInRate = funding.AllInRate,
                                ApprovedMarginAsPerCM = funding.ApprovedMarginAsPerCM,
                                SpecialFTP = funding.SpecialFTP,
                                Margin = funding.Margin,
                                AllInSpecialRate = funding.AllInSpecialRate,
                                PeggingDate = funding.PeggingDate,
                                NoOfInstallment = funding.NoOfInstallment,
                                LimitIDinFin10 = funding.LimitIDinFin10,
                                LimitExpiryDate = funding.LimitExpiryDate,
                                SanctionLimitAmount = funding.SanctionLimit,
                                utilizationAmount = funding.utilization,
                                OutstandingAmount = funding.Outstanding,
                                OutstandingCurrID = funding.OutstandingCurrID,
                                Remarks = funding.Remarks,
                                OtherRemarks = funding.OtherRemarks,
                                LastModifiedDate = funding.UpdateDate == null ? funding.CreateDate : funding.UpdateDate.Value,
                                LastModifiedBy = funding.UpdateDate == null ? funding.CreateBy : funding.UpdateBy,
                                ParameterSystemModel = funding.Customer != null ? new ParameterSystemModelFunding { LoanTypeID = funding.LoanTypeID, LoanTypeName = parametersysmodel.ParameterValue } : new ParameterSystemModelFunding { LoanTypeID = 0, LoanTypeName = "" }
                            }).Take(limit).ToList();
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSuccess;
        }
        public bool AddFundingMemoTL(FundingMemoLTModel funding, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (funding.CSOFundingMemoID == null)
                {
                    context.CSOFundingMemoes.Add(new Entity.CSOFundingMemo()
                    {
                        CIF = funding.CIF,
                        //CurrencyID=funding.Currency.ID,
                        BizSegmentID = funding.BizSegmentID,
                        BizName = funding.BizName,
                        AmountDisburse = funding.AmountDisburse,
                        AmountDisburseID = funding.AmountDisburseID.ID,
                        ValueDate = funding.ValueDate,
                        CustomerCategoryID = funding.Customer.CustomerCategoryID,//add
                        LoanTypeID = funding.LoanID.LoanTypeID,//add
                        SolID = funding.SolID,//add
                        ProgramTypeID = funding.ProgramType.ProgramTypeID,//add
                        FinacleSchemeCodeID = funding.FinacleScheme.FinacleSchemeCodeID,//add
                        IsAdhoc = funding.IsAdhoc,
                        CreditingOperative = funding.CreditingOperative,
                        DebitingOperative = funding.DebitingOperative,
                        MaturityDate = funding.MaturityDate,
                        InterestCodeID = Convert.ToString(funding.Interest.InterestID),//add
                        BaseRate = funding.BaseRate,
                        AccountPreferencial = funding.AccountPreferencial,
                        AllInRate = funding.AllInRate,
                        RepricingPlanID = funding.RepricingPlan.RepricingPlanID,//add
                        ApprovedMarginAsPerCM = funding.ApprovedMarginAsPerCM,
                        SpecialFTP = funding.SpecialFTP,
                        Margin = funding.Margin,
                        AllInSpecialRate = funding.AllInSpecialRate,
                        PeggingDate = funding.PeggingDate,
                        PeggingFrequencyID = funding.PeggingFrequency.PeggingFrequencyID,//add
                        NoOfInstallment = funding.NoOfInstallment,
                        PrincipalFrequencyID = funding.PrincipalFrequency.PrincipalFrequencyID,//add interest same principal                    
                        InterestFrequencyID = funding.InterestFrequency.InterestFrequencyID,//add interest same principal
                        LimitIDinFin10 = funding.LimitIDinFin10,
                        LimitExpiryDate = funding.LimitExpiryDate,
                        SanctionLimit = funding.SanctionLimitAmount,
                        SanctionLimitCurrID = funding.SanctionLimitCurr.ID,//add refere currency
                        UtilizationCurrID = funding.utilizationID.ID,//add refere currency
                        utilization = funding.utilizationAmount,
                        Outstanding = funding.OutstandingAmount,
                        OutstandingCurrID = funding.OutstandingID.ID,//add refere currency
                        CSOName = funding.CSOName,
                        InterestStartDate = funding.InterestStartDate,
                        PrincipalStartDate = funding.PrincipalStartDate,
                        //Remarks = funding.Remarks,
                        //OtherRemarks = funding.OtherRemarks,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName,
                        DoAName = funding.DoAName,
                        IsDeleted = false,
                    });
                    context.SaveChanges();

                    var CSOFundingMEmoID = context.CSOFundingMemoes.OrderByDescending(p => p.CSOFundingMemoID).Select(r => r.CSOFundingMemoID).First();
                    long CSOId = CSOFundingMEmoID;
                    //Add transaction document
                    //if(funding.fundingmemodocument !=null && funding.fundingmemodocument.ToList().Count)
                    if (funding.fundingmemodocument.Count > 0)
                    {
                        foreach (var item in funding.fundingmemodocument)
                        {
                            Entity.FundingMemoDocument document = new Entity.FundingMemoDocument()
                            {
                                FundingMemoID = CSOFundingMEmoID,
                                Filename = item.FileName,
                                DocumentPath = item.DocumentPath,
                                DocTypeID = item.Type.ID,
                                PurposeID = item.Purpose.ID,
                                CreateDate = DateTime.UtcNow,
                                CreateBy = currentUser.GetCurrentUser().LoginName
                            };
                            context.FundingMemoDocuments.Add(document);
                        }

                        context.SaveChanges();
                    }
                }


                isSuccess = true;

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool UpdateFundingMemoTL(int id, FundingMemoLTModel funding, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.CSOFundingMemo data = context.CSOFundingMemoes.Where(a => a.CSOFundingMemoID.Equals(id)).SingleOrDefault();


                if (data != null)
                {
                    data.CIF = funding.CIF;
                    //CurrencyID=funding.Currency.ID,
                    data.BizSegmentID = funding.BizSegmentID;
                    data.BizName = funding.BizName;
                    data.AmountDisburse = funding.AmountDisburse;
                    data.AmountDisburseID = funding.AmountDisburseID.ID;
                    data.ValueDate = funding.ValueDate;
                    data.CustomerCategoryID = funding.Customer.CustomerCategoryID;
                    data.LoanTypeID = funding.LoanID.LoanTypeID;
                    data.SolID = funding.SolID;
                    data.ProgramTypeID = funding.ProgramType.ProgramTypeID;
                    data.FinacleSchemeCodeID = funding.FinacleScheme.FinacleSchemeCodeID;
                    data.IsAdhoc = funding.IsAdhoc;
                    data.CreditingOperative = funding.CreditingOperative;
                    data.DebitingOperative = funding.DebitingOperative;
                    data.MaturityDate = funding.MaturityDate;
                    data.InterestCodeID = Convert.ToString(funding.Interest.InterestID);
                    data.BaseRate = funding.BaseRate;
                    data.AccountPreferencial = funding.AccountPreferencial;
                    data.AllInRate = funding.AllInRate;
                    data.RepricingPlanID = funding.RepricingPlan.RepricingPlanID;
                    data.ApprovedMarginAsPerCM = funding.ApprovedMarginAsPerCM;
                    data.SpecialFTP = funding.SpecialFTP;
                    data.Margin = funding.Margin;
                    data.AllInSpecialRate = funding.AllInSpecialRate;
                    data.PeggingDate = funding.PeggingDate;
                    data.PeggingFrequencyID = funding.PeggingFrequency.PeggingFrequencyID;
                    data.NoOfInstallment = funding.NoOfInstallment;
                    data.PrincipalFrequencyID = funding.PrincipalFrequency.PrincipalFrequencyID;
                    data.InterestFrequencyID = funding.InterestFrequency.InterestFrequencyID;
                    data.LimitIDinFin10 = funding.LimitIDinFin10;
                    data.LimitExpiryDate = funding.LimitExpiryDate;
                    data.SanctionLimit = funding.SanctionLimitAmount;
                    data.SanctionLimitCurrID = funding.SanctionLimitCurr.ID;
                    data.UtilizationCurrID = funding.utilizationID.ID;
                    data.utilization = funding.utilizationAmount;
                    data.Outstanding = funding.OutstandingAmount;
                    data.OutstandingCurrID = funding.OutstandingID.ID;
                    data.CSOName = funding.CSOName;
                    data.InterestStartDate = funding.InterestStartDate;
                    data.PrincipalStartDate = funding.PrincipalStartDate;
                    //Remarks = funding.Remarks,
                    //OtherRemarks = funding.OtherRemarks,

                    data.DoAName = funding.DoAName;

                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;


                    context.SaveChanges();

                    var CSOFundingMEmo = context.CSOFundingMemoes.OrderByDescending(p => p.CSOFundingMemoID).Select(r => r.CSOFundingMemoID).First();

                    //Add transaction document
                    //   if(funding.fundingmemodocument !=null && funding.fundingmemodocument.ToList().Count)
                    if (funding.fundingmemodocument != null)
                    {
                        var existingDocument = (from x in context.FundingMemoDocuments
                                                where x.FundingMemoID == id
                                                select x);
                        if (existingDocument != null && existingDocument.ToList().Count > 0)
                            foreach (var item in existingDocument.ToList()) // remove All transaction document
                            {
                                //if (item.PurposeID != 2)
                                {
                                    context.FundingMemoDocuments.Remove(item);
                                }
                            }
                        context.SaveChanges();
                        // if (funding.fundingmemodocument.Count > 0)

                        foreach (var item in funding.fundingmemodocument)
                        {
                            Entity.FundingMemoDocument document = new Entity.FundingMemoDocument()
                            {
                                FundingMemoID = id,
                                Filename = item.FileName,
                                DocumentPath = item.DocumentPath,
                                DocTypeID = item.Type.ID,
                                PurposeID = item.Purpose.ID,
                                CreateDate = DateTime.UtcNow,
                                CreateBy = currentUser.GetCurrentUser().DisplayName,

                            };
                            context.FundingMemoDocuments.Add(document);
                        }

                        context.SaveChanges();
                    }


                    isSuccess = true;

                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;

        }
        public bool DeleteFundingMemoTL(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.CSOFundingMemo data = context.CSOFundingMemoes.Where(a => a.CSOFundingMemoID.Equals(id)).SingleOrDefault();


                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Funding data  is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateFundingMemoDocument(long id, IList<FundingMemoDocumentModel> documents, ref string message)
        {
            bool isSuccess = false;
            string back = string.Empty;


            try
            {
                if (documents != null && documents.Count > 0)
                {
                    foreach (var item in documents)
                    {
                        Entity.FundingMemoDocument document = new Entity.FundingMemoDocument();
                        document.FundingMemoID = id;
                        //document.DocTypeID = item.Type.ID;
                        //document.PurposeID = item.Purpose.ID;
                        document.Filename = item.FileName;
                        document.DocumentPath = item.DocumentPath;
                        document.CreateDate = DateTime.UtcNow;
                        document.CreateBy = currentUser.GetCurrentUser().DisplayName;
                        context.FundingMemoDocuments.Add(document);
                    }
                    context.SaveChanges();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message + back;
            }


            return isSuccess;
        }

        public bool GetCSOName(string key, ref IList<EmployeeFundingMemoLTModel> EmployeeFunding, ref string Message)
        {
            bool isSuccess = false;
            string intKey = key;// int.Parse(key);
            try
            {
                using (DBSEntities cont = new DBSEntities())
                {
                    var data = context.SP_GETCSONameFunding(intKey).ToList();
                    EmployeeFunding = (from a in context.SP_GETCSONameFunding(intKey)
                                       select new EmployeeFundingMemoLTModel
                                       {
                                           nameEmp = a,
                                       }).ToList();
                    isSuccess = true;

                    //EmployeeFunding = (from z in context.CSOes
                    //                   join y in context.Employees on z.EmployeeID equals y.EmployeeID
                    //                   where z.CIF.Equals(key)
                    //                   select new EmployeeFundingMemoLTModel
                    //                   {
                    //                       CIF = key,
                    //                       Employee = new EmployeeModel()
                    //                       {
                    //                           EmployeeID = y.EmployeeID == null ? 1 : y.EmployeeID,
                    //                           EmployeeName = y.EmployeeID == null ? "" : context.Employees.Where(n => n.EmployeeID.Equals(y.EmployeeID)).Select(x => x.EmployeeName).FirstOrDefault()
                    //                       }
                    //                   }).ToList();
                }
            }
            catch (Exception ex)
            {
                Message = ex.Message;
            }
            return isSuccess;
        }

        public bool GetBaseRate(string currency, string tenorCode, ref FundingMemoLTModel Funding, ref string Message)
        {
            bool IsSuccess = false;
            try
            {
                SP_GetIQuoteRate_Result rate = context.SP_GetIQuoteRate(currency, tenorCode).FirstOrDefault();
                if (rate != null && rate.BaseRate > 0)
                {
                    Funding.BaseRate = rate.BaseRate.Value;
                }
                else
                {
                    Funding.BaseRate = 0;
                }


                IsSuccess = true;
            }
            catch (Exception ex)
            {
                Message = ex.Message;
            }
            return IsSuccess;
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        currentUser = null;
                        context.Dispose();
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
    #endregion
}