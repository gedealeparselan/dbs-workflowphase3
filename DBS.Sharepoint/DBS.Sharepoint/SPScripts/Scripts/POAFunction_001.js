//var api_server = "http://localhost/DBS.WebAPI/";
var accessToken;

var ViewModel = function () {
	//Make the self as 'this' reference
	var self = this;
	
	//Declare observable which will be bind with UI 
	self.ID = ko.observable("");
	self.Name= ko.observable("");
	self.Description = ko.observable("");
	self.LastModifiedBy = ko.observable("");
	self.LastModifiedDate = ko.observable("");

	// grid properties
	self.availableSizes = ko.observableArray([10, 25, 50, 75, 100]);
	self.Page = ko.observable(1);
	self.Size = ko.observableArray([10]);
	self.Total = ko.observable(0);
	self.TotalPages = ko.observable(0);

	// filter
	self.FilterName = ko.observable("");
	self.FilterDescription = ko.observable("");
	self.FilterModifiedBy = ko.observable("");
	self.FilterModifiedDate = ko.observable("");

	// sorting
	self.SortColumn = ko.observable("Name");
	self.SortOrder = ko.observable("ASC");
	
	// New Data flag
	self.IsNewData = ko.observable(false);
	
	//The Object which stored data entered in the observables
	var POAFunction= {
		ID: self.ID,
		Name: self.Name,
		Description: self.Description,
		LastModifiedBy: self.LastModifiedBy,
		LastModifiedDate: self.LastModifiedDate
	};

	//Declare an ObservableArray for Storing the JSON Response
	self.POAFunctions = ko.observableArray([]);

	GetPOAFunctions(); //Call the Function which gets all records using ajax call

	
	self.save = function () {
		// validation
		var form = $("#aspnetForm");
		form.validate();
		
		if(form.valid()){
			//Ajax call to insert the POAFunction
			$.ajax({
				type: "POST",
				url: api.server + api.url.poafunction,
				data: ko.toJSON(POAFunction), //Convert the Observable Data into JSON
				contentType: "application/json",
				headers: {
					"Authorization" : "Bearer " + accessToken
				},
				success: function (data, textStatus, jqXHR) {
					if(jqXHR.status = 200){
						// send notification
						ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);
						
						// hide current popup window
						$("#modal-form").modal('hide');
	
						// refresh data
						GetPOAFunctions();       
					}else{
						ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-success', true);
					}                    
				},
				error: function (jqXHR, textStatus, errorThrown) {
					// send notification
					ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-success', true);
				}
			});
		}
	};

	self.update = function () {
		// validation
		var form = $("#aspnetForm");
		form.validate();
		
		if(form.valid()){
			// hide current popup window
			$("#modal-form").modal('hide');

			bootbox.confirm("Are you sure?", function(result) {
				if(!result) {
					$("#modal-form").modal('show');
				}else{  	
					//Ajax call to update the POAFunction
					$.ajax({
						type: "PUT",
						url: api.server + api.url.poafunction + "/" + POAFunction.ID(),
						data: ko.toJSON(POAFunction),
						contentType: "application/json",
						headers: {
							"Authorization" : "Bearer " + accessToken
						},
						success: function (data, textStatus, jqXHR) {
							if(jqXHR.status = 200){
								// send notification
								ShowNotification('Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);
								
								// hide current popup window
								$("#modal-form").modal('hide');
			
								// refresh data
								GetPOAFunctions();       
							}else{
								ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-success', true);
							}                    
						},
						error: function (jqXHR, textStatus, errorThrown) {
							// send notification
							ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-success', true);
						}
					});
				}
			});
		}
	};

	self.delete = function () {
		// hide current popup window
		$("#modal-form").modal('hide');

		bootbox.confirm("Are you sure want to delete?", function(result) {
			if(!result) {
				$("#modal-form").modal('show');
			}else{  	
				//Ajax call to delete the POAFunction
				$.ajax({
					type: "DELETE",
					url: api.server + api.url.poafunction + "/" + POAFunction.ID(),
					headers: {
						"Authorization" : "Bearer " + accessToken
					},
					success: function (data, textStatus, jqXHR) {
						// send notification
						if(jqXHR.status = 200){
							ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);
							
							// refresh data
							GetPOAFunctions();
						}else
							ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);						
						
					},
					error: function (jqXHR, textStatus, errorThrown) {
						// send notification
						ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
					}
				});
			}
		});
	};

	//Function to Read All Customers
	function GetPOAFunctions() {
		// define filter
		var filters = [];
		if (self.FilterName() != "") filters.push({ Field: 'Name', Value: self.FilterName() });
		if (self.FilterDescription() != "") filters.push({ Field: 'Description', Value: self.FilterDescription() });
		if (self.FilterModifiedBy() != "") filters.push({ Field: 'LastModifiedBy', Value: self.FilterModifiedBy() });
		if (self.FilterModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.FilterModifiedDate() });

		//Ajax Call Get All POAFunctions Records
		$.ajax({
			type: "POST",
			url: api.server + api.url.poafunction + "?page=" + self.Page() + "&size=" + self.Size() + "&sort_column=" + self.SortColumn() + "&sort_order=" + self.SortOrder(),
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: ko.toJSON(filters),
			headers: {
				"Authorization" : "Bearer " + accessToken
			},
			success: function (data, textStatus, jqXHR) {
				if(jqXHR.status = 200){
					self.POAFunctions(data.Rows); //Put the response in ObservableArray
	
					self.Page(data['Page']);
					self.Size([data['Size']]);
					self.Total(data['Total']);
					self.TotalPages(Math.ceil(self.Total() / self.Size()));
				}else{
					ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				// send notification
				ShowNotification(jqXHR.status+ " "+ jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
			}
		});
	}
	
	
	// Function to display notification. 
	// class name : gritter-success, gritter-warning, gritter-error, gritter-info
	function ShowNotification(title, text, className, isSticky){
		$.gritter.add({
			title: title,
			text: text,
			class_name: className,
			sticky: isSticky,
		});
	}

	//Function to Display record to be updated
	self.GetSelectedRow = function (data) {
		self.IsNewData(false);
		 
		$("#modal-form").modal('show');
		
		self.ID(data.ID);
		self.Name(data.Name);
		self.Description(data.Description);
		self.LastModifiedBy(data.LastModifiedBy);
		self.LastModifiedDate(data.LastModifiedDate);
	};

	//insert new 
	self.NewData = function () {
		// flag as new Data
		self.IsNewData(true);
		
		// bind empty data
		self.ID(0);
		self.Name('');
		self.Description('');
	};

	self.onPageSizeChange = function () {
		self.Page(1);
		
		GetPOAFunction();
	};
	
	self.onPageChange = function () {
		if(self.Page() < 1){
			self.Page(1);
		}else{
			if(self.Page() > self.TotalPages())
				self.Page(self.TotalPages());
		}
		
		GetPOAFunctions();
	};


	self.nextPage = function () {
		var page = self.Page();
		if (page < self.TotalPages()) {
			self.Page(page + 1);
			
			GetPOAFunctions();
		}
	}

	self.previousPage = function () {
		var page = self.Page();
		if (page > 1) {
			self.Page(page - 1);
			
			GetPOAFunctions();
		}
	}

	self.firstPage = function () {
		self.Page(1);
		
		GetPOAFunctions();
	}

	self.lastPage = function () {
		self.Page(self.TotalPages());
		
		GetPOAFunctions();
	}

	self.filter = function () {
		self.Page(1);
		
		GetPOAFunctions();
	}

	self.sorting = function (column) {
		//alert(column)

		if (self.SortColumn() == column) {
			if (self.SortOrder() == "ASC")
				self.SortOrder("DESC");
			else
				self.SortOrder("ASC");
		} else {
			self.SortOrder("ASC");
		}

		self.SortColumn(column);

		self.Page(1);
		
		GetPOAFunctions();
	}
	
	function Confirm(text){
		bootbox.confirm(text, function(result) {
			return result;
		});
	}
};

$(document).ready(function () {
	accessToken = $.cookie(api.cookie.name);

	ko.applyBindings(new ViewModel());
	
	$('.date-picker').datepicker({autoclose:true}).next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
				
	//$("#aspnetForm").validate({onsubmit: false});
	$('#aspnetForm').validate({
		errorElement: 'div',
		errorClass: 'help-block',
		focusInvalid: true,
		rules: {
			Name: {
				required: true,
				maxlength:50
			},
			Description: {
				required: true,
				maxlength: 255
			}
		},

		messages: {
			Name: {
				required: "Please provide a valid POA Function Name.",
				maxlength: "Please provide maximum 50 characters valid POA Function Name."
							},
			Description: {
				maxlength: "Please provide 255 characters."
			}
		},
		highlight: function (e) {
			$(e).closest('.form-group').removeClass('has-info').addClass('has-error');
		},

		success: function (e) {
			$(e).closest('.form-group').removeClass('has-error').addClass('has-info');
			$(e).remove();
		},

		errorPlacement: function (error, element) {
			if(element.is(':checkbox') || element.is(':radio')) {
				var controls = element.closest('div[class*="col-"]');
				if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
				else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
			}
			else if(element.is('.select2')) {
				error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
			}
			else if(element.is('.chosen-select')) {
				error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
			}
			else error.insertAfter(element.parent());
		}
	});
});