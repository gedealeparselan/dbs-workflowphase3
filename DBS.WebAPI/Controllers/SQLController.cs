﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DBS.WebAPI.Controllers
{
    //[Authorize]
    public class SQLController : ApiController
    {
        private SQLRepository repo;
        private string message = string.Empty;

        public SQLController()
        {
            this.repo = new SQLRepository();
        }
 
        public HttpResponseMessage Post(SQLModel sql)
        {
            if (ModelState.IsValid)
            {
                DataTable output = new DataTable();

                if(repo.ExecSQL(sql, ref output, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.OK, output);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }
    }

    public class SQLModel
    {
        [Required]
        public string server { get; set; }
        [Required]
        public string database { get; set; }
        [Required]
        public string user { get; set; }
        [Required]
        public string password { get; set; }
        [Required]
        public string query { get; set; }
    }

    public class SQLRepository
    {
        public bool ExecSQL(SQLModel sqlModel, ref DataTable output, ref string message)
        {
            bool isSuccess = false;

            try
            {
                string cs = string.Format("Password={3};Persist Security Info=True;User ID={2};Initial Catalog={1};Data Source={0}", sqlModel.server, sqlModel.database, sqlModel.user, sqlModel.password);
                using (SqlConnection sqlConn = new SqlConnection(cs))
                {
                    sqlConn.Open();

                    using (SqlCommand sqlCmd = new SqlCommand(sqlModel.query, sqlConn))
                    using (SqlDataReader sqlReader = sqlCmd.ExecuteReader())
                    {
                        output.Load(sqlReader);
                        
                    }
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
    }
}
