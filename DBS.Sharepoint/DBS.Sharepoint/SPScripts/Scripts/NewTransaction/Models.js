﻿var accessToken;
var NewCustomerFXWarning = "Cannot proccess FX Transaction for New Customer Added.";

//ari 20160907 var to store aucomplete array
var autoCompleteData = [];
var obj = {};

/* dodit@2014.11.08:add format function */
var formatNumber = function (num) {
    if (num == null) {
        return;
    }
    var n = num.toString(), p = n.indexOf('.');
    return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
        return p < 0 || i < p ? ($0 + ',') : $0;
    });
}
//Andi, 22 October 2015
var DecodeLogin = function (username) {
    var index = username.lastIndexOf("|");
    var str = username.substring(index + 1);
    return str;
}
//End Andi
var formatDateValue = function (date, isDateOnly, isDateLong) {
    var DateToFormat = new Date(date);

    if (date == '1970/01/01 00:00:00' || date == null) {
        return "";
    }
    if (moment(DateToFormat).isValid()) {
        return moment(DateToFormat).format(config.format.date);
    } else {
        return "";
    }
};

var formatBeneAccNumber = function (num) {//Rizki - 2016-02-09

    num = num.replace(/-/g, '');

    sheet_a = (num.length > 0) ? num.substr(0, 4) : '';
    sheet_b = (num.length > 4) ? '-' + num.substr(4, 4) : '';
    sheet_c = (num.length > 8) ? '-' + num.substr(8) : '';

    return sheet_a + sheet_b + sheet_c;
}

var StatusIPE = {
    Value: ""
};

var $box;
var $remove = false;
var cifData = '0';
var customerNameData = '';
var DocumentModels = ko.observable({ FileName: '', DocumentPath: '' });
var PPUModel = { cif: cifData, token: accessToken }
var AmountResCalculate;

var SPRole = {
    ID: ko.observable(),
    Name: ko.observable()
};

var SPUser = {
    DisplayName: ko.observable(),
    Email: ko.observable(),
    ID: ko.observable(),
    LoginName: ko.observable(),
    Roles: ko.observableArray([SPRole])
};

var LLDDocument = {
    LLDDocumentID: ko.observable(),
    LLDDocumentCode: ko.observable(),
    Description: ko.observable()
};

var BizSegmentModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var BranchModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Code: ko.observable()
};

var CityModel = {
    CityID: ko.observable(),
    CityCode: ko.observable(),
    Description: ko.observable()
};

var TypeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var ProductModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Name: ko.observable(),
    WorkflowID: ko.observable(),
    Workflow: ko.observable()
};

var LLDModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};
var ValidationTransactionModel = {
    Cif: ko.observable(),
    ProductID: ko.observable(),
    CurrencyID: ko.observable(),
    Amount: ko.observable(),
    ApplicationDate: ko.observable(),
    ExecutionDate: ko.observable(),
    AccountNumber: ko.observable(),
    BeneAccount: ko.observable(),
    BeneName: ko.observable()
};
var UnderlyingDocModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var ChannelModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

//Tambahan IPE
var BeneficiaryCountryModel = {
    ID: ko.observable(),
    Code: ko.observable()
};
var TransactionRelationshipModel = {
    ID: ko.observable(),
    Code: ko.observable()
};
var CBGCustomerModel = {
    ID: ko.observable(),
    Code: ko.observable()
};
var TransactionUsingDebitSundryModel = {
    ID: ko.observable(),
    Code: ko.observable()
};
//End

var BankModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    BranchCode: ko.observable(),
    SwiftCode: ko.observable(),
    BankAccount: ko.observable(),
    Description: ko.observable(),
    CommonName: ko.observable(),
    Currency: ko.observable(),
    PGSL: ko.observable(),
    IBranchBank: ko.observable()
};

//Started by haqi
var GroupCheckBoxModel = {
    IsNameMaintenance: ko.observable(false),
    IsIdentityTypeMaintenance: ko.observable(false),
    IsNPWPMaintenance: ko.observable(false),
    IsMaritalStatusMaintenance: ko.observable(false),
    IsCorrespondenceMaintenance: ko.observable(false),
    IsIdentityAddressMaintenance: ko.observable(false),
    IsOfficeAddressMaintenance: ko.observable(false),
    IsCorrespondenseAddressMaintenance: ko.observable(false),
    IsPhoneFaxEmailMaintenance: ko.observable(false),
    IsNationalityMaintenance: ko.observable(false),
    IsFundSourceMaintenance: ko.observable(false),
    IsNetAssetMaintenance: ko.observable(false),
    IsMonthlyIncomeMaintenance: ko.observable(false),
    IsJobMaintenance: ko.observable(false),
    IsAccountPurposeMaintenance: ko.observable(false),
    IsMonthlyTransactionMaintenance: ko.observable(false),
    IsBeneficialOwner: ko.observable(false),

    IsTujuanBukaRekeningLainnya: ko.observable(false),
    IsSumberDanaLainnya: ko.observable(false),
    IsPekerjaanProfesional: ko.observable(false),
    IsPekerjaanLainnya: ko.observable(false)
}

var BrachRiskRatingModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var CustomerModelModal = {
    CIF: ko.observable(),
    Name: ko.observable()
};

// Tambah Agung
var JoinModel = {
    ID: ko.observable(),
    Name: ko.observable()
};
//End

var FFDAccountModel = {
    AccountNumber: ko.observable(),
    IsAddFFDAccount: ko.observable(false)
};

var DormantAccountModel = {
    AccountNumber: ko.observable(),
    IsAddCurrencyDormant: ko.observable(false)
};

var MaintenanceTypeModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var TransactionSubTypeModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var ParameterSystemModel = {
    ID: ko.observable(),
    Name: ko.observable()
}

var ModificationData = [{ ID: 1, Name: 'Add' }, { ID: 2, Name: 'Edit' }, { ID: 3, Name: 'Delete' }]
var DispatchModeData = [{ ID: 1, Name: 'BSPL Delivery and Email' }, { ID: 2, Name: 'Collect by Person' }, { ID: 3, Name: 'Email' }, { ID: 4, Name: 'No Dispatch' }, { ID: 5, Name: 'Post' }, { ID: 6, Name: 'SSPL Delivery' }]
var RiskRatingResultData = [{ ID: 1, Name: 'Low Risk' }, { ID: 2, Name: 'Medium Risk' }, { ID: 3, Name: 'High Risk' }]
var MaritalStatusData = [{ ID: 1, Name: 'Belum Menikah' }, { ID: 2, Name: 'Duda / Janda' }, { ID: 3, Name: 'Sudah Menikah' }]

var RetailCIFCBOModel = {
    //DispatchModeTypeID: ko.observable(),
    AccountNumber: ko.observable(),
    GroupCheckBox: ko.observable(GroupCheckBoxModel),
    Name: ko.observable(),
    IdentityNumber: ko.observable(),
    IdentityStartDate: ko.observable(),
    IdentityEndDate: ko.observable(),
    IdentityAddress: ko.observable(),
    IdentityKelurahan: ko.observable(),
    IdentityKecamatan: ko.observable(),
    IdentityCity: ko.observable(),
    IdentityProvince: ko.observable(),
    IdentityCountry: ko.observable(),
    IdentityPostalCode: ko.observable(),
    NPWPNumber: ko.observable(),
    IsNPWPReceived: ko.observable(),
    MaritalStatusID: ko.observable(ParameterSystemModel),
    SpouseName: ko.observable(),
    IsCorrespondenseToEmail: ko.observable(),
    CorrespondenseAddress: ko.observable(),
    CorrespondenseKelurahan: ko.observable(),
    CorrespondenseKecamatan: ko.observable(),
    CorrespondenseCity: ko.observable(),
    CorrespondenseProvince: ko.observable(),
    CorrespondenseCountry: ko.observable(),
    CorrespondensePostalCode: ko.observable(),
    CellPhoneMethodID: ko.observable(ParameterSystemModel),
    CellPhone: ko.observable(),
    UpdatedCellPhone: ko.observable(),
    HomePhoneMethodID: ko.observable(ParameterSystemModel),
    HomePhone: ko.observable(),
    UpdatedHomePhone: ko.observable(),
    OfficePhoneMethodID: ko.observable(ParameterSystemModel),
    OfficePhone: ko.observable(),
    UpdatedOfficePhone: ko.observable(),
    FaxMethodID: ko.observable(ParameterSystemModel),
    Fax: ko.observable(),
    UpdatedFax: ko.observable(),
    EmailAddress: ko.observable(),
    OfficeAddress: ko.observable(),
    OfficeKelurahan: ko.observable(),
    OfficeKecamatan: ko.observable(),
    OfficeCity: ko.observable(),
    OfficeProvince: ko.observable(),
    OfficeCountry: ko.observable(),
    OfficePostalCode: ko.observable(),
    Nationality: ko.observable(),
    UBOName: ko.observable(),
    UBOIdentityType: ko.observable(),
    UBOPhone: ko.observable(),
    UBOJob: ko.observable(),
    CompanyName: ko.observable(),
    Position: ko.observable(),
    WorkPeriod: ko.observable(),
    IndustryType: ko.observable(),
    //Parameter System
    IdentityTypeID: ko.observable(ParameterSystemModel),
    FundSource: ko.observable(ParameterSystemModel),
    NetAsset: ko.observable(ParameterSystemModel),
    MonthlyIncome: ko.observable(ParameterSystemModel),
    MonthlyExtraIncome: ko.observable(ParameterSystemModel),
    Job: ko.observable(ParameterSystemModel),
    AccountPurpose: ko.observable(ParameterSystemModel),
    IncomeForecast: ko.observable(ParameterSystemModel),
    OutcomeForecast: ko.observable(ParameterSystemModel),
    TransactionForecast: ko.observable(ParameterSystemModel),
    //Risk rating form
    ReportDate: ko.observable(),
    NextReviewDate: ko.observable(),
    RiskRatingResult: ko.observable(ParameterSystemModel),
    ATMNumber: ko.observable(),
    DispatchModeType: ko.observable(ParameterSystemModel),
    HubunganNasabah: ko.observable(),

    //dani 8-4-2016
    SumberDanaLainnya: ko.observable(),
    PekerjaanProfesional: ko.observable(),
    PekerjaanLainnya: ko.observable(),
    TujuanBukaRekeningLainnya: ko.observable(),
    //end dani

    //dani 20-5-2016
    CellPhoneMethodID: ko.observable(ParameterSystemModel),
    HomePhoneMethodID: ko.observable(ParameterSystemModel),
    OfficePhoneMethodID: ko.observable(ParameterSystemModel),
    FaxMethodID: ko.observable(ParameterSystemModel)
    //end dani
};
//end by Haqi

var RMModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    RankID: ko.observable(),
    Rank: ko.observable(),
    SegmentID: ko.observable(),
    Segment: ko.observable(),
    BranchID: ko.observable(),
    Branch: ko.observable(),
    Email: ko.observable()
};

var ContactModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    PhoneNumber: ko.observable(),
    DateOfBirth: ko.observable(),
    Address: ko.observable(),
    IDNumber: ko.observable()
};

var FunctionModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var CurrencyModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};
var ChargingAccountCurrencyModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};
//henggar
var SundryModel = {
    ID: ko.observable(),
    OABAccNo: ko.observable()
};

var NostroModel = {
    ID: ko.observable(),
    NostroUsed: ko.observable()
};
//end
/*var AccountModel = {
    AccountNumber: ko.observable(),
    Currency: ko.observable(CurrencyModel)
};*/

var PaymentMode = {
    ModeID: ko.observable(),
    ModeName: ko.observable(),
    Status: ko.observable()
};

var AccountModel = {
    AccountNumber: ko.observable(),
    CustomerName: ko.observable(),
    Currency: ko.observable(CurrencyModel),
    IsJointAccount: ko.observable()
};
var CustomerDraftModel = {
    DraftCIF: ko.observable(),
    DraftCustomerName: ko.observable(),
    DraftAccountNumber: ko.observable(AccountModel)
}

var ProductTypeModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    IsFlowValas: ko.observable(),
    Description: ko.observable()
};
//Agung
var FNACoreModel = {
    ID: ko.observable(),
    Name: ko.observable()
};
var FunctionTypeModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var AccountTypeModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var Transaction_TypeModel = {
    TransTypeID: ko.observable(),
    TransactionTypeName: ko.observable()
};
//End Agung
var UnderlyingDocumentModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

var DocumentTypeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var DocumentPurposeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var FXComplianceModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};

var ChargesModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};

var ThresholdModel = {
    IsHitThreshold: ko.observable(),
    ThresholdValue: ko.observable(),
    RoundingValue: ko.observable(),
    TransactionType: ko.observable(),
    ResidentType: ko.observable()
}

var UnderlyingModel = {
    ID: ko.observable(),
    UnderlyingDocument: ko.observable(UnderlyingDocumentModel),
    DocumentType: ko.observable(DocumentTypeModel),
    Currency: ko.observable(CurrencyModel),
    Amount: ko.observable(),
    AttachmentNo: ko.observable(),
    IsStatementLetter: ko.observable(),
    IsDeclarationOfException: ko.observable(),
    StartDate: ko.observable(),
    EndDate: ko.observable(),
    DateOfUnderlying: ko.observable(),
    ExpiredDate: ko.observable(),
    ReferenceNumber: ko.observable(),
    SupplierName: ko.observable(),
    InvoiceNumber: ko.observable()
};

var DocumentModel = {
    ID: ko.observable(),
    Type: ko.observable(DocumentTypeModel),
    Purpose: ko.observable(DocumentPurposeModel),
    FileName: ko.observable(),
    DocumentPath: ko.observable(),
    LastModifiedDate: ko.observable(new Date),
    LastModifiedBy: ko.observable()

};

var CustomerModel = {
    CIF: ko.observable(),
    Name: ko.observable(),
    POAName: ko.observable(),
    BizSegment: ko.observable(BizSegmentModel),
    Branch: ko.observable(BranchModel),
    Type: ko.observable(TypeModel),
    RM: ko.observable(RMModel),
    Contacts: ko.observableArray([ContactModel]),
    Functions: ko.observableArray([FunctionModel]),
    Accounts: ko.observableArray([AccountModel]),
    Underlyings: ko.observableArray([UnderlyingModel])
};

var BeneficiaryBusinesModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
}

var FundListModel = {
    ID: ko.observable(),
    OffOnshoreModel: ko.observable(),
    FundCode: ko.observable(),
    FundName: ko.observable(),
    AccountNo: ko.observable(),
    SavingPlanDate: ko.observable(),
    IsSwitching: ko.observable(),
    IsSaving: ko.observable(),
    IsDeleted: ko.observable(),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
};
var InvestmentListModel = {
    InvestmentID: ko.observable(),
    Investment: ko.observable(),
    CIF: ko.observable(),
    IsUT: ko.observable(),
    IsBond: ko.observable(),
    STNumber: ko.observable(),
    CheckIN: ko.observable(),
    CheckCIF: ko.observable(),
    CheckName: ko.observable(),
    CheckST: ko.observable(),
}
var SolIDModel = {
    SolID: ko.observable(),
    ID: ko.observable(),
    Name: ko.observable(),
    Code: ko.observable()
}
//Andi, 22 October 2015
var TransactionTypeModel = {
    TransTypeID: ko.observable(),
    TransactionTypeName: ko.observable(),
    ProductID: ko.observable(),
    ProductName: ko.observable()
};
var FDRemarksModel = {
    ID: ko.observable(),
    Name: ko.observable()
};

//End Andi
var LimitProductModel = {
    TransactionLimitProductID: ko.observable(),
    MinAmount: ko.observable(),
    MaxAmount: ko.observable(),
    Unlimited: ko.observable(),
    Product: ko.observable(ProductModel),
    Currency: ko.observable(CurrencyModel)
};
//Tambah Tag Untag
var TagUntagModel = {
    ID: ko.observable(),
    Name: ko.observable()
};
//End
var MidrateDataModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable(),
    RupiahRate: ko.observable()
};

// added model form underlying -----------
var GridPropertiesModel = function (callback) {
    var self = this;

    self.SortColumn = ko.observable();
    self.SortOrder = ko.observable();
    self.AllowFilter = ko.observable(false);
    self.AvailableSizes = ko.observableArray(config.sizes);
    self.Page = ko.observable(1);
    self.Size = ko.observable(config.sizeDefault);
    self.Total = ko.observable(0);
    self.TotalPages = ko.observable(0);

    self.Callback = function () {
        callback();
    };

    self.OnPageSizeChange = function () {
        self.Page(1);

        self.Callback();
    };

    self.OnPageChange = function () {
        if (self.Page() < 1) {
            self.Page(1);
        } else {
            if (self.Page() > self.TotalPages())
                self.Page(self.TotalPages());
        }

        self.Callback();
    };

    self.NextPage = function () {
        var page = self.Page();
        if (page < self.TotalPages()) {
            self.Page(page + 1);

            self.Callback();
        }
    };

    self.PreviousPage = function () {
        var page = self.Page();
        if (page > 1) {
            self.Page(page - 1);

            self.Callback();
        }
    };

    self.FirstPage = function () {
        self.Page(1);

        self.Callback();
    };

    self.LastPage = function () {
        self.Page(self.TotalPages());

        self.Callback();
    };

    self.Filter = function (data) {
        self.Page(1);

        self.Callback();
    };

    // get sorted column
    self.GetSortedColumn = function (columnName) {
        var sort = "sorting";

        if (self.SortColumn() == columnName) {
            if (self.SortOrder() == "DESC")
                sort = sort + "_asc";
            else
                sort = sort + "_desc";
        }

        return sort;
    };

    // Sorting data
    self.Sorting = function (column) {
        if (self.SortColumn() == column) {
            if (self.SortOrder() == "ASC")
                self.SortOrder("DESC");
            else
                self.SortOrder("ASC");
        } else {
            self.SortOrder("ASC");
        }

        self.SortColumn(column);

        self.Page(1);

        self.Callback();
    };
};
var SelectModel = function (cif, id) {
    var self = this;
    self.ID = ko.observable(0);
    self.CIF = ko.observable(cif);
    self.UnderlyingID = ko.observable(id);
    self.ParentID = ko.observable(0);
}
var SelectBulkModel = function (cif, id) {
    var self = this;
    self.ID = ko.observable(0);
    self.CIF = ko.observable(cif);
    self.UnderlyingID = ko.observable(id);
    self.ParentID = ko.observable(0);
}
var UnderlyingDocumentModel2 = function (id, name, code) {
    var self = this;
    self.ID = ko.observable(id);
    self.Code = ko.observable(code);
    self.Name = ko.observable(name);
    self.CodeName = ko.observable(code + ' (' + name + ')');
}
var DocumentTypeModel2 = function (id, name) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
}
var StatementLetterModel = function (id, name) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
}
var CurrencyModel2 = function (id, code, description) {
    var self = this;
    self.ID = ko.observable(id);
    self.Code = ko.observable(code);
    self.Description = ko.observable(description);
}

var ProductTypeModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    IsFlowValas: ko.observable(),
    Description: ko.observable()
};

var DocumentPurposeModel2 = function (id, name, description) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
    self.Description = ko.observable(description);
}

var CustomerUnderlyingMappingModel = function (FileID, UnderlyingID, attachNo) {
    var self = this;
    self.ID = ko.observable(0);
    self.UnderlyingID = ko.observable(UnderlyingID);
    self.UnderlyingFileID = ko.observable(FileID);
    self.AttachmentNo = ko.observable(attachNo);
}

var SelectUtillizeModel = function (id, enable, uSDAmount, statementLetter) {
    var self = this;
    self.ID = ko.observable(id);
    self.Enable = ko.observable(enable);
    self.USDAmount = ko.observable(uSDAmount);
    self.StatementLetter = ko.observable(statementLetter);
}
var today = Date.now();
//Andi
var FDModel = {
    WorkflowInstanceID: ko.observable(),
    ApplicationID: ko.observable(),
    Customer: ko.observable(CustomerModel),
    Product: ko.observable(ProductModel),
    IsTopUrgent: ko.observable(0),
    IsTopUrgentChain: ko.observable(false),
    IsNormal: ko.observable(false),
    ApplicationDate: ko.observable(moment(today).format(config.format.date)),
    ExecutionDate: ko.observable(moment(today).format(config.format.date)),
    IsDraft: ko.observable(false),
    IsNewCustomer: ko.observable(false),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    ID: ko.observable(),
    TransactionID: ko.observable(),
    Channel: ko.observable(ChannelModel),
    TransactionType: ko.observable(TransactionTypeModel),
    FDAccNumber: ko.observable(""),
    Currency: ko.observable(CurrencyModel),
    Amount: ko.observable(0),
    CreditAccNumber: ko.observable(""),
    DebitAccNumber: ko.observable(""),
    InterestRate: ko.observable(0),
    Tenor: ko.observable(0),
    ValueDate: ko.observable(),
    MaturityDate: ko.observable(),
    Remarks: ko.observable(FDRemarksModel),
    FDBankName: ko.observable(""),
    Documents: ko.observableArray([]),
    AttachmentRemarks: ko.observable(),
    IsBringupTask: ko.observable(false)
}
//End Andi
var formatAccount = function () {
    var num = document.getElementById("bene-acc-number").value;
    num = num.replace(/-/g, '');

    viewModel.TransactionModel().BeneAccNumber(num);

    sheet_a = (num.length > 0) ? num.substr(0, 4) : '';
    sheet_b = (num.length > 4) ? '-' + num.substr(4, 4) : '';
    sheet_c = (num.length > 8) ? '-' + num.substr(8) : '';

    viewModel.BeneAccNumberMask(sheet_a + sheet_b + sheet_c);
    viewModel.OnChangedBene();
}
//Dani
var TransactionTMOModel = {
    IsTopUrgent: ko.observable(0),
    IsResident: ko.observable(false),
    IsCitizen: ko.observable(false),
    IsSignatureVerified: ko.observable(false),
    IsDormantAccount: ko.observable(false),
    IsFreezeAccount: ko.observable(false),
    Rate: ko.observable(0.00),
    AmountUSD: ko.observable(0.00),
    BeneName: ko.observable("-"),
    Bank: ko.observable(BankModel),
    BizSegment: ko.observable(BizSegmentModel),
    DebitCurrency: ko.observable(CurrencyModel),
    ApplicationDate: ko.observable(moment(today).format(config.format.date)),

    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    Account: ko.observable(AccountModel),
    IsOtherAccountNumber: ko.observable(false),
    OtherAccountNumber: ko.observable(),
    Customer: ko.observable(CustomerModel),
    Product: ko.observable(ProductModel),
    Currency: ko.observable(CurrencyModel),
    Amount: ko.observable(0),
    Channel: ko.observable(ChannelModel),
    ValueDate: ko.observable(moment(today).format(config.format.date)),
    DealNumber: ko.observable(""),
    CustomerDraft: ko.observable(CustomerDraftModel),
    Remarks: ko.observable(""),
    IsDraft: ko.observable(false),
    IsNewCustomer: ko.observable(false),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([]),
    AttachmentRemarks: ko.observable()
}
//end Dani

var TransactionLoanModel = {
    IsTopUrgent: ko.observable(0),
    IsTopUrgentChain: ko.observable(false),
    IsNormal: ko.observable(false),
    ApplicationDate: ko.observable(moment(today).format(config.format.date)),
    ExecutionDate: ko.observable(moment(today).format(config.format.date)),
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    Customer: ko.observable(CustomerModel),
    Product: ko.observable(ProductModel),
    Currency: ko.observable(CurrencyModel),
    BizSegment: ko.observable(BizSegmentModel),
    Amount: ko.observable(),
    Channel: ko.observable(ChannelModel),
    ValueDate: ko.observable(moment(today).format(config.format.date)),
    IsDraft: ko.observable(false),
    IsNewCustomer: ko.observable(false),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([])
}
function SetDate(dt, ext) {
    var dt2 = dt.setFullYear(dt.getFullYear() + ext);
    return dt2;
}

function SetDate2(dt, th, tgl) {
    var dt1 = dt.setFullYear(dt.getFullYear() + th);
    var dt2 = new Date(dt1);
    var dt3 = dt.setDate(dt2.getDate() - 1);
    var dt4 = new Date(dt3);
    return dt4;
}

var TransactionUTModel = {
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    Customer: ko.observable(CustomerModel),
    Product: ko.observable(ProductModel),
    FNACore: ko.observable([FNACoreModel]),
    FunctionType: ko.observable([FunctionTypeModel]),
    AccountType: ko.observable([AccountTypeModel]),
    SolID: ko.observable(),
    CustomerRiskEffectiveDate: ko.observable(moment(new Date).format(config.format.date)),
    RiskScore: ko.observable(),
    RiskProfileExpiryDate: ko.observable(moment(SetDate2(new Date, 1, 1)).format(config.format.date)),
    OperativeAccount: ko.observable(),
    Transaction_Type: ko.observable([Transaction_TypeModel]),
    Investment: ko.observable(),
    CustomerDraft: ko.observable(CustomerDraftModel),
    Remarks: ko.observable(""),
    IsDraft: ko.observable(false),
    IsNewCustomer: ko.observable(false),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([]),
    MutualFundForms: ko.observableArray([]),
    MutualFund: ko.observable(),
    MutualFundCode: ko.observable(),
    MutualCurrency: ko.observable(),
    MutualAmount: ko.observable(),
    MutualSelected: ko.observable(),
    MutualPartial: ko.observable(),
    MutualUnitNumber: ko.observable(),
    MutualFundSwitchFrom: ko.observable(),
    MutualFundCodeSwitchFrom: ko.observable(),
    MutualFundSwitchTo: ko.observable(),
    MutualFundCodeSwitchTo: ko.observable(),

    UTJoin: ko.observableArray([]),
    Join: ko.observable(JoinModel),
    CustomerJoin: ko.observable(CustomerModel),
    SolIDJoin: ko.observable(),
    CustomerRiskEffectiveDateJoin: ko.observable(moment(today).format(config.format.date)),
    RiskScoreJoin: ko.observable(),
    RiskProfileExpiryDateJoin: ko.observable(moment(today).format(config.format.date)),

    MutualFundList: ko.observable(FundListModel),
    MutualFundSwitchFrom: ko.observable(FundListModel),
    MutualFundSwitchTo: ko.observable(FundListModel),
    InvestmentList: ko.observable(InvestmentListModel),
    SolIDList: ko.observable(SolIDModel),
    IsBringupTask: ko.observable(false),
    AttachmentRemarks: ko.observable()
}

var TransactionModel = {
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    IsTopUrgent: ko.observable(false),
    IsTopUrgentChain: ko.observable(false),
    IsNormal: ko.observable(false),
    Customer: ko.observable(CustomerModel),
    IsNewCustomer: ko.observable(false),
    Product: ko.observable(ProductModel),
    ProductType: ko.observable(ProductTypeModel),
    LLD: ko.observable(LLDModel),
    UnderlyingDoc: ko.observable(UnderlyingDocModel),
    OtherUnderlyingDoc: ko.observable(),
    Currency: ko.observable(CurrencyModel),
    Sundry: ko.observable(SundryModel),
    Nostro: ko.observable(NostroModel),
    Amount: ko.observable(0),
    Rate: ko.observable(),
    TrxRate: ko.observable(),
    AmountUSD: ko.observable(0.00),
    Channel: ko.observable(ChannelModel),
    Account: ko.observable(AccountModel),
    ApplicationDate: ko.observable(moment(today).format(config.format.date)),
    ExecutionDate: ko.observable(moment(today).format(config.format.date)),
    BizSegment: ko.observable(BizSegmentModel),
    Bank: ko.observable(BankModel),
    BankCharges: ko.observable(ChargesModel),
    AgentCharges: ko.observable(ChargesModel),
    Type: ko.observable(20),
    IsResident: ko.observable(true),
    IsCitizen: ko.observable(true),
    PaymentDetails: ko.observable(null),
    IsSignatureVerified: ko.observable(false),
    IsDormantAccount: ko.observable(false),
    IsFreezeAccount: ko.observable(false),
    Others: ko.observable(null),
    IsDraft: ko.observable(false),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([]),
    DetailCompliance: ko.observable(),
    BeneName: ko.observable(),
    BeneAccNumber: ko.observable(),
    TZNumber: ko.observable(),
    TotalTransFX: ko.observable(0.00),
    TotalUtilization: ko.observable(0.00),
    utilizationAmount: ko.observable(0.00),
    underlyings: ko.observableArray([]),
    IsOtherBeneBank: ko.observable(true),
    OtherBeneBankName: ko.observable(),
    OtherBeneBankSwift: ko.observable(),
    UnderlyingDocID: ko.observable(),
    CustomerDraft: ko.observable(CustomerDraftModel),
    IsOtherAccountNumber: ko.observable(false),
    OtherAccountNumber: ko.observable(),
    DebitCurrency: ko.observable(CurrencyModel),
    IsJointAccount: ko.observable(),
    BeneficiaryBusines: ko.observable(BeneficiaryBusinesModel),
    IsBeneficiaryResident: ko.observable(null),
    AttachmentRemarks: ko.observable(),
    //IPE
    LLDDocument: ko.observable(LLDDocument),
    LLDUnderlyingAmount: ko.observable(),
    ChargingAccountName: ko.observable(),
    ChargingAccountBank: ko.observable(),
    BeneficiaryAddress: ko.observable(),
    AccountNumber: ko.observable(),
    Compliance: ko.observable(FXComplianceModel),
    CCY: ko.observable(),
    BranchID: ko.observable(),
    CityID: ko.observable(),
    ChargingACCNumber: ko.observable(),
    PaymentMode: ko.observable(PaymentMode),
    ChargingAccountCurrency: ko.observable(),
    PaymentModeStatus: ko.observable(),
    selectedOptionId: ko.observable(null),
    ModePayment: ko.observable(),
    Branch: ko.observable(BranchModel),
    City: ko.observable(CityModel),
    //ChargingAccountCurrency: ko.observable(ChargingAccountCurrencyModel),
    //Tambah Ipe
    BeneficiaryCountry: ko.observable(BeneficiaryCountryModel),
    TransactionRelationship: ko.observable(TransactionRelationshipModel),
    CBGCustomer: ko.observable(CBGCustomerModel),
    TransactionUsingDebitSundry: ko.observable(TransactionUsingDebitSundryModel),
    IsUnderlyingUtilizeStatus: ko.observable(),
    //aridya add 20161011 for SKN BULK ~OFFLINE~
    Remarks: ko.observable()
};

var ParameterUT = {
    FNACores: ko.observableArray([FNACoreModel]),
    FunctionTypes: ko.observableArray([FunctionTypeModel]),
    AccountTypes: ko.observableArray([AccountTypeModel]),
    TransactionTypes: ko.observableArray([Transaction_TypeModel]),
    Joins: ko.observableArray([{ ID: false, Name: "And" }, { ID: true, Name: "Or" }])
}

var SelectedDDL = {
    IdentityTypeIDs: ko.observable(null),
    IdentityTypeIDs: ko.observable(null),
    FundSources: ko.observable(null),
    NetAssets: ko.observable(null),
    MonthlyIncomes: ko.observable(null),
    MonthlyExtraIncomes: ko.observable(null),
    Jobs: ko.observable(null),
    AccountPurposes: ko.observable(null),
    IncomeForecasts: ko.observable(null),
    OutcomeForecasts: ko.observable(null),
    TransactionForecasts: ko.observable(null),
    RequestType: ko.observable(null),
    SubType: ko.observable(null),
};
var CIFTransactionModel = {
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    Customer: ko.observable(CustomerModel),
    CustomerNameRetailCIF: ko.observable(),
    CustomerCIFRetailCIF: ko.observable(),
    IsNewCustomer: ko.observable(false),
    Product: ko.observable(ProductModel),
    TransactionType: ko.observable(TransactionTypeModel),
    MaintenanceType: ko.observable(MaintenanceTypeModel),
    TransactionSubType: ko.observable(TransactionSubTypeModel),
    DocumentsCIF: ko.observableArray([]),
    AddJoinTableCustomerCIF: ko.observableArray([]),
    AddJoinTableFFDAcountCIF: ko.observableArray([]),
    AddJoinTableAccountCIF: ko.observableArray([]),
    AddJoinTableDormantCIF: ko.observableArray([]),
    AddJoinTableFreezeUnfreezeCIF: ko.observableArray([]),
    Currency: ko.observable(CurrencyModel),
    IsDraft: ko.observable(false),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    StaffTagging: ko.observable(TagUntagModel),
    StaffID: ko.observable(),
    ATMNumber: ko.observable(),
    RetailCIFCBO: ko.observable(RetailCIFCBOModel),
    IsLOI: ko.observable(),
    IsPOI: ko.observable(),
    IsPOA: ko.observable(),
    BrachRiskRating: ko.observable(BrachRiskRatingModel),
    AccountNumberDormant: ko.observable(),
    BranchName: ko.observable(),
    AccountNumber: ko.observable(),
    IsBringupTask: ko.observable(false)
};
//End by Haqi

var Parameter = {
    Products: ko.observableArray([ProductModel]),
    ProductType: ko.observableArray([ProductTypeModel]),
    Currencies: ko.observableArray([CurrencyModel]),
    Sundries: ko.observableArray([SundryModel]),
    Nostroes: ko.observableArray([NostroModel]),
    DynamicNostroes: ko.observableArray([NostroModel]),
    Channels: ko.observableArray([ChannelModel]),
    BizSegments: ko.observableArray([BizSegmentModel]),
    Banks: ko.observableArray([BankModel]),
    LLDs: ko.observableArray([LLDModel]),
    LLDDocuments: ko.observableArray([LLDDocument]),
    BankCharges: ko.observableArray([ChargesModel]),
    AgentCharges: ko.observableArray([ChargesModel]),
    FXCompliances: ko.observableArray([FXComplianceModel]),
    DocumentTypes: ko.observableArray([DocumentTypeModel]),
    DocumentPurposes: ko.observableArray([DocumentPurposeModel]),
    DocumentPurposesTMO: ko.observableArray([DocumentPurposeModel]),
    DocumentPurposesNonTMO: ko.observableArray([DocumentPurposeModel]),
    UnderlyingDocs: ko.observableArray([UnderlyingDocModel]),
    BeneficiaryBusiness: ko.observableArray([BeneficiaryBusinesModel]),
    TransactionType: ko.observableArray([TransactionTypeModel]),
    FDRemarks: ko.observableArray([FDRemarksModel]),
    TransactionTypes: ko.observableArray([TransactionTypeModel]),
    MaintenanceTypes: ko.observableArray([MaintenanceTypeModel]),
    TransactionSubTypes: ko.observableArray([TransactionSubTypeModel]),
    MaritalStatusIDs: ko.observableArray([ParameterSystemModel]),
    CellPhoneMethodIDs: ko.observableArray([ParameterSystemModel]),
    HomePhoneMethodIDs: ko.observableArray([ParameterSystemModel]),
    OfficePhoneMethodIDs: ko.observableArray([ParameterSystemModel]),
    FaxMethodIDs: ko.observableArray([ParameterSystemModel]),
    IdentityTypeIDs: ko.observableArray([ParameterSystemModel]),
    FundSources: ko.observableArray([ParameterSystemModel]),
    NetAssets: ko.observableArray([ParameterSystemModel]),
    MonthlyIncomes: ko.observableArray([ParameterSystemModel]),
    MonthlyExtraIncomes: ko.observableArray([ParameterSystemModel]),
    Jobs: ko.observableArray([ParameterSystemModel]),
    AccountPurposes: ko.observableArray([ParameterSystemModel]),
    IncomeForecasts: ko.observableArray([ParameterSystemModel]),
    OutcomeForecasts: ko.observableArray([ParameterSystemModel]),
    TransactionForecasts: ko.observableArray([ParameterSystemModel]),
    DispatchModeTypes: ko.observableArray([ParameterSystemModel]),
    RiskRatingResults: ko.observableArray([ParameterSystemModel]),
    DynamicTransactionSubTypes: ko.observableArray([TransactionSubTypeModel]),
    DynamicMaintenanceTypes: ko.observableArray([MaintenanceTypeModel]),
    DynamicTransactionTypes: ko.observableArray([TransactionTypeModel]),
    FXCompliance: ko.observableArray([FXComplianceModel]),
    PaymentModes: ko.observableArray([PaymentMode]),
    ChargingAccountCurrencies: ko.observableArray([ChargingAccountCurrencyModel]),
    BeneficiaryCountry: ko.observableArray([BeneficiaryCountryModel]),
    CBGCustomer: ko.observableArray([{ ID: false, Code: "No" }, { ID: true, Code: "Yes" }]),
    TransactionUsingDebitSundry: ko.observableArray([{ ID: false, Code: "No" }, { ID: true, Code: "Yes" }]),
    TransactionRelationship: ko.observableArray([TransactionRelationshipModel]),
    DynamicTagUntages: ko.observableArray([TagUntagModel])
};

var SelectedModel = {
    Product: ko.observable(),
    Currency: ko.observable(),
    //20150528-reizvan : Channel default "Original"
    Sundry: ko.observable(),
    Nostro: ko.observable(),
    Channel: ko.observable(),
    Account: ko.observable(),
    BizSegment: ko.observable(),
    Bank: ko.observable(),
    LLD: ko.observable(),
    LLDDocument: ko.observable(),
    UnderlyingDoc: ko.observable(),
    DocumentType: ko.observable(),
    DocumentPurpose: ko.observable(),
    NewCustomer: ko.observable({
        Currency: ko.observable(),
        BizSegment: ko.observable()
    }),
    BankCharges: ko.observable(2),
    AgentCharges: ko.observable(2),
    ProductType: ko.observable(),
    DebitCurrency: ko.observable(),
    ChooseProduct: ko.observable(),
    BeneficiaryBusines: ko.observable(),
    TransactionType: ko.observable(),
    FDRemarks: ko.observable(),
    Join: ko.observable(),
    FNACore: ko.observable(),
    FunctionType: ko.observable(),
    AccountType: ko.observable(),
    Transaction_Type: ko.observable(),
    TagUntag: ko.observable(),
    MaintenanceType: ko.observable(),
    TransactionSubType: ko.observable(),
    CellPhoneMethodID: ko.observable(),
    HomePhoneMethodID: ko.observable(),
    OfficePhoneMethodID: ko.observable(),
    FaxMethodID: ko.observable(),
    MaritalStatusID: ko.observable(),
    IdentityTypeID: ko.observable(),
    FundSource: ko.observable(),
    NetAsset: ko.observable(),
    MonthlyIncome: ko.observable(),
    MonthlyExtraIncome: ko.observable(),
    Job: ko.observable(),
    AccountPurpose: ko.observable(),
    IncomeForecast: ko.observable(),
    OutcomeForecast: ko.observable(),
    TransactionForecast: ko.observable(),
    DispatchModeType: ko.observable(),
    RiskRatingResult: ko.observable(),
    cifAccNumber: ko.observable(),
    FXCompliance: ko.observable(),
    PaymentMode: ko.observable(),
    PaymentModeStatus: ko.observable(),
    ChargingAccountCurrency: ko.observable(),
    BeneficiaryCountry: ko.observable(),
    CBGCustomer: ko.observable(),
    TransactionUsingDebitSundry: ko.observable(),
    TransactionRelationship: ko.observable()
    //end by haqi
};

var BankBranchModel = {
    CityCode: ko.observable(),
    CityDescription: ko.observable(),
    CityID: ko.observable(),
    Code: ko.observable(),
    ID: ko.observable(),
    Name: ko.observable(),
};