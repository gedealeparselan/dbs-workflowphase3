﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Web;
using DBS.Entity;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Models
{
    #region Property
    public class DocumentPurposeModel
    {
        /// <summary>
        /// PurposeDocs ID.
        /// </summary>
        public int ID { get; set; }
        public int Purpose { get; set; } // add azam
        /// <summary>
        /// PurposeDocss Name.
        /// </summary>
        [MaxLength(50, ErrorMessage = "Purpose of Docs Name is must be in {1} characters.")]
        public string Name { get; set; }

        /// <summary>
        /// PurposeDocs Description.
        /// </summary>
        [MaxLength(255, ErrorMessage = "Maximum length is {1} characters.")]
        public string Description { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }
    }

    public class PurposeDocsRow : DocumentPurposeModel
    {
        public int RowID { get; set; }

    }

    public class PurposeDocsGrid : Grid
    {
        public IList<PurposeDocsRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class PurposeDocsFilter : Filter { }
    #endregion

    #region Interface
    public interface IDocumentPurposeRepository : IDisposable
    {
        bool GetPurposeDocsByID(int id, ref DocumentPurposeModel purposeDocs, ref string message);
        bool GetPurposeDocs(ref IList<DocumentPurposeModel> purposeDocs, int limit, int index, ref string message);
        bool GetPurposeDocs(ref IList<DocumentPurposeModel> purposeDocs, ref string message);
        bool GetPurposeDocs(int page, int size, IList<PurposeDocsFilter> filters, string sortColumn, string sortOrder, ref PurposeDocsGrid purposeDocs, ref string message);
        bool GetPurposeDocs(PurposeDocsFilter filter, ref IList<DocumentPurposeModel> purposeDocs, ref string message);
        bool GetPurposeDocs(string key, int limit, ref IList<DocumentPurposeModel> purposeDocs, ref string message);
        bool AddPurposeDocs(DocumentPurposeModel purposeDocs, ref string message);
        bool UpdatePurposeDocs(int id, DocumentPurposeModel purposeDocs, ref string message);
        bool DeletePurposeDocs(int id, ref string message);
    }
    #endregion

    #region Repository
    public class PurposeDocsRepository : IDocumentPurposeRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetPurposeDocsByID(int id, ref DocumentPurposeModel purposeDocs, ref string message)
        {
            bool isSuccess = false;

            try
            {
                purposeDocs = (from a in context.DocumentPurposes
                               where a.IsDeleted.Equals(false) && a.PurposeID.Equals(id)
                               select new DocumentPurposeModel
                               {
                                   ID = a.PurposeID,
                                   Name = a.PurposeName,
                                   Description = a.PurposeDescription,
                                   LastModifiedDate = a.CreateDate,
                                   LastModifiedBy = a.CreateBy
                               }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetPurposeDocs(ref IList<DocumentPurposeModel> purposeDocs, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    purposeDocs = (from a in context.DocumentPurposes
                                    where a.IsDeleted.Equals(false)
                                    orderby new { a.PurposeName, a.PurposeDescription }
                                    select new DocumentPurposeModel
                                    {
                                        ID = a.PurposeID,
                                        Name = a.PurposeName,
                                        Description = a.PurposeDescription,
                                        LastModifiedBy = a.CreateBy,
                                        LastModifiedDate = a.CreateDate
                                    }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetPurposeDocs(ref IList<DocumentPurposeModel> purposeDocs, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    purposeDocs = (from a in context.DocumentPurposes
                                    where a.IsDeleted.Equals(false)
                                    orderby new { a.PurposeName, a.PurposeDescription }
                                    select new DocumentPurposeModel
                                    {
                                        ID = a.PurposeID,
                                        Name = a.PurposeName,
                                        Description = a.PurposeDescription,
                                        LastModifiedBy = a.CreateBy,
                                        LastModifiedDate = a.CreateDate
                                    }).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetPurposeDocs(int page, int size, IList<PurposeDocsFilter> filters, string sortColumn, string sortOrder, ref PurposeDocsGrid purposeDocsGrid, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                DocumentPurposeModel filter = new DocumentPurposeModel();
                if (filters != null)
                {
                    filter.Name = (string)filters.Where(a => a.Field.Equals("Name")).Select(a => a.Value).SingleOrDefault();
                    filter.Description = (string)filters.Where(a => a.Field.Equals("Description")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.DocumentPurposes
                                let isPurposeDocsName = !string.IsNullOrEmpty(filter.Name)
                                let isPurposeDocsDescription = !string.IsNullOrEmpty(filter.Description)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) &&
                                    (isPurposeDocsName ? a.PurposeName.Contains(filter.Name) : true) &&
                                    (isPurposeDocsDescription ? a.PurposeDescription.Contains(filter.Description) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new DocumentPurposeModel
                                {
                                    ID = a.PurposeID,
                                    Name = a.PurposeName,
                                    Description = a.PurposeDescription,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    purposeDocsGrid.Page = page;
                    purposeDocsGrid.Size = size;
                    purposeDocsGrid.Total = data.Count();
                    purposeDocsGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    purposeDocsGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new PurposeDocsRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            Name = a.Name,
                            Description = a.Description,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetPurposeDocs(PurposeDocsFilter filter, ref IList<DocumentPurposeModel> purposeDocs, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetPurposeDocs(string key, int limit, ref IList<DocumentPurposeModel> purposeDocs, ref string message)
        {
            bool isSuccess = false;

            try
            {
                purposeDocs = (from a in context.DocumentPurposes
                                where a.IsDeleted.Equals(false) &&
                                    a.PurposeName.Contains(key) || a.PurposeDescription.Contains(key)
                                orderby new { a.PurposeName, a.PurposeDescription }
                                select new DocumentPurposeModel
                                {
                                    Name = a.PurposeName,
                                    Description = a.PurposeDescription
                                }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddPurposeDocs(DocumentPurposeModel PurposeDocs, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (!context.DocumentPurposes.Where(a => a.PurposeName.Equals(PurposeDocs.Name) && a.IsDeleted.Equals(false)).Any())
                {
                    context.DocumentPurposes.Add(new Entity.DocumentPurpose()
                    {
                        PurposeName = PurposeDocs.Name,
                        PurposeDescription = PurposeDocs.Description,
                        IsDeleted = false,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    });

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Purpose of Docs data for Purpose of Docs Name {0} is already exist.", PurposeDocs.Name);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdatePurposeDocs(int id, DocumentPurposeModel purposeDocs, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.DocumentPurpose data = context.DocumentPurposes.Where(a => a.PurposeID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    if (context.DocumentPurposes.Where(a => a.PurposeID != purposeDocs.ID && a.PurposeName.Equals(purposeDocs.Name) && a.IsDeleted.Equals(false)).Count() >= 1)
                    {
                        message = string.Format("Purpose of Docs Name {0} is exist.", purposeDocs.Name);
                    }
                    else
                    {
                        data.PurposeName = purposeDocs.Name;
                        data.PurposeDescription = purposeDocs.Description;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        context.SaveChanges();

                        isSuccess = true;
                    }
                }
                else
                {
                    message = string.Format("Purpose of Docs data for Purpose of Docs ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeletePurposeDocs(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.DocumentPurpose data = context.DocumentPurposes.Where(a => a.PurposeID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Purpose of Docs data for Purpose of Docs ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}