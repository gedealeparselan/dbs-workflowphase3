$.cookie.json = true;

var TotalDealModel = {
    //RateIDR :0,
    //TreshHold: 0,
    //FCYIDRTrashHold: 0,
    Total : {
        IDRFCYDeal: 0,
        RemainingBalance : 0,
        UtilizationDeal: 0
    }
}

var TotalPPUModel ={
    //RateIDR :0,
    //TreshHold: 0,
    //FCYIDRTrashHold: 0,
    Total : {
        IDRFCYPPU: 0,
        UtilizationPPU: 0
    }
}

var DataModel = {
    RateIDR :0,
    ThresholdBuy: 0,
    ThresholdSell: 0
}

/*
function GetTreshHold(data,success,error)
{
    var requestAjax = $.ajax({
        type: "GET",
        url: api.server + api.url.helper +'/GetTreshHold',
        contentType: "application/json; charset=utf-8; odata=verbose;",
        dataType: "json",
        headers: {
            "Authorization" : "Bearer " + data.token
        },
        success: function (data, textStatus, jqXHR) {
            TotalFXModel.TreshHold = data;
            success(data, success, error);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            error(jqXHR, textStatus, errorThrown);
        }
    });
    // execute ajax request
    $.ajax(requestAjax);
}

function GetFCYIDRTrashHold(data,success,error)
{
    var requestAjax = $.ajax({
        type: "GET",
        url: api.server + api.url.helper +'/GetFCYIDRTrashHold',
        contentType: "application/json; charset=utf-8; odata=verbose;",
        dataType: "json",
        headers: {
            "Authorization" : "Bearer " + data.token
        },
        success: function (data, textStatus, jqXHR) {
            TotalFXModel.FcyIdrTrashHold =data;
            success(data, success, error);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            error(jqXHR, textStatus, errorThrown);
        }
    });
    // execute ajax request
    $.ajax(requestAjax);
}



function GetTotalIDRFCYDeal(data,success,error){
    var requestAjax = $.ajax({
        type: "GET",
        url: api.server + api.url.helper +'/GetTotalIDRFCYDeal/'+ data.cif,
        contentType: "application/json; charset=utf-8; odata=verbose;",
        dataType: "json",
        headers: {
            "Authorization" : "Bearer " + data.token
        },
        success: function (data, textStatus, jqXHR) {
            TotalFXModel.TotalIdrFcyDeal = data;
            success(data, success, error);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            error(jqXHR, textStatus, errorThrown);
        }
    });
    // execute ajax request
    $.ajax(requestAjax);
}

// Get UtilizeIDR-FCY Deal transaction
function GetTotalUtilizationDeal(data,success,error){
    var requestAjax = $.ajax({
        type: "GET",
        url: api.server + api.url.helper +'/GetUtilizeDeal/'+ data.cif,
        contentType: "application/json; charset=utf-8; odata=verbose;",
        dataType: "json",
        headers: {
            "Authorization" : "Bearer " + data.token
        },
        success: function (data, textStatus, jqXHR) {
            success(data, success, error);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            error(jqXHR, textStatus, errorThrown);
        }
    });
    // execute ajax request
    $.ajax(requestAjax);
}

function GetParameterData(data,callback,error){
    var requestRateIDR =  $.ajax({
        type: "GET",
        url: api.server + api.url.currency + "/CurrencyRate/" + "1",
        contentType: "application/json; charset=utf-8; odata=verbose;",
        dataType: "json",
        headers: {
            "Authorization" : "Bearer " + data.token
        },
        success: function (data, textStatus, jqXHR) {
            TotalDealModel.RateIDR = data.RupiahRate;
            TotalPPUModel.RateIDR = data.RupiahRate;
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });
    var requestTreshold = $.ajax({
        type: "GET",
        url: api.server + api.url.helper +'/GetTreshHold',
        contentType: "application/json; charset=utf-8; odata=verbose;",
        dataType: "json",
        headers: {
            "Authorization" : "Bearer " + data.token
        },
        success: function (data, textStatus, jqXHR) {
            TotalDealModel.TreshHold = data;
            TotalPPUModel.TreshHold = data;
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });
    var requestFCYIDRTrashHold = $.ajax({
        type: "GET",
        url: api.server + api.url.helper +'/GetFCYIDRTrashHold',
        contentType: "application/json; charset=utf-8; odata=verbose;",
        dataType: "json",
        headers: {
            "Authorization" : "Bearer " + data.token
        },
        success: function (data, textStatus, jqXHR) {
            TotalDealModel.FCYIDRTrashHold = data;
            TotalPPUModel.FCYIDRTrashHold = data;
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });

    $.when(requestRateIDR,requestTreshold,requestFCYIDRTrashHold).done(
        function(RateIDR,treshold,fcyIdrFycTrashold){
            DataModel.RateIDR = RateIDR[0].RupiahRate;
            DataModel.ThresholdBuy = treshold[0];
            DataModel.ThresholdSell = fcyIdrFycTrashold[0];
            callback(DataModel);
        });
} 
//change chandra : 2015-05-15
function GetTotalDeal(data, callback, error) {
    var requestAjax = $.ajax({
        type: "GET",
        url: api.server + api.url.helper + '/GetCalculateDeal/' + data.cif,
        contentType: "application/json; charset=utf-8; odata=verbose;",
        dataType: "json",
        headers: {
            "Authorization": "Bearer " + data.token
        },
        success: function (data, textStatus, jqXHR) {
            TotalDealModel.Total.IDRFCYDeal = data.Total.IDRFCYDeal;
            TotalDealModel.Total.UtilizationDeal = data.Total.UtilizationDeal;
            TotalDealModel.Total.RemainingBalance = data.Total.RemainingBalance;
            callback(data.AvailableUnderlying);
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });
    // execute ajax request
    $.ajax(requestAjax);
}

function GetRemainingBalance(data,callback,error){
    var requestTIDRFCYDealStatementB = $.ajax({
        type: "GET",
        url: api.server + api.url.helper +'/GetTotalIDRFCYDealStatementB/'+ data.cif,
        contentType: "application/json; charset=utf-8; odata=verbose;",
        dataType: "json",
        headers: {
            "Authorization" : "Bearer " + data.token
        },
        success: function (data, textStatus, jqXHR) {
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });

    var resultAvailableUnderlying_B = $.ajax({
        type: "GET",
        url: api.server + api.url.helper + "/GetAvailableStatementB/" + data.cif,
        contentType: "application/json",
        headers: {
            "Authorization": "Bearer "  + data.token
        },
        success: function (data, textStatus, jqXHR) {
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });

    $.when(requestTIDRFCYDealStatementB,resultAvailableUnderlying_B).done(function(tIDRFCYDealStatementB,availableUnderlying){
        TotalDealModel.Total.RemainingBalance = AmountFixed(availableUnderlying) - AmountFixed(tIDRFCYDealStatementB);


        callback();
    });
}

function GetTotalUtilizationPPU(data, success, error) {
    var requestAjax = $.ajax({
        type: "GET",
        url: api.server + api.url.helper + '/GetIDRFCYTransaction/' + data.cif,
        contentType: "application/json; charset=utf-8; odata=verbose;",
        dataType: "json",
        headers: {
            "Authorization": "Bearer " + data.token
        },
        success: function (data, textStatus, jqXHR) {
            success(data, success, error);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            error(jqXHR, textStatus, errorThrown);
        }
    });
    // execute ajax request
    $.ajax(requestAjax);
}


/* ------ Start Function for Calculate PPU (FX) */
//#region PBI MODE
// Get Total IDR-FCY PPU transaction
function GetTotalIDRFCYPPU(data, success, error) {
    var requestAjax = $.ajax({
        type: "GET",
        url: api.server + api.url.helper + '/GetIDRFCYPPU/' + data.cif,
        contentType: "application/json; charset=utf-8; odata=verbose;",
        dataType: "json",
        headers: {
            "Authorization": "Bearer " + data.token
        },
        success: function (data, textStatus, jqXHR) {
            TotalFXModel.TotalIdrFcyDeal = data;
            success(data, success, error);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            error(jqXHR, textStatus, errorThrown);
        }
    });
    // execute ajax request
    $.ajax(requestAjax);
}
// Get GetIDRFCYTransaction transaction (ALL Transaction)
function GetThresholdParameter(data, OnSuccess, OnError)
{
    var thresholdParameter = $.ajax({
        type: "GET",
        url: api.server + api.url.helper +'/ThresholdParameter',
        contentType: "application/json; charset=utf-8; odata=verbose;",
        dataType: "json",
        headers: {
            "Authorization" : "Bearer " + data.token
        },
        success: function (data, textStatus, jqXHR) {
            DataModel.RateIDR = data.RupiahRate;
            DataModel.ThresholdBuy = data.ThresholdBuy;
            DataModel.ThresholdSell = data.ThresholdSell;
            OnSuccess();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            OnError(jqXHR, textStatus, errorThrown);
        }
    });
    // execute ajax request
    $.ajax(thresholdParameter);

}
function GetTotalAllIDRFCY(data,success,errror){
    var requestAjax = $.ajax({
        type: "GET",
        url: api.server + api.url.helper +'/GetIDRFCYTransaction/'+ data.cif,
        contentType: "application/json; charset=utf-8; odata=verbose;",
        dataType: "json",
        headers: {
            "Authorization" : "Bearer " + data.token
        },
        success: function (data, textStatus, jqXHR) {
            success(data, success, error);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            error(jqXHR, textStatus, errorThrown);
        }
    });
    // execute ajax request
    $.ajax(requestAjax);
}
//#endregion
/*-------- End  Function for Calculate PPU (FX)  */
function GetTotalPPU(data, callback, error) {
    var requestAjax = $.ajax({
        type: "GET",
        url: api.server + api.url.helper + '/GetCalculatePPU/' + data.cif,
        contentType: "application/json; charset=utf-8; odata=verbose;",
        dataType: "json",
        headers: {
            "Authorization": "Bearer " + data.token
        },
        success: function (data, textStatus, jqXHR) {
            TotalPPUModel.Total.IDRFCYPPU = data.Total.IDRFCYPPU;
            TotalPPUModel.Total.UtilizationPPU = data.Total.UtilizationPPU;
            callback(TotalPPUModel, data.currentAmount);
        },
        error: function (jqXHR, textStatus, errorThrown) {
        }
    });
    // execute ajax request
    $.ajax(requestAjax);
}

function AmountFixed(value){
    if(value != null){
        if(value[0] != null){
           return parseFloat(value[0]);
        }
    }else
    {
        return 0;
    }
}