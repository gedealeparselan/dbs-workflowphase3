//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DBS.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class TransactionHistory
    {
        public long TransactionHistoryID { get; set; }
        public long TransactionID { get; set; }
        public string Activity { get; set; }
        public Nullable<int> UserName { get; set; }
        public System.DateTime LogDate { get; set; }
        public string Outcome { get; set; }
        public string Comments { get; set; }
        public bool IsHumanApproval { get; set; }
    
        public virtual Transaction Transaction { get; set; }
    }
}
