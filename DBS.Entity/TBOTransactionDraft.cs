//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DBS.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class TBOTransactionDraft
    {
        public long TBOTransactionDraftID { get; set; }
        public Nullable<System.Guid> WorkflowInstanceID { get; set; }
        public Nullable<long> TransactionID { get; set; }
        public Nullable<long> TransactionDocumentID { get; set; }
        public Nullable<int> TBOSLAID { get; set; }
        public Nullable<System.DateTime> TBOSubmissionDate { get; set; }
        public Nullable<System.DateTime> TBOActualDate { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string Status { get; set; }
        public Nullable<int> DocumentPurposeID { get; set; }
        public Nullable<int> DocumentTypeID { get; set; }
        public string TBOApplicationID { get; set; }
    }
}
