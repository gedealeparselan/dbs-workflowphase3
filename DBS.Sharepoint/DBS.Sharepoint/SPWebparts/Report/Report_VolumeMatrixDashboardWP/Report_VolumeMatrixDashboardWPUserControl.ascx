﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Report_VolumeMatrixDashboardWPUserControl.ascx.cs" Inherits="DBS.Sharepoint.SPWebparts.Report.Report_VolumeMatrixDashboardWP.Report_VolumeMatrixDashboardWPUserControl" %>

<div class="row alert alert-info" style="margin-bottom:5px;padding:10px;height:55px;">
    <div id="report-form" class="form-horizontal" role="form">
        <div class="form-group">
            <label class="col-sm-2 control-label bolder no-padding-right" for="product-type">Product Type</label>
            <div class="col-sm-6">
                <div class="clearfix">
                    <select id="product-type" name="product-type" data-bind="options: $root.ProductType(), optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Please Select...', value: $root.Selected().ProductType, event: { change: $root.OnProductTypeChange }"></select>                
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row" id="lblInfoPeriod" style="margin-bottom:10px;padding:10px;height:55px;"><label  id="lblDashboardReport" class="coll-sm4 pull-left"></label><label  id="lblLastUpdate" class="coll-sm4 pull-right"></label></div>
<label id="lblDateFromServer" style="display:none;"></label>
<div class="row">
<div class="col-xs-12">
<div class="row">
<div id="chartholder" class="col-sm-7">
    <div id="legendcluster"></div>
    <div id="chartcluster"></div>    
</div>
<div id="tableholder" class="col-sm-5"></div> <!--20161005 aridya hilangin  style="float:right"-->
</div>
</div>
</div>
<div id="modal-form" class="modal fade" tabindex="-1">
	<div class="modal-dialog" style="width:1000px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h4 class="blue bigger">
					<span id="spProductName"></span>
					
				</h4>
			</div>
			<div class="modal-body overflow-visible">
					<div class="row">
						<div class="col-xs-12">
						
						<!-- widget box start -->
						<div id="widget-box" class="widget-box">
							<!-- widget header start -->
							<div class="widget-header widget-hea1der-small header-color-dark">
								<h6>Transactions</h6>
						
								<div class="widget-toolbar">
									<label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
										<i class="blue icon-filter"></i>
										<input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: GridProperties().AllowFilter" />
										<span class="lbl"></span>
									</label>
									<a href="#" data-bind="click: GetData" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
										<i class="blue icon-refresh"></i>
									</a>
								</div>
							</div>
							<!-- widget header end -->
						
							<!-- widget body start -->
							<div class="widget-body">
								<!-- widget main start -->
								<div class="widget-main padding-0">
									<!-- widget slim control start -->
									<div class="slim-scroll" data-height="400">
										<!-- widget content start -->
										<div class="content">
											<div class="table-responsive">
												<div class="dataTables_wrapper" role="grid">
													<!--If the lenght of the Branch is greater than 0 then visible the Table-->
											        <table id="Transactions-table" class="table table-striped table-bordered table-hover dataTable">
											            <thead>
											                <tr data-bind="with: GridProperties">
											                    <th style="width:50px;">
																No.</th>
											                    <th data-bind="click: function () { Sorting('applicationIDTrx'); }, css: GetSortedColumn('applicationIDTrx')">
																Application ID </th>
																<th data-bind="click: function () { Sorting('customerNameTrx'); }, css: GetSortedColumn('customerNameTrx')">											                    
											                    Customer Name </th>
											                    <th data-bind="click: function () { Sorting('transactionStatusTrx'); }, css: GetSortedColumn('transactionStatusTrx')">
											                    Transaction 
																Status </th>
											                    <th style="width:100px;" data-bind="click: function () { Sorting('TATTrx'); }, css: GetSortedColumn('TATTrx')">
											                    TAT <span><span class="badge badge-success"></span></span></th>
											                    <th style="width:80px;" data-bind="click: function () { Sorting('percentageTrx'); }, css: GetSortedColumn('percentageTrx')">
											                    % <span><span class="badge badge-success"></span></span></th>						                    
											                    
											                </tr>
											            </thead>
											           	<thead data-bind="visible: GridProperties().AllowFilter" class="table-filter">
											                <tr>
																<th class="clear-filter">
						                                            <a href="#" data-bind="click: ClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
						                                                <i class="green icon-trash"></i>
						                                            </a>
						                                        </th>																
																<th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterApplicationID, event: { change: GridProperties().Filter } " /></th>
																<th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCustomerName, event: { change: GridProperties().Filter }" /></th>
																<th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterTransactionStatus, event: { change: GridProperties().Filter }" /></th>
																<th><input style="width:100px;" type="text" class="input-sm col-xs-12 input-numericOnly" data-bind="value: $root.FilterTAT, event: { change: GridProperties().Filter }" /></th>
																<th><input style="width:80px;" type="text" class="input-sm col-xs-12 input-numericOnly" data-bind="value: $root.FilterPercentage, event: { change: GridProperties().Filter }" /></th>
															</tr>

											            </thead>
											            <!--Iterate through an observableArray using foreach-->
														<tbody data-bind="foreach: Transactions, visible: Transactions().length > 0">
															<tr data-bind="click: $root.GetSelectedRow" data-toggle="modal">
																<td><span data-bind="text: RowID"></span></td>
																<td><span data-bind="text: applicationIDTrx"></span></td>
																<td><span data-bind="text: customerNameTrx"></span></td>
																<td><span data-bind="text: transactionStatusTrx == 1 ? 'Is Pending' : transactionStatusTrx == 2 ?'Completed' : 'Cancelled'"></span></td>
																<td><span data-bind="text: TATTrx + ' minute(s)'"></span></td>
																<td><span data-bind="text: percentageTrx + '%'"></span></td>
																
				
															</tr>
														</tbody>
											            <tbody data-bind="visible: Transactions().length == 0">
											            	<tr >
											            		<td colspan="6" class="text-center">
											            			No entries 
																	available.
											            		</td>
											            	</tr>
											            </tbody>
											        </table>
											      </div>
											   </div>
											</div>
										</div>
										        	
										<!-- widget footer start -->
			<div class="widget-toolbox padding-8 clearfix">
				<div class="row" data-bind="with: GridProperties">
					<!-- pagination size start -->
					<div class="col-sm-6">
						<div class="dataTables_paginate paging_bootstrap pull-left">
							Showing <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
							rows of <span data-bind="text: Total"></span>
							entries
						</div>
					</div>
					<!-- pagination size end -->

					<!-- pagination page jump start -->
					<div class="col-sm-3">
						<div class="dataTables_paginate paging_bootstrap">
							Page <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width:50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page"/>
							of <span data-bind="text: TotalPages"></span>
						</div>
					</div>
					<!-- pagination page jump end -->

					<!-- pagination navigation start -->
					<div class="col-sm-3">
						<div class="dataTables_paginate paging_bootstrap">
							<ul class="pagination">
								<li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
									<a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
										<i class="icon-double-angle-left"></i>
									</a>
								</li>
								<li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
									<a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
										<i class="icon-angle-left"></i>
									</a>
								</li>
								<li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
									<a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
										<i class="icon-angle-right"></i>
									</a>
								</li>
								<li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
									<a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
										<i class="icon-double-angle-right"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
					<!-- pagination navigation end -->

				</div>
			</div>
			<!-- widget footer end -->

		</div>
		<!-- widget main end -->
	</div>
	<!-- widget body end -->
</div>

<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/additional-methods.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/Knockout/knockout-3.1.0.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/Knockout/Plugins/Moment/moment.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/Knockout/Plugins/Moment/moment-with-locales.min.js"></script>

<!-- Don't touch this! -->


    <script class="include" type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.jqplot.min.js"></script>
    <!--<script type="text/javascript" src="syntaxhighlighter/scripts/shCore.min.js"></script>
    <script type="text/javascript" src="syntaxhighlighter/scripts/shBrushJScript.min.js"></script>
    <script type="text/javascript" src="syntaxhighlighter/scripts/shBrushXml.min.js"></script>-->
<!-- Additional plugins go here -->

<!-- jqplot library -->
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/excanvas.min.js"></script><![endif]-->
  <script class="include" type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jqplot.barRenderer.min.js"></script> 
  <script class="include" type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jqplot.categoryAxisRenderer.min.js"></script>
  <script class="include" type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jqplot.pointLabels.min.js"></script>  
  <script class="include" type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jqplot.canvasAxisLabelRenderer.min.js"></script>
  <script class="include" type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jqplot.canvasAxisTickRenderer.min.js"></script>
  <script class="include" type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jqplot.canvasTextRenderer.min.js"></script>
  <script class="include" type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jqplot.highlighter.js"></script>
  <script class="include" type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jqplot.dateAxisRenderer.min.js"></script>  
  <link href="/_catalogs/masterpage/Ace/assets/css/jquery.jqplot.min.css" rel="stylesheet"/>

<!-- End additional plugins -->
<script type="text/javascript" src="/SiteAssets/Scripts/VolumeMatrixDashboard.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/Knockout/knockout.mapping-latest.js"></script>
</div>
<style type="text/css">
	.jqplot-point-label{
		color: #eaeaea;	
	}	
	.jqplot-highlighter{
		color: #d15b47;
		font-size:14px;
		font-weight:bold;
	}
[id*='btnProduct']{
	margin-top:-6px;
}

@media (max-width: 1200px) {
	[id*='btnProduct']{
		margin-top:-46px;
	}
}

</style>



</div>
</div>
</div>
</div>
</div>