﻿var ViewModel = function () {
    //Make the self as 'this' reference
    var cifData = '0';
    var self = this;

    var CategoryModel = function (id, name) {
        var self = this;
        self.ID = ko.observable(id);
        self.Name = ko.observable(name);
        //self.Description = ko.observable(desc);

    }
    var CustomerModel = {
        CIF: ko.observable(),
        CustomerName: ko.observable(),
        Name: ko.observable(),
        POAName: ko.observable(),
        BizSegment: ko.observable(BizSegmentModel),
        BizSegmentID: ko.observable(),
        Branch: ko.observable(),
        Type: ko.observable(),
        RM: ko.observable(),
        Contacts: ko.observableArray(),
        Functions: ko.observableArray(),
        Accounts: ko.observableArray(),
        Underlyings: ko.observableArray()
    };

    var BankModel = {
        ID: ko.observable(),
        Code: ko.observable(),
        BranchCode: ko.observable(),
        SwiftCode: ko.observable(),
        BankAccount: ko.observable(),
        Description: ko.observable(),
        CommonName: ko.observable(),
        Currency: ko.observable(),
        PGSL: ko.observable()
    };

    var LocationModel = {
        ID: ko.observable(),
        Code: ko.observable(),
        LocationName: ko.observable(),
        Region: ko.observable()

    }

    var BizSegmentModel = {
        ID: ko.observable(),
        Name: ko.observable()
        //Description: ko.observable()
    };

    self.Readonly = ko.observable(false);
    self.IsWorkflow = ko.observable(false);
    //self.BranchCode = ko.observable("");
    //self.Currency = ko.observable(new CurrencyBankModel('', ''));

    //Declare observable which will be bind with UI
    self.AdhocID = ko.observable("");
    self.CIF = ko.observable("");
    self.Customer = ko.observable(CustomerModel);
    //self.Name = ko.observable(CustomerModel);//CustomerName
    self.Name = ko.observable();//CustomerName
    self.Location = ko.observable(LocationModel);
    self.Code = ko.observable("");
    self.CompanyName = ko.observable("");
    self.CategoryID = ko.observable(new CategoryModel('', ''));
    //self.CategoryID = ko.observable(new BizSegmentModel('', ''));
    self.CreditManager = ko.observable("");
    self.IsAdhocRequired = ko.observable(false);
    self.LastModifiedBy = ko.observable("");
    self.LastModifiedDate = ko.observable("");
    self.CategoryName = ko.observable("");

    //Dropdownlist
    //self.ddlCurrency = ko.observableArray([]);
    //self.ddlBranch = ko.observableArray([]);
    self.ddlCategory = ko.observableArray([]);

    // filter
    self.FilterCIF = ko.observable("");
    self.FilterCustomer = ko.observable("");
    self.FilterCreditManager = ko.observable("");
    self.FilterIsAdhocRequired = ko.observable("");
    self.FilterModifiedBy = ko.observable("");
    self.FilterModifiedDate = ko.observable("");

    self.IsRoleMaker = ko.observable(false);
    self.IsPermited = ko.observable(false);

    // New Data flag
    self.IsNewData = ko.observable(false);

    // Declare an ObservableArray for Storing the JSON Response
    self.Adhocs = ko.observableArray([]);

    // grid properties
    self.GridProperties = ko.observable();
    self.GridProperties(new GridPropertiesModel(GetData));

    // set default sorting
    self.GridProperties().SortColumn("CIF");
    self.GridProperties().SortOrder("ASC");

    // bind clear filters
    self.ClearFilters = function () {

        self.FilterCIF("");
        self.FilterCustomer("");
        self.FilterCreditManager("");
        self.FilterIsAdhocRequired("");
        self.FilterModifiedBy("");
        self.FilterModifiedDate("");

        GetData();
    };
    self.GetDropdown = function () {

        GetDropdown();
    };

    // bind get data function to view
    self.GetData = function () {
        GetData();
    };

    //The Object which stored data entered in the observables
    var Adhoc = {
        AdhocID: self.AdhocID,
        Name: self.Name,
        Customer: self.Customer,
        CIF: self.CIF,
        Location: self.Location,
        Code: self.Code,
        CompanyName: self.CompanyName,
        CategoryID: self.CategoryID,
        CreditManager: self.CreditManager,
        IsAdhocRequired: self.IsAdhocRequired,
        LastModifiedBy: self.LastModifiedBy,
        LastModifiedDate: self.LastModifiedDate

    };
    self.IsPermited = function (IsPermitedResult) {
        //Ajax call to delete the Customer
        spUser = $.cookie(api.cookie.spUser);

        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/CheckUserPermited/" + _spFriendlyUrlPageContextInfo.title,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    //compare user role to page permission to get checker validation
                    console.log("spUser Roles : " + ko.toJSON(spUser.Roles));
                    console.log("spUser Permited : " + data);
                    for (i = 0; i < spUser.Roles.length; i++) {
                        for (j = 0; j < data.length; j++) {
                            if (Const_RoleName[spUser.Roles[i].ID] == data[j]) { //if (spUser.Roles[i].Name == data[j]) {
                                if (Const_RoleName[spUser.Roles[i].ID].endsWith('Maker')) { //if (spUser.Roles[i].Name.endsWith('Maker')) {
                                    self.IsRoleMaker(true);
                                    return;
                                }
                                else {
                                    self.IsRoleMaker(false);
                                }
                            }
                        }
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }
    self.save = function () {
        // validation
        var form = $("#aspnetForm");

        var CategoryName = $('#CategoryID option:selected').val();//categoryID
        var customername = $('#Customer').val();
        var cif = $('#CIF').val();
        var Location = $('#Location').val();
        form.validate();


        if (form.valid()) {
            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {
                    CustomerModel.Name(customername);
                    CustomerModel.CIF(cif);
                    CustomerModel.BizSegmentID(CategoryName);
                    LocationModel.LocationName(Location);
                    Adhoc.CategoryID(CategoryName);
                    Adhoc.Customer(CustomerModel);
                    Adhoc.Location(LocationModel);
                    Adhoc.IsAdhocRequired(false);
                    Adhoc.Name(customername);
                    Adhoc.CIF(cif);




                    $.ajax({
                        type: "POST",
                        url: api.server + api.url.adhoc,
                        data: ko.toJSON(Adhoc), //Convert the Observable Data into JSON
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {
                                // send notification
                                ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // hide current popup window
                                $("#modal-form").modal('hide');

                                // refresh data
                                GetData();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });
                }
            });
        }
    };

    self.update = function () {
        // validation
        var form = $("#aspnetForm");
        var CategoryName = $('#CategoryID option:selected').val();//categoryID
        var customername = $('#Customer').val();
        var cif = $('#CIF').val();
        var Location = $('#Location').val();
        form.validate();


        if (form.valid()) {


            //console.log(ko.toJSON(Bank));
            // hide current popup window

            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form").modal('show');
                } else {
                    CustomerModel.Name(customername);
                    CustomerModel.CIF(cif);
                    CustomerModel.BizSegmentID(CategoryName);
                    LocationModel.LocationName(Location);
                    Adhoc.CategoryID(CategoryName);
                    Adhoc.Customer(CustomerModel);
                    Adhoc.Location(LocationModel);
                    Adhoc.IsAdhocRequired(false);
                    //add for grid workflow
                    //Adhoc.Name(customername);
                    ///Adhoc.CIF(cif);
                    //Ajax call to update the Customer
                    $.ajax({
                        type: "PUT",
                        url: api.server + api.url.adhoc + "/" + Adhoc.AdhocID(),
                        data: ko.toJSON(Adhoc),
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {
                                // send notification
                                ShowNotification('Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                // hide current popup window
                                $("#modal-form").modal('hide');

                                // refresh data
                                GetData();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // send notification
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        }
                    });
                }
            });
        }
    };

    self.delete = function () {

        var CategoryName = $('#CategoryID option:selected').val();//categoryID
        var customername = $('#Customer').val();
        var cif = $('#CIF').val();
        var Location = $('#Location').val();

        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                $("#modal-form").modal('show');
            } else {
                CustomerModel.Name(customername);
                CustomerModel.CIF(cif);
                CustomerModel.BizSegmentID(CategoryName);
                LocationModel.LocationName(Location);
                Adhoc.CategoryID(CategoryName);
                Adhoc.Customer(CustomerModel);
                Adhoc.Location(LocationModel);
                Adhoc.IsAdhocRequired(false);
                //add for grid workflow
                //Adhoc.Name(customername);
                //Adhoc.CIF(cif);
                //Bank.Currency(CurrencyDelete);
                //Ajax call to delete the Customer
                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.adhoc + "/" + Adhoc.AdhocID(),
                    data: ko.toJSON(Adhoc),
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if (jqXHR.status = 200) {
                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                            // refresh data
                            GetData();
                        } else
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
            }
        });
    };
    self.OnChangeCurrency = function () {
        var indexOption = $('#CategoryID option:selected').index();
        $('#Categoryhidden option').eq(indexOption).prop('selected', true);

    };
    // Local Date
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return "";
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-17
    };

    //Function to Display record to be updated
    self.GetSelectedRow = function (data) {
        self.IsNewData(false);

        if (self.IsWorkflow()) $("#modal-form-workflow").modal('show');
        else $("#modal-form").modal('show');

        self.AdhocID(data.AdhocID);
        if (data.Customer != null) {
            self.Customer(data.Customer.Name);
            self.CIF(data.Customer.CIF);
            self.Location(data.Location.LocationName);
            self.CompanyName(data.CompanyName);
            self.CategoryID(new CategoryModel(data.CategoryID, data.CategoryName));
        } else {
            self.Customer(data.Name);
            self.CIF(data.CIF);
            //self.Location(data.LocationName);
            self.Location(data.Branch);
            self.CompanyName(data.CompanyName);
            self.CategoryID(new CategoryModel(data.CategoryID, data.CategoryName));
        }
        self.Code(data.Code);
        self.CreditManager(data.CreditManager);
        self.LastModifiedBy(data.LastModifiedBy);
        self.LastModifiedDate(data.LastModifiedDate);
    };

    //insert new
    self.NewData = function () {
        // flag as new Data
        self.IsNewData(true);
        self.Readonly(false);
        // bind empty data
        self.AdhocID(0);
        self.Customer('');
        //self.Name('');
        self.CIF('');
        self.Location('');
        self.Code('');
        self.CategoryID(new CategoryModel('', ''));
        //self.CategoryID(new BizSegmentModel('', ''));
        self.CompanyName('');
        self.CreditManager('');
        //self.SwiftCode('');
    };

    //Function to Read All Customers
    function GetData() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.adhoc,
            params: {
                page: self.GridProperties().Page(),
                size: self.GridProperties().Size(),
                sort_column: self.GridProperties().SortColumn(),
                sort_order: self.GridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetData, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetData, OnError, OnAlways);
        }
    }

    //Function to validation dynamic field

    function IsvalidField() { /*
     $("[name^=days]").each(function () {
     $(this).rules('add', {
     required: true,
     maxlength: 2,
     number: true,
     messages: {
     required: "Please provide a valid Day.",
     maxlength: "Please provide maximum 2 number valid day value.",
     number: "Please provide number"
     }
     });
     }); */
        return true;
    }
    function GetDropdown() {
        $.ajax({
            async: false,
            type: "GET",
            url: api.server + api.url.parameter + '?select=CategoryID',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status == 200) {
                    //console.log(data['Currency']);					
                    self.ddlCategory(data['CategoryID']);
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }

        });

    }
    // Get filtered columns value
    function GetFilteredColumns() {
        // define filter
        var filters = [];

        //if (self.FilterCIF() != "") filters.push({ Field: 'CIF', Value: self.FilterCIF() });
        //if (self.FilterCustomer() != "") filters.push({ Field: 'Customer', Value: self.FilterCustomer() });
        if (self.FilterCIF() != "") filters.push({ Field: 'CIF', Value: self.FilterCIF() });
        if (self.FilterCustomer() != "") filters.push({ Field: 'Name', Value: self.FilterCustomer() });
        if (self.FilterCreditManager() != "") filters.push({ Field: 'CreditManager', Value: self.FilterCreditManager() });
        if (self.FilterIsAdhocRequired() != "") filters.push({ Field: 'IsAdhocRequired', Value: self.FilterIsAdhocRequired() });
        if (self.FilterModifiedBy() != "") filters.push({ Field: 'LastModifiedBy', Value: self.FilterModifiedBy() });
        if (self.FilterModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.FilterModifiedDate() });

        return filters;
    };

    // On success GetData callback
    function OnSuccessGetData(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.Adhocs(data.Rows);

            self.GridProperties().Page(data['Page']);
            self.GridProperties().Size(data['Size']);
            self.GridProperties().Total(data['Total']);
            self.GridProperties().TotalPages(Math.ceil(self.GridProperties().Total() / self.GridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }

    // On Error callback
    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    // On Always callback
    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }
    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }

    });


    $("#Customer").autocomplete({

        source: function (request, response) {
            // declare options variable for ajax get request
            var options = {
                url: api.server + api.url.customer + "/Search",
                data: {
                    query: request.term,
                    limit: 20
                },
                token: accessToken
            };

            // exec ajax request
            Helper.Ajax.AutoComplete(options, response, OnSuccessAutoComplete, OnError);
        },
        minLength: 2,
        select: function (event, ui) {
            // set customer data model
            if (ui.item.data != undefined || ui.item.data != null) {
                var mapping = {
                    'ignore': ["LastModifiedDate", "LastModifiedBy"]
                };
                $("#CIF").val(ui.item.data.CIF);
                $("#Location").val(ui.item.data.Branch.Name);
            }
        }
    });

    // Autocomplete
    function OnSuccessAutoComplete(response, data, textStatus, jqXHR) {
        response($.map(data, function (item) {
            return {
                // default autocomplete object
                label: item.CIF + " - " + item.Name + " - " + item.Branch.Name,
                value: item.Name,

                // custom object binding
                data: {
                    CIF: item.CIF,
                    Customer: item.Name,
                    Branch: item.Branch
                }
            }
        })
      );
    }

    var CustomerResult = {
        CIF: ko.observable(),
        Name: ko.observable()
    };
};
