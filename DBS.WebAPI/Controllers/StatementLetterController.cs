﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class StatementLetterController : ApiController
    {
        private string message = string.Empty;

        private readonly IStatementLetterRepository repository = null;

        public StatementLetterController()
        {
            this.repository = new StatementLetterRepository();
        }

        public StatementLetterController(IStatementLetterRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all Statement Letters.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<StatementLetterModel>))]
        public HttpResponseMessage Get()
        {
            IList<StatementLetterModel> output = new List<StatementLetterModel>();

            if (!repository.GetStatementLetter(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<StatementLetterModel>>(HttpStatusCode.OK, output);
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}