﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Models
{
    #region Property
    [ModelName("FDTransfer")]
    public class FDTransferModel
    {

        public int ID { get; set; }
        public string Name { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public string LastModifiedBy { get; set; }
    }

    public class FDTransferRow : FDTransferModel
    {
        public int RowID { get; set; }

    }

    public class FDTransferGrid : Grid
    {
        public IList<FDTransferRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class FDTransferFilter : Filter { }
    #endregion

    #region Interface
    public interface IFDTransferRepository : IDisposable
    {
        bool GetFDTransferByID(int id, ref FDTransferModel fdtransfer, ref string message);
        bool GetFDTransfer(ref IList<FDTransferModel> fdtransfers, int limit, int index, ref string message);
        bool GetFDTransfer(ref IList<FDTransferModel> fdtransfers, ref string message);
        bool GetFDTransfer(int page, int size, IList<FDTransferFilter> filters, string sortColumn, string sortOrder, ref FDTransferGrid FDTransfer, ref string message);
        bool GetFDTransfer(FDTransferFilter filter, ref IList<FDTransferModel> fdtransfers, ref string message);
        bool GetFDTransfer(string key, int limit, ref IList<FDTransferModel> fdtransfers, ref string message);
        bool AddFDTransfer(FDTransferModel fdtransfer, ref string message);
        bool UpdateFDTransfer(int id, FDTransferModel fdtransfer, ref string message);
        bool DeleteFDTransfer(int id, ref string message);
    }
    #endregion

    #region Repository
    public class FDTransferRepository : IFDTransferRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetFDTransferByID(int id, ref FDTransferModel fdtransfer, ref string message)
        {
            bool isSuccess = false;

            try
            {
                fdtransfer = (from a in context.FDTransferFdGenerates
                           where a.IsDeleted.Equals(false) && a.FDTransferFdGenerateID.Equals(id)
                           select new FDTransferModel
                           {
                               ID = a.FDTransferFdGenerateID,
                               Name = a.TransferName,
                               LastModifiedDate = a.CreateDate,
                               LastModifiedBy = a.CreatedBy
                           }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetFDTransfer(ref IList<FDTransferModel> fdtransfers, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    fdtransfers = (from a in context.FDTransferFdGenerates
                                where a.IsDeleted.Equals(false)
                                   orderby new { a.TransferName }
                                select new FDTransferModel
                                {
                                    ID = a.FDTransferFdGenerateID,
                                    Name = a.TransferName,
                                    LastModifiedBy = a.CreatedBy,
                                    LastModifiedDate = a.CreateDate
                                }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetFDTransfer(ref IList<FDTransferModel> fdtransfers, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    fdtransfers = (from a in context.FDTransferFdGenerates
                                where a.IsDeleted.Equals(false)
                                   orderby new { a.TransferName }
                                select new FDTransferModel
                                {
                                    ID = a.FDTransferFdGenerateID,
                                    Name = a.TransferName,
                                    LastModifiedBy = a.CreatedBy,
                                    LastModifiedDate = a.CreateDate
                                }).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetFDTransfer(int page, int size, IList<FDTransferFilter> filters, string sortColumn, string sortOrder, ref FDTransferGrid customer, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                FDTransferModel filter = new FDTransferModel();
                if (filters != null)
                {
                    filter.Name = filters.Where(a => a.Field.Equals("Name")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse(filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.FDTransferFdGenerates
                                let isName = !string.IsNullOrEmpty(filter.Name)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted==false &&
                                //a.IsDeleted.Equals(false) &&
                                    (isName ? a.TransferName.Contains(filter.Name) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreatedBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new FDTransferModel
                                {
                                    ID = a.FDTransferFdGenerateID,
                                    Name = a.TransferName,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreatedBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    customer.Page = page;
                    customer.Size = size;
                    customer.Total = data.Count();
                    customer.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    customer.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new FDTransferRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            Name = a.Name,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetFDTransfer(FDTransferFilter filter, ref IList<FDTransferModel> fdtransfers, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetFDTransfer(string key, int limit, ref IList<FDTransferModel> fdtransfers, ref string message)
        {
            bool isSuccess = false;

            try
            {
                fdtransfers = (from a in context.FDTransferFdGenerates
                            where a.IsDeleted.Equals(false) &&
                                a.TransferName.Contains(key)
                               orderby new { a.TransferName }
                            select new FDTransferModel
                            {
                                Name = a.TransferName,

                            }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddFDTransfer(FDTransferModel fdtransfer, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (!context.FDTransferFdGenerates.Where(a => a.TransferName.Equals(fdtransfer.Name) && a.IsDeleted==false).Any())
                {
                    context.FDTransferFdGenerates.Add(new Entity.FDTransferFdGenerate()
                    {
                        TransferName = fdtransfer.Name,
                        IsDeleted = false,
                        CreateDate = DateTime.UtcNow,
                        CreatedBy = currentUser.GetCurrentUser().DisplayName
                    });

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("FDTransfer data for fdtransfer name {0} is already exist.", fdtransfer.Name);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }


        public bool UpdateFDTransfer(int id, FDTransferModel fdtransfer, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.FDTransferFdGenerate data = context.FDTransferFdGenerates.Where(a => a.FDTransferFdGenerateID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    if (context.FDTransferFdGenerates.Where(a => a.FDTransferFdGenerateID != fdtransfer.ID && a.TransferName.Equals(fdtransfer.Name) && a.IsDeleted==false).Count() >= 1)
                    {
                        message = string.Format("fdtransfer name {0} is exist.", fdtransfer.Name);
                    }
                    else
                    {
                        data.TransferName = fdtransfer.Name;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        context.SaveChanges();

                        isSuccess = true;
                    }
                }
                else
                {
                    message = string.Format("FDTransfer data for FDTransfer ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeleteFDTransfer(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.FDTransferFdGenerate data = context.FDTransferFdGenerates.Where(a => a.FDTransferFdGenerateID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("FDTransfer data for FDTransfer ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}