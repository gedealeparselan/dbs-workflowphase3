﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Home_EntityWPUserControl.ascx.cs" Inherits="DBS.Sharepoint.SPWebparts.Home.Home_EntityWP.Home_EntityWPUserControl" %>


<div id="home-master">

<!-- modal form start -->
<div id="modal-form-master" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
	<div class="modal-dialog" style="width:70%">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">x</button>
				<h4 class="blue bigger">
					<span data-bind="text: Form().ActivityTitle"></span>
					<span data-bind="text: Form().ApprovalTemplate"></span>
					<span data-bind="text: Form().ApprovalData"></span>
				</h4>
			</div>
			<div class="alert alert-danger" data-bind="visible: !$root.Form().IsAuthorizedNintex()">
				<!-- <button type="button" class="close" data-dismiss="alert">
					<i class="icon-remove"></i>
				</button> -->
				<strong>
					<i class="icon-remove"></i>
					Unauthorized!
				</strong>
				<span data-bind="text: Form().MessageNintex"></span>
			</div>
			<!-- modal header end -->
			
			<!-- modal body start -->
			<div class="modal-body overflow-visible">
				<div class="row" id="form-content-dialog">
				</div>
				<div class="row">
					<div class="col-xs-12">
						<!-- modal body form start -->
						<div class="tabbable">
							<ul class="nav nav-tabs" id="myTab">
								<li class="active">
									<a data-toggle="tab" href="#transaction">
										<i class="blue icon-credit-card bigger-110"></i>
										<span data-bind="text: Form().ActivityTitle"></span>
									</a>
								</li>
								<li>
									<a data-toggle="tab" href="#history">
										<i class="blue icon-comments bigger-110"></i>
										History
										<!--<span class="badge badge-danger">4</span>-->
									</a>
								</li>
								<!--<li>
									<a data-toggle="tab" href="#context">
										<i class="blue icon-cogs bigger-110"></i>
										Context
									</a>
								</li>
								<li>
								    <a data-toggle="tab" href="#ko-model">
								        <i class="blue icon-cogs bigger-110"></i>
								        Knockout Model
								    </a>
								</li>-->
							</ul>
							<div class="tab-content">
								<div id="transaction" class="tab-pane in active">
									<!--div id="form-content-dialog">
									</div-->								
								    <div id="transaction-progress" class="col-sm-4 col-sm-offset-4" style="display: none">
                                        <h4 class="smaller blue">Please wait...</h4>
                                        <span class="space-4"></span>
                                        <div class="progress progress-small progress-striped active">
                                            <div class="progress-bar progress-bar-info" style="width:100%"></div>
                                        </div>
                                    </div>
									<div id="transaction-data" data-bind="template: { name: Form().ActiveTemplate, data: Form().Parent }"></div>
								</div>
								<div id="history" class="tab-pane">
                                    <div id="timeline-2" data-bind="with: Form().ApprovalData">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                                                <div class="timeline-container timeline-style2">
                                                    <!--<span class="timeline-label">
                                                        <b>Today</b>
                                                    </span>-->

                                                    <div class="timeline-items" data-bind="foreach: Form().Timelines">
                                                        <div class="timeline-item clearfix">
                                                            <div class="timeline-info">
                                                                <span class="timeline-date" data-bind="text: $root.Form().LocalDate(Time)"></span>

                                                                <i class="timeline-indicator btn btn-info no-hover"></i>
                                                            </div>

                                                            <div class="widget-box transparent">
                                                                <div class="widget-body">
                                                                    <div class="widget-main no-padding">
                                                                        <span class="bigger-110">
                                                                            <span class="blue bolder" data-bind="text: DisplayName"></span>
                                                                            <!-- ko if: IsGroup -->
                                                                            (<span data-bind="text: UserOrGroup"></span>)
                                                                            <!-- /ko -->
                                                                        </span>

                                                                        <span class="bolder" data-bind="text: Outcome"></span>
                                                                        <span data-bind="text: Activity"></span>

                                                                        <!-- ko ifnot: ApproverID == 0 -->
                                                                        <br />
                                                                        <i class="icon-comments-alt bigger-110 blue"></i>
                                                                        <span data-bind="text: Comment"></span>
                                                                        <!-- /ko -->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
								</div>

								<div id="context" class="tab-pane">
									<table class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th>Parameter</th>
												<th>Value</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Web ID</td>
												<td><span data-bind="text: Form().SPWebID"></span></td>
											</tr>
											<tr>
												<td>Site ID</td>
												<td><span data-bind="text: Form().SPSiteID"></span></td>
											</tr>
											<tr>
												<td>List ID</td>
												<td><span data-bind="text: Form().SPListID"></span></td>
											</tr>
											<tr>
												<td>List Item ID</td>
												<td><span data-bind="text: Form().SPListItemID"></span></td>
											</tr>
											<tr>
												<td>Task List ID</td>
												<td><span data-bind="text: Form().SPTaskListID"></span></td>
											</tr>
											<tr>
												<td>Task List Item ID</td>
												<td><span data-bind="text: Form().SPTaskListItemID"></span></td>
											</tr>
											<tr>
												<td>Initiator</td>
												<td><span data-bind="text: Form().Initiator"></span></td>
											</tr>
											<tr>
												<td>Workflow Instance ID</td>
												<td><span data-bind="text: Form().WorkflowInstanceID"></span></td>
											</tr>
											<tr>
												<td>Workflow ID</td>
												<td><span data-bind="text: Form().WorkflowID"></span></td>
											</tr>
											<tr>
												<td>Workflow Name</td>
												<td><span data-bind="text: Form().WorkflowName"></span></td>
											</tr>
											<tr>
												<td>Start Time</td>
												<td><span data-bind="text: Form().LocalDate(Form().StartTime())"></span></td>
											</tr>
											<tr>
												<td>State ID </td>
												<td><span data-bind="text: Form().StateID"></span></td>
											</tr>
											<tr>
												<td>State Description</td>
												<td><span data-bind="text: Form().StateDescription"></span></td>
											</tr>
											<tr>
												<td>Is Authorized</td>
												<td><span data-bind="text: Form().IsAuthorized"></span></td>
											</tr>
											<tr>
												<td>Task Type</td>
												<td><span data-bind="text: Form().TaskTypeDescription"></span></td>
											</tr>
											<tr>
												<td>Title</td>
												<td><span data-bind="text: Form().Title"></span></td>
											</tr>
											<tr>
												<td>Activity Title</td>
												<td><span data-bind="text: Form().ActivityTitle"></span></td>
											</tr>
											<tr>
												<td>User</td>
												<td><span data-bind="text: Form().User"></span></td>
											</tr>
											<tr>
												<td>Is Sharepoint Group</td>
												<td><span data-bind="text: Form().IsSPGroup"></span></td>
											</tr>
											<tr>
												<td>Entry Time</td>
												<td><span data-bind="text: Form().LocalDate(Form().EntryTime())"></span></td>
											</tr>
											<tr>
												<td>Exit Time</td>
												<td><span data-bind="text: Form().LocalDate(Form().ExitTime())"></span></td>
											</tr>
											<tr>
												<td>Outcome ID</td>
												<td><span data-bind="text: Form().OutcomeID"></span></td>
											</tr>
											<tr>
												<td>Outcome Description</td>
												<td><span data-bind="text: Form().OutcomeDescription"></span></td>
											</tr>
											<tr>
												<td>Custom Outcome ID</td>
												<td><span data-bind="text: Form().CustomOutcomeID"></span></td>
											</tr>
											<tr>
												<td>Custom Outcom Description</td>
												<td><span data-bind="text: Form().CustomOutcomeDescription"></span></td>
											</tr>
											<tr>
												<td>Comments</td>
												<td><span data-bind="text: Form().Comments"></span></td>
											</tr>
										</tbody>
									</table>
								</div>

								<div id="ko-model" class="tab-pane">
								    <pre data-bind="text: ko.toJSON(Form().ApprovalData, null, 2)"></pre>
                                    <!--<pre data-bind="text: JSON.stringify(ko.toJS(ApprovalData), null, 2)"></pre>-->
								</div>
							</div>
						</div>	
						<!-- modal body form end -->
					</div>
				</div>
			</div>
			<!-- modal body end -->
			
			<!-- modal footer start -->
			<div class="modal-footer">
				<div class="row" data-bind="visible: Form().IsAuthorizedNintex()">
				    <div class="form-group">
				        <label class="col-sm-3 control-label bolder no-padding-right" for="workflow-comments">Comments</label>

				        <div class="col-xs-9 align-left">
                            <div class="clearfix">
                                <textarea class="col-lg-12" id="workflow-comments" name="workflow-comments" rows="4" data-bind="value: $root.Form().Comments, enable: Form().IsPendingNintex()" placeholder="Type any comment here..."></textarea>
                            </div>
                        </div>
				    </div>
				</div>
				
				<div class="space-8"></div>
				
				<!-- dynamic outcomes button start -->
				<span data-bind="foreach: Form().Outcomes, visible: Form().IsAuthorizedNintex()">
					<button data-bind="click: $root.Form().ApprovalProcess, enable: $root.Form().IsPendingNintex()" class="btn btn-sm btn-primary">
						<i class="icon-approve"></i>
						<span data-bind="text: $root.Form().ApprovalButton(Name)"></span>
					</button>
				</span>
				<!-- dynamic outcomes button end -->
				
				<button class="btn btn-sm btn-success" data-dismiss="modal">
					<i class="icon-remove"></i>
					Close
				</button>
			</div>
			<!-- modal footer end -->
			
		</div>
	</div>
</div>
<!-- modal form end -->

</div>

<!-- approval templates start -->
<!-- TaskRoleChecker templates start -->
<script type="text/html" id="TaskRoleChecker" src="/Pages/Approval/RoleChecker.html"></script>
<!-- TaskRoleChecker templates end -->

<!-- TaskUserChecker templates start -->
<script type="text/html" id="TaskUserChecker" src="/Pages/Approval/UserChecker.html"></script>
<!-- TaskUserChecker templates end -->
<!-- TaskUserChecker templates start -->
<script type="text/html" id="TaskCustomerPOAEmailChecker" src="/Pages/Dialogs/ApprovalPOAEmail.html"></script>
<!-- TaskUserChecker templates end -->
<!-- TaskUnderlyingChecker templates start -->
<script type="text/html" id="TaskUnderlyingChecker" src="/Pages/Dialogs/MasterUnderlying.html"></script>
<!-- TaskUnderlyingChecker templates end -->

<!-- CustomerCSOChecker templates start -->
<script type="text/html" id="TaskCustomerCsoChecker" src="/Pages/Dialogs/CustomerCso.html"></script>
<!-- CustomerCSOChecker templates end -->

<script type="text/html" id="TaskCustomerCallbackChecker" src="/Pages/Dialogs/CustomerCallback.html"></script>

<script type="text/javascript">
    //Synchronously load the templates
    var $templates = $('script[type="text/html"]');
    $templates.each(function () {
        //Only get non-inline templates
        //No need to change code for production.
        if ($(this).attr("src")) {
            $.ajax(
              $(this).attr("src"),
              {
                  async: false,
                  context: this,
                  success: function (data) { $(this).html(data); }
              }
            );
        }
    });
    //From this point onwards (in the same script, in the 
    //following scripts) all the "text/html" script tags 
    //will be loaded as if they were inline.
</script>

<!-- PPU Checker templates end -->
<!-- approval templates finish -->
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/additional-methods.min.js"></script>

<script type="text/javascript" src="/SiteAssets/Scripts/HomeDraft_Test.js"></script>