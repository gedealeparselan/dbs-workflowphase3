﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddDocumentFlowWPUserControl.ascx.cs" Inherits="DBS.Sharepoint.SPWebparts.Others.AddDocumentFlowWP.AddDocumentFlowWPUserControl" %>

<div class="header smaller no-margin-top lighter dark">
    <h3>New Transaction 
        <label id="lblNewTR">Document Flow</label>
    </h3>
</div>
<div id="new-transaction" class="modal-body overflow-visible">
    <div class="row">
        <div class="col-xs-12">
        </div>
    </div>

    <div class="modal-body overflow-visible" id="header-transaction">
        <div class="row">
            <div class="col-xs-12">
                <div id="transaction-form" class="form-horizontal" role="form">
                    <div class="form-group">
                        <label class="col-sm-3 control-label bolder no-padding-left">
                            <span data-bind="text: $root.LocalDate($root.CreateDate(), true, true)"></span><br />
                            <b>Branch Name</b> : <span data-bind="text: $root.BranchName()"></span>
                        </label>

                        <label class="col-sm-4 pull-right">
                            <b>Application ID</b> : <span data-bind="text: DocumentFlowData().ApplicationID"></span><br />
                            <b>User Name</b> : <span data-bind="text: $root.SPUser().DisplayName"></span>
                        </label>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label bolder no-padding-right">Top Urgent</label>
                        <div class="col-sm-1">
                            <label>
                                <input class="ace ace-switch ace-switch-6" type="checkbox" data-bind="checked: DocumentFlowData().IsTopUrgent, event: {change: $root.TopUrgent}">
                                <span class="lbl"></span>
                            </label>
                        </div>
                        <label class="col-sm-1 control-label bolder no-padding-right">Urgent</label>
                        <div class="col-sm-1">
                            <label>
                                <input class="ace ace-switch ace-switch-6" type="checkbox" data-bind="checked: DocumentFlowData().IsUrgent, event: {change: $root.Urgent}">
                                <span class="lbl"></span>
                            </label>
                        </div>
                        <label class="col-sm-1 control-label bolder no-padding-right">Normal</label>
                        <div class="col-sm-1">
                            <label>
                                <input class="ace ace-switch ace-switch-6" type="checkbox" data-bind="checked: DocumentFlowData().IsNormal, event: { change: $root.Normal}">
                                <span class="lbl"></span>
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="product">Product</label>

                        <div class="col-sm-9">
                            <div class="clearfix">
                                <input type="text" class="col-sm-6" name="product" id="product" data-bind="value: DocumentFlowData().Product, enable: false" />
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="client">Client</label>

                        <div class="col-sm-9">
                            <div class="clearfix">
                                <input type="text" autocomplete="off" class="col-sm-11" name="client" id="client" data-rule-required="true" data-bind="value: DocumentFlowData().Client" />
                                <label class="control-label bolder text-danger" for="miname">*</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="typeOfTransaction">Type of Transaction</label>

                        <div class="col-sm-9">
                            <label>
                                <select id="typeOfTransaction" name="typeOfTransaction" data-rule-required="true" data-rule-value="true"
                                    data-bind="options: $root.Parameter().TransactionTypes,
                                                optionsText: 'Name',
                                                optionsValue: 'ID',
                                                optionsCaption: 'Please Select...',
                                                value: $root.Selected().TransactionType">
                                </select>
                                <label class="control-label bolder text-danger" for="miname">*</label>
                                <span class="lbl"></span>
                            </label>
                        </div>
                    </div>

                    <div class="form-group" data-bind="visible: IsOthers">
                        <label class="col-sm-3 control-label bolder no-padding-right" data-rule-required="true" for="others">Others</label>

                        <div class="col-sm-9">
                            <div class="clearfix">
                                <textarea class="col-sm-11" name="others" id="others" rows="6" data-bind="value: DocumentFlowData().Others"></textarea>
                                <label class="control-label bolder text-danger" for="miname">*</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="refnumber">Ref Number</label>

                        <div class="col-sm-9">
                            <div class="clearfix">
                                <input type="text" class="col-sm-11" name="refnumber" id="refnumber" data-rule-required="true" data-bind="value: DocumentFlowData().RefNumber" />
                                <label class="control-label bolder text-danger" for="refnumber">*</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="recipient">Recipient</label>

                        <div class="col-sm-9">
                            <label>
                                <select id="recipient" name="recipient" data-rule-required="true" data-rule-value="true"
                                    data-bind="options: $root.Parameter().Recipients,
                                               optionsText: 'DFRecipientName',
                                               optionsValue: 'DFRecipientID',
                                               optionsCaption: 'Please Select...',
                                               value: $root.Selected().Recipient"></select>
                                <label class="control-label bolder text-danger" for="recipient">*</label>
                                <span class="lbl"></span>
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="sheets">Sheets</label>

                        <div class="col-sm-9">
                            <div class="clearfix">
                                <input type="text" class="col-sm-2 align-right" data-rule-number="true" name="sheets" id="sheets" data-bind="value: DocumentFlowData().Sheets" />
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="remarks">Remarks</label>

                        <div class="col-sm-9">
                            <div class="clearfix">
                                <textarea class="col-sm-11" name="remarks" id="remarks" rows="6" data-bind="value: DocumentFlowData().Remarks"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label bolder no-padding-right">Signature Verification</label>
                        <div class="col-sm-1">
                            <div class="clearfix">
                                <label>
                                    <input id="signature-verification" class="ace ace-switch ace-switch-6" type="checkbox" data-bind="checked: DocumentFlowData().IsSignatureVerification" />
                                    <span class="lbl"></span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="space-4"></div>

                    <div class="padding-4">
                        <h3 class="header smaller lighter dark">Attachment
                            <button class="btn btn-sm btn-primary pull-right" data-bind="click: $root.UploadDocument">
                                <i class="icon-plus"></i>
                                Attach
                            </button>
                        </h3>

                        <div class="dataTables_wrapper" role="grid">
                            <table class="table table-striped table-bordered table-hover dataTable">
                                <thead>
                                    <tr>
                                        <th style="width: 50px">No.</th>
                                        <th>File Name</th>
                                        <th>Document Type</th>
                                        <th>Modified</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody data-bind="foreach: $root.Documents, visible: $root.Documents().length > 0">
                                    <tr>
                                        <td><span data-bind="text: $index() + 1"></span></td>
                                        <td><a data-bind="attr: { href: DocumentPath.DocPath, target: '_blank' }, text: DocumentPath.name, disable: (ID > 0)"></a></td>
                                        <td><span data-bind="text: Type.Name"></span></td>
                                        <td><span data-bind="text: moment(LastModifiedDate).format(config.format.dateTime)"></span></td>
                                        <td><a href="#" data-bind="click: $root.RemoveDocument">Remove</a></td>
                                    </tr>
                                </tbody>
                                <tbody data-bind="visible: $root.Documents().length == 0">
                                    <tr>
                                        <td colspan="5" style="text-align:center">No entries available.</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    </div>

    <div class="space-16"></div>

    <div class="row">
        <div class="col-sm-12 align-right">
            <button class="btn btn-sm btn-info" data-bind="click: SaveAsDraft, disable: !IsEditable()"><i class="icon-ok bigger-110"></i>Draft</button>
            <button class="btn btn-sm btn-primary" data-bind="click: Submit, disable: !IsEditable()"><i class="icon-approve"></i>Submit</button>
            <button class="btn btn-sm btn-success" data-dismiss="modal" data-bind="click: $root.CloseTransaction"><i class="icon-remove"></i>Close</button>
        </div>
    </div>

    
    <!-- Modal upload form start -->
    <div id="modal-form-upload" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="customer-form" class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="document-type">Document Type</label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="document-type" name="document-type" data-rule-required="true" data-rule-value="true" data-bind="options: $root.Parameter().DocumentTypes, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Options..', value: $root.Selected().DocumentType"></select>
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="document-purpose">Purpose of Docs</label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="document-purpose" name="document-purpose" data-rule-required="true" data-rule-value="true" data-bind="options: $root.Parameter().DocumentPurposes, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Options..', value: $root.Selected().DocumentPurpose"></select>
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="document-path">Document</label>

                                    <div class="col-sm-8">
                                        <div class="clearfix">
                                            <input type="file" id="document-path" name="document-path" data-bind="file: $root.DocumentPath" />
                                        </div>
                                    </div>
                                    <label class="control-label bolder text-danger">*</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.AddDocument">
                        <i class="icon-save"></i>
                        Save
                    </button>
                    <button class="btn btn-sm" data-dismiss="modal">
                        <i class="icon-remove"></i>
                        Cancel
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal upload form end -->

    <!-- Modal application ID List start-->
    <%--<div id="modal-form-applicationID" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="blue bigger">Application ID List
                    </h4>
                </div>
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="customer-form" class="form-horizontal" role="form">
                                <div class="dataTables_wrapper" role="grid">
                                    <table class="table table-striped table-bordered table-hover dataTable">
                                        <thead>
                                            <tr>
                                                <th style="width: 50px">No.</th>
                                                <th>Application ID</th>
                                                <th>Customer</th>
                                            </tr>
                                        </thead>
                                        <tbody data-bind="foreach: $root.ApplicationIDColl, visible: $root.ApplicationIDColl().length > 0">
                                            <tr data-toggle="modal">
                                                <td><span data-bind="text: $index() + 1"></span></td>
                                                <td><span data-bind="text: ApplicationID"></span></td>
                                                <td><span data-bind="text: Customer.Name"></span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <br />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.FinishBulk">
                        <i class="icon-save"></i>
                        OK
                    </button>
                </div>
            </div>
        </div>
    </div>--%>
    <!-- Modal application ID List end-->

    <%--<div id="modal-form-Notif" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="blue bigger"><span data-bind="text: $root.NotifTitle()" />
                    </h3>
                </div>
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="customer-form" class="form-horizontal" role="form">
                                <div class="dataTables_wrapper" role="grid">
                                    <h2><span data-bind="text: $root.NotifHeader()" /></h2>
                                    <span data-bind="text: $root.NotifMessage()" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.NotifOK">
                        <i class="icon-save"></i>
                        OK
                    </button>
                </div>
            </div>
        </div>
    </div>--%>
</div>

<!-- additional page scripts start -->
<script type="text/javascript" src="/SiteAssets/Scripts/Helper.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/date-time/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/additional-methods.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/Knockout/knockout.mapping-latest.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/DocumentFlow.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.maskedinput.min.js"></script>
