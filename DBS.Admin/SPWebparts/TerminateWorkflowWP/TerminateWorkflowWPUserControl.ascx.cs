﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.Workflow;
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace DBS.Admin.SPWebparts.TerminateWorkflowWP
{
    public partial class TerminateWorkflowWPUserControl : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        if (!IsPostBack)
            {
                ddlListName.Visible = true;
                txtListName.Visible = false;
                txtListName.Text = "";
                txtStartDateAfter.Text = "";
                txtStartDateBefore.Text = "";
                lblMessageResult.Visible = false;
                lblResult.Text = "";
                lblResult.Visible = false;
            }            
        }

        protected void ddlInputType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlInputType.SelectedValue == "1")
            {
                ddlListName.Visible = true;
                txtListName.Visible = false;
            }
            else if (ddlInputType.SelectedValue == "2")
            {
                ddlListName.Visible = false;
                txtListName.Visible = true;
            }
        }

        protected void btnTerminate_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(txtStartDateAfter.Text) || String.IsNullOrWhiteSpace(txtStartDateBefore.Text))
            {
                string message = "Start date after and start date before is mandatory.";
                lblMessageResult.Visible = true;
                lblResult.Text = message;
                lblResult.Visible = true;
            }
            else
            {
                if (Convert.ToDateTime(txtStartDateAfter.Text) >= Convert.ToDateTime(txtStartDateBefore.Text))
                {
                    string message = "Start date after cannot be more or equal than start date before.";
                    lblMessageResult.Visible = true;
                    lblResult.Text = message;
                    lblResult.Visible = true;
                }
                else
                {
                    if (ddlInputType.SelectedValue == "1")
                    {
                        RunTerminateWF(ddlListName.SelectedValue, txtStartDateBefore.Text, txtStartDateAfter.Text);
                    }
                    else if (ddlInputType.SelectedValue == "2")
                    {
                        RunTerminateWF(txtListName.Text, txtStartDateBefore.Text, txtStartDateAfter.Text);
                    }
                }
            }            
        }

        private void RunTerminateWF(string listName, string dateBefore, string dateAfter)
        {
            string dateAfterQuery = dateAfter.Split('/')[2] + "-" + dateAfter.Split('/')[0] + "-" + dateAfter.Split('/')[1] + "T00:00:00Z";

            string dateBeforeQuery = dateBefore.Split('/')[2] + "-" + dateBefore.Split('/')[0] + "-" + dateBefore.Split('/')[1] + "T00:00:00Z";

            string title = "Terminate Workflow";

            using (SPSite site = new SPSite(SPContext.Current.Site.RootWeb.Url))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    SPList myList = web.Lists[listName];

                    string query;

                    string hasil = "";

                    query = string.Format(@"<Where>
                                                <And>
                                                    <Geq>
                                                        <FieldRef Name='Created' />
                                                        <Value IncludeTimeValue='TRUE' Type='DateTime'>{0}</Value>
                                                    </Geq>
                                                    <Leq>
                                                        <FieldRef Name='Created' />
                                                        <Value IncludeTimeValue='TRUE' Type='DateTime'>{1}</Value>
                                                    </Leq>
                                                </And>
                                            </Where>", dateAfterQuery, dateBeforeQuery);

                    SPQuery queryGet = new SPQuery();
                    queryGet.Query = query;

                    SPListItemCollection items = myList.GetItems(queryGet);

                    if (items.Count > 0)
                    {
                        int ii = 0;
                        foreach (SPListItem item in items)
                        {
                            foreach (SPWorkflow workflow in item.Workflows)
                            {
                                if (workflow.InternalState == SPWorkflowState.Running)
                                {
                                    if (ii == items.Count - 1)
                                    {
                                        hasil += item.ID;
                                    }
                                    else{                                        
                                        hasil += item.ID + ", ";
                                    }
                                    SPWorkflowManager.CancelWorkflow(workflow);                                    
                                }
                            }
                            ii++;
                        }

                        if (String.IsNullOrEmpty(hasil))
                        {
                            //Javascript.ConsoleLog("no data found");
                            lblMessageResult.Visible = true;
                            lblResult.Text = "no data found";
                            lblResult.Visible = true;

                        }
                        else
                        {
                            //Javascript.ConsoleLog(hasil.TrimEnd());
                            string message = "Workflow with ID: " + hasil.TrimEnd() + " has been cancelled";
                            lblMessageResult.Visible = true;
                            lblResult.Text = message;
                            lblResult.Visible = true;
                        }
                    }
                    else
                    {
                        //Javascript.ConsoleLog("no data found");
                        lblMessageResult.Visible = true;
                        lblResult.Text = "no data found";
                        lblResult.Visible = true;
                    }                    
                }
            }
        }
    }

    public static class Javascript
    {
        static string scriptTag = "<script type=\"\" language=\"\">{0}</script>";
        public static void ConsoleLog(string message)
        {
            string function = "console.log('{0}');";
            string log = string.Format(GenerateCodeFromFunction(function), message);
            HttpContext.Current.Response.Write(log);
        }

        public static void Alert(string message)
        {
            string function = "alert('{0}');";
            string log = string.Format(GenerateCodeFromFunction(function), message);
            HttpContext.Current.Response.Write(log);
        }

        static string GenerateCodeFromFunction(string function)
        {
            return string.Format(scriptTag, function);
        }
    }
}
