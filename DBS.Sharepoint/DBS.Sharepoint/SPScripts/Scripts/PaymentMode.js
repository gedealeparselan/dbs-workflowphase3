var ViewModel = function () {
    var self = this;
    self.Readonly = ko.observable(false);
    self.IsWorkflow = ko.observable(false);
    // New Data flag
    self.IsNewData = ko.observable(false);
    //Parameter
    self.SingleValue = ko.observable("");
    self.SingleValueLabel = ko.observable("");
    self.SingleValueExisting = ko.observable("");
    self.SingleValueApproval = ko.observable("");
    self.LastModifiedBy = ko.observable("");
    self.LastModifiedDate = ko.observable("");
    self.IsRoleMaker = ko.observable(false);
    self.IsPermited = ko.observable(false);
    //ddl
    self.options = ko.observableArray([]);

    //Get Selected ddl
    self.OnChangeModeName = function () {
        //var ParameterValueID = parseInt($("#ParameterValue option:selected").text(), 10);
        var ParameterValueID = $("#modeName option:selected").text();
        var val1 = ParameterValueID.includes("BCP1");
        var val2 = ParameterValueID.includes("BCP2");
        var val3 = ParameterValueID.includes("IPE");

        if (val1) { ParameterValueID = 'BCP1' }
        if (val2) { ParameterValueID = 'BCP2' }
        if (val3) { ParameterValueID = 'IPE' }

        self.SingleValue(ParameterValueID);
        //console.log('singlevalue = ' + val1 + ' ; ' + val2 + ' ;' + val3);
    };

    //add || update data
    var SingleValueParameterData = {
        ID: 0,
        Name: "PAYMENT_MODE",
        Value: self.SingleValue,
        LastModifiedDate: new Date(),
        LastModifiedBy: "Unknown"
    }

    //Body
    self.SingleValueParameter = ko.observable(new SingleValueParameter(SingleValueParameterData));

    //Get Value
    self.GetData = function () { GetData(); }

    function GetData() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        var options = {
            url: api.server + api.url.paymentmode,
            params: {
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGet, OnError, OnAlways);
    }

    self.GetSelectedRow = function (data) {
        self.IsNewData(false);
        if (self.IsWorkflow()) {
            $("#modal-form-workflow").modal('show');
            var ParameterValueID = 1;
            if (data.Value == 'BCP1') {
                ParameterValueID = 1;
            }
            else if (data.Value == 'BCP2') {
                ParameterValueID = 2;
            }
            else if (data.Value == 'IPE') {
                ParameterValueID = 3;
            }
            else if (data.Value == null || data.Value == 'undefined') {
                ParameterValueID = 1;
            }
            self.SingleValue(ParameterValueID);
            self.SingleValueLabel(data.Value);
            self.SingleValueApproval(ParameterValueID);
        }
        else {
            $("#modal-form").modal('show');
        }
        self.SingleValueApproval(data.Value);
        self.LastModifiedBy(data.LastModifiedBy);
        self.LastModifiedDate(data.LastModifiedDate);
    };

    function OnSuccessGet(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            if (!self.IsWorkflow()) {

                //bind dropdown
                self.SingleValue(data);
                var singleVal = data;
                var DataConnect;
                var DataConnect2;
                var DataConnect3;

                if (singleVal == "BCP1") {
                    DataConnect = "(Lost Connection)";
                    DataConnect2 = "(Disconnected)";
                    DataConnect3 = " ";
                }

                if (singleVal == "BCP2") {
                    DataConnect2 = "(Disconnected)";
                    DataConnect = "(Lost Connection)";
                    DataConnect3 = " ";
                }

                if (singleVal == "IPE") {
                    DataConnect3 = " ";
                    DataConnect2 = "(Disconnected)";
                    DataConnect = "(Lost Connection)";
                }

                //array values
                var ObservableArray = [
                    new optionModel(1, "BCP1" + " " + DataConnect),
                    new optionModel(2, "BCP2" + " " + DataConnect2),
                    new optionModel(3, "IPE" + " " + DataConnect3)
                ];

                //push array values
                if (self.options() == null || self.options() == "" || self.options().length == 0) {
                    for (x = 0; x < ObservableArray.length; x++) {
                        self.options.push(ObservableArray[x]);
                    }
                    //console.log('option = ' + self.options().length);
                }

                //for validate existing data
                self.SingleValueExisting(data);

                //console.log('isi dataaa = ' + data);
                var ParameterValueID = 1;
                if (data == 'BCP1') {
                    ParameterValueID = 1;
                }
                else if (data == 'BCP2') {
                    //console.log('Options kepanggil = ');
                    ParameterValueID = 2;
                }
                else if (data == 'IPE') {
                    ParameterValueID = 3;
                }
                else if (data == null || data == 'undefined') {
                    ParameterValueID = 1;
                }

                // bind result to observable array
                self.SingleValue(ParameterValueID);

            } else {
                //ShowNotification(jqXHR.status + " HAHAHA " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
            }
        }
    }


    self.UpdateValue = function () { UpdateValue(); }

    self.IsPermited = function (IsPermitedResult) {
        //Ajax call to delete the Customer
        spUser = $.cookie(api.cookie.spUser);

        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/CheckUserPermited/" + _spFriendlyUrlPageContextInfo.title,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    //compare user role to page permission to get checker validation
                    for (i = 0; i < spUser.Roles.length; i++) {
                        for (j = 0; j < data.length; j++) {
                            if (Const_RoleName[spUser.Roles[i].ID] == data[j]) { //if (spUser.Roles[i].Name == data[j]) {
                                if (Const_RoleName[spUser.Roles[i].ID].endsWith('Maker')) { //if (spUser.Roles[i].Name.endsWith('Maker')) {
                                    self.IsRoleMaker(true);
                                    return;
                                }
                                else {
                                    self.IsRoleMaker(false);
                                }
                            }
                        }
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    function UpdateValue() {
        // validation
        var selectedID = self.SingleValue();
        if (selectedID == "1") {
            self.SingleValue("BCP1");
        }
        else if (selectedID == "2") {
            self.SingleValue("BCP2");
        }
        else if (selectedID == "3") {
            self.SingleValue("IPE");
        }
        console.log('data update = ' + self.SingleValue());
        var form = $("#aspnetForm");
        form.validate();
        console.log(ko.toJSON(self.SingleValueParameter()));
        if (form.valid()) {
            bootbox.confirm("Are you sure?", function (result) {
                if (self.SingleValueExisting() != self.SingleValue()) {
                    if (!result) {
                        $("#modal-form").modal('show');
                    } else {

                        var options = {
                            url: api.server + api.url.singlevalueparameter + "/" + "PAYMENT_MODE",
                            data: ko.toJSON(self.SingleValueParameter()),
                            params: {
                            },
                            token: accessToken
                        };

                        Helper.Ajax.Put(options, OnSuccessUpdate, OnError, OnAlways);
                        GetData();
                    }
                }
                else {
                    ShowNotification("" + " " + "WARNING", "Payment mode data Mode Name " + self.SingleValue() + " is already exist", 'gritter-error', true);
                }
            });
        }
    }

    function OnSuccessUpdate(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            // bind result to observable array
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-success', false);
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }

    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }
    });
}