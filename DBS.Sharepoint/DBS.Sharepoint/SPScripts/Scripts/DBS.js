//region untuk constant role id aridya 20170123
//contains ID and role name respectively
//use Const_RoleName[ID] to get the role name
var Const_RoleName = {};
//end region

var optionSelected = {
    IsCurrency: 13, //id currency IDR
    IsAccount: 13, //id account number IDR
    SelectedTanpaUnderying: 36
}

var Const_AmountLLD = {
    USDAmount: 100000,
    LLDDocID: 1,
    NonIDRSelected: 13
};

var Const_ProductType = {
    vsdBoardC: 8,
    vsdBoardT: 9,
    vsdQuote: 7
};

var Const_Force = {
    Complete: 1031,
    Cancel: 1032
};

var Const_BankCharge = {
    SHA: 3,
};

var Const_AgentCharge = {
    SHA: 3,
};

var Const_TagUntag = {
    Tag: 1154,
    Untag: 1155
};

//add aridya 20161030 for skn bulk document (instruction purpose, skn bulk type) ~OFFLINE~
var ConsSKNBulkDocumentDefaultValue = {
    DocumentTypeSKNBulk: 1007,
    PurposeDocInstruction: 1

}
//end add

var ConsProductID = {
    RTGSProductIDCons: 1,
    SKNProductIDCons: 2,
    OTTProductIDCons: 3,
    IDInvestmentProductIDCons: 12,
    SavingPlanProductIDCons: 13,
    UTOnshoreproductIDCons: 14,
    UTOffshoreProductIDCons: 15,
    UTCPFProductIDCons: 16,
    FDProductIDCons: 17,
    CollateralProductIDCons: 18,
    CIFProductIDCons: 19,
    LoanDisbursmentProductIDCons: 20,
    LoanRolloverProductIDCons: 21,
    LoanIMProductIDCons: 22,
    LoanSettlementProductIDCons: 1014,
    TMOProductIDCons: 1013,
    OverbookingProductIDCons: 2017,
    SKNBulkProductIDCons: 2018, //aridya add 20161011 SKN Bulk ~OFFLINE~
    FXProductIDCons: 46,
    FXNettingProductIDCons: 1015,
    DFProductIDCons: 3018
};

//hidayat
var ProductUnusedID = {
    21: 'RTGSProductIDCons',
    2: 'SKNProductIDCons'
};
//hidayat

var ConsTransactionType = {
    fdNewPlacement: 13,
    fdPrematurebreak: 14,
    fdBreakmaturity: 15,
    fdChangeInstruction: 16,
    fdMaintenance: 17,
    utSubcription: [4, 7, 10, 13],
    utRedemption: [5, 8, 11, 14],
    utSwitching: [6, 9, 12, 15],
    MaintenanceCons: 25,
    SuspendCIFCons: 26,
    StandingInstructionCons: 27,
    UnsuspendCIFCons: 31,
};
var ConsRequestType = {
    CancelStandingInstructionCons: 3,
    SubcriptionCons: 4,
    RedemptionCons: 5,
    SwitchingCons: 6,
    SubcriptionCons: 7,
    RedemptionCons: 8,
    SwitchingCons: 9,
    SubcriptionCons: 10,
    RedemptionCons: 11,
    SwitchingCons: 12,
    NewPlacementCons: 13,
    PrematureBreakCons: 14,
    BreakAtMaturityCons: 15,
    ChangeOfInstructionCons: 16,
    FDMaintenanceCons: 17,
    NewCons: 18,
    TemporaryRetrieveCons: 19,
    PermanentRetrieveCons: 20,
    ReturnCons: 21,
    CCPRequestCons: 22,
    ExtendCons: 23,
    ExceptionalPermanentReleaseCons: 24,
    MaintenanceCons: 25,
    SuspendCIFCons: 26,
    UnsuspendCIFCons: 1003,
    StandingInstructionCons: 1004
};
var TMONettingPurpose = {
    Rollover: 1147,
    Unwind: 1148,
    EarlyTermination: 1149
};
var ConsCIFMaintenanceType = {
    PengkinianDataCons: 6,
    UpdateFXTierCons: 7,
    AtmCardCons: 8,
    RiskRatingCons: 9,
    AddCurrencyCons: 10,
    AdditionalAccountCons: 11,
    LinkFFDCons: 12,
    FreezeUnfreezeCons: 13,
    ActiveDormantCons: 14,
    LPSCons: 15,
    LOIPOIPOACons: 16,
    ChangeRMCons: 17,
    ActiveHPSPCons: 18,
    SuspendCIF: 26,
    UnsuspendCIFCons: 31,
    StandingInstructionCons: 27,
    TagUntagStaffCons: 19,
    ATMClosure: 20,
    DispatchMode: 21,
    LienUnlien: 22
};
var ConsParSysCIFPekerjaan = {
    KaryawanPemerintahBUMN: 64,
    IbuRumahTangga: 65,
    Wiraswasta: 66,
    PelajarMahasiswa: 67,
    TNIPolisi: 68,
    Pensiunan: 69,
    KaryawanSwasta: 70,
    TidakBekerja: 71,
    Profesional: 72,
    Lainnya: 73
};
var ConsModification = {
    Add: 1,
    Edit: 2,
    Delete: 3
};
var ConsParSysCIFTujuanBukaRekening = {
    TransaksiPribadi: 74,
    TransaksiBisnis: 75,
    Investasi: 76,
    Pendidikan: 77,
    Tabungan: 78,
    Lainnya: 79
}
var ConsParSysSumberDana = {
    Gaji: 48,
    HasilUsaha: 49,
    SimpananPribadi: 50,
    PemberianWarisan: 51,
    OrangTua: 52,
    Pasangan: 53,
    Lainnya: 54
};
var ConsMaritalStatus = {
    BelumMenikah: 1,
    DudaJanda: 2,
    SudahMenikah: 3
};
var ConsDocFlowTransType = {
    Others: 49
}
var ConsDispatchModeType = {
    BSPLDeliveryAndEmail: 1,
    CollectByPerson: 2,
    Email: 3,
    NoDispatch: 4,
    Post: 5,
    SSPLDelivery: 6
};
var ConsEmailApprovalDefaultValue = {
    DocumentTypeDash: 1,
    PurposeDocEmailApproval: 7

}
var ConsSumberDana = {
    Pemberian: 51,
    Parent: 52,
    Pasangan: 53,
    Lain: 54
};
//henggar form vaidation IPE
var Currency = {
    IDRselected: 13,
    LimitEqvUSD: 1000000.00
}
var ConsPARSYS = {
    clTypeDoc: "CL_TYPE_OF_DOCUMENT",
    clCollateralStatus: "CL_COLLATERAL_STATUS",
    clLoanStatus: "CL_LOAN_STATUS",
    fdCuttOff: "FD_CUTTOFF",
    fdRemarks: "FD_REMARKS",
    utCuttOffIdInvestment: "UT_CUTOFF_IDINVESTMENT",
    utCuttOffSP: "UT_CUTOFF_SAVINGPLAN",
    utCuttoffUT: "UT_CUTOFF_UTPRODUCT",
    utBringupSP: "UT_BRINGUP_SAVINGPLAN",
    utBringupUT: "UT_BRINGUP_UTPRODUCT",
    utMaxRisk: "UT_MAXIMUMRISK",
    utMinRisk: "UT_MINIMUM_RISK",
    utFNACore: "UT_FNACORE",
    utFunctionType: "UT_FUNCTION_TYPE",
    utAccType: "UT_ACCOUNT_TYPE",
    cifCuttof: "CIF_CUTOFF",
    cifBringup: "CIF_BRINGUP",
    cifMaintenanceType: "CIF_MAINTENANCE_TYPE",
    cifReasonCallback: "CIF_REASON_CALLBACK",
    cifJenisIdentitas: "CIF_JENIS_IDENTITAS",
    cifSumberDana: "CIF_SUMBER_DANA",
    cifAssetBersih: "CIF_ASSET_BERSIH",
    cifPendapatanBulanan: "CIF_PENDAPATAN_BULANAN",
    cifPenghasilanTambahan: "CIF_PENGHASILAN_TAMBAHAN",
    cifPekerjaan: "CIF_PEKERJAAN",
    cifTujuanPembukaanRek: "CIF_TUJUAN_PEMBUKAAN_REKENING",
    cifPerkiraanDanaKeluar: "CIF_PERKIRAAN_DANA_KELUAR",
    cifPerkiraanTransKeluar: "CIF_PERKIRAAN_TRANSAKSI_KELUAR",
    tmoMaxUnderlying: "TMO_MAX_DAY_UNDERLYING",
    tmoMaxStatement: "TMO_MAX_DAY_STATEMENT",
    tmoResBuy: "TMO_RESIDENT_BUY_THRESHOLD",
    tmoResSell: "TMO_RESIDENT_SELL_THRESHOLD",
    tmoNonResBuy: "TMO_NONRESIDENT_BUY_THRESHOLD",
    tmoNonResSell: "TMO_NONRESIDENT_SELL_THRESHOLD",
    tmoNettingPurpose: "TMO_NETTING_PURPOSE",
    loanCustCategory: "LOAN_CUSTOMER_CATEGORY",
    loanType: "LOAN_TYPE",
    loanProgramType: "LOAN_PROGRAM_TYPE",
    loanSchemeCode: "LOAN_SCHEME_CODE",
    loanInterestCode: "LOAN_INTEREST_RATE_CODE",
    loanrepricingPlan: "LOAN_REPRICING_PLAN",
    loanPrincipalFreq: "LOAN_PRINCIPAL_FREQ",
    loanInterestFreq: "LOAN_INTEREST_FREQ",
    loanPeggingFreq: "LOAN_PEGGING_FREQ",
    loanBringup: "LOAN_BRINGUP",
    loanMaintenanceType: "LOAN_MAINTENANCE_TYPE",
    tmoNettingPurpose: "TMO_NETTING_PURPOSE",
    cifCallbackTimeReason: "CIF_REASON",
    utCutOffSubcription: "UT_CUTOFF_SUBCRIPTION",
    utCutOffRedemption: "UT_CUTOFF_REDEMPTION",
    utCutOffSwitching: "UT_CUTOFF_SWITCHING",
    cifCutOffBranchMaker: "CIF_CUTOFF_BRANCH_MAKER",
    cifCutOffBranchChecker: "CIF_CUTOFF_BRANCH_CHECKER"
};
var Util = {
    spitemdoc: "spitemdoc"
};
var ConsLoanSchemeCode = {
    LongLTLAA: 107,
    LongSLLAA: 109,
    ShortSTLA: 106,
    ShortSSLAA: 108,

};

var ProductName = {
    sknbulk: "sknbulk", //add aridya 20161012 sknbulk ~OFFLINE~
    payment: "payment",
    fd: "fd",
    ut: "ut",
    cif: "cif",
    tmo: "tmo",
    collateral: "collateral",
    loan: "loan",
    df: "df"
};
var nintexStatus = {
    payment: "payment",
    loan: "loan"
};
var ConsUTPar = {
    funcAdd: 19,
    funcModify: 20,
    funcClose: 21,
    accSingle: 22,
    accJoin: 23,
    fnaYes: 17,
    fnaNo: 18
};
//henggar
var Const_Underlying = {
    SelectedStatement: 2,
    SelectedUnderlying: 36
};

var Const_OverBookingProduct = {
    SelectedBeneBank: 2,
    SelectedProduct: 2017
};

//chandra added constant variable
var CONST_STATEMENT = {
    StatementA_ID: 1,
    StatementB_ID: 2,
    Dash_ID: 3,
    AnnualStatement_ID: 4
};
var ConsCustomer = {
    Affiliate: 98,
    NonAffiliate: 99
};
var ConsParSysCIFCallback = {
    NoPickUp: 1147
}

$.cookie.defaults.path = '/';
$.cookie('api.cookie.timeout', 0);

// BLOCK PAGE UNTIL USER DATA EXISTS //
if ($.cookie(api.cookie.spUser) == null) {
    bootbox.dialog({
        closeButton: false,
        message: '<center><i class="icon-spinner icon-spin red bigger-125"></i><br><h8>Loading...</h8></center>',
        buttons: {}
    });
    var iSpUser = setInterval(function () {
        if ($.cookie(api.cookie.spUser) != null) {
            bootbox.hideAll();	// close block if spUser exists
            clearInterval(iSpUser);
        }
    }, 1000);
}

function GetRoleName() {
    if ($.cookie(api.cookie.name) == undefined) {
        Helper.Token.Request(function () {
            Const_RoleName = {};
            var intervalCookie = setInterval(function () {
                if ($.cookie().SPUser != undefined) {
                    if (typeof $.cookie().SPUser == 'object') {
                        $.ajax({
                            type: "GET",
                            url: "/_api/web/GetUserById(" + $.cookie().SPUser.ID + ")/Groups?$select=Id,Title&$filter=startswith(Title, 'DBS')", //"/_api/web/sitegroups?$select=Id,Title&$filter=startswith(Title, 'DBS')",//GetUserById(" + datas.d.Id + ")/Groups?$select=Id,Title&$filter=startswith(Title, 'DBS')",
                            headers: {
                                "accept": "application/json; odata=verbose"
                            },
                            success: function (data, textStatus, jqXHR) {
                                for (i = 0; i < data.d.results.length; i++) {
                                    Const_RoleName[data.d.results[i].Id] = data.d.results[i].Title;
                                }
                                //$(document).trigger("RoleNameReady");
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                error(jqXHR, textStatus, errorThrown);
                            }
                        });
                        clearInterval(intervalCookie);
                    }
                }
            }, 100);
        }, function () { });
    } else {
        Const_RoleName = {};
        if (typeof $.cookie().SPUser == 'object') {
            $.ajax({
                type: "GET",
                url: "/_api/web/GetUserById(" + $.cookie().SPUser.ID + ")/Groups?$select=Id,Title&$filter=startswith(Title, 'DBS')", //"/_api/web/sitegroups?$select=Id,Title&$filter=startswith(Title, 'DBS')",//GetUserById(" + datas.d.Id + ")/Groups?$select=Id,Title&$filter=startswith(Title, 'DBS')",
                headers: {
                    "accept": "application/json; odata=verbose"
                },
                success: function (data, textStatus, jqXHR) {
                    for (i = 0; i < data.d.results.length; i++) {
                        Const_RoleName[data.d.results[i].Id] = data.d.results[i].Title;
                    }
                    //$(document).trigger("RoleNameReady");
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    error(jqXHR, textStatus, errorThrown);
                }
            });
        }
    }
}
window.onload = GetRoleName();


function GetRoleName() {
    if ($.cookie(api.cookie.name) == undefined) {
        Helper.Token.Request(function () {
            Const_RoleName = {};
            var intervalCookie = setInterval(function () {
                if ($.cookie().SPUser != undefined) {
                    if (typeof $.cookie().SPUser == 'object') {
                        $.ajax({
                            type: "GET",
                            url: "/_api/web/GetUserById(" + $.cookie().SPUser.ID + ")/Groups?$select=Id,Title&$filter=startswith(Title, 'DBS')", //"/_api/web/sitegroups?$select=Id,Title&$filter=startswith(Title, 'DBS')",//GetUserById(" + datas.d.Id + ")/Groups?$select=Id,Title&$filter=startswith(Title, 'DBS')",
                            headers: {
                                "accept": "application/json; odata=verbose"
                            },
                            success: function (data, textStatus, jqXHR) {
                                for (i = 0; i < data.d.results.length; i++) {
                                    Const_RoleName[data.d.results[i].Id] = data.d.results[i].Title;
                                }
                                //$(document).trigger("RoleNameReady");
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                error(jqXHR, textStatus, errorThrown);
                            }
                        });
                        clearInterval(intervalCookie);
                    }
                }
            }, 100);
        }, function () { });
    } else {
        Const_RoleName = {};
        if (typeof $.cookie().SPUser == 'object') {
            $.ajax({
                type: "GET",
                url: "/_api/web/GetUserById(" + $.cookie().SPUser.ID + ")/Groups?$select=Id,Title&$filter=startswith(Title, 'DBS')", //"/_api/web/sitegroups?$select=Id,Title&$filter=startswith(Title, 'DBS')",//GetUserById(" + datas.d.Id + ")/Groups?$select=Id,Title&$filter=startswith(Title, 'DBS')",
                headers: {
                    "accept": "application/json; odata=verbose"
                },
                success: function (data, textStatus, jqXHR) {
                    for (i = 0; i < data.d.results.length; i++) {
                        Const_RoleName[data.d.results[i].Id] = data.d.results[i].Title;
                    }
                    //$(document).trigger("RoleNameReady");
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    error(jqXHR, textStatus, errorThrown);
                }
            });
        }
    }
}
window.onload = GetRoleName();

$(document).ready(function () {
    // CLOSE BLOCK PAGE IF SYSTEM ACCOUNT
    if ($('.user-info > span').html() == 'System Account') bootbox.hideAll();

    // ANTI TIMEOUT //
    //setInterval(function () {
    //    $.ajax({
    //        url: "/",
    //        cache: false,
    //        success: function () {
    //            //console.log(result);
    //        }
    //    });
    //}, 60000);
    // TIMEOUT //
    if (api.timeout.active) {
        //$('ul.ace-nav>li>ul.user-menu').prepend('<li><a href="#"><i class="icon-time"></i><span id="TIMEOUT_COUNTER"></span></a></li>');
        api.timeout.interval = setInterval(function () {
            $.cookie('api.cookie.timeout', $.cookie('api.cookie.timeout') + 1);
            $('#TIMEOUT_COUNTER').html(api.timeout.value - $.cookie('api.cookie.timeout'));
            if ($.cookie('api.cookie.timeout') > api.timeout.value) {
                window.location.href = '/_layouts/15/SignOut.aspx';
            }
        }, 1000);
        $.ajaxSetup({
            beforeSend: function (jqXHR, data) {
                if (data.url.substr(0, 4) != '/?_=' && data.url.indexOf('WorkflowEntity?page=') == -1) $.cookie('api.cookie.timeout', 0); // no reset counter if ajax request on home page for background refresh purpose
            },
            complete: function (jqXHR, data) {
            }
        });
    }
    // AUTO LOAD WORKFLOW FEATURE ON REGISTERED MODULES IN APIConfig.js
    var module = api.workflow.entity.modules[window.location.pathname];
    if (module != null) {
        // widget loader
        $box = $('#widget-box');
        var event;
        $box.trigger(event = $.Event('reload.ace.widget'))
        if (event.isDefaultPrevented()) return
        $box.blur();

        $.getScript('/SiteAssets/Scripts/System.js', function () {
            modelFile = '/SiteAssets/Scripts/Models.js';
            if (module.nomodels != null && module.nomodels) modelFile = '/SiteAssets/Scripts/Blank.js';
            $.getScript(modelFile, function () {
                $.getScript('/SiteAssets/Scripts/WorkflowEntity.js', function () {
                    if (module.file == '') {
                        // CONVERT CONFIGURATION INTO DIALOG CONFIGURATION FOR AUTOMATIC LOAD MODULES
                        var dialogs = new Array();
                        $.each(api.workflow.entity.modules, function (index, item) {
                            if (item.url != undefined)
                                name = item.url.substr(item.url.lastIndexOf('/') + 1);

                            if (name != '')
                                if (dialogs[name] == null) {
                                    dialogs[name] = { file: item.file };
                                }
                        });

                        module.dialogs = dialogs;
                        module.columns = { sort: '', filter: '', row: '', fields: [] };
                        new WorkflowEntityModel(module);
                    } else {
                        function LoadModule() {
                            $.getScript(module.file, function () {
                                //var tableColumns = GetMainTableItems(module); // Get fields from main table to add on workflow table
                                var viewModel = new ViewModel();
                                // ADD Readonly observable property if not exitst

                                ko.applyBindings(viewModel);

                                // Token Validation
                                if ($.cookie(api.cookie.name) == undefined) {
                                    Helper.Token.Request(function (data, textStatus, jqXHR) {
                                        // store token on browser cookies
                                        $.cookie(api.cookie.name, data.AccessToken);
                                        accessToken = $.cookie(api.cookie.name);

                                        // call spuser function
                                        GetCurrentUser();

                                        // call get data inside view model
                                        if (viewModel.GetDropdown != null) viewModel.GetDropdown();
                                        if (viewModel.GetParameters != null) viewModel.GetParameters();
                                        if (viewModel.GetData != null) viewModel.GetData();
                                        if (viewModel.IsPermited != null) viewModel.IsPermited();
                                        if (viewModel.SetAutoCompleted != null) viewModel.SetAutoCompleted();
                                        if (viewModel.GetDataAvailableCustomer != null) viewModel.GetDataAvailableCustomer();
                                        if (viewModel.GetDataEscalatedCustomer != null) viewModel.GetDataEscalatedCustomer();

                                        viewModel.GetData();
                                        if (!(module.disable != null && module.disable == true)) {
                                            module.view = viewModel; // set current view to config
                                            new WorkflowEntityModel(module);
                                        }

                                    }, TokenOnError);
                                } else {
                                    // read token from cookie
                                    accessToken = $.cookie(api.cookie.name);
                                    // call spuser function
                                    GetCurrentUser(viewModel);
                                    // call get data inside view model
                                    if (viewModel.GetParameters != null) viewModel.GetParameters();
                                    if (viewModel.GetDropdown != null) viewModel.GetDropdown();
                                    if (viewModel.GetData != null) viewModel.GetData();
                                    if (viewModel.IsPermited != null) viewModel.IsPermited();
                                    if (viewModel.SetAutoCompleted != null) viewModel.SetAutoCompleted();
                                    if (viewModel.GetDataAvailableCustomer != null) viewModel.GetDataAvailableCustomer();
                                    if (viewModel.GetDataEscalatedCustomer != null) viewModel.GetDataEscalatedCustomer();

                                    if (!(module.disable != null && module.disable == true)) {
                                        module.view = viewModel; // set current view to config
                                        new WorkflowEntityModel(module);
                                    }

                                }
                            });
                        }
                        dialog_id = 'modal-form';
                        if (module.modalid != null && module.modalid != '') dialog_id = module.modalid;
                        var dialog = $('#' + dialog_id + ' > .modal-dialog > .modal-content > .modal-body');

                        if (dialog.length > 0) {/*
                         if(dialog.prop('innerHTML').length>100){ // DONT LOAD FORM IF FORM ALREADY EXISTS!
                         $('button',$('#modal-form')).each(function(){
                         b = $(this).attr('data-bind');
                         if(b)$(this).attr('data-bind',b+'&& !$root.Readonly()');
                         });
                         module.columns = GetMainTableItems(module);
                         LoadModule();
                         }else{
                         dialog.load('/Pages/Dialogs/' + module.url.substr(module.url.lastIndexOf('/') + 1) + '.html',function(){
                         $('button',$('#modal-form')).each(function(){
                         b = $(this).attr('data-bind');
                         if(b)$(this).attr('data-bind',b+'&& !$root.Readonly()');
                         });
                         module.columns = GetMainTableItems(module);
                         LoadModule();
                         });
                         }*/
                            //aridya add for CCU 20170323
                            var url = module.url.substr(module.url.lastIndexOf('/') + 1);
                            if (url.lastIndexOf('CCU') > -1) {
                                url = url.substr(0, url.lastIndexOf('CCU'));
                            }
                            //end aridya
                            dialog.load('/Pages/Dialogs/' + url + '.html', function () {
                                $('button', $('#modal-form')).each(function () {
                                    b = $(this).attr('data-bind');
                                    if (b) $(this).attr('data-bind', b + '&& !$root.Readonly()');
                                });
                                module.columns = GetMainTableItems(module);
                                LoadModule();
                            });
                        } else { // SINGLE VALUE
                            module.columns = {
                                sort: '<th>Value</th>',
                                filter: '<th></th>',
                                row: '<td><span data-bind="text: Value"></span></td>',
                                fields: ['Value']
                            };
                            LoadModule();
                        }
                    }
                });
            });
        });
    }
});