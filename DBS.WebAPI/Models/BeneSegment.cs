﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Models
{
    #region Property
    [ModelName("BeneSegment")]
    public class BeneSegmentModel
    {
        /// <summary>
        /// Bene Segment ID.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Bene Segment Name.
        /// </summary>
        [MaxLength(50, ErrorMessage = "Bene Segment Name is must be in {1} characters.")]
        public string Name { get; set; }

        /// <summary>
        /// Bene Segment Description.
        /// </summary>
        [MaxLength(255, ErrorMessage = "Maximum length is {1} characters.")]
        public string Description { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }
    }

    public class BeneSegmentRow : BeneSegmentModel
    {
        public int RowID { get; set; }

    }

    public class BeneSegmentGrid : Grid
    {
        public IList<BeneSegmentRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class BeneSegmentFilter : Filter { }
    #endregion

    #region Interface
    public interface IBeneSegmentRepository : IDisposable
    {
        bool GetBeneSegmentByID(int id, ref BeneSegmentModel beneSegment, ref string message);
        bool GetBeneSegment(ref IList<BeneSegmentModel> beneSegments, int limit, int index, ref string message);
        bool GetBeneSegment(ref IList<BeneSegmentModel> beneSegments, ref string message);
        bool GetBeneSegment(int page, int size, IList<BeneSegmentFilter> filters, string sortColumn, string sortOrder, ref BeneSegmentGrid beneSegment, ref string message);
        bool GetBeneSegment(BeneSegmentFilter filter, ref IList<BeneSegmentModel> beneSegments, ref string message);
        bool GetBeneSegment(string key, int limit, ref IList<BeneSegmentModel> beneSegments, ref string message);
        bool AddBeneSegment(BeneSegmentModel beneSegment, ref string message);
        bool UpdateBeneSegment(int id, BeneSegmentModel beneSegment, ref string message);
        bool DeleteBeneSegment(int id, ref string message);
    }
    #endregion

    #region Repository
    public class BeneSegmentRepository : IBeneSegmentRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetBeneSegmentByID(int id, ref BeneSegmentModel beneSegment, ref string message)
        {
            bool isSuccess = false;

            try
            {
                beneSegment = (from a in context.BeneSegments
                               where a.IsDeleted.Equals(false) && a.BeneSegmentID.Equals(id)
                               select new BeneSegmentModel
                               {
                                   ID = a.BeneSegmentID,
                                   Name = a.BeneSegmentName,
                                   Description = a.BeneSegmentDescription,
                                   LastModifiedDate = a.CreateDate,
                                   LastModifiedBy = a.CreateBy
                               }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetBeneSegment(ref IList<BeneSegmentModel> beneSegments, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    beneSegments = (from a in context.BeneSegments
                                    where a.IsDeleted.Equals(false)
                                    orderby new { a.BeneSegmentName, a.BeneSegmentDescription }
                                    select new BeneSegmentModel
                                    {
                                        ID = a.BeneSegmentID,
                                        Name = a.BeneSegmentName,
                                        Description = a.BeneSegmentDescription,
                                        LastModifiedBy = a.CreateBy,
                                        LastModifiedDate = a.CreateDate
                                    }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetBeneSegment(ref IList<BeneSegmentModel> beneSegments, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (DBSEntities context = new DBSEntities())
                {
                    beneSegments = (from a in context.BeneSegments
                                    where a.IsDeleted.Equals(false)
                                    orderby new { a.BeneSegmentName, a.BeneSegmentDescription }
                                    select new BeneSegmentModel
                                    {
                                        ID = a.BeneSegmentID,
                                        Name = a.BeneSegmentName,
                                        Description = a.BeneSegmentDescription,
                                        LastModifiedBy = a.CreateBy,
                                        LastModifiedDate = a.CreateDate
                                    }).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetBeneSegment(int page, int size, IList<BeneSegmentFilter> filters, string sortColumn, string sortOrder, ref BeneSegmentGrid beneSegmentGrid, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                BeneSegmentModel filter = new BeneSegmentModel();
                if (filters != null)
                {
                    filter.Name = (string)filters.Where(a => a.Field.Equals("Name")).Select(a => a.Value).SingleOrDefault();
                    filter.Description = (string)filters.Where(a => a.Field.Equals("Description")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.BeneSegments
                                let isBeneSegmentName = !string.IsNullOrEmpty(filter.Name)
                                let isBeneSegmentDescription = !string.IsNullOrEmpty(filter.Description)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) &&
                                    (isBeneSegmentName ? a.BeneSegmentName.Contains(filter.Name) : true) &&
                                    (isBeneSegmentDescription ? a.BeneSegmentDescription.Contains(filter.Description) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new BeneSegmentModel
                                {
                                    ID = a.BeneSegmentID,
                                    Name = a.BeneSegmentName,
                                    Description = a.BeneSegmentDescription,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    beneSegmentGrid.Page = page;
                    beneSegmentGrid.Size = size;
                    beneSegmentGrid.Total = data.Count();
                    beneSegmentGrid.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    beneSegmentGrid.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new BeneSegmentRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            Name = a.Name,
                            Description = a.Description,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetBeneSegment(BeneSegmentFilter filter, ref IList<BeneSegmentModel> beneSegments, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetBeneSegment(string key, int limit, ref IList<BeneSegmentModel> beneSegments, ref string message)
        {
            bool isSuccess = false;

            try
            {
                beneSegments = (from a in context.BeneSegments
                                where a.IsDeleted.Equals(false) &&
                                    a.BeneSegmentName.Contains(key) || a.BeneSegmentDescription.Contains(key)
                                orderby new { a.BeneSegmentName, a.BeneSegmentDescription }
                                select new BeneSegmentModel
                                {
                                    Name = a.BeneSegmentName,
                                    Description = a.BeneSegmentDescription
                                }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddBeneSegment(BeneSegmentModel beneSegment, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (!context.BeneSegments.Where(a => a.BeneSegmentName.Equals(beneSegment.Name) && a.IsDeleted.Equals(false)).Any())
                {
                    context.BeneSegments.Add(new Entity.BeneSegment()
                    {
                        BeneSegmentName = beneSegment.Name,
                        BeneSegmentDescription = beneSegment.Description,
                        IsDeleted = false,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    });

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Bene Segment data for Bene Segment Name {0} is already exist.", beneSegment.Name);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateBeneSegment(int id, BeneSegmentModel beneSegment, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.BeneSegment data = context.BeneSegments.Where(a => a.BeneSegmentID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    if (context.BeneSegments.Where(a => a.BeneSegmentID != beneSegment.ID && a.BeneSegmentName.Equals(beneSegment.Name) && a.IsDeleted.Equals(false)).Count() >= 1)
                    {
                        message = string.Format("Bene Segment Name {0} is exist.", beneSegment.Name);
                    }
                    else
                    {
                        data.BeneSegmentName = beneSegment.Name;
                        data.BeneSegmentDescription = beneSegment.Description;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        context.SaveChanges();

                        isSuccess = true;
                    }
                }
                else
                {
                    message = string.Format("Bene Segment data for Bene Segment ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeleteBeneSegment(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.BeneSegment data = context.BeneSegments.Where(a => a.BeneSegmentID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Bene Segment data for Bene Segment ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}