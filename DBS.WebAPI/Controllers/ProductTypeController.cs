﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class ProductTypeController : ApiController
    {
        private string message = string.Empty;

        private readonly IProductTypeRepository repository = null;

        public ProductTypeController()
        {
            this.repository = new ProductTypeRepository();
        }

        public ProductTypeController(IProductTypeRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all ProductTypes.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<ProductTypeModel>))]
        public HttpResponseMessage Get()
        {
            IList<ProductTypeModel> output = new List<ProductTypeModel>();

            if (!repository.GetProductType(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<ProductTypeModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get ProductTypes with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<ProductTypeModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<ProductTypeModel> output = new List<ProductTypeModel>();

            if (!repository.GetProductType(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<IList<ProductTypeModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get ProductTypes for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(ProductTypeGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<ProductTypeFilter> filters)
        {
            ProductTypeGrid output = new ProductTypeGrid();

            if (!repository.GetProductType(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Code" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<ProductTypeGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find ProductTypes using criterias Code or Description.
        /// </summary>
        /// <param name="query">Code or Description.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<ProductTypeModel>))]
        [HttpGet]
        [Route("api/ProductType/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<ProductTypeModel> output = new List<ProductTypeModel>();

            if (!repository.GetProductType(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<ProductTypeModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get ProductType by ID.
        /// </summary>
        /// <param name="id">ProductType ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(ProductTypeModel))]
        [Route("api/ProductType/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            ProductTypeModel output = new ProductTypeModel();

            if (!repository.GetProductTypeByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<ProductTypeModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new ProductType.
        /// </summary>
        /// <param name="ProductType">ProductType data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Post(ProductTypeModel ProductType)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddProductType(ProductType, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New ProductType data has been created." });
                }
            }
        }

        /// <summary>
        /// Modify existing ProductType.
        /// </summary>
        /// <param name="id">ID of ProductType to be modify.</param>
        /// <param name="ProductType">ProductType data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/ProductType/{id}")]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(int id, ProductTypeModel ProductType)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateProductType(id, ProductType, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "ProductType data has been updated." });
                }
            }
        }

        /// <summary>
        /// Remove exisiting ProductType.
        /// </summary>
        /// <param name="id">ID of ProductType to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/ProductType/{id}")]
        [ResponseType(typeof(string))]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteProductType(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Product data has been deleted." });
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    
    }
}