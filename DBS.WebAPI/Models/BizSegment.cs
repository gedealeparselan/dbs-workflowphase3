﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Models
{
    #region Property
    [ModelName("BizSegment")]
    public class BizSegmentModel
    {
        /// <summary>
        /// BizSegment ID.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Curreny Name.
        /// </summary>
        //[MaxLength(50, ErrorMessage = "BizSegment Name is must be in {1} characters.")]
        public string Name { get; set; }

        /// <summary>
        /// Currency Description.
        /// </summary>
        //[MaxLength(255, ErrorMessage = "Maximum length is {1} characters.")]
        public string Description { get; set; }

        /// <summary>
        /// Last modified date
        /// </summary>
        public DateTime? LastModifiedDate { get; set; }

        /// <summary>
        /// Last modified By
        /// </summary>
        public string LastModifiedBy { get; set; }
    }

    public class BizSegmentRow : BizSegmentModel
    {
        public int RowID { get; set; }

    }

    public class BizSegmentGrid : Grid
    {
        public IList<BizSegmentRow> Rows { get; set; }
    }

    #endregion

    #region Filter
    public class BizSegmentFilter : Filter { }
    #endregion

    #region Interface
    public interface IBizSegmentRepository : IDisposable
    {
        bool GetBizSegmentByID(int id, ref BizSegmentModel bizSegment, ref string message);
        bool GetBizSegment(ref IList<BizSegmentModel> bizSegments, int limit, int index, ref string message);
        bool GetBizSegment(int page, int size, IList<BizSegmentFilter> filters, string sortColumn, string sortOrder, ref BizSegmentGrid bizSegment, ref string message);
        bool GetBizSegment(ref IList<BizSegmentModel> bizSegments, ref string message);
        bool GetBizSegment(BizSegmentFilter filter, ref IList<BizSegmentModel> bizSegments, ref string message);
        bool GetBizSegment(string key, int limit, ref IList<BizSegmentModel> bizSegments, ref string message);
        bool AddBizSegment(BizSegmentModel bizSegment, ref string message);
        bool UpdateBizSegment(int id, BizSegmentModel bizSegment, ref string message);
        bool DeleteBizSegment(int id, ref string message);
    }
    #endregion

    #region Repository
    public class BizSegmentRepository : IBizSegmentRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        public bool GetBizSegmentByID(int id, ref BizSegmentModel bizSegment, ref string message)
        {
            bool isSuccess = false;

            try
            {
                bizSegment = (from a in context.BizSegments
                              where a.IsDeleted.Equals(false) && a.BizSegmentID.Equals(id)
                              select new BizSegmentModel
                            {
                                ID = a.BizSegmentID,
                                Name = a.BizSegmentName,
                                Description = a.BizSegmentDescription,
                                LastModifiedDate = a.CreateDate,
                                LastModifiedBy = a.CreateBy
                            }).SingleOrDefault();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetBizSegment(ref IList<BizSegmentModel> bizSegments, ref string message)
        { 
         bool isSuccess = false;

            try
            {

                using (DBSEntities context = new DBSEntities())
                {
                    bizSegments = (from a in context.BizSegments
                                  where a.IsDeleted.Equals(false)
                                  orderby new { a.BizSegmentName, a.BizSegmentDescription }
                                  select new BizSegmentModel
                                  {
                                      ID = a.BizSegmentID,
                                      Name = a.BizSegmentName,
                                      Description = a.BizSegmentDescription,
                                      LastModifiedBy = a.CreateBy,
                                      LastModifiedDate = a.CreateDate
                                  }).ToList();
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }
        public bool GetBizSegment(ref IList<BizSegmentModel> bizSegment, int limit, int index, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    bizSegment = (from a in context.BizSegments
                                where a.IsDeleted.Equals(false)
                                  orderby new { a.BizSegmentName, a.BizSegmentDescription }
                                  select new BizSegmentModel
                                {
                                    ID = a.BizSegmentID,
                                    Name = a.BizSegmentName,
                                    Description = a.BizSegmentDescription,
                                    LastModifiedBy = a.CreateBy,
                                    LastModifiedDate = a.CreateDate
                                }).Skip(skip).Take(limit).ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetBizSegment(int page, int size, IList<BizSegmentFilter> filters, string sortColumn, string sortOrder, ref BizSegmentGrid customer, ref string message)
        {
            bool isSuccess = false;

            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                BizSegmentModel filter = new BizSegmentModel();
                if (filters != null)
                {
                    filter.Name = (string)filters.Where(a => a.Field.Equals("Name")).Select(a => a.Value).SingleOrDefault();
                    filter.Description = (string)filters.Where(a => a.Field.Equals("Description")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = (string)filters.Where(a => a.Field.Equals("LastModifiedBy")).Select(a => a.Value).SingleOrDefault();

                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse((string)filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }

                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.BizSegments
                                let isCode = !string.IsNullOrEmpty(filter.Name)
                                let isDesc = !string.IsNullOrEmpty(filter.Description)
                                let isCreateBy = !string.IsNullOrEmpty(filter.LastModifiedBy)
                                let isCreateDate = filter.LastModifiedDate.HasValue ? true : false
                                where a.IsDeleted.Equals(false) &&
                                    (isCode ? a.BizSegmentName.Contains(filter.Name) : true) &&
                                    (isDesc ? a.BizSegmentDescription.Contains(filter.Description) : true) &&
                                    (isCreateBy ? (a.UpdateDate == null ? a.CreateBy.Contains(filter.LastModifiedBy) : a.UpdateBy.Contains(filter.LastModifiedBy)) : true) &&
                                    (isCreateDate ? ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate) > filter.LastModifiedDate.Value && ((a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value) < maxDate)) : true)
                                select new BizSegmentModel
                                {
                                    ID = a.BizSegmentID,
                                    Name = a.BizSegmentName,
                                    Description = a.BizSegmentDescription,
                                    LastModifiedBy = a.UpdateDate == null ? a.CreateBy : a.UpdateBy,
                                    LastModifiedDate = a.UpdateDate == null ? a.CreateDate : a.UpdateDate.Value
                                });

                    customer.Page = page;
                    customer.Size = size;
                    customer.Total = data.Count();
                    customer.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    customer.Rows = data.OrderBy(orderBy).AsEnumerable()
                        .Select((a, i) => new BizSegmentRow
                        {
                            RowID = i + 1,
                            ID = a.ID,
                            Name = a.Name,
                            Description = a.Description,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate
                        })
                        .Skip(skip)
                        .Take(size)
                        .ToList();
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool GetBizSegment(BizSegmentFilter filter, ref IList<BizSegmentModel> customer, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool GetBizSegment(string key, int limit, ref IList<BizSegmentModel> bizSegment, ref string message)
        {
            bool isSuccess = false;

            try
            {
                bizSegment = (from a in context.BizSegments
                            where a.IsDeleted.Equals(false) &&
                                a.BizSegmentName.Contains(key) || a.BizSegmentDescription.Contains(key)
                              orderby new { a.BizSegmentName, a.BizSegmentDescription }
                              select new BizSegmentModel
                            {
                                Name = a.BizSegmentName,
                                Description = a.BizSegmentDescription
                            }).Take(limit).ToList();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool AddBizSegment(BizSegmentModel bizSegment, ref string message)
        {
            bool isSuccess = false;

            try
            {
                if (!context.BizSegments.Where(a => a.BizSegmentName.Equals(bizSegment.Name) && a.IsDeleted.Equals(false)).Any())
                {
                    context.BizSegments.Add(new Entity.BizSegment()
                    {
                        BizSegmentName = bizSegment.Name,
                        BizSegmentDescription = bizSegment.Description,
                        IsDeleted = false,
                        CreateDate = DateTime.UtcNow,
                        CreateBy = currentUser.GetCurrentUser().DisplayName
                    });

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("BizSegment data for bizSegment Name {0} is already exist.", bizSegment.Name);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool UpdateBizSegment(int id, BizSegmentModel bizSegment, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.BizSegment data = context.BizSegments.Where(a => a.BizSegmentID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    if (context.BizSegments.Where(a => a.BizSegmentID != bizSegment.ID && a.BizSegmentName.Equals(bizSegment.Name) && a.IsDeleted.Equals(false)).Count() >= 1)
                    {
                        message = string.Format("Biz Segment name {0} is exist.", bizSegment.Name);
                    }
                    else
                    {
                        data.BizSegmentName = bizSegment.Name;
                        data.BizSegmentDescription = bizSegment.Description;
                        data.UpdateDate = DateTime.UtcNow;
                        data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                        context.SaveChanges();

                        isSuccess = true;
                    }
                }
                else
                {
                    message = string.Format("Biz Segment data for Biz Segment ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public bool DeleteBizSegment(int id, ref string message)
        {
            bool isSuccess = false;

            try
            {
                Entity.BizSegment data = context.BizSegments.Where(a => a.BizSegmentID.Equals(id)).SingleOrDefault();

                if (data != null)
                {
                    data.IsDeleted = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;

                    context.SaveChanges();

                    isSuccess = true;
                }
                else
                {
                    message = string.Format("Biz Segment data for Biz Segment ID {0} is does not exist.", id);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}