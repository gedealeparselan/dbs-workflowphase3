﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using System.Linq.Dynamic;

namespace DBS.WebAPI.Models
{

    #region Properties
    public class FindingEmailTimeModel
    {
        public int FindingEmailTimeID { get; set; }
        public TimeSpan EmailTime { get; set; }
        public string Status { get; set; }
       //public Status Status { get; set; }
        public bool isDelete { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime? LastModifiedDate { get; set; } 
    }



    public class FindingEmailTimeRow : FindingEmailTimeModel
    {
        public int RowID { get; set; }
    }

    public class FindingEmailTimeGrid : Grid
    {
        public IList<FindingEmailTimeRow> Rows { get; set; }
    }
    #endregion

    #region filter
    public class FindingEmailTimeModelFilter : Filter { }
    #endregion

    #region interface
    public interface IFindingEmailTime : IDisposable
    {
        bool GetFindingEmailTimeByID(int id,ref FindingEmailTimeModel Model, ref string Message);
        bool GetFindingEmailTime(ref IList<FindingEmailTimeModel> Model, ref string Message);
        bool GetFindingEmailTime(ref IList<FindingEmailTimeModel> Model, int limit, int index, ref string message);
        bool GetFindingEmailTime(int page, int size, IList<FindingEmailTimeModelFilter> filters, string sortColumn, string sortOrder, ref FindingEmailTimeGrid FindingEmailTimes, ref string message);
        bool GetFindingEmailTime(string key, int limit, ref IList<FindingEmailTimeModel> Model, ref string message);
        bool UpdateFindingEmailTime(int id, FindingEmailTimeModel Model, ref string message);
        bool AddFindingEmaliTime(FindingEmailTimeModel Model, ref string message);
        bool deleteFindingEmailTime(int id, ref string message);
    }
    #endregion

    #region Repository
    public class FindingEmailTimeRepository : IFindingEmailTime
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();
 
        public bool GetFindingEmailTime(ref IList<FindingEmailTimeModel> Model, ref string Message)
        {
            bool isSucces = false;
           
            try
            {
              
                var data = (from a in context.FindingEmailTimes
                            
                            select new FindingEmailTimeModel
                            {
                                EmailTime = a.EmailTime,
                                Status = a.Status.Equals(false) ? "Non Active" : "Active",
                                LastModifiedBy = a.CreatedBy,
                                LastModifiedDate = a.CreatedDate
                            }).SingleOrDefault();
                
                isSucces = true;
            }
            catch (Exception ex)
            {
                Message = ex.Message;
            }
            return isSucces;
        }

        public bool GetFindingEmailTime(ref IList<FindingEmailTimeModel> FindingEmailTimes, int limit, int index, ref string message)
        {
            bool isSucces = false;
            try
            {
                int skip = limit * index;
                using (DBSEntities context = new DBSEntities())
                {
                    FindingEmailTimes = (from a in context.FindingEmailTimes
                                         select new FindingEmailTimeModel
                                         {
                                             EmailTime = a.EmailTime,
                                             //Status = a.Status,
                                             Status = a.Status.Equals(false) ? "Non Active" : "Active", 
                                             LastModifiedBy = a.CreatedBy,
                                             LastModifiedDate = a.CreatedDate
                                         }).Skip(skip).Take(limit).ToList();
                }
                isSucces = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSucces;
        }

        public bool GetFindingEmailTime(int page, int size, IList<FindingEmailTimeModelFilter> filters, string sortColumn, string sortOrder, ref FindingEmailTimeGrid FindingEmailTimes, ref string message)
        {
            bool isSucces = false;
            try
            {
                int skip = (page - 1) * size;
                string orderBy = sortColumn + " " + sortOrder;
                DateTime maxDate = new DateTime();

                FindingEmailTimeModel filter = new FindingEmailTimeModel();
                if (filters != null)
                {
                    filter.EmailTime = TimeSpan.Parse(filters.Where(a => a.Field == "EmailTime").Select(a => a.Value).SingleOrDefault());
                    filter.Status =filters.Where(a => a.Field.Equals("Status")).Select(a => a.Value).SingleOrDefault();
                    filter.LastModifiedBy = filters.Where(a => a.Field.Equals("ModifiedBy")).Select(a => a.Value).SingleOrDefault();
                    if (filters.Where(a => a.Field.Equals("LastModifiedDate")).Any())
                    {
                        filter.LastModifiedDate = DateTime.Parse(filters.Where(a => a.Field.Equals("LastModifiedDate")).Select(a => a.Value).SingleOrDefault());
                        maxDate = filter.LastModifiedDate.Value.AddDays(1);
                    }
                }
                using (DBSEntities context = new DBSEntities())
                {
                    var data = (from a in context.FindingEmailTimes
                                select new FindingEmailTimeModel
                                {
                                    FindingEmailTimeID = a.FindingEmailTimeID,
                                    EmailTime = a.EmailTime,
                                    //Status = a.Status,
                                    Status = a.Status.Equals(false) ? "Non Active" : "Active",
                                    LastModifiedBy = a.CreatedBy,
                                    LastModifiedDate = a.CreatedDate
                                });

                    FindingEmailTimes.Page = page;
                    FindingEmailTimes.Size = size;
                    FindingEmailTimes.Total = data.Count();
                    FindingEmailTimes.Sort = new Sort { Column = sortColumn, Order = sortOrder };
                    FindingEmailTimes.Rows = data.OrderBy(orderBy).AsEnumerable().Select((a, i) => new FindingEmailTimeRow
                        {
                             RowID = i + 1,
                            FindingEmailTimeID = a.FindingEmailTimeID,
                            EmailTime = a.EmailTime,
                            Status = a.Status,
                            LastModifiedBy = a.LastModifiedBy,
                            LastModifiedDate = a.LastModifiedDate,
                        }).Skip(skip).Take(size).ToList();
                }
                isSucces = true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return isSucces;
        }

        public bool GetFindingEmailTimeByID(int id,ref FindingEmailTimeModel Model, ref string Message)
        {
            bool isSucces = false;
            try
            {
                Model = (from a in context.FindingEmailTimes
                        where a.FindingEmailTimeID.Equals(id)
                        select new FindingEmailTimeModel
                        {
                            EmailTime = a.EmailTime,
                            Status = a.Status.Equals(false) ? "Non Active" : "Active",
                            LastModifiedBy = a.CreatedBy,
                            LastModifiedDate = a.CreatedDate
                        }).SingleOrDefault();
                isSucces = true;
            }
            catch (Exception ex)
            {
                Message = ex.Message;
            }
            return isSucces;

        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        currentUser = null;
                        context.Dispose();
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public bool UpdateFindingEmailTime(int id, FindingEmailTimeModel Model, ref string message)
        {
            bool isSucces = false;
            try {
                Entity.FindingEmailTime data = context.FindingEmailTimes.Where(a => a.FindingEmailTimeID.Equals(id)).SingleOrDefault();
                if (data != null)
                {

                    data.EmailTime = Model.EmailTime;

                    data.Status =  Model.Status.Equals("1") ? true :false;
                    data.CreatedBy = currentUser.GetCurrentUser().DisplayName;
                    data.CreatedDate = DateTime.UtcNow;
                    context.SaveChanges();
                    isSucces = true;
                }
                else {
                    message = string.Format("Finding Email Time data for  EmailTime {0} is does not exist.", id);
                }
            }
            catch(Exception ex)
            {
                message = ex.Message;
            }
            return isSucces;
        }

        public bool GetFindingEmailTime(string key, int limit, ref IList<FindingEmailTimeModel> Model, ref string message)
        {
            throw new NotImplementedException();
        }

        public bool AddFindingEmaliTime(FindingEmailTimeModel Model, ref string message)
        {
            bool isSucces = false;
            try
            {
                context.FindingEmailTimes.Add(new Entity.FindingEmailTime()
                {
                    EmailTime = Model.EmailTime,
                    Status = Model.Status.Equals("1") ? true : false,
                    CreatedDate = DateTime.UtcNow,
                    CreatedBy = currentUser.GetCurrentUser().DisplayName
                });
                context.SaveChanges();
                isSucces = true;
            }
            catch (Exception ex) {
                message = ex.Message;
            }
            return isSucces;
        }

        public bool deleteFindingEmailTime(int id, ref string message)
        {
            bool isSucces = false;
            try {
                Entity.FindingEmailTime data = context.FindingEmailTimes.Where(a => a.FindingEmailTimeID.Equals(id)).SingleOrDefault();
                if (data != null)
                {
                    data.IsDelete = true;
                    data.UpdateDate = DateTime.UtcNow;
                    data.UpdateBy = currentUser.GetCurrentUser().DisplayName;
                    context.SaveChanges();
                    isSucces = true;
                }
                else {
                    message = string.Format("Finding Email Time data for FindingEmailTimeID {0} is does not exist.", id);
                }
            }
            catch(Exception ex){
                message = ex.Message;
            }
            return isSucces;
        }

        /*void IDisposable.Dispose()
        {
            throw new NotImplementedException();
        }*/
    }
    #endregion
}