
//#region variable global
var accessToken;
var $box;
var $remove = false;
var cifData = '0';
var customerNameData = '';
var DocumentModels = ko.observable({ FileName: '', DocumentPath: '' });
var DealModel = { cif: cifData, token: accessToken }
var idrrate = 0;
//#endregion

var DataTBOTableDynamicModel = function () {
    var self = this;
    self.TBOTMOID = ko.observable('');
    self.TBOTMOName = ko.observable('');
    self.ExpectedDateTBO = ko.observable('');
    self.RemaksTBO = ko.observable('');
    self.IsChecklistTBO = ko.observable(false);
}

//#region Constanta Variable
var CONST_TRANSACTION = {
    FX: "FX",
    TMO: "TMO"
}
var CONST_STATEMENT = {
    StatementA_ID: 1,
    StatementB_ID: 2,
    Dash_ID: 3,
    AnnualStatement_ID: 4
}
var CONST_MSG = {
    NotCompleteUpload: 'Please complete the upload form fields.',
    ActivityTitleNotFound: 'Something went Wrong, the activity title task not found.',
    ValidationFields: 'Mandatory field must be filled.',
    ValidationUnderlyingInstraction: 'Please Select Underlying or upload Instruction Document.',
    ValidationAnnualSL: 'Customer does not have annual statement.',
    ValidationEnter: 'Enter key is disabled for this form.',
    SuccessSave: 'Transaction data has been saved.',
    ValidationMinTotal: 'The Total Underlying Amount must be greater than Transaction Amount'
}
//#endregion

//#region Model
var SPRole = {
    ID: ko.observable(),
    Name: ko.observable()
};
var SPUser = {
    DisplayName: ko.observable(),
    Email: ko.observable(),
    ID: ko.observable(),
    LoginName: ko.observable(),
    Roles: ko.observableArray([SPRole])
};
var AmountModel = {
    TotalAmountsUSD: ko.observable(0),
    RemainingBalance: ko.observable(0),
    TotalUtilization: ko.observable(0)
}
var StatementModel = {
    ID: ko.observable(0),
    Name: ko.observable("")
};
var BranchModel = {
    ID: ko.observable(),
    Name: ko.observable()
};
var TypeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};
var RMModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    RankID: ko.observable(),
    Rank: ko.observable(),
    SegmentID: ko.observable(),
    Segment: ko.observable(),
    BranchID: ko.observable(),
    Branch: ko.observable(),
    Email: ko.observable()
};
var ContactModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    PhoneNumber: ko.observable(),
    DateOfBirth: ko.observable(),
    Address: ko.observable(),
    IDNumber: ko.observable()
};
var FunctionModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};
var CurrencyModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};
var AccountModel = {
    AccountNumber: ko.observable(),
    CustomerName: ko.observable(),
    Currency: CurrencyModel,
    IsJointAccount: ko.observable()
};
var UnderlyingDocumentModel = {
    ID: ko.observable(),
    Name: ko.observable()
};
var DocumentTypeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};
var DocumentPurposeModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};
var ProductTypeModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable(),
    IsFlowValas: ko.observable()
};
var FXComplianceModel = {
    ID: ko.observable(),
    Name: ko.observable(),
    Description: ko.observable()
};
var ChargesModel = {
    ID: ko.observable(),
    Code: ko.observable(),
    Description: ko.observable()
};
var DocumentTBOMOdel = {
    TBOIndex: ko.observable(''),
    TBOTMOID: ko.observable(''),
    TBOTMOName: ko.observable(''),
    ExpectedDateTBO: ko.observable(''),
    RemaksTBO: ko.observable(''),
    IsChecklistTBO: ko.observable(false)
}

var UnderlyingModel = {
    ID: ko.observable(),
    UnderlyingDocument: ko.observable(UnderlyingDocumentModel),
    DocumentType: ko.observable(DocumentTypeModel),
    Currency: ko.observable(CurrencyModel),
    Amount: ko.observable(),
    AttachmentNo: ko.observable(),
    StatementLetter: ko.observable(StatementLetterModel),
    IsStatementLetter: ko.observable(),
    IsDeclarationOfException: ko.observable(),
    StartDate: ko.observable(),
    EndDate: ko.observable(),
    DateOfUnderlying: ko.observable(),
    AvailableAmount: ko.observable(),
    UtilizeAmountDeal: ko.observable(),
    ExpiredDate: ko.observable(),
    ReferenceNumber: ko.observable(),
    SupplierName: ko.observable(),
    InvoiceNumber: ko.observable()
};
var DocumentModel = {
    ID: ko.observable(),
    Type: ko.observable(DocumentTypeModel),
    Purpose: ko.observable(DocumentPurposeModel),
    FileName: ko.observable(),
    DocumentPath: ko.observable(),
    LastModifiedDate: ko.observable(new Date),
    LastModifiedBy: ko.observable()
};
var TransactionGridModel = {
    TransactionDealID: ko.observable(),
    ApplicationID: ko.observable(),
    TZReference: ko.observable(),
    CustomerName: ko.observable(),
    Product: ko.observable(),
    CurrencyDesc: ko.observable(),
    Amount: ko.observable(),
    AmountUSD: ko.observable(),
    FxTransactionDesc: ko.observable(),
    TransactionStatus: ko.observable(),
    UpdateDate: ko.observable(),
    UpdateBy: ko.observable()
};
var CustomerModel = {
    CIF: ko.observable(),
    Name: ko.observable(),
    POAName: ko.observable(),
    Branch: ko.observable(BranchModel),
    Type: ko.observable(TypeModel),
    RM: ko.observable(RMModel),
    Contacts: ko.observableArray([ContactModel]),
    Functions: ko.observableArray([FunctionModel]),
    Accounts: ko.observableArray([AccountModel]),
    Underlyings: ko.observableArray([UnderlyingModel])
};
var TransactionDealModel = {
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    Customer: ko.observable(CustomerModel),
    ValueDate: ko.observable(),
    TradeDate: ko.observable(),
    Account: ko.observable(AccountModel),
    StatementLetter: ko.observable(StatementModel),
    TZRef: ko.observable(),
    Rate: ko.observable(0),
    Currency: ko.observable(CurrencyModel),
    Amount: ko.observable(0),
    AmountUSD: ko.observable(0),
    UnderlyingCurrency: ko.observable(CurrencyModel),
    UnderlyingAmount: ko.observable(0),
    UtilizedAmount: ko.observable(0),
    CreateDate: ko.observable(new Date)
};
var TransactionDealDetailModel = {
    TransactionDealID: ko.observable(),
    AccountNumber: ko.observable(),
    StatementLetter: ko.observable(StatementModel),
    ProductType: ProductTypeModel,
    WorkflowInstanceID: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    Customer: ko.observable(CustomerModel),
    Currency: CurrencyModel,
    Amount: ko.observable(),
    Rate: ko.observable(),
    AmountUSD: ko.observable(),
    Account: ko.observable(AccountModel),
    TradeDate: ko.observable(),
    ValueDate: ko.observable(),
    Type: ko.observable(20),
    CreateDate: ko.observable(new Date),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    Documents: ko.observableArray([]),
    DetailCompliance: ko.observable(),
    BeneName: ko.observable(),
    BeneAccNumber: ko.observable(),
    TZRef: ko.observable(),
    underlyings: ko.observableArray([]),
    TZReference: ko.observable(),
    IsBookDeal: ko.observable(false),
    bookunderlyingamount: ko.observable(),
    bookunderlyingcurrency: ko.observable(),
    bookunderlyingcode: ko.observable(),
    CreateBy: ko.observable(),
    AccountCurrencyCode: ko.observable(),
    IsFxTransaction: ko.observable(false),
    OtherUnderlying: ko.observable(),
    IsOtherAccountNumber: ko.observable(false),
    OtherAccountNumber: ko.observable(),
    DebitCurrency: CurrencyModel,
    IsJointAccount: ko.observable(false),
    TransactionStatus: ko.observable(),
    IsResident: ko.observable(),
    NB: ko.observable(),
    SwapType: ko.observable(),
    SwapTZDealNo: ko.observable(),
    SwapTransactionID: ko.observable(),
    IsNettingTransaction: ko.observable(false)
};
var TransactionTMOModel = {
    TransactionDealID: ko.observable(),
    WorkflowInstanceID: ko.observable(),
    IsDraft: ko.observable(),
    ID: ko.observable(),
    ApplicationID: ko.observable(),
    Customer: ko.observable(CustomerModel),
    StatementLetter: StatementModel,
    BuyCurrency: CurrencyModel,
    SellCurrency: CurrencyModel,
    BuyAmount: ko.observable(),
    SellAmount: ko.observable(),
    Rate: ko.observable(),
    AmountUSD: ko.observable(),
    TradeDate: ko.observable(),
    ValueDate: ko.observable(),
    Remarks: ko.observable(),
    TZReference: ko.observable(),
    SubmissionDateSL: ko.observable(),
    SubmissionDateInstruction: ko.observable(),
    SubmissionDateUnderlying: ko.observable(),
    IsUnderlyingCompleted: ko.observable(false),
    OtherUnderlying: ko.observable(),
    UnderlyingCodeID: ko.observable(),
    TransactionStatus: ko.observable(),
    Documents: ko.observableArray([]),
    DocumentTBO: ko.observableArray([]),
    Underlyings: ko.observableArray([]),
    AccountCurrencyCode: ko.observable(),
    AccountNumber: ko.observable(),
    ProductType: ProductTypeModel,
    IsOtherAccountNumber: ko.observable(false),
    OtherAccountNumber: ko.observable(),
    DebitCurrency: CurrencyModel,
    IsJointAccount: ko.observable(false),
    IsResident: ko.observable(),
    NB: ko.observable(),
    SwapType: ko.observable(),
    SwapTZDealNo: ko.observable(),
    SwapTransactionID: ko.observable(),
    IsNettingTransaction: ko.observable(false),
    CreateDate: ko.observable(),
    CreateBy: ko.observable(),
    LastModifiedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    IsResubmit: ko.observable()
};
var Parameter = {
    Currencies: ko.observableArray([CurrencyModel]),
    FXCompliances: ko.observableArray([FXComplianceModel]),
    DocumentTypes: ko.observableArray([DocumentTypeModel]),
    DocumentPurposes: ko.observableArray([DocumentPurposeModel]),
    ProductType: ko.observableArray([ProductTypeModel]),
    StatementLetter: ko.observableArray([StatementModel])
};
var SelectedModel = {
    Currency: ko.observable(),
    DebitCurrency: ko.observable(),
    Account: ko.observable(),
    DocumentType: ko.observable(),
    DocumentPurpose: ko.observable(),
    NewCustomer: ko.observable({
        Currency: ko.observable()
    }),
    ProductType: ko.observable(),
    StatementLetter: ko.observable(),
    SellCurrency: ko.observable(),
    BuyCurrency: ko.observable()
};
//#endregion 

//#region Function Model
var formatNumber = function (num) {
    if (num != null) {
        var n = num.toString(), p = n.indexOf('.');
        return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, function ($0, i) {
            return p < 0 || i < p ? ($0 + ',') : $0;
        });
    } else { return 0 }
}
var GridPropertiesModel = function (callback) {
    var self = this;

    self.SortColumn = ko.observable();
    self.SortOrder = ko.observable();
    self.AllowFilter = ko.observable(false);
    self.AvailableSizes = ko.observableArray(config.sizes);
    self.Page = ko.observable(1);
    self.Size = ko.observable(config.sizeDefault);
    self.Total = ko.observable(0);
    self.TotalPages = ko.observable(0);

    self.Callback = function () {
        callback();
    };

    self.OnPageSizeChange = function () {
        self.Page(1);

        self.Callback();
    };

    self.OnPageChange = function () {
        if (self.Page() < 1) {
            self.Page(1);
        } else {
            if (self.Page() > self.TotalPages())
                self.Page(self.TotalPages());
        }

        self.Callback();
    };

    self.NextPage = function () {
        var page = self.Page();
        if (page < self.TotalPages()) {
            self.Page(page + 1);

            self.Callback();
        }
    };

    self.PreviousPage = function () {
        var page = self.Page();
        if (page > 1) {
            self.Page(page - 1);

            self.Callback();
        }
    };

    self.FirstPage = function () {
        self.Page(1);

        self.Callback();
    };

    self.LastPage = function () {
        self.Page(self.TotalPages());

        self.Callback();
    };

    self.Filter = function (data) {
        self.Page(1);

        self.Callback();
    };

    // get sorted column
    self.GetSortedColumn = function (columnName) {
        var sort = "sorting";

        if (self.SortColumn() == columnName) {
            if (self.SortOrder() == "DESC")
                sort = sort + "_asc";
            else
                sort = sort + "_desc";
        }

        return sort;
    };

    // Sorting data
    self.Sorting = function (column) {
        if (self.SortColumn() == column) {
            if (self.SortOrder() == "ASC")
                self.SortOrder("DESC");
            else
                self.SortOrder("ASC");
        } else {
            self.SortOrder("ASC");
        }

        self.SortColumn(column);

        self.Page(1);

        self.Callback();
    };
};
var SelectModel = function (cif, id) {
    var self = this;
    self.ID = ko.observable(0);
    self.CIF = ko.observable(cif);
    self.UnderlyingID = ko.observable(id);
    self.ParentID = ko.observable(0);
}
var SelectBulkModel = function (cif, id) {
    var self = this;
    self.ID = ko.observable(0);
    self.CIF = ko.observable(cif);
    self.UnderlyingID = ko.observable(id);
    self.ParentID = ko.observable(0);
}
var UnderlyingDocumentModel2 = function (id, name, code) {
    var self = this;
    self.ID = ko.observable(id);
    self.Code = ko.observable(code);
    self.Name = ko.observable(name);
    self.CodeName = ko.observable(code + ' (' + name + ')');
}
var DocumentTypeModel2 = function (id, name) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);

}
var StatementLetterModel = function (id, name) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
}
var CurrencyModel2 = function (id, code, description) {
    var self = this;
    self.ID = ko.observable(id);
    self.Code = ko.observable(code);
    self.Description = ko.observable(description);
}
var RateTypeModel2 = function (id, code, description) {
    var self = this;
    self.ID = ko.observable(id);
    self.Code = ko.observable(code);
    self.Description = ko.observable(description);
}
var DocumentPurposeModel2 = function (id, name, description) {
    var self = this;
    self.ID = ko.observable(id);
    self.Name = ko.observable(name);
    self.Description = ko.observable(description);
}
var CustomerUnderlyingMappingModel = function (FileID, UnderlyingID, attachNo) {
    var self = this;
    self.ID = ko.observable(0);
    self.UnderlyingID = ko.observable(UnderlyingID);
    self.UnderlyingFileID = ko.observable(FileID);
    self.AttachmentNo = ko.observable(attachNo);
}
var SelectUtillizeModel = function (id, enable, uSDAmount, statementLetter) {
    var self = this;
    self.ID = ko.observable(id);
    self.Enable = ko.observable(enable);
    self.USDAmount = ko.observable(uSDAmount);
    self.StatementLetter = ko.observable(statementLetter);
}

var SelectUtillizeTMOModel = function (id, enable, uSDAmount, utilizeamountdeal, statementLetter) {
    var self = this;
    self.ID = ko.observable(id);
    self.Enable = ko.observable(enable);
    self.USDAmount = ko.observable(uSDAmount);
    self.UtilizeAmountDeal = ko.observable(utilizeamountdeal);
    self.StatementLetter = ko.observable(statementLetter);
}

var DocumentTypeTBO = {
    ID: ko.observable(),
    Name: ko.observable()
}

var Const_TBOSLADoc = {
    MT103: 4,
    Other: 10
}

var ParameterTBO = {
    DocumentTypes: ko.observableArray([{ id: Const_TBOSLADoc.MT103, name: 'MT103' }, { id: Const_TBOSLADoc.Other, name: 'Other' }])
};

var SelectedModelTBO = {
    DocumentType: ko.observable()
}

var TBOTransactionModel = {
    TransactionID: ko.observable(),
    ApplicationID: ko.observable(),
    CIF: ko.observable(),
    CustomerName: ko.observable(),
    ID: ko.observable(),
    TBOSLAID: ko.observable(),
    ActDate: ko.observable(),
    ExptDate: ko.observable(),
    Status: ko.observable(),
    TypeofDoc: ko.observable(DocumentTypeModel),
    PurposeofDoc: ko.observable(DocumentPurposeModel),
    FileName: ko.observable(DocumentModel),
    CreatedBy: ko.observable(),
    CreatedDate: ko.observable(),
    LastModifiedBy: ko.observable(),
    LastModifiedDate: ko.observable(),
    TBOApplicationID: ko.observable()
};


//#endregion

var ViewModel = function () {
    //Make the self as 'this' reference

    var self = this;
    var underlyingCodeSave = '';
    self.IsRole = function (name) {
        var result = false;
        $.each($.cookie(api.cookie.spUser).Roles, function (index, item) {
            if (Const_RoleName[item.ID] == name) result = true; //if (item.Name == name) result = true;
        });
        return result;
    };

    //TBO Tmo
    self.ToBeObtained = ko.observable(false);
    self.IsToBeObtained = ko.observable(false);
    self.TBOTMOTransaction = ko.observableArray([DocumentTBOMOdel]);
    self.ParameterTBO = ko.observable(ParameterTBO);
    self.SelectedTBO = ko.observable(SelectedModelTBO);
    self.ActSubmissionDateTMO = ko.observable();
    self.bookTBOTMO = ko.observable();
    self.IsChecklistSubmitTBO = ko.observable();
    self.DocumentTypesTBO = ko.observable(DocumentTypeTBO);
    self.dynamicTableTBO = ko.observableArray([]);
    var tempDyn = ko.observable(new DataTBOTableDynamicModel());
    self.dynamicTableTBO.push(tempDyn());
    self.TBONumber = ko.observable(0);
    self.IsSubmitValidationTBO = ko.observable(false);
    self.TBOData = ko.observableArray([TBOTransactionModel]);

    // input form controls
    self.IsEditable = ko.observable(true);
    self.IsEditTableUnderlying = ko.observable(true);
    // Sharepoint User
    self.SPUser = ko.observable(SPUser);
    self.ActivityTitle = ko.observable();
    self.IsShowUnderlyingDocuments = ko.observable(false);
    self.IsShowSubmit = ko.observable(true);
    self.IsShowSubmitCorrection = ko.observable(false);
    self.AccountNumber = ko.observable();
    //Treshold value
    self.FCYIDRTresHold = ko.observable(0);
    // Is check attachments
    self.IsFXTransactionAttach = ko.observable(false);
    self.IsUploading = ko.observable(false);
    self.IsUploaded = ko.observable(false);
    self.SelectingUnderlying = ko.observable(false);
    self.SelectingInstruction = ko.observable(true);
    // ddl Joint Accounts
    self.JointAccounts = ko.observableArray([{ ID: false, Name: "Single" }, { ID: true, Name: "Join" }]);
    self.IsJointAccount = ko.observable(false);
    self.IsHitThreshold = ko.observable(false);
    self.ThresholdType = ko.observable();
    self.ThresholdValue = ko.observable(0);
    self.IsNoThresholdValue = ko.observable(false)
    self.IsAnnualStatement = ko.observable(false);
    self.IsSwapTransaction = ko.observable(false);
    self.IsNettingTransaction = ko.observable(false);
    self.MessageTransactionType = ko.observable(false);
    self.Swap_NettingDFNumber = ko.observable();
    self.MessageNettingTransactionType = ko.observable(false);
    self.NettingApplicationID = ko.observable();
    self.TouchTimeStartDate = ko.observable(new Date().toUTCString());
    //bu group val
    self.isDealBUGroup = ko.observable(false);
    // counter upload file ppu maker
    self.counterUpload = ko.observable(0);
    // set Permission button book deal
    self.IsPermissionBookDeal = ko.observable(false);
    self.IsNewTransaction = ko.observable(false);
    self.TransactionID = ko.observable();
    self.IsUploading = ko.observable(false);
    //is fx transaction
    self.t_IsFxTransaction = ko.observable(true);
    self.t_IsFxTransactionToIDR = ko.observable(false);

    //underlying declare
    self.AmountModel = ko.observable(AmountModel);
    self.IsBookDeal = ko.observable(false);
    self.isEditBook = ko.observable(false);
    self.MakerUnderlyings = ko.observableArray([]);
    self.MakerAllDocumentsTemp = ko.observableArray([]);
    self.MakerDocuments = ko.observableArray([]);
    self.isNewUnderlying = ko.observable(false);
    self.IsSelectedRow = ko.observable(false);
    // Parameters
    self.Parameter = ko.observable(Parameter);
    // grid model
    self.TransactionGridModel = ko.observable(TransactionGridModel);
    // Main Model
    self.TransactionDealDetailModel = ko.observable(TransactionDealDetailModel);
    self.TransactionTMO = ko.observable(TransactionTMOModel);
    // Main Model
    self.TransactionDealModel = ko.observable(TransactionDealModel);
    // Double Transaction
    self.DoubleTransactions = ko.observableArray([]);
    // Validation User
    self.UserValidation = ko.observable({
        UserID: ko.observable(),
        Password: ko.observable(),
        Error: ko.observable()
    });
    // set approval data template
    self.ApprovalData = function () {
        var output;
        switch (self.ActivityTitle()) {
            case CONST_TRANSACTION.FX:
                output = self.TransactionDealDetailModel();
                break;
            case CONST_TRANSACTION.TMO:
                output = self.TransactionTMO();
                break;
        }
        return output;
    };
    // set approval ui template
    self.ApprovalTemplate = function () {
        var output;
        switch (self.ActivityTitle()) {
            case CONST_TRANSACTION.FX:
                output = "NewTransactionFXTracking";
                break;
            case CONST_TRANSACTION.TMO:
                output = "NewTransactionTMOTracking";
                break;
        }
        return output;
    };
    self.IsTMODoneStatementLetter = ko.observable(false);
    self.IsTMODoneDocument = ko.observable(false);
    self.IsTMODocumentCompleted = ko.observable(false);
    // Options selected value
    self.Selected = ko.observable(SelectedModel);
    // Temporary documents
    self.Documents = ko.observableArray([]);
    self.DocumentPath = ko.observable();
    self.DocumentType = ko.observable();
    self.DocumentPurpose = ko.observable();
    self.DocumentFileName = ko.observable();
    self.IsEmptyAccountNumber = ko.observable(false);
    // temp amount_u for bulk underlying // add 2015.03.09
    self.tempAmountBulk = ko.observable(0);
    //// start Checked Calculate for Attach Grid and Underlying Grid
    self.TempSelectedAttachUnderlying = ko.observableArray([]);
    //Tambah Agung
    self.TempSelectedInstruction = ko.observableArray([]);
    //End
    //// start Checked Calculate for Underlying Proforma Grid
    self.TempSelectedUnderlyingProforma = ko.observableArray([]);
    //// start Checked Calculate for Underlying Bulk Grid // add 2015.03.09
    self.TempSelectedUnderlyingBulk = ko.observableArray([]);
    self.AllDataCustomerUnderlyingBulk = ko.observableArray([]);
    //// start Checked Calculate for Underlying Grid
    self.TempSelectedUnderlying = ko.observableArray([]);
    self.DynamicAccounts = ko.observableArray([]);
    self.JointAccountNumbers = ko.observable([]);
    self.DynamicCurrencies = ko.observableArray([]);
    // New Data flag
    self.IsDeal = ko.observable(false);

    // New Data flag
    self.IsNewDataUnderlying = ko.observable(false);

    //Tambah Agung
    self.InstructionDocuments = ko.observableArray([]);
    //End

    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerUnderlyings = ko.observableArray([]);

    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerUnderlyingProformas = ko.observableArray([]);

    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerUnderlyingFiles = ko.observableArray([]);

    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerAttachUnderlyings = ko.observableArray([]);

    // Declare an ObservableArray for Storing the JSON Response
    self.CustomerBulkUnderlyings = ko.observableArray([]); // add 2015.03.09

    // grid properties Underlying
    self.UnderlyingGridProperties = ko.observable();
    self.UnderlyingGridProperties(new GridPropertiesModel(GetDataUnderlying));

    // grid properties Attach Grid
    self.AttachGridProperties = ko.observable();
    self.AttachGridProperties(new GridPropertiesModel(GetDataAttachFile));

    // grid properties Underlying File
    self.UnderlyingAttachGridProperties = ko.observable();
    self.UnderlyingAttachGridProperties(new GridPropertiesModel(GetDataUnderlyingAttach));

    //tambah Agung
    self.InstructionGridProperties = ko.observable();
    self.InstructionGridProperties(new GridPropertiesModel(GetDataInstructionDocument));
    //End
    // grid properties Proforma
    self.UnderlyingProformaGridProperties = ko.observable();
    self.UnderlyingProformaGridProperties(new GridPropertiesModel(GetDataUnderlyingProforma));

    // grid properties Bulk // 2015.03.09
    self.UnderlyingBulkGridProperties = ko.observable();
    self.UnderlyingBulkGridProperties(new GridPropertiesModel(GetDataBulkUnderlying));

    // set default sorting for Underlying Grid
    self.UnderlyingGridProperties().SortColumn("ID");
    self.UnderlyingGridProperties().SortOrder("ASC");

    // set default sorting for Underlying File Grid
    self.AttachGridProperties().SortColumn("FileName");
    self.AttachGridProperties().SortOrder("ASC");
    //Tambah Agung
    self.InstructionGridProperties().SortColumn("ID");
    self.InstructionGridProperties().SortOrder("ASC");
    //End
    // set default sorting for Attach Underlying Grid
    self.UnderlyingAttachGridProperties().SortColumn("ID");
    self.UnderlyingAttachGridProperties().SortOrder("DESC");
    // set default sorting for Proforma grid
    self.UnderlyingProformaGridProperties().SortColumn("IsSelectedProforma");
    self.UnderlyingProformaGridProperties().SortOrder("DESC");
    // set default sorting for Bulk grid //add 2015.03.09
    self.UnderlyingBulkGridProperties().SortColumn("IsSelectedBulk");
    self.UnderlyingBulkGridProperties().SortOrder("DESC");
    // Local Date
    self.LocalDate = function (date, isDateOnly, isDateLong) {
        if (ko.isObservable(date)) {
            date = date();
        }
        /*
        var localDate = new Date(date);
        if (date == '1970/01/01 00:00:00' || date == null) {
            return "";
        }
        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return ""
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-18
    };
    /* self.SetRequired = ko.computed(function () {
         if (!self.TransactionTMO().IsUnderlyingCompleted()) {
             $('#ActualSubmissionDateSL').data({ ruleRequired: true });
         } else {
             $('#ActualSubmissionDateSL').data({ ruleRequired: false });
             $('.form-group').removeClass('has-error').addClass('has-info');
             $('.help-block').remove();
         } 
     });*/
    // Uploading document
    self.UploadDocumentUnderlying = function () {
        self.IsUploading(false);
        // show the dialog task form
        $("#modal-form-Attach").modal('show');
        $('#backDrop').show();
        self.ID_a(0);
        self.DocumentPurpose_a(new DocumentPurposeModel2('', '', ''));
        self.DocumentType_a(new DocumentTypeModel2('', ''));
        self.DocumentPath_a('');
        self.TempSelectedAttachUnderlying([]);
        GetDataUnderlyingAttach();
        $('.remove').click();
    }
    self.OnChangeStatement = function (obj1, event) {

        if (obj1.StatementLetter.ID() != CONST_STATEMENT.AnnualStatement_ID) {
            self.IsAnnualStatement(false);
            self.IsShowUnderlyingDocuments(true);
        } else {
            self.IsAnnualStatement(true);
            self.IsShowUnderlyingDocuments(false);
        }
        UnCheckDataUnderlying(obj1);
        GetTotalTransaction();
    }
    self.OnChangeResident = function (obj1, event) {
        //if (self.TransactionDealDetailModel().IsResident() != null) {
        GetTotalTransaction();
        //}
    }

    self.UploadDocument = function () {
        $("#modal-form-upload").modal('show');
        self.IsUploading(false);
        self.DocumentPath(null);
        self.Selected().DocumentType(null);
        self.Selected().DocumentPurpose(null);
        self.DocumentType(null);
        self.DocumentPurpose(null);
        $('#backDrop').show();
    }

    self.SelectInstruction = function () {
        self.DocumentPurpose_a(new DocumentPurposeModel2('', '', ''));
        GetDataInstructionDocument();
        $("#modal-form-instruction").modal('show');
        $('#backDrop').show();
    }
    // Add new document handler
    self.AddDocument = function () {
        var doc = {
            ID: 0,
            Type: self.DocumentType(),
            Purpose: self.DocumentPurpose(),
            FileName: viewModel.DocumentPath() != null ? viewModel.DocumentPath().name.split('.')[viewModel.DocumentPath().name.split('.').length - 2].replace(/[<>:"\/\\|?!@#$%^&*]+/g, '_') + "." + viewModel.DocumentPath().name.split('.')[viewModel.DocumentPath().name.split('.').length - 1] : '',//aridya 20170131 untuk handle special char //self.DocumentFileName(),
            DocumentPath: self.DocumentPath(),
            IsNewDocument: true,
            LastModifiedDate: new Date(),
            LastModifiedBy: null
        };

        //alert(JSON.stringify(self.DocumentPath()))
        if (doc.Type == null || doc.Purpose == null || doc.DocumentPath == null || doc.DocumentPath == "") {
            ShowNotification("Warning", CONST_MSG.NotCompleteUpload, 'gritter-warning', true);
        } else {
            /*switch (viewModel.ActivityTitle()) {
                case CONST_TRANSACTION.FX:
                    viewModel.TransactionDealDetailModel().Documents.push(doc);
                    break;
                case CONST_TRANSACTION.TMO:
                    viewModel.TransactionTMO().Documents.push(doc);
                    break;
            }*/
            self.MakerDocuments.push(doc);
            $('.remove').click();
            // hide upload dialog
            $("#modal-form-upload").modal('hide');
            $('#backDrop').hide();
        }
    };
    self.CloseAttach = function () {
        $('#backDrop').hide();
    }
    self.EditDocument = function (data) {
        self.DocumentPath(data.DocumentPath);
        self.Selected().DocumentType(data.Type.ID);
        self.Selected().DocumentPurpose(data.Purpose.ID);

        // show upload dialog
        $("#modal-form-upload").modal('show');
        $('#backDrop').show();
    };
    self.CheckEmptyAccountNumber = function (item) {
        SetChangeAccountNumber(item);
    };
    self.RemoveDocument = function (data) {
        //alert(JSON.stringify(data))
        self.Documents.remove(data);
    };
    self.RemoveDocumentData = function (data) {
        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (result) {
                if (data.Purpose.ID == 2) {
                    // delete underlying document
                    DeleteUnderlyingAttachment(data);

                    switch (self.ActivityTitle()) {
                        case CONST_TRANSACTION.FX:
                            var item = ko.utils.arrayFilter(viewModel.TransactionDealDetailModel().Documents(), function (dta) { return dta.DocumentPath != data.DocumentPath; });
                            if (item != null) {
                                viewModel.TransactionDealDetailModel().Documents(item);
                            }
                            break;
                        case CONST_TRANSACTION.TMO:
                            var item = ko.utils.arrayFilter(viewModel.TransactionTMO().Documents(), function (dta) { return dta.DocumentPath != data.DocumentPath; });
                            if (item != null) {
                                viewModel.TransactionTMO().Documents(item);
                            }
                            break;
                    }
                } else {
                    if (data.IsNewDocument == false) {
                        switch (self.ActivityTitle()) {

                            case CONST_TRANSACTION.FX:
                                var item = ko.utils.arrayFilter(viewModel.TransactionDealDetailModel().Documents(), function (dta) { return dta.ID != data.ID; });
                                if (item != null) {
                                    viewModel.TransactionDealDetailModel().Documents(item);
                                }
                                break;
                            case CONST_TRANSACTION.TMO:
                                var item = ko.utils.arrayFilter(viewModel.TransactionTMO().Documents(), function (dta) { return dta.ID != data.ID; });
                                if (item != null) {
                                    viewModel.TransactionTMO().Documents(item);
                                }
                                break;
                        }
                    }
                }
                self.MakerAllDocumentsTemp.remove(data);
                self.MakerDocuments.remove(data);

            }
        });

    }
    // Save and draft event
    self.Submit = function () {
        switch (self.ActivityTitle()) {
            case CONST_TRANSACTION.FX:
                self.SubmitFX();
                break;
            case CONST_TRANSACTION.TMO:
                self.SubmitTMO();
                break;
        }
    };
    //Tambah Agung
    self.SubmitCorrection = function () {
        // upload hanya document selain underlying
        if (self.TransactionTMO().ApplicationID() == null) {
            var tzRef = self.TransactionTMO().TZReference() == null ? $.now : self.TransactionTMO().TZReference();
            var newApplicationID = 'FX-' + tzRef;
            self.TransactionTMO().ApplicationID(newApplicationID);
        }
        var data = {
            ApplicationID: viewModel.TransactionTMO().ApplicationID(),
            CIF: viewModel.TransactionTMO().Customer.CIF(),
            Name: viewModel.TransactionTMO().Customer.Name()
        };

        var items = ko.utils.arrayFilter(viewModel.MakerDocuments(), function (item) {
            return item.Purpose.ID != 2 && item.IsNewDocument == true;
        });

        if (items != null && items.length > 0) {
            viewModel.counterUpload(0);
            for (var i = 0; items.length > i; i++) {
                UploadFile(data, items[i], UpdateCorrection, items.length);
            }
        } else {
            UpdateCorrection();
        }
    };
    //END

    self.DependantObervableTBO = function (index) {
        viewModel.DocumentTypesTBO(null);
        var a = viewModel.dynamicTableTBO()[index].TBOTMOID();
        if (a != null) {
            for (i = 0; i <= viewModel.ParameterTBO().DocumentTypes().length - 1; i++) {
                if (viewModel.ParameterTBO().DocumentTypes()[i].id == a) {
                    var data = {
                        ID: viewModel.ParameterTBO().DocumentTypes()[i].id,
                        Name: viewModel.ParameterTBO().DocumentTypes()[i].name
                    }
                    viewModel.DocumentTypesTBO(data);
                    if (viewModel.TransactionTMO().DocumentTBO().length > 0) {
                        if (viewModel.IsSubmitValidationTBO() == true) {
                            if (viewModel.DocumentTypesTBO().ID == Const_TBOSLADoc.MT103) {
                                ShowNotification("Form Validation Warning", "Can not choose document MT103, Please choose Other to complete Document", 'gritter-warning', true);
                                return false;
                            }
                        }
                    }
                }
            }
        } else {
            viewModel.DocumentTypesTBO(null);
            if (viewModel.TransactionTMO().DocumentTBO().length > 0) {
                if (viewModel.IsSubmitValidationTBO() == true) {
                    if (viewModel.DocumentTypesTBO() == null) {
                        ShowNotification("Form Validation Warning", "Please choose Other to complete Document", 'gritter-warning', true);
                        return false;
                    }
                }
            }
        }
    }

    self.AddNewTBO = function () {
        if (viewModel.ToBeObtained() == true) {
            viewModel.IsSubmitValidationTBO(false);
            BindingDataTBO(viewModel.dynamicTableTBO().length);
            var b = ko.observable(new DataTBOTableDynamicModel());
            viewModel.dynamicTableTBO.push(b());
            viewModel.dynamicTableTBO()[viewModel.dynamicTableTBO().length - 1].ExpectedDateTBO(viewModel.LocalDate(new Date().toUTCString(), true));
        } else {
            viewModel.IsToBeObtained(false);
        }
    }

    self.BindingDataTBOTMO = function (index) {
        if (viewModel.ToBeObtained() == true) {
            BindingDataTBO(index);
        } else {
            viewModel.IsToBeObtained(false);
        }
    }

    self.formatCheckerDateApplicationTBOTMO = function (index) {
        var ActualSubmissionDate = $('#application-date-actsubmissiondatetmo');
        viewModel.ActSubmissionDateTMO(moment(ActualSubmissionDate.value).format('YYYY-MM-DD'));
    }

    self.SubmitFX = function () {
        // validation
        self.IsDeal(false);

        // validation
        var form = $("#aspnetForm");
        form.validate();

        if (!IsValidUnderlying()) {
            return false;
        } else {

            if (form.valid()) {
                //bind data TBO
                if (viewModel.ToBeObtained()) {
                    if (viewModel.dynamicTableTBO().length > 0) {
                        viewModel.TransactionTMO().DocumentTBO(viewModel.dynamicTableTBO());
                        if (viewModel.TBOData().length > 0) {
                            for (var i = 0; viewModel.TBOData().length > i; i++) {
                                if (viewModel.TBOData()[i].Status == null || viewModel.TBOData()[i].Status == "Add") {
                                    ShowNotification("Task Validation Warning", String.format("This Transaction Submitted to TBO Home"), "gritter-success", true);
                                    return;
                                }
                            }
                        }
                    }
                }
                self.IsEditable(false);
                self.TransactionDealDetailModel().CreateDate(viewModel.TouchTimeStartDate());
                if (viewModel.TransactionDealDetailModel().ApplicationID() == null) {
                    var tzRef = viewModel.TransactionDealDetailModel().TZReference() == null ? $.now : viewModel.TransactionDealDetailModel().TZReference();
                    var newApplicationID = 'FX-' + tzRef;
                    viewModel.TransactionDealDetailModel().ApplicationID(newApplicationID);
                }

                if (viewModel.TransactionDealDetailModel().StatementLetter.ID() == CONST_STATEMENT.AnnualStatement_ID) {
                    GetAnnualStatement(cifData);
                } else {
                    var isSubmit = true;
                    GetThreshold(isSubmit);
                }
            }
            else {
                ShowNotification("Form Validation Warning", CONST_MSG.ValidationFields, 'gritter-warning', false);
            }
        }
    }
    self.SubmitTMO = function () {
        var isDraft_ = event.currentTarget.id == 'btnDraft';
        var form = $("#aspnetForm");
        form.validate();
        if (!IsValidUnderlying()) {
            return false;
        }
        else {
            if (form.valid() || isDraft_) {
                self.IsEditable(false);
                self.TransactionTMO().CreateDate(viewModel.TouchTimeStartDate());
                if (viewModel.ToBeObtained()) {
                    if (viewModel.dynamicTableTBO().length > 0) {
                        viewModel.TransactionTMO().DocumentTBO(viewModel.dynamicTableTBO());
                        if (viewModel.TBOData().length > 0) {
                            for (var i = 0; viewModel.TBOData().length > i; i++) {
                                if (viewModel.TBOData()[i].Status == null || viewModel.TBOData()[i].Status == "Add") {
                                    ShowNotification("Task Validation Warning", String.format("This Transaction Submitted to , Please Complete Mandatory TBO Document"), "gritter-warning", true);
                                    return;
                                }
                            }
                        }
                    }
                }
                if (self.TransactionTMO().ApplicationID() == null) {
                    var tzRef = self.TransactionTMO().TZReference() == null ? $.now : self.TransactionTMO().TZReference();
                    var newApplicationID = 'FX-' + tzRef;
                    self.TransactionTMO().ApplicationID(newApplicationID);
                }
                if (event.currentTarget.id == 'btnDraft' || event.currentTarget.id == 'bookDeal') {
                    self.TransactionTMO().IsDraft(true);
                } else {
                    self.TransactionTMO().IsDraft(false);
                }

                if (self.TransactionTMO().StatementLetter.ID() == CONST_STATEMENT.AnnualStatement_ID) {
                    GetAnnualStatement(self.TransactionTMO().Customer.CIF());
                } else {
                    GetThreshold(true);
                }
            }
            else {
                ShowNotification("Form Validation Warning", CONST_MSG.ValidationFields, 'gritter-warning', false);
            }
        }
    }
    // User Validation process
    self.ValidationProcess = function () {
        UploadDocuments(); // start upload docs and save the transaction
    };
    // Continue editing
    self.ContinueEditing = function () {
        // enable form input controls
        self.IsEditable(true);
    };
    //--------------------------- set Underlying function & variable start
    self.IsViewTransaction = ko.observable(false);
    self.IsViewTransactionCorrection = ko.observable(true);
    self.IsUnderlyingMode = ko.observable(true);
    self.ID_u = ko.observable("");
    self.CIF_u = ko.observable(cifData);
    self.CustomerName_u = ko.observable(customerNameData);
    self.UnderlyingDocument_u = ko.observable(new UnderlyingDocumentModel2('', '', ''));
    self.ddlUnderlyingDocument_u = ko.observableArray([]);
    self.DocumentType_u = ko.observable(new DocumentTypeModel2('', ''));
    self.ddlDocumentType_u = ko.observableArray([]);
    self.AmountUSD_u = ko.observable(0.00);
    self.Currency_u = ko.observable(new CurrencyModel2('', '', ''));
    self.Rate_u = ko.observable(0);
    self.ddlCurrency_u = ko.observableArray([]);
    self.StatementLetter_u = ko.observable(new StatementLetterModel('', ''));
    self.ddlStatementLetter = ko.observableArray([]);
    self.ddlStatementLetter_u = ko.observableArray([]);
    self.Amount_u = ko.observable(0);
    self.AttachmentNo_u = ko.observable("");
    self.SelectUnderlyingDocument_u = ko.observable("");
    self.IsDeclarationOfException_u = ko.observable(false);
    self.StartDate_u = ko.observable('');
    self.EndDate_u = ko.observable('');
    self.DateOfUnderlying_u = ko.observable("");
    self.ExpiredDate_u = ko.observable("");
    self.ReferenceNumber_u = ko.observable("");
    self.SupplierName_u = ko.observable("");
    self.InvoiceNumber_u = ko.observable("");
    self.IsStatementA = ko.observable(true);
    self.IsProforma_u = ko.observable(false);
    self.IsUtilize_u = ko.observable(false);
    self.IsSelectedProforma = ko.observable(false);
    self.IsJointAccount_u = ko.observable(false);
    self.AccountNumber_u = ko.observable("");
    // add bulkUnderlying // add 2015.03.09
    self.IsBulkUnderlying_u = ko.observable(false);
    self.IsSelectedBulk = ko.observable(false);
    /*self.Amounttmp_u = ko.computed({
         read: function () {
             if (document.getElementById("Amount_u") != null) {
                 var fromFormat = document.getElementById("Amount_u").value;
                 fromFormat = fromFormat.toString().replace(/,/g, '');
                 if (self.StatementLetter_u().ID() == CONST_STATEMENT.StatementA_ID) {
                     self.Currency_u().ID(1);
                     self.Amount_u(formatNumber(DataModel.ThresholdBuy));
                     var date = new Date();
                     var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                     var day = lastDay.getDate();
                     var month = lastDay.getMonth() + 1;
                     var year = lastDay.getFullYear();
                     var fixLastDay = year + "/" + month + "/" + day;
                     self.ExpiredDate_u(self.LocalDate(fixLastDay, true, false));
                     SetDefaultValueStatementA();
                 } else if (self.StatementLetter_u().ID() == CONST_STATEMENT.StatementB_ID && self.DateOfUnderlying_u() != "" && self.IsNewDataUnderlying()) {
                     self.ClearUnderlyingData();
                     if (self.LocalDate(self.DateOfUnderlying_u()) != "") {
                         var date = new Date(self.DateOfUnderlying_u());
                         var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                         var day = date.getDate();
                         var month = lastDay.getMonth() + 13;
                         var year = lastDay.getFullYear();
                         if (month > 12) {
                             month -= 12;
                             year += 1;
                         }
                         var fixLastDay = year + "/" + month + "/" + day;
                         if (!moment(fixLastDay, 'YYYY/MM/DD').isValid()) {
                             var LastDay_ = new Date(year, month, 0);
                             day = LastDay_.getDate();
                         }
                         self.ExpiredDate_u(self.LocalDate(fixLastDay, true, false));
                     }
                 }
 
                 var e = document.getElementById("Amount_u").value;
                 e = e.toString().replace(/,/g, '');
 
                 var res = parseFloat(e) * parseFloat(self.Rate_u()) / parseFloat(idrrate);
                 res = Math.round(res * 100) / 100;
                 res = isNaN(res) ? 0 : res; //avoid NaN
                 self.AmountUSD_u(parseFloat(res).toFixed(2));
                 self.Amount_u(formatNumber(e));
             }
         },
         write: function (data) {
             var fromFormat = document.getElementById("Amount_u").value;
             fromFormat = fromFormat.toString().replace(/,/g, '');
 
             var res = parseInt(fromFormat) * parseFloat(self.Rate_u()) / parseFloat(idrrate);
             res = Math.round(res * 100) / 100;
             res = isNaN(res) ? 0 : res; //avoid NaN
             self.AmountUSD_u(parseFloat(res).toFixed(2));
             self.Amount_u(formatNumber(fromFormat));
 
         }
     }, this); */
    self.CaluclateRate_u = ko.computed({
        read: function () {
            var CurrencyID = self.Currency_u().ID();
            var AccountID = self.Selected().Account;
            if (CurrencyID != '' && CurrencyID != undefined) {
                GetRateIDRAmount(CurrencyID);
            } else {
                self.Rate_u(0);
            }
            if (AccountID == '' && AccountID != undefined) {
                // alert('test');
            }
        },
        write: function () { }
    });
    self.Proformas = ko.observableArray([new SelectModel('', '')]);
    self.BulkUnderlyings = ko.observableArray([new SelectBulkModel('', '')]); // add 2015.03.09
    self.CodeUnderlying = ko.computed({
        read: function () {
            if (self.UnderlyingDocument_u().ID() != undefined && self.UnderlyingDocument_u().ID() != '') {
                var item = ko.utils.arrayFirst(self.ddlUnderlyingDocument_u(), function (x) {
                    return self.UnderlyingDocument_u().ID() == x.ID();
                }
                )
                if (item != null) {
                    return item.Code();
                }
                else {
                    return ''
                }
            } else { return '' }
        },
        write: function (value) {
            return value;
        }
    });
    self.OtherUnderlying_u = ko.observable('');
    self.IsTMO_u = ko.observable(false);
    self.tmpOtherUnderlying = ko.observable('');
    self.IsSelected_u = ko.observable(false);
    self.LastModifiedBy_u = ko.observable("");
    self.LastModifiedDate_u = ko.observable("");
    // grid properties
    self.GridProperties = ko.observable();
    self.GridProperties(new GridPropertiesModel(GetTransactionData));
    // set default sorting for pending documents grid
    self.GridProperties().SortColumn("TradeDate");
    self.GridProperties().SortOrder("DESC");
    // Attach Variable
    self.ID_a = ko.observable(0);
    self.FileName_a = ko.observable('');
    self.DocumentPurpose_a = ko.observable(new DocumentPurposeModel2('', '', ''));
    self.ddlDocumentPurpose_a = ko.observableArray([]);
    self.ddlDocumentPurposeNoFx = ko.observableArray([]);
    self.DocumentType_a = ko.observable(new DocumentTypeModel2('', ''));
    self.ddlDocumentType_a = ko.observableArray([]);
    self.DocumentRefNumber_a = ko.observable("");
    self.LastModifiedBy_a = ko.observable("");
    self.LastModifiedDate_a = ko.observable("");
    self.CustomerUnderlyingMappings_a = ko.observableArray([new CustomerUnderlyingMappingModel('', '', '')]);
    self.Documents = ko.observableArray([]);
    self.DocumentPath_a = ko.observable("");
    //#region Variable Filtering for Table
    // filters transaction
    self.FilterApplicationID = ko.observable("");
    self.FilterTZReference = ko.observable("");
    self.FilterCustomer = ko.observable("");
    self.FilterProduct = ko.observable("");
    self.FilterTradeDate = ko.observable("");
    self.FilterValueDate = ko.observable("");
    self.FilterCurrency = ko.observable("");
    self.FilterAmount = ko.observable("");
    self.FilterAccountNumber = ko.observable("");
    self.FilterFXTransaction = ko.observable("");
    self.FilterTransactionStatus = ko.observable("");
    self.FilterTzStatus = ko.observable("");
    self.FilterUpdateBy = ko.observable("");
    self.FilterUpdateDate = ko.observable("");
    self.FilterIsTMO = ko.observable(false);

    // filter Underlying
    self.UnderlyingFilterCIF = ko.observable("");
    self.UnderlyingFilterParentID = ko.observable("");
    self.UnderlyingFilterIsSelected = ko.observable(false);
    self.UnderlyingFilterUnderlyingDocument = ko.observable("");
    self.UnderlyingFilterDocumentType = ko.observable("");
    self.UnderlyingFilterCurrency = ko.observable("");
    self.UnderlyingFilterStatementLetter = ko.observable("");
    self.UnderlyingFilterAmount = ko.observable("");
    self.UnderlyingFilterDateOfUnderlying = ko.observable("");
    self.UnderlyingFilterExpiredDate = ko.observable("");
    self.UnderlyingFilterReferenceNumber = ko.observable("");
    self.UnderlyingFilterInvoiceNumber = ko.observable("");
    self.UnderlyingFilterSupplierName = ko.observable("");
    self.UnderlyingFilterIsProforma = ko.observable("");
    self.UnderlyingFilterIsAvailable = ko.observable(false);
    self.UnderlyingFilterIsExpiredDate = ko.observable(false);
    self.UnderlyingFilterIsSelectedBulk = ko.observable(false);
    self.UnderlyingFilterAvailableAmount = ko.observable("");
    self.UnderlyingFilterAttachmentNo = ko.observable("");
    self.UnderlyingFilterIsTMO = ko.observable(false);
    self.UnderlyingFilterAccountNumber = ko.observable("");
    self.UnderlyingFilterShow = ko.observable(1);
    //Tambah Agung
    self.InstructionFilterShow = ko.observable(1);
    //End
    // filter Attach Underlying File
    self.AttachFilterFileName = ko.observable("");
    self.AttachFilterDocumentPurpose = ko.observable("");
    self.AttachFilterModifiedDate = ko.observable("");
    self.AttachFilterDocumentRefNumber = ko.observable("");
    self.AttachFilterAttachmentReference = ko.observable("");
    // filter Underlying for Attach
    self.UnderlyingAttachFilterIsSelected = ko.observable("");
    self.UnderlyingAttachFilterUnderlyingDocument = ko.observable("");
    self.UnderlyingAttachFilterDocumentType = ko.observable("");
    self.UnderlyingAttachFilterCurrency = ko.observable("");
    self.UnderlyingAttachFilterStatementLetter = ko.observable("");
    self.UnderlyingAttachFilterAmount = ko.observable("");
    self.UnderlyingAttachFilterDateOfUnderlying = ko.observable("");
    self.UnderlyingAttachFilterExpiredDate = ko.observable("");
    self.UnderlyingAttachFilterReferenceNumber = ko.observable("");
    self.UnderlyingAttachFilterSupplierName = ko.observable("");
    self.UnderlyingAttachFilterAccountNumber = ko.observable("");
    self.UnderlyingAttachFilterIsTMO = ko.observable(false);
    //Tambah Agung
    self.InstructionFilterIsSelected = ko.observable(false);
    self.InstructionFilterApplicationID = ko.observable("");
    self.InstructionFilterFileName = ko.observable("");
    self.InstructionFilterPurposeofDoc = ko.observable("");
    self.InstructionFilterDocumentType = ko.observable("");
    self.InstructionFilterLastModifiedDate = ko.observable("");
    //End
    // filter Underlying for Attach
    self.ProformaFilterIsSelected = ko.observable(false);
    self.ProformaFilterUnderlyingDocument = ko.observable("");
    self.ProformaFilterDocumentType = ko.observable("Proforma");
    self.ProformaFilterCurrency = ko.observable("");
    self.ProformaFilterStatementLetter = ko.observable("");
    self.ProformaFilterAmount = ko.observable("");
    self.ProformaFilterDateOfUnderlying = ko.observable("");
    self.ProformaFilterExpiredDate = ko.observable("");
    self.ProformaFilterReferenceNumber = ko.observable("");
    self.ProformaFilterInvoiceNumber = ko.observable("");
    self.ProformaFilterSupplierName = ko.observable("");
    self.ProformaFilterIsProforma = ko.observable(false);
    // filter Underlying for Attach // add 2015.03.09
    self.BulkFilterIsSelected = ko.observable(false);
    self.BulkFilterUnderlyingDocument = ko.observable("");
    self.BulkFilterDocumentType = ko.observable("");
    self.BulkFilterCurrency = ko.observable("");
    self.BulkFilterStatementLetter = ko.observable("");
    self.BulkFilterAmount = ko.observable("");
    self.BulkFilterDateOfUnderlying = ko.observable("");
    self.BulkFilterExpiredDate = ko.observable("");
    self.BulkFilterReferenceNumber = ko.observable("");
    self.BulkFilterSupplierName = ko.observable("");
    self.BulkFilterInvoiceNumber = ko.observable("");
    self.BulkFilterIsBulkUnderlying = ko.observable(false);
    self.utilizationAmount = ko.observable(0.00);
    self.Rounding = ko.observable(0.00);
    self.ParameterRounding = ko.observable(0);
    //#endregion

    //#region Bind clear filters
    self.ClearFilters = function () {
        self.FilterApplicationID("");
        self.FilterTZReference("");
        self.FilterCustomer("");
        self.FilterProduct("");
        self.FilterCurrency("");
        self.FilterAmount("");
        self.FilterAccountNumber("");
        self.FilterFXTransaction("");
        self.FilterTransactionStatus("");
        self.FilterTradeDate("");
        self.FilterValueDate("");
        self.FilterTzStatus("");
        self.FilterUpdateBy("");
        self.FilterUpdateDate("");
        //self.FilterIsTMO(false);
        GetTransactionData();
    };
    self.UnderlyingClearFilters = function () {
        //self.UnderlyingFilterParentID("");
        self.UnderlyingFilterIsSelected(false);
        self.UnderlyingFilterUnderlyingDocument("");
        self.UnderlyingFilterDocumentType("");
        self.UnderlyingFilterCurrency("");
        self.UnderlyingFilterStatementLetter("");
        self.UnderlyingFilterAmount("");
        self.UnderlyingFilterDateOfUnderlying("");
        self.UnderlyingFilterExpiredDate("");
        self.UnderlyingFilterReferenceNumber("");
        self.UnderlyingFilterInvoiceNumber("");
        self.UnderlyingFilterSupplierName("");
        self.UnderlyingFilterIsProforma("");
        self.UnderlyingFilterIsAvailable(false);
        self.UnderlyingFilterIsExpiredDate(false);
        self.UnderlyingFilterAccountNumber("")

        GetDataUnderlying();
    };
    self.ProformaClearFilters = function () {
        //self.UnderlyingFilterParentID("");
        self.ProformaFilterIsSelected(false);
        self.ProformaFilterUnderlyingDocument("");
        self.ProformaFilterDocumentType("Proforma");
        self.ProformaFilterCurrency("");
        self.ProformaFilterStatementLetter("");
        self.ProformaFilterAmount("");
        self.ProformaFilterDateOfUnderlying("");
        self.ProformaFilterExpiredDate("");
        self.ProformaFilterReferenceNumber("");
        self.ProformaFilterInvoiceNumber("");
        self.ProformaFilterSupplierName("");
        self.ProformaFilterIsProforma("");

        GetDataUnderlyingProforma();
    };
    self.BulkClearFilters = function () {
        self.BulkFilterIsSelected(false);
        self.BulkFilterUnderlyingDocument("");
        self.BulkFilterDocumentType("");
        self.BulkFilterCurrency("");
        self.BulkFilterStatementLetter("");
        self.BulkFilterAmount("");
        self.BulkFilterDateOfUnderlying("");
        self.BulkFilterExpiredDate("");
        self.BulkFilterReferenceNumber("");
        self.BulkFilterInvoiceNumber("");
        self.BulkFilterSupplierName("");
        self.BulkFilterIsBulkUnderlying("");
        GetDataBulkUnderlying();
    };
    self.AttachClearFilters = function () {
        self.AttachFilterFileName("");
        self.AttachFilterDocumentPurpose("");
        self.AttachFilterModifiedDate("");
        self.AttachFilterDocumentRefNumber("");
        self.AttachFilterAttachmentReference("");
        GetDataAttachFile();
    };
    self.UnderlyingAttachClearFilters = function () {
        self.UnderlyingAttachFilterIsSelected(false);
        self.UnderlyingAttachFilterUnderlyingDocument("");
        self.UnderlyingAttachFilterDocumentType("");
        self.UnderlyingAttachFilterCurrency("");
        self.UnderlyingAttachFilterStatementLetter("");
        self.UnderlyingAttachFilterAmount("");
        self.UnderlyingAttachFilterDateOfUnderlying("");
        self.UnderlyingAttachFilterExpiredDate("");
        self.UnderlyingAttachFilterReferenceNumber("");
        self.UnderlyingAttachFilterSupplierName("");
        GetDataUnderlyingAttach();
    };
    //Tambah Agung
    self.InstructionClearFilters = function () {
        self.InstructionFilterIsSelected(false);
        self.InstructionFilterApplicationID("");
        self.InstructionFilterFileName("");
        self.InstructionFilterPurposeofDoc("");
        self.InstructionFilterDocumentType("");
        self.InstructionFilterLastModifiedDate("");
        GetDataInstructionDocument();
    };
    //End

    //#endregion

    self.GetTransactionData = function () {
        GetTransactionData();
    };
    self.GetSelectedRow = function (data) {

        // Get transaction Data
        cifData = data.CIF;
        customerNameData = data.CustomerName;
        self.utilizationAmount(0.00);
        self.Rounding(0);
        self.TempSelectedUnderlying([]);
        viewModel.TransactionID(data.TransactionDealID);
        $("#modal-form").modal('show');
        self.TouchTimeStartDate(new Date().toUTCString()); // set value when you touch data
        GetDataTBOTool(data.CIF);
        GetDetailTransactionData(data.TransactionDealID);
    };

    function GetDataTBOTool(cif) {
        var param = cif;
        var options = {
            url: api.server + api.url.tbotmotoolbycif,
            params: {
                id: param,
            },
            token: accessToken
        };
        Helper.Ajax.Get(options, OnSuccessGetDataTBOTool, OnError, OnAlways);
    }

    function OnSuccessGetDataTBOTool(data, textStatus, jqXHR) {
        if (jqXHR.status == 200) {
            viewModel.TBOData([]);
            for (var i = 0 ; i < data.length; i++) {
                var DataTBO = {
                    TransactionID: data[i].TransactionID,
                    ApplicationID: data[i].ApplicationID,
                    CIF: data[i].CIF,
                    CustomerName: data[i].CustomerName,
                    ID: data[i].ID,
                    TBOSLAID: data[i].TBOSLAID,
                    ActDate: data[i].ActDate,
                    ExptDate: data[i].ExptDate,
                    Status: data[i].Status,
                    CreatedBy: data[i].CreatedBy,
                    CreatedDate: data[i].CreatedDate,
                    LastModifiedBy: data[i].ModifiedBy,
                    LastModifiedDate: data[i].ModifiedDate,
                    TBOApplicationID: data[i].TBOApplicationID
                };
                viewModel.TBOData.push(DataTBO);
            }
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }

    self.onSelectionOther = function (underlyingID) {
        if (underlyingID() > 0) {
            GetUnderlyingDocName(underlyingID());
            var underlyingCode = $("#book-underlying-code option:selected").val();
            switch (self.ActivityTitle()) {
                case CONST_TRANSACTION.FX:
                    viewModel.TransactionDealDetailModel().bookunderlyingcode(parseInt(underlyingCode, 10));
                    break;
                case CONST_TRANSACTION.TMO:
                    viewModel.TransactionTMO().UnderlyingCodeID(parseInt(underlyingCode, 10));
                    break;
            }
            $("#new-underlying-code").val(underlyingCode);
        }
    };
    self.onSelectionAttach = function (index, item) {
        var data = ko.utils.arrayFirst(self.TempSelectedAttachUnderlying(), function (x) {
            return x.ID == item.ID;
        });
        if (data != null) {
            self.TempSelectedAttachUnderlying.remove(data);
            self.MakerUnderlyings.remove(data.ID);
            ResetDataAttachment(index, false);
        } else {
            self.TempSelectedAttachUnderlying.push({
                ID: item.ID
            });
            self.MakerUnderlyings.push(item.ID);
            ResetDataAttachment(index, true);
        }
    }
    //Tambah Agung
    self.onSelectionAttachUnderlying = function (index, item) {
        var data = ko.utils.arrayFirst(self.TempSelectedAttachUnderlying(), function (x) {
            return x.ID == item.ID;
        });
        if (data != null) {
            self.TempSelectedAttachUnderlying.remove(data);
            self.MakerUnderlyings.remove(data.ID);
            ResetDataAttachment(index, false);
        } else {
            self.TempSelectedAttachUnderlying.push({
                ID: item.ID
            });
            self.MakerUnderlyings.push(item.ID);
            ResetDataAttachment(index, true);
        }
    }
    self.onSelectionInstruction = function (index, item) {
        var data = ko.utils.arrayFirst(self.TempSelectedInstruction(), function (x) {
            return x.ID == item.ID;
        });
        if (data != null) {
            self.TempSelectedInstruction.remove(data);
            ko.utils.arrayForEach(self.MakerDocuments(), function (itemdoc) {
                if (itemdoc.FileName == item.FileName) {
                    self.MakerDocuments.remove(itemdoc);
                }
            });
            //ko.utils.arrayForEach(self.TransactionTMO().Documents(), function (itemdoc) {
            //    if (itemdoc.FileName == item.FileName) {
            //        self.MakerDocuments.remove(itemdoc);
            //    }
            //});
            ResetDataInstruction(index, false);
        } else {
            self.TempSelectedInstruction.push({
                ID: item.ID
            });
            self.MakerDocuments.push(item);
            //self.TransactionTMO().Documents.push(item);
            ResetDataInstruction(index, true);
        }
    }
    //End
    self.onSelectionProforma = function (index, item) {
        var data = ko.utils.arrayFirst(self.TempSelectedUnderlyingProforma(), function (x) {
            return x.ID == item.ID;
        });
        if (data != null) {
            //self.IsSelectedAttach(false);
            self.TempSelectedUnderlyingProforma.remove(data);
            ResetDataProforma(index, false);
        } else {
            //self.IsSelectedAttach(true);
            self.TempSelectedUnderlyingProforma.push({
                ID: item.ID
            });
            ResetDataProforma(index, true);
        }
    }
    // add 2015.03.09
    self.OnCurrencyChange = function (CurrencyID) {
        if (CurrencyID != null) {
            var item = ko.utils.arrayFirst(self.ddlCurrency_u(), function (x) {
                return CurrencyID == x.ID();
            });
            if (item != null) {
                self.Currency_u(new CurrencyModel2(item.ID(), item.Code(), item.Description()));
            }
            if (!self.IsNewDataUnderlying()) {
                GetSelectedBulkID(GetDataBulkUnderlying);
            } else {
                self.TempSelectedUnderlyingBulk([]);
                GetDataBulkUnderlying();
            }
        }
    }
    self.ClearSelectedBulkUnderlying = function () {
        self.TempSelectedUnderlyingBulk([]);
        self.CustomerBulkUnderlyings([]);
        ko.utils.arrayForEach(self.AllDataCustomerUnderlyingBulk(), function (item) {
            item.IsSelectedBulk = false;
        });
    }
    self.onSelectionBulk = function (index, item) {
        var total = 0;
        var data = ko.utils.arrayFirst(self.TempSelectedUnderlyingBulk(), function (x) {
            return x.ID == item.ID;
        });
        if (data != null) {
            self.TempSelectedUnderlyingBulk.remove(data);
            ResetBulkData(index, false);
        } else {
            self.TempSelectedUnderlyingBulk.push({
                ID: item.ID,
                Amount: item.Amount
            });
            ResetBulkData(index, true);
        }
        for (var i = 0; self.TempSelectedUnderlyingBulk.push() > i; i++) {
            total += self.TempSelectedUnderlyingBulk()[i].Amount;
        }
        if (total > 0) {
            var e = total;
            var res = parseInt(e) * parseFloat(self.Rate_u()) / parseFloat(idrrate);
            res = Math.round(res * 100) / 100;
            res = isNaN(res) ? 0 : res; //avoid NaN
            self.AmountUSD_u(parseFloat(res).toFixed(2));
            self.Amount_u(formatNumber(e));
        } else {
            var e = self.tempAmountBulk();
            var res = parseInt(e) * parseFloat(self.Rate_u()) / parseFloat(idrrate);
            res = Math.round(res * 100) / 100;
            res = isNaN(res) ? 0 : res; //avoid NaN
            self.AmountUSD_u(parseFloat(res).toFixed(2));
            self.Amount_u(formatNumber(e));
        }
    }

    self.onSelectionUtilize = function (index, item) {
        var statementLetterID;
        var statementLetterName;
        var itemdeal;
        switch (viewModel.ActivityTitle()) {
            case CONST_TRANSACTION.FX:
                statementLetterID = self.TransactionDealDetailModel().StatementLetter.ID();
                statementLetterName = self.TransactionDealDetailModel().StatementLetter.Name();
                break;
            case CONST_TRANSACTION.TMO:
                itemdeal = $('#Position' + index).val();
                statementLetterID = self.TransactionTMO().StatementLetter.ID();
                statementLetterName = self.TransactionTMO().StatementLetter.Name();
                if (itemdeal.replace(/,/g, '') > item.AvailableAmountDeal) {
                    ResetDataUnderlying(index, false);
                    ShowNotification("Form Validation Warning", "Available Amount Underlying must be greater and lesh than Utilize Amount Deal", 'gritter-warning', true);
                    return false;
                }
                break;
            default:
                ShowMesageDefault();
                break;
        }
        var data = ko.utils.arrayFirst(self.TempSelectedUnderlying(), function (x) {
            return x.ID() == item.ID;
        });

        if (data != null) { //uncheck condition
            self.TempSelectedUnderlying.remove(data);
            self.MakerUnderlyings.remove(item.ID); //remove underlying for maker
            //self.TempSelectedUnderlying.remove(data);
            ResetDataUnderlying(index, false);
            setTotalUtilize();
            switch (viewModel.ActivityTitle()) {
                case CONST_TRANSACTION.FX:
                    SetFXDocuments();
                    break;
                case CONST_TRANSACTION.TMO:
                    SetTMODocuments();
                    break;
            }
        } else {
            var IsSelectedUnderlying = (self.TempSelectedUnderlying() != null && self.TempSelectedUnderlying().length > 0);
            var dataUnderlyingStatementLetterID = IsSelectedUnderlying && self.TempSelectedUnderlying()[0].StatementLetter();
            if (IsSelectedUnderlying && dataUnderlyingStatementLetterID == item.StatementLetter.ID || (statementLetterID == item.StatementLetter.ID && !IsSelectedUnderlying)) {
                switch (viewModel.ActivityTitle()) {
                    case CONST_TRANSACTION.FX:
                        self.TempSelectedUnderlying.push(new SelectUtillizeModel(item.ID, false, item.AvailableAmount, item.StatementLetter.ID));
                        self.MakerUnderlyings.push(item.ID);
                        ResetDataUnderlying(index, true);
                        setTotalUtilize();
                        SetFXDocuments();
                        break;
                    case CONST_TRANSACTION.TMO:
                        self.TempSelectedUnderlying.push(new SelectUtillizeTMOModel(item.ID, false, item.AvailableAmount, parseFloat(itemdeal.replace(/,/g, '')), item.StatementLetter.ID));
                        self.MakerUnderlyings.push(item.ID);
                        ResetDataUnderlying(index, true);
                        //self.CustomerUnderlyings()[index].UtilizeAmountDeal = itemdeal;
                        setTotalUtilize();
                        SetTMODocuments();
                        break;
                }
            } else {
                ResetDataUnderlying(index, false);
            }
        }
    }

    self.onChangeUtilizeAmountDeal = function (item) {
        var index = item.RowID - 1;
        var itemdeal = $('#Position' + index).val();
        if (parseFloat($('#Position' + index).val().replace(/,/g, '')) > item.AvailableAmountDeal) {
            if (itemdeal == "NaN" || itemdeal == "") {
                $('#Position' + index).val(formatNumber(parseFloat(item.AvailableAmountDeal)));
            } else {
                $('#Position' + index).val(formatNumber(parseFloat(item.UtilizeAmountDeal.replace(/,/g, ''))));
            }
            ShowNotification("Form Validation Warning", "Available Amount Underlying must be greater and lesh than Utilize Amount Deal", 'gritter-warning', true);
            return false;
        } else {
            if (itemdeal == "NaN" || itemdeal == "") {
                $('#Position' + index).val(formatNumber(parseFloat(item.AvailableAmountDeal)));
            } else {
                $('#Position' + index).val(formatNumber(parseFloat(itemdeal.replace(/,/g, ''))));
            }
            if (item.IsEnable == true) {
                //item.IsEnable = false;
                if (itemdeal == "NaN" || itemdeal == "") {
                    item.UtilizeAmountDeal = formatNumber(parseFloat(item.AvailableAmountDeal));
                } else {
                    item.UtilizeAmountDeal = formatNumber(parseFloat(itemdeal.replace(/,/g, '')));
                }

                ko.utils.arrayForEach(self.TempSelectedUnderlying(), function (itemtemp) {
                    if (itemtemp.ID() == item.ID) {
                        self.TempSelectedUnderlying.remove(itemtemp);
                        itemtemp.UtilizeAmountDeal(parseFloat(item.UtilizeAmountDeal.replace(/,/g, '')));
                        self.TempSelectedUnderlying.push(itemtemp);
                        setTotalUtilize();
                    }
                });
                //var datad = ko.utils.arrayFirst(self.TempSelectedUnderlying(), function (x) {
                //    return x.ID() == item.ID;
                //});
                //if (datad != null) {
                //    self.TempSelectedUnderlying.remove(datad);
                //    setTotalUtilize();
                //}
                //self.MakerUnderlyings.remove(item.ID);
                //SetTMODocuments();
                //ResetDataUnderlying(index, false);
            }
        }
        //viewModel.CustomerUnderlyings()[item.RowID - 1].UtilizeAmountDeal = formatNumber(parseFloat(itemdeal));
    }

    self.OnChangeStatementLetterUnderlying = function () {
        var fromFormat = document.getElementById("Amount_u").value;
        fromFormat = fromFormat.toString().replace(/,/g, '');
        if (self.StatementLetter_u().ID() == CONST_STATEMENT.StatementA_ID) {
            self.Currency_u().ID(1);
            self.Amount_u(formatNumber(DataModel.ThresholdBuy));
            // set default last day of month
            var date = new Date();
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            var day = lastDay.getDate();
            var month = lastDay.getMonth() + 1;
            var year = lastDay.getFullYear()
            var fixLastDay = year + "/" + month + "/" + day;
            self.ExpiredDate_u(self.LocalDate(fixLastDay, true, false));
            SetDefaultValueStatementA();
        } else if (self.StatementLetter_u().ID() == CONST_STATEMENT.StatementB_ID && self.IsNewDataUnderlying()) {
            self.ClearUnderlyingData();
            if (self.LocalDate(self.DateOfUnderlying_u()) != "") {
                var date = new Date(self.DateOfUnderlying_u());
                var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                var day = date.getDate();
                var month = lastDay.getMonth() + 13;
                var year = lastDay.getFullYear();
                if (month > 12) {
                    month -= 12;
                    year += 1;
                }
                var fixLastDay = year + "/" + month + "/" + day;
                if (!moment(fixLastDay, 'YYYY/MM/DD').isValid()) {
                    var LastDay_ = new Date(year, month, 0);
                    day = LastDay_.getDate();
                }
                self.ExpiredDate_u(self.LocalDate(fixLastDay, true, false));
            }
        }

        var e = document.getElementById("Amount_u").value;
        e = e.toString().replace(/,/g, '');

        var res = parseFloat(e) * parseFloat(self.Rate_u()) / parseFloat(idrrate);
        res = Math.round(res * 100) / 100;
        res = isNaN(res) ? 0 : res; //avoid NaN
        self.AmountUSD_u(parseFloat(res).toFixed(2));
        self.Amount_u(formatNumber(e));

    }
    self.OnChangeJointAcc = function (obj, event) {
        if (!self.IsSelectedRow()) {
            if (obj.IsJointAccount != null) {
                var dataAccounts = ko.utils.arrayFilter(GetAccounts(), function (dta) {
                    var sAccount = dta.IsJointAccount() == null ? false : dta.IsJointAccount();
                    var debitCurrency = GetDebitCurrency() != null ? GetDebitCurrency() : "";
                    return sAccount == obj.IsJointAccount() && debitCurrency == dta.Currency.Code()
                });
                if (dataAccounts != null && dataAccounts.length > 0) {
                    self.DynamicAccounts([]);
                    if (obj.IsJointAccount() == false) {
                        dataAccounts = AddOtherAccounts(dataAccounts);
                        //single account               
                        self.UnderlyingFilterShow(1); // show single account
                        self.GetDataUnderlying();
                    }
                    self.DynamicAccounts(dataAccounts);
                } else {
                    if (!obj.IsJointAccount()) {
                        self.DynamicAccounts([]);
                        if (obj.IsJointAccount() == false) {
                            dataAccounts = AddOtherAccounts(dataAccounts);
                        }
                        self.DynamicAccounts(dataAccounts);
                        //single account               
                        self.UnderlyingFilterShow(1); // show single account
                        self.GetDataUnderlying();
                    }
                }

                GetTotalTransaction(); //Rizki - 20161108 - TFS 6400
            }
        }
    }
    self.OnChangeJointAccUnderlying = function (obj, evnt) {
        if (evnt.originalEvent != null) {
            self.ClearSelectedBulkUnderlying();
            var filterDataUnderlying = ko.utils.arrayFilter(self.AllDataCustomerUnderlyingBulk(), function (item) {
                return self.IsJointAccount_u() == (item.IsJointAccount != null ? item.IsJointAccount : false);
            });

            if (filterDataUnderlying != null && filterDataUnderlying.length > 0) {

                self.CustomerBulkUnderlyings(filterDataUnderlying);
            } else {
                self.CustomerBulkUnderlyings(filterDataUnderlying);
            }

            if (!self.IsJointAccount_u()) {
                CustomerUnderlying.AccountNumber(null);
            }
        }
    }
    self.OnChangeAccountNumber = function (obj, evnt) {
        if (evnt.originalEvent != null) {
            self.ClearSelectedBulkUnderlying();
            if (self.AccountNumber_u() != null) {
                var filterDataUnderlying = ko.utils.arrayFilter(self.AllDataCustomerUnderlyingBulk(), function (item) {
                    return self.AccountNumber_u() == item.AccountNumber;
                });
                if (filterDataUnderlying != null && filterDataUnderlying.length > 0) {
                    self.CustomerBulkUnderlyings(filterDataUnderlying);
                } else {
                    self.CustomerBulkUnderlyings(filterDataUnderlying);
                }
            }
        }
    }
    // bind get data function to view
    self.GetDataUnderlying = function () {
        GetDataUnderlying();
    };
    self.SetDynamicAccount = function () {
        SetDynamicAccount();
    }
    self.GetSelectedUtilizeID = function () {
        GetSelectedUtilizeID();
    }

    self.GetTotalAmount = function (CIFData, AmountUSD) {
        GetTotalAmount(CIFData, AmountUSD);
    }

    self.GetAvailableStatementB = function (CIFData) {
        GetAvailableStatementB(CIFData);
    }

    self.GetTreshold = function () {
        GetTreshold();
    }

    self.GetFCYIDRTreshold = function () {
        GetFCYIDRTreshold();
    }

    self.GetDataAttachFile = function () {
        GetDataAttachFile();
    }

    self.GetDataUnderlyingAttach = function () {
        GetDataUnderlyingAttach();
    }
    //Tambah Agung
    self.GetDataInstructionDocument = function () {
        GetDataInstructionDocument();
    }
    //End

    self.GetDataUnderlyingProforma = function () {
        GetDataUnderlyingProforma();
    }

    // 2015.03.09
    self.GetDataBulkUnderlying = function () {
        GetDataBulkUnderlying();
    }

    self.GetUnderlyingParameters = function (data) {
        GetUnderlyingParameters(data);
    }

    self.CalculateFX = function (amountUSD) {
        CalculateFX(amountUSD);
    }

    self.SetCalculateFX = function (amountUSD) {
        SetCalculateFX(amountUSD);
    }
    /*self.ChangedIsResident = ko.computed(function () {
        if (self.TransactionDealDetailModel().IsResident() != null) {
            GetTotalTransaction();
        }
    });*/
    self.btnNewTransaction = function () {
        if (!IsValidUnderlying()) {
            return false;
        } else {
            self.IsNewTransaction(true);
            self.IsShowSubmit(true);
        }
    }

    //add validation if statement B is choose then underlying doc select "tanpa underlying" by henggar wicaksana
    function IsValidUnderlying() {
        //var defaultvalue = 0;
        //var tmpData = [];
        //for (var i = 0; i < viewModel.TransactionDealDetailModel().bookunderlyingcode() ; i++) {
        //    if (viewModel.ddlUnderlyingDocument_u()[i].Name() == "tanpa underlying") {
        //        tmpData.push(viewModel.ddlUnderlyingDocument_u()[i]);
        //    }
        //}
        //var ArraySelected = i;
        //console.log(tmpData);
        //if (GetModelTransaction().StatementLetter.ID() == Const_Underlying.SelectedStatement) {
        //    var SelectedUnderlying = Const_Underlying.SelectedUnderlying;
        //    if (ArraySelected == SelectedUnderlying) {
        //        ShowNotification("Form Validation Warning", "Please dont select 'tanpa undrlying' in Underlying Code", 'gritter-warning', true);
        //        defaultvalue = 1;
        //        return defaultvalue;
        //        return;
        //    }
        //}
        switch (self.ActivityTitle()) {
            case CONST_TRANSACTION.FX:
                if (GetModelTransaction().StatementLetter.ID() == Const_Underlying.SelectedStatement) {
                    if (GetModelTransaction().bookunderlyingcode() == Const_Underlying.SelectedUnderlying) {
                        ShowNotification("Form Validation Warning", "Please dont select 'tanpa undrlying' in Underlying Code", 'gritter-warning', true);
                        return false;
                    }
                }
                break;
            case CONST_TRANSACTION.TMO:
                if (GetModelTransaction().StatementLetter.ID() == Const_Underlying.SelectedStatement) {
                    if (GetModelTransaction().UnderlyingCodeID() == Const_Underlying.SelectedUnderlying) {
                        ShowNotification("Form Validation Warning", "Please dont select 'tanpa undrlying' in Underlying Code", 'gritter-warning', true);
                        return false;
                    }
                }
                break;
        }

        return true;
    }
    //end wicaksana

    self.IsBooked = function () {
        self.IsDeal(true);
        // validation
        var form = $("#aspnetForm");
        form.validate();

        if (!IsValidUnderlying()) {
            return false;
        } else {

            if (form.valid()) {
                self.IsEditable(false);
                if (self.TransactionDealDetailModel().ApplicationID() == null) {
                    var tzRef = (self.TransactionDealDetailModel().TZReference() == null ? $.now : self.TransactionDealDetailModel().TZReference());
                    var newApplicationID = 'FX-' + tzRef;
                    self.TransactionDealDetailModel().ApplicationID(newApplicationID);
                }
                if (self.t_IsFxTransaction() || self.t_IsFxTransactionToIDR()) {
                    var isSubmit = false;
                    GetThreshold(isSubmit);
                    //UpdateTransaction();
                } else {
                    self.IsEditable(true);
                    ShowNotification("Form Validation Warning", "Transaction FCY->FCY, please select IDR->FCY or FCY->IDR transaction", 'gritter-warning', false);
                }
                //ValidateTransaction();
            }
            else {
                self.IsEditable(true);
                ShowNotification("Form Validation Warning", CONST_MSG.ValidationFields, 'gritter-warning', false);
            }
        }
    };

    self.save_u = function () {
        // validation
        self.IsEditTableUnderlying(false);
        var form = $("#frmUnderlying");
        form.validate();

        if (form.valid()) {
            if (CustomerUnderlying.StatementLetter().ID() == CONST_STATEMENT.StatementB_ID && CustomerUnderlying.UnderlyingDocument().ID() == 36) {
                ShowNotification("Form Validation Warning", "Please Change Document Underlying other 998(tanpa underlying). ", 'gritter-warning', false);
                self.IsEditTableUnderlying(true);
                return false;
            }
            self.IsTMO_u(self.FilterIsTMO());
            viewModel.Amount_u(viewModel.Amount_u().toString().replace(/,/g, ''));
            CustomerUnderlying.OtherUnderlying = self.CodeUnderlying() == '999' ? self.OtherUnderlying_u : '';
            SetMappingProformas();
            SetMappingBulks(); // add 2015.03.09
            CustomerUnderlying.AvailableAmountUSD = self.AmountUSD_u();
            //Ajax call to insert the CustomerUnderlyings
            $.ajax({
                type: "POST",
                url: api.server + api.url.customerunderlying + "/Save",
                data: ko.toJSON(CustomerUnderlying), //Convert the Observable Data into JSON
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + accessToken
                },
                success: function (data, textStatus, jqXHR) {
                    if (jqXHR.status = 200) {
                        // send notification
                        ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);
                        $("#modal-form-Underlying").modal('hide');
                        $('#backDrop').hide();
                        self.IsEditTableUnderlying(true);
                        // refresh data
                        GetDataUnderlying();
                        GetTotalTransaction();
                        //GetRemainingBalance(DealModel, onSuccessRemainingBlnc, OnErrorDeal);
                        $("#new-underlying-code").val(underlyingCodeSave);
                    } else {

                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                        self.IsEditTableUnderlying(true);

                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // send notification
                    if (jqXHR.responseJSON.Message.toLowerCase().startsWith("double underlying")) {
                        ShowNotification("", jqXHR.responseJSON.Message, 'gritter-warning', true);
                    } else {
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    } self.IsEditTableUnderlying(true);
                }
            });

        } else {
            self.IsEditTableUnderlying(true);
            ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);
        }
    };

    self.save_a = function () {
        self.IsUploading(true);
        var doc = {
            ID: 0,
            FileName: Documents.DocumentPath() != null ? Documents.DocumentPath().name : null,
            DocumentPath: self.DocumentPath_a(),
            Type: ko.utils.arrayFirst(self.ddlDocumentType_a(), function (item) {
                return item.ID == self.DocumentType_a().ID();
            }),
            Purpose: ko.utils.arrayFirst(self.ddlDocumentPurpose_a(), function (item) {
                return item.ID == self.DocumentPurpose_a().ID();
            }),
            IsNewDocument: true,
            LastModifiedDate: new Date(),
            LastModifiedBy: null
        };
        if (doc.Type == null || doc.Purpose == null || doc.DocumentPath == null || doc.FileName == null) {
            ShowNotification("Information", CONST_MSG.NotCompleteUpload, 'gritter-warning', true);
            self.IsUploading(false);
        } else {
            var FxWithUnderlying = (self.TempSelectedAttachUnderlying().length > 0);
            var FxWithNoUnderlying = (doc.Purpose.ID != 2);
            if (FxWithUnderlying || FxWithNoUnderlying) {
                var dataCIF = null;
                var dataName = null;
                switch (self.ActivityTitle()) {
                    case CONST_TRANSACTION.FX:
                        dataCIF = viewModel.TransactionDealDetailModel().Customer.CIF();
                        dataName = viewModel.TransactionDealDetailModel().Customer.Name();
                        break;
                    case CONST_TRANSACTION.TMO:
                        dataCIF = viewModel.TransactionTMO().Customer.CIF();
                        dataName = viewModel.TransactionTMO().Customer.Name();
                        break;
                }
                var data = {
                    CIF: dataCIF,
                    Name: dataName,
                    Type: ko.utils.arrayFirst(self.ddlDocumentType_a(), function (item) {
                        return item.ID == self.DocumentType_a().ID();
                    }),
                    Purpose: ko.utils.arrayFirst(self.ddlDocumentPurpose_a(), function (item) {
                        return item.ID == self.DocumentPurpose_a().ID();
                    })
                };
                if (FxWithUnderlying && data.Purpose.ID == 2) {
                    UploadFileUnderlying(data, doc, SaveDataFile);
                }
                if (FxWithNoUnderlying) {
                    self.MakerDocuments.push(doc);
                }
                $("#modal-form-Attach").modal('hide');
                $("#modal-form-Attach1").modal('hide');
                $('#backDrop').hide();
            } else {
                ShowNotification("Information", CONST_MSG.NotCompleteUpload, 'gritter-warning', true);
                self.IsUploading(false);
            }
        }
    }
    self.update_u = function () {
        var form = $("#frmUnderlying");
        form.validate();

        if (form.valid()) {
            if (CustomerUnderlying.StatementLetter().ID() == CONST_STATEMENT.StatementB_ID && CustomerUnderlying.UnderlyingDocument().ID() == 36) {
                ShowNotification("Form Validation Warning", "Please Change Document Underlying other 998(tanpa underlying). ", 'gritter-warning', false);
                self.IsEditTableUnderlying(true);
                return false;
            }

            // hide current popup window
            viewModel.Amount_u(viewModel.Amount_u().toString().replace(/,/g, ''));
            $("#modal-form-Underlying").modal('hide');

            bootbox.confirm("Are you sure?", function (result) {
                if (!result) {
                    $("#modal-form-Underlying").modal('show');
                    $('#backDrop').show();
                } else {
                    CustomerUnderlying.OtherUnderlying = self.CodeUnderlying() == '999' ? self.OtherUnderlying_u : '';
                    SetMappingProformas();
                    SetMappingBulks(); // add 2015.03.09

                    $.ajax({
                        type: "PUT",
                        url: api.server + api.url.customerunderlying + "/" + CustomerUnderlying.ID(),
                        data: ko.toJSON(CustomerUnderlying),
                        contentType: "application/json",
                        headers: {
                            "Authorization": "Bearer " + accessToken
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (jqXHR.status = 200) {

                                // send notification
                                ShowNotification('Update Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                                $("#modal-form-Underlying").modal('hide');
                                $('#backDrop').hide();

                                GetDataUnderlying();
                                GetTotalTransaction();
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            if (jqXHR.responseJSON.Message.toLowerCase().startsWith("double underlying")) {
                                ShowNotification("", jqXHR.responseJSON.Message, 'gritter-warning', true);
                            } else {
                                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                            }
                        }
                    });
                }
            });
        } else {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);
        }
    };
    self.delete_u = function () {
        // hide current popup window
        $("#modal-form-Underlying").modal('hide');

        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                $("#modal-form-Underlying").modal('show');
                $('#backDrop').show();
            } else {
                //Ajax call to delete the Customer
                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.customerunderlying + "/remove/" + CustomerUnderlying.ID(),
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if (jqXHR.status = 200) {

                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);
                            $('#backDrop').hide();
                            self.IsEditTableUnderlying(true);

                            // refresh data
                            GetDataUnderlying();
                            GetTotalTransaction();
                            var data = ko.utils.arrayFirst(self.TempSelectedUnderlying(), function (x) {
                                return x.ID() == CustomerUnderlying.ID();
                            });
                            if (data != null) { //uncheck condition
                                self.MakerUnderlyings.remove(CustomerUnderlying.ID()); //remove underlying for maker
                                self.TempSelectedUnderlying.remove(data);
                                setTotalUtilize();
                                switch (self.ActivityTitle()) {
                                    case CONST_TRANSACTION.FX:
                                        SetFXDocuments();
                                        break;
                                    case CONST_TRANSACTION.TMO:
                                        SetTMODocuments();
                                        break;
                                    default:
                                        ShowMesageDefault();
                                        break;
                                }
                            }
                        } else {
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                            self.IsEditTableUnderlying(true);
                        }

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                        self.IsEditTableUnderlying(true);
                    }
                });
            }
        });
    };
    self.cancel_u = function () {
        $('#backDrop').hide();
    }
    self.cancel_a = function () {
        $('#backDrop').hide();
    }

    self.delete_a = function (item) {

        bootbox.confirm("Are you sure want to delete?", function (result) {
            if (!result) {
                //$("#modal-form").modal('show');
            } else {
                //Ajax call to delete the Customer
                $.ajax({
                    type: "DELETE",
                    url: api.server + api.url.customerunderlyingfile + "/" + item.ID,
                    headers: {
                        "Authorization": "Bearer " + accessToken
                    },
                    success: function (data, textStatus, jqXHR) {
                        // send notification
                        if (jqXHR.status = 200) {
                            DeleteFile(item.DocumentPath);
                            ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);

                            // refresh data
                            GetDataAttachFile();
                        } else
                            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // send notification
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                });
            }
        });

    }

    // check permission for button book deal
    self.IsPermitedButtonBookDeal = function () {
        GetPermissionBookDeal();
        /*
        //Ajax call to delete the Customer
        var oSPUser = viewModel.SPUser();

        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/CheckUserPermited/Book Deal Transaction",
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    //compare user role to page permission to get checker validation
                    for (i = 0; i < oSPUser.Roles.length; i++) {
                        for (j = 0; j < data.length; j++) {
                            if (oSPUser.Roles[i].Name.toLowerCase() == data[j].toLowerCase()) {
                                self.IsPermissionBookDeal(true);
                                return false;
                            }
                        }
                    }
                    self.IsPermissionBookDeal(false);
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        }); */
    }

    self.DateToFormat = function (date, isDateOnly, isDateLong) {
        if (date == '1970/01/01 00:00:00' || date == null) {
            return "";
        }
        var DateToFormat = new Date(date);
        if (moment(DateToFormat).isValid()) {
            return moment(DateToFormat).format(config.format.date);
            //return moment(DateToFormat).format("YYYY-MM-DD");
        } else {
            return "";
        }
    };

    //Function to Display record to be updated
    self.GetUnderlyingSelectedRow = function (data) {
        data = ko.toJS(data);
        self.IsNewDataUnderlying(false);

        $("#modal-form-Underlying").modal('show');
        $('#backDrop').show();
        self.ID_u(data.ID);
        self.CustomerName_u(data.CustomerName);
        self.UnderlyingDocument_u(new UnderlyingDocumentModel2(data.UnderlyingDocument.ID, data.UnderlyingDocument.Name, data.UnderlyingDocument.Code));
        self.OtherUnderlying_u(data.OtherUnderlying);
        self.DocumentType_u(new DocumentTypeModel2(data.DocumentType.ID, data.DocumentType.Name));

        //self.Currency_u(data.Currency);
        self.Currency_u(new CurrencyModel2(data.Currency.ID, data.Currency.Code, data.Currency.Description));

        self.Amount_u(data.Amount);
        self.AttachmentNo_u(data.AttachmentNo);

        //self.StatementLetter_u(data.StatementLetter);
        self.StatementLetter_u(new StatementLetterModel(data.StatementLetter.ID, data.StatementLetter.Name));

        self.IsDeclarationOfException_u(data.IsDeclarationOfException);
        self.StartDate_u(self.LocalDate(data.StartDate, true, false));
        self.EndDate_u(self.LocalDate(data.EndDate, true, false));
        self.DateOfUnderlying_u(self.LocalDate(data.DateOfUnderlying, true, false));
        self.ExpiredDate_u(self.LocalDate(data.ExpiredDate, true, false));
        self.ReferenceNumber_u(data.ReferenceNumber);
        self.SupplierName_u(data.SupplierName);
        self.InvoiceNumber_u(data.InvoiceNumber);
        self.IsProforma_u(data.IsProforma);
        self.IsUtilize_u(data.IsUtilize);
        self.IsJointAccount_u(data.IsJointAccount);
        self.AccountNumber_u(data.AccountNumber);
        self.IsSelectedProforma(data.IsSelectedProforma);
        self.LastModifiedBy_u(data.LastModifiedBy);
        self.LastModifiedDate_u(data.LastModifiedDate);
        self.IsBulkUnderlying_u(data.IsBulkUnderlying); // add 2015.03.09
        self.IsSelectedBulk(data.IsSelectedBulk); // add 2015.03.09
        self.tempAmountBulk(data.Amount);// add 2015.03.09
        //Call Grid Underlying for Check Proforma
        GetSelectedProformaID(GetDataUnderlyingProforma);

        //Call Grid Underlying for Check Bullk // add 2015.03.09
        GetSelectedBulkID(GetDataBulkUnderlying);


    };

    //Add Attach File
    self.NewAttachFile = function () {
        // show the dialog task form
        $("#modal-form-Attach").modal('show');
        $('#backDrop').show();
        self.TempSelectedAttachUnderlying([]);
        self.ID_a(0);
        self.DocumentPurpose_a(new DocumentPurposeModel2('', '', ''));
        self.DocumentType_a(new DocumentTypeModel2('', ''));
        self.DocumentPath_a('');
        GetDataUnderlyingAttach();
    }

    self.ClearUnderlyingData = function () {
        var currencyID = self.ActivityTitle() == "FX" ? GetModelTransaction().Currency.ID() : GetModelTransaction().BuyCurrency.ID();
        if (currencyID != self.Currency_u().ID && self.StatementLetter_u().ID != CONST_STATEMENT.StatementA_ID) {
            var cur = ko.utils.arrayFirst(self.ddlCurrency_u(), function (item) {
                return currencyID == item.ID();
            });

            if (cur != null) {
                self.Currency_u(new CurrencyModel2(cur.ID(), cur.Code(), cur.Description()));
            } else {
                self.Currency_u(new CurrencyModel2('', '', ''));
            }

        }
        self.UnderlyingDocument_u(new UnderlyingDocumentModel2('', '', ''));
        self.OtherUnderlying_u('');
        self.DocumentType_u(new DocumentTypeModel2('', ''));

        self.Amount_u(0);
        self.AttachmentNo_u('');
        self.IsDeclarationOfException_u(false);
        self.StartDate_u('');
        self.EndDate_u('');
        self.ExpiredDate_u('');
        self.SupplierName_u('');
        self.InvoiceNumber_u('');
    }
    self.onChangeDateOfUnderlying = function () {
        if (self.StatementLetter_u().ID() == CONST_STATEMENT.StatementA_ID) {
            self.Currency_u().ID(1);
            self.Amount_u(formatNumber(DataModel.ThresholdBuy));
            var date = new Date();
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            var day = lastDay.getDate();
            var month = lastDay.getMonth() + 1;
            var year = lastDay.getFullYear()
            var fixLastDay = year + "/" + month + "/" + day;
            self.ExpiredDate_u(self.LocalDate(fixLastDay, true, false));
            //SetDefaultValueStatementA();
        } else if (self.StatementLetter_u().ID() == CONST_STATEMENT.StatementB_ID && self.IsNewDataUnderlying()) {
            //self.ClearUnderlyingData();
            if (self.LocalDate(self.DateOfUnderlying_u()) != "") {
                var date = new Date(self.DateOfUnderlying_u());
                var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                var day = date.getDate();
                var month = lastDay.getMonth() + 13;
                var year = lastDay.getFullYear();
                if (month > 12) {
                    month -= 12;
                    year += 1;
                }
                var fixLastDay = year + "/" + month + "/" + day;
                if (!moment(fixLastDay, 'YYYY/MM/DD').isValid()) {
                    var LastDay_ = new Date(year, month, 0);
                    day = LastDay_.getDate();
                }
                self.ExpiredDate_u(self.LocalDate(fixLastDay, true, false));
            }
        }
    }
    //insert new
    self.NewDataUnderlying = function () {
        //Remove required filed if show
        $('.form-group').removeClass('has-error').addClass('has-info');
        $('.help-block').remove();

        $("#modal-form-Underlying").modal('show');
        $('#backDrop').show();
        // flag as new Data
        self.IsNewDataUnderlying(true);
        // bind empty data
        self.ID_u(0);
        self.StatementLetter_u(new StatementLetterModel('', ''));
        self.ReferenceNumber_u('');
        self.ClearUnderlyingData();
        self.DateOfUnderlying_u('');
        self.IsProforma_u(false);
        self.IsSelectedProforma(false);
        self.IsUtilize_u(false);
        self.IsJointAccount_u(false);
        self.AccountNumber_u("");
        self.IsBulkUnderlying_u(false); //add 2015.03.09
        self.IsSelectedBulk(false); //add 2015.03.09
        self.tempAmountBulk(0);//add 2015.03.09
        //Call Grid Underlying for Check Proforma
        GetDataUnderlyingProforma();
        GetDataBulkUnderlying();
        self.TempSelectedUnderlyingProforma([]);
        self.TempSelectedUnderlyingBulk([]);
        //setStatementA();
        setRefID();
        if (GetModelTransaction().IsJointAccount()) {
            self.AccountNumber_u(GetModelTransaction().AccountNumber());
            self.IsJointAccount_u(true);
        }
        //underlyingCodeSave = $("#new-underlying-code option:selected").val();
        //viewModel.TransactionDealDetailModel().bookunderlyingcode(parseInt(underlyingCodeSave, 10));
        //console.log(underlyingCodeSave);


    };

    //The Object which stored data entered in the observables
    var CustomerUnderlying = {
        ID: self.ID_u,
        CIF: self.CIF_u,
        CustomerName: self.CustomerName_u,
        UnderlyingDocument: self.UnderlyingDocument_u,
        DocumentType: self.DocumentType_u,
        Currency: self.Currency_u,
        Amount: self.Amount_u,
        Rate: self.Rate_u,
        AmountUSD: self.AmountUSD_u,
        AvailableAmountUSD: self.AvailableAmount_u,
        UtilizeAmountDeal: self.AvailableAmount_u,
        AttachmentNo: self.AttachmentNo_u,
        StatementLetter: self.StatementLetter_u,
        IsDeclarationOfException: self.IsDeclarationOfException_u,
        StartDate: self.StartDate_u,
        EndDate: self.EndDate_u,
        DateOfUnderlying: self.DateOfUnderlying_u,
        ExpiredDate: self.ExpiredDate_u,
        ReferenceNumber: self.ReferenceNumber_u,
        SupplierName: self.SupplierName_u,
        InvoiceNumber: self.InvoiceNumber_u,
        IsSelectedProforma: self.IsSelectedProforma,
        IsProforma: self.IsProforma_u,
        IsSelectedBulk: self.IsSelectedBulk,
        IsBulkUnderlying: self.IsBulkUnderlying_u,
        BulkUnderlyings: self.BulkUnderlyings,
        IsUtilize: self.IsUtilize_u,
        IsEnable: self.IsEnable_u,
        Proformas: self.Proformas,
        OtherUnderlying: self.OtherUnderlying_u,
        IsJointAccount: self.IsJointAccount_u,
        IsTMO: self.IsTMO_u,
        AccountNumber: self.AccountNumber_u,
        LastModifiedBy: self.LastModifiedBy_u,
        LastModifiedDate: self.LastModifiedDate_u
    };

    var Documents = {
        Documents: self.Documents,
        DocumentPath: self.DocumentPath_a,
        Type: self.DocumentType_a,
        Purpose: self.DocumentPurpose_a
    }

    var CustomerUnderlyingFile = {
        ID: self.ID_a,
        FileName: DocumentModels().FileName,
        DocumentPath: DocumentModels().DocumentPath,
        DocumentPurpose: self.DocumentPurpose_a,
        DocumentType: self.DocumentType_a,
        DocumentRefNumber: self.DocumentRefNumber_a,
        LastModifiedBy: self.LastModifiedBy_a,
        LastModifiedDate: self.LastModifiedDate_a,
        CustomerUnderlyingMappings: self.CustomerUnderlyingMappings_a
    };

    //#region Function Switch Case TMO and FX Model
    function SetDocumentData(doc) {
        switch (viewModel.ActivityTitle()) {
            case CONST_TRANSACTION.FX:
                viewModel.TransactionDealDetailModel().Documents.push(doc);
                break;
            case CONST_TRANSACTION.TMO:
                viewModel.TransactionTMO().Documents.push(doc);
                break;
            default:
                ShowMesageDefault();
                break;
        }
    }
    function GetUrlDetailTransactionData(TransactionDealID) {
        switch (self.ActivityTitle()) {
            case CONST_TRANSACTION.FX:
                return api.server + api.url.transactiondeal + '/update/' + TransactionDealID;
                break;
            case CONST_TRANSACTION.TMO:
                return api.server + api.url.transactiondeal + '/TMOUpdate/' + TransactionDealID;
                break;
            default:
                ShowMesageDefault();
                break;
        }
    }
    function GetStatementLetterID() {
        switch (viewModel.ActivityTitle()) {
            case CONST_TRANSACTION.FX:
                return viewModel.TransactionDealDetailModel().StatementLetter.ID();
                break;
            case CONST_TRANSACTION.TMO:
                return viewModel.TransactionTMO().StatementLetter.ID();
                break;
        }
    }
    function GetFlowValas() {
        switch (viewModel.ActivityTitle()) {
            case CONST_TRANSACTION.FX:
                return GetIsFlowValas(viewModel.TransactionDealDetailModel().ProductType);
                break;
            case CONST_TRANSACTION.TMO:
                isFlowValas = true;
                break;
            default:
                ShowMesageDefault();
                break;
        }
    }
    function GetAmountUSD() {
        switch (viewModel.ActivityTitle()) {
            case CONST_TRANSACTION.FX:
                return viewModel.TransactionDealDetailModel().AmountUSD();
                break;
            case CONST_TRANSACTION.TMO:
                return viewModel.TransactionTMO().AmountUSD();
                break;
            default:
                ShowMesageDefault();
                break;
        }
    }
    function GetTransactionID() {
        switch (viewModel.ActivityTitle()) {
            case CONST_TRANSACTION.FX:
                return viewModel.TransactionDealDetailModel().ID();
                break;
            case CONST_TRANSACTION.TMO:
                return viewModel.TransactionTMO().ID();
                break;
            default:
                ShowMesageDefault();
                break;
        }
    }
    function GetPermissionBookDeal() {
        var oSPUser = viewModel.SPUser();

        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/CheckUserPermited/Book Deal Transaction",
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    //compare user role to page permission to get checker validation
                    for (i = 0; i < oSPUser.Roles.length; i++) {
                        for (j = 0; j < data.length; j++) {
                            if (Const_RoleName[oSPUser.Roles[i].ID].toLowerCase() == data[j].toLowerCase()) { //if (oSPUser.Roles[i].Name.toLowerCase() == data[j].toLowerCase()) {
                                self.IsPermissionBookDeal(true);
                                return false;
                            }
                        }
                    }
                    self.IsPermissionBookDeal(false);
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    function GetPermissionGroup() {
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/CheckUserPermited/" + _spFriendlyUrlPageContextInfo.title,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    //compare user role to page permission to get checker validation
                    viewModel.isDealBUGroup(false);
                    for (i = 0; i < spUser.Roles.length; i++) {
                        for (j = 0; j < data.length; j++) {
                            if (Const_RoleName[spUser.Roles[i].ID] == data[j]) { //if (spUser.Roles[i].Name == data[j]) {
                                if (Const_RoleName[spUser.Roles[i].ID].toLowerCase().startsWith("dbs fx") && Const_RoleName[spUser.Roles[i].ID].toLowerCase().endsWith("maker")) { //if (spUser.Roles[i].Name.toLowerCase().startsWith("dbs fx") && spUser.Roles[i].Name.toLowerCase().endsWith("maker")) {
                                    viewModel.isDealBUGroup(true);
                                }
                                if (Const_RoleName[spUser.Roles[i].ID].toLowerCase().startsWith("dbs fx") || Const_RoleName[spUser.Roles[i].ID].toLowerCase().startsWith("dbs business")) {//if (spUser.Roles[i].Name.toLowerCase().startsWith("dbs fx") || spUser.Roles[i].Name.toLowerCase().startsWith("dbs business")) {
                                    viewModel.ActivityTitle("FX");
                                    viewModel.FilterIsTMO(null);
                                } else if (Const_RoleName[spUser.Roles[i].ID].toLowerCase().startsWith("dbs tmo")) { //} else if (spUser.Roles[i].Name.toLowerCase().startsWith("dbs tmo")) {
                                    viewModel.ActivityTitle("TMO");
                                    viewModel.FilterIsTMO(true);
                                }
                            }
                        }
                    }
                    viewModel.GetTransactionData();
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    function GetCustomerName() {
        switch (viewModel.ActivityTitle()) {
            case CONST_TRANSACTION.FX:
                return viewModel.TransactionDealDetailModel().Customer.Name();
                break;
            case CONST_TRANSACTION.TMO:
                return viewModel.TransactionTMO().Customer.Name();
                break;
            default:
                ShowMesageDefault();
                break;
        }
    }
    function GetCustomerCIF() {
        switch (viewModel.ActivityTitle()) {
            case CONST_TRANSACTION.FX:
                return viewModel.TransactionDealDetailModel().Customer.CIF();
                break;
            case CONST_TRANSACTION.TMO:
                return viewModel.TransactionTMO().Customer.CIF();
                break;
            default:
                ShowMesageDefault();
                break;
        }
    }
    function ShowMesageDefault() {
        ShowNotification("Information", CONST_MSG.ActivityTitleNotFound, 'gritter-waring', true);
        return;
    }
    function GetCustomerAccount() {
        switch (viewModel.ActivityTitle()) {
            case CONST_TRANSACTION.FX:
                return viewModel.TransactionDealDetailModel().Customer.Accounts();
                break;
            case CONST_TRANSACTION.TMO:
                return viewModel.TransactionTMO().Customer.Accounts();
                break;
            default:
                ShowMesageDefault();
                break;
        }

    }
    //#endregion

    // get Transaction data
    function GetTransactionData() {
        // declare options variable for ajax get request
        var filters = GetFilteredColumns();
        var options = {
            url: api.server + api.url.transactiondeal,
            params: {
                page: self.GridProperties().Page(),
                size: self.GridProperties().Size(),
                sort_column: self.GridProperties().SortColumn(),
                sort_order: self.GridProperties().SortOrder()
            },
            token: accessToken
        };

        // define method GET / POST
        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetTransactionData, OnError, OnAlways);
        }
        else
            // GET
            Helper.Ajax.Get(options, OnSuccessGetTransactionData, OnError, OnAlways);
    }
    function OnSuccessGetTransactionData(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {

            self.TransactionGridModel(data.Rows); //Put the response in ObservableArray

            self.GridProperties().Page(data['Page']);
            self.GridProperties().Size([data['Size']]);
            self.GridProperties().Total(data['Total']);
            self.GridProperties().TotalPages(Math.ceil(self.GridProperties().Total() / self.GridProperties().Size()));


        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }
    function GetDetailTransactionData(TransactionDealID) {
        var endPointURL = GetUrlDetailTransactionData(TransactionDealID);
        // declare options variable for ajax get request
        var options = {
            url: endPointURL,
            token: accessToken
        };
        if (options.url != null) {
            // show progress bar
            $("#transaction-progress").show();
            // hide transaction data
            $("#transaction-data").hide();
            // call ajax
            Helper.Ajax.Get(options, OnSuccessGetDetailTransactionData, OnError, function () {
                // hide progress bar
                $("#transaction-progress").hide();
                // show transaction data
                $("#transaction-data").show();

            });
        }
    }
    function OnSuccessGetDetailTransactionData(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.IsSelectedRow(true);
            var mapping = {
                'ignore': ["LastModifiedDate", "LastModifiedBy"]
            };
            $('.date-picker').datepicker({ autoclose: true }).next().on(ace.click_event, function () {
                $(this).prev().focus();
            });
            self.IsShowUnderlyingDocuments(true);
            self.IsAnnualStatement(data.StatementLetter.ID == CONST_STATEMENT.AnnualStatement_ID);
            switch (self.ActivityTitle()) {
                case CONST_TRANSACTION.FX:
                    if (data.IsTMO == false) {
                        GetPermissionBookDeal();
                        GetPermissionGroup();

                        //GetPermission();
                    } else {
                        self.isDealBUGroup(false);
                        self.IsPermissionBookDeal(false);
                    }
                    self.isNewUnderlying(false);
                    self.IsNewTransaction(false);
                    self.IsJointAccount(true); // set Default value
                    self.AccountNumber(data.AccountNumber != null ? data.AccountNumber : "-");
                    self.IsViewTransaction(data.TransactionStatus == null ? false : true);
                    self.TransactionDealDetailModel(ko.mapping.fromJS(data, {}));
                    self.IsBookDeal(self.TransactionDealDetailModel().IsBookDeal());
                    self.IsShowSubmit(false);
                    DealModel.cif = cifData;
                    self.SetDynamicAccount();
                    self.IsSwapTransaction(false);
                    self.IsNettingTransaction(false);
                    self.Swap_NettingDFNumber('')
                    if (data.SwapType != null && data.SwaptType != 0) {
                        SetSwapTypeTransaction();
                    }
                    if (data.IsNettingTransaction != null && data.IsNettingTransaction == true) {
                        SetNettingTypeTransaction();
                    }
                    self.Selected().DebitCurrency(data.DebitCurrency != null ? data.DebitCurrency.ID : null);
                    var isOtherAccount = viewModel.TransactionDealDetailModel().IsOtherAccountNumber();
                    if (isOtherAccount) {
                        self.IsEmptyAccountNumber(true);
                        self.TransactionDealDetailModel().AccountNumber("-");
                    } else {
                        self.IsEmptyAccountNumber(false);
                        self.TransactionDealDetailModel().AccountNumber(data.AccountNumber);
                    }
                    GetTotalTransaction();


                    self.MakerUnderlyings([]);
                    self.CustomerUnderlyings([]);
                    self.TempSelectedUnderlying([]);
                    if (data.Customer.Underlyings != null) {
                        ko.utils.arrayForEach(data.Customer.Underlyings, function (item) {
                            if (data.IsTMO == false) {
                                if (item.IsEnable == true) {
                                    self.MakerUnderlyings.push(item.ID);
                                    self.TempSelectedUnderlying.push(new SelectUtillizeModel(item.ID, false, item.AvailableAmount, item.StatementLetter.ID));
                                }
                            } else {
                                if (item.IsEnable == true) {
                                    self.MakerUnderlyings.push(item.ID);
                                    self.TempSelectedUnderlying.push(new SelectUtillizeTMOModel(item.ID, false, item.AvailableAmount, item.UtilizeAmountDeal, item.StatementLetter.ID));
                                }
                            }
                        });
                        setTotalUtilize();
                    }
                    // chandra 2015.05.03 : set attachment document
                    self.MakerDocuments([]);
                    self.MakerAllDocumentsTemp([]);
                    self.TransactionDealDetailModel().Documents([]);
                    if (data.ReviseDealDocuments != null) {
                        ko.utils.arrayForEach(data.ReviseDealDocuments, function (item) {
                            item.IsNewDocument = false;
                            self.MakerAllDocumentsTemp.push(item);
                            ko.utils.arrayForEach(self.MakerUnderlyings(), function (item2) {
                                if (item2 == item.UnderlyingID) {
                                    self.MakerDocuments.push(item);
                                    self.TransactionDealDetailModel().Documents.push(item);
                                }
                            });
                            if (item.Purpose.ID != 2) {
                                self.MakerDocuments.push(item);
                                self.TransactionDealDetailModel().Documents.push(item);
                            }
                        });
                    }

                    var t_IsFxTransaction = (viewModel.TransactionDealDetailModel().Currency.Code() != 'IDR' && viewModel.TransactionDealDetailModel().DebitCurrency.Code() == 'IDR');
                    var t_IsFxTransactionToIDR = (viewModel.TransactionDealDetailModel().Currency.Code() == 'IDR' && viewModel.TransactionDealDetailModel().DebitCurrency.Code() != 'IDR');
                    self.t_IsFxTransaction(t_IsFxTransaction);
                    self.t_IsFxTransactionToIDR(t_IsFxTransactionToIDR);
                    break;
                case CONST_TRANSACTION.TMO:
                    self.InstructionDocuments([]);
                    self.TempSelectedInstruction([]);
                    if (data != null) {
                        data.SubmissionDateSL = self.LocalDate(data.SubmissionDateSL, true, false)
                        data.SubmissionDateInstruction = self.LocalDate(data.SubmissionDateInstruction, true, false)
                        data.SubmissionDateUnderlying = self.LocalDate(data.SubmissionDateUnderlying, true, false)
                    }
                    if (data.DocumentTBO == null) {
                        data.DocumentTBO = [];
                    }
                    self.TransactionTMO(ko.mapping.fromJS(data, {}));
                    DealModel.cif = cifData;
                    //GetTotalDeal(DealModel, OnSuccessTotal, OnErrorDeal);
                    self.AccountNumber(data.AccountNumber != null ? data.AccountNumber : "-");
                    self.IsShowSubmit(!(data.TransactionStatus != null || data.IsCanceled == true ? true : false));
                    DealModel.cif = cifData;
                    if (data.IsNettingTransaction != null && data.IsNettingTransaction == true) {
                        self.NettingApplicationID('');
                        SetTMONettingTypeTransaction();
                    }
                    self.IsViewTransaction(data.TransactionStatus != null || data.IsCanceled == true ? true : false);
                    if (data.TransactionStatus == "Completed" || data.TransactionStatus == "FX Deal TMO Checker Approve Task") {
                        self.UnderlyingFilterCurrency(data.BuyCurrency.Code);
                        self.IsViewTransactionCorrection(false);
                        self.IsShowSubmitCorrection(true);
                        self.MakerUnderlyings([]);
                        self.CustomerUnderlyings([]);
                        self.TempSelectedUnderlying([]);
                        if (data.Customer.Underlyings != null) {
                            ko.utils.arrayForEach(data.Customer.Underlyings, function (item) {
                                if (data.IsTMO == false) {
                                    if (item.IsEnable == true) {
                                        self.MakerUnderlyings.push(item.ID);
                                        self.TempSelectedUnderlying.push(new SelectUtillizeModel(item.ID, false, item.AvailableAmount, item.StatementLetter.ID));
                                    }
                                } else {
                                    if (item.IsEnable == true) {
                                        self.MakerUnderlyings.push(item.ID);
                                        self.TempSelectedUnderlying.push(new SelectUtillizeTMOModel(item.ID, false, item.AvailableAmount, item.UtilizeAmountDeal, item.StatementLetter.ID));
                                    }
                                }
                            });
                            setTotalUtilize();
                        }
                    } else {
                        if (data.TransactionStatus == "" || data.TransactionStatus == null) {
                            self.UnderlyingFilterCurrency(data.BuyCurrency.Code);
                            self.IsViewTransactionCorrection(false);
                        } else {
                            self.IsViewTransactionCorrection(true);
                        }
                        self.IsShowSubmitCorrection(false);
                    }
                    if (data.IsIPE == true || data.IsNettingTransaction == true) {
                        self.IsViewTransaction(true);
                        self.IsViewTransactionCorrection(true);
                        self.IsShowSubmit(false);
                        self.IsShowSubmitCorrection(false);
                    }
                    self.SetDynamicAccount();
                    GetTotalTransaction();
                    self.IsSwapTransaction(false);
                    self.IsNettingTransaction(false);
                    self.Swap_NettingDFNumber('')

                    self.Selected().DebitCurrency(data.DebitCurrency != null ? data.DebitCurrency.ID : null);

                    var isOtherAccount = self.TransactionTMO().IsOtherAccountNumber();
                    if (isOtherAccount) {
                        self.IsEmptyAccountNumber(true);
                        self.TransactionTMO().AccountNumber("-");
                    } else {
                        self.IsEmptyAccountNumber(false);
                    }
                    /*var debitCurrencyID = self.TransactionTMO().DebitCurrency.ID();
                    if (data.productType.Code == 'CCS') {
                        self.TransactionTMO().SellCurrency.ID(data.SellCurrency!=null ? data.SellCurrency.ID:null);
                    } else {
                        self.TransactionTMO().SellCurrency.ID(debitCurrencyID);
                    }*/
                    self.TransactionTMO().SellCurrency.ID(data.SellCurrency != null ? data.SellCurrency.ID : debitCurrencyID);
                    // chandra 2015.05.03 : set attachment document
                    self.MakerDocuments([]);
                    self.MakerAllDocumentsTemp([]);
                    self.TransactionTMO().Documents([]);
                    if (data.TransactionStatus == "Completed") {
                        //if (data.Documents != null) {
                        //    ko.utils.arrayForEach(data.Documents, function (item) {
                        //        item.IsNewDocument = false;
                        //        self.MakerDocuments.push(item);
                        //        self.TransactionTMO().Documents.push(item);
                        //    });
                        //}
                        if (data.ReviseDealDocuments != null) {
                            ko.utils.arrayForEach(data.ReviseDealDocuments, function (item) {
                                item.IsNewDocument = false;
                                self.MakerAllDocumentsTemp.push(item);
                                if (data.Customer.Underlyings != null) {
                                    ko.utils.arrayForEach(data.Customer.Underlyings, function (item2) {
                                        if (item2.ID == item.UnderlyingID) {
                                            self.MakerDocuments.push(item);
                                            self.TransactionTMO().Documents.push(item);
                                        }
                                    });
                                }
                                if (item.Purpose.ID != 2) {
                                    self.MakerDocuments.push(item);
                                    self.TransactionTMO().Documents.push(item);
                                }
                            });
                        }
                    }
                    else {
                        if (data.ReviseDealDocuments != null) {
                            ko.utils.arrayForEach(data.ReviseDealDocuments, function (item) {
                                item.IsNewDocument = false;
                                self.MakerAllDocumentsTemp.push(item);
                                if (data.Customer.Underlyings != null) {
                                    ko.utils.arrayForEach(data.Customer.Underlyings, function (item2) {
                                        if (item2.ID == item.UnderlyingID) {
                                            self.MakerDocuments.push(item);
                                            self.TransactionTMO().Documents.push(item);
                                        }
                                    });
                                }
                                if (data.TransactionStatus != null) {
                                    if (item.Purpose.ID != 2) {
                                        self.MakerDocuments.push(item);
                                        self.TransactionTMO().Documents.push(item);
                                    }
                                }
                            });
                        }
                    }

                    if (data.SwapType != null && data.SwaptType != 0) {
                        SetSwapTypeTransaction();
                        if (self.IsViewTransaction()) {
                            self.MakerDocuments([]);
                            self.TransactionTMO().Documents([]);
                            ko.utils.arrayForEach(data.Documents, function (item) {
                                item.IsNewDocument = false;
                                self.MakerDocuments.push(item);
                                self.TransactionTMO().Documents.push(item);
                            });
                        }
                    }
                    if (data.IsNettingTransaction != null && data.IsNettingTransaction == true) {
                        SetNettingTypeTransaction();
                    }
                    var t_IsFxTransaction = (viewModel.TransactionTMO().BuyCurrency.Code() != 'IDR' && viewModel.TransactionTMO().SellCurrency.Code() == 'IDR');
                    var t_IsFxTransactionToIDR = (viewModel.TransactionTMO().BuyCurrency.Code() == 'IDR' && viewModel.TransactionTMO().SellCurrency.Code() != 'IDR');
                    self.t_IsFxTransaction(t_IsFxTransaction);
                    self.t_IsFxTransactionToIDR(t_IsFxTransactionToIDR);
                    break;
            }
            var accountNumber = GetModelTransaction().Account.AccountNumber() != null && GetModelTransaction().Account.AccountNumber() != "";
            if (accountNumber && GetModelTransaction().IsJointAccount()) {
                viewModel.UnderlyingFilterAccountNumber(GetModelTransaction().Account.AccountNumber());
                viewModel.UnderlyingFilterShow(2); // show data only joint account
                viewModel.GetDataUnderlying();
            } else {
                viewModel.UnderlyingFilterAccountNumber("");
                viewModel.UnderlyingFilterShow(1); // show data single account
                viewModel.GetDataUnderlying();
            }
            //UnCheckUnderlyingTable();
            //viewModel.GetDataUnderlying();
            self.IsSelectedRow(false);
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
        }
    }
    function ResetDataAttachment(index, isSelect) {
        self.CustomerAttachUnderlyings()[index].IsSelectedAttach = isSelect;
        var dataRows = self.CustomerAttachUnderlyings().slice(0);
        self.CustomerAttachUnderlyings([]);
        self.CustomerAttachUnderlyings(dataRows);
    }
    //Tambah Agung
    function ResetDataInstruction(index, isSelect) {
        self.InstructionDocuments()[index].IsSelected = isSelect;
        var dataRows = self.InstructionDocuments().slice(0);
        self.InstructionDocuments([]);
        self.InstructionDocuments(dataRows);
    }
    //End
    function ResetDataProforma(index, isSelect) {
        self.CustomerUnderlyingProformas()[index].IsSelectedProforma = isSelect;
        var dataRows = self.CustomerUnderlyingProformas().slice(0);
        self.CustomerUnderlyingProformas([]);
        self.CustomerUnderlyingProformas(dataRows);
    }
    function ResetBulkData(index, isSelect) { //add 2015.03.09
        self.CustomerBulkUnderlyings()[index].IsSelectedBulk = isSelect;
        var dataRows = self.CustomerBulkUnderlyings().slice(0);
        self.CustomerBulkUnderlyings([]);
        self.CustomerBulkUnderlyings(dataRows);
    }
    function ResetDataUnderlying(index, isSelect) {
        self.CustomerUnderlyings()[index].IsEnable = isSelect;
        var itemdeal = $('#Position' + index).val();
        switch (self.ActivityTitle()) {
            case CONST_TRANSACTION.TMO:
                if (itemdeal != undefined) {
                    self.CustomerUnderlyings()[index].UtilizeAmountDeal = itemdeal;
                }
                break;
        }
        var dataRows = self.CustomerUnderlyings().slice(0);
        self.CustomerUnderlyings([]);
        if (dataRows[0].UtilizeAmountDeal == "NaN") {
            dataRows[0].UtilizeAmountDeal = formatNumber(dataRows[0].AvailableAmountDeal);
        }
        self.CustomerUnderlyings(dataRows);
    }
    function UnCheckDataUnderlying(obj1) {
        self.MakerUnderlyings([]);
        self.TempSelectedUnderlying([]);
        var dataRows = [];
        ko.utils.arrayForEach(self.CustomerUnderlyings(), function (item) {
            item.IsEnable = false;
            dataRows.push(item);
        });
        self.CustomerUnderlyings([]);
        self.CustomerUnderlyings(dataRows);
        // Remove Underlying Document
        switch (self.ActivityTitle()) {
            case CONST_TRANSACTION.FX:
                ko.utils.arrayForEach(self.MakerAllDocumentsTemp(), function (item) {
                    if (item.Purpose.ID == 2) {
                        self.MakerDocuments.remove(item);
                        self.TransactionDealDetailModel().Documents(jQuery.grep(self.TransactionDealDetailModel().Documents(), function (value) {
                            return value.ID != item.ID;
                        }));
                    }
                });
                break;
            case CONST_TRANSACTION.TMO:
                ko.utils.arrayForEach(self.MakerAllDocumentsTemp(), function (item) {
                    if (item.Purpose.ID == 2) {
                        self.MakerDocuments.remove(item);
                        self.TransactionTMO().Documents(jQuery.grep(self.TransactionTMO().Documents(), function (value) {
                            return value.ID != item.ID;
                        }));
                    }
                });
                break;
        }
    }
    function GetUnderlyingDocName(underlyingID) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.underlyingdoc + "/" + underlyingID,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    if (data != null) {

                        self.isNewUnderlying(false); //return default disabled

                        // available type to input for new underlying name if code is 999
                        if (data.Code == '999') {
                            switch (self.ActivityTitle()) {
                                case CONST_TRANSACTION.FX:
                                    //self.TransactionDealDetailModel().bookunderlyingdesc('');
                                    self.isNewUnderlying(true);
                                    break;
                                case CONST_TRANSACTION.TMO:
                                    //self.TransactionTMO().UnderlyingCodeID('');
                                    self.isNewUnderlying(true);
                                    break;
                            }
                        }
                        else {
                            switch (self.ActivityTitle()) {
                                case CONST_TRANSACTION.FX:
                                    self.isNewUnderlying(false);
                                    //self.TransactionDealDetailModel().bookunderlyingdesc('');
                                    break;
                                case CONST_TRANSACTION.TMO:
                                    self.isNewUnderlying(false);
                                    //self.TransactionTMO().UnderlyingCodeID('');
                                    break;
                            }

                        }
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    function SetSelectedProforma(data) {
        // if(self.TempSelectedUnderlyingProforma().length>0) {
        for (var i = 0 ; i < data.length; i++) {
            var selected = ko.utils.arrayFirst(self.TempSelectedUnderlyingProforma(), function (item) {
                return item.ID == data[i].ID;
            });
            if (selected != null) {
                data[i].IsSelectedProforma = true;
            }
            else {
                data[i].IsSelectedProforma = false;
            }
        }
    }
    function SetSelectedBulk(data) { //add 2015.03.09

        for (var i = 0 ; i < data.length; i++) {
            var selected = ko.utils.arrayFirst(self.TempSelectedUnderlyingBulk(), function (item) {
                return item.ID == data[i].ID;
            });
            if (selected != null) {
                data[i].IsSelectedBulk = true;
            }
            else {
                data[i].IsSelectedBulk = false;
            }
        }
    }
    function SetDefaultValueStatementA() {
        var today = Date.now();
        var documentType = ko.utils.arrayFirst(self.ddlDocumentType_u(), function (item) { return item.Name == '-' });
        var underlyingDocument = ko.utils.arrayFirst(self.ddlUnderlyingDocument_u(), function (item) { return item.Code() == 998 });

        if (documentType != null) {
            self.DocumentType_u(new DocumentTypeModel2(documentType.ID, documentType.Name));
        }

        if (underlyingDocument != null) {
            self.UnderlyingDocument_u(new UnderlyingDocumentModel2(underlyingDocument.ID(), underlyingDocument.Name(), underlyingDocument.Code()));
        }

        self.DateOfUnderlying_u(self.LocalDate(today, true, false));
        self.SupplierName_u('-');
        self.InvoiceNumber_u('-');
    }
    function SetSelectedUnderlying(data) {
        for (var i = 0; i < data.length; i++) {
            var selected = ko.utils.arrayFirst(self.TempSelectedUnderlying(), function (item) {
                return item.ID() == data[i].ID;
            });
            if (selected != null) {
                data[i].IsEnable = true;
                data[i].UtilizeAmountDeal = formatNumber(selected.UtilizeAmountDeal());
            }
            else {
                data[i].IsEnable = false;
            }
        }
        return data;
    }
    //tambah Agung
    function SetSelectedInstruction(data) {
        for (var i = 0; i < data.length; i++) {
            var selected = ko.utils.arrayFirst(self.TempSelectedInstruction(), function (item) {
                return item.ID == data[i].ID;
            });
            if (selected != null) {
                data[i].IsSelected = true;
            }
            else {
                data[i].IsSelected = false;
            }
        }
        return data;
    }
    //End
    function SetDynamicAccount() {
        $.ajax({
            type: "GET",
            url: api.server + api.url.customerAccount + "/" + cifData,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    switch (self.ActivityTitle()) {
                        case CONST_TRANSACTION.FX:
                            self.TransactionDealDetailModel().Customer.Accounts = ko.mapping.fromJS(data);
                            var dataAccount = ko.utils.arrayFilter(self.TransactionDealDetailModel().Customer.Accounts(), function (item) {
                                var sAccount = item.IsJointAccount() == null ? false : item.IsJointAccount();
                                var debitCurrency = viewModel.TransactionDealDetailModel().DebitCurrency != null ? viewModel.TransactionDealDetailModel().DebitCurrency.Code() : "";
                                return sAccount == self.TransactionDealDetailModel().IsJointAccount() && debitCurrency == item.Currency.Code();
                            });
                            if (!self.TransactionDealDetailModel().IsJointAccount()) {
                                dataAccount = AddOtherAccounts(dataAccount);
                            }
                            self.DynamicAccounts(dataAccount);


                            var isJointAcc = ko.utils.arrayFilter(data, function (item) {
                                return true == item.IsJointAccount;
                            });
                            if (isJointAcc != null & isJointAcc.length == 0) {
                                self.IsJointAccount(false);
                            }
                            var jointAccount = ko.utils.arrayFilter(data, function (item) {
                                return true == item.IsJointAccount;
                            });
                            if (jointAccount != null && jointAccount.length > 0) {
                                self.JointAccountNumbers(jointAccount); // set drowpdownlist joint account underlying
                            }
                            self.TransactionDealDetailModel().AccountNumber(self.AccountNumber());
                            break;
                        case CONST_TRANSACTION.TMO:
                            self.TransactionTMO().Customer.Accounts = ko.mapping.fromJS(data);
                            var dataAccount = ko.utils.arrayFilter(self.TransactionTMO().Customer.Accounts(), function (item) {
                                var sAccount = item.IsJointAccount() == null ? false : item.IsJointAccount();
                                var debitCurrency = viewModel.TransactionTMO().SellCurrency != null ? viewModel.TransactionTMO().SellCurrency.Code() : (viewModel.TransactionTMO().DebitCurrency != null ? viewModel.TransactionTMO().DebitCurrency.Code() : "");
                                return sAccount == self.TransactionTMO().IsJointAccount() && debitCurrency == item.Currency.Code();
                            });
                            if (!self.TransactionTMO().IsJointAccount()) {
                                dataAccount = AddOtherAccounts(dataAccount);
                            }
                            self.DynamicAccounts(dataAccount);

                            //check customer have joint account
                            var isJointAcc = ko.utils.arrayFilter(data, function (item) {
                                return true == item.IsJointAccount;
                            });
                            if (isJointAcc != null & isJointAcc.length == 0) {
                                self.IsJointAccount(false);
                            }
                            self.TransactionTMO().AccountNumber(self.AccountNumber());
                            break;
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }
    function SetSwapTypeTransaction() {
        var swap = GetModelTransaction().SwapType();
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/GetSwapDataTransaction/" + GetModelTransaction().NB() + "/" + GetModelTransaction().TZReference(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    if (data != null) {
                        self.IsSwapTransaction(true);
                        self.MessageTransactionType('Swap transaction');
                        self.Swap_NettingDFNumber(data.DFNumber);
                        GetModelTransaction().SwapTransactionID(data.SwapTransactionID);
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });

    }

    function SetNettingTypeTransaction() {
        var isNetting = GetModelTransaction().IsNettingTransaction();
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/GetNettingDataTransaction/" + GetModelTransaction().TZReference(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    if (data != null) {
                        self.IsNettingTransaction(true);
                        self.MessageTransactionType('Netting transaction');
                        self.Swap_NettingDFNumber(data.DFNumber);
                        //GetModelTransaction().Transaction.SwapTransactionID(data.SwapTransactionID);
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });

    }

    function SetTMONettingTypeTransaction() {
        var isNetting = GetModelTransaction().IsNettingTransaction();
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/GetTMONettingTransaction/" + GetModelTransaction().TZReference(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    if (data != null) {
                        self.IsNettingTransaction(true);
                        self.MessageNettingTransactionType('Netting transaction');
                        self.NettingApplicationID(data.DFNumber);
                        //GetModelTransaction().Transaction.SwapTransactionID(data.SwapTransactionID);
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });

    }

    function TotalUtilize() {
        var total = 0;
        ko.utils.arrayForEach(self.TempSelectedUnderlying(), function (item) {

            total += parseFloat(ko.utils.unwrapObservable(item.USDAmount()));
        });
        var fixTotal = parseFloat(total).toFixed(2);
        return fixTotal;
    }
    function GetRateIDRAmount(CurrencyID) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.currency + "/CurrencyRate/" + CurrencyID,
            params: {
            },
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status == 200) {
                    self.Rate_u(data.RupiahRate);
                    //if(CurrencyID!=1){ // if not USD

                    var fromFormat = document.getElementById("Amount_u").value;
                    fromFormat = fromFormat.toString().replace(/,/g, '');
                    res = parseFloat(fromFormat) * data.RupiahRate / idrrate;
                    res = Math.round(res * 100) / 100;
                    res = isNaN(res) ? 0 : res; //avoid NaN
                    self.AmountUSD_u(parseFloat(res).toFixed(2));
                    self.Amount_u(formatNumber(fromFormat));
                }
            }
        });
    }
    function GetUSDAmount(currencyID, amount, IdUnderlying) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/GetAmountUSD/" + currencyID + "/" + amount,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {

                    self.TempSelectedUnderlying.push(new SelectUtillizeModel(IdUnderlying, false, data));
                    var total = TotalUtilize();
                    viewModel.utilizationAmount(total);
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }
    function SetTMODocuments() {
        ko.utils.arrayForEach(self.MakerAllDocumentsTemp(), function (item) {
            if (item.Purpose.ID == 2) {
                self.MakerDocuments.remove(item);
                var dataDoc = jQuery.grep(self.TransactionTMO().Documents, function (value) {
                    return value.ID != item.ID;
                });
                self.TransactionTMO().Documents(dataDoc);
            }
        });
        ko.utils.arrayForEach(self.MakerAllDocumentsTemp(), function (item) {
            ko.utils.arrayForEach(self.MakerUnderlyings(), function (item2) {
                if (item2 == item.UnderlyingID && item.Purpose.ID == 2) {
                    self.MakerDocuments.push(item);
                    self.TransactionTMO().Documents.push(item);
                }
            });
        });
    }
    function SetFXDocuments() {
        // remove all underlying documents
        ko.utils.arrayForEach(self.MakerAllDocumentsTemp(), function (item) {
            if (item.Purpose.ID == 2) {
                self.MakerDocuments.remove(item);
                var dataDoc = jQuery.grep(self.TransactionDealDetailModel().Documents, function (value) {
                    return value.ID != item.ID;
                });
                self.TransactionDealDetailModel().Documents(dataDoc);
            }
        });
        // push underlying by makerunderlyings
        ko.utils.arrayForEach(self.MakerAllDocumentsTemp(), function (item) {
            ko.utils.arrayForEach(self.MakerUnderlyings(), function (item2) {
                if (item2 == item.UnderlyingID && item.Purpose.ID == 2) {
                    var IsdocumentAlready = ko.utils.arrayFirst(self.MakerDocuments(), function (document) { return document.ID == item.ID });
                    if (IsdocumentAlready == null) {
                        self.MakerDocuments.push(item);
                        self.TransactionDealDetailModel().Documents.push(item);
                    }
                }
            });
        });
    }
    function GetModelTransaction() {
        var model;
        switch (viewModel.ActivityTitle()) {
            case CONST_TRANSACTION.FX:
                model = viewModel.TransactionDealDetailModel();
                break;
            case CONST_TRANSACTION.TMO:
                model = viewModel.TransactionTMO();
                break;
            default:
                break;
        }
        return model;
    }
    function CalculateFX(amountUSD) {
        var statementLetterID = GetStatementLetterID();
        var isFlowValas = GetFlowValas();
        var t_TotalAmountsUSD = TotalDealModel.Total.IDRFCYDeal;
        var t_RemainingBalance = TotalDealModel.Total.RemainingBalance;
        viewModel.AmountModel().TotalUtilization(TotalDealModel.Total.UtilizationDeal);
        viewModel.FCYIDRTresHold(TotalDealModel.FCYIDRTrashHold);
        var t_Treshold = TotalDealModel.TreshHold;
        var t_FCYTreshold = TotalDealModel.FCYIDRTrashHold;
        var t_FcyAmount = parseFloat(amountUSD);
        var t_IsFxTransaction = viewModel.t_IsFxTransaction();
        var t_IsFxTransactionToIDR = viewModel.t_IsFxTransactionToIDR();
        if (t_IsFxTransactionToIDR) {
            if (t_FcyAmount >= t_FCYTreshold && isFlowValas) {
                viewModel.Selected().StatementLetter(CONST_STATEMENT.StatementB_ID);
                t_TotalAmountsUSD = parseFloat(t_TotalAmountsUSD);
                t_TotalAmountsUSD = t_TotalAmountsUSD.toFixed(2);

            } else {
                viewModel.Selected().StatementLetter(CONST_STATEMENT.Dash_ID);
            }
        }
        if (t_IsFxTransaction) {
            t_TotalAmountsUSD = parseFloat(t_TotalAmountsUSD);
            t_TotalAmountsUSD = t_TotalAmountsUSD.toFixed(2);

        }
        if (viewModel.statementLetterID != undefined) {
            if (viewModel.statementLetterID == CONST_STATEMENT.StatementA_ID) {
                if (t_TotalAmountsUSD <= t_Treshold) {
                    t_RemainingBalance = 0.00;
                }
            } else {
                if (amountUSD != null) {
                    t_RemainingBalance = parseFloat(t_RemainingBalance);
                } else {
                    t_RemainingBalance = t_RemainingBalance
                }
            }
            if (t_IsFxTransactionToIDR) {
                if (t_FcyAmount >= t_FCYTreshold && isFlowValas) {
                    if (t_RemainingBalance == 0.00) {
                        t_RemainingBalance = t_RemainingBalance;
                    }
                }
            }
            t_RemainingBalance = parseFloat(t_RemainingBalance.toFixed(2));
        }
        viewModel.AmountModel().TotalAmountsUSD(t_TotalAmountsUSD);
        viewModel.AmountModel().RemainingBalance(t_RemainingBalance);
    }
    function onSuccessRemainingBlnc() {
        var amountUSD = GetAmountUSD();
        CalculateFX(amountUSD);
    }
    function SetMappingAttachment(customerName) {
        CustomerUnderlyingFile.CustomerUnderlyingMappings([]);
        for (var i = 0 ; i < self.TempSelectedAttachUnderlying().length; i++) {
            var sValue = {
                ID: 0,
                UnderlyingID: self.TempSelectedAttachUnderlying()[i].ID,
                UnderlyingFileID: 0,
                AttachmentNo: customerName + '-(A)'
            }
            CustomerUnderlyingFile.CustomerUnderlyingMappings.push(sValue);
        }
        /* CustomerUnderlyingFile.CustomerUnderlyingMappings([]);
         for (var i = 0 ; i < self.TempSelectedAttachUnderlying().length; i++) {
             CustomerUnderlyingFile.CustomerUnderlyingMappings.push(new CustomerUnderlyingMappingModel(0, self.TempSelectedAttachUnderlying()[i].ID, customerNameData + '-(A)'));
         }*/
    }
    function SetMappingProformas() {
        CustomerUnderlying.Proformas([]);
        for (var i = 0 ; i < self.TempSelectedUnderlyingProforma().length; i++) {
            CustomerUnderlying.Proformas.push(new SelectModel(cifData, self.TempSelectedUnderlyingProforma()[i].ID));
        }
    }
    function SetMappingBulks() { //add 2015.03.09
        //console.log(CustomerUnderlying.BulkUnderlyings());
        CustomerUnderlying.BulkUnderlyings([]);
        for (var i = 0 ; i < self.TempSelectedUnderlyingBulk().length; i++) {
            CustomerUnderlying.BulkUnderlyings.push(new SelectBulkModel(cifData, self.TempSelectedUnderlyingBulk()[i].ID));
        }
    }
    function setTotalUtilize() {
        var total = 0;
        switch (self.ActivityTitle()) {
            case CONST_TRANSACTION.FX:
                ko.utils.arrayForEach(self.TempSelectedUnderlying(), function (item) {
                    total += parseFloat(ko.utils.unwrapObservable(item.USDAmount()));
                });
                break;
            case CONST_TRANSACTION.TMO:
                ko.utils.arrayForEach(self.TempSelectedUnderlying(), function (item) {
                    total += parseFloat(ko.utils.unwrapObservable(item.UtilizeAmountDeal()));
                });
                break;
        }
        viewModel.utilizationAmount(parseFloat(total).toFixed(2));
        var sRounding = GetRounding(parseFloat(total), self.ParameterRounding());
        viewModel.Rounding(sRounding);
    }
    function GetRounding(totalUnderlying, parameterRounding) {
        if (totalUnderlying != null && parameterRounding != null) {
            var rounding = totalUnderlying % parameterRounding;
            if (rounding != null && rounding != 0) {
                return rounding = parseFloat(parameterRounding - rounding).toFixed(2);
            }
        }
        return 0;
    }
    function setStatementA() {
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/GetStatementA/" + cifData,
            data: null, //Convert the Observable Data into JSON
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + accessToken
            }, success: function (data) {
                if (data.IsStatementA) {
                    self.StatementLetter_u(new StatementLetterModel(CONST_STATEMENT.StatementB_ID, 'Statement B'));
                    self.IsStatementA(false);
                } else { self.IsStatementA(true); }
            }
        });
    }
    function setRefID() {
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/GetRefID/" + cifData,
            data: null, //Convert the Observable Data into JSON
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + accessToken
            }, success: function (data) {
                self.ReferenceNumber_u(customerNameData + '-' + data.ID);
            }
        });

    }
    function SaveDataFile() {
        var transactionID = GetTransactionID();
        var customerName = GetCustomerName();
        SetMappingAttachment(customerName);
        CustomerUnderlyingFile.ID = transactionID;
        CustomerUnderlyingFile.FileName = DocumentModels().FileName;
        CustomerUnderlyingFile.DocumentPath = DocumentModels().DocumentPath;
        CustomerUnderlyingFile.DocumentType = ko.utils.arrayFirst(self.ddlDocumentType_a(), function (item) {
            return item.ID == self.DocumentType_a().ID();
        });
        CustomerUnderlyingFile.DocumentPurpose = ko.utils.arrayFirst(self.ddlDocumentPurpose_a(), function (item) {
            return item.ID == self.DocumentPurpose_a().ID();
        });
        $.ajax({
            type: "POST",
            //url: api.server + api.url.customerunderlyingfile,
            url: api.server + api.url.customerunderlyingfile + "/AddFile",
            data: ko.toJSON(CustomerUnderlyingFile), //Convert the Observable Data into JSON
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    viewModel.IsEditable(true);
                    // send notification
                    ShowNotification('Create Success', jqXHR.responseJSON.Message, 'gritter-success', false);
                    // hide current popup window
                    $("#modal-form-Attach").modal('hide');
                    $('#backDrop').hide();
                    viewModel.IsUploading(false);
                    var doc = {
                        ID: data,
                        Type: CustomerUnderlyingFile.DocumentType,
                        Purpose: CustomerUnderlyingFile.DocumentPurpose,
                        FileName: CustomerUnderlyingFile.FileName,
                        DocumentPath: CustomerUnderlyingFile.DocumentPath,
                        IsNewDocument: true,
                        LastModifiedDate: new Date(),
                        LastModifiedBy: null,
                        UnderlyingID: 0
                    };
                    if (doc.Purpose.ID == 2) { // data underlying
                        // Set All Document Transaction
                        //self.TempSelectedUnderlying([]);
                        self.MakerUnderlyings([]);
                        switch (self.ActivityTitle()) {
                            case CONST_TRANSACTION.FX:
                                ko.utils.arrayForEach(self.MakerAllDocumentsTemp(), function (item) {
                                    if (item.Purpose.ID == 2) {
                                        self.MakerDocuments.remove(item);
                                        self.TransactionDealDetailModel().Documents(jQuery.grep(self.TransactionDealDetailModel().Documents(), function (value) {
                                            return value.ID != item.ID;
                                        }));
                                    }
                                });
                                break;
                            case CONST_TRANSACTION.TMO:
                                ko.utils.arrayForEach(self.MakerAllDocumentsTemp(), function (item) {
                                    if (item.Purpose.ID == 2) {
                                        self.MakerDocuments.remove(item);
                                        self.TransactionTMO().Documents(jQuery.grep(self.TransactionTMO().Documents(), function (value) {
                                            return value.ID != item.ID;
                                        }));
                                    }
                                });
                                break;
                        }
                        ko.utils.arrayForEach(self.TempSelectedAttachUnderlying(), function (itemTemp) {
                            var docUnderlying = {
                                ID: data,
                                Type: CustomerUnderlyingFile.DocumentType,
                                Purpose: CustomerUnderlyingFile.DocumentPurpose,
                                FileName: CustomerUnderlyingFile.FileName,
                                DocumentPath: CustomerUnderlyingFile.DocumentPath,
                                IsNewDocument: false,
                                LastModifiedDate: new Date(),
                                LastModifiedBy: null,
                                UnderlyingID: itemTemp.ID
                            };
                            self.MakerAllDocumentsTemp.push(docUnderlying);
                            var IsdocumentAlready = ko.utils.arrayFirst(self.MakerDocuments(), function (document) { return document.ID == docUnderlying.ID });
                            if (IsdocumentAlready == null) {
                                self.MakerDocuments.push(docUnderlying);
                                // set checked underlying
                                ko.utils.arrayForEach(self.CustomerUnderlyings(), function (underlyingItem) {
                                    if (underlyingItem.ID == itemTemp.ID) {
                                        switch (self.ActivityTitle()) {
                                            case CONST_TRANSACTION.FX:
                                                self.TempSelectedUnderlying.push(new SelectUtillizeModel(underlyingItem.ID, true, underlyingItem.AvailableAmount, underlyingItem.StatementLetter.ID));
                                                break;
                                            case CONST_TRANSACTION.TMO:
                                                self.TempSelectedUnderlying.push(new SelectUtillizeTMOModel(underlyingItem.ID, true, underlyingItem.AvailableAmount, parseFloat(underlyingItem.UtilizeAmountDeal.replace(/,/g, '')), underlyingItem.StatementLetter.ID));
                                                break;
                                        }
                                        self.MakerUnderlyings.push(underlyingItem.ID);
                                    }
                                });
                            }
                        });
                        setTotalUtilize();
                    } else {
                        self.MakerDocuments.push(doc);
                        self.MakerAllDocumentsTemp.push(doc);

                    }
                    self.GetDataUnderlying();
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                    viewModel.IsEditable(true);
                    viewModel.IsUploading(false);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                viewModel.IsEditable(true);
                viewModel.IsUploading(false);
            }
        });


    }
    // Function to Read parameter Underlying
    function GetUnderlyingParameters(data) {

        var underlying = data['UnderltyingDoc'];
        for (var i = 0; i < underlying.length; i++) {
            self.ddlUnderlyingDocument_u.push(new UnderlyingDocumentModel2(underlying[i].ID, underlying[i].Name, underlying[i].Code));
        }

        self.ddlDocumentType_u(data['DocType']);
        self.ddlDocumentType_a(data['DocType']);
        ko.utils.arrayForEach(data['Currency'], function (item) {
            self.ddlCurrency_u.push(new CurrencyModel2(item.ID, item.Code, item.Description));
        });
        self.ddlDocumentPurpose_a(data['PurposeDoc']);
        self.ddlStatementLetter(data['StatementLetter']);
        ko.utils.arrayForEach(data['StatementLetter'], function (item) {
            if (item.ID == CONST_STATEMENT.StatementA_ID || item.ID == CONST_STATEMENT.StatementB_ID) {
                self.ddlStatementLetter_u.push(item);
            }
        });
        ko.utils.arrayForEach(data['PurposeDoc'], function (item) {
            if (item.ID != 2) { //tanpa underlying
                self.ddlDocumentPurposeNoFx.push(item);
            }
        });
    }
    //Function to Read All Customers Underlying
    function GetDataUnderlying() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        var transactionDealID = GetModelTransaction().ID();

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlying + "/Underlying",
            //url: api.server + api.url.customerunderlying + "/UnionTransactionDeal/" + transactionDealID,
            params: {
                page: self.UnderlyingGridProperties().Page(),
                size: self.UnderlyingGridProperties().Size(),
                sort_column: self.UnderlyingGridProperties().SortColumn(),
                sort_order: self.UnderlyingGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        self.UnderlyingFilterIsAvailable(true);
        self.UnderlyingFilterIsExpiredDate(true);
        var filters = GetUnderlyingFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataUnderlying, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataUnderlying, OnError, OnAlways);
        }
    }
    //Function to Read All Customers Underlying File
    function GetDataAttachFile() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlyingfile,
            params: {
                page: self.AttachGridProperties().Page(),
                size: self.AttachGridProperties().Size(),
                sort_column: self.AttachGridProperties().SortColumn(),
                sort_order: self.AttachGridProperties().SortOrder()
            },
            token: accessToken
        };
        self.AttachFilterAccountNumber(self.UnderlyingFilterAccountNumber());
        // get filtered columns
        var filters = GetAttachFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetDataAttachFile, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetDataAttachFile, OnError, OnAlways);
        }
    }
    //Tambah Agung
    function GetDataInstructionDocument() {
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });

        var options = {
            url: api.server + api.url.workflow.instructiondocumenttmo(viewModel.TransactionTMO().CIF()),
            params: {
                page: self.InstructionGridProperties().Page(),
                size: self.InstructionGridProperties().Size(),
                sort_column: self.InstructionGridProperties().SortColumn(),
                sort_order: self.InstructionGridProperties().SortOrder()
            },
            token: accessToken
        };
        var filters = GetInstructionFilteredColumns();
        if (filters.length > 0) {
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetInstructionData, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetInstructionData, OnError, OnAlways);
        }
    }

    function OnSuccessGetInstructionData(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            var dataInstruction = SetSelectedInstruction(data.Rows);
            self.InstructionDocuments([]);
            self.InstructionDocuments(dataInstruction);
            self.InstructionGridProperties().Page(data['Page']);
            self.InstructionGridProperties().Size(data['Size']);
            self.InstructionGridProperties().Total(data['Total']);
            self.InstructionGridProperties().TotalPages(Math.ceil(self.InstructionGridProperties().Total() / self.InstructionGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }
    //End

    //Function to Read All Customers Underlying for Attach File
    function GetDataUnderlyingAttach() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlying + "/Attach",
            params: {
                page: self.UnderlyingAttachGridProperties().Page(),
                size: self.UnderlyingAttachGridProperties().Size(),
                sort_column: self.UnderlyingAttachGridProperties().SortColumn(),
                sort_order: self.UnderlyingAttachGridProperties().SortOrder()
            },
            token: accessToken
        };
        //self.UnderlyingAttachFilterIsTMO(self.FilterIsTMO());
        self.UnderlyingAttachFilterAccountNumber(self.UnderlyingFilterAccountNumber());
        // get filtered columns
        var filters = GetUnderlyingAttachFilteredColumns();
        //var filters = self.MakerUnderlyings();
        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetUnderlyingDataAttachFile, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetUnderlyingDataAttachFile, OnError, OnAlways);
        }
    }
    //Function to Read All Customers Underlying Proforma
    function GetDataUnderlyingProforma() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlying + "/Proforma",
            params: {
                page: self.UnderlyingProformaGridProperties().Page(),
                size: self.UnderlyingProformaGridProperties().Size(),
                sort_column: self.UnderlyingProformaGridProperties().SortColumn(),
                sort_order: self.UnderlyingProformaGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns
        var filters = GetUnderlyingProformaFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetUnderlyingProforma, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetUnderlyingProforma, OnError, OnAlways);
        }
    }
    //Function to Read All Customers Bulk Underlying // add 2015.03.09
    function ReSetTotalUnderlying() {
        self.TempSelectedUnderlying([]);
        ko.utils.arrayForEach(self.CustomerUnderlyings(), function (underlyingItem) {
            if (underlyingItem.IsEnable == true) {
                switch (self.ActivityTitle()) {
                    case CONST_TRANSACTION.FX:
                        self.TempSelectedUnderlying.push(new SelectUtillizeModel(underlyingItem.ID, true, underlyingItem.AvailableAmount, underlyingItem.StatementLetter.ID))
                        break;
                    case CONST_TRANSACTION.TMO:
                        self.TempSelectedUnderlying.push(new SelectUtillizeTMOModel(underlyingItem.ID, true, underlyingItem.AvailableAmount, parseFloat(underlyingItem.UtilizeAmountDeal.replace(/,/g, '')), underlyingItem.StatementLetter.ID))
                        break;
                }
            }
        });
        setTotalUtilize();
    }

    //#region PBI Threshold 
    function GetThreshold(isSubmit) {
        var paramhreshold = GetValueParamterThreshold();

        var options = {
            url: api.server + api.url.helper + "/GetThresholdFXValue",
            token: accessToken,
            params: {},
            data: ko.toJSON(paramhreshold)
        };
        Helper.Ajax.Post(options, function (data, textStatus, jqXHR) {
            if (jqXHR.status == 200) {
                //viewModel.ThresholdValue(ko.mapping.fromJS(data));
                var amountUSD = GetModelTransaction().AmountUSD();
                switch (self.ActivityTitle()) {
                    case CONST_TRANSACTION.FX:
                        if (IsValidationFX(data, parseFloat(amountUSD))) {
                            if (isSubmit) {
                                UploadDocuments();
                            } else {
                                UpdateTransaction();
                            }
                        }
                        break;
                    case CONST_TRANSACTION.TMO:
                        if (IsValidationTMO(data, parseFloat(amountUSD))) {
                            UploadDocuments();
                        }
                        break;
                }
            }
        }, OnError);
    }
    function IsValidationFX(data, amountUSD) {
        //var isValid = viewModel.TransactionTMO().IsDraft();
        var IsStatus = true;
        var totalUnderlying = GetTotalUnderlyingRounding(self.Rounding());
        var statementLetterID = viewModel.TempSelectedUnderlying().length > 0 ? viewModel.TempSelectedUnderlying()[0].StatementLetter() : GetModelTransaction().StatementLetter.ID();

        if (self.IsNoThresholdValue() && statementLetterID != CONST_STATEMENT.Dash_ID) {
            viewModel.IsEditable(true);
            ShowNotification("Form Validation Warning", "Transaction does not have threshold, Please Select (-) Statement Letter.", 'gritter-warning', false);
            return false;
        }
        if (!self.IsNoThresholdValue() && statementLetterID == CONST_STATEMENT.Dash_ID) {
            viewModel.IsEditable(true);
            ShowNotification("Form Validation Warning", "Please Select Statement Letter.", 'gritter-warning', false);
            return false;
        }
        if (self.IsDeal()) {
            IsStatus = true;
            return IsStatus;
        }
        if ((self.TempSelectedUnderlying() == null || (self.TempSelectedUnderlying() != null && self.TempSelectedUnderlying().length == 0)) && !self.IsNoThresholdValue()) {
            ShowNotification("Form Underlying Warning", "Please select utilize underlying data.", 'gritter-warning', false);
            self.IsEditable(true);
            self.IsUploaded(true);
            return false;
        }
        if (statementLetterID == 1 && data.IsHitThreshold) {
            ShowNotification("Form Validation Warning", "Total Amount Statement A hit " + data.ThresholdValue + ", Please Select Statement B  ", 'gritter-warning', false);
            self.IsEditable(true);
            self.IsUploaded(true);
            IsStatus = false;
            return IsStatus;
        }
        if (!self.IsNoThresholdValue() && viewModel.TempSelectedUnderlying().length > 0 && viewModel.TempSelectedUnderlying()[0].StatementLetter() != GetModelTransaction().StatementLetter.ID()) {
            var errorMessage;
            switch (GetModelTransaction().StatementLetter.ID()) {
                case CONST_STATEMENT.StatementA_ID:
                    errorMessage = "Please select utilize underlying Statement A.";
                    break;
                case CONST_STATEMENT.StatementB_ID:
                    errorMessage = "Please select utilize underlying Statement B.";
                    break;
                case CONST_STATEMENT.AnnualStatement_ID:
                    errorMessage = "Please select utilize underlying Annual Statement Letter.";
                    break;
                case CONST_STATEMENT.Dash_ID:
                    errorMessage = "Please select statement letter or untick utilization underlying.";
                    break;
            }
            self.IsEditable(true);
            self.IsUploaded(true);
            ShowNotification("Form Underlying Warning", errorMessage, 'gritter-warning', false);
            IsStatus = false;
            return IsStatus;
        }
        if ((amountUSD <= totalUnderlying) || statementLetterID == 4 || self.IsNoThresholdValue()) {
            if (data.IsHitThreshold && (statementLetterID == 1 || statementLetterID == 3)) {
                self.IsEditable(true);
                self.IsUploaded(true);
                ShowNotification("Form Underlying Warning", "Total IDR->FCY Amount greater than " + data.ThresholdValue + " (USD), Please add statement B underlying", 'gritter-warning', false);
                IsStatus = false;
            }
        } else {
            self.IsEditable(true);
            self.IsUploaded(true);
            ShowNotification("Form Underlying Warning", "Total Underlying Amount must be equal greater than Transaction Amount", 'gritter-warning', false);
            IsStatus = false;
        }
        return IsStatus;
    }

    function IsValidationTMO(data, amountUSD) {
        var IsStatus = true;
        var totalUnderlying = GetTotalUnderlyingRounding(viewModel.Rounding());
        var statementLetterID = viewModel.TempSelectedUnderlying().length > 0 ? viewModel.TempSelectedUnderlying()[0].StatementLetter() : GetModelTransaction().StatementLetter.ID();

        if (self.IsNoThresholdValue() && statementLetterID != CONST_STATEMENT.Dash_ID) {
            viewModel.IsEditable(true);
            ShowNotification("Form Validation Warning", "Transaction does not have threshold, Please Select (-) Statement Letter.", 'gritter-warning', false);
            IsStatus = false;
            return IsStatus;
        }
        if (!self.IsNoThresholdValue() && statementLetterID == CONST_STATEMENT.Dash_ID) {
            viewModel.IsEditable(true);
            ShowNotification("Form Validation Warning", "Please Select Statement Letter.", 'gritter-warning', false);
            IsStatus = false;
            return IsStatus;
        }
        if (self.TransactionTMO().IsDraft()) {
            IsStatus = true;
            return IsStatus;
        }
        var IsSelectedUnderlying = (viewModel.TempSelectedUnderlying() != null && viewModel.TempSelectedUnderlying().length > 0);
        var document = ko.utils.arrayFilter(viewModel.MakerDocuments(), function (item) {
            return item.Purpose.ID == 1; // Instruction Document
        });
        var documentUnderlying = ko.utils.arrayFilter(viewModel.MakerDocuments(), function (item) {
            return item.Purpose.ID == 2; // Underlying Document
        });

        var documentStatementLetter = ko.utils.arrayFilter(viewModel.MakerDocuments(), function (item) {
            return item.Purpose.Name == 'Statement Letter'; // Statement Letter Document
        });

        var isNeedUnderlying = viewModel.t_IsFxTransaction() || (viewModel.t_IsFxTransactionToIDR() && !viewModel.IsNoThresholdValue());
        if (isNeedUnderlying) {
            if (!IsSelectedUnderlying && (document != null && document.length == 0)) { //validastion submit underlyin or statement
                self.IsEditable(true);
                self.IsUploaded(true);
                ShowNotification("Form Validation Warning", CONST_MSG.ValidationUnderlyingInstraction, 'gritter-warning', false);
                IsStatus = false;
                return IsStatus;
            }

            //if (IsSelectedUnderlying && !self.TransactionTMO().IsUnderlyingCompleted()) {
            //    self.IsEditable(true);
            //    self.IsUploaded(true);
            //    var msg = 'Please tick Is Underlying Completed field.';
            //    ShowNotification("Form Validation Warning", msg, 'gritter-warning', false);
            //    return false;
            //}
            if ((!IsSelectedUnderlying && self.TransactionTMO().IsUnderlyingCompleted())) { // || statementLetterID == CONST_STATEMENT.StatementA_ID
                self.IsEditable(true);
                self.IsUploaded(true);
                var msg = 'Please untick Is Underlying Completed field or select utilize underlying document.';
                ShowNotification("Form Validation Warning", msg, 'gritter-warning', false);
                IsStatus = false;
                return IsStatus;
            }
            if (IsSelectedUnderlying && self.TransactionTMO().IsUnderlyingCompleted() && (documentUnderlying != null && documentUnderlying.length == 0)) {
                self.IsEditable(true);
                self.IsUploaded(true);
                var msg = 'Transaction does not have underlying document, Please upload document underlying file.';
                ShowNotification("Form Validation Warning", msg, 'gritter-warning', false);
                IsStatus = false;
                return IsStatus;
            }
            if (IsSelectedUnderlying && viewModel.TempSelectedUnderlying()[0].StatementLetter() != GetModelTransaction().StatementLetter.ID()) {
                var errorMessage;
                switch (GetModelTransaction().StatementLetter.ID()) {
                    case CONST_STATEMENT.StatementA_ID:
                        errorMessage = "Please select utilize underlying Statement A.";
                        break;
                    case CONST_STATEMENT.StatementB_ID:
                        errorMessage = "Please select utilize underlying Statement B.";
                        break;
                    case CONST_STATEMENT.AnnualStatement_ID:
                        errorMessage = "Please select utilize underlying Annual Statement Letter.";
                        break;
                    case CONST_STATEMENT.Dash_ID:
                        errorMessage = "Please select statement letter or untick utilization underlying.";
                        break;
                }
                self.IsEditable(true);
                self.IsUploaded(true);
                ShowNotification("Form Underlying Warning", errorMessage, 'gritter-warning', false);
                IsStatus = false;
                return IsStatus;
            }


        }

        if (statementLetterID == 1 && data.IsHitThreshold) {
            ShowNotification("Form Validation Warning", "Total Amount Statement A hit " + data.ThresholdValue + ", Please Select Statement B  ", 'gritter-warning', false);
            self.IsEditable(true);
            self.IsUploaded(true);
            IsStatus = false;
            return IsStatus;
        }
        if (((amountUSD <= totalUnderlying) || statementLetterID == 4 || self.IsNoThresholdValue()) || !IsSelectedUnderlying) {
            if (data.IsHitThreshold && (statementLetterID == 1 || statementLetterID == 3)) {
                self.IsEditable(true);
                self.IsUploaded(true);
                ShowNotification("Form Underlying Warning", "Total IDR->FCY Amount greater than " + data.ThresholdValue + " (USD), Please add statement B underlying", 'gritter-warning', false);
                IsStatus = false;
                return IsStatus;
            }
        } else {
            self.IsEditable(true);
            self.IsUploaded(true);
            ShowNotification("Form Underlying Warning", "Total Underlying Amount must be equal greater than Transaction Amount", 'gritter-warning', false);
            IsStatus = false;
            return IsStatus;
        }

        var IsSubmissionDateStatementLetter = self.TransactionTMO().SubmissionDateSL() == '';
        if (IsSubmissionDateStatementLetter == false && !(documentStatementLetter != null && documentStatementLetter.length > 0)) {
            self.IsEditable(true);
            self.IsUploaded(true);
            var msg = 'Please upload Underlying document or remove Actual Submission Date Statement Letter field.';
            ShowNotification("Form Validation Warning", msg, 'gritter-warning', false);
            IsStatus = false;
            return IsStatus;
        }
        if (IsSubmissionDateStatementLetter == true && (documentStatementLetter != null && documentStatementLetter.length > 0)) {
            self.IsEditable(true);
            self.IsUploaded(true);
            var msg = 'Please entry Actual Submission Date Statement Letter field.';
            ShowNotification("Form Validation Warning", msg, 'gritter-warning', false);
            IsStatus = false;
            return IsStatus;
        }

        var IsSubmissionDateInstruction = self.TransactionTMO().SubmissionDateInstruction() == '';
        if (IsSubmissionDateInstruction == false && !(document != null && document.length > 0)) {
            self.IsEditable(true);
            self.IsUploaded(true);
            var msg = 'Please upload Instruction Document or remove Actual Submission Date Instruction field.';
            ShowNotification("Form Validation Warning", msg, 'gritter-warning', false);
            IsStatus = false;
            return IsStatus;
        }
        if (IsSubmissionDateInstruction == true && (document != null && document.length > 0)) {
            self.IsEditable(true);
            self.IsUploaded(true);
            var msg = 'Please entry Actual Submission Date Instruction field.';
            ShowNotification("Form Validation Warning", msg, 'gritter-warning', false);
            IsStatus = false;
            return IsStatus;
        }

        var IsSubmissionDateUnderlying = self.TransactionTMO().SubmissionDateUnderlying() == '';
        if (IsSubmissionDateUnderlying == false && !(documentUnderlying != null && documentUnderlying.length > 0)) {
            self.IsEditable(true);
            self.IsUploaded(true);
            var msg = 'Please upload Underlying document or remove Actual Submission Date Underlying field.';
            ShowNotification("Form Validation Warning", msg, 'gritter-warning', false);
            IsStatus = false;
            return IsStatus;
        }
        if (IsSubmissionDateUnderlying == true && (documentUnderlying != null && documentUnderlying.length > 0)) {
            self.IsEditable(true);
            self.IsUploaded(true);
            var msg = 'Please entry Actual Submission Date Underlying field.';
            ShowNotification("Form Validation Warning", msg, 'gritter-warning', false);
            IsStatus = false;
            return IsStatus;
        }
        return IsStatus;
    }

    function GetTotalUnderlyingRounding(rounding) {
        var totalUnderlying;
        var underlyingData = viewModel.TempSelectedUnderlying() != null && viewModel.TempSelectedUnderlying().length > 0 ? viewModel.TempSelectedUnderlying() : null;
        var IsSelectedUnderlying = (viewModel.TempSelectedUnderlying() != null && viewModel.TempSelectedUnderlying().length > 0);
        if (underlyingData != null) {
            switch (underlyingData[0].StatementLetter()) {
                case 2:
                    totalUnderlying = parseFloat(self.utilizationAmount()) + parseFloat(rounding);
                    break;
                default:
                    totalUnderlying = parseFloat(self.utilizationAmount());
                    break;
            }
        } else {
            totalUnderlying = parseFloat(self.utilizationAmount());
        }
        return totalUnderlying;
    }
    function GetTotalTransaction() {
        if (viewModel != null) {
            var CurrencyCode = GetCurrencyCode();
            var DebitCurrencyCode = GetDebitCurrencyCode();
            var paramhreshold = {
                ProductTypeID: GetModelTransaction().ProductType.ID(),
                DebitCurrencyCode: DebitCurrencyCode,
                TransactionCurrencyCode: CurrencyCode,
                IsResident: GetModelTransaction().IsResident(),
                AmountUSD: parseFloat(GetModelTransaction().AmountUSD()),
                CIF: GetModelTransaction().Customer.CIF(),
                AccountNumber: GetModelTransaction().IsOtherAccountNumber() ? null : GetModelTransaction().Account.AccountNumber(),
                IsJointAccount: GetModelTransaction().IsJointAccount(),
                StatementLetterID: GetModelTransaction().StatementLetter.ID(),
                TZReference: GetModelTransaction().TZReference()
            }
            var options = {
                url: api.server + api.url.helper + "/GetTotalTransactionIDRFCY",
                token: accessToken,
                params: {},
                data: ko.toJSON(paramhreshold)
            };
            Helper.Ajax.Post(options, function (data, textStatus, jqXHR) {
                if (jqXHR.status == 200) {
                    TotalDealModel.Total.IDRFCYDeal = data.TotalTransaction;
                    self.AmountModel().TotalAmountsUSD(data.TotalTransaction);
                    TotalDealModel.Total.UtilizationDeal = data.TotalUtilizationAmount;
                    self.AmountModel().TotalUtilization(data.TotalUtilizationAmount);
                    TotalDealModel.Total.RemainingBalance = data.AvailableUnderlyingAmount;
                    self.AmountModel().RemainingBalance(data.AvailableUnderlyingAmount);
                    //self.Rounding(data.RoundingValue
                    self.ParameterRounding(data.RoundingValue);
                    self.ThresholdType(data.ThresholdType);
                    self.ThresholdValue(data.ThresholdValue);
                    self.IsNoThresholdValue(data.ThresholdValue == 0 ? true : false);
                    SetCalculateFX(paramhreshold.AmountUSD);
                }
            }, OnError);
        }
    }
    function SetCalculateFX(amountUSD) {
        var statementLetterID = (GetModelTransaction().StatementLetter !== null ? GetModelTransaction().StatementLetter.ID() : "");
        GetModelTransaction().AmountUSD(amountUSD);
        if (GetModelTransaction().StatementLetter != null && statementLetterID == CONST_STATEMENT.AnnualStatement_ID) {
            self.AmountModel().RemainingBalance(0.00);
        } else if (GetModelTransaction().StatementLetter != null && GetModelTransaction().StatementLetter.ID() != CONST_STATEMENT.AnnualStatement_ID) {
            self.AmountModel().RemainingBalance(TotalDealModel.Total.RemainingBalance);
        }
        //Rizki 2016-03-08
        if (self.ThresholdType() == "T") {
            self.AmountModel().TotalAmountsUSD(viewModel.TransactionDealDetailModel().AmountUSD());
        }
        else {
            self.AmountModel().TotalAmountsUSD(TotalDealModel.Total.IDRFCYDeal);
        }
    }
    function SetHitThreshold(amountUSD) {
        if (self.t_IsFxTransactionToIDR()) {
            self.IsHitThreshold(false);
            self.IsShowUnderlyingDocuments(false);
            if (self.ThresholdValue() != null && self.ThresholdValue() != 0) {
                if (self.ThresholdType() == 'T') {
                    if (amountUSD > self.ThresholdValue()) {
                        self.IsHitThreshold(true);
                        self.IsShowUnderlyingDocuments(true);
                        viewModel.Selected().StatementLetter(CONST_STATEMENT.StatementB_ID);
                    } else {
                        viewModel.Selected().StatementLetter(CONST_STATEMENT.Dash_ID);
                        self.AmountModel().TotalAmountsUSD(0);
                    }
                } else {
                    if (TotalDealModel.Total.IDRFCYDeal > self.ThresholdValue()) {
                        self.IsHitThreshold(true);
                        self.IsShowUnderlyingDocuments(true);
                        viewModel.Selected().StatementLetter(CONST_STATEMENT.StatementB_ID);
                    } else {
                        viewModel.Selected().StatementLetter(CONST_STATEMENT.Dash_ID);
                        self.AmountModel().TotalAmountsUSD(0);
                    }
                }
            }

        }

    }
    function GetValueParamterThreshold() {
        var output;
        var Transaction;
        var debitCurrency;
        var trnscurrency;
        var amountUSD;
        switch (self.ActivityTitle()) {
            case CONST_TRANSACTION.FX:
                Transaction = self.TransactionDealDetailModel();
                debitCurrency = Transaction.DebitCurrency;
                trnscurrency = Transaction.Currency;
                amountUSD = Transaction.AmountUSD();
                break;
            case CONST_TRANSACTION.TMO:
                Transaction = self.TransactionTMO();
                debitCurrency = Transaction.SellCurrency;
                trnscurrency = Transaction.BuyCurrency;
                amountUSD = Transaction.AmountUSD();
                break;
        }
        if (Transaction != null) {
            output = {
                TzRef: GetModelTransaction().TZReference(),
                StatementLetterID: GetModelTransaction().StatementLetter.ID(),
                ProductTypeID: GetProductTypeID(Transaction.ProductType),
                DebitCurrencyCode: debitCurrency.Code(),
                TransactionCurrencyCode: trnscurrency.Code(),
                IsResident: Transaction.IsResident(),
                AmountUSD: parseFloat(amountUSD),
                CIF: Transaction.Customer.CIF(),
                AccountNumber: Transaction.IsOtherAccountNumber() ? null : Transaction.Account.AccountNumber(),
                IsJointAccount: Transaction.IsJointAccount()
            }
        }

        return output;
    }
    function SetChangeAccountNumber(account) {
        if (!self.IsSelectedRow()) {
            switch (self.ActivityTitle()) {
                case CONST_TRANSACTION.FX:
                    //Rizki 20161108 - TFS 6400
                    var AccountNumberSelected = account.AccountNumber();
                    if (AccountNumberSelected != null) {
                        if (AccountNumberSelected == '-') {
                            self.IsEmptyAccountNumber(true);
                            self.TransactionDealDetailModel().IsOtherAccountNumber(true);
                        }
                        else {
                            self.IsEmptyAccountNumber(false);
                            self.TransactionDealDetailModel().OtherAccountNumber(null);
                            self.TransactionDealDetailModel().IsOtherAccountNumber(false);
                            self.TransactionDealDetailModel().Account.AccountNumber(account.AccountNumber());
                            GetTotalTransaction();
                        }
                    }
                    else {
                        self.IsEmptyAccountNumber(false);
                        self.TransactionDealDetailModel().Account.AccountNumber(null);
                        if (self.TransactionDealDetailModel().IsJointAccount()) {
                            self.AmountModel().TotalAmountsUSD(0);
                            self.AmountModel().RemainingBalance(0);
                        }
                    }
                    //end Rizki
                    break;
                case CONST_TRANSACTION.TMO:
                    var AccountNumberSelected = account.AccountNumber();
                    if (AccountNumberSelected != null && AccountNumberSelected == '-') {
                        self.IsEmptyAccountNumber(true);
                        self.TransactionTMO().IsOtherAccountNumber(true);
                    } else if (AccountNumberSelected != null && AccountNumberSelected != '-') {
                        self.IsEmptyAccountNumber(false);
                        self.TransactionTMO().OtherAccountNumber(null);
                        self.TransactionTMO().IsOtherAccountNumber(false);
                        GetTotalTransaction();
                    }

                    break;
            }
            var accountNumber = GetModelTransaction().AccountNumber() != null && GetModelTransaction().AccountNumber() != "";

            if (accountNumber && GetModelTransaction().IsJointAccount()) {
                viewModel.UnderlyingFilterAccountNumber(GetModelTransaction().AccountNumber());
                viewModel.UnderlyingFilterShow(2); // show data only joint account
                viewModel.GetDataUnderlying();
            } else {
                viewModel.UnderlyingFilterAccountNumber("");
                viewModel.UnderlyingFilterShow(1); // show data single account
                viewModel.GetDataUnderlying();
            }
            UnCheckUnderlyingTable();
        }
    }
    function GetAmountUSD() {
        var output = 0
        switch (self.ActivityTitle()) {
            case CONST_TRANSACTION.FX:
                output = self.TransactionDealDetailModel().AmountUSD();
                break;
            case CONST_TRANSACTION.TMO:
                output = self.TransactionTMO().AmountUSD();
                break;
            default:
                break;
        }
        return output;
    }
    function GetCurrencyCode() {
        var output = 0
        switch (self.ActivityTitle()) {
            case CONST_TRANSACTION.FX:
                output = self.TransactionDealDetailModel().Currency.Code();
                break;
            case CONST_TRANSACTION.TMO:
                output = self.TransactionTMO().BuyCurrency.Code();
                break;
            default:
                break;
        }
        return output;
    }
    function GetDebitCurrencyCode() {
        var output = 0
        switch (self.ActivityTitle()) {
            case CONST_TRANSACTION.FX:
                output = self.TransactionDealDetailModel().DebitCurrency.Code();
                break;
            case CONST_TRANSACTION.TMO:
                output = self.TransactionTMO().SellCurrency.Code();
                break;
            default:
                break;
        }
        return output;
    }

    function GetAccounts() {
        var output;
        switch (self.ActivityTitle()) {
            case CONST_TRANSACTION.FX:
                output = viewModel.TransactionDealDetailModel().Customer.Accounts();
                break;
            case CONST_TRANSACTION.TMO:
                output = viewModel.TransactionTMO().Customer.Accounts();
                break;
        }
        return output;
    }
    function UnCheckUnderlyingTable() {
        viewModel.TempSelectedUnderlying([]);
        var dataRows = [];
        ko.utils.arrayForEach(viewModel.CustomerUnderlyings(), function (item) {
            item.IsEnable = false;
            dataRows.push(item);
        });
        viewModel.CustomerUnderlyings([]);
        viewModel.CustomerUnderlyings(dataRows);
    }
    function GetDebitCurrency() {
        var output;
        switch (self.ActivityTitle()) {
            case CONST_TRANSACTION.FX:
                output = viewModel.TransactionDealDetailModel().DebitCurrency != null ? viewModel.TransactionDealDetailModel().DebitCurrency.Code() : null;
                break;
            case CONST_TRANSACTION.TMO:
                output = viewModel.TransactionTMO().DebitCurrency != null ? viewModel.TransactionTMO().DebitCurrency.Code() : null;
                break;
        }
        return output;
    }
    //#endregion
    function GetModelTransaction() {
        var model;
        switch (self.ActivityTitle()) {
            case CONST_TRANSACTION.FX:
                model = self.TransactionDealDetailModel();
                break;
            case CONST_TRANSACTION.TMO:
                model = self.TransactionTMO();
                break;

        }
        return model;
    }
    function GetDataBulkUnderlying() {
        // widget reloader function start
        if ($box.css('position') == 'static') {
            $remove = true;
            $box.addClass('position-relative');
        }
        $box.append(config.spinner);

        $box.one('reloaded.ace.widget', function () {
            $box.find('.widget-box-overlay').remove();
            if ($remove) $box.removeClass('position-relative');
        });
        // widget reloader function end

        // declare options variable for ajax get request
        var options = {
            url: api.server + api.url.customerunderlying + "/BulkJoint",
            //url: api.server + api.url.customerunderlying + "/Bulk",
            params: {
                page: self.UnderlyingBulkGridProperties().Page(),
                size: self.UnderlyingBulkGridProperties().Size(),
                sort_column: self.UnderlyingBulkGridProperties().SortColumn(),
                sort_order: self.UnderlyingBulkGridProperties().SortOrder()
            },
            token: accessToken
        };

        // get filtered columns

        var filters = GetBulkUnderlyingFilteredColumns();

        if (filters.length > 0) {
            // POST
            // add request body on POST
            options.data = ko.toJSON(filters);

            Helper.Ajax.Post(options, OnSuccessGetUnderlyingBulk, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(options, OnSuccessGetUnderlyingBulk, OnError, OnAlways);
        }
    }
    function GetTotalAmount(CIFData, AmountUSD) {
        var totalValue1 = $.ajax({
            type: "GET",
            url: api.server + api.url.transactiondeal + "/GetAmountTransaction=" + CIFData + "?isStatementB=false",
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            }
        });
        var totalValue2 = $.ajax({
            type: "GET",
            url: api.server + api.url.transactiondeal + "/GetAmountTransaction=" + CIFData + "?isStatementB=true",
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            }
        });
        $.when(totalValue1, totalValue2).done(function (data1, data2) {
            var t_TotalAmounts = 0;
            if (data1 != null || data1 != undefined) {
                var t_TotalAmounts1 = parseFloat(data1);
                var t_TotalAmounts2 = parseFloat(data2);

                self.AmountModel().TotalAmounts(t_TotalAmounts2);
                self.AmountModel().TotalAmountsStatementB(t_TotalAmounts1);
                Calculating(AmountUSD);
            }
            else {
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }
    function GetTreshold() {
        $.ajax({
            type: "GET",
            url: api.server + api.url.singlevalueparameter + "/FX_TRANSACTION_TRESHOLD",
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            }, success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    viewModel.AmountModel().TreshHold(data);
                }
            }
        });
    }
    function GetFCYIDRTreshold() {
        $.ajax({
            type: "GET",
            url: api.server + api.url.singlevalueparameter + "/FCY_IDR_TRANSACTION_TRESHOLD",
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            }, success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    viewModel.AmountModel().FCYIDRTreshold(data);
                }
            }
        });
    }

    function GetSelectedUtilizeID() {
        var filters = [];
        self.UnderlyingFilterParentID(self.ID_u());
        self.CIF_u(cifData);
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });

        $.ajax({
            type: "POST",
            url: api.server + api.url.customerunderlying + "/SelectedUtilizeID",
            contentType: "application/json; charset=utf-8",
            data: ko.toJSON(filters),
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.TempSelectedUnderlying([]);
                    for (var i = 0; i < data.length; i++) {
                        switch (self.ActivityTitle()) {
                            case CONST_TRANSACTION.FX:
                                self.TempSelectedUnderlying.push(new SelectUtillizeModel(data[i].ID, true, 0));
                                break;
                            case CONST_TRANSACTION.TMO:
                                self.TempSelectedUnderlying.push(new SelectUtillizeTMOModel(data[i].ID, true, 0));
                                break;
                        }
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    function GetSelectedProformaID(callback) {
        var filters = [];
        self.UnderlyingFilterParentID(self.ID_u());
        self.CIF_u(cifData);
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.UnderlyingFilterParentID() != "") filters.push({ Field: 'ParentID', Value: self.UnderlyingFilterParentID() });
        if (self.ProformaFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.ProformaFilterIsSelected() });
        if (self.ProformaFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.ProformaFilterUnderlyingDocument() });
        if (self.ProformaFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.ProformaFilterDocumentType() });


        $.ajax({
            type: "POST",
            url: api.server + api.url.customerunderlying + "/SelectedProformaID",
            contentType: "application/json; charset=utf-8",
            data: ko.toJSON(filters),
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.TempSelectedUnderlyingProforma([]);
                    for (var i = 0; i < data.length; i++) {
                        self.TempSelectedUnderlyingProforma.push({ ID: data[i].ID });
                    }
                    if (callback != null) {
                        callback();
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    // Function to get All selected Bulk Underlying // add 2015.03.09
    function GetSelectedBulkID(callback) {
        var filters = [];
        self.UnderlyingFilterParentID(self.ID_u());
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: self.CIF_u() });
        if (self.UnderlyingFilterParentID() != "") filters.push({ Field: 'ParentID', Value: self.UnderlyingFilterParentID() });
        if (self.BulkFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.BulkFilterIsSelected() });
        if (self.BulkFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.BulkFilterUnderlyingDocument() });
        if (self.BulkFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.BulkFilterDocumentType() });


        $.ajax({
            type: "POST",
            url: api.server + api.url.customerunderlying + "/SelectedBulkID",
            contentType: "application/json; charset=utf-8",
            data: ko.toJSON(filters),
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    self.TempSelectedUnderlyingBulk([]);
                    for (var i = 0; i < data.length; i++) {
                        self.TempSelectedUnderlyingBulk.push({ ID: data[i].ID, Amount: data[i].Amount });
                    }
                    if (callback != null) {
                        callback();
                    }
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    // Get filtered columns value
    function GetFilteredColumns() {
        // define filter
        var filters = [];
        if (self.FilterApplicationID() != "") filters.push({ Field: 'ApplicationID', Value: self.FilterApplicationID() });
        if (self.FilterTZReference() != "") filters.push({ Field: 'TZReference', Value: self.FilterTZReference() });
        if (self.FilterCustomer() != "") filters.push({ Field: 'Customer', Value: self.FilterCustomer() });
        if (self.FilterProduct() != "") filters.push({ Field: 'Product', Value: self.FilterProduct() });
        if (self.FilterTradeDate() != "") filters.push({ Field: 'TradeDate', Value: self.FilterTradeDate() });
        if (self.FilterValueDate() != "") filters.push({ Field: 'ValueDate', Value: self.FilterValueDate() });
        if (self.FilterCurrency() != "") filters.push({ Field: 'CurrencyDesc', Value: self.FilterCurrency() });
        if (self.FilterAccountNumber() != "") filters.push({ Field: 'AccountNumber', Value: self.FilterAccountNumber() });
        if (self.FilterAmount() != "") filters.push({ Field: 'Amount', Value: self.FilterAmount() });
        if (self.FilterFXTransaction() != "") filters.push({ Field: 'IsFXTransaction', Value: self.FilterFXTransaction() });
        if (self.FilterTransactionStatus() != "") filters.push({ Field: 'TransactionStatus', Value: self.FilterTransactionStatus() });
        if (self.FilterTzStatus() != "") filters.push({ Field: 'TzStatus', Value: self.FilterTzStatus() });
        if (self.FilterUpdateBy() != "") filters.push({ Field: 'UpdateBy', Value: self.FilterUpdateBy() });
        if (self.FilterUpdateDate() != "") filters.push({ Field: 'UpdateDate', Value: self.FilterUpdateDate() });
        if (self.FilterIsTMO() != "" && self.FilterIsTMO() != null) filters.push({ Field: 'IsTMO', Value: self.FilterIsTMO() });

        return filters;
    };

    // Get filtered columns value
    function GetUnderlyingFilteredColumns() {
        // define filter

        var filters = [];
        self.CIF_u(cifData);
        //self.UnderlyingFilterIsTMO(self.FilterIsTMO());
        self.UnderlyingFilterIsTMO(false);
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.UnderlyingFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.UnderlyingFilterIsSelected() });
        if (self.UnderlyingFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.UnderlyingFilterUnderlyingDocument() });
        if (self.UnderlyingFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.UnderlyingFilterDocumentType() });
        if (self.UnderlyingFilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.UnderlyingFilterCurrency() });
        if (self.UnderlyingFilterStatementLetter() != "") filters.push({ Field: 'StatementLetter', Value: self.UnderlyingFilterStatementLetter() });
        if (self.UnderlyingFilterAmount() != "") filters.push({ Field: 'Amount', Value: self.UnderlyingFilterAmount() });
        if (self.UnderlyingFilterDateOfUnderlying() != "") filters.push({ Field: 'DateOfUnderlying', Value: self.UnderlyingFilterDateOfUnderlying() });
        if (self.UnderlyingFilterExpiredDate() != "") filters.push({ Field: 'ExpiredDate', Value: self.UnderlyingFilterExpiredDate() });
        if (self.UnderlyingFilterReferenceNumber() != "") filters.push({ Field: 'ReferenceNumber', Value: self.UnderlyingFilterReferenceNumber() });
        if (self.UnderlyingFilterInvoiceNumber() != "") filters.push({ Field: 'InvoiceNumber', Value: self.UnderlyingFilterInvoiceNumber() });
        if (self.UnderlyingFilterSupplierName() != "") filters.push({ Field: 'SupplierName', Value: self.UnderlyingFilterSupplierName() });
        if (self.UnderlyingFilterIsAvailable() != "") filters.push({ Field: 'IsAvailable', Value: self.UnderlyingFilterIsAvailable() });
        if (self.UnderlyingFilterIsExpiredDate() != "") filters.push({ Field: 'IsExpiredDate', Value: self.UnderlyingFilterIsExpiredDate() });
        if (self.UnderlyingFilterIsSelectedBulk() == false) filters.push({ Field: 'IsSelectedBulk', Value: self.UnderlyingFilterIsSelectedBulk() });
        if (self.UnderlyingFilterAvailableAmount() != "") filters.push({ Field: 'AvailableAmount', Value: self.UnderlyingFilterAvailableAmount() });
        if (self.UnderlyingFilterAttachmentNo() != "") filters.push({ Field: 'AttachmentNo', Value: self.UnderlyingFilterAttachmentNo() });
        if (self.UnderlyingFilterIsTMO() != "" && self.UnderlyingFilterIsTMO() != null) filters.push({ Field: 'IsTMO', Value: self.UnderlyingFilterIsTMO() });
        if (self.UnderlyingFilterAccountNumber() != "") filters.push({ Field: 'AccountNumber', Value: self.UnderlyingFilterAccountNumber() });
        filters.push({ Field: 'StatusShowData', Value: self.UnderlyingFilterShow() });
        return filters;
    };
    //Tambah Agung
    function GetInstructionFilteredColumns() {
        var filters = [];
        self.InstructionFilterIsSelected(true);
        //if (self.InstructionFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.InstructionFilterIsSelected() });
        if (self.InstructionFilterApplicationID() != "") filters.push({ Field: 'ApplicationID', Value: self.InstructionFilterApplicationID() });
        if (self.InstructionFilterFileName() != "") filters.push({ Field: 'FileName', Value: self.InstructionFilterFileName() });
        if (self.InstructionFilterPurposeofDoc() != "") filters.push({ Field: 'PurposeofDoc', Value: self.InstructionFilterPurposeofDoc() });
        if (self.InstructionFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.InstructionFilterDocumentType() });
        if (self.InstructionFilterLastModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.InstructionFilterLastModifiedDate() });

        filters.push({ Field: 'StatusShowData', Value: self.InstructionFilterShow() });
        return filters;
    }
    //End
    // Get filtered columns value
    function GetUnderlyingAttachFilteredColumns() {
        var filters = [];
        self.UnderlyingFilterIsSelected(true); // flag for get all datas
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.UnderlyingAttachFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.UnderlyingAttachFilterIsSelected() });
        if (self.UnderlyingAttachFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.UnderlyingAttachFilterUnderlyingDocument() });
        if (self.UnderlyingAttachFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.UnderlyingAttachFilterDocumentType() });
        if (self.UnderlyingAttachFilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.UnderlyingAttachFilterCurrency() });
        if (self.UnderlyingAttachFilterStatementLetter() != "") filters.push({ Field: 'StatementLetter', Value: self.UnderlyingAttachFilterStatementLetter() });
        if (self.UnderlyingAttachFilterAmount() != "") filters.push({ Field: 'Amount', Value: self.UnderlyingAttachFilterAmount() });
        if (self.UnderlyingAttachFilterDateOfUnderlying() != "") filters.push({ Field: 'DateOfUnderlying', Value: self.UnderlyingAttachFilterDateOfUnderlying() });
        if (self.UnderlyingAttachFilterExpiredDate() != "") filters.push({ Field: 'ExpiredDate', Value: self.UnderlyingAttachFilterExpiredDate() });
        if (self.UnderlyingAttachFilterReferenceNumber() != "") filters.push({ Field: 'ReferenceNumber', Value: self.UnderlyingAttachFilterReferenceNumber() });
        if (self.UnderlyingAttachFilterSupplierName() != "") filters.push({ Field: 'SupplierName', Value: self.UnderlyingAttachFilterSupplierName() });
        if (self.UnderlyingAttachFilterAccountNumber() != "") filters.push({ Field: 'AccountNumber', Value: self.UnderlyingAttachFilterAccountNumber() });
        if (self.UnderlyingAttachFilterIsTMO() != "" && self.UnderlyingAttachFilterIsTMO() != null) filters.push({ Field: 'IsTMO', Value: self.UnderlyingAttachFilterIsTMO() });

        filters.push({ Field: 'StatusShowData', Value: self.UnderlyingFilterShow() });
        return filters;
    }

    // Get filtered columns value
    function GetAttachFilteredColumns() {
        // define filter

        var filters = [];
        self.CIF_u(cifData);
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.AttachFilterFileName() != "") filters.push({ Field: 'FileName', Value: self.AttachFilterFileName() });
        if (self.AttachFilterDocumentPurpose() != "") filters.push({ Field: 'DocumentPurpose', Value: self.AttachFilterDocumentPurpose() });
        if (self.AttachFilterModifiedDate() != "") filters.push({ Field: 'LastModifiedDate', Value: self.AttachFilterModifiedDate() });
        if (self.AttachFilterDocumentRefNumber() != "") filters.push({ Field: 'DocumentRefNumber', Value: self.AttachFilterDocumentRefNumber() });
        if (self.AttachFilterAttachmentReference() != "") filters.push({ Field: 'AttachmentReference', Value: self.AttachFilterAttachmentReference() });
        if (self.AttachFilterAccountNumber() != "") filters.push({ Field: 'AccountNumber', Value: self.AttachFilterAccountNumber() });

        filters.push({ Field: 'StatusShowData', Value: self.UnderlyingFilterShow() });
        return filters;
    };

    // Get filtered columns value
    function GetUnderlyingProformaFilteredColumns() {
        // define filter
        var filters = [];
        self.UnderlyingFilterParentID(self.ID_u());
        self.CIF_u(cifData);
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: cifData });
        if (self.UnderlyingFilterParentID() != "") filters.push({ Field: 'ParentID', Value: self.UnderlyingFilterParentID() });
        if (self.ProformaFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.ProformaFilterIsSelected() });
        if (self.ProformaFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.ProformaFilterUnderlyingDocument() });
        if (self.ProformaFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.ProformaFilterDocumentType() });
        if (self.ProformaFilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.ProformaFilterCurrency() });
        if (self.ProformaFilterStatementLetter() != "") filters.push({ Field: 'StatementLetter', Value: self.ProformaFilterStatementLetter() });
        if (self.ProformaFilterAmount() != "") filters.push({ Field: 'Amount', Value: self.ProformaFilterAmount() });
        if (self.ProformaFilterDateOfUnderlying() != "") filters.push({ Field: 'DateOfUnderlying', Value: self.ProformaFilterDateOfUnderlying() });
        if (self.ProformaFilterExpiredDate() != "") filters.push({ Field: 'ExpiredDate', Value: self.ProformaFilterExpiredDate() });
        if (self.ProformaFilterReferenceNumber() != "") filters.push({ Field: 'ReferenceNumber', Value: self.ProformaFilterReferenceNumber() });
        if (self.ProformaFilterInvoiceNumber() != "") filters.push({ Field: 'InvoiceNumber', Value: self.ProformaFilterInvoiceNumber() });
        if (self.ProformaFilterSupplierName() != "") filters.push({ Field: 'SupplierName', Value: self.ProformaFilterSupplierName() });

        return filters;
    };

    // Get filtered columns value // add 2015.03.09
    function GetBulkUnderlyingFilteredColumns() {
        // define filter
        var filters = [];
        self.UnderlyingFilterParentID(self.ID_u());
        self.BulkFilterCurrency(self.Currency_u().Code() != "" && self.Currency_u().Code() != null ? self.Currency_u().Code() : "xyz");
        if (self.CIF_u() != "") filters.push({ Field: 'CIF', Value: self.CIF_u() });
        if (self.UnderlyingFilterParentID() != "") filters.push({ Field: 'ParentID', Value: self.UnderlyingFilterParentID() });
        if (self.BulkFilterIsSelected() != "") filters.push({ Field: 'IsSelected', Value: self.BulkFilterIsSelected() });
        if (self.BulkFilterUnderlyingDocument() != "") filters.push({ Field: 'UnderlyingDocument', Value: self.BulkFilterUnderlyingDocument() });
        if (self.BulkFilterDocumentType() != "") filters.push({ Field: 'DocumentType', Value: self.BulkFilterDocumentType() });
        if (self.BulkFilterCurrency() != "") filters.push({ Field: 'Currency', Value: self.BulkFilterCurrency() });
        if (self.BulkFilterStatementLetter() != "") filters.push({ Field: 'StatementLetter', Value: self.BulkFilterStatementLetter() });
        if (self.BulkFilterAmount() != "") filters.push({ Field: 'Amount', Value: self.BulkFilterAmount() });
        if (self.BulkFilterDateOfUnderlying() != "") filters.push({ Field: 'DateOfUnderlying', Value: self.BulkFilterDateOfUnderlying() });
        if (self.BulkFilterExpiredDate() != "") filters.push({ Field: 'ExpiredDate', Value: self.BulkFilterExpiredDate() });
        if (self.BulkFilterReferenceNumber() != "") filters.push({ Field: 'ReferenceNumber', Value: self.BulkFilterReferenceNumber() });
        if (self.BulkFilterInvoiceNumber() != "") filters.push({ Field: 'InvoiceNumber', Value: self.BulkFilterInvoiceNumber() });
        if (self.BulkFilterSupplierName() != "") filters.push({ Field: 'SupplierName', Value: self.BulkFilterSupplierName() });

        return filters;
    };

    function GetAnnualStatement(cif) {
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/CheckAnnualStatement/" + cif,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            }, success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    if (data != null) {
                        if (data) {
                            GetThreshold(true);
                        } else {
                            ShowNotification("Form Validation Warning", CONST_MSG.ValidationAnnualSL, 'gritter-warning', true);
                            self.IsEditable(true);
                            return;
                        }
                    } else {
                        ShowNotification("Warning", "Something went worng with function get annual statment", 'gritter-warning', true);
                        self.IsEditable(true);
                        return;
                    }
                }
            }
        });

    }

    // On success GetData callback
    function OnSuccessGetDataUnderlying(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            //self.CustomerUnderlyings(data.Rows);
            //self.TempSelectedUnderlying([]);
            //self.MakerAllDocumentsTemp([]);

            //self.TempSelectedUnderlying([]);
            ko.utils.arrayForEach(data.Rows, function (itemd) {
                itemd.UtilizeAmountDeal = formatNumber(itemd.AvailableAmountDeal);
            });
            var dataunderlying = SetSelectedUnderlying(data.Rows);
            self.CustomerUnderlyings([]);
            self.CustomerUnderlyings(dataunderlying);

            ko.utils.arrayForEach(self.CustomerUnderlyings(), function (item2) {
                if (item2 != undefined) {
                    if (item2.UtilizeAmountDeal == 0) {
                        self.CustomerUnderlyings.remove(item2);
                    }
                }
            });
            self.UnderlyingGridProperties().Page(data['Page']);
            self.UnderlyingGridProperties().Size(data['Size']);
            self.UnderlyingGridProperties().Total(data['Total']);
            self.UnderlyingGridProperties().TotalPages(Math.ceil(self.UnderlyingGridProperties().Total() / self.UnderlyingGridProperties().Size()));
            ReSetTotalUnderlying();
            //UnCheckDataUnderlying();
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    // On success GetData callback
    function OnSuccessGetUnderlyingDataAttachFile(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {

            for (var i = 0 ; i < data.Rows.length; i++) {
                var selected = ko.utils.arrayFirst(self.TempSelectedAttachUnderlying(), function (item) {
                    return item.ID == data.Rows[i].ID;
                });
                if (selected != null) {
                    data.Rows[i].IsSelectedAttach = true;
                }
                else {
                    data.Rows[i].IsSelectedAttach = false;
                }
            }
            self.CustomerAttachUnderlyings(data.Rows);

            self.UnderlyingAttachGridProperties().Page(data['Page']);
            self.UnderlyingAttachGridProperties().Size(data['Size']);
            self.UnderlyingAttachGridProperties().Total(data['Total']);
            self.UnderlyingAttachGridProperties().TotalPages(Math.ceil(self.UnderlyingAttachGridProperties().Total() / self.UnderlyingAttachGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    // On success GetData callback
    function OnSuccessGetDataAttachFile(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            self.CustomerUnderlyingFiles(data.Rows);
            self.AttachGridProperties().Page(data['Page']);
            self.AttachGridProperties().Size(data['Size']);
            self.AttachGridProperties().Total(data['Total']);
            self.AttachGridProperties().TotalPages(Math.ceil(self.AttachGridProperties().Total() / self.AttachGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    // On success GetData callback
    function OnSuccessGetUnderlyingProforma(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            SetSelectedProforma(data.Rows);

            self.CustomerUnderlyingProformas(data.Rows);

            self.UnderlyingProformaGridProperties().Page(data['Page']);
            self.UnderlyingProformaGridProperties().Size(data['Size']);
            self.UnderlyingProformaGridProperties().Total(data['Total']);
            self.UnderlyingProformaGridProperties().TotalPages(Math.ceil(self.UnderlyingProformaGridProperties().Total() / self.UnderlyingProformaGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    // On success GetData callback // Add 2015.03.09
    function OnSuccessGetUnderlyingBulk(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            SetSelectedBulk(data.Rows);
            self.AllDataCustomerUnderlyingBulk(data.Rows)
            var filterDataUnderlying;
            if (self.IsJointAccount_u()) {
                if (self.AccountNumber_u() != null) {
                    var filterDataUnderlying = ko.utils.arrayFilter(self.AllDataCustomerUnderlyingBulk(), function (item) {
                        return self.AccountNumber_u() == item.AccountNumber;
                    });
                }
            } else {
                filterDataUnderlying = ko.utils.arrayFilter(self.AllDataCustomerUnderlyingBulk(), function (item) {
                    return self.IsJointAccount_u() == (item.IsJointAccount != null ? item.IsJointAccount : false);
                });
            }


            if (filterDataUnderlying != null && filterDataUnderlying.length > 0) {
                self.CustomerBulkUnderlyings(filterDataUnderlying);
            } else {
                self.CustomerBulkUnderlyings(filterDataUnderlying);
            }
            self.UnderlyingBulkGridProperties().Page(data['Page']);
            self.UnderlyingBulkGridProperties().Size(data['Size']);
            self.UnderlyingBulkGridProperties().Total(data['Total']);
            self.UnderlyingBulkGridProperties().TotalPages(Math.ceil(self.UnderlyingBulkGridProperties().Total() / self.UnderlyingBulkGridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
        }
    }

    function DeleteUnderlyingAttachment(item) {
        //Ajax call to delete the Customer

        $.ajax({
            type: "DELETE",
            url: api.server + api.url.customerunderlyingfile + "/" + item.ID,
            headers: {
                "Authorization": "Bearer " + accessToken
            },
            success: function (data, textStatus, jqXHR) {
                // send notification
                if (jqXHR.status = 200) {
                    //DeleteFile(item.DocumentPath); deleted file underlying
                    //if(!item.IsNewDocument) {
                    // DeleteTransactionDocumentUnderlying(item);
                    //}
                    ShowNotification('Delete Success', jqXHR.responseJSON.Message, 'gritter-success', false);
                    // refresh data
                    //GetDataAttachFile();
                } else
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);

            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }

    // On Error callback
    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }

    // On Always callback
    function OnAlways() {
        $box.trigger('reloaded.ace.widget');
    }
    //--------------------------- set Underlying Function & variable end
};

// Knockout View Model
var viewModel = new ViewModel();

$.holdReady(true);
var intervalRoleName = setInterval(function () {
    if (Object.keys(Const_RoleName).length != 0) {
        $.holdReady(false);
        clearInterval(intervalRoleName);
    }
}, 100);

// jQuery Init
//$(document).on("RoleNameReady", function() {
$(document).ready(function () {
    //$('#modal-form-Underlying')

    //input in numeric    
    $.fn.ForceNumericOnly = function () {

        return this.each(function () {

            $(this).keydown(function (e) {

                var key = e.charCode || e.keyCode || 0;

                return (

                    key == 8 ||
                    key == 9 ||
                    key == 13 ||
                    key == 46 ||
                    key == 110 ||
                    key == 190 ||
                    (key >= 35 && key <= 40) ||
                    (key >= 48 && key <= 57) ||
                    (key >= 96 && key <= 105));

            });

        });

    };
    $(".input-numericonly").ForceNumericOnly();

    // ace file upload
    $('#document-path').ace_file_input({
        no_file: 'No File ...',
        btn_choose: 'Choose',
        btn_change: 'Change',
        droppable: false,
        //onchange:null,
        thumbnail: false, //| true | large
        //whitelist:'gif|png|jpg|jpeg'
        blacklist: 'exe|dll'
        //onchange:''
        //
    });

    $('#document-path-upload').ace_file_input({
        no_file: 'No File ...',
        btn_choose: 'Choose',
        btn_change: 'Change',
        droppable: false,
        //onchange:null,
        thumbnail: false, //| true | large
        //whitelist:'gif|png|jpg|jpeg'
        blacklist: 'exe|dll'
        //onchange:''
        //
    });

    // Knockout custom file handler
    ko.bindingHandlers.file = {
        init: function (element, valueAccessor) {
            $(element).change(function () {
                var file = this.files[0];

                if (ko.isObservable(valueAccessor()))
                    valueAccessor()(file);
            });
        }
    };

    $('#aspnetForm').after('<form id="frmUnderlying"></form>');
    $("#modal-form-Underlying").appendTo("#frmUnderlying");
    $("#modal-form-Attach").appendTo("#frmUnderlying");
    $box = $('#widget-box');
    var event;
    $box.trigger(event = $.Event('reload.ace.widget'))
    if (event.isDefaultPrevented()) return
    $box.blur();

    // datepicker
    $('.date-picker').datepicker({
        autoclose: true,
        onSelect: function () {
            this.focus();
        }
    }).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });

    // Load all templates
    $('script[src][type="text/html"]').each(function () {
        //alert($(this).attr("src"))
        var src = $(this).attr("src");
        if (src != undefined) {
            $(this).load(src);
        }
    });
    // block enter key from user to prevent submitted data.
    $(this).keypress(function (event) {
        var code = event.charCode || event.keyCode;
        if (code == 13) {
            ShowNotification("Page Information", CONST_MSG.ValidationEnter, "gritter-warning", false);
            return false;
        }
    });

    // Token Validation
    if ($.cookie(api.cookie.name) == undefined) {
        Helper.Token.Request(OnSuccessToken, OnError);
    } else {
        // read token from cookie
        accessToken = $.cookie(api.cookie.name);

        // read spuser from cookie
        if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
            spUser = $.cookie(api.cookie.spUser);

            viewModel.SPUser(ko.mapping.toJS(spUser));
            viewModel.isDealBUGroup(true);
            viewModel.IsPermitedButtonBookDeal();
            GetPermission();
        }

        // call get data inside view model
        //DealModel.token = accessToken;
        //GetParameterData(DealModel, OnSuccessGetTotal, OnErrorDeal);
        DealModel.token = accessToken;
        GetThresholdParameter(DealModel, OnSuccessThresholdPrm, OnErrorDeal);
        GetParameters();

    }

    // Knockout Bindings
    ko.applyBindings(viewModel);
    //ko.applyBindings(viewModel, document.getElementById('add-transaction'));
    //GET ALL BOOKED TRANSACTION
    // viewModel.GetTransactionData();

    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        focusCleanup: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }
    });

    $('#frmUnderlying').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        focusCleanup: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }
    });



    // Knockout Datepicker custom binding handler
    ko.bindingHandlers.datepicker = {
        init: function (element) {
            $(element).datepicker({ autoclose: true }).next().on(ace.click_event, function () {
                $(this).prev().focus();
            });
        },
        update: function (element) {
            $(element).datepicker("refresh");
        }
    };



    // Dependant Observable for dropdown auto fill
    ko.dependentObservable(function () {
        /* var t_IsFxTransaction;
         var t_IsFxTransactionToIDR;
         var t_IsFxTransactionToIDRB;
         var isFlowValas; */


        var currency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) {
            return item.ID() == viewModel.Selected().Currency();
        });
        /*var account = ko.utils.arrayFirst(viewModel.TransactionDealModel().Customer().Accounts, function (item) {
            return item.AccountNumber == viewModel.Selected().Account();
        });*/
        var debitcurrency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) {
            return item.ID() == viewModel.Selected().DebitCurrency();
        });
        var productType = ko.utils.arrayFirst(viewModel.Parameter().ProductType(), function (item) {
            return item.ID() == viewModel.Selected().ProductType();
        });
        if (currency != null && currency.ID() != null) {
            viewModel.TransactionDealDetailModel().Currency(currency);
            GetRateAmount(currency.ID());
        }
        if (debitcurrency != null && debitcurrency.ID() != null) {
            viewModel.TransactionDealDetailModel().DebitCurrency = debitcurrency;
        }
        var sellcurrency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) { return item.ID() == viewModel.Selected().SellCurrency(); });
        var buycurrency = ko.utils.arrayFirst(viewModel.Parameter().Currencies(), function (item) { return item.ID() == viewModel.Selected().BuyCurrency(); });

        /*  isFlowValas = GetIsFlowValas(viewModel.TransactionDealDetailModel().ProductType);
          t_IsFxTransaction = (viewModel.TransactionDealDetailModel().Currency.Code() != 'IDR' && viewModel.TransactionDealDetailModel().DebitCurrency.Code() == 'IDR') ? true : false;
          t_IsFxTransactionToIDR = (viewModel.TransactionDealDetailModel().Currency.Code() == 'IDR' && viewModel.TransactionDealDetailModel().DebitCurrency.Code() != 'IDR') ? true : false;
          t_IsFxTransactionToIDRB = (viewModel.TransactionDealDetailModel().Currency.Code() == 'IDR' && viewModel.TransactionDealDetailModel().DebitCurrency.Code() != 'IDR' && GetIsFlowValas(viewModel.TransactionDealDetailModel().ProductType)) ? true : false;
          */
        if (sellcurrency != null && sellcurrency.ID() != null) {
            viewModel.TransactionTMO().SellCurrency(sellcurrency);
        }
        if (buycurrency != null && buycurrency.ID() != null) {
            viewModel.TransactionTMO().BuyCurrency(buycurrency);
            GetRateAmount(buycurrency.ID());
        }
        /* isFlowValas = true;
         t_IsFxTransaction = true;
         t_IsFxTransactionToIDR = false;
         t_IsFxTransactionToIDRB = true; */


        var docPurpose = ko.utils.arrayFirst(viewModel.Parameter().DocumentPurposes(), function (item) { return item.ID() == viewModel.Selected().DocumentPurpose(); });
        var statementLetter = ko.utils.arrayFirst(viewModel.Parameter().StatementLetter(), function (item) { return item.ID == viewModel.Selected().StatementLetter(); });
        var docType = ko.utils.arrayFirst(viewModel.Parameter().DocumentTypes(), function (item) { return item.ID() == viewModel.Selected().DocumentType(); });
        if (viewModel.DocumentPurpose_a() != null) {
            if (viewModel.DocumentPurpose_a().ID() == 2) {
                viewModel.SelectingUnderlying(true);
                viewModel.SelectingInstruction(false);
            }
            else {
                viewModel.SelectingUnderlying(false);
                viewModel.SelectingInstruction(true);
            }
        }
        if (docType != null) viewModel.DocumentType(docType);
        if (docPurpose != null) viewModel.DocumentPurpose(docPurpose);
        if (viewModel.ActivityTitle() == CONST_TRANSACTION.FX) {
            t_IsFxTransaction = (viewModel.TransactionDealDetailModel().Currency.Code() != 'IDR' && viewModel.TransactionDealDetailModel().DebitCurrency.Code() == 'IDR');
            t_IsFxTransactionToIDR = (viewModel.TransactionDealDetailModel().Currency.Code() == 'IDR' && viewModel.TransactionDealDetailModel().DebitCurrency.Code() != 'IDR');
        } else {
            t_IsFxTransaction = (viewModel.TransactionTMO().BuyCurrency.Code() != 'IDR' && viewModel.TransactionTMO().SellCurrency.Code() == 'IDR');
            t_IsFxTransactionToIDR = (viewModel.TransactionTMO().BuyCurrency.Code() == 'IDR' && viewModel.TransactionTMO().SellCurrency.Code() != 'IDR');
        }


        viewModel.t_IsFxTransaction(t_IsFxTransaction);
        viewModel.t_IsFxTransactionToIDR(t_IsFxTransactionToIDR);
        //SetDependentObservable();
    });
    function SetDependentObservable() {
        ShowNotification("Dependent", 'gritter-error', true);
    }
    function GetRateAmount(CurrencyID) {
        var options = {
            url: api.server + api.url.currency + "/CurrencyRate/" + CurrencyID,
            params: {
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetRateAmount, OnError, OnAlways);
    }

    function OnSuccessGetRateAmount(data, textStatus, jqXHR) {

        if (jqXHR.status = 200) {
            viewModel.TransactionDealDetailModel().Rate(data.RupiahRate);
            viewModel.TransactionTMO().Rate(data.RupiahRate);
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }


    function GetRateIDR() {
        var options = {
            url: api.server + api.url.currency + "/CurrencyRate/" + "1",
            params: {
            },
            token: accessToken
        };

        Helper.Ajax.Get(options, OnSuccessGetRateIDR, OnError, OnAlways);
    }
    function GetPermission() {
        $.ajax({
            type: "GET",
            url: api.server + api.url.helper + "/CheckUserPermited/" + _spFriendlyUrlPageContextInfo.title,
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + $.cookie(api.cookie.name)
            },
            success: function (data, textStatus, jqXHR) {
                if (jqXHR.status = 200) {
                    //compare user role to page permission to get checker validation
                    viewModel.isDealBUGroup(false);
                    for (i = 0; i < spUser.Roles.length; i++) {
                        for (j = 0; j < data.length; j++) {
                            if (Const_RoleName[spUser.Roles[i].ID] == data[j]) { //if (spUser.Roles[i].Name == data[j]) {
                                if (Const_RoleName[spUser.Roles[i].ID].toLowerCase().startsWith("dbs fx") && Const_RoleName[spUser.Roles[i].ID].toLowerCase().endsWith("maker")) { //if (spUser.Roles[i].Name.toLowerCase().startsWith("dbs fx") && spUser.Roles[i].Name.toLowerCase().endsWith("maker")) {
                                    viewModel.isDealBUGroup(true);
                                }
                                if (Const_RoleName[spUser.Roles[i].ID].toLowerCase().startsWith("dbs fx") || Const_RoleName[spUser.Roles[i].ID].toLowerCase().startsWith("dbs business")) { //if (spUser.Roles[i].Name.toLowerCase().startsWith("dbs fx") || spUser.Roles[i].Name.toLowerCase().startsWith("dbs business")) {
                                    viewModel.ActivityTitle("FX");
                                    viewModel.FilterIsTMO(null);
                                } else if (Const_RoleName[spUser.Roles[i].ID].toLowerCase().startsWith("dbs tmo")) { //} else if (spUser.Roles[i].Name.toLowerCase().startsWith("dbs tmo")) {
                                    viewModel.ActivityTitle("TMO");
                                    viewModel.FilterIsTMO(true);
                                }
                            }
                        }
                    }
                    viewModel.GetTransactionData();
                } else {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-warning', true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // send notification
                ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
            }
        });
    }
    function OnSuccessGetRateIDR(data, textStatus, jqXHR) {
        if (jqXHR.status = 200) {
            // bind result to observable array
            idrrate = data.RupiahRate;
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
        }
    }

    //================================= Eqv Calculation ====================================

    var x = document.getElementById("book-amount");
    if (x != null) {
        var y = document.getElementById("book-rate");
        var z = document.getElementById("book-currency");
        var d = document.getElementById("book-eqv-usd");
        var xstored = x.getAttribute("data-in");
        var ystored = y.getAttribute("data-in");

        setInterval(function () {
            if (x == document.activeElement) {
                var temp = x.value;
                if (xstored != temp) {
                    xstored = temp;
                    x.setAttribute("data-in", temp);
                    calculate();
                }
            }
            if (y == document.activeElement) {
                var temp = y.value;
                if (ystored != temp) {
                    ystored = temp;
                    y.setAttribute("data-in", temp);
                    calculate();
                }
            }

            if (y == document.activeElement) {
                var temp = y.value;
                if (ystored != temp) {
                    ystored = temp;
                    y.setAttribute("data-in", temp);
                    calculate();
                }
            }
            if (z == document.activeElement) { // add by dodit 2014/11/8
                calculate();
            }
        }, 100);

        function calculate() {
            /* dodit@2014.11.08:altering calculate process to avoid NaN, avoid formula on USD, and formating feature */
            currency = viewModel.Selected().Currency();
            res = x.value;
            if (currency != 1) { // if not USD
                res = x.value * y.value / idrrate;
                res = Math.round(res * 100) / 100;
            }
            res = isNaN(res) ? 0 : res; //avoid NaN
        }
        x.onblur = calculate;
        calculate();
    }

});

function read_u() {
    //var value = parseFloat(viewModel.Amount_u());

    var e = document.getElementById("Amount_u").value;
    e = e.toString().replace(/,/g, '');

    var res = parseInt(e) * parseFloat(viewModel.Rate_u()) / parseFloat(idrrate);
    res = Math.round(res * 100) / 100;
    res = isNaN(res) ? 0 : res; //avoid NaN
    viewModel.AmountUSD_u(parseFloat(res).toFixed(2));
    viewModel.Amount_u(formatNumber(e));
}
function GetCustomerAutocompleted(element) {
    // autocomplete
    element.autocomplete({
        source: function (request, response) {
            // declare options variable for ajax get request
            var options = {
                url: api.server + api.url.customer + "/Search",
                data: {
                    query: request.term,
                    limit: 20
                },
                token: accessToken
            };

            // exec ajax request
            Helper.Ajax.AutoComplete(options, response, OnSuccessAutoComplete, OnError);
        },
        minLength: 2,
        select: function (event, ui) {
            // set customer data model
            if (ui.item.data != undefined || ui.item.data != null) {
                var mapping = {
                    'ignore': ["LastModifiedDate", "LastModifiedBy"]
                };
                viewModel.TransactionDealDetailModel().Customer(ko.mapping.fromJS(ui.item.data, mapping));
                viewModel.TransactionTMO().Customer(ko.mapping.fromJS(ui.item.data, mapping));
                cifData = ui.item.data.CIF;
                DealModel.cif = cifData;
                customerNameData = ui.item.data.Name;
                //GetTotalDeal(DealModel, OnSuccessTotal, OnErrorDeal);
            }
            else {


                viewModel.TransactionDealDetailModel().Customer(null);
                viewModel.TransactionTMO().Customer(null);
            }
        }
    });
}
function BookTransactioin() {

    viewModel.IsUploading(false);

    //clearing transaction temp attachment
    viewModel.IsFXTransactionAttach(false);
    viewModel.IsUploaded(false);
    viewModel.SelectingUnderlying(false);


    //GET ALL BOOKED TRANSACTION
    viewModel.GetTransactionData();

    $('#s4-workspace').animate({ scrollTop: 0 }, 'fast');
}
// reset model
function ResetModel() {
    viewModel = new ViewModel();
}
// get all parameters need
function GetParameters() {
    var options = {
        url: api.server + api.url.parameter,
        params: {
            select: "Currency,AgentCharge,FXCompliance,ChargesType,DocType,PurposeDoc,statementletter,underlyingdoc,customer,ratetype,producttype"
        },
        token: accessToken
    };

    Helper.Ajax.Get(options, OnSuccessGetParameters, OnError, OnAlways);
}


function UpdateCorrection() {

    if (viewModel.TransactionTMO().Underlyings == null) {
        viewModel.TransactionTMO().Underlyings = ko.observableArray([]);
    } else {
        viewModel.TransactionTMO().Underlyings([]);
    }
    ko.utils.arrayForEach(viewModel.TempSelectedUnderlying(), function (item) {
        viewModel.TransactionTMO().Underlyings().push(new SelectUtillizeTMOModel(item.ID(), false, item.USDAmount(), item.UtilizeAmountDeal(), ''));
    });

    // save transaction  fx
    viewModel.TransactionTMO().IsDraft(false);
    viewModel.TransactionTMO().IsResubmit(true);
    var options = {
        url: api.server + api.url.transactiondeal + '/TMO/updatecorrection/' + viewModel.TransactionTMO().ID(),
        token: accessToken,
        data: ko.toJSON(viewModel.TransactionTMO())
    };
    var obj = ko.toJSON(viewModel.TransactionTMO());
    // if id is null = Save
    Helper.Ajax.Put(options, OnSuccessTMOSaveAPI, OnError, OnAlways);
}
function UpdateTransaction() {

    switch (viewModel.ActivityTitle()) {
        case CONST_TRANSACTION.FX:
            UpdateTransactionFX();
            break;
        case CONST_TRANSACTION.TMO:
            UpdateTransactionTMO();
            break;
    }


}
function UpdateTransactionFX() {
    viewModel.TransactionDealDetailModel().IsBookDeal = viewModel.IsDeal();

    if (viewModel.TransactionDealDetailModel().Underlyings == null) {
        viewModel.TransactionDealDetailModel().Underlyings = ko.observableArray([]);
    } else {
        viewModel.TransactionDealDetailModel().Underlyings([]);
    }
    ko.utils.arrayForEach(viewModel.TempSelectedUnderlying(), function (item) {
        viewModel.TransactionDealDetailModel().Underlyings().push(new SelectUtillizeModel(item.ID(), false, item.USDAmount(), ''));
    });

    // save transaction  fx
    var options = {
        url: api.server + api.url.transactiondeal + '/deal/update/' + viewModel.TransactionDealDetailModel().ID(),
        token: accessToken,
        data: ko.toJSON(viewModel.TransactionDealDetailModel())
    };
    // if id is null = Save
    Helper.Ajax.Put(options, OnSuccessSaveAPI, OnError, OnAlways);
}

function UpdateTransactionTMO() {

    if (viewModel.TransactionTMO().Underlyings == null) {
        viewModel.TransactionTMO().Underlyings = ko.observableArray([]);
    } else {
        viewModel.TransactionTMO().Underlyings([]);
    }
    ko.utils.arrayForEach(viewModel.TempSelectedUnderlying(), function (item) {
        viewModel.TransactionTMO().Underlyings().push(new SelectUtillizeTMOModel(item.ID(), false, item.USDAmount(), item.UtilizeAmountDeal(), ''));
    });

    // save transaction  fx
    var options = {
        url: api.server + api.url.transactiondeal + '/TMO/update/' + viewModel.TransactionTMO().ID(),
        token: accessToken,
        data: ko.toJSON(viewModel.TransactionTMO())
    };
    var obj = ko.toJSON(viewModel.TransactionTMO());
    // if id is null = Save
    Helper.Ajax.Put(options, OnSuccessTMOSaveAPI, OnError, OnAlways);
}

function OnSuccessSaveAPI(data, textStatus, jqXHR) {
    // Get transaction id from api result
    // insert to sharepoint list item
    if (viewModel.IsDeal() == false) {
        AddListItem();
    }
    else {
        ShowNotification('Success', CONST_MSG.SuccessSave, 'gritter-success', true);
        window.location = "/home";
    }
}
function OnSuccessTMOSaveAPI(data, textStatus, jqXHR) {
    // Get transaction id from api result
    // insert to sharepoint list item
    if (viewModel.TransactionTMO().IsDraft() == false) {
        AddListItemTMO();
    }
    else {
        ShowNotification('Success', CONST_MSG.SuccessSave, 'gritter-success', true);
        window.location = "/home";
    }
}
function GetStatusNotification() {
    var output;
    var IsSelectedUnderlying = (viewModel.TempSelectedUnderlying() != null && viewModel.TempSelectedUnderlying().length > 0);
    var document = ko.utils.arrayFilter(viewModel.MakerDocuments(), function (item) {
        return item.Purpose.ID == 1; // Instruction Document
    });

    if (IsSelectedUnderlying && document != null && document.length > 0) {
        return output = 'Underlying Document and Statement Letter has been submited. </br>';
    }
    if (document != null && document.length > 0 && !IsSelectedUnderlying) {
        return output = 'Statement Letter has been submited. Ensure Submission of underlying document. </br>';
    }
    if (document == null && document.length > 0 && IsSelectedUnderlying) {
        return output = 'Underlying Document has been submited. Ensure Submission of statement letter. </br>';
    }

    return CONST_MSG.SuccessSave;
}
function AddListItem() {
    var body = {
        Title: viewModel.TransactionDealDetailModel().ApplicationID(),
        __metadata: {
            type: config.sharepoint.metadata.listDeal
        }
    };
    var options = {
        url: config.sharepoint.url.api + "/Lists(guid'" + config.sharepoint.listIdDeal + "')/Items",
        data: JSON.stringify(body),
        digest: jQuery("#__REQUESTDIGEST").val()
    };
    Helper.Sharepoint.List.Add(options, OnSuccessAddListItem, OnError, OnAlways);
}
function AddListItemTMO() {
    var body = {
        Title: viewModel.TransactionTMO().ApplicationID(),
        __metadata: {
            type: config.sharepoint.metadata.listDeal
        }
    };
    var options = {
        url: config.sharepoint.url.api + "/Lists(guid'" + config.sharepoint.listIdDeal + "')/Items",
        data: JSON.stringify(body),
        digest: jQuery("#__REQUESTDIGEST").val()
    };
    Helper.Sharepoint.List.Add(options, OnSuccessAddListItem, OnError, OnAlways);
}
function ValidateTransaction() {
    var isValid = false; // for bookdeal and draft process
    var t_TreshHold = TotalDealModel.TreshHold;
    var t_TotalAmountsUSD = parseFloat(viewModel.AmountModel().TotalAmountsUSD());
    var t_AmountUSD = viewModel.TransactionDealDetailModel().AmountUSD() != null ? parseFloat(viewModel.TransactionDealDetailModel().AmountUSD()) : parseFloat(viewModel.TransactionTMO().AmountUSD());
    var t_UtilizeAmount = parseFloat(viewModel.utilizationAmount());
    if (event.currentTarget.id == 'btnDraft' || event.currentTarget.id == 'bookDeal') {
        viewModel.TransactionTMO().IsDraft(true);
        isValid = true;
    } else {
        viewModel.TransactionTMO().IsDraft(false);
    }
    //if ((t_IsFxTransaction) || t_AmountUSD >= TotalDealModel.FCYIDRTrashHold) {
    if (isValid || t_AmountUSD <= t_UtilizeAmount) {
        if (!isValid && (t_TotalAmountsUSD > t_TreshHold && viewModel.TempSelectedUnderlying()[0].StatementLetter() == CONST_STATEMENT.StatementA_ID &&
            viewModel.TempSelectedUnderlying()[0].StatementLetter() == CONST_STATEMENT.StatementA_ID)) {
            ShowNotification('Form Underlying Warning", "Total Transaction greater than ' + t_TreshHold + ' (USD), Please add statement B underlying', 'gritter-warning', false);
            viewModel.IsEditable(true);
            viewModel.IsUploaded(true);
            return;
        } else {
            // process update data   
            UploadDocuments();
        }
    } else {
        viewModel.IsEditable(true);
        viewModel.IsUploaded(true);
        ShowNotification("Form Underlying Warning", CONST_MSG.ValidationMinTotal, 'gritter-warning', false);
        return;
    }
    // } 
}
function AddOtherAccounts(accounts) {
    if (accounts != null) {
        var accountData = {
            AccountNumber: ko.observable('-'),
            CIFJoint: ko.observable(null),
            IsJointAccount: ko.observable(false),
            Currency: ko.observable(CurrencyModel)
        };
        accounts.push(accountData);
    }
    return accounts;
}
//self.IsShowSubmit
function GetIsFlowValas(FlowValas) {
    return (FlowValas == null ? false : FlowValas.IsFlowValas());
}
function GetProductTypeID(FlowValas) {
    return (FlowValas == null ? null : FlowValas.ID());
}
function UploadDocuments() {
    // uploading documents. after upload completed, see SaveTransaction()
    var data;
    switch (viewModel.ActivityTitle()) {
        case CONST_TRANSACTION.FX:
            data = {
                ApplicationID: viewModel.TransactionDealDetailModel().ApplicationID(),
                CIF: viewModel.TransactionDealDetailModel().Customer.CIF(),
                Name: viewModel.TransactionDealDetailModel().Customer.Name()
            };
            break;
        case CONST_TRANSACTION.TMO:
            var itemsd = ko.utils.arrayFilter(viewModel.MakerDocuments(), function (iteme) {
                return iteme.Purpose.ID != 2 && iteme.IsNewDocument != true;
            });

            if (itemsd != null && itemsd.length > 0) {
                for (var i = 0; itemsd.length > i; i++) {
                    viewModel.TransactionTMO().Documents.push(itemsd[i]);
                }
            }

            data = {
                ApplicationID: viewModel.TransactionTMO().ApplicationID(),
                CIF: viewModel.TransactionTMO().Customer.CIF(),
                Name: viewModel.TransactionTMO().Customer.Name()
            };
            break;
    }

    // upload hanya document selain underlying
    var items = ko.utils.arrayFilter(viewModel.MakerDocuments(), function (item) {
        return item.Purpose.ID != 2 && item.IsNewDocument == true;
    });

    if (items != null && items.length > 0) {
        viewModel.counterUpload(0);
        for (var i = 0; items.length > i; i++) {
            UploadFile(data, items[i], UpdateTransaction, items.length);
        }
    } else {
        UpdateTransaction();
    }
}

// Event handlers declaration start
function OnSuccessUpload(data, textStatus, jqXHR) {
    ShowNotification("Upload Success", JSON.stringify(data), "gritter-success", true);
}

function OnSuccessGetApplicationID(data, textStatus, jqXHR) {
    // set ApplicationID
    if (data != null) {
        viewModel.TransactionDealDetailModel().ApplicationID(data.ApplicationID);
        ValidateTransaction();
    }
}

function GetApplicationID(newApplicationID) {
    var options = {
        url: api.server + api.url.transactiondeal + "/Set/ApplicationID/",
        params: {
            applicationID: newApplicationID,
            transactionID: viewModel.TransactionID()
        },
        token: accessToken
    };

    Helper.Ajax.Get(options, OnSuccessGetApplicationID, OnError, OnAlways);
};

function OnSuccessGetParameters(data, textStatus, jqXHR) {
    if (jqXHR.status = 200) {
        // bind result to observable array
        var mapping = {
            'ignore': ["LastModifiedDate", "LastModifiedBy"]
        };

        viewModel.Parameter().Currencies = ko.mapping.fromJS(data.Currency, mapping);
        viewModel.Parameter().FXCompliances = ko.mapping.fromJS(data.FXCompliance, mapping);
        viewModel.Parameter().DocumentTypes = ko.mapping.fromJS(data.DocType, mapping);
        viewModel.Parameter().DocumentPurposes = ko.mapping.fromJS(data.PurposeDoc, mapping);
        viewModel.Parameter().ProductType = ko.mapping.fromJS(data.ProductType, mapping);
        viewModel.Parameter().StatementLetter = ko.mapping.fromJS(data.StatementLetter, mapping);

        // underlying parameter
        viewModel.GetUnderlyingParameters(data);
    } else {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
    }
}

function OnSuccessUpdateUnderlying(data, textStatus, jqXHR) {
    if (jqXHR.status != 200) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-success', true);
    }
}

function OnSuccessAddListItem(data, textStatus, jqXHR) {
    var textMessage;
    textMessage = GetStatusNotification();
    ShowNotification("Success", textMessage, "gritter-success", true);
    window.location = "/home";
}

// Autocomplete
function OnSuccessAutoComplete(response, data, textStatus, jqXHR) {
    response($.map(data, function (item) {
        return {
            // default autocomplete object
            label: item.CIF + " - " + item.Name,
            value: item.Name,

            // custom object binding
            data: {
                CIF: item.CIF,
                Name: item.Name,
                POAName: item.POAName,
                Accounts: item.Accounts,
                Branch: item.Branch,
                Contacts: item.Contacts,
                Functions: item.Functions,
                RM: item.RM,
                Type: item.Type,
                Underlyings: item.Underlyings,
                LastModifiedBy: item.LastModifiedBy,
                LastModifiedDate: item.LastModifiedDate
            }
        }
    })
    );
}

// Token validation On Success function
function OnSuccessToken(data, textStatus, jqXHR) {
    // store token on browser cookies
    $.cookie(api.cookie.name, data.AccessToken);
    accessToken = $.cookie(api.cookie.name);

    // read spuser from cookie
    if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
        spUser = $.cookie(api.cookie.spUser);
        viewModel.SPUser(ko.mapping.toJS(spUser));
    }
    // get parameter values
    // call get data inside view model
    //DealModel.token = accessToken;
    //GetParameterData(DealModel, OnSuccessGetTotal, OnErrorDeal);
    DealModel.token = accessToken;
    GetThresholdParameter(DealModel, OnSuccessThresholdPrm, OnErrorDeal);
    GetParameters();
    GetPermission();
}

// delete & upload document underlying start
function DeleteFile(documentPath) {
    // Get the server URL.
    var serverUrl = _spPageContextInfo.webAbsoluteUrl;

    // output variable
    var output;

    // Delete the file to the SharePoint folder.

    var deleteFile = deleteFileToFolder();
    deleteFile.done(function (file, status, xhr) {
        output = file.d;
    });
    deleteFile.fail(OnError);
    // Call function delete File Shrepoint Folder
    function deleteFileToFolder() {
        UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval);
        return $.ajax({
            url: serverUrl + "/_api/web/GetFileByServerRelativeUrl('" + documentPath + "')",
            type: "POST",
            headers: {
                "accept": "application/json;odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val(),
                "X-HTTP-Method": "DELETE",
                "IF-MATCH": "*"
                //"content-length": arrayBuffer.byteLength
            }
        });
    }
}

function UploadFileUnderlying(context, document, callBack) {

    // Define the folder path for this example.
    var serverRelativeUrlToFolder = '/Underlying Documents';

    // Get the file name from the file input control on the page.
    var parts = document.DocumentPath.name.split('.');

    var fileExtension = parts[parts.length - 1];
    var timeStamp = new Date().getTime();
    var fileName = timeStamp + "." + fileExtension; //document.DocumentPath.name;

    // Get the server URL.
    var serverUrl = _spPageContextInfo.webAbsoluteUrl;

    // output variable
    var output;

    // Initiate method calls using jQuery promises.
    // Get the local file as an array buffer.
    var getFile = getFileBufferUnderlying();
    getFile.done(function (arrayBuffer) {

        // Add the file to the SharePoint folder.
        var addFile = addFileToFolderUnderlying(arrayBuffer);
        addFile.done(function (file, status, xhr) {
            output = file.d;

            // Get the list item that corresponds to the uploaded file.
            var getItem = getListItemUnderlying(file.d.ListItemAllFields.__deferred.uri);
            getItem.done(function (listItem, status, xhr) {
                // Insert MakerUnderlyings (underlyingID) untuk mapping document
                //ko.arrayFilter(viewModel.TempSelectedAttachUnderlying(), function (underlyingID) {
                //    viewModel.MakerUnderlyings.push(underlyingID);
                //});

                //return output;
                var documentData = {
                    ID: 0,
                    Type: document.Type,
                    Purpose: document.Purpose,
                    LastModifiedDate: null,
                    LastModifiedBy: null,
                    DocumentPath: output.ServerRelativeUrl,
                    FileName: document.DocumentPath.name
                };
                switch (viewModel.ActivityTitle()) {
                    case CONST_TRANSACTION.FX:
                        viewModel.TransactionDealDetailModel().Documents.push(documentData);
                        break;
                    case CONST_TRANSACTION.TMO:
                        viewModel.TransactionTMO().Documents.push(documentData);
                        break;
                }
                // Change the display name and title of the list item.
                var changeItem = updateListItemUnderlying(listItem.d.__metadata);
                changeItem.done(function (data, status, xhr) {
                    DocumentModels({ "FileName": document.DocumentPath.name, "DocumentPath": output.ServerRelativeUrl });
                    callBack();
                });
                changeItem.fail(OnError);
            });
            getItem.fail(OnError);
        });
        addFile.fail(OnError);
    });
    getFile.fail(OnError);

    // Get the local file as an array buffer.
    function getFileBufferUnderlying() {
        var deferred = $.Deferred();
        var reader = new FileReader();
        reader.onloadend = function (e) {
            deferred.resolve(e.target.result);
        }
        reader.onerror = function (e) {
            deferred.reject(e.target.error);
        }
        reader.readAsArrayBuffer(document.DocumentPath);
        return deferred.promise();
    }

    // Add the file to the file collection in the Shared Documents folder.
    function addFileToFolderUnderlying(arrayBuffer) {

        // Construct the endpoint.
        var fileCollectionEndpoint = String.format(
                "{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files" +
                "/add(overwrite=false, url='{2}')",
            serverUrl, serverRelativeUrlToFolder, fileName);

        // Send the request and return the response.
        UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval);
        // This call returns the SharePoint file.
        return $.ajax({
            url: fileCollectionEndpoint,
            type: "POST",
            data: arrayBuffer,
            processData: false,
            headers: {
                "accept": "application/json;odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val()
            }
        });
    }

    // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
    function getListItemUnderlying(fileListItemUri) {

        // Send the request and return the response.
        return $.ajax({
            url: fileListItemUri,
            type: "GET",
            headers: { "accept": "application/json;odata=verbose" }
        });
    }

    // Change the display name and title of the list item.
    function updateListItemUnderlying(itemMetadata) {

        // Define the list item changes. Use the FileLeafRef property to change the display name.
        // For simplicity, also use the name as the title.
        // The example gets the list item type from the item's metadata, but you can also get it from the
        // ListItemEntityTypeFullName property of the list.
        var body = {
            Title: document.DocumentPath.name,
            Application_x0020_ID: context.ApplicationID,
            CIF: context.CIF,
            Customer_x0020_Name: context.Name,
            Document_x0020_Type: context.Type.DocTypeName,
            Document_x0020_Purpose: context.Purpose.Name,
            __metadata: {
                type: itemMetadata.type,
                FileLeafRef: fileName,
                Title: fileName
            }
        };
        UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval);
        // Send the request and return the promise.
        // This call does not return response content from the server.
        return $.ajax({
            url: itemMetadata.uri,
            type: "POST",
            data: ko.toJSON(body),
            headers: {
                "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                "content-type": "application/json;odata=verbose",
                //"content-length": body.length,
                "IF-MATCH": itemMetadata.etag,
                "X-HTTP-Method": "MERGE"
            }
        });
    }
}

// Upload the file
function UploadFile(context, document, callBack, countItem) {

    var serverRelativeUrlToFolder = '/Instruction Documents';

    var parts = document.DocumentPath.name.split('.');

    var fileExtension = parts[parts.length - 1];
    var timeStamp = new Date().getTime();
    var fileName;
    if (parts[parts.length - 2] != null) {
        fileName = parts[parts.length - 2].replace(/[<>:"\/\\|?!@#$%^&*]+/g, '_') + timeStamp + "." + fileExtension; //aridya 20170131 handle special char //document.DocumentPath.name;
    } else {
        fileName = timeStamp + "." + fileExtension; //document.DocumentPath.name;
    }

    // Get the server URL.
    var serverUrl = _spPageContextInfo.webAbsoluteUrl;

    // output variable
    var output;

    // Get the local file as an array buffer.
    var getFile = getFileBuffer();
    getFile.done(function (arrayBuffer) {

        // Add the file to the SharePoint folder.
        var addFile = addFileToFolder(arrayBuffer);
        addFile.done(function (file, status, xhr) {
            output = file.d;

            // Get the list item that corresponds to the uploaded file.
            var getItem = getListItem(file.d.ListItemAllFields.__deferred.uri);
            getItem.done(function (listItem, status, xhr) {

                // Change the display name and title of the list item.
                var changeItem = updateListItem(listItem.d.__metadata);
                changeItem.done(function (data, status, xhr) {

                    //return output;
                    var documentData = {
                        ID: 0,
                        Type: document.Type,
                        Purpose: document.Purpose,
                        LastModifiedDate: null,
                        LastModifiedBy: null,
                        DocumentPath: output.ServerRelativeUrl,
                        FileName: document.DocumentPath.name
                    };
                    switch (viewModel.ActivityTitle()) {
                        case CONST_TRANSACTION.FX:
                            viewModel.TransactionDealDetailModel().Documents.push(documentData);
                            break;
                        case CONST_TRANSACTION.TMO:
                            viewModel.TransactionTMO().Documents.push(documentData);
                            break;
                    }
                    viewModel.counterUpload(viewModel.counterUpload() + 1);
                    if (countItem == viewModel.counterUpload()) {
                        callBack();
                    }
                    return 1;

                    //ResetTransactionModel(viewModel.TransactionDealDetailModel());
                    // viewModel.TransactionDealDetailModel().Documents.push(kodok);

                    //callBack();
                });
                changeItem.fail(OnError);
            });
            getItem.fail(OnError);
        });
        addFile.fail(OnError);
    });
    getFile.fail(OnError);

    // Get the local file as an array buffer.
    function getFileBuffer() {
        var deferred = $.Deferred();
        var reader = new FileReader();
        reader.onloadend = function (e) {
            deferred.resolve(e.target.result);
        }
        reader.onerror = function (e) {
            deferred.reject(e.target.error);
        }

        reader.readAsArrayBuffer(document.DocumentPath);
        return deferred.promise();
    }

    // Add the file to the file collection in the Shared Documents folder.
    function addFileToFolder(arrayBuffer) {

        // Construct the endpoint.
        var fileCollectionEndpoint = String.format(
                "{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files" +
                "/add(overwrite=false, url='{2}')",
            serverUrl, serverRelativeUrlToFolder, fileName);
        UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval);
        // Send the request and return the response.
        // This call returns the SharePoint file.
        return $.ajax({
            url: fileCollectionEndpoint,
            type: "POST",
            data: arrayBuffer,
            processData: false,
            headers: {
                "accept": "application/json;odata=verbose",
                "X-RequestDigest": $("#__REQUESTDIGEST").val()
            }
        });
    }

    // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
    function getListItem(fileListItemUri) {

        // Send the request and return the response.
        return $.ajax({
            url: fileListItemUri,
            type: "GET",
            headers: { "accept": "application/json;odata=verbose" }
        });
    }

    function ResetTransactionModel(data) {
        viewModel.TransactionDealDetailModel = ko.observable(TransactionDealDetailModel);
        viewModel.TransactionDealDetailModel(data);
    }

    // Change the display name and title of the list item.
    function updateListItem(itemMetadata) {
        var body = {
            Title: document.DocumentPath.name,
            Application_x0020_ID: context.ApplicationID,
            CIF: context.CIF,
            Customer_x0020_Name: context.Name,
            Document_x0020_Type: document.Type.DocTypeName,
            Document_x0020_Purpose: document.Purpose.Name,
            __metadata: {
                type: itemMetadata.type,
                FileLeafRef: fileName,
                Title: fileName
            }
        };
        UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval);
        // Send the request and return the promise.
        // This call does not return response content from the server.
        return $.ajax({
            url: itemMetadata.uri,
            type: "POST",
            data: ko.toJSON(body),
            headers: {
                "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                "content-type": "application/json;odata=verbose",
                //"content-length": body.length,
                "IF-MATCH": itemMetadata.etag,
                "X-HTTP-Method": "MERGE"
            }
        });
    }
}
// AJAX On Error Callback
function OnError(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}
/*function OnSuccessGetTotal(dataCall) {
    idrrate = dataCall.RateIDR;
}*/
function OnSuccessThresholdPrm() {
    idrrate = DataModel.RateIDR;
}
function OnSuccessTotal() {
    switch (viewModel.ActivityTitle()) {
        case CONST_TRANSACTION.FX:
            if (viewModel.TransactionDealDetailModel().AmountUSD() != null) {
                viewModel.CalculateFX(viewModel.TransactionDealDetailModel().AmountUSD());
            } else {
                viewModel.CalculateFX(0);
            }
            break;
        case CONST_TRANSACTION.TMO:
            if (viewModel.TransactionTMO().AmountUSD() != null) {
                viewModel.CalculateFX(viewModel.TransactionTMO().AmountUSD());
            } else {
                viewModel.CalculateFX(0);
            }
            break;
    }

}
function OnErrorDeal(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}
// AJAX On Alway Callback
function OnAlways() {
    // enabling form input controls
    // viewModel.IsEditable(true);
}

function SetTopUrgentTBO() {
    if (document.getElementById('check-document-tobeobtainedtmo').checked == true) {
        viewModel.ToBeObtained(true);
        viewModel.IsToBeObtained(true);
        viewModel.dynamicTableTBO()[viewModel.dynamicTableTBO().length - 1].ExpectedDateTBO(self.LocalDate(new Date().toUTCString(), true));
    } else {
        viewModel.IsToBeObtained(false);
        viewModel.ToBeObtained(false);
    }
}

function BindingDataTBO(index) {
    var TBOTMOID;
    var TBOTMOName;
    var data = [];
    var datatmobind = [];
    var Index = index - 1;


    if (viewModel.dynamicTableTBO()[Index].TBOTMOID() == Const_TBOSLADoc.MT103) {
        TBOTMOID = viewModel.dynamicTableTBO()[Index].TBOTMOID();
        TBOTMOName = 'MT103';
        viewModel.dynamicTableTBO()[Index].TBOTMOID(TBOTMOID);
        viewModel.dynamicTableTBO()[Index].TBOTMOName(TBOTMOName);
    } else {
        if (viewModel.dynamicTableTBO()[Index].TBOTMOID() == Const_TBOSLADoc.Other) {
            TBOTMOID = viewModel.dynamicTableTBO()[Index].TBOTMOID();
            TBOTMOName = 'Other'
            viewModel.dynamicTableTBO()[Index].TBOTMOID(TBOTMOID);
            viewModel.dynamicTableTBO()[Index].TBOTMOName(TBOTMOName);
        } else {
            TBOTMOID = 0;
            TBOTMOName = '';
            viewModel.dynamicTableTBO()[Index].TBOTMOID(TBOTMOID);
            viewModel.dynamicTableTBO()[Index].TBOTMOName(TBOTMOName);
        }
    }

    if (viewModel.dynamicTableTBO().length > 0) {
        if (viewModel.dynamicTableTBO()[Index].TBOTMOID == Const_TBOSLADoc.MT103) {
            viewModel.IsSubmitValidationTBO(true);
        } else {
            if (viewModel.dynamicTableTBO()[Index].TBOTMOID == 0) {
                viewModel.IsSubmitValidationTBO(true);
            } else {
                viewModel.IsSubmitValidationTBO(false);
            }
        }
    }
}
