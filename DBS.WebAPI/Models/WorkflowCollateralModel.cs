﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using DBS.Entity;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Models;
using System.Net.Http;
using System.Web.Script.Serialization;

namespace DBS.WebAPI.Models
{
    #region Property
    #endregion

    #region Interface
    public interface IWorkflowCollateralRepository : IDisposable
    {

    }
    #endregion
    #region Repository
    public class WorkflowCollateralRepository : IWorkflowCollateralRepository
    {
        private DBSEntities context = new DBSEntities();
        private CurrentUser currentUser = new CurrentUser();

        /// <summary>
        /// Add method below...
        /// </summary>




        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (context != null)
                    {
                        context.Dispose();
                        currentUser = null;
                    }
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    #endregion
}