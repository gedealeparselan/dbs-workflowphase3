﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DBS.WebAPI.Models;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace DBS.WebAPI.Controllers
{
    public class FindingEmailTimeController : ApiController
    {
        private string message = string.Empty;
        private readonly IFindingEmailTime repository;

        public FindingEmailTimeController()
        {
            this.repository = new FindingEmailTimeRepository();
        }

        public FindingEmailTimeController(IFindingEmailTime repository) {
            this.repository = repository;
        }

        //Get All Data Finding Email Time
        [ResponseType(typeof(IList<FindingEmailTimeModel>))]
        public HttpResponseMessage Get()
        {
            IList<FindingEmailTimeModel> output = new List<FindingEmailTimeModel>();

            if (!repository.GetFindingEmailTime(ref output, 10, 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<FindingEmailTimeModel>>(HttpStatusCode.OK, output);
            }
        }

        [ResponseType(typeof(FindingEmailTimeGrid))]
        [AcceptVerbs("GET")]
        [Route("api/FindingEmailTime")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<FindingEmailTimeModelFilter> filters)
        {
            FindingEmailTimeGrid output = new FindingEmailTimeGrid();

            if (!repository.GetFindingEmailTime(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "EmailTime" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<FindingEmailTimeGrid>(HttpStatusCode.OK, output);
            }
        }


        [ResponseType(typeof(IList<FindingEmailTimeModel>))]
        public HttpResponseMessage GetAll(int? limit, int? index)
        {
            IList<FindingEmailTimeModel> model = new List<FindingEmailTimeModel>();
            if (!repository.GetFindingEmailTime(ref model, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<IList<FindingEmailTimeModel>>(HttpStatusCode.OK, model);
            }
        }

        //serach
        [ResponseType(typeof(IList<FindingEmailTimeModel>))]
        [HttpGet]
        [Route("api/FindingEmailTime/Search")]
        public HttpResponseMessage Search(string key, int? limit)
        {
            IList<FindingEmailTimeModel> result = new List<FindingEmailTimeModel>();

            if (!repository.GetFindingEmailTime(key, limit.HasValue ? limit.Value : 10, ref result, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<FindingEmailTimeModel>>(HttpStatusCode.OK, result);
            }
        }

         ////<summary>
         ////Get FindingEmailTime by ID.
         ////</summary>
         ////<param name="id">FindingEmailTime ID.</param>
         ////<returns></returns>
        [ResponseType(typeof(FindingEmailTimeModel))]
        [Route("api/FindingEmailTime/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            FindingEmailTimeModel output = new FindingEmailTimeModel();

            if (!repository.GetFindingEmailTimeByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<FindingEmailTimeModel>(HttpStatusCode.OK, output);
            }
        }

        ///Create New Data Finding Email Time
        [HttpPost]
        [ResponseType(typeof(string))]
        [Route("api/FindingEmailTime")]
        public HttpResponseMessage AddFindingEmailTime(FindingEmailTimeModel Model)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddFindingEmaliTime( Model, ref  message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New Finding Email Time data has been created." });
                }
            }
        }

        /// <summary>
        /// Remove exisiting FindingEmailTime.
        /// </summary>
        /// <param name="id">ID of Finding Email Time to be deleted.</param>
        /// <returns></returns>
        /// delete Finding Email Time
        [HttpDelete]
        [Route("api/FindingEmailTime/{id}")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.deleteFindingEmailTime(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "FindingEmailTime data has been deleted." });
                }
            }
        }

        //Update data FindingEmail Time
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/FindingEmailTime/{id}")]
        public HttpResponseMessage Update(int id, FindingEmailTimeModel Model)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else 
            {
                if (!repository.UpdateFindingEmailTime(id, Model, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { ID = id, Message = message });
                }
            }
        }
    } 
}