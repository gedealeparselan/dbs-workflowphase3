﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;

using Nintex.Workflow;
using Nintex.Workflow.Common;
using Nintex.Workflow.HumanApproval;

using DBS.Sharepoint.ControlTemplates;
using DBS.Sharepoint.Helpers;

namespace DBS.Sharepoint.Layouts.DBS.Sharepoint
{
    public partial class CustomTaskForm : Nintex.Workflow.ServerControls.NintexLayoutsBase
    {
        public NintexContext CurrentContext { get; set; }
        public BaseUserControl CurrentControl { get; set; }
        public bool IsTaskComplete { get; set; }
        /// <summary>
        /// Page Init
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Init(object sender, EventArgs e)
        {
            // try.. catch
            try
            {
                CompleteTaskButton.Enabled = false;
                // These querystrings are provided when the page is linked as a task edit form
                if (Page.Request.QueryString["ID"] == null || Page.Request.QueryString["List"] == null)
                {
                    throw new SPException("Failed to get task ID or List Id from query string.");
                }
                int spTaskItemId = int.Parse(Page.Request.QueryString["ID"]);
                Guid taskListId = new Guid(Page.Request.QueryString["List"]);
                // create Nintext context
                this.CurrentContext = NintexHelper.ParseRequest(Web, spTaskItemId, taskListId);
                this.CurrentContext.TaskAuthorized = Nintex.Workflow.HumanApproval.User.CheckCurrentUserMatchesHWUser(Web, CurrentContext.Approver);
                // check if a task has already completed
                IsTaskComplete = (CurrentContext.Approver.ApprovalOutcome != Outcome.Pending);
                // IsTaskLocked
                CompleteTaskButton.Enabled = this.CurrentContext.TaskAuthorized;
                string controlUrl = string.Empty;
                // Wodc.Nes.VoorCodering.Codeur1
                string contentAction = CurrentContext.TaskItem.ContentType.Name;
                string appParam = string.Empty;
                string modParam = string.Empty;
                if (contentAction.IndexOf('.') > -1)
                {
                    string[] actionParams = contentAction.Split('.');
                    appParam = actionParams[0];
                    modParam = actionParams[1];
                    controlUrl = String.Format("~/_ControlTemplates/{0}/{1}Control.ascx", appParam, modParam);
                    this.PageTitleInTitleAreaLiteral.Text = modParam;
                    this.HeaderLabel.Text = this.CurrentContext.TaskItem["Title"].ToString();
                }
                // control
                CurrentControl = (BaseUserControl)this.LoadControl(controlUrl);
                CurrentControl.ID = modParam + "_UID";
                CurrentControl.CurrentContext = this.CurrentContext;
                ControlPlaceHolder.Controls.Add(CurrentControl);
            }
            catch (Exception ex)
            {
                // show error
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if ((IsTaskComplete) || (!this.CurrentContext.TaskAuthorized))
            {
                // set form disabed
            }
        }
        protected void CloseButton_Click(object sender, EventArgs e)
        {
            Utility.RedirectOrCloseDialog(HttpContext.Current, Web.Url);
        }
        /// <summary>
        /// Complete step
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CompleteTaskButton_Click(object sender, EventArgs e)
        {
            // already complete
            if (IsTaskComplete)
            {
                Utility.RedirectOrCloseDialog(HttpContext.Current, Web.Url);
            }
            // call control save
            TaskResult result = CurrentControl.SaveTask();
            if (result != TaskResult.Invalid)
            {
                if (result == TaskResult.Completed)
                {
                    NintexHelper.CompleteTask(CurrentContext);
                }
                else
                {
                    NintexHelper.RejectTask(CurrentContext);
                }
                // redirect
                Utility.RedirectOrCloseDialog(HttpContext.Current, Web.Url);
            }
        }
    }
}
