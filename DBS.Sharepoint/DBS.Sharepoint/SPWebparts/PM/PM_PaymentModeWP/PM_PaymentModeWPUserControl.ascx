﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PM_PaymentModeWPUserControl.ascx.cs" Inherits="DBS.Sharepoint.SPWebparts.PM.PM_PaymentModeWP.PM_PaymentModeWPUserControl" %>


<h1 class="header smaller lighter dark">Payment Mode</h1>
<div class="modal-body overflow-visible">
    <div class="row">
        <div class="col-xs-12">
            <div id="customer-form" class="form-horizontal" role="form">
                <div class="form-group no-margin-bottom no-padding-bottom">
                    <label class="col-sm-2" for="SingleValue">
                        Payment Mode Name
                    </label>
                    <div class="col-sm-8">
                        <div class="clearfix">
                            <select id="modeName" name="modeName" data-bind="options: options, optionsText: 'name', optionsValue: 'id', value: SingleValue, event: { change: $root.OnChangeModeName }"></select>
                            <button class="btn btn-sm btn-primary" data-bind="click: $root.UpdateValue, visible: $root.IsRoleMaker()">
                                <i class="icon-save"></i>
                                Save
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/additional-methods.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.slimscroll.min.js"></script>
