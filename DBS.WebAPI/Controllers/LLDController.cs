﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class LLDController : ApiController
    {
        private string message = string.Empty;

        private readonly ILLDRepository repository = null;

        public LLDController()
        {
            this.repository = new LLDRepository();
        }

        public LLDController(ILLDRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all LLDs.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<LLDModel>))]
        public HttpResponseMessage Get()
        {
            IList<LLDModel> output = new List<LLDModel>();

            if (!repository.GetLLD(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<LLDModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get LLDs with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<LLDModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<LLDModel> output = new List<LLDModel>();

            if (!repository.GetLLD(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<LLDModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get LLDs for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(LLDGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<LLDFilter> filters)
        {
            LLDGrid output = new LLDGrid();

            if (!repository.GetLLD(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Code" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<LLDGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find LLDs using criterias Code or Description.
        /// </summary>
        /// <param name="query">Code or Description.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<LLDModel>))]
        [HttpGet]
        [Route("api/LLD/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<LLDModel> output = new List<LLDModel>();

            if (!repository.GetLLD(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<LLDModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get LLD by ID.
        /// </summary>
        /// <param name="id">LLD ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(LLDModel))]
        [Route("api/LLD/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            LLDModel output = new LLDModel();

            if (!repository.GetLLDByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<LLDModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get LLD by Product.
        /// </summary>
        /// <param name="id">LLD ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<LLDModel>))]
        [Route("api/LLD/byProduct")]
        [HttpGet]
        public HttpResponseMessage GetbyProduct(int id , string name)  
        {
            IList<LLDModel> output = new List<LLDModel>();

            if (!repository.GetLLDbyProduct(ref output, id, name, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<LLDModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new LLD.
        /// </summary>
        /// <param name="LLD">LLD data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(LLDModel LLD)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddLLD(LLD, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New LLD data has been created." });
                }
            }
        }

        /// <summary>
        /// Modify existing LLD.
        /// </summary>
        /// <param name="id">ID of LLD to be modify.</param>
        /// <param name="LLD">LLD data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/LLD/{id}")]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(int id, LLDModel LLD)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateLLD(id, LLD, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "LLD data has been updated." });
                }
            }
        }

        /// <summary>
        /// Remove exisiting LLD.
        /// </summary>
        /// <param name="id">ID of LLD to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/LLD/{id}")]
        [ResponseType(typeof(string))]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteLLD(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "LLD data has been deleted." });
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}