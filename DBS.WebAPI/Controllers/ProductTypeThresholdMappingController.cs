﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    public class ProductTypeThresholdMappingController : ApiController
    {
        private string message = string.Empty;

        private readonly IProductTypeThresholdMapping repository = null;

        public ProductTypeThresholdMappingController()
        {
            this.repository = new ProductTypeThresholdModelRepository();
        }

        public ProductTypeThresholdMappingController(IProductTypeThresholdMapping repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all Product Type Threshold.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<ThresholdGroupModel>))]
        [HttpGet]
       
        public HttpResponseMessage Get()
        {
            IList<ThresholdGroupModel> output = new List<ThresholdGroupModel>();

            if (!repository.GetProductTypeThresholdMapping(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<ThresholdGroupModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get Product Type Threshold with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<ThresholdGroupModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<ThresholdGroupModel> output = new List<ThresholdGroupModel>();

            if (!repository.GetProductTypeThresholdMapping(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<IList<ThresholdGroupModel>>(HttpStatusCode.OK, output);
            }
        }

        ///// For Dropdown
        //[ResponseType(typeof(IList<ThresholdGroupModel>))]
        //[Route("api/ProductTypeThresholdMapping/Search")]
        //public HttpResponseMessage Get(int? limit, int? index)
        //{
        //    IList<ThresholdGroupModel> output = new List<ThresholdGroupModel>();

        //    if (!repository.getThreshold(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
        //    {
        //        return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
        //    }
        //    else
        //    {
        //        return Request.CreateResponse<IList<ThresholdGroupModel>>(HttpStatusCode.OK, output);
        //    }
        //}


        /// <summary>
        /// Get Product Type Threshold for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(ThresholdGroupModelGrid))]
        [AcceptVerbs("GET", "POST")]
        [Route("api/ProductTypeThresholdMapping")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<ThresholdGroupModelFilter> filters)
        {
            ThresholdGroupModelGrid output = new ThresholdGroupModelGrid();

            if (!repository.GetProductTypeThresholdMapping(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "Group Name" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
            }
            else
            {
                return Request.CreateResponse<ThresholdGroupModelGrid>(HttpStatusCode.OK, output);
            }
        }


        /// <summary>
        /// Get Product Type Threshold by ID.
        /// </summary>
        /// <param name="id">ProductMappingID</param>
        /// <returns></returns>
        [ResponseType(typeof(ThresholdGroupModel))]
        [Route("api/ProductTypeThresholdMapping/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            ThresholdGroupModel output = new ThresholdGroupModel();

            if (!repository.GetProductTypeThresholdMappingByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<ThresholdGroupModel>(HttpStatusCode.OK, output);
            }
        }



        /// <summary>
        /// Create a new Product Mapping
        //Add new
        /// </summary>
        /// <param name="ProductTypeCode">Product Type Code</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        [Route("api/ProductTypeThresholdMapping")]
        public HttpResponseMessage Post(ThresholdGroupModel ThresholdGroup)
        {
            
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddProductTypeThresholdMapping(ThresholdGroup, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New Threshold mapping product has been created." });
                }
            }
        }

        /// <summary>
        /// Modify existing product type threshold mapping.
        /// </summary>
        /// <param name="id">ID of product type mapping to be modify.</param>
        /// <param name="threshold">Product type mapping data to be updated.</param>
        /// <returns></returns>
        /// Update Data ThresholdGroup
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/ProductTypeThresholdMapping/{ID}")]
        public HttpResponseMessage Put(int id, ThresholdGroupModel ThresholdGroup)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateProductThreshold(id, ThresholdGroup, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "Product Type Mapping data has been updated." });
                }
            }
        }

        //Delete Data
        [HttpDelete]
        [Route("api/ProductTypeThresholdMapping/{id}")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteProductTypeThresholdMapping(id, ref message))
                {
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, message);
                }
                else
                {
                    return Request.CreateResponse<string>(HttpStatusCode.OK, "Product Type Threshold Mapping data has been deleted.");
                }
            }
        }
    }
}