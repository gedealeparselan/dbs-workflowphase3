﻿using DBS.Sharepoint.Helpers;
using Excel;
using Microsoft.SharePoint;
using Nintex.Workflow.HumanApproval;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.ServiceModel.Activation;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using DBS.Sharepoint.Entity;

namespace DBS.Sharepoint
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class UploadDraftServices : IUploadDraftServices
    {
        public Test HelloWorld()
        {
            return new Test { LocalName = "dsada", RemoteName = "asdas", Size = 41 };
        }

        public UploadFileInfo UploadFile(UploadFileModel data)
        {
            UploadFileInfo result;
            string message = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(data.Path))
                {
                    switch (data.Type)
                    {
                        // Read txt file
                        case "text/plain":
                            Text(data, ref message);
                            break;

                        // Read xlsx format
                        case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                            Excel(data, ref message);
                            break;

                        // Read xls format
                        case "application/vnd.ms-excel":
                            Excel(data, ref message);
                            break;

                        default:
                            message = "File type does not support for upload function.";
                            break;
                    }

                    if (string.IsNullOrEmpty(message))
                    {
                        result = new UploadFileInfo
                        {
                            Message = "Upload File Success."
                        };
                    }
                    else
                    {
                        result = new UploadFileInfo
                        {
                            Message = message
                        };
                    }
                }
                else
                {
                    result = new UploadFileInfo
                    {
                        Message = "File does not exist."
                    };
                }
            }
            catch (Exception ex)
            {
                result = new UploadFileInfo
                {
                    Message = ex.Message
                };
            }

            return result;
        }

        //Read Data From excel
        public void Excel(UploadFileModel model, ref string message)
        {
            DataSet result = new DataSet();

            if (ReadStreamToDataSet(model, ref result, ref message))
            {
                DBSSharePointEntities context = new DBSSharePointEntities();

                StringBuilder sb = new StringBuilder();

                //get customers
                var customer = (from a in context.Customers select a).ToList();

                //get bank
                var bank = (from a in context.Banks select a).ToList();

                //get charges
                var charges = (from a in context.Charges select a).ToList();
                StringBuilder stringbuilder = new StringBuilder();

                for (int a = 0; a < result.Tables[0].Rows.Count - 1; a++)
                {
                    if (result.Tables[0].Rows[a][0].ToString() == "R")
                    {
                        #region Break Down Value
                        //4	    Debit Acc
                        //message = "Debit Acc";
                        var DebitAcc = result.Tables[0].Rows[a][4].ToString();

                        //5 	ValueDate
                        //message = result.Tables[0].Rows[a][5].ToString();
                        var ValueDate = result.Tables[0].Rows[a][5].ToString().Substring(0, 2) + "/" + result.Tables[0].Rows[a][5].ToString().Substring(2, 2) + "/" + result.Tables[0].Rows[a][5].ToString().Substring(4, 4);

                        //6	    Amount
                        //message = "Amount";
                        var Amount = result.Tables[0].Rows[a][6].ToString();

                        //7	    Bene Name
                        //message = "Bene Name";
                        var BeneName = result.Tables[0].Rows[a][7].ToString();

                        //9	    Bene Acc
                        //message = "Bene Acc";
                        var BeneAcc = result.Tables[0].Rows[a][9].ToString();

                        //10	Bank Code
                        //message = "Bank Code";
                        string var1 = result.Tables[0].Rows[a][10].ToString();
                        var bankId = (from x in bank where x.BankCode.Equals(var1) select x.BankID);
                        var BankId = bankId != null ? bankId.First().ToString() : null;

                        //11	Branch Code
                        //message = "Branch Code";
                        var BranchCode = result.Tables[0].Rows[a][11].ToString();

                        //17	Payment Detail
                        //message = "Payment Detail";
                        var PaymentDetail = result.Tables[0].Rows[a][17].ToString();

                        //34	our
                        //message = "our";
                        string var2 = result.Tables[0].Rows[a][34].ToString();
                        var bankChargesId = (from x in charges where x.ChargesCode.Equals(var2) select x.ChargesID);
                        var BankChargesId = bankChargesId != null ? bankChargesId.First().ToString() : null;

                        //35	ben
                        //message = "ben";
                        string var3 = result.Tables[0].Rows[a][35].ToString();
                        var agentChargesId = (from x in charges where x.ChargesCode.Equals(var3) select x.ChargesID);
                        var AgentChargesId = agentChargesId != null ? agentChargesId.First().ToString() : null;
                        #endregion

                        sb.Append("INSERT [Data].[TransactionDraft] ( "
                            //+ "[CIF], "
                            //+ "[BizSegmentID], "
                                + "[AccountNumber], "
                                + "[ApplicationDate], "
                                + "[Amount], "
                                + "[BeneName], "
                                + "[BeneAccNumber], "
                                + "[BankID], "
                                + "[PaymentDetails], "
                                + "[IsResident], "
                                + "[IsCitizen], "
                                + "[BankChargesID], "
                                + "[AgentChargesID], "
                                + "[Type], "
                                + "[IsTopUrgent], "
                                + "[IsNewCustomer], "
                            //+ "[IsSignatureVerified], "
                            //+ "[IsDormantAccount], "
                            //+ "[IsFrezeAccount], "
                            //+ "[IsDocumentComplete], "
                                + "[IsDraft], "
                                + "[CreateDate], "
                                + "[CreateBy]"
                                + ") VALUES (");

                        sb.Append(
                            "'" + DebitAcc + "', " +
                            "convert(datetime, '" + ValueDate + "', 103), " +
                            "" + Amount + ", " +
                            "'" + BeneName + "', " +
                            "" + BeneAcc + ", " +
                            "" + BankId + ", " +
                            "'" + PaymentDetail + "', " +
                            "1, 1, " +
                            "" + BankChargesId + ", " +
                            "" + AgentChargesId + ", " +
                            "20, 0, 0, 1, " +
                            "'" + DateTime.UtcNow + "', " +
                            "'" + "Unknown" + "')"
                            );
                    }
                }

                //Execute string and insert data into database
                DataHelper.ExecuteNonQuery(sb.ToString());
            }
        }

        public bool ReadStreamToDataSet(UploadFileModel model, ref DataSet result, ref string message)
        {
            bool isSuccess = false;

            try
            {
                using (SPSite site = new SPSite(SPContext.Current.Site.RootWeb.Url))
                {
                    using (SPWeb web = site.OpenWeb())
                    {
                        SPList myList = web.Lists["Instruction Documents"];
                        SPListItem myListItem = myList.GetItemById(model.ID);

                        SPFile attchFile = web.GetFile(site.Url + "/" + myListItem.Url);

                        Stream stream = attchFile.OpenBinaryStream();

                        IExcelDataReader excelReader = null;

                        if (stream != null)
                        {
                            switch (model.Type)
                            {
                                case "application/vnd.ms-excel":
                                    //Reading from a binary Excel file ('97-2003 format; *.xls)
                                    excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                                    break;

                                case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                                    //Reading from a OpenXml Excel file (2007 format; *.xlsx)
                                    excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                                    break;
                            }
                        }

                        if (string.IsNullOrEmpty(message))
                        {
                            excelReader.IsFirstRowAsColumnNames = true;
                            result = excelReader.AsDataSet();
                            excelReader.Close();
                            isSuccess = true;
                        }

                        //Close stream
                        stream.Close();
                        //fileStream.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return isSuccess;
        }

        public void Text(UploadFileModel model, ref string message)
        {
            if (!string.IsNullOrEmpty(model.Path))
            {
                string[] file = model.Path.Split('.');

                if ((file[0] != null) && (file[1] == "txt"))
                {
                    string[] type = file[0].Split('_');

                    switch (type[type.Length - 1])
                    {
                        case "RT":
                            FileHelper.ReadRTGS(model, ref message);
                            break;
                        case "SK":
                            FileHelper.ReadSKN(model, ref message);
                            break;
                        default:
                            message = "Product type does not exist.";
                            break;
                    }
                }
            }
        }
    }
}
