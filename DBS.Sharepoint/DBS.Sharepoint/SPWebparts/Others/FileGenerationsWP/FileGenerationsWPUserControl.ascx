﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FileGenerationsWPUserControl.ascx.cs" Inherits="DBS.Sharepoint.SPWebparts.Others.FileGenerationsWP.FileGenerationsWPUserControl" %>


<h1 class="header smaller lighter dark">Completed Transaction</h1>

<%--<div class="space-4"></div>--%>

<div id="action-form" class="form-horizontal" role="form">
    
         <div class="space-4"></div>
        <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right" for="product">Mode</label>
        <div class="col-sm-5 center">
           <div class="clearfix">
                 <select id="ddlMode" name="ddlMode" data-rule-required="true" data-rule-value="true" 
                     data-bind="options: SelectedMode,
                                optionsValue: 'ID',
                                optionsText: 'Name',
                                value: ModeValue,
                                event: { change: $root.OnChangeMode }"></select>
                    <%-- <div style="display:none">
										Selection Option Object : <span data-bind="text: selectedOptionMode"></span><br/>
										Selection Option name : <span data-bind="text: selectedOptionMode().name"></span><br/>
										Selection Option id : <span data-bind="text: selectedOptionMode().id"></span><br/>
					 </div>--%>

                 <!--dropdown for mode Finacle-->
               <select data-rule-required="true" data-rule-value="true"
                        data-bind="options: $root.Products,
                                   optionsText: function (item) { if (item.Code != null) return item.Code + ' (' + item.Name + ')' },
                                   optionsValue: 'ID',
                                   optionsCaption: 'Please Select...',
                                   value: $root.Selected().Product,
                                   event: { change: $root.OnProductChange }">
                </select>               
                 <button class="btn btn-sm btn-primary" data-bind="click: $root.GenerateFileXML, visible: !$root.ButtonGenerateFile()"><%--disabled: $root.IsEnable()(GenerateFile)(GenerateFileXML)(GenerateFileIPE)--%>
                    <i class="icon-save"></i>
                    Generate File XML
                </button>
                <button class="btn btn-sm btn-primary" data-bind="click: $root.GenerateFile"><%--visible: $root.ButtonGenerateFile()disabled:$root.ModeValue() == 0(GenerateFile)(GenerateFileXML)(GenerateFileIPE)--%>
                    <i class="icon-save"></i>
                    Generate File TXT
                </button>
            </div>
        </div>
    </div>

 

   <%-- <div class="form-group" data-bind="visible:PaymentMode='Finacle'"> 
         <label class="col-sm-3 control-label no-padding-right" for="product"></label>
        <div class="col-sm-5 center">
            <div class="clearfix">

            </div>
        </div>
    </div>--%>




</div> 

<!-- widget box start -->
<div id="widget-box" class="widget-box">
    <!-- widget header start -->
    <div class="widget-header widget-hea1der-small header-color-dark">
        <h6>Completed Transaction Table</h6>

        <div class="widget-toolbar">
            <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                <i class="blue icon-filter"></i>
                <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: GridProperties().AllowFilter" />
                <span class="lbl"></span>
            </label>
            <a href="#" data-bind="click: GetData" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                <i class="blue icon-refresh"></i>
            </a>
        </div>
    </div>
    <!-- widget header end -->

    <!-- widget body start -->
    <div class="widget-body">
        <!-- widget main start -->
        <div class="widget-main padding-0">
            <!-- widget slim control start -->
            <div class="slim-scroll" data-height="400">
                <!-- widget content start -->
                <div class="content">

                    <!-- table responsive start -->
                    <div class="table-responsive">
                        <div class="dataTables_wrapper" role="grid">
                            <table id="CompletedTransaction-table" class="table table-striped table-bordered table-hover dataTable">
                                <thead>
                                    <tr data-bind="with: GridProperties">
                                        <th style="width: 50px">No.</th>
                                        <th style="width: 50px">Select</th>
                                        <th data-bind="click: function () { Sorting('applicationID'); }, css: GetSortedColumn('applicationID')">Application ID</th>
                                        <th data-bind="click: function () { Sorting('customerName'); }, css: GetSortedColumn('customerName')">Customer Name</th>
                                        <th data-bind="click: function () { Sorting('productName'); }, css: GetSortedColumn('productName')">Product</th>
                                        <th data-bind="click: function () { Sorting('currencyDesc'); }, css: GetSortedColumn('currencyDesc')">Currency</th>
                                        <th data-bind="click: function () { Sorting('transactionAmount'); }, css: GetSortedColumn('transactionAmount')">Trx Amount</th>
                                        <th data-bind="click: function () { Sorting('eqvUsd'); }, css: GetSortedColumn('eqvUsd')">Eqv USD</th>
                                        <th data-bind="click: function () { Sorting('fxTransaction'); }, css: GetSortedColumn('fxTransaction')">FX Transaction</th>
                                        <th data-bind="click: function () { Sorting('topUrgentDesc'); }, css: GetSortedColumn('topUrgentDesc')">Urgency</th>
                                        <th data-bind="click: function () { Sorting('stateID'); }, css: GetSortedColumn('stateID')">Transaction Status</th>
                                        <th data-bind="click: function () { Sorting('LastModifiedBy'); }, css: GetSortedColumn('LastModifiedBy')">Modified By</th>
                                        <th data-bind="click: function () { Sorting('LastModifiedDate'); }, css: GetSortedColumn('LastModifiedDate')">Modified Date</th>
                                    </tr>
                                </thead>
                                <thead data-bind="visible: GridProperties().AllowFilter" class="table-filter">
                                    <tr>
                                        <th class="clear-filter">
                                            <a href="#" data-bind="click: ClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                <i class="green icon-trash"></i>
                                            </a>
                                        </th>
                                        <th></th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterapplicationID, event: { change: GridProperties().Filter }" /></th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FiltercustomerName, event: { change: GridProperties().Filter }" /></th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterproductName, event: { change: GridProperties().Filter }" /></th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FiltercurrencyDesc, event: { change: GridProperties().Filter }" /></th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FiltertransactionAmount, event: { change: GridProperties().Filter }" /></th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FiltereqvUsd, event: { change: GridProperties().Filter }" /></th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterfxTransaction, event: { change: GridProperties().Filter }" /></th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FiltertopUrgentDesc, event: { change: GridProperties().Filter }" /></th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterstateID, event: { change: GridProperties().Filter }" /></th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterModifiedBy, event: { change: GridProperties().Filter }" /></th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.FilterModifiedDate, event: { change: GridProperties().Filter }" /></th>
                                    </tr>
                                </thead>
                                <tbody data-bind="foreach: Transactions, visible: Transactions() != null && Transactions().length > 0" id="gridCompleted">
                                    <tr>
                                        <td><span data-bind="text: RowID"></span></td>
                                        <td class="center">
                                            <label>
                                                <input type="checkbox" data-bind="event: { change: $root.SelectedTransaction }" />
                                                <span class="lbl"></span>
                                            </label>
                                        </td>
                                        <td><span data-bind="text: applicationID"></span></td>
                                        <td><span data-bind="text: customerName"></span></td>
                                        <td><span data-bind="text: productName"></span></td>
                                        <td><span data-bind="text: currencyDesc"></span></td>
                                        <td align="right"><span data-bind="text: formatNumber(transactionAmount)"></span></td>
                                        <td align="right"><span data-bind="text: formatNumber_r(eqvUsd)"></span></td>
                                        <td><span data-bind="text: fxTransaction"></span></td>
                                        <td><span data-bind="text: topUrgentDesc"></span></td>
                                        <td><span data-bind="text: stateID == 1 ? 'Pending' : 'Completed'"></span></td>
                                        <td><span data-bind="text: LastModifiedBy"></span></td>
                                        <td><span data-bind="text: $root.LocalDate(LastModifiedDate)"></span></td>
                                    </tr>
                                </tbody>
                                <tbody data-bind="visible: Transactions() == null || (Transactions() != null && Transactions().length) == 0">
                                    <tr>
                                        <td colspan="13" class="text-center">No entries available.
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- table responsive end -->
                </div>
                <!-- widget content end -->
            </div>
            <!-- widget slim control end -->

            <!-- widget footer start -->
            <div class="widget-toolbox padding-8 clearfix">
                <div class="row" data-bind="with: GridProperties">
                    <!-- pagination size start -->
                    <div class="col-sm-6">
                        <div class="dataTables_paginate paging_bootstrap pull-left">
                            Showing
                            <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                            rows of <span data-bind="text: Total"></span>
                            entries
                        </div>
                    </div>
                    <!-- pagination size end -->

                    <!-- pagination page jump start -->
                    <div class="col-sm-3">
                        <div class="dataTables_paginate paging_bootstrap">
                            Page
                            <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                            of <span data-bind="text: TotalPages"></span>
                        </div>
                    </div>
                    <!-- pagination page jump end -->

                    <!-- pagination navigation start -->
                    <div class="col-sm-3">
                        <div class="dataTables_paginate paging_bootstrap">
                            <ul class="pagination">
                                <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                        <i class="icon-double-angle-left"></i>
                                    </a>
                                </li>
                                <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                        <i class="icon-angle-left"></i>
                                    </a>
                                </li>
                                <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                        <i class="icon-angle-right"></i>
                                    </a>
                                </li>
                                <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                        <i class="icon-double-angle-right"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- pagination navigation end -->

                </div>
            </div>
            <!-- widget footer end -->

        </div>
        <!-- widget main end -->
    </div>
    <!-- widget body end -->
</div>
<!-- widget box end -->



<h1 class="header smaller lighter dark"> Generated Transaction For Disconnection IPE</h1>

<!-- widget box generated transaction start -->
<div id="widget-box" class="widget-box">
    <!-- widget header start -->
    <div class="widget-header widget-hea1der-small header-color-dark">
        <h6>Generated Transaction Table</h6>

        <div class="widget-toolbar">
            <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                <i class="blue icon-filter"></i>
                <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: GridGeneratedProperties().AllowFilter" />
                <span class="lbl"></span>
            </label>
            <a href="#" data-bind="click: GetGeneratedFileData" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                <i class="blue icon-refresh"></i>
            </a>
        </div>
    </div>
    <!-- widget header end -->

    <!-- widget body start -->
    <div class="widget-body">
        <!-- widget main start -->
        <div class="widget-main padding-0">
            <!-- widget slim control start -->
            <div class="slim-scroll" data-height="200">
                <!-- widget content start -->
                <div class="content">

                    <!-- table responsive start -->
                    <div class="table-responsive">
                        <div class="dataTables_wrapper" role="grid">
                            <table id="GeneratedTransaction-table" class="table table-striped table-bordered table-hover dataTable">
                                <thead>
                                    <tr data-bind="with: GridGeneratedProperties">
                                        <th style="width: 50px">No.</th>
                                        <th data-bind="click: function () { Sorting('FileName'); }, css: GetSortedColumn('FileName')">File Name</th>
                                        <th data-bind="click: function () { Sorting('ApplicationID'); }, css: GetSortedColumn('ApplicationID')">Application ID</th>
                                    </tr>
                                </thead>
                                <thead data-bind="visible: GridGeneratedProperties().AllowFilter" class="table-filter">
                                    <tr>
                                        <th class="clear-filter">
                                            <a href="#" data-bind="click: ClearGeneratedFileFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                <i class="green icon-trash"></i>
                                            </a>
                                        </th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterFileName, event: { change: GridGeneratedProperties().Filter }" /></th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterApplicationID, event: { change: GridGeneratedProperties().Filter }" /></th>
                                    </tr>
                                </thead>
                                <tbody data-bind="foreach: GeneratedTransactions">
                                    <tr>
                                        <td><span data-bind="text: RowID"></span></td>
                                        <td><a data-bind="event: { click: $root.DownloadFile }, text: FileName" target="_blank" class="btn-link"></a></td>
                                        <td><span data-bind="text: ApplicationID"></span></td>
                                    </tr>
                                </tbody>
                                <tbody data-bind="visible: GeneratedTransactions().length == 0">
                                    <tr>
                                        <td colspan="3" class="text-center">No entries available.
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- table responsive end -->
                </div>
                <!-- widget content end -->
            </div>
            <!-- widget slim control end -->

            <!-- widget footer start -->
            <div class="widget-toolbox padding-8 clearfix">
                <div class="row" data-bind="with: GridGeneratedProperties">
                    <!-- pagination size start -->
                    <div class="col-sm-6">
                        <div class="dataTables_paginate paging_bootstrap pull-left">
                            Showing
                            <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                            rows of <span data-bind="text: Total"></span>
                            entries
                        </div>
                    </div>
                    <!-- pagination size end -->

                    <!-- pagination page jump start -->
                    <div class="col-sm-3">
                        <div class="dataTables_paginate paging_bootstrap">
                            Page
                            <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                            of <span data-bind="text: TotalPages"></span>
                        </div>
                    </div>
                    <!-- pagination page jump end -->

                    <!-- pagination navigation start -->
                    <div class="col-sm-3">
                        <div class="dataTables_paginate paging_bootstrap">
                            <ul class="pagination">
                                <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                        <i class="icon-double-angle-left"></i>
                                    </a>
                                </li>
                                <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                        <i class="icon-angle-left"></i>
                                    </a>
                                </li>
                                <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                        <i class="icon-angle-right"></i>
                                    </a>
                                </li>
                                <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                        <i class="icon-double-angle-right"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- pagination navigation end -->

                </div>
            </div>
            <!-- widget footer end -->

        </div>
        <!-- widget main end -->
    </div>
    <!-- widget body end -->
</div>
<!-- widget box generated transaction end -->

<h1 class="header smaller lighter dark">Generated Transaction For Lost Connection IPE</h1>
<div id="widget-box" class="widget-box">
    <!-- widget header start -->
    <div class="widget-header widget-hea1der-small header-color-dark">
        <h6>Generated File XML Table</h6>

        <div class="widget-toolbar">
            <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                <i class="blue icon-filter"></i>
                <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: GridGeneratedXMLProperties().AllowFilter" />
                <span class="lbl"></span>
            </label>
            <a href="#" data-bind="click: GetGeneratedFileXMLData" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                <i class="blue icon-refresh"></i>
            </a>
        </div>
    </div>
    <!-- widget header end -->

    <!-- widget body start -->
    <div class="widget-body">
        <!-- widget main start -->
        <div class="widget-main padding-0">
            <!-- widget slim control start -->
            <div class="slim-scroll" data-height="200">
                <!-- widget content start -->
                <div class="content">

                    <!-- table responsive start -->
                    <div class="table-responsive">
                        <div class="dataTables_wrapper" role="grid">
                            <table id="GeneratedTransactionXML-table" class="table table-striped table-bordered table-hover dataTable">
                                <thead>
                                    <tr data-bind="with: GridGeneratedXMLProperties">
                                        <th style="width: 50px">No.</th>
                                        <th data-bind="click: function () { Sorting('FileName'); }, css: GetSortedColumn('FileName')">File Name</th>
                                        <th data-bind="click: function () { Sorting('ApplicationID'); }, css: GetSortedColumn('ApplicationID')">Application ID</th>
                                    </tr>
                                </thead>
                                <thead data-bind="visible: GridGeneratedXMLProperties().AllowFilter" class="table-filter">
                                    <tr>
                                        <th class="clear-filter">
                                            <a href="#" data-bind="click: ClearGeneratedFileXMLFilter" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                <i class="green icon-trash"></i>
                                            </a>
                                        </th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterFileName, event: { change: GridGeneratedXMLProperties().Filter }" /></th>
                                        <th>
                                            <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterApplicationID, event: { change: GridGeneratedXMLProperties().Filter }" /></th>
                                    </tr>
                                </thead>
                                <tbody data-bind="foreach: GeneratedTransactionXML">
                                    <tr>
                                        <td><span data-bind="text: RowID"></span></td>
                                        <td><a data-bind="event: { click: $root.DownloadFile }, text: FileName" target="_blank" class="btn-link"></a></td>
                                        <td><span data-bind="text: ApplicationID"></span></td>
                                    </tr>
                                </tbody>
                                <tbody data-bind="visible: GeneratedTransactionXML().length == 0">
                                   <%-- visible: (GeneratedTransactionXML() == null || GeneratedTransactionXML() == undefined)--%>
                                    <tr>
                                        <td colspan="3" class="text-center">No entries available.
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- table responsive end -->
                </div>
                <!-- widget content end -->
            </div>
            <!-- widget slim control end -->

            <!-- widget footer start -->
            <div class="widget-toolbox padding-8 clearfix">
                <div class="row" data-bind="with: GridGeneratedProperties">
                    <!-- pagination size start -->
                    <div class="col-sm-6">
                        <div class="dataTables_paginate paging_bootstrap pull-left">
                            Showing
                            <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                            rows of <span data-bind="text: Total"></span>
                            entries
                        </div>
                    </div>
                    <!-- pagination size end -->
                    
                    <!-- pagination page jump start -->
                    <div class="col-sm-3">
                        <div class="dataTables_paginate paging_bootstrap">
                            Page
                            <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                            of <span data-bind="text: TotalPages"></span>
                        </div>
                    </div>
                    <!-- pagination page jump end -->

                    <!-- pagination navigation start -->
                    <div class="col-sm-3">
                        <div class="dataTables_paginate paging_bootstrap">
                            <ul class="pagination">
                                <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                        <i class="icon-double-angle-left"></i>
                                    </a>
                                </li>
                                <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                        <i class="icon-angle-left"></i>
                                    </a>
                                </li>
                                <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                        <i class="icon-angle-right"></i>
                                    </a>
                                </li>
                                <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                    <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                        <i class="icon-double-angle-right"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- pagination navigation end -->

                </div>
            </div>
            <!-- widget footer end -->

        </div>
        <!-- widget main end -->
    </div>
    <!-- widget body end -->
</div>


<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/additional-methods.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/CompletedTransaction.js"></script>
