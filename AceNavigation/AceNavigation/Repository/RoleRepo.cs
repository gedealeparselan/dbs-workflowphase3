﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AceNavigation.Model;
using AceNavigation.Entity;

namespace AceNavigation.Repository
{
    class RoleRepo
    {
        public IList<Model.RoleModel> GetRoleByUsername(string Username)
        {
            try
            {
                using (DBSEntitiesNav context = new DBSEntitiesNav())
                {
                    var data = (from a in context.Employees
                                join b in context.EmployeeRoleMappings on a.EmployeeID equals b.EmployeeID
                                join c in context.Roles on b.RoleID equals c.RoleID
                                where a.IsDeleted.Equals(false) && c.IsDeleted.Equals(false) 
                                && a.EmployeeUsername.ToLower() == Username.Trim().ToLower()
                                select new RoleModel
                                {
                                    RoleID = c.RoleID,
                                    RoleName = c.RoleName
                                }).ToList();

                    return data;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
