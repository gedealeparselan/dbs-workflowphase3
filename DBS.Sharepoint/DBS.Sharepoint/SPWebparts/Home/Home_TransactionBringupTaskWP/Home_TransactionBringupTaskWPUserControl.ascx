﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Home_TransactionBringupTaskWPUserControl.ascx.cs" Inherits="DBS.Sharepoint.SPWebparts.Home.Home_TransactionBringupTaskWP.Home_TransactionBringupTaskWPUserControl" %>

<h1 class="header smaller no-margin-top lighter dark">Bringup Transactions</h1>

<!-- widget box start -->
<div id="widget-box" class="widget-box">
	<!-- widget header start -->
	<div class="widget-header widget-header-small header-color-dark">
		<h6>Bringup Transactions</h6>

		<div class="widget-toolbar">
			<label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
				<i class="blue icon-filter"></i>
				<input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: GridProperties().AllowFilter" />
				<span class="lbl"></span>
			</label>
			<a href="#" data-bind="click: GetData" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
				<i class="blue icon-refresh"></i>
			</a>
		</div>
	</div>
	<!-- widget header end -->
	
	<!-- widget body start -->
	<div class="widget-body">
		<!-- widget main start -->
		<div class="widget-main padding-0">
			<!-- widget slim control start -->
			<div class="slim-scroll" data-height="400">
				<!-- widget content start -->
				<div class="content">

					<!-- table responsive start -->						
					<div class="table-responsive">
						<div class="dataTables_wrapper" role="grid">
							<table id="workflow-task-table" class="table table-striped table-bordered table-hover dataTable"
							        workflow-state="running, error"
							        workflow-outcome="pending, custom"
							        workflow-custom-outcome=""
							        workflow-show-contribute="true">
								<thead>
									<tr data-bind="with: GridProperties">
										<th>No.</th>
										<th data-bind="click: function () { Sorting('customerName') }, css: GetSortedColumn('Customer')">Customer Name</th>
										<th data-bind="click: function () { Sorting('productName') }, css: GetSortedColumn('Product')">Product</th>
										<th data-bind="click: function () { Sorting('currencyCode') }, css: GetSortedColumn('Currency')">Currency</th>
										<th data-bind="click: function () { Sorting('transactionAmount') }, css: GetSortedColumn('Amount')">Trxn Amount</th>
										<th data-bind="click: function () { Sorting('DebitAccNumber') }, css: GetSortedColumn('DebitAccNumber')">Debit Acc Number</th>
										<th data-bind="click: function () { Sorting('fxTransaction') }, css: GetSortedColumn('IsFXTransaction')">FX Transaction</th>
										<th data-bind="click: function () { Sorting('topUrgent') }, css: GetSortedColumn('IsTopUrgent')">Urgency</th>
                                        <th data-bind="click: function () { Sorting('applicationDate') }, css: GetSortedColumn('ApplicationDate')">Application Date</th>
										<th data-bind="click: function () { Sorting('LastModifiedBy') }, css: GetSortedColumn('UserDisplayName')">Modified By</th>
                                        <th data-bind="click: function () { Sorting('LastModifiedDate') }, css: GetSortedColumn('LastModifiedDate')">Modified Date</th>
									</tr>
								</thead>
								<thead data-bind="visible: GridProperties().AllowFilter" class="table-filter">
									<tr>
										<th class="clear-filter">
											<a href="#" data-bind="click: ClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
												<i class="green icon-trash"></i>
											</a>
										</th>
										<th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCustomer, event: { change: GridProperties().Filter }" /></th>
										<th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterProduct, event: { change: GridProperties().Filter }" /></th>
										<th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCurrency, event: { change: GridProperties().Filter }" /></th>
										<th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterAmount, event: { change: GridProperties().Filter }" /></th>
										<th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterDebitAccNumber, event: { change: GridProperties().Filter }" /></th>
										<th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterFXTransaction, event: { change: GridProperties().Filter }" /></th>
										<th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterTopUrgent, event: { change: GridProperties().Filter }" /></th>
										<th><input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.FilterApplicationDate, event: { change: GridProperties().Filter }" /></th>
                                        <th><input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterUser, event: { change: GridProperties().Filter }" /></th>
										<th><input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.FilterEntryTime, event: { change: GridProperties().Filter }" /></th>
									</tr>
								</thead>
								<tbody data-bind="foreach: Tasks">
									<tr data-bind="click: $root.GetSelectedRow" data-toggle="modal">
										<td><span data-bind="text: RowID"></span></td>
										<td><span data-bind="text: customerName"></span></td>
										<td><span data-bind="text: productName"></span></td>
										<td><span data-bind="text: currencyCode"></span></td>
										<td align="right"><span data-bind="text: formatNumber(transactionAmount)"></span></td>
										<td><span data-bind="text: debitAccNumber"></span></td>
										<td ><span data-bind="text: fxTransaction"></span></td>
										<td><span data-bind="text: $root.RenameStatus('Urgency', topUrgent)"></span></td>
										<td><span data-bind="text: $root.LocalDate(applicationDate, true, false)"></span></td>
										<td><span data-bind="text: LastModifiedBy"></span></td>
										<td><span data-bind="text: $root.LocalDate(LastModifiedDate)"></span></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<!-- table responsive end -->
				</div>
				<!-- widget content end -->
			</div>
			<!-- widget slim control end -->
			
			<!-- widget footer start -->
			<div class="widget-toolbox padding-8 clearfix">
				<div class="row" data-bind="with: GridProperties">
					<!-- pagination size start -->
					<div class="col-sm-6">
						<div class="dataTables_paginate paging_bootstrap pull-left">
							Showing <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
							rows of <span data-bind="text: Total"></span> 
							entries
						</div>
					</div>
					<!-- pagination size end -->
					
					<!-- pagination page jump start -->
					<div class="col-sm-3">
						<div class="dataTables_paginate paging_bootstrap">
							Page <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width:50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page"/>
							of <span data-bind="text: TotalPages"></span>
						</div>
					</div>
					<!-- pagination page jump end -->
					
					<!-- pagination navigation start -->
					<div class="col-sm-3">
						<div class="dataTables_paginate paging_bootstrap">
							<ul class="pagination">
								<li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
									<a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
										<i class="icon-double-angle-left"></i>
									</a>
								</li>
								<li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
									<a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
										<i class="icon-angle-left"></i>
									</a>
								</li>				
								<li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
									<a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
										<i class="icon-angle-right"></i>
									</a>
								</li>
								<li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
									<a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
										<i class="icon-double-angle-right"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
					<!-- pagination navigation end -->
					
				</div>
			</div>
			<!-- widget footer end -->
			
		</div>
		<!-- widget main end -->
	</div>
	<!-- widget body end -->
</div>
<!-- widget box end -->

<!-- added by dani -->
<!-- modal form start -->
<div id="modal-form" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
    <div class="modal-dialog" style="width:100%;">
        <div id="backDrop" class="absCustomBackDrop topCustomBackDrop darkCustomBackDrop hideCustomBackDrop fullHeightCustomBackDrop fullWidthCustomBackDrop centerCustomBackDrop"></div>
        <div class="modal-content">
            <!-- modal header end -->
            <!-- modal body start -->
            <div class="modal-body overflow-visible">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" style="margin-top: -10px;font-size:30px;margin-right: -8px;" data-bind:"$root.OnCloseApproval">×</button>
                <div class="row">
                    <div class="col-xs-12">
                        <!-- modal body form start -->
                        <div class="tabbable">
                            <ul class="nav nav-tabs" id="myTab">
                                <li class="active">
                                    <a data-toggle="tab" href="#transaction">
                                        <i class="blue icon-credit-card bigger-110"></i>
                                        <span data-bind="text: ActivityTitle"></span>
                                    </a>
                                </li>
                                
                                <li>
                                    <a data-toggle="tab" href="#history">
                                        <i class="blue icon-comments bigger-110"></i>
                                        WorkFlow History
                                    </a>
                                </li>
                                <!--
                                <li class="TransHistory">
                                    <a data-toggle="tab" href="#Transactionhistory">
                                        <i class="blue icon-list bigger-110"></i>
                                        Transaction History
                                    </a>
                                </li>
                                -->
                            </ul>

                            <div class="tab-content">
                                <div id="transaction" class="tab-pane in active">
                                    <div id="transaction-progress" class="col-sm-4 col-sm-offset-4" style="display: none">
                                        <h4 class="smaller blue">Please wait...</h4>
                                        <span class="space-4"></span>
                                        <div class="progress progress-small progress-striped active">
                                            <div class="progress-bar progress-bar-info" style="width:100%"></div>
                                        </div>
                                    </div>

                                    <div id="transaction-data" data-bind="template: { name: ApprovalTemplate(), data: ApprovalData() }"></div>
                                </div>

                                <div id="history" class="tab-pane">
                                    <div id="timeline-2" data-bind="with: ApprovalData()">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                                                <div class="timeline-container timeline-style2">
                                                    <div data-bind="if: $root.IsTimelines">

                                                        <div class="timeline-items" data-bind="foreach: Timelines">
                                                            <div class="timeline-item clearfix">
                                                                <div class="timeline-info">
                                                                    <span class="timeline-date" data-bind="text: $root.LocalDate(Time)"></span>

                                                                    <i class="timeline-indicator btn btn-info no-hover"></i>
                                                                </div>

                                                                <div class="widget-box transparent">
                                                                    <div class="widget-body">
                                                                        <div class="widget-main no-padding">
                                                                            <span class="bigger-110">
                                                                                <span class="blue bolder" data-bind="text: DisplayName"></span>
                                                                                <!-- ko if: IsGroup -->
                                                                                (<span data-bind="text: UserOrGroup"></span>)
                                                                                <!-- /ko -->
                                                                            </span>

                                                                            <span class="bolder" data-bind="text: Outcome"></span>
                                                                            <span data-bind="text: Activity"></span>

                                                                            <!-- ko ifnot: ApproverID == 0 -->
                                                                            <br />
                                                                            <i class="icon-comments-alt bigger-110 blue"></i>
                                                                            <span data-bind="text: Comment"></span>
                                                                            <!-- /ko -->
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                

                                <!-- <div id="context" class="tab-pane">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Parameter</th>
                                                <th>Value</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Web ID</td>
                                                <td><span data-bind="text: SPWebID"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Site ID</td>
                                                <td><span data-bind="text: SPSiteID"></span></td>
                                            </tr>
                                            <tr>
                                                <td>List ID</td>
                                                <td><span data-bind="text: SPListID"></span></td>
                                            </tr>
                                            <tr>
                                                <td>List Item ID</td>
                                                <td><span data-bind="text: SPListItemID"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Task List ID</td>
                                                <td><span data-bind="text: SPTaskListID"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Task List Item ID</td>
                                                <td><span data-bind="text: SPTaskListItemID"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Initiator</td>
                                                <td><span data-bind="text: Initiator"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Workflow Instance ID</td>
                                                <td><span data-bind="text: WorkflowInstanceID"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Workflow ID</td>
                                                <td><span data-bind="text: WorkflowID"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Workflow Name</td>
                                                <td><span data-bind="text: WorkflowName"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Start Time</td>
                                                <td><span data-bind="text: LocalDate(StartTime())"></span></td>
                                            </tr>
                                            <tr>
                                                <td>State ID </td>
                                                <td><span data-bind="text: StateID"></span></td>
                                            </tr>
                                            <tr>
                                                <td>State Description</td>
                                                <td><span data-bind="text: StateDescription"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Is Authorized</td>
                                                <td><span data-bind="text: IsAuthorized"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Task Type</td>
                                                <td><span data-bind="text: TaskTypeDescription"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Title</td>
                                                <td><span data-bind="text: Title"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Activity Title</td>
                                                <td><span data-bind="text: ActivityTitle"></span></td>
                                            </tr>
                                            <tr>
                                                <td>User</td>
                                                <td><span data-bind="text: User"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Is Sharepoint Group</td>
                                                <td><span data-bind="text: IsSPGroup"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Entry Time</td>
                                                <td><span data-bind="text: LocalDate(EntryTime())"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Exit Time</td>
                                                <td><span data-bind="text: LocalDate(ExitTime())"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Outcome ID</td>
                                                <td><span data-bind="text: OutcomeID"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Outcome Description</td>
                                                <td><span data-bind="text: OutcomeDescription"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Custom Outcome ID</td>
                                                <td><span data-bind="text: CustomOutcomeID"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Custom Outcom Description</td>
                                                <td><span data-bind="text: CustomOutcomeDescription"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Comments</td>
                                                <td><span data-bind="text: Comments"></span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                    -->
                                <div id="ko-model" class="tab-pane">
                                    <pre data-bind="text: ko.toJSON($root.ApprovalData, null, 2)"></pre>
                                    <!--<pre data-bind="text: JSON.stringify(ko.toJS(ApprovalData), null, 2)"></pre>-->
                                </div>
                            </div>
                        </div>
                        <!-- modal body form end -->
                    </div>
                </div>
            </div>
            <!-- modal body end -->
            <!-- modal footer start -->
            <div class="modal-footer">
                <!--
                <div class="row" data-bind="visible: $root.IsAuthorizedNintex()">
                    <div class="form-group">
                        <label class="col-sm-3 control-label bolder no-padding-right" for="workflow-comments">Comments</label>

                        <div class="col-xs-9 align-left">
                            <div class="clearfix">
                                <textarea class="col-lg-12" id="workflow-comments" name="workflow-comments" rows="4" data-bind="value: $root.Comments, enable: $root.IsPendingNintex()" placeholder="Type any comment here..."></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                -->

                <div class="space-8"></div>

                <!-- dynamic outcomes button start -->
                <!--<span data-bind="foreach: Outcomes, visible: $root.IsAuthorizedNintex()">
                    <button data-bind="click: $root.ApprovalProcess, enable: $root.IsPendingNintex()" class="btn btn-sm btn-primary">
                        <i class="icon-approve"></i>
                        <span data-bind="text: $root.ApprovalButton(Name)"></span>
                    </button>
                </span>-->
                <!-- dynamic outcomes button end -->

                <button class="btn btn-sm btn-success" data-dismiss="modal" data-bind="click: $root.OnCloseApproval">
                    <i class="icon-remove"></i>
                    Close
                </button>
            </div>
            <!-- modal footer end -->

        </div>
    </div>
</div>
<!-- modal form end -->

<!-- template mapping start -->
<script type="text/html" id="TransactionLoanBringup" src="/Pages/Approval/LoanComplete.html"></script>
<!-- template mapping end -->
<!-- added by dani end -->

<!-- additional page scripts start -->
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/additional-methods.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/Knockout/knockout.mapping-latest.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/Helper.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/TransactionBringupTask.js"></script>
<!-- additional page scripts end -->