﻿using System;
using DBS.WebAPI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Description;
using System.Web;
using System.Web.Http;

namespace DBS.WebAPI.Controllers
{
    //[Authorize]
    public class UserCategoryController : ApiController
    {
        private string message = string.Empty;

        private readonly IUserCategoryRepository repository = null;

        public UserCategoryController()
        {
            this.repository = new UserCategoryRepository();
        }

        public UserCategoryController(IUserCategoryRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all bene segments.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<UserCategoryModel>))]
        public HttpResponseMessage Get()
        {
            IList<UserCategoryModel> output = new List<UserCategoryModel>();

            if (!repository.GetUserCategory(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<UserCategoryModel>>(HttpStatusCode.OK, output);
            }
        }
        //int id, string name
        [ResponseType(typeof(IList<UserCategoryModel>))]
        [HttpGet]
        [Route("api/UserCategory/IPE")]
        public HttpResponseMessage GetIPE(int id, string name, string text) 
        {
            IList<UserCategoryModel> output = new List<UserCategoryModel>();

            if (!repository.GetUserCategoryIPE(ref output, id, name ,text, ref message))
            { 
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<UserCategoryModel>>(HttpStatusCode.OK, output);
            }
        }


        /// <summary>
        /// Get bene segments with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<UserCategoryModel>))]
        public HttpResponseMessage Get(int? limit, int? index)
        {
            IList<UserCategoryModel> output = new List<UserCategoryModel>();

            if (!repository.GetUserCategory(ref output, limit.HasValue ? limit.Value : 10, index.HasValue ? index.Value : 0, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<UserCategoryModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get bene segments for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(UserCategoryGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<UserCategoryFilter> filters)
        {
            UserCategoryGrid output = new UserCategoryGrid();

            if (!repository.GetUserCategory(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "ModeName" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<UserCategoryGrid>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Find bene segments using criterias.
        /// </summary>
        /// <param name="query">Code or Description.</param>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<UserCategoryModel>))]
        [HttpGet]
        [Route("api/UserCategory/Search")]
        public HttpResponseMessage Search(string query, int? limit)
        {
            IList<UserCategoryModel> output = new List<UserCategoryModel>();

            if (!repository.GetUserCategory(query, limit.HasValue ? limit.Value : 10, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<UserCategoryModel>>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Get bene segment by ID.
        /// </summary>
        /// <param name="id">Bene Segment ID.</param>
        /// <returns></returns>
        [ResponseType(typeof(UserCategoryModel))]
        [Route("api/UserCategory/{id}")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            UserCategoryModel output = new UserCategoryModel();

            if (!repository.GetUserCategoryByID(id, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                if (output == null)
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                else
                    return Request.CreateResponse<UserCategoryModel>(HttpStatusCode.OK, output);
            }
        }

        /// <summary>
        /// Create a new bene segment.
        /// </summary>
        /// <param name="beneSegment">Bene Segment data.</param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(string))]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(UserCategoryModel UserCategory)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddUserCategory(UserCategory, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New User Category data has been created." });
                }
            }
        }

        /// <summary>
        /// Modify existing bene segment.
        /// </summary>
        /// <param name="id">ID of BeneSegment to be modify.</param>
        /// <param name="beneSegment">Bene segment data to be updated.</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseType(typeof(string))]
        [Route("api/UserCategory/{id}")]
        //[Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Put(int id, UserCategoryModel beneSegment)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.UpdateUserCategory(id, beneSegment, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "User Category data has been updated." });
                }
            }
        }

        /// <summary>
        /// Remove exisiting bene segment.
        /// </summary>
        /// <param name="id">ID of bene segment to be deleted.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/UserCategory/{id}")]
        //[Authorize(Roles = "DBS Admin")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.DeleteUserCategory(id, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Message = "User Category data has been deleted." });
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }

}