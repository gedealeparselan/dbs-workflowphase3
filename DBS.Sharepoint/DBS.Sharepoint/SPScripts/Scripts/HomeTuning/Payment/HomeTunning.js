var accessToken;
var $box;
var $remove = false;
var $container;
var cifData = '0';
var CurrentAmount = 0;
var FXModel = { cif: cifData, token: accessToken, currentAmount: CurrentAmount }
//Maping Instruction
var instructionRow = 0;
//End
var amountPPUmaker;
var IsTaskHubConnected = false;
var taskHub = $.hubConnection(config.signal.server + "hub");
var taskProxy = taskHub.createHubProxy("TaskService");
var viewModel = null;

taskProxy.on("Message", function (data) {
    ShowNotification(data.From, data.Message, "gritter-info", false);
});

taskProxy.on("NotifyInfo", function (data) {
    //alert(JSON.stringify(data))
    ShowNotification(data.From, data.Message, "gritter-info", false);
});

taskProxy.on("NotifyWarning", function (data) {
    //alert(JSON.stringify(data))
    ShowNotification(data.From, data.Message, "gritter-warning", false);
});

taskProxy.on("NotifyError", function (data) {
    //alert(JSON.stringify(data))
    ShowNotification(data.From, data.Message, "gritter-error", true);
});

function OnKeyUp(form, element) {
    viewModel.OnKeyUp(form, element);
}

function SetTopUrgentNormal() {
    viewModel.SetTopUrgentNormal();
}

function SetTopUrgent() {
    viewModel.SetTopUrgent();
}

function SetTopUrgentChain() {
    viewModel.SetTopUrgentChain();
}

function checkAllPPUCheckerIPE(param) {
    viewModel.CheckAllData('TransactionChecker');
}

function checkAllLoan(param) {
    viewModel.CheckAllData('TransactionLoanCheckerUnsettlement');
}

function checkAllPaymentChecker(param) {
    viewModel.checkAllPaymentChecker(param);
}

function LockAndUnlockTaskHub(lockType, instanceId, approverID, taskId, activityTitle) {
    if (IsTaskHubConnected) {
        var data = {
            WorkflowInstanceID: instanceId,
            ApproverID: approverID,
            TaskID: taskId,
            ActivityTitle: activityTitle,
            LoginName: spUser.LoginName,
            DisplayName: spUser.DisplayName
        };

        var LockType = lockType.toLowerCase() == "lock" ? "LockTask" : "UnlockTask";

        // try lock current task
        taskProxy.invoke(LockType, data)
            .fail(function (err) {
                ShowNotification("LockType Task Failed", err, "gritter-error", true);
            }
        );
    } else {
        ShowNotification("Task Manager", "There is no active connection to task server manager.", "gritter-error", true);
    }
}

function StartTaskHub() {
    taskHub.start()
        .done(function () {
            IsTaskHubConnected = true;
        })
        .fail(function () {
            ShowNotification("Hub Connection Failed", "Fail while try connecting to hub server", "gritter-error", true);
        }
    );
}

var spUser;
function GetCurrentUser() {
    //if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
    if ($.cookie(api.cookie.spUser) != undefined) {
        spUser = $.cookie(api.cookie.spUser);

        viewModel.SPUser = spUser;

        //alert(JSON.stringify(vm.SPUser));

        //Helper.Signal.Connect(OnSuccessSignal, OnErrorSignal, OnReceivedSignal);

        // load hub script
        //$.getScript(config.signal.server +"Scripts/hubs.js");

    }
}

function TokenOnSuccess(data, textStatus, jqXHR) {
    // store token on browser cookies
    $.cookie(api.cookie.name, data.AccessToken);
    accessToken = $.cookie(api.cookie.name);
    // call spuser function
    GetCurrentUser(viewModel);
    // call get data inside view model
    FXModel.token = accessToken;
    //GetParameterData(FXModel, OnSuccessGetTotal, OnError);
    GetThresholdParameter(FXModel, OnSuccessThresholdPrm, OnError);
    // Performance
    viewModel.GetParameters();
    viewModel.GetData();
    viewModel.GetRateIDR();
    viewModel.GetFCYIDRTreshold();
    viewModel.GetEmployeeLocation();
}

function OnSuccessThresholdPrm() {
    vSystem.idrrate = DataModel.RateIDR;
}

function TokenOnError(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
}

function enforceMaxlength(data, event) {
    switch (viewModel.ActivityTitle()) {
        case "PPU Maker Task":
        case "PPU Maker After Checker and Caller Task":
        case "PPU Maker After Payment Task":
            if (viewModel.TransactionMaker().Transaction.Product.Name == 'SKN') {
                if (event.target.value.length >= 70) {
                    return false;
                }
            }
            else {
                if (viewModel.TransactionMaker().Transaction.Product.Name != 'OTT') {
                    if (event.target.value.length >= 35) {
                        return false;
                    }
                }
                else {
                    if (event.target.value.length >= 255) {
                        return false;
                    }
                }
            }
            return true;
            break;
        case "PPU Caller Task":
            if (viewModel.TransactionCallback().Transaction.Product.Name == 'SKN') {
                if (event.target.value.length >= 70) {
                    return false;
                }
            }
            else {
                if (viewModel.TransactionCallback().Transaction.Product.Name != 'OTT') {
                    if (event.target.value.length >= 35) {
                        return false;
                    }
                }
                else {
                    if (event.target.value.length >= 255) {
                        return false;
                    }
                }
            }
            return true;
            break;
        case "Payment Maker Task":
        case "Payment Maker Revise Task":
            if (viewModel.PaymentMaker().Payment.Product.Name == 'SKN') {
                if (event.target.value.length >= 70) {
                    return false;
                }
            }
            else {
                if (viewModel.PaymentMaker().Payment.Product.Name != 'OTT') {
                    if (event.target.value.length >= 35) {
                        return false;
                    }
                }
                else {
                    if (event.target.value.length >= 255) {
                        return false;
                    }
                }
            }
            return true;
            break;
        default:
            return true;
            break;

    }
}

function IsDocumentCompleteCheck(data) {
    //IsDocumentCompleteCheck

    if (viewModel.TransactionChecker().Verify != undefined) {

        var verify = ko.utils.arrayFilter(viewModel.TransactionChecker().Verify, function (item) {
            return item.Name == "Document Completeness";
        });

        if (verify != null) {
            var index = viewModel.TransactionChecker().Verify.indexOf(verify[0]);

            if (verify[0].IsVerified) {
                viewModel.TransactionChecker().Transaction.IsDocumentComplete = true;
            } else {
                viewModel.TransactionChecker().Transaction.IsDocumentComplete = false;
            }
        }
    }

    //Dani
    //~dani~ start 26 10 2015
    if (viewModel.TransactionCheckerFD().Verify != undefined) {
        var verifyFD = ko.utils.arrayFilter(viewModel.TransactionCheckerFD().Verify, function (item) {
            return item.Name == "Document Completeness";
        });
        if (verifyFD != null) {
            var index = viewModel.TransactionCheckerFD().Verify.indexOf(verifyFD[0]);

            if (verifyFD[0].IsVerified) {
                viewModel.TransactionCheckerFD().Transaction.IsDocumentComplete = true;
            } else {
                viewModel.TransactionCheckerFD().Transaction.IsDocumentComplete = false;
            }
        }
    }
    //~dani~ end
    if (viewModel.TransactionCheckerTMO().Verify != undefined) {
        var verifyTMO = ko.utils.arrayFilter(viewModel.TransactionCheckerTMO().Verify, function (item) {
            return item.Name == "Document Completeness";
        });
        if (verifyTMO != null) {
            var index = viewModel.TransactionCheckerTMO().Verify.indexOf(verifyTMO[0]);

            if (verifyTMO[0].IsVerified) {
                viewModel.TransactionCheckerTMO().Transaction.IsDocumentComplete = true;
            } else {
                viewModel.TransactionCheckerTMO().Transaction.IsDocumentComplete = false;
            }
        }
    }
    //End Dani
    //Agung Start Document Completness
    if (viewModel.TransactionCheckerLoan().Verify != undefined) {
        var verifyLoan = ko.utils.arrayFilter(viewModel.TransactionCheckerLoan().Verify, function (item) {
            return item.Name == "Document Completeness";
        });
        if (verifyLoan != null) {
            var index = viewModel.TransactionCheckerLoan().Verify.indexOf(verifyLoan[0]);

            if (verifyLoan[0].IsVerified) {
                viewModel.TransactionCheckerLoan().Transaction.IsDocumentComplete = true;
            } else {
                viewModel.TransactionCheckerLoan().Transaction.IsDocumentComplete = false;
            }
        }
    }
    //End
    //udin
    if (viewModel.ActivityTitle() == "Pending Documents Checker Task") {
        if (viewModel.TransactionChecker().Transaction != undefined) {
            if (viewModel.TransactionChecker().Transaction.IsDocumentComplete) {
                viewModel.TransactionChecker().Transaction.IsDocumentComplete = true;
            }
            else {
                viewModel.TransactionChecker().Transaction.IsDocumentComplete = false;
            }
        }
    }
    //end
}

// Document Ready
$(document).ready(function () {
    Helper.GetToken(function (token) {
        console.log("Token : " + token);
        accessToken = token;
        $.getScript("/SiteAssets/Scripts/HomeTuning/Payment/Models.js").done(function () {
            console.log("Models loaded!");
            $.getScript("/SiteAssets/Scripts/HomeTuning/Payment/ViewModel.js").done(function () {
                console.log("ViewModel loaded!");
                viewModel = new ViewModel();
                viewModel.GetParameters();
                viewModel.GetData();
                viewModel.GetRateIDR();
                viewModel.GetEmployeeLocation();
                GetCurrentUser(viewModel);
                //Maping Instruction
                $(document).on('click', '.chooseInstruction', function () {
                    instructionRow = $(this).attr('id').replace('instruction_', '');
                    var handsontable = $container.data('handsontable');
                    var maintenanceType = handsontable.getDataAtRowProp(instructionRow, "MaintenanceTypeName");
                    var cif = handsontable.getDataAtRowProp(instructionRow, "CIF");
                    if (maintenanceType == "Rollover") {
                        GetCSOTaskTransaction(cif, "Loan Rollover");
                        $("#modal-form-instruction").modal('show');
                        $('.remove').click();
                    }
                    else if (maintenanceType == "Payoff") {
                        GetCSOTaskTransaction(cif, "Loan Settlement");
                        $("#modal-form-instruction").modal('show');
                        $('.remove').click();
                    } else {
                        ShowNotification("Task Validation Warning", "Please Select Maintenance Type", 'gritter-warning', true);
                        return;
                    }
                });

                function GetCSOTaskTransaction(cif, productName) {
                    var options = {
                        url: api.server + api.url.csoTaskTransaction,
                        params: {
                            cif: cif,
                            productname: productName
                        },
                        token: accessToken
                    };
                    Helper.Ajax.Get(options, OnSuccessGetCSOTask, OnError, OnAlways);
                };

                function OnSuccessGetCSOTask(data, textStatus, jqXHR) {
                    if (jqXHR.status = 200) {
                        viewModel.ChooseInstruction(data);
                    } else {
                        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseJSON.Message, 'gritter-error', true);
                    }
                }

                $(document).on('click', '.calculateDOA', function () {
                    instructionRow = $(this).attr('id').replace('doa_', '');
                    calculateDOA(instructionRow);
                });

                $(document).on('click', '.calculateRate', function () {
                    instructionRow = $(this).attr('id').replace('rate_', '');
                    calculateRate(instructionRow);
                });

                $(document).on('click', '.calculateAllInRate', function () {
                    instructionRow = $(this).attr('id').replace('allInRate_', '');
                    calculateAllInRate(instructionRow);
                });
                //End

                $box = $('#widget-box');
                var event;
                $box.trigger(event = $.Event('reload.ace.widget'))
                if (event.isDefaultPrevented()) return
                $box.blur();

                // block enter key from user to prevent submitted data.
                $(this).keypress(function (event) {
                    var code = event.charCode || event.keyCode;
                    if (code == 13) {
                        ShowNotification("Page Information", "Enter key is disabled for this form.", "gritter-warning", false);

                        return false;
                    }
                });

                // Load all templates
                $('script[src][type="text/html"]').each(function () {
                    //alert($(this).attr("src"))
                    var src = $(this).attr("src");
                    if (src != undefined) {
                        $(this).load(src);
                    }
                });

                // scrollables
                $('.slim-scroll').each(function () {
                    var $this = $(this);
                    $this.slimscroll({
                        height: $this.data('height') || 100,
                        railVisible: true,
                        alwaysVisible: true,
                        color: '#D15B47'
                    });
                });

                // tooltip
                $('[data-rel=tooltip]').tooltip();

                // datepicker
                $('.date-picker').datepicker({ autoclose: true }).next().on(ace.click_event, function () {
                    $(this).prev().focus();
                });


                $('.time-picker').timepicker({
                    defaultTime: "",
                    minuteStep: 1,
                    showSeconds: false,
                    showMeridian: false
                }).next().on(ace.click_event, function () {
                    $(this).prev().focus();
                });
                // ace file upload
                $('#document-path-upload').ace_file_input({
                    no_file: 'No File ...',
                    btn_choose: 'Choose',
                    btn_change: 'Change',
                    droppable: false,
                    //onchange:null,
                    thumbnail: false, //| true | large
                    //whitelist:'gif|png|jpg|jpeg'
                    blacklist: 'exe|dll'
                    //onchange:''
                    //
                });
                // ace file upload
                $('#document-path-upload1').ace_file_input({
                    no_file: 'No File ...',
                    btn_choose: 'Choose',
                    btn_change: 'Change',
                    droppable: false,
                    //onchange:null,
                    thumbnail: false, //| true | large
                    //whitelist:'gif|png|jpg|jpeg'
                    blacklist: 'exe|dll'
                    //onchange:''
                    //
                });
                // ace file upload
                $('#document-path').ace_file_input({
                    no_file: 'No File ...',
                    btn_choose: 'Choose',
                    btn_change: 'Change',
                    droppable: false,
                    //onchange:null,
                    thumbnail: false, //| true | large
                    //whitelist:'gif|png|jpg|jpeg'
                    blacklist: 'exe|dll'
                    //onchange:''
                    //
                });
                // backspace bank name
                $('#bank-bank').keyup(function (event) {
                    var code = event.charCode || event.keyCode;
                    if (code == 46 || code == 8) {
                        if (event.target.value == '') {
                            $('#bank-code').val('');
                        }
                    }
                });
                // Knockout custom file handler
                ko.bindingHandlers.file = {
                    init: function (element, valueAccessor) {
                        $(element).change(function () {
                            var file = this.files[0];

                            if (ko.isObservable(valueAccessor()))
                                valueAccessor()(file);
                        });
                    }
                };

                //add aridya 20161017 for editable table ~OFFLINE~
                ko.bindingHandlers.editableText = {
                    init: function (element, valueAccessor) {
                        var $element = $(element);
                        var initialValue = ko.utils.unwrapObservable(valueAccessor());
                        $element.html(initialValue);
                        $element.on('keydown', function (e) {
                            if (e.which != 13) { //disable enter key
                                $element.on('keyup', function () {
                                    observable = valueAccessor();
                                    observable($element.html());
                                });
                            } else {
                                return false;
                            }
                        });
                    }
                };
                //end add aridya

                // validation
                $('#aspnetForm').validate({
                    errorElement: 'div',
                    errorClass: 'help-block',
                    focusInvalid: true,

                    highlight: function (e) {
                        $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
                    },

                    success: function (e) {
                        $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
                        $(e).remove();
                    },

                    errorPlacement: function (error, element) {
                        if (element.is(':checkbox') || element.is(':radio')) {
                            var controls = element.closest('div[class*="col-"]');
                            if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                            else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                        }
                        else if (element.is('.select2')) {
                            error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                        }
                        else if (element.is('.chosen-select')) {
                            error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                        }
                        else error.insertAfter(element.parent());
                    }
                });
                $('#aspnetForm').after('<form id="frmUnderlying"></form>');
                $("#modal-form-Underlying").appendTo("#frmUnderlying");

                $('#frmUnderlying').validate({
                    errorElement: 'div',
                    errorClass: 'help-block',
                    focusInvalid: true,
                    focusCleanup: true,
                    highlight: function (e) {
                        $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
                    },

                    success: function (e) {
                        $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
                        $(e).remove();
                    },

                    errorPlacement: function (error, element) {
                        if (element.is(':checkbox') || element.is(':radio')) {
                            var controls = element.closest('div[class*="col-"]');
                            if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                            else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                        }
                        else if (element.is('.select2')) {
                            error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                        }
                        else if (element.is('.chosen-select')) {
                            error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                        }
                        else error.insertAfter(element.parent());
                    }
                });

                // Knockout Datepicker custom binding handler
                ko.bindingHandlers.datepicker = {
                    init: function (element) {
                        $(element).datepicker({ autoclose: true }).next().on(ace.click_event, function () {
                            $(this).prev().focus();
                        });
                    },
                    update: function (element) {
                        $(element).datepicker("refresh");
                    }
                };

                // Knockout Timepicker custom binding handler
                ko.bindingHandlers.timepicker = {
                    init: function (element) {
                        $(element).timepicker({
                            defaultTime: "",
                            minuteStep: 1,
                            showSeconds: false,
                            showMeridian: false
                        }).next().on(ace.click_event, function () {
                            $(this).prev().focus();
                        });
                    },
                    update: function (element) {
                        $(element).timepicker('showWidget');
                    }
                };

                //dani 16-1-2016
                //for numberofunit
                ko.bindingHandlers.decimalPlacement = {
                    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
                        ko.utils.registerEventHandler(element, 'change', function (event) {
                            var observable = valueAccessor();
                            observable(CurrencyFormat(element.value));
                            observable.notifySubscribers(5);
                        });
                    },
                    update: function (element, valueAccessor, allBindingsAccessor) {
                        var value = ko.utils.unwrapObservable(valueAccessor());
                        $(element).val(value);
                    }
                };
                //dani end

                // Knockout Bindings
                //bangkit
                //ko.applyBindings(viewModel);
                //if (viewModel == null) {
                ko.applyBindings(viewModel, document.getElementById('home-transaction'));
                ko.applyBindings(viewModel, document.getElementById('frmUnderlying'));
                //}         

                // -- test error login

                // Token validation On Success function    
                function TokenOnSuccessLoad(data, textStatus, jqXHR) {
                    // store token on browser cookies
                    $.cookie(api.cookie.name, data.AccessToken);
                    accessToken = $.cookie(api.cookie.name);
                    // call spuser function
                    GetCurrentUser(viewModel);
                    // call get data inside view model
                    FXModel.token = accessToken;
                    //GetParameterData(FXModel, OnSuccessGetTotal, OnError);
                    GetThresholdParameter(FXModel, OnSuccessThresholdPrm, OnError);
                    // Performance
                    viewModel.GetParameters();
                    viewModel.GetData();
                    viewModel.GetRateIDR();
                    viewModel.GetFCYIDRTreshold();
                    viewModel.GetEmployeeLocation();
                }

                // Token validation On Error function
                function TokenOnErrorLoad(jqXHR, textStatus, errorThrown) {
                    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', true);
                }

                if ($.cookie(api.cookie.name) == undefined) {
                    Helper.Token.Request(TokenOnSuccessLoad, TokenOnErrorLoad);
                    //Helper.Token.Request(testSuccess, testError);
                    StartTaskHub();
                    //StartTaskHub();
                } else {
                    accessToken = $.cookie(api.cookie.name);
                    StartTaskHub();
                    GetCurrentUser(viewModel);
                    FXModel.token = accessToken;
                    //GetParameterData(FXModel, OnSuccessGetTotal, OnError);
                    GetThresholdParameter(FXModel, OnSuccessThresholdPrm, OnError);
                    // Performance
                    viewModel.GetParameters();
                    viewModel.GetData();
                    viewModel.GetRateIDR();
                    viewModel.GetEmployeeLocation();
                }

                // -- end test
                //Andriyanto 10-11-2015
                $.fn.ShowDetail = function (control) {
                    switch (viewModel.ActivityTitle()) {
                        case "CSO Revise Task":
                        case "Loan Maker Task":
                            var pID = viewModel.ProductID();
                            switch (pID) {
                                case ConsProductID.LoanRolloverProductIDCons:
                                case ConsProductID.LoanSettlementProductIDCons:
                                    var selection = $("#dataExcelRol").handsontable('getSelected');
                                    var row = selection[0];
                                    viewModel.GetDataDetails(row);
                                    break;
                            }
                            break;
                        case "Loan Checker Request Cancel Task":
                        case "Loan Checker Task":
                            var pID = viewModel.ProductID();
                            switch (pID) {
                                case ConsProductID.LoanRolloverProductIDCons:
                                case ConsProductID.LoanSettlementProductIDCons:
                                    var selection = $("#dataExcelRol").handsontable('getSelected');
                                    var row = selection[0];
                                    viewModel.GetDataDetails(row);
                                    break;
                            }
                            break;
                        case "Branch Checker Task":
                            var pID = viewModel.ProductID();
                            switch (pID) {
                                case ConsProductID.LoanSettlementProductIDCons:
                                    var selection = $("#dataExcelSS").handsontable('getSelected');
                                    var row = selection[0];
                                    viewModel.GetDataDetails(row);
                                    $('#modal-form-detail-loan').modal('show');
                                    $('#backDrop').show();
                                    break;
                            }
                            break;
                        case "Branch Checker After CSO":
                            var pID = viewModel.ProductID();
                            switch (pID) {
                                case ConsProductID.LoanRolloverProductIDCons:
                                case ConsProductID.LoanSettlementProductIDCons:
                                    var selection = $("#dataExcelRol").handsontable('getSelected');
                                    var row = selection[0];
                                    viewModel.GetDataDetails(row);
                                    $('#modal-form-detail-loan').modal('show');
                                    $('#backDrop').show();
                                    break;
                            }
                            break;
                    }

                };

                // Modal form on close handler
                $("#modal-form").on('hidden.bs.modal', function () {
                    //alert("close")

                    LockAndUnlockTaskHub("unlock", viewModel.WorkflowInstanceID(), viewModel.ApproverId(), viewModel.SPTaskListItemID(), viewModel.ActivityTitle());
                });
            })
            .fail(function (a, b, c) {
                console.error(c);
            })
        });
    });
});
//end 
