﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class ExceptionHandlingOpController : ApiController
    {
        private string message = string.Empty;

        private readonly IExceptionHandlingOperatorRepository repository = null;

        public ExceptionHandlingOpController()
        {
            this.repository = new ExceptionHandlingOperatorRepository();
        }

        public ExceptionHandlingOpController(IExceptionHandlingOperatorRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get all currencies.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IList<ExceptionHandlingOperatorModel>))]
        public HttpResponseMessage Get()
        {
            IList<ExceptionHandlingOperatorModel> output = new List<ExceptionHandlingOperatorModel>();

            if (!repository.GetExceptionHandlingOperator(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<ExceptionHandlingOperatorModel>>(HttpStatusCode.OK, output);
            }
        }        

        protected override void Dispose(bool disposing)
        {
            if (repository != null)
                repository.Dispose();
            base.Dispose(disposing);
        }
    }
}