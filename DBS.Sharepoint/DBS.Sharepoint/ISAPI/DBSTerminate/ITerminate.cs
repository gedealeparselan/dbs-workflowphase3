﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace DBS.Sharepoint
{
    #region Interfaces

    [ServiceContract]
    public interface ITerminate
    {
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]

        string TerminateWF(ItemProperties itemProp);


    }

    #endregion

    #region Properties
    [DataContract]
    public class ItemProperties
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string WFInstanceID { get; set; }
        [DataMember]
        public string namaList { get; set; }
    }

   
    #endregion
}
