﻿ChildModels["Loan"] = function () {
    var self = this;
    var loan_a;
    console.log(this);
    self.Init = function (callback) {
        self.startCalculateLoan();
        callback();
    };

    self.Load = function (id, callback) {
        callback();
    }

    //function load when page load
    self.startCalculateLoan = function () {
        console.log('a');
        loan_a = document.getElementById("trxn-amount");
        if (loan_a != null) {
            var xstored = loan_a.getAttribute("data-in");

            setInterval(function () {
                if (loan_a == document.activeElement) {
                    calculateLoan();
                }
            }, 100);
        }
    }

    function calculateLoan() {
        loan_a.value = loan_a.value.replace(',', '').replace(',', '').replace(',', '').replace(',', '');
        viewModel.TransactionLoanModel().Amount(loan_a.value);
        loan_a.value = formatNumber(loan_a.value);
    };
    //end

    //function save draft & save trx loan
    self.SaveLoan = function() {
        var form = $("#aspnetForm");
        form.validate();
        if (form.valid() && viewModel.TransactionLoanModel().Customer().CIF != "") {
            viewModel.TransactionLoanModel().IsDraft(false);
            viewModel.TransactionLoanModel().Amount($("#trxn-amount").val().replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(',', '').replace(' ', ''));
            viewModel.IsEditable(false);
            var data = {
                ApplicationID: viewModel.TransactionLoanModel().ApplicationID(),
                CIF: viewModel.TransactionLoanModel().Customer().CIF,
                Name: viewModel.TransactionLoanModel().Customer().Name
            };
            if (viewModel.Documents().length > 0) {
                UploadFileRecuresive(data, viewModel.Documents(), SaveLoanTransaction, viewModel.Documents().length);
            } else {
                SaveLoanTransaction();
            }
        }
        else {
            ShowNotification("Form Validation Warning", "Mandatory field must be filled", 'gritter-warning', false);
        }
    }

    self.SaveDraftLoan = function() {
        var data = {
            ApplicationID: viewModel.TransactionLoanModel().ApplicationID(),
            CIF: viewModel.TransactionLoanModel().Customer().CIF,
            Name: viewModel.TransactionLoanModel().Customer().Name
        };
        if (viewModel.Documents().length > 0) {
            UploadFileRecuresive(data, viewModel.Documents(), SaveLoanTransaction, viewModel.Documents().length);
        } else {
            SaveLoanTransaction();
        }
    }

    function SaveLoanTransaction() {
        viewModel.TransactionLoanModel().IsTopUrgent(viewModel.TransactionLoanModel().IsTopUrgent() == 1 ? 1 : 0);
        viewModel.TransactionLoanModel().IsTopUrgentChain(viewModel.TransactionLoanModel().IsTopUrgentChain() == 1 ? 1 : 0)
        viewModel.TransactionLoanModel().IsNormal(viewModel.TransactionLoanModel().IsNormal() == 1 ? 1 : 0)

        if (viewModel.TransactionLoanModel().IsDraft() == true) {

            if (viewModel.TransactionLoanModel().IsNewCustomer()) {
                var CustomerName = $("#customer-name").val();
                viewModel.TransactionLoanModel().CustomerDraft().DraftCustomerName(CustomerName);
                viewModel.TransactionLoanModel().CustomerDraft().DraftCIF(viewModel.TransactionLoanModel().Customer().CIF);
                viewModel.TransactionLoanModel().CustomerDraft().DraftAccountNumber(viewModel.TransactionLoanModel().Account());
                viewModel.TransactionLoanModel().Currency().DraftCurrencyID = viewModel.Selected().NewCustomer().Currency();
            }

            if (viewModel.TransactionLoanModel().ID() == null) {
                var options = {
                    url: api.server + api.url.transactionloan,
                    token: accessToken,
                    data: ko.toJSON(viewModel.TransactionLoanModel())
                };
                if (viewModel.TransactionLoanModel().Documents().length == viewModel.Documents().length)
                    Helper.Ajax.Post(options, OnSuccessSaveDraftLoan, viewModel.OnError, viewModel.OnAlways);
            } else {
                var options = {
                    url: api.server + api.url.transactionloan + "/Draft/" + viewModel.TransactionLoanModel().ID(),
                    token: accessToken,
                    data: ko.toJSON(viewModel.TransactionLoanModel())
                };
                if (viewModel.TransactionLoanModel().Documents().length == viewModel.Documents().length)
                    Helper.Ajax.Put(options, OnSuccessSaveDraftLoan, viewModel.OnError, viewModel.OnAlways);
            }
            return;
        }

        if (viewModel.TransactionLoanModel().Documents().length == viewModel.Documents().length) {
            var options = {
                url: api.server + api.url.transactionloan,
                token: accessToken,
                data: ko.toJSON(viewModel.TransactionLoanModel())
            };
            if (viewModel.TransactionLoanModel().ID() == null) {
                Helper.Ajax.Post(options, OnSuccessSaveAPILoan, viewModel.OnError, viewModel.OnAlways);
            } else {
                Helper.Ajax.Post(options, OnSuccessSaveAPILoan, viewModel.OnError, viewModel.OnAlways);
            }
        }

    }

    function OnSuccessSaveDraftLoan(data, textStatus, jqXHR) {
        if (data.ID != null || data.ID != undefined) {
            switch (viewModel.ProductID()) {
                case ConsProductID.LoanDisbursmentProductIDCons:
                case ConsProductID.LoanRolloverProductIDCons:
                case ConsProductID.LoanIMProductIDCons:
                case ConsProductID.LoanSettlementProductIDCons:
                    viewModel.TransactionLoanModel().ID(data.ID);
                    break;
            }
        }
        ShowNotification("Transaction Draft Success", "Transaction draft save attachments and underlying", "gritter-warning", true);
        window.location = "/home/draft-transactions";
    }

    function OnSuccessSaveAPILoan(data, textStatus, jqXHR) {
        if (data.ID != null || data.ID != undefined) {
            if (viewModel.TransactionLoanModel().ID() != null) {
                var DraftID = viewModel.TransactionLoanModel().ID();
                var AppID = {
                    TransactionID: data.ID,
                    ApplicationID: data.AppID
                };
                viewModel.RetIDColl([]);
                viewModel.RetIDColl.push(AppID);
                viewModel.TransactionLoanModel().ApplicationID(data.AppID);
                viewModel.TransactionLoanModel().ID(data.ID);
                viewModel.DeleteDraftTr(ProductName.loan, DraftID, data.ID);
            }
            else {
                var AppID = {
                    TransactionID: data.ID,
                    ApplicationID: data.AppID
                };
                viewModel.RetIDColl([]);
                viewModel.RetIDColl.push(AppID);
                viewModel.TransactionLoanModel().ApplicationID(data.AppID);
                viewModel.TransactionLoanModel().ID(data.ID);
                viewModel.AddListItem();
            }
        }
    }
    //end
}