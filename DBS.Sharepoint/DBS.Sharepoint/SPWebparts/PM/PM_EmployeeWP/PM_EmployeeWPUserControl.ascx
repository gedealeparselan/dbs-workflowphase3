﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PM_EmployeeWPUserControl.ascx.cs" Inherits="DBS.Sharepoint.SPWebparts.PM.PM_EmployeeWP.PM_EmployeeWPUserControl" %>

<h1 class="header smaller lighter dark">Employee</h1>

<div id="master">

    <!-- widget box start -->
    <div id="widget-box" class="widget-box">
        <!-- widget header start -->
        <div class="widget-header widget-hea1der-small header-color-dark">
            <h6>Employee Table</h6>

            <div class="widget-toolbar">
                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                    <i class="blue icon-filter"></i>
                    <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: GridProperties().AllowFilter" />
                    <span class="lbl"></span>
                </label>
                <a href="#" data-bind="click: GetData" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                    <i class="blue icon-refresh"></i>
                </a>
            </div>
        </div>
        <!-- widget header end -->
        <!-- widget body start -->
        <div class="widget-body">
            <!-- widget main start -->
            <div class="widget-main padding-0">
                <!-- widget slim control start -->
                <div class="slim-scroll" data-height="400">
                    <!-- widget content start -->
                    <div class="content">

                        <!-- table responsive start -->
                        <div class="table-responsive">
                            <div class="dataTables_wrapper" role="grid">
                                <table id="User Approval-table" class="table table-striped table-bordered table-hover dataTable">
                                    <thead>
                                        <tr data-bind="with: GridProperties">
                                            <th style="width: 50px">No.</th>
                                            <th data-bind="click: function () { Sorting('EmployeeUsername'); }, css: GetSortedColumn('EmployeeUsername')">Employee Username</th>
                                            <th data-bind="click: function () { Sorting('EmployeeName'); }, css: GetSortedColumn('EmployeeName')">Employee Name</th>
                                            <th data-bind="click: function () { Sorting('EmployeeEmail'); }, css: GetSortedColumn('EmployeeEmail')">Employee Email</th>
                                            <th data-bind="click: function () { Sorting('RankCode'); }, css: GetSortedColumn('Rank')">Rank</th>
                                            <th data-bind="click: function () { Sorting('SegmentName'); }, css: GetSortedColumn('Segment')">Segment</th>
                                            <th data-bind="click: function () { Sorting('Branch.Name'); }, css: GetSortedColumn('Branch')">Branch</th>
                                            <th data-bind="click: function () { Sorting('FunctionRole.Name'); }, css: GetSortedColumn('FunctionRole')">Function Role</th>
                                            <th data-bind="click: function () { Sorting('LastModifiedBy'); }, css: GetSortedColumn('LastModifiedBy')">Modified By</th>
                                            <th data-bind="click: function () { Sorting('LastModifiedDate'); }, css: GetSortedColumn('LastModifiedDate')">Modified Date</th>
                                            <th data-bind="click: function () { Sorting('UserCategoryCode'); }, css: GetSortedColumn('UserCategoryCode')">User Category Code</th>

                                        </tr>
                                    </thead>
                                    <thead data-bind="visible: GridProperties().AllowFilter" class="table-filter">
                                        <tr>
                                            <th class="clear-filter">
                                                <a href="#" data-bind="click: ClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                    <i class="green icon-trash"></i>
                                                </a>
                                            </th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterEmployeeUsername, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterEmployeeName, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterEmployeeEmail, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterRank, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterSegment, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterBranch, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterRole, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterModifiedBy, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.FilterModifiedDate, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterUserCategoryCode, event: { change: GridProperties().Filter }" /></th>
                                        </tr>
                                    </thead>
                                    <tbody data-bind="foreach: UserApprovals, visible: UserApprovals().length > 0">
                                        <tr data-bind="click: $root.GetSelectedRow" data-toggle="modal">
                                            <td><span data-bind="text: RowID"></span></td>
                                            <td><span data-bind="text: EmployeeUsername.split('|')[2]"></span></td>
                                            <td><span data-bind="text: EmployeeName"></span></td>
                                            <td><span data-bind="text: EmployeeEmail"></span></td>
                                            <td><span data-bind="text: RankCode"></span></td>
                                            <td><span data-bind="text: SegmentName"></span></td>
                                            <td><span data-bind="text: Branch.Name"></span></td>
                                            <td><span data-bind="text: FunctionRole.Name"></span></td>
                                            <!--<td><span data-bind="text: Role.Name"></span></td>-->
                                            <td><span data-bind="text: LastModifiedBy"></span></td>
                                            <td><span data-bind="text: $root.LocalDate(LastModifiedDate)"></span></td>
                                            <td><span data-bind="text:UserCategoryCode"></span></td>
                                        </tr>
                                    </tbody>
                                    <tbody data-bind="visible: UserApprovals().length == 0">
                                        <tr>
                                            <td colspan="11" class="text-center">No entries available.
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- table responsive end -->
                    </div>
                    <!-- widget content end -->
                </div>
                <!-- widget slim control end -->
                <!-- widget footer start -->
                <div class="widget-toolbox padding-8 clearfix">
                    <div class="row" data-bind="with: GridProperties">
                        <!-- pagination size start -->
                        <div class="col-sm-6">
                            <div class="dataTables_paginate paging_bootstrap pull-left">
                                Showing
                                <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                rows of <span data-bind="text: Total"></span>
                                entries
                            </div>
                        </div>
                        <!-- pagination size end -->
                        <!-- pagination page jump start -->
                        <div class="col-sm-3">
                            <div class="dataTables_paginate paging_bootstrap">
                                Page
                                <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                of <span data-bind="text: TotalPages"></span>
                            </div>
                        </div>
                        <!-- pagination page jump end -->
                        <!-- pagination navigation start -->
                        <div class="col-sm-3">
                            <div class="dataTables_paginate paging_bootstrap">
                                <ul class="pagination">
                                    <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                            <i class="icon-double-angle-left"></i>
                                        </a>
                                    </li>
                                    <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                            <i class="icon-angle-left"></i>
                                        </a>
                                    </li>
                                    <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                            <i class="icon-angle-right"></i>
                                        </a>
                                    </li>
                                    <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                            <i class="icon-double-angle-right"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- pagination navigation end -->

                    </div>
                </div>
                <!-- widget footer end -->

            </div>
            <!-- widget main end -->
        </div>
        <!-- widget body end -->
    </div>
    <!-- widget box end -->

    <div data-bind="if: $root.IsRoleMaker()">
        <h4 class="pink">
            <i class="icon-hand-right green"></i>
            <a href="#modal-form" role="button" class="blue" data-toggle="modal" data-bind="click: NewData">New Employee
            </a>
        </h4>
    </div>

    <!-- modal form start -->
    <div id="modal-form" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" style="width: 70%">
            <div class="modal-content">
                <!-- modal header start -->
                <div class="modal-header">
                    <h4 class="blue bigger">
                        <span data-bind="if: $root.IsNewData()">Create a new Employee Parameter</span>
                        <span data-bind="if: !$root.IsNewData()">Modify a Employee Parameter</span>
                    </h4>
                </div>
                <!-- modal header end -->
                <!-- modal body start -->
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- modal body form start -->
                            <div id="customer-form" class="form-horizontal" role="form">

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="EmployeeUsername">
                                        Employee Username
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="EmployeeUsername" name="EmployeeUsername" data-bind="value: $root.EmployeeUsername().split('|')[2], enable: IsNewData" class="col-xs-10 col-sm-7" data-rule-required="true" data-rule-maxlength="50" />
                                            <label class="control-label bolder text-danger">*</label>
                                            <div id="load-bar" data-bind="visible: $root.BarLoad"><i style="margin-left: 5px;" class="icon-spinner icon-spin red bigger-200"></i></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="EmployeeName">
                                        Employee Name
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="EmployeeName" name="EmployeeName" data-bind="value: $root.EmployeeName, enable: false" class="col-xs-10 col-sm-7" data-rule-required="true" data-rule-maxlength="50" />
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="EmployeeEmail">
                                        Employee Email
                                    </label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="EmployeeEmail" name="EmployeeEmail" data-bind="value: $root.EmployeeEmail, enable: false" class="col-xs-10 col-sm-7" data-rule-required="true" data-rule-maxlength="255" data-rule-email="true" />
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="rank">
                                        Rank
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="rank" name="rank" data-bind="value: Rank().ID, options: ddlRank, optionsText: function (item) { if (item.Name != null) return item.Name + ' (' + item.Description + ')' }, optionsValue: 'ID', optionsCaption: 'Choose...'" class="col-xs-6"></select>
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="segment">
                                        Segment
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="segment" name="segment" data-bind="value: Segment().ID, options: ddlSegment, optionsText: function (item) { if (item.Name != null) return item.Name + ' (' + item.Description + ')' }, optionsValue: 'ID', optionsCaption: 'Choose...'" class="col-xs-6"></select>
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="branch">
                                        Branch
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="branch" name="branch" data-bind="value: Branch().ID, options: ddlBranch, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Choose...'" data-rule-required="true" data-rule-value="true" class="col-xs-6"></select>
                                            <label class="control-label bolder text-danger" for="branch">*</label>
                                        </div>
                                    </div>
                                </div>

                                <%--awal--%>

                                <div class="space-4"></div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="user-category">
                                        User Category
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="user-category" name="user-category" data-bind="value: UserCategory().Code, options: ddlUserCategory, optionsText: 'UserCategoryCode', optionsValue: 'UserCategoryCode',event: { change: $root.OnChangeMode }, optionsCaption: 'Choose...'" data-rule-required="false" data-rule-value="true" class="col-xs-6"></select>
                                        </div>
                                    </div>
                                </div>
                                <%--akhir--%>
                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="function-role">
                                        Function Role
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="function-role" name="function-role" data-bind="value: FunctionRole().ID, options: ddlFunctionRole, optionsText: function (item) { if (item.Name != null) return item.Name + ' (' + item.Description + ')' }, optionsValue: 'ID', optionsCaption: 'Choose...'" class="col-xs-6"></select>
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="role">
                                        Role
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix" style="margin-right: 20px">

                                            <!-- Start Grid Role -->
                                            <div class="slim-scroll" data-height="400">
                                                <!-- widget content start -->
                                                <div class="content">

                                                    <div class="table-responsive">
                                                        <div class="table-responsive" role="grid">
                                                            <table id="role-table" class="table table-striped table-bordered table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="center">Name</th>
                                                                        <th class="center">Description</th>
                                                                        <th class="center">Select</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody data-bind="foreach: Role()">
                                                                    <tr>
                                                                        <td><span data-bind="text: Name"></span></td>
                                                                        <td><span data-bind="text: Description"></span></td>
                                                                        <td class="center">
                                                                            <label>
                                                                                <input type="checkbox" data-bind="value: ID, event: { change: $root.SelectRole }, attr: { id: 'checkbox' + $index() + '' }" />
                                                                                <span class="lbl"></span>
                                                                            </label>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <!-- End Grid Role -->

                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- modal body form end -->
                        </div>
                    </div>
                </div>
                <!-- modal body end -->
                <!-- modal footer start -->
                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.save, visible: $root.IsNewData() && $root.IsRoleMaker()">
                        <i id="ADD" class="icon-save"></i>
                        Save
                    </button>
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.update, visible: !$root.IsNewData() && $root.IsRoleMaker()">
                        <i id="UPDATE" class="icon-edit"></i>
                        Update
                    </button>
                    <button class="btn btn-sm btn-warning" data-bind="click: $root.update, visible: !$root.IsNewData() && $root.IsRoleMaker()">
                        <i id="DELETE" class="icon-trash"></i>
                        Delete
                    </button>
                    <button class="btn btn-sm" data-dismiss="modal" data-bind="click: $root.clear">
                        <i class="icon-remove"></i>
                        Close
                    </button>
                </div>
                <!-- modal footer end -->

            </div>
        </div>
    </div>
    <!-- modal form end -->

</div>

<div id="draft">

    <!-- widget box start -->
    <div id="widget-box" class="widget-box">
        <!-- widget header start -->
        <div class="widget-header widget-hea1der-small header-color-dark">
            <h6>Employee Table</h6>

            <div class="widget-toolbar">
                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                    <i class="blue icon-filter"></i>
                    <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: GridProperties().AllowFilter" />
                    <span class="lbl"></span>
                </label>
                <a href="#" data-bind="click: GetData" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                    <i class="blue icon-refresh"></i>
                </a>
            </div>
        </div>
        <!-- widget header end -->
        <!-- widget body start -->
        <div class="widget-body">
            <!-- widget main start -->
            <div class="widget-main padding-0">
                <!-- widget slim control start -->
                <div class="slim-scroll" data-height="400">
                    <!-- widget content start -->
                    <div class="content">

                        <!-- table responsive start -->
                        <div class="table-responsive">
                            <div class="dataTables_wrapper" role="grid">
                                <table id="User Approval-table" class="table table-striped table-bordered table-hover dataTable">
                                    <thead>
                                        <tr data-bind="with: GridProperties">
                                            <th style="width: 50px">No.</th>
                                            <th data-bind="click: function () { Sorting('EmployeeUsername'); }, css: GetSortedColumn('EmployeeUsername')">Employee Username</th>
                                            <th data-bind="click: function () { Sorting('EmployeeName'); }, css: GetSortedColumn('EmployeeName')">Employee Name</th>
                                            <th data-bind="click: function () { Sorting('EmployeeEmail'); }, css: GetSortedColumn('EmployeeEmail')">Employee Email</th>
                                            <th data-bind="click: function () { Sorting('RankCode'); }, css: GetSortedColumn('Rank')">Rank</th>
                                            <th data-bind="click: function () { Sorting('SegmentName'); }, css: GetSortedColumn('Segment')">Segment</th>
                                            <th data-bind="click: function () { Sorting('Branch.Name'); }, css: GetSortedColumn('Branch')">Branch</th>
                                            <th data-bind="click: function () { Sorting('FunctionRole.Name'); }, css: GetSortedColumn('FunctionRole')">Function Role</th>
                                            <th data-bind="click: function () { Sorting('LastModifiedBy'); }, css: GetSortedColumn('LastModifiedBy')">Modified By</th>
                                            <th data-bind="click: function () { Sorting('LastModifiedDate'); }, css: GetSortedColumn('LastModifiedDate')">Modified Date</th>
                                            <th data-bind="click: function () { Sorting('UserCategoryCode'); }, css: GetSortedColumn('UserCategoryCode')">User Category Code</th>
                                        </tr>
                                    </thead>
                                    <thead data-bind="visible: GridProperties().AllowFilter" class="table-filter">
                                        <tr>
                                            <th class="clear-filter">
                                                <a href="#" data-bind="click: ClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                    <i class="green icon-trash"></i>
                                                </a>
                                            </th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterEmployeeUsername, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterEmployeeName, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterEmployeeEmail, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterRank, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterSegment, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterBranch, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterFunctionRole, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterModifiedBy, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.FilterModifiedDate, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterUserCategoryCode, event: { change: GridProperties().Filter }" /></th>
                                        </tr>
                                    </thead>
                                    <tbody data-bind="foreach: UserApprovals, visible: UserApprovals().length > 0">
                                        <tr data-bind="click: $root.GetSelectedRow" data-toggle="modal">
                                            <td><span data-bind="text: RowID"></span></td>
                                            <td><span data-bind="text: EmployeeUsername.split('|')[2]"></span></td>
                                            <td><span data-bind="text: EmployeeName"></span></td>
                                            <td><span data-bind="text: EmployeeEmail"></span></td>
                                            <td><span data-bind="text: RankCode"></span></td>
                                            <td><span data-bind="text: SegmentName"></span></td>
                                            <td><span data-bind="text: Branch.Name"></span></td>
                                            <td><span data-bind="text: FunctionRole.Name"></span></td>
                                            <td><span data-bind="text: LastModifiedBy"></span></td>
                                            <td><span data-bind="text: $root.LocalDate(LastModifiedDate)"></span></td>
                                            <td><span data-bind="text: UserCategoryCode"></span></td>
                                        </tr>
                                    </tbody>
                                    <tbody data-bind="visible: UserApprovals().length == 0">
                                        <tr>
                                            <td colspan="11" class="text-center">No entries available.
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- table responsive end -->
                    </div>
                    <!-- widget content end -->
                </div>
                <!-- widget slim control end -->
                <!-- widget footer start -->
                <div class="widget-toolbox padding-8 clearfix">
                    <div class="row" data-bind="with: GridProperties">
                        <!-- pagination size start -->
                        <div class="col-sm-6">
                            <div class="dataTables_paginate paging_bootstrap pull-left">
                                Showing
                                <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                rows of <span data-bind="text: Total"></span>
                                entries
                            </div>
                        </div>
                        <!-- pagination size end -->
                        <!-- pagination page jump start -->
                        <div class="col-sm-3">
                            <div class="dataTables_paginate paging_bootstrap">
                                Page
                                <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                of <span data-bind="text: TotalPages"></span>
                            </div>
                        </div>
                        <!-- pagination page jump end -->
                        <!-- pagination navigation start -->
                        <div class="col-sm-3">
                            <div class="dataTables_paginate paging_bootstrap">
                                <ul class="pagination">
                                    <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                            <i class="icon-double-angle-left"></i>
                                        </a>
                                    </li>
                                    <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                            <i class="icon-angle-left"></i>
                                        </a>
                                    </li>
                                    <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                            <i class="icon-angle-right"></i>
                                        </a>
                                    </li>
                                    <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                            <i class="icon-double-angle-right"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- pagination navigation end -->

                    </div>
                </div>
                <!-- widget footer end -->

            </div>
            <!-- widget main end -->
        </div>
        <!-- widget body end -->
    </div>
    <!-- widget box end -->
    <!-- modal form start -->
    <div id="modal-form-draft" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" style="width: 70%">
            <div class="modal-content">
                <!-- modal header start -->
                <div class="modal-header">
                    <h4 class="blue bigger">
                        <span data-bind="if: $root.IsNewData()">Create a new Employee Parameter</span>
                        <span data-bind="if: !$root.IsNewData()">Modify a Employee Parameter</span>
                    </h4>
                </div>
                <!-- modal header end -->
                <!-- modal body start -->
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- modal body form start -->
                            <div id="customer-form" class="form-horizontal" role="form">

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="EmployeeUsername">
                                        Employee Username
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="EmployeeUsername" name="EmployeeUsername" data-bind="value: $root.EmployeeUsername().split('|')[2], enable: IsNewData" class="col-xs-10 col-sm-7" data-rule-required="true" data-rule-maxlength="50" />
                                        </div>
                                        <div id="load-bar" data-bind="visible: $root.BarLoad"><i style="margin-left: 5px;" class="icon-spinner icon-spin red bigger-200"></i></div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="EmployeeName">
                                        Employee Name
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="EmployeeName" name="EmployeeName" data-bind="value: $root.EmployeeName, enable: false" class="col-xs-10 col-sm-7" data-rule-required="true" data-rule-maxlength="50" />
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="EmployeeEmail">
                                        Employee Email
                                    </label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input type="text" id="EmployeeEmail" name="EmployeeEmail" data-bind="value: $root.EmployeeEmail, enable: false" class="col-xs-10 col-sm-7" data-rule-required="true" data-rule-maxlength="255" data-rule-email="true" />
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="rank">
                                        Rank
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="rank" name="rank" data-bind="value: Rank().ID, enable: false, options: ddlRank, optionsText: function (item) { if (item.Name != null) return item.Name + ' (' + item.Description + ')' }, optionsValue: 'ID', optionsCaption: 'Choose...'" data-rule-required="true" data-rule-value="true" class="col-xs-6"></select>
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="segment">
                                        Segment
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="segment" name="segment" data-bind="value: Segment().ID, enable: false, options: ddlSegment, optionsText: function (item) { if (item.Name != null) return item.Name + ' (' + item.Description + ')' }, optionsValue: 'ID', optionsCaption: 'Choose...'" data-rule-required="true" data-rule-value="true" class="col-xs-6"></select>
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="branch">
                                        Branch
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="branch" name="branch" data-bind="value: Branch().ID, enable: false, options: ddlBranch, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Choose...'" data-rule-required="true" data-rule-value="true" class="col-xs-6"></select>
                                        </div>
                                    </div>
                                </div>

                                <%--awal--%>

                                <div class="space-4"></div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="user-category">
                                        User Category
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="user-category" name="user-category" data-bind="value: UserCategory().Code, enable: false, options: ddlUserCategory, optionsText: 'UserCategoryCode', optionsValue: 'UserCategoryCode', optionsCaption: 'Choose...'" data-rule-required="true" data-rule-value="true" class="col-xs-6"></select>
                                        </div>
                                    </div>
                                </div>
                                <%--akhir--%>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="function-role">
                                        Function Role
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="function-role" name="function-role" data-bind="value: FunctionRole().ID, enable: false, options: ddlFunctionRole, optionsText: function (item) { if (item.Name != null) return item.Name + ' (' + item.Description + ')' }, optionsValue: 'ID', optionsCaption: 'Choose...'" data-rule-required="true" data-rule-value="true" class="col-xs-6"></select>
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="role">
                                        Role
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix" style="margin-right: 20px">

                                            <div class="slim-scroll" data-height="400">
                                                <!-- widget content start -->
                                                <div class="content">
                                                    <!-- Start Grid Role -->
                                                    <div class="table-responsive">
                                                        <div class="table-responsive" role="grid">
                                                            <table id="role-table" class="table table-striped table-bordered table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="center">Name</th>
                                                                        <th class="center">Description</th>
                                                                        <th class="center">Select</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody data-bind="foreach: Role()">
                                                                    <tr>
                                                                        <td><span data-bind="text: Name"></span></td>
                                                                        <td><span data-bind="text: Description"></span></td>
                                                                        <td class="center">
                                                                            <label>
                                                                                <input type="checkbox" data-bind="value: ID, enable: false, event: { change: $root.SelectRole }, attr: { id: 'checkbox' + $index() + '' }" />
                                                                                <span class="lbl"></span>
                                                                            </label>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <!-- End Grid Role -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- modal body form end -->
                        </div>
                    </div>
                </div>
                <!-- modal body end -->
                <!-- modal footer start -->
                <div class="modal-footer">
                    <!--<button class="btn btn-sm btn-primary" data-bind="click: $root.save, visible: $root.IsNewData()">
                            <i id="ADD" class="icon-save"></i>
                            Save
                        </button>
                        <button class="btn btn-sm btn-primary" data-bind="click: $root.update, visible: !$root.IsNewData()">
                            <i id="UPDATE" class="icon-edit"></i>
                            Update
                        </button>
                        <button class="btn btn-sm btn-warning" data-bind="click: $root.update, visible: !$root.IsNewData()">
                            <i id="DELETE" class="icon-trash"></i>
                            Delete
                        </button>-->
                    <button class="btn btn-sm" data-dismiss="modal" data-bind="click: $root.clear">
                        <i class="icon-remove"></i>
                        Close
                    </button>
                </div>
                <!-- modal footer end -->

            </div>
        </div>
    </div>
    <!-- modal form end -->

</div>

<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/additional-methods.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/UserApproval.js"></script>
