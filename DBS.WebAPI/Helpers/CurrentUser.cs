﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Helpers
{
    public class CurrentUser
    {
        public SharepointUserModel GetCurrentUser()
        {
            var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;
            var claims = from c in identity.Claims select new { CType = c.Type, Value = c.Value, ValueType = c.ValueType };
            SharepointUserModel spuser = new SharepointUserModel();
            IList<UserRole> roles = new List<UserRole>();

            foreach (var item in identity.Claims)
            {
                switch (item.Type)
                {
                    case "ID":
                        spuser.ID = Convert.ToInt32(item.Value);
                        break;
                    case "LoginName":
                        spuser.LoginName = item.Value;
                        break;
                    case "DisplayName":
                        spuser.DisplayName = item.Value;
                        break;
                    case "Email":
                        spuser.Email = item.Value;
                        break;
                    case ClaimTypes.Role:
                        roles.Add(new UserRole { Name = item.Value });
                        break;
                    default:
                        break;
                }
            }
            spuser.Roles = roles;

            return spuser;
        }
    }
}