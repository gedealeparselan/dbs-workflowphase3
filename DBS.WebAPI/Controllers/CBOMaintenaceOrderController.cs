﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;

namespace DBS.WebAPI.Controllers
{
    public class CBOMaintenanceOrderController : ApiController
    {
        private string message = string.Empty;

        private readonly ICBOMaintenancePriorityOrderModelRepository repository = null;

        public CBOMaintenanceOrderController()
        {
            this.repository = new CBOMaintenanceOrderRepository();
        }

        public CBOMaintenanceOrderController(ICBOMaintenancePriorityOrderModelRepository repository)
        {
            this.repository = repository;
        }
        /// <summary>
        /// Get customers with maximum rows will be retreive and select page index to display.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(IList<CBOMaintenancePriorityOrderModel>))]
       
        public HttpResponseMessage GetCBOMaintenaceType()
        {
            IList<CBOMaintenancePriorityOrderModel> output = new List<CBOMaintenancePriorityOrderModel>();

            if (!repository.GetCBOMaintenaceType(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<CBOMaintenancePriorityOrderModel>>(HttpStatusCode.OK, output);
            }
        }
        [ResponseType(typeof(IList<CBOMaintenancePriorityOrderModel>))]
        public HttpResponseMessage GetCBOMaintenaceTypePriority()
        {
            IList<CBOMaintenancePriorityOrderModel> output = new List<CBOMaintenancePriorityOrderModel>();

            if (!repository.GetCBOMaintenaceTypePriority(ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.NoContent, message);
            }
            else
            {
                return Request.CreateResponse<IList<CBOMaintenancePriorityOrderModel>>(HttpStatusCode.OK, output);
            }
        }
        [HttpPost]
        [ResponseType(typeof(string))]
        [Authorize(Roles = "DBS Admin")]
        public HttpResponseMessage Post(IList<CBOMaintenancePriorityOrderModel> cboMaintenancePriorityOrder)
        {
            
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                if (!repository.AddCBOMaintenaceType(cboMaintenancePriorityOrder, ref message))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = message});
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, new { Message = "New CBo Maintenance Priority Order data has been created." });
                }
            }
        }
    }
}
