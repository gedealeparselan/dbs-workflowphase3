﻿using Microsoft.SharePoint;
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using AceNavigation.Repository;

namespace AceNavigation.CONTROLTEMPLATES
{
    public partial class AceSPHeaderNavigation : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            using (SPSite site = new SPSite(SPContext.Current.Site.RootWeb.Url))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    if (!Page.IsPostBack)
                    {
                        Insert(web.CurrentUser.LoginName, "URL Access", HttpContext.Current.Request.Url.AbsolutePath);
                        lblDisplayName.Text = web.CurrentUser.Name;
                    }
                }
            }
        }

        protected void lbLogout_Click(object sender, EventArgs e)
        {
            if (Request.Cookies["DBSApi"] != null)
            {
                HttpCookie dbsCookie = new HttpCookie("DBSApi");
                dbsCookie.Expires = DateTime.Now.AddDays(-1d);
                Response.Cookies.Add(dbsCookie);
            }

            Insert(SPContext.Current.Web.CurrentUser.LoginName, "Logout", HttpContext.Current.Request.Url.AbsolutePath);
            string sUrl = string.Format("~/_layouts/closeConnection.aspx?loginasanotheruser=true&Source={0}", HttpUtility.UrlEncode(SPContext.Current.Web.Url));
            Response.Redirect(sUrl);
        }

        public void Insert(string userName, string logType, string remark)
        {
            if (!remark.ToLower().EndsWith("?antitimeout=1"))
            {
                SystemLogRepo repo = new SystemLogRepo();
                repo.InsertSystemLog(userName, logType, remark);
            }
        }
    }
}
