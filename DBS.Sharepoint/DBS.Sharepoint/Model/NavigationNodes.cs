﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DBS.Sharepoint.Model
{
    [DataContract]
    public class NavigationNodes
    {
        [DataMember]
        public string ID { get; set; }
        [DataMember]
        public string ParentID { get; set; }
        [DataMember]
        public string ParentTitle { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string FriendlyUrl { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string Icon { get; set; }
        [DataMember]
        public bool IsHide { get; set; }
        [DataMember]
        public bool IsHasAccess { get; set; }
    }
}
