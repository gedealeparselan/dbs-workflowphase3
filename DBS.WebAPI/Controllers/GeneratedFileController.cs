﻿using DBS.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using DBS.WebAPI.Models;
using DBS.Entity;
using DBS.WebAPI.Helpers;
using DBS.WebAPI.Areas.HelpPage.ModelDescriptions;

namespace DBS.WebAPI.Controllers
{
    [Authorize]
    public class GeneratedFileController : ApiController
    {
        private string message = string.Empty;

        private readonly IGeneratedFileModelRepository repository = null;

        public GeneratedFileController()
        {
            this.repository = new GeneratedFileModelRepository();
        }

        public GeneratedFileController(IGeneratedFileModelRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get Generated File for data grid.
        /// </summary>
        /// <param name="limit">Total rows will be retreive, default is 10.</param>
        /// <param name="index">Select page index, default is 0.</param>
        /// <returns></returns>
        [ResponseType(typeof(GeneratedFileGrid))]
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Get(int? page, int? size, string sort_column, string sort_order, IList<GeneratedFileFilter> filters)
        {
            GeneratedFileGrid output = new GeneratedFileGrid();

            if (!repository.GetCompletedTransaction(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filters, string.IsNullOrEmpty(sort_column) ? "FileName" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<GeneratedFileGrid>(HttpStatusCode.OK, output);
            }
        }
        [ResponseType(typeof(GeneratedFileModel))]
        [Route("api/GeneratedFile/{applicationID}")]
        public HttpResponseMessage Get(string applicationID)
        {
            IList<GeneratedFileModel> Output = new List<GeneratedFileModel>();

            if (!repository.GetGenerateFileName(applicationID, ref Output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<IList<GeneratedFileModel>>(HttpStatusCode.OK, Output);
            }
        }

        // add by adi
        [ResponseType(typeof(GeneratedFileXMLGrid))]
        //[Route("api/GeneratedFileXML")]
        [AcceptVerbs("GET", "POST")]
        [Route("api/GeneratedFile/FileXML")]
        public HttpResponseMessage GetGeneratedXML(int? page, int? size, string sort_column, string sort_order, IList<GeneratedFileXMLFilter> filtersxml)
        {
            GeneratedFileXMLGrid Output = new GeneratedFileXMLGrid();

            if (!repository.GetGeneratedFileXML(page.HasValue ? page.Value : 0, size.HasValue ? size.Value : 10, filtersxml, string.IsNullOrEmpty(sort_column) ? "FileName" : sort_column, string.IsNullOrEmpty(sort_order) ? "DESC" : sort_order, ref Output, ref message))
            {
                return Request.CreateResponse<string>(HttpStatusCode.BadRequest, message);
            }
            else
            {
                return Request.CreateResponse<GeneratedFileXMLGrid>(HttpStatusCode.OK, Output);
            }
        }
    }
}