﻿var accessToken;
var DocumentModel = ko.observable({ FileName: '', DocumentPath: '' });
var GridPropertiesModel = function (callback) {
    var self = this;

    self.SortColumn = ko.observable();
    self.SortOrder = ko.observable();
    self.AllowFilter = ko.observable(false);
    self.AvailableSizes = ko.observableArray(config.sizes);
    self.Page = ko.observable(1);
    self.Size = ko.observable(config.sizeDefault);
    self.Total = ko.observable(0);
    self.TotalPages = ko.observable(0);

    self.Callback = function () {
        callback();
    };

    self.OnPageSizeChange = function () {
        self.Page(1);

        self.Callback();
    };

    self.OnPageChange = function () {
        if (self.Page() < 1) {
            self.Page(1);
        } else {
            if (self.Page() > self.TotalPages())
                self.Page(self.TotalPages());
        }

        self.Callback();
    };

    self.NextPage = function () {
        var page = self.Page();
        if (page < self.TotalPages()) {
            self.Page(page + 1);

            self.Callback();
        }
    };

    self.PreviousPage = function () {
        var page = self.Page();
        if (page > 1) {
            self.Page(page - 1);

            self.Callback();
        }
    };

    self.FirstPage = function () {
        self.Page(1);

        self.Callback();
    };

    self.LastPage = function () {
        self.Page(self.TotalPages());

        self.Callback();
    };

    self.Filter = function (data) {
        self.Page(1);

        self.Callback();
    };

    // get sorted column
    self.GetSortedColumn = function (columnName) {
        var sort = "sorting";

        if (self.SortColumn() == columnName) {
            if (self.SortOrder() == "DESC")
                sort = sort + "_asc";
            else
                sort = sort + "_desc";
        }

        return sort;
    };

    // Sorting data
    self.Sorting = function (column) {
        if (self.SortColumn() == column) {
            if (self.SortOrder() == "ASC")
                self.SortOrder("DESC");
            else
                self.SortOrder("ASC");
        } else {
            self.SortOrder("ASC");
        }

        self.SortColumn(column);

        self.Page(1);

        self.Callback();
    };
};
var SPRole = {
    ID: ko.observable(),
    Name: ko.observable()
};
var SPUser = {
    DisplayName: ko.observable(),
    Email: ko.observable(),
    ID: ko.observable(),
    LoginName: ko.observable(),
    Roles: ko.observableArray([SPRole])
};
var FileTipeModel = {
    ID: ko.observable(),
    TipeDesc: ko.observable()
}
var Parameter = {
    Filetipes: ko.observableArray([FileTipeModel])
}
var SelectedModel = {
    Filetipes: ko.observable()
}
var ViewModel = function () {
    var self = this;
    self.SPUser = ko.observable(SPUser);
    self.ID = ko.observable("");
    self.Filetipes = ko.observable(Parameter);
    self.Document = ko.observable("");
    self.LastModifiedBy = ko.observable("");
    self.LastModifiedDate = ko.observable("");

    self.LocalDate = function (date, isDateOnly, isDateLong) {
        /*
        var localDate = new Date(date);
        if (date == '1970/01/01 00:00:00' || date == null) {
            return "";
        }

        if (moment(localDate).isValid()) {
            if (isDateOnly != undefined || isDateOnly == true) {
                if (isDateLong)
                    return moment(localDate).format(config.format.dateLong);
                else
                    return moment(localDate).format(config.format.date);
            } else {
                return moment(localDate).format(config.format.dateTime);
            }
        } else {
            return "";
        }
        */
        return Helper.LocalDate(date, isDateOnly, isDateLong); //Rizki - 2017-05-17
    };

    self.FilterSchemeType = ko.observable("");
    self.FilterAccountNumber = ko.observable("");
    self.FilterIntTableCode = ko.observable("");
    self.FilterVersion = ko.observable("");
    self.FilterPrefIntCr = ko.observable("");
    self.FilterPrefIntDr = ko.observable("");
    self.FilterIntPegged = ko.observable("");
    self.FilterPeggingFreq = ko.observable("");
    self.FilterPegReviewDate = ko.observable("");
    self.FilterStartDate = ko.observable("");
    self.FilterEndDate = ko.observable("");
    self.FilterInterestMargin = ko.observable("");

    // grid properties
    self.availableSizes = ko.observableArray([10, 25, 50, 75, 100]);
    self.Page = ko.observable(1);
    self.Size = ko.observableArray([10]);
    self.Total = ko.observable(0);
    self.TotalPages = ko.observable(0);


    // grid properties
    self.GridProperties = ko.observable();
    self.GridProperties(new GridPropertiesModel(FDBindIM));

    self.GridProperties().SortColumn("SchemeType");
    self.GridProperties().SortOrder("ASC");

    // Options selected value
    self.Selected = ko.observable(SelectedModel);

    // Parameters
    self.Parameter = ko.observable(Parameter);

    self.Modul = function () {
        var url = window.location.href;
        var urlmodul = url.substring(url.lastIndexOf("/") + 1);
        var arrmodul = urlmodul.split("-");
        var modul = null;
        switch (arrmodul[2]) {
            case "fd":
                modul = "fd";
                break;
            case "loan":
                modul = "loan";
                break;
        }
        return modul;
    }
    //Declare an ObservableArray for Storing the JSON Response
    self.FieldUploadItems = ko.observableArray([]);
    self.FailedUploads = ko.observableArray([]);
    self.DocumentPath = ko.observable();

    self.ClearFilters = function () {
        self.FilterSchemeType("");
        self.FilterAccountNumber("");
        self.FilterIntTableCode("");
        self.FilterVersion("");
        self.FilterPrefIntCr("");
        self.FilterPrefIntDr("");
        self.FilterIntPegged("");
        self.FilterPeggingFreq("");
        self.FilterPegReviewDate("");
        self.FilterStartDate("");
        self.FilterEndDate("");
        self.FilterInterestMargin("");

        FDBindIM();
    };
    function GetFilteredColumns() {
        // define filter
        var filters = [];

        if (self.FilterSchemeType() != "") filters.push({ Field: 'SchemeType', Value: self.FilterSchemeType() });
        if (self.FilterAccountNumber() != "") filters.push({ Field: 'AccountNumber', Value: self.FilterAccountNumber() });
        if (self.FilterIntTableCode() != "") filters.push({ Field: 'IntTableCode', Value: self.FilterIntTableCode() });
        if (self.FilterVersion() != "") filters.push({ Field: 'Version', Value: self.FilterVersion() });
        if (self.FilterPrefIntCr() != "") filters.push({ Field: 'PrefIntCr', Value: self.FilterPrefIntCr() });
        if (self.FilterPrefIntDr() != "") filters.push({ Field: 'PrefIntDr', Value: self.FilterPrefIntDr() });
        if (self.FilterIntPegged() != "") filters.push({ Field: 'IntPegged', Value: self.FilterIntPegged() });
        if (self.FilterPeggingFreq() != "") filters.push({ Field: 'PeggingFreq', Value: self.FilterPeggingFreq() });
        if (self.FilterPegReviewDate() != "") filters.push({ Field: 'PegReviewDate', Value: self.FilterPegReviewDate() });
        if (self.FilterStartDate() != "") filters.push({ Field: 'StartDate', Value: self.FilterStartDate() });
        if (self.FilterEndDate() != "") filters.push({ Field: 'EndDate', Value: self.FilterEndDate() });
        if (self.FilterInterestMargin() != "") filters.push({ Field: 'InterestMargin', Value: self.FilterInterestMargin() });
        return filters;
    };
    var UploadFDModel = {
        FileTipe: ko.observable(),
        dtimeFile: ko.observable(),
        DataInformation: ko.observableArray([])
    }
    self.UploadFD = ko.observable(UploadFDModel);
    self.IsIM = ko.observable(false);
    self.IsLoanIM = ko.observable(false);
    self.IsLoanRO = ko.observable(false);
    self.IsSuccess = ko.observable(false);
    self.InsertedURL = ko.observable('');
    self.FileName = ko.observable('');
    self.SearchDate = ko.observable(self.LocalDate(new Date, true, false));

    self.GetParameters = function () {
        GetParameters();
    }

    var FDInterestModel = {
        FDMaturityID: ko.observable(),
        FDIMSelect: ko.observable(),
        SchemeType: ko.observable(),
        AccNumber: ko.observable(),
        IntTableCode: ko.observable(),
        Version: ko.observable(),
        IntCr: ko.observable(),
        IntDr: ko.observable(),
        IntPegged: ko.observable(),
        PegReviewDate: ko.observable(),
        StartDate: ko.observable(),
        EndDate: ko.observable(),
        IntMargin: ko.observable(),
    };
    self.FDInterest = ko.observableArray([]);
    self.OnUploadChange = function () {
        self.IsIM(false);
        self.IsLoanIM(false);
        self.IsLoanRO(false);
        if (viewModel.Selected().Filetipes() == 3) {
            self.IsIM(true);
        }
        else if (viewModel.Selected().Filetipes() == 4) {
            self.IsLoanIM(true);
        }
        else if (viewModel.Selected().Filetipes() == 5) {
            self.IsLoanRO(true);
        }
        //if (viewModel.Parameter.FileTipe.ID == 3) {
                
        //}

        self.InsertedURL(null);
        self.FileName(null);
        self.IsSuccess(false);
        self.FDInterest([]);
        viewModel.FDInterest([]);
    }

    function FDBindIM() {
        var Invest;
        if (viewModel.Selected().Filetipes() == 3) {
            Invest = {
                url: api.server + api.url.FDGenerate + "/" + self.SearchDate(),
                params: {
                    page: self.GridProperties().Page(),
                    size: self.GridProperties().Size(),
                    sort_column: self.GridProperties().SortColumn(),
                    sort_order: self.GridProperties().SortOrder()
                },
                token: accessToken
            };
        }
        if (viewModel.Selected().Filetipes() == 4) {
            Invest = {
                url: api.server + api.url.LoanIMGenerate + "/" + self.SearchDate(),
                params: {
                    page: self.GridProperties().Page(),
                    size: self.GridProperties().Size(),
                    sort_column: self.GridProperties().SortColumn(),
                    sort_order: self.GridProperties().SortOrder()
                },
                token: accessToken
            };
        }
        if (viewModel.Selected().Filetipes() == 5) {
            Invest = {
                url: api.server + api.url.LoanROGenerate + "/" + self.SearchDate(),
                params: {
                    page: self.GridProperties().Page(),
                    size: self.GridProperties().Size(),
                    sort_column: self.GridProperties().SortColumn(),
                    sort_order: self.GridProperties().SortOrder()
                },
                token: accessToken
            };
        }
        var filters = GetFilteredColumns();
        if (filters.length > 0) {
            // POST
            // add request body on POST
            Invest.data = ko.toJSON(filters);

            Helper.Ajax.Post(Invest, OnSuccessBindIM, OnError, OnAlways);
        } else {
            // GET
            Helper.Ajax.Get(Invest, OnSuccessBindIM, OnError, OnAlways);
        }
    }

    self.FDBindIM = function () { FDBindIM(); };
    self.CheckAll = ko.computed({
        read: function () {
            // Get selected when dependent children are selected
            /*var someSelected = false;
            var dataArray = self.FDInterest();
            dataArray.forEach(function (param) {
                if (param.FDIMSelect) {
                    someSelected = true;
                }
            });
            return someSelected;*/
        },
        write: function (newState) {
            // If checked / unchecked, propagate this change to children. This isn't called if we're only
            // only checking the group checkbox because of a change to a dependent.
            var dataArray = self.FDInterest();
            dataArray.forEach(function (param) {
                param.FDIMSelect = newState;
            });
            //console.log(dataArray);
            self.FDInterest(ko.mapping.toJS(dataArray));
        }
    });

    function OnSuccessBindIM(data, textStatus, jqXHR) {
        if (jqXHR.status == 200) {
            self.FDInterest(data.Rows);
            //viewModel.FDInterest(data.Rows);

            self.GridProperties().Page(data['Page']);
            self.GridProperties().Size(data['Size']);
            self.GridProperties().Total(data['Total']);
            self.GridProperties().TotalPages(Math.ceil(self.GridProperties().Total() / self.GridProperties().Size()));
        } else {
            ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', false);
        }
    }
    self.FDGenerate = function () {
       // UploadFDModel.FileTipe("");
        UploadFDModel.DataInformation([]);
        UploadFDModel.dtimeFile("");
        if (viewModel.Selected().Filetipes() == 1) {
            UploadFDModel.FileTipe("FD_RTGS");
        }
        else if (viewModel.Selected().Filetipes() == 2) {
            UploadFDModel.FileTipe("FD_SKN");
        }
        else if (viewModel.Selected().Filetipes() == 3) {
            UploadFDModel.FileTipe("FD_IM");
            var dataArray = self.FDInterest();
            dataArray.forEach(function (param) {
                if (param.FDIMSelect) {
                    var dInfo = {
                        ID: param.FDMaturityID,
                        TipeDesc: ''
                    };
                    UploadFDModel.DataInformation.push(dInfo);
                }
            });
            /*for (var i = 0; i < viewModel.FDInterest().length; i++) {
                if (self.FDInterest()[i].FDIMSelect == true) {
                    var dInfo = {
                        ID: self.FDInterest()[i].FDMaturityID,
                        TipeDesc: ''
                    };
                    UploadFDModel.DataInformation.push(dInfo);
                }
            }*/
            if (UploadFDModel.DataInformation().length <= 0) {
                ShowNotification("Attention", "You need to select transaction", 'gritter-warning', false);
                return;
            }
        }
        else if (viewModel.Selected().Filetipes() == 4) {
            UploadFDModel.FileTipe("LOAN_IM");
            var dataArray = self.FDInterest();
            dataArray.forEach(function (param) {
                if (param.FDIMSelect) {
                    var dInfo = {
                        ID: param.FDMaturityID,
                        TipeDesc: ''
                    };
                    UploadFDModel.DataInformation.push(dInfo);
                }
            });

            //var dInfo = {
            //    ID: 1,
            //    TipeDesc: ''
            //};
            //var dInfo2 = {
            //    ID: 2,
            //    TipeDesc: ''
            //};
            //UploadFDModel.DataInformation.push(dInfo);
            //UploadFDModel.DataInformation.push(dInfo2);

            if (UploadFDModel.DataInformation().length <= 0) {
                ShowNotification("Attention", "You need to select transaction", 'gritter-warning', false);
                return;
            }
        }
        else if (viewModel.Selected().Filetipes() == 5) {
            UploadFDModel.FileTipe("LOAN_RO");
            var dataArray = self.FDInterest();
            dataArray.forEach(function (param) {
                if (param.FDIMSelect) {
                    var dInfo = {
                        ID: param.FDMaturityID,
                        TipeDesc: ''
                    };
                    UploadFDModel.DataInformation.push(dInfo);
                }
            });

            //var dInfo = {
            //    ID: param.FDMaturityID,
            //    TipeDesc: ''
            //};
            //UploadFDModel.DataInformation.push(dInfo);

            if (UploadFDModel.DataInformation().length <= 0) {
                ShowNotification("Attention", "You need to select transaction", 'gritter-warning', false);
                return;
            }
        }
        else {
            return;
        }
        UploadFDModel.dtimeFile(self.SearchDate());
        GenerateProcess(UploadFDModel);
    }
    function GenerateProcess(UploadFDModel) {
        var options = {
            url: "/_vti_bin/DBSGenerateFileV2/GenerateFileV2Service.svc/GenerateFileV2",
            data: ko.toJSON(UploadFDModel)
        };
        Helper.Sharepoint.Nintex.Post(options, OnSuccessGenerateFile, OnError, OnAlways);
    }

    function OnSuccessGenerateFile(data, textStatus, jqXHR) {
        if (data.IsSuccess == true) {
            ShowNotification("Success", "Generate File Success", 'gritter-success', false);
            self.InsertedURL(data.InsertedURL);
            self.FileName(data.FileName);
            self.IsSuccess(true);
        }
        else {
            ShowNotification("Attention", "Generate File failed", 'gritter-warning', false);
            self.InsertedURL(null);
            self.FileName(null);
            self.IsSuccess(false);
        }
    }


    function GetParameters() {
        if (viewModel.Modul() != null || viewModel.Modul() != '') {
            switch (viewModel.Modul()) {
                case "fd":
                    var UTipe = [{ ID: 1, TipeDesc: "RTGS" }, { ID: 2, TipeDesc: "SKN" }, { ID: 3, TipeDesc: "Interest Maintenance" }, ];
                    viewModel.Parameter().Filetipes(UTipe);
                    break;
                case "loan":
                    var UTipe = [{ ID: 4, TipeDesc: "Loan Interest Maintenance" }, { ID: 5, TipeDesc: "Loan Rollover" }];
                    viewModel.Parameter().Filetipes(UTipe);
                    break;
                default:
                    break;
            }

        }
    }

    // Function to display notification. 
    function ShowNotification(title, text, className, isSticky) {
        $.gritter.add({
            title: title,
            text: text,
            class_name: className,
            sticky: isSticky,
        });
    }

    function Confirm(text) {
        bootbox.confirm(text, function (result) {
            return result;
        });
    }

    function OnError(jqXHR, textStatus, errorThrown) {
        ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', false);
    }

    function OnAlways() {
        //$box.trigger('reloaded.ace.widget');
    }
    // Event handlers declaration end
    // Token validation On Success function
    function OnSuccessToken(data, textStatus, jqXHR) {
        // store token on browser cookies
        $.cookie(api.cookie.name, data.AccessToken);
        accessToken = $.cookie(api.cookie.name);

        // read spuser from cookie
        if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
            spUser = $.cookie(api.cookie.spUser);
            viewModel.SPUser(ko.mapping.toJS(spUser));
        }

        // get parameter values
        viewModel.GetParameters();
    }
};
// View Model
var viewModel = new ViewModel();

$(document).ready(function () {
    if ($.cookie(api.cookie.name) == undefined) {
        Helper.Token.Request(OnSuccessToken, OnError);
    } else {
        accessToken = $.cookie(api.cookie.name);
        if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
            spUser = $.cookie(api.cookie.spUser);
            viewModel.SPUser(ko.mapping.toJS(spUser));
        }
        viewModel.GetParameters();
        //console.log( viewModel.Parameter().Filetipes());
    }


    $('.date-picker').datepicker({ autoclose: true }).next().on(ace.click_event, function () {
        $(this).prev().focus();
    });

    //$("#aspnetForm").validate({onsubmit: false});
    $('#aspnetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: true,
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        }
    });
    // ace file upload
    $('#document-path').ace_file_input({
        no_file: 'No File ...',
        btn_choose: 'Choose',
        btn_change: 'Change',
        droppable: false,
        thumbnail: false, //| true | large
        blacklist: 'exe|dll'
    });
    // Knockout custom file handler
    ko.bindingHandlers.file = {
        init: function (element, valueAccessor) {
            $(element).change(function () {
                var file = this.files[0];

                if (ko.isObservable(valueAccessor()))
                    valueAccessor()(file);
            });
        }
    };

    // Knockout Datepicker custom binding handler
    ko.bindingHandlers.datepicker = {
        init: function (element) {
            $(element).datepicker({ autoclose: true }).next().on(ace.click_event, function () {
                $(this).prev().focus();
            });
        },
        update: function (element) {
            $(element).datepicker("refresh");
        }
    };

    // Dependant Observable for dropdown auto fill
    ko.dependentObservable(function () {

        var Filetipes = ko.utils.arrayFirst(viewModel.Parameter().Filetipes(), function (item) {
            return item.ID == viewModel.Selected().Filetipes();
        });

        //if (Filetipes != null) viewModel.UploadFD().Filetipes(Filetipes);
    });
    ko.applyBindings(new ViewModel());
});

function OnSuccessToken(data, textStatus, jqXHR) {
    // store token on browser cookies
    $.cookie(api.cookie.name, data.AccessToken);
    accessToken = $.cookie(api.cookie.name);

    // read spuser from cookie
    if ($.cookie(api.cookie.name) != undefined && $.cookie(api.cookie.spUser) != undefined) {
        spUser = $.cookie(api.cookie.spUser);
        viewModel.SPUser(ko.mapping.toJS(spUser));
    }

    // get parameter values
    viewModel.GetParameters();
}
function OnError(jqXHR, textStatus, errorThrown) {
    ShowNotification(jqXHR.status + " " + jqXHR.statusText, jqXHR.responseText, 'gritter-error', false);
}