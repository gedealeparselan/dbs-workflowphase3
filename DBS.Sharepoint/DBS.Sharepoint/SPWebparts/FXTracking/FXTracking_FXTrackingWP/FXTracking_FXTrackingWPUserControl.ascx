﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FXTracking_FXTrackingWPUserControl.ascx.cs" Inherits="DBS.Sharepoint.SPWebparts.FXTracking.FXTracking_FXTrackingWP.FXTracking_FXTrackingWPUserControl" %>



<h1 class="header smaller no-margin-top lighter dark">All Book Deal Transaction</h1>
<div id="add-transaction">
    <div id="widget" class="widget-box">
        <!-- widget header start -->
        <div class="widget-header widget-hea1der-small header-color-dark">
            <h6>All Book Deal Transaction</h6>

            <div class="widget-toolbar">
                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                    <i class="blue icon-filter"></i>
                    <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: GridProperties().AllowFilter" />
                    <span class="lbl"></span>
                </label>
                <a href="#" data-bind="click: GetTransactionData" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                    <i class="blue icon-refresh"></i>
                </a>
            </div>
        </div>
        <!-- widget header end -->
        <!-- widget body start -->
        <div class="widget-body">
            <!-- widget main start -->
            <div class="widget-main padding-0">
                <!-- widget slim control start -->
                <div class="slim-scroll" data-height="400">
                    <!-- widget content start -->
                    <div class="content">

                        <!-- table responsive start -->
                        <div class="table-responsive">
                            <div class="dataTables_wrapper" role="grid">
                                <table id="workflow-task-table" class="table table-striped table-bordered table-hover dataTable"
                                    workflow-state="running, error"
                                    workflow-outcome="pending, custom"
                                    workflow-custom-outcome=""
                                    workflow-show-contribute="true">
                                    <thead>
                                        <tr data-bind="with: GridProperties">
                                            <th>No.</th>
                                            <th data-bind="click: function () { Sorting('TZReference') }, css: GetSortedColumn('TZReference')">Booking Reference</th>
                                            <th data-bind="click: function () { Sorting('CustomerName') }, css: GetSortedColumn('CustomerName')">Customer Name</th>
                                            <th data-bind="click: function () { Sorting('Product') }, css: GetSortedColumn('Product')">Product</th>
                                            <th data-bind="click: function () { Sorting('TradeDate') }, css: GetSortedColumn('TradeDate')">Trade Date</th>
                                            <th data-bind="click: function () { Sorting('ValueDate') }, css: GetSortedColumn('ValueDate')">Value Date</th>
                                            <th data-bind="click: function () { Sorting('CurrencyDesc') }, css: GetSortedColumn('CurrencyDesc')">Currency</th>
                                            <th data-bind="click: function () { Sorting('Amount') }, css: GetSortedColumn('Amount')">Trxn Amount</th>
                                            <th data-bind="click: function () { Sorting('AccountNumber') }, css: GetSortedColumn('AccountNumber')">Account Number</th>
                                            <th data-bind="click: function () { Sorting('IsFXTransaction') }, css: GetSortedColumn('IsFXTransaction')">FX Transaction</th>
                                            <th data-bind="click: function () { Sorting('TransactionStatus') }, css: GetSortedColumn('TransactionStatus')">Transaction Status</th>
                                            <th data-bind="click: function () { Sorting('TzStatus') }, css: GetSortedColumn('TzStatus')">TZ Status</th>
                                            <th data-bind="click: function () { Sorting('UpdateDate') }, css: GetSortedColumn('UpdateDate')">Modified Date</th>
                                        </tr>
                                    </thead>
                                    <thead data-bind="visible: GridProperties().AllowFilter" class="table-filter">
                                        <tr>
                                            <th class="clear-filter">
                                                <a href="#" data-bind="click: ClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                    <i class="green icon-trash"></i>
                                                </a>
                                            </th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterTZReference, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCustomer, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterProduct, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.FilterTradeDate, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.FilterValueDate, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterCurrency, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterAmount, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterAccountNumber, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterFXTransaction, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterTransactionStatus, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.FilterTzStatus, event: { change: GridProperties().Filter }" /></th>
                                            <th>
                                                <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.FilterUpdateDate, event: { change: GridProperties().Filter }" /></th>
                                        </tr>
                                    </thead>
                                    <tbody data-bind="foreach: TransactionGridModel, visible: $root.TransactionGridModel().length > 0">
                                        <tr data-bind="click: $root.GetSelectedRow" data-toggle="modal">
                                            <td><span data-bind="text: RowID"></span></td>
                                            <td style="white-space: nowrap"><span data-bind="text: TZReference"></span></td>
                                            <td><span data-bind="text: CustomerName"></span></td>
                                            <td><span data-bind="text: Product"></span></td>
                                            <td><span data-bind="text: $root.LocalDate(TradeDate, true, false)"></span></td>
                                            <td><span data-bind="text: $root.LocalDate(ValueDate, true, false)"></span></td>
                                            <td><span data-bind="text: CurrencyDesc"></span></td>
                                            <td align="right"><span data-bind="text: formatNumber(Amount)"></span></td>
                                            <td><span data-bind="text: AccountNumber"></span></td>
                                            <td><span data-bind="text: IsFxTransaction"></span></td>
                                            <td><span data-bind="text: TransactionStatus"></span></td>
                                            <td><span data-bind="text: TzStatus"></span></td>
                                            <td><span data-bind="text: $root.LocalDate(UpdateDate)"></span></td>
                                        </tr>
                                    </tbody>
                                    <tbody data-bind="visible: $root.TransactionGridModel().length == 0">
                                        <tr>
                                            <td colspan="13" class="text-center">No entries available.
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- table responsive end -->
                    </div>
                    <!-- widget content end -->
                </div>
                <!-- widget slim control end -->
                <!-- widget footer start -->
                <div class="widget-toolbox padding-8 clearfix">
                    <div class="row" data-bind="with: GridProperties">
                        <!-- pagination size start -->
                        <div class="col-sm-6">
                            <div class="dataTables_paginate paging_bootstrap pull-left">
                                Showing
                                <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                rows of <span data-bind="text: Total"></span>
                                entries
                            </div>
                        </div>
                        <!-- pagination size end -->
                        <!-- pagination page jump start -->
                        <div class="col-sm-3">
                            <div class="dataTables_paginate paging_bootstrap">
                                Page
                                <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                of <span data-bind="text: TotalPages"></span>
                            </div>
                        </div>
                        <!-- pagination page jump end -->
                        <!-- pagination navigation start -->
                        <div class="col-sm-3">
                            <div class="dataTables_paginate paging_bootstrap">
                                <ul class="pagination">
                                    <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                            <i class="icon-double-angle-left"></i>
                                        </a>
                                    </li>
                                    <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                            <i class="icon-angle-left"></i>
                                        </a>
                                    </li>
                                    <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                            <i class="icon-angle-right"></i>
                                        </a>
                                    </li>
                                    <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                            <i class="icon-double-angle-right"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- pagination navigation end -->

                    </div>
                </div>
                <!-- widget footer end -->

            </div>
            <!-- widget main end -->
        </div>
        <!-- widget body end -->
    </div>
    <!-- modal form start -->
    <div id="modal-form" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" style="width: 125%;">
            <div id="backDrop" class="absCustomBackDrop topCustomBackDrop darkCustomBackDrop hideCustomBackDrop fullHeightCustomBackDrop fullWidthCustomBackDrop centerCustomBackDrop"></div>
            <div class="modal-content">
                <div class="header smaller no-margin-top lighter dark">
                    <button type="button" class="bootbox-close-button close" data-dismiss="modal" style="margin-top: -10px; font-size: 30px; margin-right: 6px;" data-bind="click: $root.OnCloseApproval">×</button>
                    <h2><span>&nbsp</span>
                        Transaction
								<small>
                                    <i class="icon-double-angle-right"></i>
                                    <span data-bind="text: ActivityTitle"></span>
                                </small>
                    </h2>
                </div>
                <!-- modal body start -->
                <div class="modal-body overflow-visible">

                    <div class="row">
                        <div class="col-xs-12">

                            <div id="transaction-progress" class="col-sm-4 col-sm-offset-4" style="display: none">
                                <h4 class="smaller blue">Please wait...</h4>
                                <span class="space-4"></span>
                                <div class="progress progress-small progress-striped active">
                                    <div class="progress-bar progress-bar-info" style="width: 100%"></div>
                                </div>
                            </div>
                            <div id="transaction-data" data-bind="template: { name: ApprovalTemplate(), data: ApprovalData() }"></div>

                        </div>
                    </div>
                </div>
                <!-- modal body end -->
                <!-- modal footer start -->
                <div class="modal-footer">
                    <div class="space-8"></div>
                    <!-- dynamic outcomes button start -->
                    <span>
                        <button id="btnDraft" data-bind="click: $root.Submit, visible: $root.ActivityTitle() == 'TMO' && $root.IsShowSubmit(), disable: !$root.IsEditable()" class="btn btn-sm btn-info">
                            <i class="icon-edit"></i>
                            <span>Draft</span>
                        </button>
                        <button data-bind="click: $root.Submit, visible: $root.IsShowSubmit, disable: !$root.IsEditable()" class="btn btn-sm btn-primary">
                            <i class="icon-save"></i>
                            <span>Submit</span>
                        </button>
                        <button data-bind="click: $root.SubmitCorrection, visible: $root.IsShowSubmitCorrection, disable: !$root.IsEditable()" class="btn btn-sm btn-primary">
                            <i class="icon-save"></i>
                            <span>Submit Correction</span>
                        </button>
                    </span>
                    <!-- dynamic outcomes button end -->

                    <button class="btn btn-sm btn-success" data-dismiss="modal" data-bind="click: $root.OnCloseApproval">
                        <i class="icon-remove"></i>
                        Close
                    </button>
                </div>
                <!-- modal footer end -->

            </div>
        </div>
    </div>
    <!-- modal form end -->
    <!-- Modal upload form start -->
    <div id="modal-form-upload" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" style="width: 75%;">
            <div class="modal-content">
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="customer-form1" class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="document-type">
                                        Document Type</label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="document-type" name="document-type" data-rule-required="true" data-rule-value="true" class="col-sm-6" data-bind="options: ddlDocumentType_a, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Options..', value: Selected().DocumentType"></select>
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="document-purpose">
                                        Purpose of Docs</label>

                                    <div class="col-sm-7">
                                        <div class="clearfix">
                                            <select id="document-purpose" name="document-purpose" data-rule-required="true" data-rule-value="true" class="col-sm-6" data-bind="options: ddlDocumentPurposeNoFx, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Options..', value: Selected().DocumentPurpose"></select>
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>

                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="document-path">
                                        Document</label>
                                    <div class="col-sm-7">
                                        <div class="clearfix">
                                            <input type="file" id="document-path" name="document-path" data-bind="file: DocumentPath" /><!---->
                                        </div>
                                    </div>
                                    <label class="control-label bolder text-danger">*</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.AddDocument, disable: !IsEditable()">
                        <i class="icon-save"></i>
                        Save
                    </button>
                    <button class="btn btn-sm" data-dismiss="modal" data-bind="click: $root.CloseAttach, disable: !IsEditable()">
                        <i class="icon-remove"></i>
                        Cancel
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal upload form end -->
    <!--Tambah Agung -->
    <!-- modal form start Attach Document Underlying Form start -->
    <div id="modal-form-instruction" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" style="width: 100%">
            <div class="modal-content">
                <!-- modal header start Attach FIle Form -->
                <div class="modal-header">
                    <h4 class="blue bigger">
                        <span>Select Document</span>
                    </h4>
                </div>
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="Name">
                                Purpose of Doc
                            </label>
                            <div class="col-sm-9">
                                <div class="clearfix">
                                    <select id="documentPurpose" name="documentPurpose" data-bind="value: $root.DocumentPurpose_a().ID, options: ddlDocumentPurpose_a, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                    <label class="control-label bolder text-danger">*</label>
                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="space-4"></div>
                        <div id="widget-boxins" class="widget-box" data-bind="visible: $root.SelectingInstruction">
                            <div id="widget-boxinstruction" class="widget-box">
                                <div class="widget-header widget-hea1der-small header-color-dark">
                                    <h6>Instruction Document Table</h6>

                                    <div class="widget-toolbar">
                                        <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                            <i class="blue icon-filter"></i>
                                            <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: InstructionGridProperties().AllowFilter" />
                                            <span class="lbl"></span>
                                        </label>
                                        <a href="#" data-bind="click: GetDataInstructionDocument" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                            <i class="blue icon-refresh"></i>
                                        </a>
                                    </div>
                                </div>
                                <!-- widget header end -->
                                <!-- widget body underlying Attach start -->
                                <div class="widget-body">
                                    <!-- widget main start -->
                                    <div class="widget-main padding-0">
                                        <!-- widget slim control start -->
                                        <div class="slim-scroll" data-height="400">
                                            <!-- widget content start -->
                                            <div class="content">
                                                <!-- table responsive start -->
                                                <div class="table-responsive">
                                                    <div class="dataTables_wrapper" role="grid">
                                                        <table id="instruction-table" class="table table-striped table-bordered table-hover dataTable">
                                                            <thead>
                                                                <tr data-bind="with: $root.InstructionGridProperties">
                                                                    <th style="width: 50px">No.</th>
                                                                    <th style="width: 50px">Select</th>
                                                                    <th data-bind="click: function () { Sorting('ApplicationID'); }, css: GetSortedColumn('ApplicationID')">Application ID</th>
                                                                    <th data-bind="click: function () { Sorting('FileName'); }, css: GetSortedColumn('FileName')">File Name</th>
                                                                    <th data-bind="click: function () { Sorting('PurposeofDoc'); }, css: GetSortedColumn('PurposeofDocs')">Purpose of Docs</th>
                                                                    <th data-bind="click: function () { Sorting('DocumentType'); }, css: GetSortedColumn('DocumentType')">Type of Doc</th>
                                                                    <th data-bind="click: function () { Sorting('LastModifiedDate'); }, css: GetSortedColumn('LastModifiedDate')">Modified Date</th>
                                                                </tr>
                                                            </thead>
                                                            <thead data-bind="visible: $root.InstructionGridProperties().AllowFilter" class="table-filter">
                                                                <tr>
                                                                    <th class="clear-filter">
                                                                        <a href="#" data-bind="click: $root.InstructionClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                            <i class="green icon-trash"></i>
                                                                        </a>
                                                                    </th>
                                                                    <th class="clear-filter"></th>
                                                                    <th>
                                                                        <input type="text" class="input-sm col-xs-12" data-bind="value: $root.InstructionFilterApplicationID, event: { change: InstructionGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" class="input-sm col-xs-12" data-bind="value: $root.InstructionFilterFileName, event: { change: InstructionGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" class="input-sm col-xs-12" data-bind="value: $root.InstructionFilterPurposeofDoc, event: { change: InstructionGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" class="input-sm col-xs-12" data-bind="value: $root.InstructionFilterDocumentType, event: { change: InstructionGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.InstructionFilterLastModifiedDate, event: { change: InstructionGridProperties().Filter }" /></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody data-bind="foreach: { data: $root.InstructionDocuments, as: 'AttachData' }, visible: ($root.InstructionDocuments() != null && $root.InstructionDocuments().length > 0)">
                                                                <tr>
                                                                    <td><span data-bind="text: ID"></span></td>
                                                                    <td align="center">
                                                                        <input type="checkbox" id="isSelectedinstruction2" name="isSelectedinstruction" data-bind="checked: IsSelected, event: { change: function (data) { $root.onSelectionInstruction($index(), $data) } }" /></td>
                                                                    <td><span data-bind="text: ApplicationID"></span></td>
                                                                    <td><a data-bind="attr: { href: DocumentPath, target: '_blank' }, text: FileName"></a></td>
                                                                    <td><span data-bind="text: Purpose.Name"></span></td>
                                                                    <td><span data-bind="text: Type.Name"></span></td>
                                                                    <td><span data-bind="text: $root.LocalDate(LastModifiedDate, true, false)"></span></td>
                                                                </tr>
                                                            </tbody>
                                                            <tbody data-bind="visible: ($root.InstructionDocuments() != null && $root.InstructionDocuments().length <= 0)">
                                                                <tr>
                                                                    <td colspan="11" class="text-center">No
																				entries
																				available.
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <!-- table responsive end -->
                                            </div>
                                            <!-- widget content end -->
                                        </div>
                                        <!-- widget slim control end -->
                                        <!-- widget footer start -->
                                        <div class="widget-toolbox padding-8 clearfix">
                                            <div class="row" data-bind="with: InstructionGridProperties">
                                                <!-- pagination size start -->
                                                <div class="col-sm-6">
                                                    <div class="dataTables_paginate paging_bootstrap pull-left">
                                                        Showing
                                                        <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                                        rows of <span data-bind="text: Total"></span>
                                                        entries
                                                    </div>
                                                </div>
                                                <!-- pagination size end -->
                                                <!-- pagination page jump start -->
                                                <div class="col-sm-3">
                                                    <div class="dataTables_paginate paging_bootstrap">
                                                        Page
                                                        <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                        of <span data-bind="text: TotalPages"></span>
                                                    </div>
                                                </div>
                                                <!-- pagination page jump end -->
                                                <!-- pagination navigation start -->
                                                <div class="col-sm-3">
                                                    <div class="dataTables_paginate paging_bootstrap">
                                                        <ul class="pagination">
                                                            <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                    <i class="icon-double-angle-left"></i>
                                                                </a>
                                                            </li>
                                                            <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                    <i class="icon-angle-left"></i>
                                                                </a>
                                                            </li>
                                                            <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                    <i class="icon-angle-right"></i>
                                                                </a>
                                                            </li>
                                                            <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                    <i class="icon-double-angle-right"></i>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <!-- pagination navigation end -->

                                            </div>
                                        </div>
                                        <!-- widget footer end -->

                                    </div>
                                    <!-- widget main end -->
                                </div>
                                <!-- widget body end -->
                            </div>
                        </div>
                        <div class="space-4"></div>
                        <div id="widget-box2" class="widget-box" data-bind="visible: $root.SelectingUnderlying">
                            <div id="widget-boxinstruction2" class="widget-box">
                                <div class="widget-header widget-hea1der-small header-color-dark">
                                    <h6>Instruction Document Table</h6>

                                    <div class="widget-toolbar">
                                        <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                            <i class="blue icon-filter"></i>
                                            <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: InstructionGridProperties().AllowFilter" />
                                            <span class="lbl"></span>
                                        </label>
                                        <a href="#" data-bind="click: GetDataInstructionDocument" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                            <i class="blue icon-refresh"></i>
                                        </a>
                                    </div>
                                </div>
                                <!-- widget header end -->
                                <!-- widget body underlying Attach start -->
                                <div class="widget-body">
                                    <!-- widget main start -->
                                    <div class="widget-main padding-0">
                                        <!-- widget slim control start -->
                                        <div class="slim-scroll" data-height="400">
                                            <!-- widget content start -->
                                            <div class="content">
                                                <!-- table responsive start -->
                                                <div class="table-responsive">
                                                    <div class="dataTables_wrapper" role="grid">
                                                        <table id="instruction-table1" class="table table-striped table-bordered table-hover dataTable">
                                                            <thead>
                                                                <tr data-bind="with: $root.InstructionGridProperties">
                                                                    <th style="width: 50px">No.</th>
                                                                    <th style="width: 50px">Select</th>
                                                                    <th data-bind="click: function () { Sorting('ApplicationID'); }, css: GetSortedColumn('ApplicationID')">Application ID</th>
                                                                    <th data-bind="click: function () { Sorting('FileName'); }, css: GetSortedColumn('FileName')">File Name</th>
                                                                    <th data-bind="click: function () { Sorting('PurposeofDoc'); }, css: GetSortedColumn('PurposeofDocs')">Purpose of Docs</th>
                                                                    <th data-bind="click: function () { Sorting('DocumentType'); }, css: GetSortedColumn('DocumentType')">Type of Doc</th>
                                                                    <th data-bind="click: function () { Sorting('LastModifiedDate'); }, css: GetSortedColumn('LastModifiedDate')">Modified Date</th>
                                                                </tr>
                                                            </thead>
                                                            <thead data-bind="visible: $root.InstructionGridProperties().AllowFilter" class="table-filter">
                                                                <tr>
                                                                    <th class="clear-filter">
                                                                        <a href="#" data-bind="click: $root.InstructionClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                            <i class="green icon-trash"></i>
                                                                        </a>
                                                                    </th>
                                                                    <th class="clear-filter"></th>
                                                                    <th>
                                                                        <input type="text" class="input-sm col-xs-12" data-bind="value: $root.InstructionFilterApplicationID, event: { change: InstructionGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" class="input-sm col-xs-12" data-bind="value: $root.InstructionFilterFileName, event: { change: InstructionGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" class="input-sm col-xs-12" data-bind="value: $root.InstructionFilterPurposeofDoc, event: { change: InstructionGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" class="input-sm col-xs-12" data-bind="value: $root.InstructionFilterDocumentType, event: { change: InstructionGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.InstructionFilterLastModifiedDate, event: { change: InstructionGridProperties().Filter }" /></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody data-bind="foreach: { data: $root.InstructionDocuments, as: 'AttachData' }, visible: ($root.InstructionDocuments() != null && $root.InstructionDocuments().length > 0)">
                                                                <tr>
                                                                    <td><span data-bind="text: ID"></span></td>
                                                                    <td align="center">
                                                                        <input type="checkbox" id="isSelectedinstruction" name="isSelectedinstruction" data-bind="checked: IsSelected, event: { change: function (data) { $root.onSelectionInstruction($index(), $data) } }" /></td>
                                                                    <td><span data-bind="text: ApplicationID"></span></td>
                                                                    <td><a data-bind="attr: { href: DocumentPath, target: '_blank' }, text: FileName"></a></td>
                                                                    <td><span data-bind="text: Purpose.Name"></span></td>
                                                                    <td><span data-bind="text: Type.Name"></span></td>
                                                                    <td><span data-bind="text: $root.LocalDate(LastModifiedDate, true, false)"></span></td>
                                                                </tr>
                                                            </tbody>
                                                            <tbody data-bind="visible: ($root.InstructionDocuments() != null && $root.InstructionDocuments().length <= 0)">
                                                                <tr>
                                                                    <td colspan="11" class="text-center">No
																				entries
																				available.
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <!-- table responsive end -->
                                            </div>
                                            <!-- widget content end -->
                                        </div>
                                        <!-- widget slim control end -->
                                        <!-- widget footer start -->
                                        <div class="widget-toolbox padding-8 clearfix">
                                            <div class="row" data-bind="with: InstructionGridProperties">
                                                <!-- pagination size start -->
                                                <div class="col-sm-6">
                                                    <div class="dataTables_paginate paging_bootstrap pull-left">
                                                        Showing
                                                        <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                                        rows of <span data-bind="text: Total"></span>
                                                        entries
                                                    </div>
                                                </div>
                                                <!-- pagination size end -->
                                                <!-- pagination page jump start -->
                                                <div class="col-sm-3">
                                                    <div class="dataTables_paginate paging_bootstrap">
                                                        Page
                                                        <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                        of <span data-bind="text: TotalPages"></span>
                                                    </div>
                                                </div>
                                                <!-- pagination page jump end -->
                                                <!-- pagination navigation start -->
                                                <div class="col-sm-3">
                                                    <div class="dataTables_paginate paging_bootstrap">
                                                        <ul class="pagination">
                                                            <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                    <i class="icon-double-angle-left"></i>
                                                                </a>
                                                            </li>
                                                            <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                    <i class="icon-angle-left"></i>
                                                                </a>
                                                            </li>
                                                            <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                    <i class="icon-angle-right"></i>
                                                                </a>
                                                            </li>
                                                            <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                    <i class="icon-double-angle-right"></i>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <!-- pagination navigation end -->

                                            </div>
                                        </div>
                                        <!-- widget footer end -->

                                    </div>
                                    <!-- widget main end -->
                                </div>
                                <!-- widget body end -->
                            </div>
                            <!-- widget header Underlying Attach start -->
                            <div class="widget-header widget-hea1der-small header-color-dark">
                                <h6>Underlying Form Table</h6>

                                <div class="widget-toolbar">
                                    <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                        <i class="blue icon-filter"></i>
                                        <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: UnderlyingAttachGridProperties().AllowFilter" />
                                        <span class="lbl"></span>
                                    </label>
                                    <a href="#" data-bind="click: GetDataUnderlyingAttach" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                        <i class="blue icon-refresh"></i>
                                    </a>
                                </div>
                            </div>
                            <!-- widget header end -->
                            <!-- widget body underlying Attach start -->
                            <div class="widget-body">
                                <!-- widget main start -->
                                <div class="widget-main padding-0">
                                    <!-- widget slim control start -->
                                    <div class="slim-scroll" data-height="400">
                                        <!-- widget content start -->
                                        <div class="content">
                                            <!-- table responsive start -->
                                            <div class="table-responsive">
                                                <div class="dataTables_wrapper" role="grid">
                                                    <table id="Customer Underlying-table2" class="table table-striped table-bordered table-hover dataTable">
                                                        <thead>
                                                            <tr data-bind="with: $root.UnderlyingAttachGridProperties">
                                                                <!-- <th style="width:50px">Select</th> -->
                                                                <th style="width: 50px">No.</th>
                                                                <th style="width: 50px">Select</th>
                                                                <th data-bind="click: function () { Sorting('StatementLetter'); }, css: GetSortedColumn('StatementLetter')">Statement
																			Letter</th>
                                                                <th data-bind="click: function () { Sorting('UnderlyingDocument'); }, css: GetSortedColumn('UnderlyingDocument')">Underlying
																			Document</th>
                                                                <th data-bind="click: function () { Sorting('DocumentType'); }, css: GetSortedColumn('DocumentType')">Type
																			of
																			Doc</th>
                                                                <th data-bind="click: function () { Sorting('Currency'); }, css: GetSortedColumn('Currency')">Currency</th>
                                                                <th data-bind="click: function () { Sorting('Amount'); }, css: GetSortedColumn('Amount')">Amount</th>
                                                                <th data-bind="click: function () { Sorting('DateOfUnderlying'); }, css: GetSortedColumn('DateOfUnderlying')">Date</th>
                                                                <th data-bind="click: function () { Sorting('SupplierName'); }, css: GetSortedColumn('SupplierName')">Supplier
																			Name</th>
                                                                <th data-bind="click: function () { Sorting('ReferenceNumber'); }, css: GetSortedColumn('ReferenceNumber')">Reference
																			Number</th>
                                                                <th data-bind="click: function () { Sorting('ExpiredDate'); }, css: GetSortedColumn('ExpiredDate')">Expiry
																			Date</th>
                                                            </tr>
                                                        </thead>
                                                        <thead data-bind="visible: $root.UnderlyingAttachGridProperties().AllowFilter" class="table-filter">
                                                            <tr>
                                                                <th class="clear-filter">
                                                                    <a href="#" data-bind="click: $root.UnderlyingAttachClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                        <i class="green icon-trash"></i>
                                                                    </a>
                                                                </th>
                                                                <th class="clear-filter"></th>
                                                                <th>
                                                                    <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterStatementLetter, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                <th>
                                                                    <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterUnderlyingDocument, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                <th>
                                                                    <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterDocumentType, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                <th>
                                                                    <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterCurrency, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                <th>
                                                                    <input type="text" class="input-sm col-xs-12 input-numericonly" data-bind="value: $root.UnderlyingAttachFilterAmount, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                <th>
                                                                    <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.UnderlyingAttachFilterDateOfUnderlying, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                <th>
                                                                    <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterSupplierName, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                <th>
                                                                    <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterReferenceNumber, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                <th>
                                                                    <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.UnderlyingAttachFilterExpiredDate, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody data-bind="foreach: { data: $root.CustomerAttachUnderlyings, as: 'AttachData' }, visible: ($root.CustomerAttachUnderlyings() != null && $root.CustomerAttachUnderlyings().length > 0)">
                                                            <tr>
                                                                <td><span data-bind="text: RowID"></span></td>
                                                                <td align="center">
                                                                    <input type="checkbox" id="isSelectedins" name="isSelect2" data-bind="checked: IsSelectedAttach, event: { change: function (data) { $root.onSelectionAttachUnderlying($index(), $data) } }, enable: $root.SelectingUnderlying()" /></td>
                                                                <td><span data-bind="text: StatementLetter.Name"></span></td>
                                                                <td><span data-bind="text: UnderlyingDocument.Name"></span></td>
                                                                <td><span data-bind="text: DocumentType.Name"></span></td>
                                                                <td><span data-bind="text: Currency.Code"></span></td>
                                                                <td align="right"><span data-bind="text: formatNumber(Amount)"></span></td>
                                                                <td><span data-bind="text: $root.LocalDate(DateOfUnderlying, true, false)"></span></td>
                                                                <td><span data-bind="text: SupplierName"></span></td>
                                                                <td><span data-bind="text: ReferenceNumber"></span></td>
                                                                <td><span data-bind="text: $root.LocalDate(ExpiredDate, true, false)"></span></td>
                                                            </tr>
                                                        </tbody>
                                                        <tbody data-bind="visible: ($root.CustomerAttachUnderlyings() != null && $root.CustomerAttachUnderlyings().length <= 0)">
                                                            <tr>
                                                                <td colspan="11" class="text-center">No
																				entries
																				available.
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <!-- table responsive end -->
                                        </div>
                                        <!-- widget content end -->
                                    </div>
                                    <!-- widget slim control end -->
                                    <!-- widget footer start -->
                                    <div class="widget-toolbox padding-8 clearfix">
                                        <div class="row" data-bind="with: UnderlyingAttachGridProperties">
                                            <!-- pagination size start -->
                                            <div class="col-sm-6">
                                                <div class="dataTables_paginate paging_bootstrap pull-left">
                                                    Showing
                                                        <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                                    rows of <span data-bind="text: Total"></span>
                                                    entries
                                                </div>
                                            </div>
                                            <!-- pagination size end -->
                                            <!-- pagination page jump start -->
                                            <div class="col-sm-3">
                                                <div class="dataTables_paginate paging_bootstrap">
                                                    Page
                                                        <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                    of <span data-bind="text: TotalPages"></span>
                                                </div>
                                            </div>
                                            <!-- pagination page jump end -->
                                            <!-- pagination navigation start -->
                                            <div class="col-sm-3">
                                                <div class="dataTables_paginate paging_bootstrap">
                                                    <ul class="pagination">
                                                        <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                            <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                <i class="icon-double-angle-left"></i>
                                                            </a>
                                                        </li>
                                                        <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                            <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                <i class="icon-angle-left"></i>
                                                            </a>
                                                        </li>
                                                        <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                            <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                <i class="icon-angle-right"></i>
                                                            </a>
                                                        </li>
                                                        <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                            <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                <i class="icon-double-angle-right"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <!-- pagination navigation end -->

                                        </div>
                                    </div>
                                    <!-- widget footer end -->

                                </div>
                                <!-- widget main end -->
                            </div>
                            <!-- widget body end -->
                        </div>
                        <div class="space-4"></div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-sm" data-dismiss="modal" data-bind="click: $root.cancel_a">
                            <i class="icon-remove"></i>
                            Close
                        </button>
                    </div>
                    <!-- modal footer end attach file -->

                </div>
            </div>
        </div>
    </div>
    <!-- modal form end Attach Document Underlying Form end -->
    <!-- End -->
    <!-- modal form start Attach Document Underlying Form start -->
    <div id="modal-form-Attach" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" style="width: 100%">
            <div class="modal-content">
                <!-- modal header start Attach FIle Form -->
                <div class="modal-header">
                    <h4 class="blue bigger">
                        <span>Added Attachment File</span>
                    </h4>
                </div>
                <!-- modal header end Attach file -->
                <!-- modal body start Attach File -->
                <div class="modal-body overflow-visible">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- modal body form start -->
                            <div id="customer-form" class="form-horizontal" role="form">
                                <div class="space-4"></div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="Name">
                                        Purpose of Doc
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="documentPurpose" name="documentPurpose" data-bind="value: $root.DocumentPurpose_a().ID, options: ddlDocumentPurpose_a, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="description">
                                        Type of Doc
                                    </label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select id="documentType1" name="documentType" data-bind="value: $root.DocumentType_a().ID, options: ddlDocumentType_a, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                            <label class="control-label bolder text-danger">*</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="document-path-upload">
                                        Document</label>

                                    <div class="col-sm-8">
                                        <div class="clearfix">
                                            <input type="file" id="document-path-upload" class="col-xs-8" name="document-path-upload" data-bind="file: DocumentPath_a" /><!---->
                                        </div>
                                    </div>
                                    <label class="control-label bolder text-danger" style="position: absolute;">*</label>
                                </div>
                            </div>
                            <!-- modal body form Attach File End -->
                            <!-- widget box Underlying Attach start -->
                            <div id="widget-box1" class="widget-box">
                                <!-- widget header Underlying Attach start -->
                                <div class="widget-header widget-hea1der-small header-color-dark">
                                    <h6>Underlying Form Table</h6>

                                    <div class="widget-toolbar">
                                        <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                            <i class="blue icon-filter"></i>
                                            <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: UnderlyingAttachGridProperties().AllowFilter" />
                                            <span class="lbl"></span>
                                        </label>
                                        <a href="#" data-bind="click: GetDataUnderlyingAttach" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                            <i class="blue icon-refresh"></i>
                                        </a>
                                    </div>
                                </div>
                                <!-- widget header end -->
                                <!-- widget body underlying Attach start -->
                                <div class="widget-body">
                                    <!-- widget main start -->
                                    <div class="widget-main padding-0">
                                        <!-- widget slim control start -->
                                        <div class="slim-scroll" data-height="400">
                                            <!-- widget content start -->
                                            <div class="content">
                                                <!-- table responsive start -->
                                                <div class="table-responsive">
                                                    <div class="dataTables_wrapper" role="grid">
                                                        <table id="Customer Underlying-table1" class="table table-striped table-bordered table-hover dataTable">
                                                            <thead>
                                                                <tr data-bind="with: $root.UnderlyingAttachGridProperties">
                                                                    <!-- <th style="width:50px">Select</th> -->
                                                                    <th style="width: 50px">No.</th>
                                                                    <th style="width: 50px">Select</th>
                                                                    <th data-bind="click: function () { Sorting('StatementLetter'); }, css: GetSortedColumn('StatementLetter')">Statement
																			Letter</th>
                                                                    <th data-bind="click: function () { Sorting('UnderlyingDocument'); }, css: GetSortedColumn('UnderlyingDocument')">Underlying
																			Document</th>
                                                                    <th data-bind="click: function () { Sorting('DocumentType'); }, css: GetSortedColumn('DocumentType')">Type
																			of
																			Doc</th>
                                                                    <th data-bind="click: function () { Sorting('Currency'); }, css: GetSortedColumn('Currency')">Currency</th>
                                                                    <th data-bind="click: function () { Sorting('Amount'); }, css: GetSortedColumn('Amount')">Amount</th>
                                                                    <th data-bind="click: function () { Sorting('DateOfUnderlying'); }, css: GetSortedColumn('DateOfUnderlying')">Date</th>
                                                                    <th data-bind="click: function () { Sorting('SupplierName'); }, css: GetSortedColumn('SupplierName')">Supplier
																			Name</th>
                                                                    <th data-bind="click: function () { Sorting('ReferenceNumber'); }, css: GetSortedColumn('ReferenceNumber')">Reference
																			Number</th>
                                                                    <th data-bind="click: function () { Sorting('ExpiredDate'); }, css: GetSortedColumn('ExpiredDate')">Expiry
																			Date</th>
                                                                </tr>
                                                            </thead>
                                                            <thead data-bind="visible: $root.UnderlyingAttachGridProperties().AllowFilter" class="table-filter">
                                                                <tr>
                                                                    <th class="clear-filter">
                                                                        <a href="#" data-bind="click: $root.UnderlyingAttachClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                            <i class="green icon-trash"></i>
                                                                        </a>
                                                                    </th>
                                                                    <th class="clear-filter"></th>
                                                                    <th>
                                                                        <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterStatementLetter, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterUnderlyingDocument, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterDocumentType, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterCurrency, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" class="input-sm col-xs-12 input-numericonly" data-bind="value: $root.UnderlyingAttachFilterAmount, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.UnderlyingAttachFilterDateOfUnderlying, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterSupplierName, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" class="input-sm col-xs-12" data-bind="value: $root.UnderlyingAttachFilterReferenceNumber, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                    <th>
                                                                        <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.UnderlyingAttachFilterExpiredDate, event: { change: UnderlyingAttachGridProperties().Filter }" /></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody data-bind="foreach: { data: $root.CustomerAttachUnderlyings, as: 'AttachData' }, visible: ($root.CustomerAttachUnderlyings() != null && $root.CustomerAttachUnderlyings().length > 0)">
                                                                <tr>
                                                                    <td><span data-bind="text: RowID"></span></td>
                                                                    <td align="center">
                                                                        <input type="checkbox" id="isSelected2" name="isSelect2" data-bind="checked: IsSelectedAttach, event: { change: function (data) { $root.onSelectionAttach($index(), $data) } }, enable: $root.SelectingUnderlying()" /></td>
                                                                    <td><span data-bind="text: StatementLetter.Name"></span></td>
                                                                    <td><span data-bind="text: UnderlyingDocument.Name"></span></td>
                                                                    <td><span data-bind="text: DocumentType.Name"></span></td>
                                                                    <td><span data-bind="text: Currency.Code"></span></td>
                                                                    <td align="right"><span data-bind="text: formatNumber(Amount)"></span></td>
                                                                    <td><span data-bind="text: $root.LocalDate(DateOfUnderlying, true, false)"></span></td>
                                                                    <td><span data-bind="text: SupplierName"></span></td>
                                                                    <td><span data-bind="text: ReferenceNumber"></span></td>
                                                                    <td><span data-bind="text: $root.LocalDate(ExpiredDate, true, false)"></span></td>
                                                                </tr>
                                                            </tbody>
                                                            <tbody data-bind="visible: ($root.CustomerAttachUnderlyings() != null && $root.CustomerAttachUnderlyings().length <= 0)">
                                                                <tr>
                                                                    <td colspan="11" class="text-center">No
																				entries
																				available.
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <!-- table responsive end -->
                                            </div>
                                            <!-- widget content end -->
                                        </div>
                                        <!-- widget slim control end -->
                                        <!-- widget footer start -->
                                        <div class="widget-toolbox padding-8 clearfix">
                                            <div class="row" data-bind="with: UnderlyingAttachGridProperties">
                                                <!-- pagination size start -->
                                                <div class="col-sm-6">
                                                    <div class="dataTables_paginate paging_bootstrap pull-left">
                                                        Showing
                                                        <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                                        rows of <span data-bind="text: Total"></span>
                                                        entries
                                                    </div>
                                                </div>
                                                <!-- pagination size end -->
                                                <!-- pagination page jump start -->
                                                <div class="col-sm-3">
                                                    <div class="dataTables_paginate paging_bootstrap">
                                                        Page
                                                        <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                        of <span data-bind="text: TotalPages"></span>
                                                    </div>
                                                </div>
                                                <!-- pagination page jump end -->
                                                <!-- pagination navigation start -->
                                                <div class="col-sm-3">
                                                    <div class="dataTables_paginate paging_bootstrap">
                                                        <ul class="pagination">
                                                            <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                    <i class="icon-double-angle-left"></i>
                                                                </a>
                                                            </li>
                                                            <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                    <i class="icon-angle-left"></i>
                                                                </a>
                                                            </li>
                                                            <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                    <i class="icon-angle-right"></i>
                                                                </a>
                                                            </li>
                                                            <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                    <i class="icon-double-angle-right"></i>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <!-- pagination navigation end -->

                                            </div>
                                        </div>
                                        <!-- widget footer end -->

                                    </div>
                                    <!-- widget main end -->
                                </div>
                                <!-- widget body end -->
                            </div>
                            <!-- widget box end -->
                        </div>
                    </div>
                </div>
                <!-- modal body end Attach File -->
                <!-- modal footer start Attach File-->
                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.save_a, disable: $root.IsUploading()">
                        <i class="icon-save"></i>
                        Save
                    </button>
                    <button class="btn btn-sm" data-dismiss="modal" data-bind="click: $root.cancel_a">
                        <i class="icon-remove"></i>
                        Cancel
                    </button>
                </div>
                <!-- modal footer end attach file -->

            </div>
        </div>
    </div>
    <!-- modal form end Attach Document Underlying Form end -->
    <!-- Modal Double Transaction start -->
    <div id="modal-double-transaction" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">
        <div class="modal-dialog" style="width: 950px;">
            <div class="modal-content">
                <!-- modal header start -->
                <div class="alert alert-danger no-margin-bottom">
                    <strong>
                        <i class="icon-ban-circle"></i>
                        Double Transaction!
                    </strong>
                    This transaction has same information with existing
							transaction. To continue with this transaction, PPU
							/ Branch Ops Checker validation is required.
                </div>
                <!-- modal header end -->
                <!-- modal body start -->
                <div class="modal-body overflow-visible">
                    <h3 class="header no-padding-top no-margin-top smaller lighter dark">Existing Transactions</h3>

                    <div class="table-responsive">
                        <div class="dataTables_wrapper" role="grid">
                            <table class="table table-striped table-bordered table-hover dataTable">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>ApplicationID</th>
                                        <th>Customer Name</th>
                                        <th>Currency</th>
                                        <th>Trxn Amount</th>
                                        <th>Rate</th>
                                        <th>Eqv. USD</th>
                                        <th>Debit Account CCY</th>
                                        <th>Application Date</th>
                                    </tr>
                                </thead>
                                <tbody data-bind="foreach: DoubleTransactions">
                                    <tr>
                                        <td><span data-bind="text: $index() + 1"></span></td>
                                        <td style="white-space: nowrap"><span data-bind="text: ApplicationID"></span></td>
                                        <!-- <td style="white-space: nowrap"><span data-bind="text: TZReference"></span></td>-->
                                        <td><span data-bind="text: Customer.Name"></span></td>
                                        <td><span data-bind="text: Currency.Code"></span></td>
                                        <td align="right"><span data-bind="text: formatNumber(Amount)"></span></td>
                                        <td><span data-bind="text: Rate"></span></td>
                                        <td><span data-bind="text: AmountUSD"></span></td>
                                        <td><span data-bind="text: Account.Currency.Code"></span></td>
                                        <td><span data-bind="text: $root.LocalDate(ValueDate)"></span></td>
                                        <!--<td><span data-bind="text: moment(ValueDate).format('DD-M-YYYY')"></span></td>-->
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="space-4"></div>

                    <!-- form verify login start -->
                    <h3 class="header smaller lighter dark">PPU / Branch
							Ops. Validation</h3>
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="checker-login-form" class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="verify-userid">
                                        User ID</label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input id="userid" type="text" placeholder="User ID" class="col-xs-10 col-sm-5" data-rule-required="true" data-bind="value: UserValidation().UserID" />
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="verify-password">
                                        Password</label>

                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <input id="verify-password" type="password" placeholder="Password" class="col-xs-10 col-sm-5" data-rule-required="true" data-bind="value: UserValidation().Password" />
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" data-bind="with: UserValidation">
                                    <label class="col-sm-3 control-label no-padding-right"></label>

                                    <label class="col-sm-9">
                                        <span class="red" data-bind="text: Error"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- form verify login end -->

                </div>
                <!-- modal body end -->
                <!-- modal footer start -->
                <div class="modal-footer no-margin-top">
                    <button class="btn btn-sm btn-primary" data-bind="click: ValidationProcess">
                        <i class="icon-key"></i>
                        Submit
                    </button>
                    <button class="btn btn-sm btn-success" data-bind="click: ContinueEditing" data-dismiss="modal">
                        <i class="icon-remove"></i>
                        Cancel
                    </button>
                </div>
                <!-- modal footer end -->
            </div>
        </div>
    </div>
    <!-- Modal Double Transaction end -->
    <!-- modal form start Underlying Form -->
    <div id="modal-form-Underlying" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" style="width: 100%">
            <div class="modal-content">
                <!-- modal header start Underlying Form -->
                <div class="modal-header">
                    <h4 class="blue bigger">
                        <span data-bind="if: $root.IsNewDataUnderlying()">Create a new Customer Underlying Parameter</span>
                        <span data-bind="if: !$root.IsNewDataUnderlying()">Modify a Customer Underlying Parameter</span>
                    </h4>
                </div>
                <!-- modal header end -->
                <!-- modal body start -->
                <div class="modal-body overflow-visible">
                    <div id="modalUnderlyingtest">
                        <div class="row">
                            <div class="col-xs-12">
                                <!-- modal body form start -->

                                <div id="customerUnderlyingform" class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="JointAccountID">
                                            Joint Account</label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <select id="book-joinAcc2" name="book-statement" data-bind="value: $root.IsJointAccount_u,
    options: $root.JointAccounts,
    optionsText: 'Name',
    optionsValue: 'ID',
    event: { change: $root.OnChangeJointAccUnderlying }, enable: false"
                                                    class="col-xs-3">
                                                </select>&nbsp;      
                                             <select id="debit-acc-number" name="debit-acc-number" data-rule-required="true" data-bind="options: $root.JointAccountNumbers,
    optionsText: function (item) {
        if (item.CustomerName != null) {
            return item.AccountNumber + ' (' + item.Currency.Code + ') - ' + item.CustomerName
        } return item.AccountNumber + ' (' + item.Currency.Code + ')'
    },
    optionsValue: 'AccountNumber',
    optionsCaption: 'Please Select...',
    value: $root.AccountNumber_u,
    event: { change: $root.OnChangeAccountNumber }, visible: $root.IsJointAccount_u, enable: false"
                                                 class="col-xs-5">
                                             </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="StatementLetterID">
                                            Statement Letter
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <select id="statementLetter" name="statementLetter" data-bind="value: $root.StatementLetter_u().ID, options: $root.ddlStatementLetter_u, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Please Select...', enable: $root.IsStatementA(), event: { change: $root.OnChangeStatementLetterUnderlying }" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                                <label class="control-label bolder text-danger" for="StatementLetter_u">*</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="UnderlyingDocument">
                                            Underlying Document
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <select id="underlyingDocument" name="underlyingDocument" data-bind="value: UnderlyingDocument_u().ID, options: $root.ddlUnderlyingDocument_u, optionsText: 'CodeName', optionsValue: 'ID', optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                                <label class="control-label bolder text-danger" for="UnderlyingDocument_u">*</label>
                                            </div>
                                        </div>
                                    </div>

                                    <!--    						<span aa-bind="text:UnderlyingDocument_u().Code()"></span>-->
                                    <div data-bind="visible: UnderlyingDocument_u().ID() == '50'" class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="otherUnderlying">
                                            Other Underlying
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" id="otherUnderlying" name="otherUnderlying" data-bind="value: $root.OtherUnderlying_u" class="col-xs-7" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="DocumentType">
                                            Type of Document
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <select id="documentType" name="documentType" data-bind="value: $root.DocumentType_u().ID, options: $root.ddlDocumentType_u, optionsText: 'Name', optionsValue: 'ID', optionsCaption: 'Please Select...'" data-rule-required="true" data-rule-value="true" class="col-xs-7"></select>
                                                <label class="control-label bolder text-danger" for="DocumentType_u">*</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="Currency">
                                            Transaction Currency
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <select id="currency_u" name="currency_u" data-bind="value: $root.Currency_u().ID, disable: true, options: $root.ddlCurrency_u, optionsText: 'Code', optionsValue: 'ID', optionsCaption: 'Please Select...', event: { change: function (data) { OnCurrencyChange(Currency_u().ID()) } }" data-rule-required="true" data-rule-value="true" class="col-xs-2"></select>
                                                <%--$root.StatementLetter_u().ID()==1--%>
                                                <label class="control-label bolder text-danger" for="Currency_u">*</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="Amount">
                                            Invoice Amount
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" id="Amount_u" name="Amount_u" data-bind="value: $root.Amount_u(), disable: $root.StatementLetter_u().ID() == 1, valueUpdate: ['afterkeydown', 'propertychange', 'input']" onkeyup="read_u();" data-rule-required="true" data-rule-number="true" class="col-xs-4 align-right" />
                                                <label class="control-label bolder text-danger" for="Amount_u">*</label>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="Rate">Rate</label>

                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" class="col-sm-4 align-right" name="rate_u" id="rate_u" disabled="disabled" data-rule-required="true" data-rule-number="true" data-bind="value: formatNumber($root.Rate_u())" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="Eqv.USD">Eqv. USD</label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" class="col-sm-4 align-right" disabled="disabled" name="eqv-usd_u" id="eqv-usd_u" data-rule-required="true" data-rule-number="true" data-bind="value: formatNumber($root.AmountUSD_u())" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="DateOfUnderlying">
                                            Date of Underlying
                                        </label>
                                        <div class="col-sm-2">
                                            <div class="input-group">
                                                <input type="text" autocomplete="off" id="DateOfUnderlying" data-date-format="dd-M-yyyy" name="DateOfUnderlying_u" data-bind="value: DateOfUnderlying_u, event: { change: $root.onChangeDateOfUnderlying }" class="form-control date-picker col-xs-6" data-rule-required="true" />
                                                <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                                <label class="control-label bolder text-danger starremove" for="DateOfUnderlying_u" style="position: absolute;">*</label>
                                            </div>
                                        </div>
                                        <label class="col-sm-2 control-label no-padding-right" for="ExpiredDate">
                                            Expiry Date
                                        </label>
                                        <div class="col-sm-2">
                                            <div class="input-group">
                                                <input type="text" autocomplete="off" id="ExpiredDate" data-date-format="dd-M-yyyy" name="ExpiredDate" data-bind="value: ExpiredDate_u" class="form-control date-picker col-xs-6" data-rule-required="true" />
                                                <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                                <label class="control-label bolder text-danger starremove" for="ExpiredDate_u" style="position: absolute;">*</label>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group" data-bind="visible: false">
                                        <label class="col-sm-3 control-label no-padding-right" for="IsDeclarationOfException">
                                            Declaration of Exception
                                        </label>
                                        <div class="col-sm-9">
                                            <label>
                                                <input name="switch-field-1" id="IsDeclarationOfException" data-bind="checked: $root.IsDeclarationOfException_u" class="ace ace-switch ace-switch-6" type="checkbox" />
                                                <span class="lbl"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="space-2" data-bind="visible: false"></div>
                                    <div class="form-group" data-bind="visible: false">
                                        <label class="col-sm-3 control-label no-padding-right" for="StartDate">
                                            Start Date
                                        </label>
                                        <div class="col-sm-2">
                                            <div class="clearfix">
                                                <div class="input-group">
                                                    <input id="StartDate" name="StartDate" data-date-format="dd-M-yyyy" data-bind="value: StartDate_u" class="form-control date-picker col-xs-6" type="text" autocomplete="off" />
                                                    <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <label class="col-sm-2 control-label no-padding-right" for="EndDate">
                                            End Date
                                        </label>
                                        <div class="col-sm-2">
                                            <div class="clearfix">
                                                <div class="input-group">
                                                    <input type="text" autocomplete="off" id="EndDate" name="EndDate" data-date-format="dd-M-yyyy" data-bind="value: EndDate_u" class="form-control date-picker col-xs-6" />
                                                    <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="ReferenceNumber">
                                            Reference Number
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" id="ReferenceNumber" name="ReferenceNumber" data-bind="value: $root.ReferenceNumber_u" disabled="disabled" class="col-xs-9" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="SupplierName">
                                            Supplier Name
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" id="SupplierName" name="SupplierName" data-bind="value: $root.SupplierName_u" class="col-xs-8" data-rule-required="true" />
                                                <label class="control-label bolder text-danger" for="SupplierName_u">*</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="InvoiceNumber">
                                            Invoice Number
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <input type="text" autocomplete="off" id="invoiceNumber" name="invoiceNumber" data-bind="value: $root.InvoiceNumber_u" class="col-xs-8" data-rule-required="true" />
                                                <label class="control-label bolder text-danger" for="InvoiceNumber_u">*</label>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="AttachmentNo">
                                            Is This doc Purposed to replace the proforma doc
                                        </label>
                                        <div class="col-sm-9">
                                            <label>
                                                <input name="switch-field-1" id="IsProforma" data-bind="checked: IsProforma_u, disable: DocumentType_u().ID() == 2" class="ace ace-switch ace-switch-6" type="checkbox" />
                                                <span class="lbl"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <!-- grid proforma begin -->
                                    <!-- widget box start -->
                                    <div id="widget-box" class="widget-box" data-bind="visible: IsProforma_u()">
                                        <!-- widget header start -->
                                        <div class="widget-header widget-hea1der-small header-color-dark">
                                            <h6>Customer Underlying Proforma Table</h6>

                                            <div class="widget-toolbar">
                                                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                                    <i class="blue icon-filter"></i>
                                                    <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: UnderlyingProformaGridProperties().AllowFilter" />
                                                    <span class="lbl"></span>
                                                </label>
                                                <a href="#" data-bind="click: GetDataUnderlyingProforma" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                                    <i class="blue icon-refresh"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- widget header end -->
                                        <!-- widget body start -->
                                        <div class="widget-body">
                                            <!-- widget main start -->
                                            <div class="widget-main padding-0">
                                                <!-- widget slim control start -->
                                                <div class="slim-scroll" data-height="400">
                                                    <!-- widget content start -->
                                                    <div class="content">

                                                        <!-- table responsive start -->
                                                        <div class="table-responsive">
                                                            <div class="dataTables_wrapper" role="grid">
                                                                <table id="Customer Underlying-table" class="table table-striped table-bordered table-hover dataTable">
                                                                    <thead>
                                                                        <tr data-bind="with: UnderlyingProformaGridProperties">
                                                                            <th style="width: 50px">No.</th>
                                                                            <th style="width: 50px">Select</th>
                                                                            <th data-bind="click: function () { Sorting('StatementLetter'); }, css: GetSortedColumn('StatementLetter')">Statement Letter</th>
                                                                            <th data-bind="click: function () { Sorting('UnderlyingDocument'); }, css: GetSortedColumn('UnderlyingDocument')">Underlying Document</th>
                                                                            <th data-bind="click: function () { Sorting('DocumentType'); }, css: GetSortedColumn('DocumentType')">Type of Doc</th>
                                                                            <th data-bind="click: function () { Sorting('Currency'); }, css: GetSortedColumn('Currency')">Currency</th>
                                                                            <th data-bind="click: function () { Sorting('Amount'); }, css: GetSortedColumn('Amount')">Amount</th>
                                                                            <th data-bind="click: function () { Sorting('SupplierName'); }, css: GetSortedColumn('SupplierName')">Supplier Name</th>
                                                                            <th data-bind="click: function () { Sorting('ReferenceNumber'); }, css: GetSortedColumn('ReferenceNumber')">Reference Number</th>
                                                                            <th data-bind="click: function () { Sorting('DateOfUnderlying'); }, css: GetSortedColumn('DateOfUnderlying')">Date</th>
                                                                            <th data-bind="click: function () { Sorting('ExpiredDate'); }, css: GetSortedColumn('ExpiredDate')">Expiry Date</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <thead data-bind="visible: UnderlyingProformaGridProperties().AllowFilter" class="table-filter">
                                                                        <tr>
                                                                            <th class="clear-filter">
                                                                                <a href="#" data-bind="click: ProformaClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                                    <i class="green icon-trash"></i>
                                                                                </a>
                                                                            </th>
                                                                            <th class="clear-filter"></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterStatementLetter, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterUnderlyingDocument, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterDocumentType, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterCurrency, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterAmount, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterSupplierName, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12" data-bind="value: $root.ProformaFilterReferenceNumber, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.ProformaFilterDateOfUnderlying, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" autocomplete="off" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.ProformaFilterExpiredDate, event: { change: UnderlyingProformaGridProperties().Filter }" /></th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody data-bind="foreach: { data: CustomerUnderlyingProformas, as: 'ProformasData' }, visible: CustomerUnderlyingProformas().length > 0">
                                                                        <tr>
                                                                            <td><span data-bind="text: RowID"></span></td>
                                                                            <td align="center">
                                                                                <input type="checkbox" id="isSelectedProforma" name="isSelect" data-bind="checked: ProformasData.IsSelectedProforma, event: { change: function (data) { $root.onSelectionProforma($index(), $data) } }" /></td>
                                                                            <td><span data-bind="text: StatementLetter.Name"></span></td>
                                                                            <td><span data-bind="text: UnderlyingDocument.Name"></span></td>
                                                                            <td><span data-bind="text: DocumentType.Name"></span></td>
                                                                            <td><span data-bind="text: Currency.Code"></span></td>
                                                                            <td><span data-bind="text: Amount"></span></td>
                                                                            <td><span data-bind="text: SupplierName"></span></td>
                                                                            <td><span data-bind="text: ReferenceNumber"></span></td>
                                                                            <td><span data-bind="text: $root.LocalDate(DateOfUnderlying, true, false)"></span></td>
                                                                            <td><span data-bind="text: $root.LocalDate(ExpiredDate, true, false)"></span></td>
                                                                        </tr>
                                                                    </tbody>
                                                                    <tbody data-bind="visible: CustomerUnderlyingProformas().length == 0">
                                                                        <tr>
                                                                            <td colspan="11" class="text-center">No entries available.
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <!-- table responsive end -->
                                                    </div>
                                                    <!-- widget content end -->
                                                </div>
                                                <!-- widget slim control end -->
                                                <!-- widget footer start -->
                                                <div class="widget-toolbox padding-8 clearfix">
                                                    <div class="row" data-bind="with: UnderlyingProformaGridProperties">
                                                        <!-- pagination size start -->
                                                        <div class="col-sm-6">
                                                            <div class="dataTables_paginate paging_bootstrap pull-left">
                                                                Showing
                                                            <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                                                rows of <span data-bind="text: Total"></span>
                                                                entries
                                                            </div>
                                                        </div>
                                                        <!-- pagination size end -->
                                                        <!-- pagination page jump start -->
                                                        <div class="col-sm-3">
                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                Page
                                                            <input type="text" autocomplete="off" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                                of <span data-bind="text: TotalPages"></span>
                                                            </div>
                                                        </div>
                                                        <!-- pagination page jump end -->
                                                        <!-- pagination navigation start -->
                                                        <div class="col-sm-3">
                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                <ul class="pagination">
                                                                    <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                            <i class="icon-double-angle-left"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                            <i class="icon-angle-left"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                            <i class="icon-angle-right"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                            <i class="icon-double-angle-right"></i>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <!-- pagination navigation end -->

                                                    </div>
                                                </div>
                                                <!-- widget footer end -->

                                            </div>
                                            <!-- widget main end -->
                                        </div>
                                        <!-- widget body end -->

                                    </div>
                                    <!-- widget box end -->
                                    <!-- grid proforma end -->




                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="IsBulkUnderlying">
                                            Is This Bulk Underlying
                                        </label>
                                        <div class="col-sm-9">
                                            <label>
                                                <input name="switch-field-1" id="IsBulkUnderlying" data-bind="checked: IsBulkUnderlying_u" class="ace ace-switch ace-switch-6" type="checkbox" />
                                                <span class="lbl"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <!-- grid Bulk begin -->
                                    <!-- widget box start -->
                                    <div class="widget-box" data-bind="visible: IsBulkUnderlying_u()">
                                        <!-- widget header start -->
                                        <div class="widget-header widget-hea1der-small header-color-dark">
                                            <h6>Customer Bulk Underlying Table</h6>

                                            <div class="widget-toolbar">
                                                <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="Allow Filter">
                                                    <i class="blue icon-filter"></i>
                                                    <input type="checkbox" class="ace ace-switch ace-switch-3" data-bind="checked: UnderlyingBulkGridProperties().AllowFilter" />
                                                    <span class="lbl"></span>
                                                </label>
                                                <a href="#" data-bind="click: GetDataBulkUnderlying" class="tooltip-info" data-rel="tooltip" data-placement="top" title="Reload Content">
                                                    <i class="blue icon-refresh"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- widget header end -->
                                        <!-- widget body start -->
                                        <div class="widget-body">
                                            <!-- widget main start -->
                                            <div class="widget-main padding-0">
                                                <!-- widget slim control start -->
                                                <div class="slim-scroll" data-height="400">
                                                    <!-- widget content start -->
                                                    <div class="content">

                                                        <!-- table responsive start -->
                                                        <div class="table-responsive">
                                                            <div class="dataTables_wrapper" role="grid">
                                                                <table id="Customer-BulkUnderlying-table" class="table table-striped table-bordered table-hover dataTable">
                                                                    <thead>
                                                                        <tr data-bind="with: UnderlyingBulkGridProperties">
                                                                            <%-- <th style="width: 50px">No.</th>--%>
                                                                            <th style="width: 50px">Select</th>
                                                                            <th data-bind="click: function () { Sorting('StatementLetter'); }, css: GetSortedColumn('StatementLetter')">Statement Letter</th>
                                                                            <th data-bind="click: function () { Sorting('UnderlyingDocument'); }, css: GetSortedColumn('UnderlyingDocument')">Underlying Document</th>
                                                                            <th data-bind="click: function () { Sorting('DocumentType'); }, css: GetSortedColumn('DocumentType')">Type of Doc</th>
                                                                            <th data-bind="click: function () { Sorting('Currency'); }, css: GetSortedColumn('Currency')">Currency</th>
                                                                            <th data-bind="click: function () { Sorting('Amount'); }, css: GetSortedColumn('Amount')">Amount</th>
                                                                            <th data-bind="click: function () { Sorting('SupplierName'); }, css: GetSortedColumn('SupplierName')">Supplier Name</th>
                                                                            <th data-bind="click: function () { Sorting('InvoiceNumber'); }, css: GetSortedColumn('InvoiceNumber')">Invoice Number</th>
                                                                            <th data-bind="click: function () { Sorting('DateOfUnderlying'); }, css: GetSortedColumn('DateOfUnderlying')">Date</th>
                                                                            <th data-bind="click: function () { Sorting('ExpiredDate'); }, css: GetSortedColumn('ExpiredDate')">Expiry Date</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <thead data-bind="visible: UnderlyingBulkGridProperties().AllowFilter" class="table-filter">
                                                                        <tr>
                                                                            <th class="clear-filter">
                                                                                <a href="#" data-bind="click: BulkClearFilters" class="tooltip-info" data-rel="tooltip" data-placement="right" title="Clear Filters">
                                                                                    <i class="green icon-trash"></i>
                                                                                </a>
                                                                            </th>
                                                                            <%--<th class="clear-filter"></th>--%>
                                                                            <th>
                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterStatementLetter, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterUnderlyingDocument, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterDocumentType, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterCurrency, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterSupplierName, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterAmount, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" class="input-sm col-xs-12" data-bind="value: $root.BulkFilterInvoiceNumber, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.BulkFilterDateOfUnderlying, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                            <th>
                                                                                <input type="text" class="input-sm col-xs-12 date-picker" data-date-format="dd-M-yyyy" data-bind="value: $root.BulkFilterExpiredDate, event: { change: UnderlyingBulkGridProperties().Filter }" /></th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody data-bind="foreach: { data: CustomerBulkUnderlyings, as: 'BulkDatas' }, visible: (CustomerBulkUnderlyings() != null && CustomerBulkUnderlyings().length > 0)">
                                                                        <tr>
                                                                            <%-- <td><span data-bind="text: RowID"></span></td>--%>
                                                                            <td align="center">
                                                                                <input type="checkbox" id="isSelectedBulk" name="isSelect" data-bind="checked: BulkDatas.IsSelectedBulk, event: { change: function (data) { $root.onSelectionBulk($index(), $data) } }" /></td>
                                                                            <td><span data-bind="text: StatementLetter.Name"></span></td>
                                                                            <td><span data-bind="text: UnderlyingDocument.Name"></span></td>
                                                                            <td><span data-bind="text: DocumentType.Name"></span></td>
                                                                            <td><span data-bind="text: Currency.Code"></span></td>
                                                                            <td align="right"><span data-bind="text: formatNumber(Amount)"></span></td>
                                                                            <td><span data-bind="text: SupplierName"></span></td>
                                                                            <td><span data-bind="text: InvoiceNumber"></span></td>
                                                                            <td><span data-bind="text: $root.LocalDate(DateOfUnderlying, true, false)"></span></td>
                                                                            <td><span data-bind="text: $root.LocalDate(ExpiredDate, true, false)"></span></td>
                                                                        </tr>
                                                                    </tbody>
                                                                    <tbody data-bind="visible: (CustomerBulkUnderlyings() != null && CustomerBulkUnderlyings().length <= 0)">
                                                                        <tr>
                                                                            <td colspan="11" class="text-center">No entries available.
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <!-- table responsive end -->
                                                    </div>
                                                    <!-- widget content end -->
                                                </div>
                                                <!-- widget slim control end -->
                                                <!-- widget footer start -->
                                                <div class="widget-toolbox padding-8 clearfix">
                                                    <div class="row" data-bind="with: UnderlyingBulkGridProperties">
                                                        <!-- pagination size start -->
                                                        <div class="col-sm-6">
                                                            <div class="dataTables_paginate paging_bootstrap pull-left">
                                                                Showing
                                                            <select data-bind="options: AvailableSizes, selectedOptions: Size, event: { change: OnPageSizeChange }" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Showing Page Entries"></select>
                                                                rows of <span data-bind="text: Total"></span>
                                                                entries
                                                            </div>
                                                        </div>
                                                        <!-- pagination size end -->
                                                        <!-- pagination page jump start -->
                                                        <div class="col-sm-3">
                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                Page
                                                            <input type="text" data-bind="value: Page, event: { change: OnPageChange }" style="width: 50px" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Jump to Page" />
                                                                of <span data-bind="text: TotalPages"></span>
                                                            </div>
                                                        </div>
                                                        <!-- pagination page jump end -->
                                                        <!-- pagination navigation start -->
                                                        <div class="col-sm-3">
                                                            <div class="dataTables_paginate paging_bootstrap">
                                                                <ul class="pagination">
                                                                    <li data-bind="click: FirstPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="First Page">
                                                                            <i class="icon-double-angle-left"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li data-bind="click: PreviousPage, attr: { class: Page() > 1 ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Previous Page">
                                                                            <i class="icon-angle-left"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li data-bind="click: NextPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Next Page">
                                                                            <i class="icon-angle-right"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li data-bind="click: LastPage, attr: { class: Page() < TotalPages() ? '' : 'disabled' }">
                                                                        <a href="#" class="tooltip-info" data-rel="tooltip" data-placement="bottom" title="Last Page">
                                                                            <i class="icon-double-angle-right"></i>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <!-- pagination navigation end -->

                                                    </div>
                                                </div>
                                                <!-- widget footer end -->

                                            </div>
                                            <!-- widget main end -->
                                        </div>
                                        <!-- widget body end -->

                                    </div>
                                    <!-- widget box end -->
                                    <!-- grid Bulk end -->

                                </div>

                                <!-- modal body form end -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- modal body end -->
                <!-- modal footer start -->
                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.save_u, visible: $root.IsNewDataUnderlying(), disable: !$root.IsEditTableUnderlying()">
                        <i class="icon-save"></i>
                        Save
                    </button>
                    <button class="btn btn-sm btn-primary" data-bind="click: $root.update_u, visible: !$root.IsNewDataUnderlying() && !$root.IsUtilize_u()">
                        <i class="icon-edit"></i>
                        Update
                    </button>
                    <button class="btn btn-sm btn-warning" data-bind="click: $root.delete_u, visible: !$root.IsNewDataUnderlying() && !$root.IsUtilize_u()">
                        <i class="icon-trash"></i>
                        Delete
                    </button>
                    <button class="btn btn-sm" data-bind="click: $root.cancel_u" data-dismiss="modal">
                        <i class="icon-remove"></i>
                        Cancel
                    </button>
                </div>
                <!-- modal footer end -->

            </div>
        </div>
    </div>
    <!-- modal form end Underlying Form -->


</div>

<script type="text/html" id="NewTransactionFXTracking" src="/Pages/NewTransaction/NewTransactionFXTracking.html"></script>
<script type="text/html" id="NewTransactionTMOTracking" src="/Pages/NewTransaction/NewTransactionTMOTracking.html"></script>

<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/date-time/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/additional-methods.min.js"></script>
<script type="text/javascript" src="/_catalogs/masterpage/Ace/assets/js/Knockout/knockout.mapping-latest.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/Helper.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/CalculatedFX.js"></script>
<script type="text/javascript" src="/SiteAssets/Scripts/UpdateDealTransaction.js"></script>