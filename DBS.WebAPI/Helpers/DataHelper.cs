﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace DBS.WebAPI.Helpers
{
    public class DataHelper
    {
        public static string StrConn = "Server=.;Database=DBS;User ID=sa;Password=pass@word1";
        //System.Configuration.ConfigurationManager.ConnectionStrings["DBSDatabase"].ToString();

        public static int SQLCommandTimeout =
            Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings.Get("SqlCommandTimeOut"));

        public static DataTable ExecuteDataTable(string sql)
        {
            DataTable dsData = new DataTable();
            try
            {
                using (SqlConnection conn = new SqlConnection(StrConn))
                {
                    if (conn.State == ConnectionState.Closed) conn.Open();

                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = sql;
                        cmd.CommandTimeout = 7500;
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = conn;

                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dsData);
                    }
                }
                return dsData;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static void ExecuteNonQuery(string sql)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(StrConn))
                {
                    if (conn.State == ConnectionState.Closed) conn.Open();

                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = sql;
                        cmd.CommandTimeout = SQLCommandTimeout;
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = conn;

                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}